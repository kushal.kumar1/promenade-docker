<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::unprepared(file_get_contents('database/seeds/data/permissions.sql'));
        ;
        // $this->setAdmin();
    }

    // private function setAdmin()
    // {
    //     $role = new \Niyotail\Models\Role();
    //     $role->name = 'admin';
    //     $role->save();
    //     $employee=new \Niyotail\Models\Employee();
    //     $employee->name='Admin';
    //     $employee->email='admin@example.com';
    //     $employee->password=bcrypt('admin@123');
    //     $employee->save();
    //     $employee->roles()->attach($role);
    //     $employee->save();
    // }
}
