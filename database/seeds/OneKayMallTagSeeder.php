<?php

use Illuminate\Database\Seeder;
use Niyotail\Models\Tag;

class OneKayMallTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = new Tag();
        $tag->name = '1K-Mall';
        $tag->type = 'product';
        $tag->status = true;
        $tag->save();
    }
}
