<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RunMultiWarehouseSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::statement('UPDATE picklists p
//                JOIN picklist_items pi on pi.picklist_id = p.id
//                JOIN order_items oi on oi.id = pi.order_item_id
//                JOIN orders o on o.id = oi.order_id
//                set p.warehouse_id = o.warehouse_id');
//
//        DB::statement("UPDATE shipments
//                                join orders on orders.id = shipments.order_id
//                    set shipments.warehouse_id = orders.warehouse_id");

        $employees = \Niyotail\Models\Employee::doesntHave('warehouses')->get();
        $warehouses = \Niyotail\Models\Warehouse::get()->pluck('id')->toArray();
        foreach ($employees as $employee){
            $employee->warehouses()->sync($warehouses);
        }

//        DB::statement('UPDATE purchase_invoice_grn
//                    join purchase_invoices on purchase_invoices.id = purchase_invoice_grn.purchase_invoice_id
//                    set purchase_invoice_grn.warehouse_id = purchase_invoices.warehouse_id');
//
//        DB::statement('UPDATE purchase_invoice_debit_notes
//                    join purchase_invoices on purchase_invoices.id = purchase_invoice_debit_notes.purchase_invoice_id
//                    set purchase_invoice_debit_notes.warehouse_id = purchase_invoices.warehouse_id');
//
//        DB::statement('UPDATE return_orders
//                            join orders on orders.id = return_orders.order_id
//                            set return_orders.warehouse_id = orders.warehouse_id');
//
//        DB::statement('UPDATE invoices
//                        join shipments on invoices.shipment_id = shipments.id
//                        set invoices.warehouse_id = shipments.warehouse_id');
    }
}
