<?php

use Illuminate\Database\Seeder;
use Niyotail\Models\Permission;

class AuditPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            array('name' => 'view-audit', 'label' => 'View Audit', 'type' => 'Audit'),
            array('name' => 'end-audit', 'label' => 'End Audit', 'type' => 'Audit'),
            array('name' => 'create-audit', 'label' => 'Create Audit', 'type' => 'Audit'),
            array('name' => 'add-audit-item', 'label' => 'Add Audit Item', 'type' => 'Audit'),
            array('name' => 'remove-audit-item', 'label' => 'Remove Audit Item', 'type' => 'Audit'),
        ];

        Permission::insert($permissions);
    }
}
