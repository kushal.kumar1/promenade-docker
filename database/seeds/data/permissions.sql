SET NAMES utf8mb4;

INSERT INTO `permissions` (`name`, `label`, `type`) VALUES

(	'view-agent',	'View Agent',	'Agent'),
(	'update-agent',	'Update Agent',	'Agent'),
(	'create-agent',	'Create Agent',	'Agent'),

(  'view-product',	'View Product',	'Product'),
(  'create-product',	'Create Product',	'Product'),
(  'update-product',	'Update Product',	'Product'),

(  'view-attribute',	'View Attribute',	'Product'),
(  'create-attribute',	'Create Attribute',	'Product'),
(  'update-attribute',	'Update Attribute',	'Product'),
(  'delete-attribute',	'Delete Attribute',	'Product'),

(  'view-variant',	'View Variant',	'Product'),
(  'create-variant',	'Create Variant',	'Product'),
(  'update-variant',	'Update Variant',	'Product'),
(  'delete-variant',	'Delete Variant',	'Product'),

(  'view-brand',	'View Brand',	'Product'),
(  'create-brand',	'Create Brand',	'Product'),
(  'update-brand',	'Update Brand',	'Product'),

(  'create-collection',	'Create Collection',	'Product'),
(  'view-collection',	'View Collection',	'Product'),
(  'update-collection',	'Update Collection',	'Product'),

(  'view-badge',	'View Badge',	'Settings'),
(  'create-badge',	'Create Badge',	'Settings'),
(  'update-badge',	'Update Badge',	'Settings'),
(  'delete-badge',	'Delete Badge',	'Settings'),

(	'view-app_banner',	'View App Banner',	'Settings'),
(	'create-app_banner',	'Create App Banner',	'Settings'),
(	'update-app_banner',	'Update App Banner',	'Settings'),

(	'view-tax_class',	'View Tax Class',	'Settings'),
(	'create-tax_class',	'Create Tax Class',	'Settings'),
(	'update-tax_class',	'Update Tax Class',	'Settings'),

(	'view-warehouse',	'View Warehouse',	'Settings'),
(	'create-warehouse',	'Create Warehouse',	'Settings'),
(	'update-warehouse',	'Update Warehouse',	'Settings'),

(	'view-beat',	'View Beat',	'Beat'),
(	'update-beat',	'Update Beat',	'Beat'),
(	'create-beat',	'Create Beat',	'Beat'),

(	'view-discount',	'View Discount',	'Discount'),
(	'update-discount',	'Update Discount',	'Discount'),
(	'create-discount',	'Create Discount',	'Discount'),

(	'view-employee',	'View Employee',	'Employee'),
(	'create-employee',	'Create Employee',	'Employee'),
(	'update-employee',	'Update Employee',	'Employee'),

(	'view-role',	'View Role',	'Employee'),
(	'update-role',	'Update Role',	'Employee'),
(	'create-role',	'Create Role',	'Employee'),

(	'view-importer',	'View Importer',	'Importer'),
(	'create-importer',	'Create Importer',	'Importer'),

(	'view-order',	'View Order',	'Order'),
(	'create-order',	'Create Order',	'Order'),
(	'update-order',	'Update Order',	'Order'),
(	'assign-agent',	'Assign Agent',	'Order'),

(	'create-return',	'Create Return',	'Order'),

(	'view-shipment',	'View Shipment',	'Order'),
(	'create-shipment',	'Create Shipment',	'Order'),
(	'update-shipment',	'Update Shipment',	'Order'),

(	'view-report',	'View Report',	'Report'),

(	'view-store',	'View Store',	'Store'),
(	'update-store',	'Update Store',	'Store'),
(	'create-store',	'Create Store',	'Store'),

(	'view-transaction',	'View Transaction',	'Transaction'),
(	'create-transaction',	'Create Transaction',	'Transaction'),

(	'view-user',	'View User',	'User'),
(	'update-user',	'Update User',	'User'),
(	'create-user',	'Create User',	'User');
