<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStockTransferTypeFieldInAdditionalOrderInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->string('stock_transfer_type')->after('tsm_employee_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->dropColumn('stock_transfer_type');
        });
    }
}
