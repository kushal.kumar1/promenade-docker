<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRateCreditNoteUpdateValueCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rate_credit_notes', function (Blueprint $table) {
            $table->decimal('subtotal')->change();
            $table->decimal('tax')->change();
            $table->decimal('total')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rate_credit_notes', function (Blueprint $table) {
            $table->float('subtotal')->change();
            $table->float('tax')->change();
            $table->float('total')->change();
        });
    }
}
