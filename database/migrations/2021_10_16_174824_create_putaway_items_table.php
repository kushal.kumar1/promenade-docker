<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePutawayItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('putaway_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('putaway_id');
            $table->unsignedInteger('product_id');
            $table->unsignedBigInteger('storage_id');
            $table->integer('quantity');
            $table->foreign('putaway_id')->references('id')->on('putaways');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('storage_id')->references('id')->on('storages');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('putaway_items');
    }
}
