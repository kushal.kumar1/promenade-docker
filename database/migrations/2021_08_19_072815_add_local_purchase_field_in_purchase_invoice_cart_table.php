<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalPurchaseFieldInPurchaseInvoiceCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->boolean('is_local_purchase')->default(0)->after('is_direct_store_purchase');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->dropColumn('is_local_purchase');
        });
    }
}
