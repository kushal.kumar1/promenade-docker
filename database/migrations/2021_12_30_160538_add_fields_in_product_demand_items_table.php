<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInProductDemandItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demand_items', function (Blueprint $table) {
            $table->unsignedInteger('product_id')->nullable()->after('group_id');
            $table->decimal('cost_price')->nullable()->after('product_id');
            $table->string('cost_price_by')
                ->nullable()
                ->after('cost_price')
                ->default('manual');
            $table->foreign('product_id')->references('id')
                ->on('products')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand_items', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropColumn(['product_id', 'cost_price', 'cost_price_by']);
        });
    }
}
