<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMorphRelationInFlatInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flat_inventories', function (Blueprint $table) {
            $table->string('source_type')->after('grn_item_id')->nullable();
            $table->unsignedBigInteger('source_id')->after('source_type')->nullable();
            $table->dropForeign(['grn_item_id']);
            $table->dropColumn('grn_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flat_inventories', function (Blueprint $table) {
            $table->dropColumn(['source_type', 'source_id']);
            $table->unsignedBigInteger('grn_item_id')->nullable();
            $table->foreign('grn_item_id')
                ->references('id')
                ->on('purchase_invoice_grn_items')
                ->onDelete('restrict')->onUpdate('cascade');
        });
    }
}
