<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInFlatInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flat_inventories', function (Blueprint $table) {
            $table->date('manufacturing_date')->nullable()->after('storage_id');
            $table->date('expiry_date')->nullable()->after('manufacturing_date');
            $table->decimal('unit_cost')->nullable()->after('expiry_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flat_inventories', function (Blueprint $table) {
            $table->dropColumn('manufacturing_date');
            $table->dropColumn('expiry_date');
            $table->dropColumn('unit_cost');
        });
    }
}
