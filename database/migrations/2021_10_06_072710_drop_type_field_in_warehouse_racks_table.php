<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTypeFieldInWarehouseRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_racks', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_racks', function (Blueprint $table) {
            $table->string('type')->after('reference')->nullable();
        });
        \Illuminate\Support\Facades\Artisan::call('warehouse-rack:set-type --down');
    }
}
