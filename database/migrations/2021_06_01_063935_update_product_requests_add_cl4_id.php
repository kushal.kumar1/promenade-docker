<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductRequestsAddCl4Id extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_requests', function (Blueprint $table) {
            $table->integer('cl4_id')->nullable()->unsigned()->after('group_id');
            $table->foreign('cl4_id')->references('id')->on('newcat_l4')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_requests', function (Blueprint $table) {
            $table->dropColumn('cl4_id');
        });
    }
}
