<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRackFieldInAuditItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_items', function (Blueprint $table) {
            $table->unsignedInteger('rack_id')->nullable()->after('is_settled');
            $table->foreign('rack_id')->references('id')
                ->on('warehouse_racks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_items', function (Blueprint $table) {
            $table->dropForeign(['rack_id']);
            $table->dropColumn('rack');
        });
    }
}
