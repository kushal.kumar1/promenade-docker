<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateCreditNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_credit_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_id');
            $table->integer('store_id')->unsigned();
            $table->string('type');
            $table->string('financial_year');
            $table->integer('number')->unsigned()->unique();
            $table->float('subtotal', 8, 6);
            $table->float('tax', 8, 6);
            $table->float('total', 8, 6);
            $table->string('status');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_credit_notes');
    }
}
