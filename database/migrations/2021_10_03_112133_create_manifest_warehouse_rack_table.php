<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManifestWarehouseRackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifest_warehouse_rack', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('manifest_id');
            $table->unsignedInteger('rack_id');
            $table->foreign('manifest_id')->references('id')
                ->on('manifests')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('rack_id')->references('id')
                ->on('warehouse_racks')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['manifest_id', 'rack_id']);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manifest_warehouse_rack');
    }
}
