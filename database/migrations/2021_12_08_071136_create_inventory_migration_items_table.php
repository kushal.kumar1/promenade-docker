<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryMigrationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_migration_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('inventory_migration_id');
            $table->unsignedInteger('product_id');
            $table->unsignedBigInteger('from_storage_id');
            $table->unsignedBigInteger('to_storage_id')->nullable();
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('quantity');
            $table->string('status');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            //F.K constraints
            $table->foreign('inventory_migration_id')
                ->references('id')
                ->on('inventory_migrations')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('from_storage_id')
                ->references('id')
                ->on('storages')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('to_storage_id')
                ->references('id')
                ->on('storages')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('employee_id')
                ->references('id')->on('employees')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_migration_items');
    }
}
