<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRateCreditNoteOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_credit_note_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('rate_credit_note_id')->unsigned();
            $table->bigInteger('pos_order_id')->unsigned();
            $table->foreign('rate_credit_note_id')->references('id')->on('rate_credit_notes')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('pos_order_id')->references('id')->on('pos_orders')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_credit_note_orders');
    }
}
