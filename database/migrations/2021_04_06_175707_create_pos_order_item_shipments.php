<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosOrderItemShipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_order_item_shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_item_id')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->double('unit_cost');
            $table->double('selling_price');
            $table->integer('shipment_id')->unsigned()->nullable();
            $table->string('batch_number');
            $table->foreign('order_item_id')->references('id')->on('pos_order_items')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('shipment_id')->references('id')->on('shipments');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pos_order_item_shipments');
    }
}
