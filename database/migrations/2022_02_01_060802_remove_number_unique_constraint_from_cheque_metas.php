<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNumberUniqueConstraintFromChequeMetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cheque_metas', function (Blueprint $table) {
            $table->dropForeign(['bank_id']);
            $table->dropUnique(['bank_id', 'cheque_number']);
            $table->foreign('bank_id')->on('banks')->references('id')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cheque_metas', function (Blueprint $table) {
            $table->dropForeign(['bank_id']);
            $table->unique(['bank_id', 'cheque_number']);
            $table->foreign('bank_id')->on('banks')->references('id')->onDelete('restrict')->onUpdate('cascade');
        });
    }
}
