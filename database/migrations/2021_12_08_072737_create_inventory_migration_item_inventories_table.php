<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryMigrationItemInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_migration_item_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('inventory_migration_item_id');
            $table->unsignedBigInteger('flat_inventory_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            //F.K constraints
            $table->foreign('inventory_migration_item_id','im_item_inventories_inventory_migration_item_id_foreign')
                ->references('id')->on('inventory_migration_items')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('flat_inventory_id','im_item_inventories_flat_inventory_id_foreign')
                ->references('id')->on('flat_inventories')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_migration_item_inventories');
    }
}
