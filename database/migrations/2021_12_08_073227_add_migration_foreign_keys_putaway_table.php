<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMigrationForeignKeysPutawayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('putaways', function (Blueprint $table) {
            $table->unsignedBigInteger('inventory_migration_id')
                ->nullable()->after('warehouse_id');
            $table->foreign('inventory_migration_id')
                ->references('id')->on('inventory_migrations')
                ->onDelete('cascade')->onUpdate('cascade');

        });

        Schema::table('putaway_items', function (Blueprint $table) {
            $table->unsignedBigInteger('inventory_migration_item_id')
                ->nullable()->after('putaway_id');
            $table->string('from_remarks', 100)->nullable()->after('quantity');
            $table->unsignedInteger('placed_quantity')->nullable()->after('quantity');
            $table->foreign('inventory_migration_item_id')
                ->references('id')->on('inventory_migration_items')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('putaways', function (Blueprint $table) {
            $table->dropForeign(['inventory_migration_id']);
            $table->dropColumn('inventory_migration_id');
        });

        Schema::table('putaway_items', function (Blueprint $table) {
            $table->dropForeign(['inventory_migration_item_id']);
            $table->dropColumn('inventory_migration_item_id');
            $table->dropColumn('placed_quantity');
        });
    }
}
