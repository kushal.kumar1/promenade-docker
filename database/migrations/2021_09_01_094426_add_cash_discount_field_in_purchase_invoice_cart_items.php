<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashDiscountFieldInPurchaseInvoiceCartItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_cart_items', function (Blueprint $table) {
            $table->float('cash_discount')->nullable()->after('discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_cart_items', function (Blueprint $table) {
            $table->dropColumn('cash_discount');
        });
    }
}
