<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnusedColumnsFromInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventories', function (Blueprint $table) {
            $table->dropColumn('base_cost');
            $table->dropColumn('trade_discount');
            $table->dropColumn('scheme_discount');
            $table->dropColumn('qps_discount');
            $table->dropColumn('cash_discount');
            $table->dropColumn('program_schemes');
            $table->dropColumn('other_discount');
            $table->dropColumn('taxes');
            $table->dropColumn('tax');
            $table->dropColumn('total_taxable');
            $table->dropColumn('post_tax_discount');
            $table->dropColumn('total_invoiced_cost');
            $table->dropColumn('post_invoice_schemes');
            $table->dropColumn('post_invoice_qps');
            $table->dropColumn('post_invoice_claim');
            $table->dropColumn('post_invoice_incentive');
            $table->dropColumn('post_invoice_other');
            $table->dropColumn('post_invoice_tot');
            $table->dropColumn('total_effective_cost');
            $table->dropColumn('reward_points_cashback');
            $table->dropColumn('delivery_date');
            $table->dropColumn('mfg_date');
            $table->dropUnique(['type','product_id','warehouse_id','batch_number']);
            DB::statement("delete from inventories where type=\"group_buy\"");
            $table->unique(['product_id','warehouse_id','batch_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventories', function (Blueprint $table) {
            //
        });
    }
}
