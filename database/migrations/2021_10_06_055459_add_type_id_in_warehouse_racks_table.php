<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeIdInWarehouseRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouse_racks', function (Blueprint $table) {
            $table->unsignedBigInteger('type_id')->after('type')->nullable();
            $table->foreign('type_id')->references('id')
                ->on('warehouse_rack_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        \Illuminate\Support\Facades\Artisan::call('warehouse-rack:set-type');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouse_racks', function (Blueprint $table) {
            $table->dropForeign(['type_id']);
            $table->dropColumn('type_id');
        });
    }
}
