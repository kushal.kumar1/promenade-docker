<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('cart_item_rule');
        DB::statement('truncate table cart_items');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::statement('truncate table carts');
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('deleted_at');
            $table->bigInteger('store_order_id')->unsigned()->unique()->nullable()->after('order_id');
            $table->foreign('store_order_id')->references('id')->on('store_orders')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('warehouse_id')->after('store_id');
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('cascade')->onUpdate('cascade');
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            //
        });
    }
}
