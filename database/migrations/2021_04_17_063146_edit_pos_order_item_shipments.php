<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPosOrderItemShipments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pos_order_item_shipments', function(Blueprint $table) {
            $table->decimal('unit_cost', 8,6)->nullable()->change();
            $table->string('batch_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pos_order_item_shipments', function(Blueprint $table) {
            $table->double('unit_cost')->change();
            $table->string('batch_number')->change();
        });
    }
}
