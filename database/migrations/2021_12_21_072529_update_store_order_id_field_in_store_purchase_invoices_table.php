<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStoreOrderIdFieldInStorePurchaseInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('store_purchase_invoices', function (Blueprint $table) {
            $table->dropForeign(['store_order_id']);
            $table->renameColumn('store_order_id', 'assigned_id');
            $table->string('assigned_type')->after('store_order_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('store_purchase_invoices', function (Blueprint $table) {
            $table->renameColumn('assigned_id', 'store_order_id');
            $table->foreign('store_order_id')->references('id')->on('store_orders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }
}
