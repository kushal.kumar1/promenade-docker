<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorIdInProductDemandItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demand_items', function (Blueprint $table) {
            $table->integer('vendor_id')->unsigned()->nullable()->after('reorder_quantity');
            $table->foreign('vendor_id')->on('vendors')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand_items', function (Blueprint $table) {
            $table->dropForeign('product_demand_items_vendor_id_foreign');
            $table->dropColumn('vendor_id');
        });
    }
}
