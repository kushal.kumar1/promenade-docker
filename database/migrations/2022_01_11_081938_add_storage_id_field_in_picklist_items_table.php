<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStorageIdFieldInPicklistItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('picklist_items', function (Blueprint $table) {
            $table->unsignedBigInteger('storage_id')->after('order_item_id')->nullable();
            $table->foreign('storage_id')->references('id')->on('storages')
                ->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('picklist_items', function (Blueprint $table) {
            $table->dropColumn('storage_id');
        });
    }
}
