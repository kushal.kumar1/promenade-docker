<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductDemandItemsRemovePoId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demand_items', function(Blueprint  $table) {
            $table->dropForeign('product_demand_items_purchase_order_id_foreign');
            $table->dropColumn('purchase_order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand_items', function(Blueprint  $table) {
            $table->integer('purchase_order_id')->unsigned()->after('vendor_id');
            $table->foreign('purchase_order_id')->on('purchase_orders')->references('id')->onDelete('cascade');
        });
    }
}
