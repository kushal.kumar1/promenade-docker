<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDemandItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_demand_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_demand_id');
            $table->unsignedInteger('product_id');
            $table->string('sku');
            $table->float('total_quantity_sold');
            $table->float('daily_avg_unit_sale');
            $table->float('available_stock');
            $table->float('stock_days');
            $table->unsignedInteger('purchase_order_id')->nullable();

            $table->foreign('product_demand_id')->references('id')->on('product_demands')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('sku')->references('sku')->on('product_variants')->onDelete('cascade');
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_demand_items');
    }
}
