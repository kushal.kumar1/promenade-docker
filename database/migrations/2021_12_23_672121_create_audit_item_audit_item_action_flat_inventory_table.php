<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditItemAuditItemActionFlatInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_item_audit_item_action_flat_inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('audit_item_id');
            $table->unsignedInteger('audit_item_action_id')->nullable(true);
            $table->unsignedBigInteger('flat_inventory_id');

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('audit_item_id', 'audit_inventory_pivot_audit_item_id_fk')
                ->on('audit_items')->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('audit_item_action_id', 'audit_inventory_pivot_audit_item_action_id_fk')
                ->on('audit_item_actions')->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('flat_inventory_id', 'audit_inventory_pivot_flat_inventory_id_fk')
                ->on('flat_inventories')->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_item_audit_item_action_flat_inventory');
    }
}
