<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditPosOrderItemTaxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pos_order_items', function (Blueprint $table) {
            $table->renameColumn('order_id', 'pos_order_id');
        });
        Schema::table('pos_order_item_metas', function (Blueprint $table) {
            $table->renameColumn('order_item_id', 'pos_order_item_id');
        });
        Schema::table('pos_order_item_taxes', function (Blueprint $table) {
            $table->renameColumn('order_item_id', 'pos_order_item_id');
        });
        Schema::table('pos_order_item_shipments', function (Blueprint $table) {
            $table->renameColumn('order_item_id', 'pos_order_item_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pos_order_items', function (Blueprint $table) {
            $table->renameColumn('pos_order_id', 'order_id');
        });
        Schema::table('pos_order_item_metas', function (Blueprint $table) {
            $table->renameColumn('pos_order_item_id', 'order_item_id');
        });
        Schema::table('pos_order_item_taxes', function (Blueprint $table) {
            $table->renameColumn('pos_order_item_id', 'order_item_id');
        });
        Schema::table('pos_order_item_shipments', function (Blueprint $table) {
            $table->renameColumn('pos_order_item_id', 'order_item_id');
        });
    }
}
