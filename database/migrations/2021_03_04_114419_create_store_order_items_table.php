<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('store_order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('sku');
            $table->string('variant');
            $table->decimal('mrp',11,6);
            $table->decimal('price',11,6);
            $table->integer('quantity');
            $table->decimal('subtotal',13,6);
            $table->decimal('discount',13,6);
            $table->decimal('tax',13,6);
            $table->decimal('total',13,6);
            $table->foreign('store_order_id')->references('id')->on('store_orders')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order_items');
    }
}
