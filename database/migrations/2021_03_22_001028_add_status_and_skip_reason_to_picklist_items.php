<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAndSkipReasonToPicklistItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('picklist_items', function (Blueprint $table) {
            $table->string('status')->after('order_item_id');
            $table->string('skip_reason')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('picklist_items', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('skip_reason');
        });
    }
}
