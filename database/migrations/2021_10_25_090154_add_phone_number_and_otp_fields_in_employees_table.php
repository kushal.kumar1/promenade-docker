<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneNumberAndOtpFieldsInEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->string('mobile')->unique()->after('email')->nullable();
            $table->unsignedInteger('otp')->after('password')->nullable();
            $table->dateTime('otp_expiry')->after('otp')->nullable();
            $table->unsignedInteger('otp_sent_count')->after('otp_expiry')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('mobile');
            $table->dropColumn('otp');
            $table->dropColumn('otp_expiry');
            $table->dropColumn('otp_sent_count');
        });
    }
}
