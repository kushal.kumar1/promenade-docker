<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountPlatformIdColumnInPurchaseInvoiceCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->unsignedBigInteger('discount_platform_id')->nullable()->after('platform_id');
        });

        Schema::table('purchase_invoices', function (Blueprint $table) {
            $table->unsignedBigInteger('discount_platform_id')->nullable()->after('platform_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->dropColumn('discount_platform_id');
        });

        Schema::table('purchase_invoices', function (Blueprint $table) {
            $table->dropColumn('discount_platform_id');
        });
    }
}
