<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorMarketerMap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_marketer_map', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('vendor_id');
            $table->unsignedInteger('marketer_id');
            $table->string('relation');
            $table->string('channel');
            $table->integer('mpv');
            $table->integer('lead_time');
            $table->integer('priority')->default(1);
            $table->string('status')->default('active');
            $table->timestamp('date_from')->useCurrent();
            $table->timestamp('date_to')->nullable();
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->foreign('marketer_id')->references('id')->on('marketers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_marketer_map');
    }
}
