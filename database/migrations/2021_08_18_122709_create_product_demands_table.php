<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_demands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->string('channel')->nullable(); // primary // secondary
            $table->integer('sale_days')->nullable();
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedInteger('marketer_id')->nullable();
            $table->unsignedInteger('vendor_id')->nullable();
            $table->unsignedInteger('cl1_id')->nullable();
            $table->unsignedInteger('cl2_id')->nullable();
            $table->unsignedInteger('cl3_id')->nullable();
            $table->unsignedInteger('cl4_id')->nullable();
            $table->integer('inventory_days')->nullable();
            $table->unsignedInteger('po_id')->nullable();
            $table->morphs('created_by');
            $table->string('status');

            $table->foreign('po_id')->on('purchase_orders')->references('id')->onDelete('cascade');
            $table->foreign('brand_id')->on('brands')->references('id')->onDelete('cascade');
            $table->foreign('marketer_id')->on('marketers')->references('id')->onDelete('cascade');
            $table->foreign('vendor_id')->on('vendors')->references('id')->onDelete('cascade');
            $table->foreign('cl1_id')->on('newcat_l1')->references('id')->onDelete('cascade');
            $table->foreign('cl2_id')->on('newcat_l2')->references('id')->onDelete('cascade');
            $table->foreign('cl3_id')->on('newcat_l3')->references('id')->onDelete('cascade');
            $table->foreign('cl4_id')->on('newcat_l4')->references('id')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_demands');
    }
}
