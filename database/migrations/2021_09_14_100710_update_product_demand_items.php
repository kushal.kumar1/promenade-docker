<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductDemandItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demand_items', function(Blueprint $table) {
            $table->dropForeign('product_demand_items_product_id_foreign');
            $table->dropColumn('product_id');
            $table->dropForeign('product_demand_items_sku_foreign');
            $table->dropColumn('sku');
            $table->integer('group_id')->after('product_demand_id')->unsigned();
            $table->foreign('group_id')->on('product_groups')->references('id')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand_items', function(Blueprint $table) {
            $table->dropForeign('product_demand_items_group_id_foreign');
            $table->dropColumn('group_id');

            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->on('products')->references('id')->onDelete('cascade');

            $table->string('sku');
            $table->foreign('sku')->on('product_variants')->references('sku')->onDelete('cascade');
        });
    }
}
