<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductDemandsTableAddReorderDaysColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demands', function(Blueprint $table) {
             $table->integer('reorder_days')->after('inventory_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demands', function(Blueprint $table) {
            $table->dropColumn('reorder_days');
        });
    }
}
