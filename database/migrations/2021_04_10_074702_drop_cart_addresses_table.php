<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCartAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        Schema::drop('cart_addresses');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        Schema::drop('temp_inventory');
        Schema::drop('temp_racks');
        Schema::drop('temp_txn');
        Schema::drop('agent_beat_days');
        Schema::drop('agent_targets');
        Schema::drop('ai_agent_targets');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
