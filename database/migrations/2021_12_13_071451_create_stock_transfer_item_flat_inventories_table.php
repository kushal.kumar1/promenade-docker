<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransferItemFlatInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfer_item_flat_inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stock_transfer_item_id');
            $table->unsignedBigInteger('flat_inventory_id');
            $table->foreign('stock_transfer_item_id', 'stock_transfer_item_id_foreign')->references('id')
                ->on('stock_transfer_items')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('flat_inventory_id', 'flat_inventory_id_foreign')->references('id')
                ->on('flat_inventories')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['stock_transfer_item_id', 'flat_inventory_id'],'stock_transfer_item_id_flat_inventory_id_unique');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transfer_item_flat_inventories');
    }
}
