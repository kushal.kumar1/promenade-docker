<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePriceFieldsInPurchaseInvoiceCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_cart_items', function (Blueprint $table) {
            $table->decimal('cost',15,6)->change();
            $table->decimal('discount',15,6)->change();
            $table->decimal('cash_discount',15,6)->change();
            $table->decimal('post_tax_discount',15,6)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_cart_items', function (Blueprint $table) {
            $table->decimal('cost',8,2)->change();
            $table->decimal('discount',8,2)->change();
            $table->decimal('cash_discount',8,2)->change();
            $table->decimal('post_tax_discount',8,2)->change();
        });
    }
}
