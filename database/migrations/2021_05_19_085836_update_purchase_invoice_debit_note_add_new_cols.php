<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchaseInvoiceDebitNoteAddNewCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_debit_notes', function(Blueprint $table) {
            $table->string('ref_id')->after('id');
            $table->string('financial_year')->after('ref_id');
            $table->dateTime('generated_at')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
