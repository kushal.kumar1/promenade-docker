<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSentQuantityInStockTransferItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stock_transfer_items', function (Blueprint $table) {
            $table->integer('sent_quantity')->default(0)->after('confirmed_quantity');
            $table->string('deficiency_reason')->nullable()->after('sent_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stock_transfer_items', function (Blueprint $table) {
            $table->dropColumn('sent_quantity');
            $table->dropColumn('deficiency_reason');
        });
    }
}
