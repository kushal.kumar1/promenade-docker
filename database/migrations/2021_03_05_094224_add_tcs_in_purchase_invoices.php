<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTcsInPurchaseInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoices', function (Blueprint $table) {
            $table->float('tcs')->nullable()->after('tax');
        });

        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->float('tcs')->nullable()->after('tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoices', function (Blueprint $table) {
            $table->dropColumn('tcs');
        });

        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->dropColumn('tcs');
        });
    }
}
