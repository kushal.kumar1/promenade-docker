<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAndParentIdFieldInWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            $table->string('type')->after('name')->nullable();
            $table->unsignedInteger('parent_id')->after('city_id')->nullable();
            $table->foreign('parent_id')
                ->on('warehouses')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
        $this->updateTypeOfWarehouse();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warehouses', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropForeign(['parent_id']);
            $table->dropColumn('parent_id');
        });
    }

    private function updateTypeOfWarehouse()
    {
        $warehouses = \Niyotail\Models\Warehouse::get();
        foreach ($warehouses as $warehouse){
            if($warehouse->id == 1){
                $warehouse->type = \Niyotail\Models\Warehouse::TYPE_VIRTUAL;
                $warehouse->parent_id = 2;
            }else{
                $warehouse->type = \Niyotail\Models\Warehouse::TYPE_PHYSICAL;
            }
            $warehouse->save();
        }
    }
}
