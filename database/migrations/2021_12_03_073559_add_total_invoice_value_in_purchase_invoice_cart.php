<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalInvoiceValueInPurchaseInvoiceCart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->decimal('total_invoice_value', 15, 2)->default(0.00)->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_cart', function (Blueprint $table) {
            $table->dropColumn('total_invoice_value');
        });
    }
}
