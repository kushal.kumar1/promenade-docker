<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductDemandsTableRemovePoIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demands', function(Blueprint $table) {
            $table->dropForeign('product_demands_po_id_foreign');
            $table->dropColumn('po_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demands', function(Blueprint $table) {
            $table->integer('po_id')->nullable()->after('inventory_days');
        });
    }
}
