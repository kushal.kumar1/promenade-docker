<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStorePurchaseInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_purchase_invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_invoice_id')->unsigned();
            $table->integer('store_id')->unsigned();
            $table->unsignedBigInteger('store_order_id')->nullable();
            $table->foreign('store_order_id')->references('id')->on('store_orders');
            $table->foreign('purchase_invoice_id')->references('id')->on('purchase_invoices');
            $table->foreign('store_id')->references('id')->on('stores');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_purchase_invoices');
    }
}
