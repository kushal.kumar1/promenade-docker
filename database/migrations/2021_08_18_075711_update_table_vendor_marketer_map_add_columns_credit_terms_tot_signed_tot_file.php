<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableVendorMarketerMapAddColumnsCreditTermsTotSignedTotFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_marketer_map', function(Blueprint  $table) {
            $table->dropColumn('mpv');
            $table->integer('mov')->after('channel');
            $table->integer('credit_term')->after('lead_time'); // 0 advance // 101 bill to bill
            $table->integer('cash_discount')->after('credit_term')->default(0); // 1-100 percentage
            $table->text('comments')->after('cash_discount')->nullable();
            $table->boolean('tot_signed')->after('credit_term')->default(0);
            $table->string('tot_file')->after('tot_signed')->nullable();
            $table->dropColumn('status');
        });

        Schema::table('vendor_marketer_map', function(Blueprint  $table) {
            $table->boolean('status')->default(0)->after('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_marketer_map', function(Blueprint  $table) {
            $table->integer('mpv')->after('channel');
            $table->dropColumn('mov');
            $table->dropColumn('credit_term');
            $table->dropColumn('cash_discount');
            $table->dropColumn('comments');
            $table->dropColumn('tot_signed');
            $table->dropColumn('tot_file');
        });
    }
}
