<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditItemMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_item_metas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('audit_item_id');
            $table->unsignedInteger('quantity');
            $table->smallInteger('condition');
            $table->unsignedSmallInteger('reason_id')->nullable(true);
            $table->string('remarks')->nullable(true);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('audit_item_id')->references('id')->on('audit_items')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reason_id')->references('id')->on('reasons')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_item_metas');
    }
}
