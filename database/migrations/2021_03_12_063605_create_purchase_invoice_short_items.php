<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseInvoiceShortItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_invoice_short_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_invoice_id')->unsigned();
            $table->string('name');
            $table->integer('quantity');
            $table->string('hsn');
            $table->integer('tax_class')->unsigned();
            $table->float('mrp')->nullable();
            $table->float('discount')->default(0);
            $table->float('invoice_cost');
            $table->float('post_tax_discount')->default(0);
            $table->float('effective_cost');
            $table->foreign('purchase_invoice_id')->references('id')->on('purchase_invoices')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('tax_class')->references('id')->on('tax_classes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_invoice_short_items');
    }
}
