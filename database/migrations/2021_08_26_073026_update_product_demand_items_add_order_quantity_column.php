<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductDemandItemsAddOrderQuantityColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demand_items', function(Blueprint  $table) {
            $table->integer('reorder_quantity')->after('stock_days');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand_items', function(Blueprint  $table) {
            $table->dropColumn('reorder_quantity');
        });
    }
}
