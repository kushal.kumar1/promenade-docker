<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModificationRequestsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('purchase_invoice_modification_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('purchase_invoice_id');
            $table->unsignedInteger('vendor_id')->nullable(true);
            $table->unsignedInteger('vendor_address_id')->nullable(true);
            $table->timestamp('date')->nullable(true);
            $table->string('image')->nullable(true);
            $table->unsignedBigInteger('platform_id')->nullable(true);
            $table->string('eway_number')->nullable(true);
            $table->date('forecast_payment_date')->nullable(true);
            $table->string('delete_reason')->nullable(true);
            $table->tinyInteger('action');
            $table->unsignedInteger('employee_id');
            $table->smallInteger('status');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('purchase_invoice_id', 'pi_modification_requests_purchase_invoice_id_foreign')
                ->references('id')
                ->on('purchase_invoices')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('vendor_id', 'pi_modification_requests_vendor_id_foreign')
                ->references('id')
                ->on('vendors')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('vendor_address_id', 'pi_modification_requests_vendor_address_id_foreign')
                ->references('id')
                ->on('vendor_addresses')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('platform_id', 'pi_modification_requests_platform_id_foreign')
                ->references('id')
                ->on('purchase_platforms')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('employee_id', 'pi_modification_requests_employee_id_foreign')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->softDeletes();
        });

        Schema::create('purchase_invoice_item_modification_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('purchase_invoice_item_id');
            $table->unsignedInteger('product_id')->nullable(true);
            $table->unsignedInteger('quantity')->nullable(true);
            $table->decimal('base_cost', 15, 6)->nullable(true);
            $table->string('scheme_discount', 191)->nullable(true);
            $table->string('other_discount', 191)->nullable(true);
            $table->string('post_tax_discount', 191)->nullable(true);
            $table->string('delete_reason')->nullable(true);
            $table->tinyInteger('action');
            $table->unsignedInteger('employee_id');
            $table->smallInteger('status');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('purchase_invoice_item_id', 'pii_modification_requests_purchase_invoice_item_id_foreign')
                ->references('id')
                ->on('purchase_invoice_items')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('product_id', 'pii_modification_requests_product_id_foreign')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('employee_id', 'pii_modification_requests_employee_id_foreign')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->softDeletes();
        });

        Schema::create('transaction_deletion_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_id', 191);
            $table->string('delete_reason');
            $table->unsignedInteger('employee_id');
            $table->smallInteger('status');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('transaction_id', 't_modification_requests_transaction_id_foreign')
                ->references('transaction_id')
                ->on('transactions')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('employee_id', 't_modification_requests_employee_id_foreign')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_invoice_modification_requests');
        Schema::dropIfExists('purchase_invoice_item_modification_requests');
        Schema::dropIfExists('transaction_deletion_requests');
    }
}