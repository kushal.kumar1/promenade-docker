<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdditionalOrderInfosTableForCustomerInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->json('customer_info')->nullable()->after('tsm_employee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->dropColumn('customer_info');
        });
    }
}
