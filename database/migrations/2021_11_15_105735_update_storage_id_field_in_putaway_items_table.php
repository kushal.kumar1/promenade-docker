<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStorageIdFieldInPutawayItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('putaway_items', function (Blueprint $table) {
            $table->unsignedBigInteger('storage_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('putaway_items', function (Blueprint $table) {
            $table->unsignedBigInteger('storage_id')->nullable(false)->change();
        });
    }
}
