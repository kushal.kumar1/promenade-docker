<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePurchaseInvoiceDebitNoteItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_invoice_debit_note_items', function(Blueprint $table) {
            $table->string('source_type')->after('debit_note_id');
            $table->integer('source_id')->after('source_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_invoice_debit_note_items', function(Blueprint $table) {
            $table->dropColumn('source_type');
            $table->dropColumn('source_id');
        });
    }
}
