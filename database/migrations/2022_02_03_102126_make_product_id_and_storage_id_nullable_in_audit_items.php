<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeProductIdAndStorageIdNullableInAuditItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_items', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['storage_id']);

            $table->unsignedInteger('product_id')->nullable(true)->change();
            $table->unsignedBigInteger('storage_id')->nullable(true)->change();

            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('storage_id')->references('id')->on('storages')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_items', function (Blueprint $table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['storage_id']);
//            $table->foreign()->

            $table->unsignedInteger('product_id')->nullable(false)->change();
            $table->unsignedBigInteger('storage_id')->nullable(false)->change();

            $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('storage_id')->references('id')->on('storages')->onUpdate('cascade')->onDelete('cascade');
        });
    }
}
