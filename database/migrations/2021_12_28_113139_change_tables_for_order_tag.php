<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTablesForOrderTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->string('tag')->nullable()->after('source');
            $table->json('additional_info')->nullable()->after('source');
        });

        Schema::table('store_orders', function (Blueprint $table) {
            $table->string('tag')->nullable()->after('source');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('type')->index()->default('retail')->after('source');
            $table->index('status');
        });

        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->string('tsm_employee_id')->nullable()->after('order_id');
            $table->string('tsm_number')->nullable()->after('order_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('tag');
        });

        Schema::table('store_orders', function (Blueprint $table) {
            $table->dropColumn('tag');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropIndex(['type']);
            $table->dropIndex(['status']);
            $table->dropColumn('type');
        });

        Schema::table('additional_order_infos', function (Blueprint $table) {
            $table->dropColumn('tsm_employee_id');
            $table->dropColumn('tsm_number');
        });
    }
}
