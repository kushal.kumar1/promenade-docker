//Show all users
$('.users-select').select2({
    placeholder: "Search & Add Users",
    minimumInputLength: 2,
    ajax: {
        url: '/search/users',
        dataType: 'json',
        processResults: function(data) {
            data = $.map(data, function(obj) {
                obj.id = obj.id;
                obj.text = obj.first_name + " " +obj.last_name;
                return obj;
            });
            return {
                results: data
            };
        }
    }
});

// function storeTemplate(store) {
//     if (!store.id) {
//         return store.text;
//     }
//     let html = '<div class="padding-10">';
//     html += `<h5 class="h6 bold">${store.name} (${store.id})</h5>`;
//     html += `<h6 class="margin-t-5">${store.address}</h6>`;
//     html += `<h6 class="small margin-t-5">Beat - ${store.beat.name}</h6>`;
//     html += '</div>';
//
//     return $(html);
// };
//
// function storeSelection(store){
//     return store.name + ", " + store.address;
// }

//Show all Stores
// $('.stores-select').select2({
//     minimumInputLength: 2,
//     ajax: {
//         url: '/search/stores',
//         dataType: 'json',
//         processResults: (data) => { return { results: data }; }
//     },
//     templateResult: storeTemplate,
//     templateSelection: storeSelection
// });


//Add user to store
$('.users-select').on('select2:select', function() {
    $('#form-users').submit();
});

//Remove User from store
$('.remove-user').on('click', function(){
  $(this).closest('.form-remove-user').submit();
})

// $('.store-disable, .store-enable').on('click', function () {
//     var $form = $(this).closest('form');
//     var title = "Disable Store";
//     var description = "Are you sure you want to disable this store?";
//     if ($(this).hasClass('store-enable')) {
//         title = "Enable Store";
//         description = "Are you sure you want to enable this store?";
//     }
//     dialog(title, description, function () {
//         $form.submit();
//     });
// });
