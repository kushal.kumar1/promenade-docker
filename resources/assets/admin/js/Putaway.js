class Putaway {
    constructor() {
        this.init();
    }

    init() {
        $('.product-search-js').select2({
            placeholder: "Search Products",
            minimumInputLength: 8,
            ajax: {
                url: function () {
                    return '/putaway/search-product';
                },
                dataType: 'json',
                processResults: function (data) {
                    data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    }
}
export default new Putaway();
