function dialog(title, message, success, reject) {
    $dialog = $('#dialog');
    $dialog.find('.title').text(title);
    $dialog.find('.message').text(message);
    $buttonSuccess = $dialog.find('.button-success');
    $buttonReject = $dialog.find('.button-reject');
    $buttonClose = $dialog.find('.close');
    $buttonSuccess.on('click', function() {
        $buttonSuccess.off('click');
        $buttonReject.off('click');
        Modal.hide($dialog);
        success();
    });
    $buttonReject.on('click', function() {
        exitDialog($buttonSuccess, $buttonReject, reject);
    });
    $buttonClose.on('click', function() {
        exitDialog($buttonSuccess, $buttonReject, reject);
    });
    Modal.show($dialog);
}

function exitDialog($buttonSuccess, $buttonReject, reject)
{
    $buttonSuccess.off('click');
    $buttonReject.off('click');
    Modal.hide($dialog);
    if (reject !== undefined) {
        reject();
    }
}
