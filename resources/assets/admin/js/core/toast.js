$(document).on('click', '.toast-dismiss', function () {
    $(this).closest('.toast').remove();
});

var Toast = function (message, type = 'info') {

    const $toastHtml = $('.templates>.toast').clone();
    $toastHtml.find('.toast-message').text(message);
    var defaultTimeout = -1;

    switch (type) {
        case 'success': {
            $toastHtml.find('.toast-heading').text('SUCCESS');
            $toastHtml.addClass('success');
            $toastHtml.find('.toast-icon').addClass('fa').addClass(' fa-check-circle')
            defaultTimeout = 6000;
            break;
        }
        case 'error': {
            $toastHtml.find('.toast-heading').text('ERROR');
            $toastHtml.addClass('error');
            $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-times-circle')
            break;
        }
        case 'warn': {
            $toastHtml.find('.toast-heading').text('WARNING');
            $toastHtml.addClass('warn');
            $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-exclamation-circle')
            defaultTimeout = 10000;
            break;
        }
        case 'info':
        default: {
            $toastHtml.find('.toast-heading').text('INFO');
            $toastHtml.addClass('info');
            $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-info-circle')
            defaultTimeout = 6000;
            break;
        }
    }

    this.show = function (timeout = null) {
        var self = this;
        $('#notification-container').append($toastHtml);
        $toastHtml.show();

        if (timeout == null) {
            timeout = defaultTimeout;
        }

        if (timeout > 0) {
            setTimeout(function () {
                self.hide();
            }, timeout);
        }
    }
    this.hide = function () {
        $toastHtml.remove();
    }
};