let isSidebarVisible = localStorage.getItem("isSidebarVisible");
const sidebar = $(".sidebar");

const toggleSidebar = () => {
  if (isSidebarVisible == "true") {
    sidebar.addClass("active");
    $(".header").addClass("active");
    $(".container-wrapper").addClass("active");
    localStorage.setItem("isSidebarVisible", "true");
  } else {
    sidebar.removeClass("active");
    $(".header").removeClass("active");
    $(".container-wrapper").removeClass("active");
    localStorage.setItem("isSidebarVisible", "false");
  }
};

if (isSidebarVisible) {
  toggleSidebar();
}

$(".sidebar-close").on("click", function(e) {
  e.preventDefault();
  isSidebarVisible = "false";
  toggleSidebar();
});

$(".sidebar-open").on("click", function(e) {
  e.preventDefault();
  if (isSidebarVisible == "true") {
    isSidebarVisible = "false";
  } else {
    isSidebarVisible = "true";
  }
  toggleSidebar();
});

$(".has-submenu").on("click", function() {
  var $submenu = $(this).next(".submenu");
  var isActive = $submenu.hasClass("active");
  $(this)
    .closest(".menu")
    .find(".submenu")
    .removeClass("active");
  if (!isActive) {
    $submenu.addClass("active");
  }
});

//Image Upload
$(".upload").on("click", function(e) {
  e.preventDefault();
  var target = $(this).data("target");
  $(target).trigger("click");
});

$("#upload-input").change(function(e) {
  $("#form-upload").submit();
});

$(".test-dialog").on("click", function() {
  dialog(
    "Reset Settings",
    "This will restore all system settings to defaults. Are you sure that you want to proceed?",
    function() {
      alert("confirmed");
    }
  );
});

$(".form").each(function() {
  var $form = $(this);
  if ($form.data("actions") != undefined) {
    var $actions = $($form.data("actions"));
    var initialData = $(this).serialize();
    $form.on("keyup change", ":input", function() {
      $actions.addClass("hidden");
      if ($form.serialize() != initialData) {
        $actions.removeClass("hidden");
      }
    });
  }
});

$(".btn-discard").on("click", function() {
  dialog(
    "Discard all changes",
    "Any unsaved changes will be lost. Are you sure that you want to proceed?",
    function() {
      location.reload();
    }
  );
});

$(".mobile-country-picker").intlTelInput({
  autoPlaceholder: false,
  initialCountry: "IN"
});
