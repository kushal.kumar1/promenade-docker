//Show all badges
$('.badges-select').select2({
    minimumInputLength: 2,
    ajax: {
        url: '/search/badges',
        dataType: 'json',
        processResults: function(data) {
            data = $.map(data, function(obj) {
                obj.id = obj.id;
                obj.text = obj.name;
                return obj;
            });
            return {
                results: data
            };
        }
    }
});

//Save Customer Badges
$('.badges-select').on('select2:select select2:unselect', function() {
    $('#form-badges').submit();
});

//Enable/Disable Customer
$('.user-disable, .user-enable').on('click', function() {
    var $form = $(this).closest('form');
    var title = "Disable Account";
    var description = "Are you sure you want to disable this account?";
    if ($(this).hasClass('user-enable')) {
        title = "Enable Account";
        description = "Are you sure you want to enable this account?";
    }
    dialog(title, description,
        function() {
            $form.submit();
        }
    );
});

//Save Customer Notes
var userNoteTimeout;
$('#user-note').on('keyup', function() {
    var $form = $(this).closest('form');
    if (userNoteTimeout) {
        clearTimeout(userNoteTimeout);
    }
    userNoteTimeout = setTimeout(function() {
        $form.submit();
    }, 500);
});

$(document).on('click', '[data-close="modal"]', function() {
    Modal.hide($('.modal'));
});
