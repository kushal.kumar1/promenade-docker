class Locator {
    constructor() {
        $('#search-input').on('input', (e) => this.onQueryChanged(e.target.value));
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition((location) => this.onLocationUpdated(location), (error) => this.onLocationError(error));
        }
    }

    onLocationUpdated(location) {
        this.latitude = location.coords.latitude;
        this.longitude = location.coords.longitude;
    }

    onLocationError(error) {
        let message = "Location error occurred";
        switch (error.code) {
            case error.PERMISSION_DENIED:
                message = "User denied the request for Geolocation."
                break;
            case error.POSITION_UNAVAILABLE:
                message = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                message = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                message = "An unknown error occurred."
                break;
        }
        alert(message);
    }

    onQueryChanged(term) {
        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(() => {
            if (term) {
                if (this.searchRequest) {
                    this.searchRequest.abort();
                }
                this.searchRequest = $.ajax('/locator/stores', {
                    method: 'GET',
                    data: {
                        term: term,
                        latitude: this.latitude,
                        longitude: this.longitude
                    }
                });
                this.searchRequest.done((stores) => $('.stores').html(stores));
                // this.searchRequest.fail((error) => this.onError(error));
            }
        }, 100);
    }
}

export default new Locator();