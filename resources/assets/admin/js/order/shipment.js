var Shipment = {
    init: function init() {
        $('.shipment .shipment-cancel').on('click', this.cancel);
        $('.shipment .shipment-deliver').on('click', this.deliver);
    },
    cancel: function cancel() {
        var id = $(this).data('shipment-id');
        var url = $(this).data('url');
        dialog("Cancel Shipment", "Are you sure you want to cancel this shipment? This action cannot be reversed.", function () {
            var request = $.ajax({
                url: url,
                data: { id: id },
                type: 'POST'
            });
            request.done(function (data) {
                location.reload();
            });
            request.fail(function () {});
        });
    },

    // deliver: function cancel() {
    //     var id = $(this).data('shipment-id');
    //     var url = $(this).data('url');
    //     dialog("Mark Shipment as Delivered", "Are you sure you want to deliver this shipment? This action cannot be reversed.", function () {
    //         var request = $.ajax({
    //             url: url,
    //             data: { id: id },
    //             type: 'POST'
    //         });
    //         request.done(function (data) {
    //             location.reload();
    //         });
    //         request.fail(function () {});
    //     });
    // },
};

Shipment.init();
