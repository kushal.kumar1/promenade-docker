class Manifest {
    constructor() {
        $('.shipment').on('change','input[name=boxes]', (e) => this.updateBoxQuantity(e));
        $('input[name=select_all]').on('click', (e) => this.toggleSelection(e));
        this.initAgentSearch();
    }

    toggleSelection(e){
        const $target = $(e.currentTarget);
        if ($target.is(":checked")) {
          $('.item-checkbox').prop('checked', true);
      } else if ($target.is(":not(:checked)")) {
          $('.item-checkbox').prop('checked', false);
        }
        $('.item-checkbox').trigger('change');
    }

    updateBoxQuantity(e) {
        const $input = $(e.currentTarget);
        $input.closest('.shipment').find('.input-loader').removeClass('hidden');
        const id = $input.closest('.shipment').data('shipment_id');
        const boxes = $input.val();
        const Request = $.ajax('/shipments/update-boxes', {
            method: 'POST',
            data: {
                id: id,
                boxes: boxes,
            }
        });

        Request.done((data) => {$('.input-loader').addClass('hidden')});
        Request.fail((error) => this.onError(error));
    }

    initAgentSearch() {
        $('#agents-search').select2({
          placeholder: "Search Agent by name or code",
          minimumInputLength: 2,
          ajax: {
            url: function(params) {
              return '/search/agents?type=delivery';
            },
            dataType: 'json',
            processResults: function(data) {
              data = $.map(data, function(obj) {
                obj.id = obj.id;
                obj.text = obj.name;
                return obj;
              });
              return {
                results: data
              };
            }
          }
        });
    }

    onError(error){
        $('.input-loader').addClass('hidden');
        let message = error.responseJSON.message;
        if (message == undefined) {
            message = "An unexpected error has occurred";
        }
        const toast = new Toast(message);
        toast.show();
    }
}

export default new Manifest();
