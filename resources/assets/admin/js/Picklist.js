class Picklist {
    constructor() {
        this.picklistId =  $('#item-search').data('id');
        $('#item-search').on('input', (e) => this.onQueryChanged(e));
        $('#item-suggestions').on('click','.js-btn-submit', (e) => this.processItem(e));
        $('#picked-items').on('click','.btn-remove', (e) => this.removeItem(e));
        $('#item-suggestions').on('click', '.js-add-skip-row', (e) => this.addSkipRow(e));
        $('#item-suggestions').on('click', '.js-delete-skip-row', (e) => this.deleteSkipRow(e));
    }

    onQueryChanged(e) {
        $('.suggestions-overlay').removeClass('hidden');
        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(() => {
            const barcode = e.target.value;
            if (barcode.length >= 8) {
                if (this.searchRequest) {
                    this.searchRequest.abort();
                }
                this.searchRequest = $.ajax('/picklists/items/search', {
                    method: 'GET',
                    data: { barcode: barcode, picklist_id: this.picklistId }
                });
                this.searchRequest.done((items) => $('#item-suggestions').html(items));
                this.searchRequest.fail((error) => this.onError(error));
            }
        }, 100);
    }

    processItem(e) {
        $('.picked-items-overlay').removeClass('hidden');
        const form = $(e.currentTarget).closest('form');
        this.pickRequest = $.ajax('/picklists/items/add', {
            method: 'POST',
            data: form.serialize()
        });
        this.pickRequest.done((items) => {
            $('#picked-items').html(items);
            $('#item-search').val("");
            form.closest('.item').remove();
            location.reload();
        });
        this.pickRequest.fail((error) => this.onError(error));
    }

    removeItem(e) {
        $('.picked-items-overlay').removeClass('hidden');
        const $target = $(e.currentTarget).closest('.item');
        this.removeRequest = $.ajax('/picklists/items/remove', {
            method: 'POST',
            data: {'product_id': $target.data('product_id'), picklist_id: this.picklistId}
        });
        this.removeRequest.done((items) => {
            $('#picked-items').html(items);
            location.reload();
        });
        this.removeRequest.fail((error) => this.onError(error));
    }

    addSkipRow(e) {
        const $target = $(e.currentTarget).closest('.item');
        const template = new Template($('.templates .skip-row'));
        const data = {
            index: $target.find('.skip-row').length
        }
        template.setData(data);
        template.appendTo($target.find('#js-skip-rows'));
    }

    deleteSkipRow(e) {
        const $target = $(e.currentTarget).closest('.skip-row');
        $target.remove();
    }

    onError(error){
        $('.picked-items-overlay').addClass('hidden');
        let message = error.responseJSON.message;
        if (message == undefined) {
            message = "An unexpected error has occurred";
        }
        const toast = new Toast(message);
        toast.show();
    }
}

export default new Picklist();
