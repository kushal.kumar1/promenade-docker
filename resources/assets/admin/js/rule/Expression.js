import Selector from "./Selector";

export default class Expression {
    constructor(condition, name, data, index) {
        this.condition = condition;
        this.name = name;
        this.data = data;
        this.index = index;
        this.initView();
        this.attachListeners();
    }

    initView() {
        const data = {};
        data.expressionIndex = this.index;
        data.conditionIndex = this.condition.index;
        const template = new Template($('.templates .rule-expression'));
        template.setData(data);
        template.appendTo(this.condition.$node.find('.expressions'));
        this.$node = template.getJqueryObject();
        this.polishView();
    }

    remove() {
        this.$node.remove();
        this.polishView();
    }

    polishView() {
        this.condition.$node.find('.rule-expression .remove-wrapper').removeClass('hidden');
        if (this.condition.$node.find('.rule-expression').length < 2) {
            this.condition.$node.find('.rule-expression .remove-wrapper').addClass('hidden');
        }
    }

    attachListeners() {
        const $select2Name = this.$node.find('.select2-expression-name');
        $select2Name.select2({ data: this.getAttributes(this.condition.rule.data.type) });
        if (this.name) {
            $select2Name.val(this.name).change();
        }
        $select2Name.on('select2:select', (e) => this.onAttributeChanged($select2Name.select2('data')[0]));
        $select2Name.trigger('select2:select');
        this.$node.find('.btn-remove-expression').on('click', () => this.remove());
    }

    onAttributeChanged(attribute) {
        const data = {
            expressionIndex: this.index,
            conditionIndex: this.condition.index,
            value: 0,
            max_value: null
        };
        if (attribute.type == 'number' && this.data.length > 0) {
            data.value = this.data[0].value;
            data.max_value = this.data[0].max_value;
        }
        const template = new Template($('.templates .selector-' + attribute.type));
        template.setData(data);
        template.replaceIn(this.$node.find('.values-wrapper'));
        this.$valuesNode = template.getJqueryObject();
        if (attribute.type == "select2") {
            const selector = new Selector(this.$valuesNode.find('select'), attribute.id);
            selector.setSelected(attribute.id, this.data);
        }
    }

    getAttributes(type) {
        return [
            {
                "text": "Store",
                "children": [
                    {
                        "id": "beat",
                        "text": "Beat",
                        "type": "select2",
                        "disabled": ['order', 'product', 'pricing'].indexOf(type) == -1
                    }
                ]
            },
            {
                "text": "Product",
                "children": [
                    {
                        "id": "variant",
                        "text": "Variant",
                        "type": "select2",
                        "disabled": ['product', 'pricing'].indexOf(type) == -1
                    },
                    {
                        "id": "collection",
                        "text": "Collection",
                        "type": "select2",
                        "disabled": ['product'].indexOf(type) == -1
                    },
                    {
                        "id": "price",
                        "text": "Price",
                        "type": "number",
                        "disabled": ['product'].indexOf(type) == -1
                    },
                    {
                        "id": "quantity",
                        "text": "Quantity",
                        "type": "number",
                        "disabled": ['product'].indexOf(type) == -1
                    }
                ]
            },
            {
                "text": "Brand",
                "children": [
                    {
                        "id": "brand",
                        "text": "Brand",
                        "type": "select2",
                        "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                    },
                    {
                        "id": "marketer",
                        "text": "Marketer",
                        "type": "select2",
                        "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                    },
                    {
                        "id": "brand_quantity",
                        "text": "Brand Quantity",
                        "type": "number",
                        "disabled": ['order'].indexOf(type) == -1
                    },
                    {
                        "id": "brand_amount",
                        "text": "Brand Amount",
                        "type": "number",
                        "disabled": ['order'].indexOf(type) == -1
                    }
                ]
            },
            {
                "text": "Order",
                "children": [
                    {
                        "id": "total_amount",
                        "text": "Total Amount",
                        "type": "number",
                        "disabled": ['order'].indexOf(type) == -1
                    },
                    {
                        "id": "total_quantity",
                        "text": "Total Quantity",
                        "type": "number",
                        "disabled": ['order'].indexOf(type) == -1
                    }
                ]
            },
            {
                "text": "Miscellaneous",
                "children": [
                    {
                        "id": "medium",
                        "text": "Medium",
                        "type": "select2",
                        "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                    }
                ]
            },
        ];
    }
}