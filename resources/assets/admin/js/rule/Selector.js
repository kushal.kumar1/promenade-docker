export default class Selector {
    constructor($node, type) {
        this.$node = $node;
        switch (type) {
            case 'variant':
                this.attachVariantSelect2();
                break;

            case 'beat':
                this.attachBeatSelect2();
                break;

            case 'collection':
                this.attachCollectionSelect2();
                break;

            case 'brand':
                this.attachBrandSelect2();
                break;

            case 'marketer':
                this.attachMarketerSelect2();
                break;

            case 'medium':
                this.attachMediumSelect2();
                break;
        }
    }

    getMediumData() {
        return [
            {
                id: 'retailer',
                text: 'Retailer App'
            },
            {
                id: 'agent',
                text: 'Agent App'
            },
            {
                id: 'admin',
                text: 'Backend'
            }
        ];
    }

    attachVariantSelect2() {
        this.$node.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/variants',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: (obj) => this.defaultTemplate(obj.id, obj.text || `${obj.product.name} [${obj.value}]`),
            templateSelection: (obj) => obj.text || `${obj.product.name} [${obj.value}]`
        });
    }

    attachBeatSelect2() {
        this.$node.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/beats',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: (obj) => this.defaultTemplate(obj.id, obj.text || obj.name),
            templateSelection: (obj) => obj.text || obj.name
        });
    }

    attachCollectionSelect2() {
        this.$node.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/collections',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: (obj) => this.defaultTemplate(obj.id, obj.text || obj.name),
            templateSelection: (obj) => obj.text || obj.name
        });
    }

    attachBrandSelect2() {
        this.$node.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/brands',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: (obj) => this.defaultTemplate(obj.id, obj.text || obj.name),
            templateSelection: (obj) => obj.text || obj.name
        });
    }

    attachMarketerSelect2() {
        this.$node.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/marketers',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: (obj) => this.defaultTemplate(obj.id, obj.text || obj.name),
            templateSelection: (obj) => obj.text || obj.name
        });
    }

    attachMediumSelect2() {
        let mediums = this.getMediumData();
        this.$node.select2({
            data: mediums
        });
    }

    defaultTemplate(id, text) {
        if (!id) {
            return text;
        }
        let html = '<div class="padding-10">';
        html += `<h5>${text}</h5>`;
        html += '</div>';

        return $(html);
    };

    setSelected(type, values) {
        if (values.length > 0) {
            const data = [];
            values.forEach(value => {
                let optionData = {};
                if (value.attribute) {
                    optionData = { id: value.attribute.id, text: value.attribute.name, name: value.attribute.name };
                }
                switch (type) {
                    case 'variant':
                        optionData.text = `${value.attribute.product.name} [${value.attribute.value}]`;
                        optionData.name = value.attribute.product.name;
                        optionData.product = value.attribute.product;
                        optionData.value = value.attribute.value;
                        break;

                    case 'medium':
                        optionData = this.getMediumData().find((medium) => medium.id === value.value);
                        break;
                }
                data.push(optionData);
                if (type != 'medium') {
                    this.appendOption(optionData);
                }
            });
            if (type == 'medium') {
                this.$node.val(data.map(medium => medium.id)).trigger('change');
            }
            this.$node.trigger({ type: 'select2:select', params: { data: data } });
        }
    }

    appendOption(value) {
        const option = new Option(value.text, value.id, true, true);
        this.$node.append(option).trigger('change');
    }
}
