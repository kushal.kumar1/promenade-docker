import Condition from './Condition';
import Action from './Action';

class Rule {

    constructor() {
        this.data = ruleData;
        this.conditions = [];
        this.lastPosition = 0;
        this.action = new Action(this);
        this.attachListeners();
        this.initView();
    }

    initView() {
        $('#card-basic').find(`input[name=type][value=${this.data.type}]`)
            .prop('checked', true);
        this.action.onRuleTypeChanged();
        if (this.data.conditions) {
            this.data.conditions.forEach(conditionData => {
                this.addCondition(conditionData);
            });
        } else {
            this.addCondition({});
        }
    }

    attachListeners() {
        $('#btn-condition').on('click', () => this.addCondition({}));
        $('#card-basic').find(`input[name=type]`).on('change', (e) => {
            if (e.target.checked) {
                this.onTypeChanged(e.target.value);
            }
        });
    }

    onTypeChanged(type) {
        this.data.type = type;
        this.conditions.forEach(condition => { condition.remove() });
        this.conditions = [];
        this.addCondition({});
        this.action.onRuleTypeChanged();
    }

    addCondition(data) {
        const condition = new Condition(this, data, this.lastPosition);
        this.conditions.push(condition);
        this.lastPosition++;
    }
}

export default new Rule();