import Selector from "./Selector";

export default class Action {
    constructor(rule) {
        this.rule = rule;
        this.attachListeners();
        let offer = ":first";
        if (this.rule.data.offer) {
            offer = `[value=${this.rule.data.offer}]`;
        }
        $(`#action-card input[name=offer]${offer}`).trigger('click');
    }

    attachListeners() {
        $('#action-card input[name=offer]').on('change', (e) => {
            if (e.target.checked) {
                this.onOfferChanged(e.target.value);
            }
        });
    }

    onRuleTypeChanged() {
        const type = this.rule.data.type;
        $('#limits-wrapper').addClass('hidden');
        $(`#action-card input[name=offer]`).prop('disabled', true);
        $('#action-card .offer').addClass('hidden');

        $(`#action-card .offer-${type} input[name=offer]`).prop('disabled', false);
        $(`#action-card .offer-${type}`).removeClass('hidden');

        if (type != 'pricing') {
            $('#limits-wrapper').removeClass('hidden');
        }
    }

    onOfferChanged(type) {
        const ruleData = this.rule.data;
        const data = {
            source_quantity: ruleData.source_quantity || 1,
            target_quantity: ruleData.target_quantity || 1,
            value: ruleData.value || 0
        };
        const actionName = type == "free" ? "free" : "price";
        const template = new Template($(`.templates .action-${actionName}`));
        template.setData(data);
        template.replaceIn($('#action'));
        const $actionNode = template.getJqueryObject();
        const selector = new Selector($actionNode.find('.selector-variant'), 'variant');
        if (ruleData.targets) {
            selector.setSelected('variant', ruleData.targets);
        }
    }
}