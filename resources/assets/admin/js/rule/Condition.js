import Expression from "./Expression";

export default class Condition {

    constructor(rule, data, index) {
        this.rule = rule;
        this.expressions = [];
        this.data = data;
        this.index = index;
        this.lastPosition = 0;
        this.initView();
        this.attachListeners();
    }

    initView() {
        const data = {
            id: this.data.id || '',
            conditionIndex: this.index
        };
        const template = new Template($('.templates .rule-condition'));
        template.setData(data);
        template.appendTo($('#conditions'));
        this.$node = template.getJqueryObject();

        if (this.data.groupedExpressions) {
            Object.keys(this.data.groupedExpressions).forEach(name => {
                this.addExpression(name, this.data.groupedExpressions[name]);
            });
        } else {
            this.addExpression(null, []);
        }
        this.polishView();
    }

    remove() {
        this.$node.remove();
        this.polishView();
    }

    polishView() {
        $('#conditions').find('.rule-condition .btn-remove-condition').removeClass('hidden');
        if ($('#conditions').find('.rule-condition').length < 2) {
            $('#conditions').find('.rule-condition .btn-remove-condition').addClass('hidden');
        }
        $('#conditions').find('.condition-separator').removeClass('hidden');
        $('#conditions').find('.rule-condition:first .condition-separator').addClass('hidden');
    }

    attachListeners() {
        this.$node.find('.btn-expression').on('click', () => this.addExpression(null, []));
        this.$node.find('.btn-remove-condition').on('click', () => this.remove());
    }

    addExpression(name, data) {
        const expression = new Expression(this, name, data, this.lastPosition);
        this.expressions.push(expression);
        this.lastPosition++;
    }
}