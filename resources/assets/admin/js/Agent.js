class Agent {
    constructor() {
        $('.agent-disable, .agent-enable').on('click', (e) => this.toggleStatus(e));
        $('.beat-search').on('change',() => this.enableMarketerSearch());
        $('.btn-remove-map').on('click', (e) => this.removeBeatMap(e));
    }

    enableMarketerSearch(){
        $('.search-beat-marketers').select2({
            minimumInputLength: 2,
            disabled: false,
            multiple: true,
            ajax: {
                url: '/search/marketers-by-beat',
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term,
                        beat_id: $('.beat-search').val()
                    };
                },
                processResults: function(data) {
                    data = $.map(data, function(obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    }

    removeBeatMap(e){
        const $form = $( e.currentTarget).closest('form');
        console.log($form);
        dialog("Remove Entry", "Are you sure you want to delete this entry?",
            function() {
                $form.submit();
            }
        );
    }

    toggleStatus(e){
        const target = e.currentTarget;
        const $form = $(target).closest('form');
        let title = "Disable Account";
        let description = "Are you sure you want to disable this account?";
        if ($(this).hasClass('user-enable')) {
            title = "Enable Account";
            description = "Are you sure you want to enable this account?";
        }
        dialog(title, description,
            function() {
                $form.submit();
            }
        );
    }
}

export default new Agent();
