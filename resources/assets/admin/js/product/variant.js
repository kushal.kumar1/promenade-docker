var Variants = {
    init: function() {
        $('.variants').on('click', '.variant .remove', this.delete);
        $('.variants').on('click', '.variant .restore', this.restore);
    },
    add: function() {
        var $variant = $('#variants');
        var $clone = $('.templates .variant').clone();
        var index = $variant.data('length');
        $clone.find("input").each(function() {
            this.name = this.name.replace(/variant\[variantIndex\]/, "variant[" + index + "]");
        });
        index++;
        $variant.append($clone);
        $variant.data('length', index);
    },
    delete: function() {
        var $variant = $(this).closest('.variant');
        dialog("Delete Variant", "Are you sure you want to delete this variant? This action cannot be reversed.",
            function() {
                var request = $.ajax({
                    url: "/products/variants/delete",
                    data: {
                        id: $variant.data('id')
                    },
                    type: 'POST'
                });
                request.done(function() {
                      window.location.reload();
                });
                request.fail(function() {
                });
            }
        );
    },
    restore: function() {
      var $variant = $(this).closest('.variant');
      var request = $.ajax({
          url: "/products/variants/restore",
          data: {
              id: $variant.data('id')
          },
          type: 'POST'
      });
      request.done(function() {
          window.location.reload();
      });
    }
};

export {Variants}
