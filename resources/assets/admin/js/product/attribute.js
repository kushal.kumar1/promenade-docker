var Attributes = {
    init: function() {
        $('#attribute-add').on('click', function() {
            var selections = $('.attribute-select').select2('data');
            if (selections.length > 0) {
                var attribute = selections[0];
                Attributes.add(attribute);
            }
        });
        $('#attributes').on('click', '.attribute .remove', this.delete);
        this.attachSelect2();
    },
    add: function(attribute) {
        $('.attribute-select').val(null).trigger('change');
        var attributeTemplate = new Template($('.templates .attribute'));
        attributeTemplate.setData(attribute);
        attributeTemplate.appendTo($('#attributes'));
    },
    attachSelect2: function() {
        $('.attribute-select').select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/attributes',
                dataType: 'json',
                processResults: function(data) {
                    data = $.map(data, function(obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    },
    delete: function() {
        var $attribute = $(this);
        dialog("Delete Attribute", "Are you sure you want to delete this attribute? This action cannot be reversed.",
            function() {
                $attribute.parent().remove();
            }
        );
    }
};

export {Attributes}
