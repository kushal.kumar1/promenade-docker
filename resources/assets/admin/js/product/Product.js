import {Variants} from './variant.js';
import {Attributes} from "./attribute.js";

class Product {
    constructor() {
        Variants.init();
        Attributes.init();

        $('.custom-select2').each((index, obj) => {
            this.initSelect2($(obj));
        });
        this.sortImages();
        $(document).on('click', '.product-image-delete', (e) => this.deleteProductImage(e));
        $(document).on('click', '.sync-product', (e) => this.runSync(e));
        $(document).on('click', '.sync-price', (e) => this.runSync(e));
        $(document).on('click', '.generate-barcode', (e) => this.generateBarcode(e));
        $(document).on('click', '.barcode-print', (e) => this.printBarcodeLabels(e));
    }


    runSync(e) {
        const $selector = $(e.currentTarget);
        let productId = $selector.data('id');
        let url = $selector.data('url');
        $.ajax({
            url: url,
            type: 'POST',
            data: {id: productId}
        }).done((response) => {
            const toast = new Toast(response.message);
            toast.show();
        }).fail((error) => {
            const toast = new Toast(error.responseJSON.message);
            toast.show();
        });
    }

    initSelect2($selector, minInput = 2) {
        let selectorPlaceholder = $selector.data('placeholder');
        let selectorUrl = $selector.data('url');
        $selector.select2({
            minimumInputLength: minInput,
            placeholder: selectorPlaceholder,
            ajax: {
                url: selectorUrl,
                dataType: 'json',
                processResults: function processResults(data) {
                    return {
                        results: data
                    };
                }
            },
            templateResult: this.resultsTemplate,

            templateSelection: result => {
                if (result.text !== '') {
                    return `${result.id}  ${result.text}`;
                }
                if (typeof result.name === 'undefined') {
                    return `${result.id} ${result.reference}`;
                }
                return `${result.id}  ${result.name}`;
            }
        });
    }

    resultsTemplate(result) {
        if (!result.id) {
            return result.name;
        }
        if (typeof result.name === 'undefined') {
            return `${result.id} ${result.reference}`;
        }

        let html = '<div class="padding-10">';
        html += `<h5 class="h6 bold">${result.id + ' ' + result.name}</h5>`;
        html += '</div>';

        return $(html);
    }

    generateBarcode(e) {
        let url = $(e.currentTarget).data('url');
        $.ajax({
            url : url,
            type: 'POST',
            dataType: 'json',
        }).done(() => {
            location.reload();
        });
    }

    printBarcodeLabels(e) {
        let qty = $('.js-barcode-qty').val();
        if (qty % 2 === 0 && qty > 0) {
            let productId = $(e.currentTarget).data('id');
            const url = "/barcode/" + productId + "/" + qty;
            window.open(url, '_blank');
            Modal.hide($('#modal-barcode'));
        } else {
            const toast = new Toast('Invalid Quantity');
            toast.show();
        }
    }

    sortImages() {
        let imagelist = document.getElementById('image-list');
        if (imagelist != null) {
            let sortable = Sortable.create(imagelist, {
                animation: 150,
                dataIdAttr: 'data-id',

                onEnd: function (evt) {
                    let data = sortable.toArray();
                    let url = imagelist.dataset.destination;
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: {
                            images: data
                        }
                    });
                },
            });
        }
    }

    deleteProductImage(e) {
        let $button = $(e.currentTarget);
        dialog("Delete Image", "Are you sure you want to delete this Image? This action cannot be reversed.",
            function () {
                $.ajax({
                    url: "/products/" + $button.data('product-id') + "/images/delete",
                    type: 'post',
                    data: {
                        id: $button.data('id')
                    },
                    success: function () {
                        $button.closest('div').remove();
                    }
                });
            }
        );
    }
}

export default new Product();
