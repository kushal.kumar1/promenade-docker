class PurchaseInvoice {
    constructor() {
        this.selectStores = $('#select2-stores');
        this.initializeStoresSelect();
        $('#button-open-cart').on('click', (e) => this.createCart(e));
        $(document).on('click', '.gen-sale-order', (e) => this.generateSaleOrder(e));
    }

    createCart(e) {
        const $target = $(e.currentTarget);
        const url = $target.data('url');
        const destination = $target.data('destination');
        const vendors = this.selectStores.select2('data');
        const selectedVendor = vendors[0].id;

        $target.prop('disabled', true);
        $.ajax(url, {
            method: 'POST',
            data: {
                vendor_id: selectedVendor
            }
        }).done((res) => {
            window.location = destination.replace("#", res.id);
        }).fail((error) => {
            console.log(error);
            const toast = new Toast('Something went wrong!');
            toast.show();
            $target.prop('disabled', false);
        });
    }

    generateSaleOrder(e) {
        const $target = $(e.currentTarget);
        const url = $target.data('url');
        let pID = $target.data('id');
        const request = $.ajax({
            type: "POST",
            url: url,
            data: {purchase_invoice_id: pID},
            beforeSend: () => {
                $(".ajax-loader").show();
                $target.attr('disabled', 'disabled');
            }
        });

        request.done((response) => {
            location.reload();
        });
        request.fail((error) => {
            const toast = new Toast('Something went wrong!');
            toast.show();
        });
    }

    initializeStoresSelect() {
        const searchStoresUrl = this.selectStores.data('url');
        this.selectStores.select2({
            minimumInputLength: 2,
            ajax: {
                url: searchStoresUrl,
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: this.vendorTemplate,
            templateSelection: this.vendorSelection
        })
    }

    vendorTemplate(vendor) {
        if (!vendor.id) {
            return vendor.text;
        }
        let html = '<div class="padding-10">';
        html += `<h5 class="h6 bold">${vendor.name}</h5>`;
        if (vendor.addresses.length) {
            html += `<h6 class="margin-t-5">${vendor.addresses[0].address}
                <br>GSTIN: ${vendor.addresses[0].gstin}</h6>`;
        }
        html += '</div>';

        return $(html);
    }

    vendorSelection(vendor) {
        return vendor.name;
    }
}

export default new PurchaseInvoice();