class Search {
    constructor(){
        const searchElems = this.getSearchElems();
        searchElems.forEach(searchElem => {
            this.attachSelect2(searchElem);
        });
    }

    attachSelect2(searchElem){
        $(searchElem.selector).select2({
            minimumInputLength: searchElem.hasOwnProperty('min_input') ? searchElem.min_input : 2,
            ajax: {
                url: searchElem.url,
                dataType: 'json',
                processResults: (data) => this.processResults(data)
            }
        });
    }

    processResults(data){
        data = $.map(data, function(obj) {
            obj.id = obj.id;
            obj.text = obj.name || obj.reference || obj.label;
            return obj;
        });

        return {
            results: data
        };
    }

    //add more common searches here
    getSearchElems(){
        return [
            {
                'selector' : '.beat-search',
                'url': '/search/beats'
            },
            {
                'selector' : '.brand-search',
                'url': '/search/brands'
            },
            {
                'selector' : '.marketer-search',
                'url': '/search/marketers'
            },
            {
                'selector' : '.agent-search',
                'url': '/search/agents'
            },
            {
                'selector' : '.agent-search-picker',
                'url': '/search/agents?type=picker'
            },
            {
                'selector' : '.select-rack',
                'url' : '/search/racks'
            },
            {
                'selector' : '.beat-locations-search',
                'url' : '/search/beat-locations',
                'min_input' : '0'
            },
            {
                'selector' : '.warehouse-search',
                'url' : '/search/warehouses',
                'min_input' : '0'
            },
            {
                'selector' : '.storages-search',
                'url' : '/search/storages',
                'min_input' : '1'
            }
        ];
    }
}

export default new Search()
