class Store {
    constructor(){
        $('.users-select').on('select2:select', () => this.addUser());
        $('.remove-user').on('click', (e) => this.removeUser(e));
        this.attachUserSearch();
        $('select[name="status"]').on('change', (e) => this.toggleDisableReasons(e));
    }

    addUser(){
        $('#form-users').submit();
    }

    removeUser(e){
        $(e.currentTarget).closest('.form-remove-user').submit();
    }

    attachUserSearch(){
        $('.users-select').select2({
            placeholder: "Search & Add Users",
            minimumInputLength: 2,
            ajax: {
                url: '/search/users',
                dataType: 'json',
                processResults: function(data) {
                    data = $.map(data, function(obj) {
                        obj.id = obj.id;
                        obj.text = obj.first_name + " " +obj.last_name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    }

    toggleDisableReasons(e){
        let value = $(e.currentTarget).val();
        if(value == 0) {
            $('.inactive_reason :input').prop('disabled', false);
            $('.disable_reason :input').prop('disabled', true);
            $('.disable_reason').addClass('hidden');
            $('.inactive_reason').removeClass('hidden');
        }else if(value == 2) {
            $('.inactive_reason :input').prop('disabled', true);
            $('.disable_reason :input').prop('disabled', false);
            $('.inactive_reason').addClass('hidden');
            $('.disable_reason').removeClass('hidden');
        }else{
            $('.inactive_reason').addClass('hidden');
            $('.disable_reason').addClass('hidden');
        }

    }
}

export default new Store();
