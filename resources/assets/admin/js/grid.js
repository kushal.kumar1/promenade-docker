$(".grid").on("click", ".grid-reset", function() {
  var form = $(this).closest("form");
  var gridName = form.find("table").prop("id");
  clearCookiesForGrid(gridName);
  form.find("input").val("");
  form.find("select").prop("selectedIndex", 0);
  form.submit();
  return false;
});

$(".grid").on("change", "select", function(e) {
  $(this)
    .closest("form")
    .submit();
  return false;
});

$(".grid").on("click", ".pagination li a", function() {
  var grid = $(this).closest(".grid");
  var url = $(this).prop("href");
  submitGridForm(grid, url, null);
  return false;
});

$(".grid").on("click", 'thead th small a[title~="Sort"]', function() {
  var grid = $(this).closest(".grid");
  var url = $(this).prop("href");

  if (url === "") return false;

  submitGridForm(grid, url, null);
  return false;
});

$(".grid").on("submit", "form", function() {
  var data = $(this).serialize();
  var grid = $(this).closest(".grid");
  submitGridForm(grid, "", data);
  return false;
});

function submitGridForm(grid, url, data) {
  $(".cache_form .changed_through_ajax").val("1");
  $(".ajax-loader").show();
  $.ajax(url, {
    type: "GET",
    data: data,
    cache: false,
    success: function(data) {
      grid.html(data);
      $(".ajax-loader").hide();
    },
    error: function(data) {},
  });
}

function clearCookiesForGrid(gridName) {
  var cookies = document.cookie.split(";");
  for (var i = 0; i < cookies.length; i++) {
    var c = cookies[i];
    while (c.charAt(0) == " ") c = c.substring(1, c.length);
    if (c.indexOf(gridName) == 0) {
      cookieName = c.split("=")[0];
      document.cookie = cookieName + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    }
  }
}

$(".grid").on("change", ".export_checkbox", function() {
  var grid = $(this).closest(".grid");
  var exportButton = grid.find(".excel_export a");
  exportButton.prop(
    "href",
    exportButton
      .prop("href")
      .replace("&" + $(this).prop("name") + "=" + $(this).val(), "")
  );
  exportButton.prop(
    "href",
    exportButton
      .prop("href")
      .replace($(this).prop("name") + "=" + $(this).val(), "")
  );
  if (this.checked)
    exportButton.prop(
      "href",
      addParameter(
        exportButton.prop("href"),
        $(this).prop("name"),
        $(this).val(),
        false
      )
    );
});

$(".grid").on("change", ".select_all_checkbox", function() {
  var checkboxes = $(this)
    .closest(".grid")
    .find(".select-checkbox");
  checkboxes.filter(":checked").trigger("click");
  if (this.checked) {
    checkboxes.trigger("click");
  }
});

$(".grid").on("click", ".bulk-actions .action > a, .btn-custom > a", function(
  e
) {
  e.preventDefault();
  if (
    $(this)
      .attr("href")
      .charAt(0) === "#"
  ) {
    return false;
  }

  const href = $(this).prop("href");
  const $form = $(this).closest("form");
  const destination = $(this)
    .parent()
    .attr("data-destination");
  let itemsCount = getSelectedRecords($(this).closest(".grid"));

  if (itemsCount == 0) {
    itemsCount = getTotalRecords($(this).closest(".grid"));
  }
  const title = "Are you Sure?";
  const message = `This will affect ${itemsCount} items based on the filters selected.`;

  dialog(title, message, () => {
    // toggleSubmitDisable($form);
    const request = $.ajax(href, {
      type: "POST",
      data: $form.serialize(),
      cache: false,
    });

    request.done((response) => onBulkActionCompleted(destination, response));
    request.fail(function(error) {
      var errorFunction = $form.data("error");
      if (errorFunction != undefined) {
        window[errorFunction](error);
      } else if (error.status == 422) {
        showFormErrors($form, error);
        showMessage(error);
      } else {
        showMessage(error);
      }
    });
  });
});

function getTotalRecords($grid) {
  const recordsRegex = /Showing records [0-9]+—[0-9]+ of ([0-9]+)/;
  const match = recordsRegex.exec($grid.find(".showing_records").text());
  return match ? match[1] : 0;
}

function getSelectedRecords($grid) {
  var checkboxes = $grid.find(".select-checkbox");
  return checkboxes.filter(":checked").length;
}

function onBulkActionCompleted(destination, response) {
  if (destination != undefined) {
    if (destination != false) {
      var regex = /#(\S+)#/;
      while ((match = regex.exec(destination))) {
        path = match[1].split(".");
        var value = response;
        for (var i = 0; i < path.length; i++) value = value[path[i]];
        destination = destination.replace(
          match[0],
          value == undefined ? "" : value
        );
        match = regex.exec(destination);
      }
      window.location = destination;
    }
  } else {
    window.location.reload();
  }
}

function addParameter(url, parameterName, parameterValue, replaceDuplicates) {
  var cl;
  if (url.indexOf("#") > 0) {
    cl = url.indexOf("#");
    urlhash = url.substring(url.indexOf("#"), url.length);
  } else {
    urlhash = "";
    cl = url.length;
  }
  sourceUrl = url.substring(0, cl);

  var urlParts = sourceUrl.split("?");
  var newQueryString = "";

  if (urlParts.length > 1) {
    var parameters = urlParts[1].split("&");
    for (var i = 0; i < parameters.length; i++) {
      var parameterParts = parameters[i].split("=");
      if (!(replaceDuplicates && parameterParts[0] == parameterName)) {
        if (newQueryString === "") newQueryString = "?";
        else newQueryString += "&";
        newQueryString +=
          parameterParts[0] +
          "=" +
          (parameterParts[1] ? parameterParts[1] : "");
      }
    }
  }
  if (newQueryString === "") newQueryString = "?";

  if (newQueryString !== "" && newQueryString != "?") newQueryString += "&";
  newQueryString +=
    parameterName + "=" + (parameterValue ? parameterValue : "");

  return urlParts[0] + newQueryString + urlhash;
}
