$.jstree.defaults.core.themes.variant = "large";
var brandRequest;
var $brandContainer = $('#container-brand');
var updateAction = $brandContainer.data('action-update');
var createAction = $brandContainer.data('action-create');
var brandTemplate = new Template($('.templates>.brand'));
var brandTree = $('#tree-brands').on('select_node.jstree', function(e, data) {
    var path = data.instance.get_path(data.node, " > ");
    if (brandRequest) {
        brandRequest.abort();
    }
    brandRequest = $.ajax({
        url: "/brands/" + data.selected[0],
        type: 'GET'
    });
    brandRequest.done(function(data) {
        console.log(data);
        data['heading'] = data.name + " (ID: " + data.id +")";
        data['action'] = updateAction;
        data['button_name'] = 'Update';
        brandTemplate.setData(data);
        brandTemplate.replaceIn($brandContainer);
        $select2 = enableMarketerSearch();
        var newOption = new Option(data.marketer.name,data.marketer.id,true, true);
        $select2.append(newOption).trigger('change');

    });
    brandRequest.fail(function() {

    });
}).on('move_node.jstree', function(e, data) {
    var parent = data.instance.get_node(data.parent);
    var node = data.instance.get_node(data.node);
    var leftNodeId =null;
    var parentNodeId=null;
    if(parent.id != '#'){
        parentNodeId = parent.id;
    }
    if(data.position != 0) {
        leftNodeId = data.instance.get_node(parent.children[data.position - 1]).id;
    }
    sortBrand(node.id,parentNodeId,leftNodeId);

}).jstree({
    "core": {
        "multiple": false,
        "animation": 0,
        "check_callback": true,
    },
    "checkbox": {
        "three_state": false
    },
    "contextmenu": {
        "select_node": false,
        "items": {
            'create': {
                "label": "Create brand",
                "action": function(event) {
                    var nodeId=null;
                    var previousSiblingId=null;
                    var node = event.reference.jstree(true).get_node(event.reference);
                    var previousSibling = event.reference.jstree(true).get_prev_dom(event.reference,true);
                    var path = event.reference.jstree(true).get_path(node, " > ") + " > ";
                    if(typeof node  !== "undefined"){
                        nodeId=node.id;
                    }
                    if(typeof previousSibling[0]  !== "undefined"){
                        previousSiblingId=previousSibling[0].id;
                    }
                    showNewBrandForm(path, nodeId,previousSiblingId);
                }
            },
            'delete': {
                "label": "Delete brand",
                "action": function(event) {
                    var node = event.reference.jstree(true).get_node(event.reference);
                    var nodeId=node.id;
                    dialog("Delete brand", "Are you sure you want to Delete this brand? This action cannot be undone.",
                        function() {
                            deleteBrand(nodeId);
                        }
                    );
                }
            }
        }
    },
    "plugins": ["checkbox", "search", "dnd", "contextmenu"]
});


function showNewBrandForm(path, parentId,previousSiblingId) {
    brandTemplate.setData({
        heading: path + "New brand",
        parent_id: parentId,
        left_node_id:previousSiblingId,
        status: 1,
        visible: 1,
        action: createAction,
        button_name: 'Create'
    });
    brandTemplate.replaceIn($brandContainer);
    enableMarketerSearch();
}

showNewBrandForm("", null,null);

function enableMarketerSearch()
{
    return $brandContainer.find('.marketer').select2({
        minimumInputLength: 2,
        ajax: {
            url: '/search/marketers',
            dataType: 'json',
            processResults: function(data) {
                data = $.map(data, function(obj) {
                    obj.id = obj.id;
                    obj.text = obj.name;
                    return obj;
                });
                return {
                    results: data
                };
            }
        }
    });
}

function deleteBrand(brandId) {
    var request = $.ajax({
        url: "/brands/"+brandId+"/delete",
        type: 'POST'
    });
    request.done(function(data) {
        location.reload();
    });
    request.fail(function(error) {
        showMessage(error);
    });
}

function sortBrand(brandId,parentBrandId,leftNodeId) {
    var request = $.ajax({
        url: "/brands/sort",
        type: 'POST',
        data: {'id':brandId,'parent_id':parentBrandId,'left_node_id':leftNodeId}
    });
    request.done(function(data) {
        location.reload();
    });
    request.fail(function(error) {
        showMessage(error);
    });
}
