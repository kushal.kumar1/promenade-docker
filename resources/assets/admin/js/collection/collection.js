$.jstree.defaults.core.themes.variant = "large";
var collectionRequest;
var $collectionContainer = $('#container-collection');
var updateAction = $collectionContainer.data('action-update');
var createAction = $collectionContainer.data('action-create');
var collectionTemplate = new Template($('.templates>.collection'));
var collectionTree = $('#tree-collections').on('select_node.jstree', function(e, data) {
    var path = data.instance.get_path(data.node, " > ");
    if (collectionRequest) {
        collectionRequest.abort();
    }
    collectionRequest = $.ajax({
        url: "/collections/" + data.selected[0],
        type: 'GET'
    });
    collectionRequest.done(function(data) {
        data['heading'] = data.name + " (ID: " + data.id +")";
        data['action'] = updateAction;
        data['button_name'] = 'Update';
        collectionTemplate.setData(data);
        collectionTemplate.replaceIn($collectionContainer);
    });
    collectionRequest.fail(function() {

    });
}).on('move_node.jstree', function(e, data) {
    var parent = data.instance.get_node(data.parent);
    var node = data.instance.get_node(data.node);
    var leftNodeId =null;
    var parentNodeId=null;
    if(parent.id != '#'){
        parentNodeId = parent.id;
    }
    if(data.position != 0) {
        leftNodeId = data.instance.get_node(parent.children[data.position - 1]).id;
    }
    sortCollection(node.id,parentNodeId,leftNodeId);

}).jstree({
    "core": {
        "multiple": false,
        "animation": 0,
        "check_callback": true,
    },
    "checkbox": {
        "three_state": false
    },
    "contextmenu": {
        "select_node": false,
        "items": {
            'create': {
                "label": "Create Collection",
                "action": function(event) {
                    var nodeId=null;
                    var previousSiblingId=null;
                    var node = event.reference.jstree(true).get_node(event.reference);
                    var previousSibling = event.reference.jstree(true).get_prev_dom(event.reference,true);
                    var path = event.reference.jstree(true).get_path(node, " > ") + " > ";
                    if(typeof node  !== "undefined"){
                        nodeId=node.id;
                    }
                    if(typeof previousSibling[0]  !== "undefined"){
                        previousSiblingId=previousSibling[0].id;
                    }
                    showNewCollectionForm(path, nodeId,previousSiblingId);
                }
            },
            'delete': {
                "label": "Delete Collection",
                "action": function(event) {
                    var node = event.reference.jstree(true).get_node(event.reference);
                    var nodeId=node.id;
                    dialog("Delete Collection", "Are you sure you want to Delete this collection? This action cannot be undone.",
                        function() {
                            deleteCollection(nodeId);
                        }
                    );
                }
            }
        }
    },
    "plugins": ["checkbox", "search", "dnd", "contextmenu"]
});


function showNewCollectionForm(path, parentId,previousSiblingId) {
    collectionTemplate.setData({
        heading: path + "New Category",
        parent_id: parentId,
        left_node_id:previousSiblingId,
        status: 1,
        visible: 1,
        action: createAction,
        button_name: 'Create'
    });
    collectionTemplate.replaceIn($collectionContainer);
}

showNewCollectionForm("", null,null);

function deleteCollection(collectionId) {
    var request = $.ajax({
        url: "/collections/"+collectionId+"/delete",
        type: 'POST'
    });
    request.done(function(data) {
        location.reload();
    });
    request.fail(function(error) {
        showMessage(error);
    });
}

function sortCollection(collectionId,parentCollectionId,leftNodeId) {
    var request = $.ajax({
        url: "/collections/sort",
        type: 'POST',
        data: {'id':collectionId,'parent_id':parentCollectionId,'left_node_id':leftNodeId}
    });
    request.done(function(data) {
        location.reload();
    });
    request.fail(function(error) {
        showMessage(error);
    });
}
