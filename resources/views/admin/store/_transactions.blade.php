<div class="card margin-t-30">
  <div class="flex gutter-between">
    <div class="h5 bold">
      Recent Transactions
    </div>
    <div>
      <a href="{{route('admin::transactions', $store->id)}}" target="_blank" class="btn btn-primary btn-sm">View Ledger</a>
      <a href="{{route('admin::transactions.index', ['store_id' => $store->id, 'type' => 'pending'])}}" target="_blank" class="btn btn-primary btn-sm">View Pending Transaction</a>
      <a href="{{route('admin::transactions.create', ['store_id' => $store->id])}}" target="_blank" class="btn btn-primary btn-sm">Add Transaction</a>
    </div>
  </div>
  @if($store->transactions->isNotEmpty())
  <div class="margin-t-30">
      <div class="flex gutter-sm bold">
        <div class="col-3-12">
          Created Date
        </div>
        <div class="col-2-12">
          Source
        </div>
        <div class="col-4-12">
          Description
        </div>
        <div class="col-1-12">
          Type
        </div>
        <div class="col-2-12">
          Amount
        </div>
      </div>
      <hr class="hr hr-light margin-t-20" />
      @foreach($store->transactions as $transaction)
        <div class="margin-t-20">
          <div class="flex gutter-sm" style="flex-wrap:nowrap">
            <div class="col-3-12">
              {{(new DateTime($transaction->created_at))->format('F d, Y h:i A')}}
            </div>
            <div class="col-2-12">
              {{$transaction->source_type}}
            </div>
            <div class="col-4-12">
              {{$transaction->description}}
            </div>
            <div class="col-1-12">
              {{$transaction->type}}
            </div>
            <div class="col-2-12">
              Rs. {{$transaction->amount}}
            </div>
          </div>
        </div>
      @endforeach
    </div>
  @else
    <div class="margin-t-20 text-center">
      <i class="fa  fa-5x fa-thumbs-down" aria-hidden="true"></i>
      <div class="h4 margin-t-20">No Transactions Yet!</div>
    </div>
  @endif
</div>
