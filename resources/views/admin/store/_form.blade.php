<div id="edit-store" class="modal">
  <div class="modal-content">
    <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
    <h5 class="h6 title bold text-center">Edit Store</h5>
    <form class="form" method="post" action="{{route('admin::stores.update')}}" autocomplete="off">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$store->id}}" />
      <div class="margin-t-30">
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Beat</label>
            <select class="form-control beat-search" name="beat_id" style="width:100%">
              @if($store->beat_id)
                <option value="{{$store->beat_id}}">
                  {{$store->beat->name}}
                </option>
              @endif
            </select>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Name</label>
            <input class="form-control" name="name" value="{{$store->name}}" />
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Store Address</label>
            <input type="text" class="form-control" name="address" value="{{$store->address}}"/>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Landmark</label>
            <input type="text" class="form-control" name="landmark" value="{{$store->landmark}}"/>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-6-12">
            <label class="label">Pincode</label>
            <input class="form-control" name="pincode" value="{{$store->pincode}}" />
          </div>
          <div class="col-6-12">
            <label class="label">GSTIN</label>
            <input class="form-control" name="gstin" value="{{$store->gstin}}" />
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-6-12">
            <label class="label">Latitude</label>
            <input class="form-control" name="lat" type="text" value="{{$store->lat}}" />
          </div>
          <div class="col-6-12">
            <label class="label">Longitude</label>
            <input class="form-control" name="lng" type="text" value="{{$store->lng}}" />
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12">
            <label class="label">Tags</label>
            <select name="tag_ids[]" class="form-control" multiple>
              @foreach($tags as $tag)
              <option @if(in_array($tag->id, $selectedTagIds)) selected @endif value="{{$tag->id}}">{{$tag->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Contact Person</label>
            <input class="form-control" name="contact_person" type="text" value="{{$store->contact_person}}" />
          </div>
          <div class="col-12-12">
            <label class="label">Contact Mobile</label>
            <input class="form-control mobile-country-picker" name="contact_mobile" type="text" value="{{$store->contact_mobile}}" />
          </div>
          <div class="col-6-12">
            <label class="checkbox">
                <input type="checkbox" name="verified" value="1" {{$store->verified ? "checked" : ""}}>
                <span class="indicator"></span>
            </label>
            <label class="label">Verified</label>
          </div>
          @can('update-credit_limit')
          <div class="col-6-12">
            <label class="checkbox">
                <input type="checkbox" name="credit_allowed" value="1" {{$store->credit_allowed ? "checked" : ""}}>
                <span class="indicator"></span>
            </label>
            <label class="label">Credit Allowed</label>
          </div>
          @endcan
        </div>
        <div class="flex gutter-between margin-t-40">
          <button type="button" class="btn button-reject" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary button-success">Update</button>
        </div>
    </form>
  </div>
</div>
</div>
