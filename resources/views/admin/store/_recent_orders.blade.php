<div class="card margin-t-30">
  <div class="flex gutter-between">
    <div class="h5 bold">
      Recent Orders
    </div>
  </div>
  @if($store->orders->isNotEmpty())
    <div class="margin-t-20">
      @foreach($store->orders as $key => $order)
        <div class="margin-t-30">
          <div class="flex gutter-between">
            <div>
              <div>
                <a href="{{route('admin::orders.edit', $order->id)}}" class="link" target="_blank">Order #{{$order->reference_id}}</a>
                <span class="tag tag-success">{{$order->status}}</span>
              </div>
              <div class="margin-t-5">
                Rs. {{$order->total}}
              </div>
            </div>
            <div>
              {{(new DateTime($order->created_at))->format('F d, Y h:i A')}}
            </div>
          </div>
        </div>
        <div class="margin-t-20">
          @foreach($order->itemsBySku as $sku => $items)
            <div class="flex gutter-sm">
              <div class="col-1-12">
                <img class="bd" src="{{Image::getSrc($items->first()->product->images->first(), 60)}}" />
              </div>
              <div class="col-11-12">
                <div>
                  {{$items->first()->product->name}}
                </div>
                <div class="margin-t-5">
                  Qty: {{$items->sum('quantity')}} x {{$items->first()->variant}}
                </div>
              </div>
            </div>
            @endforeach
        </div>
        @if($key != $store->orders->count()-1)
        <hr class="hr hr-light margin-t-30" />
        @endif
        @endforeach
    </div>
    @else
    <div class="margin-t-20 text-center">
      <i class="fa  fa-5x fa-thumbs-down" aria-hidden="true"></i>
      <div class="h4 margin-t-20">No Orders Yet!</div>
    </div>
    @endif

</div>
