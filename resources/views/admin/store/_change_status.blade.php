<div id="change-status" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title text-center">Change Status</h5>
        <form class="form" method="post" action="{{route('admin::stores.updateStatus')}}" autocomplete="off">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$store->id}}" />
            <div class="margin-t-20">
                <label class="label">Status</label>
                <select class="form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                    <option value="2">Disable</option>
                </select>
            </div>
            <div class="margin-t-20 hidden disable_reason">
                <label class="label">Disable Reason</label>
                <select class="form-control" name="disable_reason">
                    <option value="">
                        -- Select --
                    </option>
                    @foreach(config('settings.store_disable_reasons') as $reason)
                        <option value="{{$reason}}">{{$reason}}</option>
                    @endforeach
                </select>
            </div>
            <div class="margin-t-20 hidden inactive_reason">
                <label class="label">Inactive Reason</label>
                <select class="form-control" name="disable_reason">
                    <option value="">
                        -- Select --
                    </option>
                    @foreach(config('settings.store_inactive_reasons') as $reason)
                        <option value="{{$reason}}">{{$reason}}</option>
                    @endforeach
                </select>
            </div>
            <div class="flex gutter-between margin-t-30">
              <button type="button" class="btn" data-dismiss="modal">Cancel</button>
              <button class="btn btn-primary button-success">Save</button>
            </div>
        </form>
    </div>
</div>
