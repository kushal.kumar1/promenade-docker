@extends('admin.master')

@section('content')

<div class="container">
  <div class="flex gutter-between">
    <div class="h1">
      Stores
    </div>
    <div>
      <button class="btn btn-primary" data-modal="#add-store">Add Store</button>
    </div>
  </div>
  <div id="add-store" class="modal">
    <div class="modal-content">
      <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
      <h5 class="h6 title text-center">Add Store</h5>
      <form class="form" method="post" action="{{route('admin::stores.store')}}" autocomplete="off">
        {{csrf_field()}}
        <div class="margin-t-30">
          <div class="flex gutter">
            <div class="col-12-12">
              <label class="label">Name</label>
              <input class="form-control" name="name" />
            </div>
          </div>
          <div class="flex gutter">
            <div class="col-12-12">
              <label class="label">Store Address</label>
              <input type="text" class="form-control" name="address" />
            </div>
          </div>
          <div class="flex gutter">
            <div class="col-12-12">
              <label class="label">Landmark</label>
              <input type="text" class="form-control" name="landmark" />
            </div>
          </div>
          <div class="flex gutter">
            <div class="col-6-12">
              <label class="label">Pincode</label>
              <input class="form-control" name="pincode" />
            </div>
            <div class="col-6-12">
              <label class="label">GSTIN</label>
              <input class="form-control" name="gstin" />
            </div>
          </div>
          <div class="flex gutter">
            <div class="col-12-12">
              <label class="label">Contact Person</label>
              <input class="form-control" name="contact_person" type="text" value="" placeholder="Enter Full Name"/>
            </div>
            <div class="col-12-12">
              <label class="label">Contact Mobile</label>
              <input class="form-control mobile-country-picker" name="contact_mobile" type="text" value="" placeholder="Mobile number" />
              <div class="small margin-t-5">A user will be created using this mobile number.</div>
            </div>
          </div>
          <div class="flex gutter-between margin-t-40">
            <button class="btn" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary button-success">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  @include('admin.grid')
</div>

@endsection
