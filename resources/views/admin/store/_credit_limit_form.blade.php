<div id="credit-limit" class="modal">
  <div class="modal-content">
    <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
    <h5 class="h5 bold text-center">Override Credit Limit</h5>
    <hr class="hr hr-light margin-t-10">
    <form class="margin-t-10 form form-credit-limit" method="post" action="{{route('admin::stores.credit-limit.update')}}" autocomplete="off">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$store->id}}" />
      <div>
        <label class="label">TYPE</label>
        <div class="flex">
          <div class="col-3-12">
            <label class="flex cross-center">
              <div class="radio margin-r-10">
                <input type="radio" name="type" value="manual" checked>
                <span class="indicator"></span>
              </div>
              <span>Manual</span>
            </label>
          </div>
          <div class="col-3-12">
            <label class="flex cross-center">
              <div class="radio margin-r-10">
                <input type="radio" name="type" value="automatic">
                <span class="indicator"></span>
              </div>
              <span>Automatic</span>
            </label>
          </div>
        </div>
      </div>

      <div class="margin-t-20 value-div">
        <label class="label">Value</label>
        <input class="form-control" type="number" name="value"/>
        <span class="error error-value"></span>
      </div>
      <div class="margin-t-20 bold">
        Calculated Limit : {{$autoCreditLimit}}
      </div>


      <div class="flex gutter-between margin-t-40">
        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary button-success">Save</button>
      </div>
    </form>
  </div>
</div>
