<div class="card margin-t-30">
    <div class="flex gutter-between">
        <div class="h5 bold">
            Store Profiling
        </div>
    </div>

    <hr class="hr hr-light margin-t-20" />
    <?php $allAnswers = $storeAnswers->groupBy('question_id'); ?>
    <div class="margin-t-20">
        <div class="flex gutter-sm bold">
          <div class="col-4-12">
            Question
          </div>
          <div class="col-4-12">
            Answer/(s)
          </div>
          <div class="col-2-12">
            Agent
          </div>
          <div class="col-2-12">
            Date
          </div>
        </div>
        <hr class="hr hr-light margin-t-20" />
        @foreach ($allAnswers as $questionID => $answers)
          <?php $answer = $answers->first(); ?>
          <div class="margin-t-20">
            <div class="flex gutter-sm" style="flex-wrap:nowrap">
              <div class="col-4-12">
                {{ $answer->question->name }}
              </div>
              <div class="col-4-12">
                {{ implode(", ", $answers->pluck('answer')->toArray()) }}
              </div>
              <div class="col-2-12">
                {{ $answer->agent->name }}
              </div>
              <div class="col-2-12">
                {{\Carbon\Carbon::parse($answer->updated_at)->format("jS F Y")}}
              </div>
            </div>
          </div>
          <hr class="hr hr-light margin-t-20" />
        @endforeach
        {{-- @foreach($storeAnswers as $answer)
            <div class="flex gutter-between margin-t-10">
                <div>
                    <p><small>Question {{ $loop->index + 1 }}</small> - <b>{{$answer->question->name}}?</b></p>
                    <p class="margin-t-10"><small>Answer</small> - {{$answer->answer}}</p>
                </div>

                <div>
                    <p><small>Agent</small> - <b>{{$answer->agent->name}}</b></p>
                    <p class="tag tag-manifested small margin-t-10">{{\Carbon\Carbon::parse($answer->updated_at)->format("jS F Y")}}</p>
                </div>
            </div>

            @if(!$loop->last)
                <hr class="hr hr-light margin-t-20" />
            @endif
        @endforeach --}}
    </div>
</div>
