@extends('admin.master')

@section('content')
<div class="container">
  <div class="header-actions hidden">
    <button type="button" class="btn btn-discard margin-r-10">Discard</button>
    <button type="button" class="btn btn-primary form-button" data-form=".form-stores">Save</button>
  </div>
  <div class="regular light-grey">
    <a href="{{route('admin::stores.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Stores</a>
  </div>
  <div class="margin-t-10">
    <div class="flex gutter-between">
      <div>
        <h1 class="h1 margin-r-20 bold">{{$store->name}}</h1>
      </div>
      <div>
        <div class="flex gutter-sm">
            @if($store->canCreateOrder())
              <div>
                <a class="btn btn-primary" href="{{route('admin::stores.cart', ['id' => $store->id])}}"><i class="fa fa-first-order" aria-hidden="true"></i> Create Order</a>
              </div>
            @endif
            @can('create-return')
              <div>
                <a class="btn btn-primary" href="{{route('admin::stores.return-cart', ['id' => $store->id])}}"><i class="fa fa-first-order" aria-hidden="true"></i> Add Return</a>
              </div>
            @endcan
            <div>
              <a class="btn btn-danger"  data-modal="#change-status"><i class="fa fa-stop-circle" aria-hidden="true"></i> Change Status</a>
            </div>
            {{-- <button type="button" class="btn btn-danger store-change-status" data-modal="#change-status"><i class="fa fa-ban" aria-hidden="true"></i> Change Status</button> --}}
        </div>
      </div>
    </div>
    <div class="flex gutter">
      <div class="col-8-12">
        <div class="card margin-t-30">
          <div class="flex gutter">
            <div class="col-12-12">
              <div class="flex gutter-between">
                <div class="h5 bold">{{$store->name}}</div>
                <div>
                  <a data-modal="#edit-store" class="btn btn-sm">Edit Store</a>
                </div>
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Address:</span>{{$store->address}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Landmark:</span>{{$store->landmark}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Pincode:</span>{{$store->pincode}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">BEAT:</span>{{!empty($store->beat_id) ? $store->beat->name: "Null"}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">GSTIN:</span>{{!empty($store->gstin) ? $store->gstin: "No record"}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Contact Person:</span>{{!empty($store->contact_person) ? $store->contact_person: "No record"}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Contact Mobile:</span>{{!empty($store->contact_mobile) ? $store->contact_mobile: "No record"}}
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Credit Allowed :</span>
                <span class="inline small tag {{$store->credit_allowed ? "tag-success" : "tag-warning"}} margin-t-5">{{$store->credit_allowed ? "Yes": "No"}}</span>
              </div>
              <div class="margin-t-5">
                <span class="bold margin-r-10">Tags :</span>
                @foreach($store->tags as $tag)
                  <span class="inline small tag tag-success">{{$tag->name}}</span>
                @endforeach
              </div>
              {{-- <hr class="hr hr-light margin-t-20" /> --}}
              @if($store->lat && $store->lng)
                <div class="margin-t-20">
                  <div class="bold">
                    Google Location:
                  </div>
                  {{$store->location}}
                </div>
                <div class="margin-t-5">
                  <a class="link" target="_blank" href="http://maps.google.com/maps?q={{$store->lat}},{{ $store->lng}}">See
                    Location in Maps</a>
                </div>
                @endif
                <div class="margin-t-10">
                    <div class="inline small tag {{$store->status == 1 ? "tag-success" : "tag-cancelled"}} margin-t-5">{{$store->getStatusText()}}
                    </div>
                    @if($store->verified)
                      <div class="inline small tag tag-success margin-t-5"><i class="fa fa-check" aria-hidden="true"></i> Verified
                      </div>
                      @else
                      <div class="inline small tag tag-warning margin-t-5"><i class="fa fa-times" aria-hidden="true"></i> Unverified
                      </div>
                      @endif
                </div>
                @if(in_array($store->status, [0,2]))
                <div class="margin-t-10 bold uppercase">
                    {{($store->status == 0) ? 'Inactive ' : 'Disable '}} Reason: {{$store->disable_reason}}
                </div>
                @endif
            </div>
          </div>
          <hr class="hr hr-light margin-t-20" />
          <div class="margin-t-20">
            <div class="flex gutter text-center">
              <div class="col-3-12">
                Credit Limit @if(!empty($store->creditLimit))<span class="tag uppercase small">{{$store->creditLimit->type}}</span>@endif
                <div class="margin-t-10 bold">
                  Rs. {{number_format($store->credit_limit_value)}}
                </div>
                @can('update-credit_limit')
                <div class="margin-t-5">
                  <a href="#" class="link small" data-modal="#credit-limit">Override</a>
                </div>
                @endcan
              </div>
              <div class="col-3-12">
                Billed Outstanding<br />
                <div class="margin-t-10 bold">
                  Rs. {{!empty($store->totalBalance) ? number_format($store->totalBalance->balance_amount) : 0}}
                </div>
              </div>
              <div class="col-3-12">
                Projected Outstanding<br />
                <div class="margin-t-10 bold">
                  Rs. {{!empty($store->totalBalance) ? number_format($store->totalBalance->balance_amount + $projectedOutstanding) : 0}}
                </div>
              </div>
              <div class="col-3-12">
                Uncleared Balance<br />
                <div class="margin-t-10 bold">
                  Rs. {{!empty($store->totalPendingBalance) ? number_format($store->totalPendingBalance->balance_amount) : 0}}
                </div>
              </div>
            </div>
          </div>

        </div>
        @include('admin.store._transactions')
        @include('admin.store._recent_orders')
        @include('admin.store._profile_questions')
      </div>
      <div class="col-4-12">
        <div class="card  margin-t-30">
          <h1 class="h5 bold">Store Users</h1>
          <div class="margin-t-20">
            <form id="form-users" class="form" action="{{route('admin::stores.users.add', ['id' => $store->id])}}" method="post">
              <select class="form-control users-select" name="user_id">
              </select>
            </form>
          </div>
          <div class="margin-t-20">
            @if($store->users->isNotEmpty())
              @foreach($store->users as $user)
                <div class="margin-t-10">
                  <div class="flex gutter-between">
                    <div>
                      <a href="{{route("admin::users.profile", $user->id)}}" class="link" target="_blank">{{$user->name}}</a>
                      (OTP:: {{$user->otp}})
                    </div>
                    <form class="form form-remove-user" action="{{route('admin::stores.users.remove', ['id' => $store->id])}}" method="post">
                      <input class="hidden" name="user_id" value="{{$user->id}}" />
                      <i class="fa fa-times pointer remove-user" aria-hidden="true"></i>
                    </form>
                  </div>
                </div>
                @if(!$loop->last)
                  <hr class="hr hr-light margin-t-10" />
                  @endif
                  @endforeach
                  @else
                  No Users Found!
                  @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@include('admin.store._form')
@include('admin.store._credit_limit_form')
@include('admin.store._change_status')
@endsection

@section('js')
<script type="text/javascript" src="{{mix('admin/js/store.js')}}"></script>
<script type="text/javascript">
var value = $('.form-credit-limit input[name=type]:checked').val();
$('.form-credit-limit input[name=type]').on('change', function(){
  value = $(this).val();
  toggleValueInput(value);
})
toggleValueInput(value);
function toggleValueInput(value)
{
  if(value == "automatic"){
    $('.value-div').hide();
  }
  else{
      $('.value-div').show();
  }
}

</script>
@endsection
