@extends('admin.master')
@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
    <link rel="stylesheet" href="/plugins/json/beautify-json.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
@endsection
@section('content')
    <div class="container">
        <form class="form-product" method="post" action="{{route('admin::profile-questions.options.post', [$question['id']])}}" data-actions=".header-actions" autocomplete="off">
            <input type="hidden" name="question_id" value="{{ $question['id'] }}" />
            
            <div class="header-actions">
                <button type="button" class="btn btn-discard margin-r-10">Discard</button>
                <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
            </div>
            {{csrf_field()}}
            <div class="h3 margin-t-10 bold">
                Question '{{$question['name']}}'
            </div>
            <div class="h5 margin-t-10">
                @isset($option)
                    Edit option #{{$option['id']}}
                    <input type="hidden" name="id" value="{{ $option['id'] }}" />
                @else
                    Add New Option
                @endisset
            </div> 

            <div class="card margin-t-20">
                <div>
                    <label class="label">Option</label>
                    <input type="text" name="option" class="form-control" value="{{!empty($option) ? $option['option'] : ""}}" required />
                    <span class="error error-option"></span>
                </div>

                <div class="flex margin-t-20">
                    <div class="col-2-12 padding-10">
                        <label class="label">Status</label>
                        <select class="form-control" name="status" required>
                            <option value="1" default selected {{ !empty($option) ? ($option['status'] == 1 ? "selected" : "") : ""}}>ACTIVE</option>
                            <option value="0" {{ !empty($option) ? ($option['status'] == 0 ? "selected" : "") : ""}}>In-ACTIVE</option>
                        </select>
                        <span class="error error-status"></span>
                    </div>

                    <div class="col-2-12 padding-10">
                        <label class="label">Priority</label>
                        <input type="number" name="priority" class="form-control" value="{{!empty($option) ? $option['priority'] : ""}}" required />
                        <span class="error error-option"></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/plugins/json/jquery.beautify-json.js"></script>
@endsection
