@extends('admin.master')
@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
    <link rel="stylesheet" href="/plugins/json/beautify-json.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
@endsection
@section('content')
    <div class="container">
        <form class="form-product" method="post" action="{{route('admin::profile-questions.post')}}" data-actions=".header-actions" autocomplete="off">
            <div class="header-actions">
                <button type="button" class="btn btn-discard margin-r-10">Discard</button>
                <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
            </div>
            {{csrf_field()}}
            <div class="h3 margin-t-10 bold">
                @isset($question) 
                    Edit #{{$question['id']}}
                    <input type="hidden" name="id" value="{{ $question['id'] }}" />
                @else
                    Add New Question
                @endisset
            </div>

            <div class="card margin-t-20">
                <div>
                    <label class="label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{!empty($question) ? $question['name'] : ""}}" required />
                    <span class="error error-name"></span>
                </div>

                <div class="flex gutter-between margin-t-20">
                    <div class="col-3-12 padding-10">
                        <label class="label">Type</label>
                        <select class="form-control" name="type" required>
                            <option value="dropdown" default selected {{ !empty($question) ? ($question['type'] == 'dropdown' ? "selected" : "") : ""}}>Dropdown</option>
                            <option value="checkbox" {{ !empty($question) ? ($question['type'] == 'checkbox' ? "selected" : "") : ""}}>Checkbox</option>
                        </select>
                        <span class="error error-type"></span>
                    </div>

                    <div class="col-3-12 padding-10">
                        <label class="label">Status</label>
                        <select class="form-control" name="status" required>
                            <option value="1" default selected {{ !empty($question) ? ($question['status'] == 1 ? "selected" : "") : ""}}>ACTIVE</option>
                            <option value="0" {{ !empty($question) ? ($question['status'] == 0 ? "selected" : "") : ""}}>In-ACTIVE</option>
                        </select>
                        <span class="error error-status"></span>
                    </div>

                    <div class="col-3-12 padding-10">
                        <label class="label">Priority</label>
                        <input type="number" name="priority" class="form-control" value="{{!empty($question) ? $question['priority'] : ""}}" required />
                        <span class="error error-priority"></span>
                    </div>

                    <div class="col-3-12 padding-10">
                        <label class="label">Is Mandatory?</label>
                        <select class="form-control" name="is_mandatory" required>
                            <option value="1" default selected {{ !empty($question) ? ($question['is_mandatory'] == 1 ? "selected" : "") : ""}}>YES</option>
                            <option value="0" {{ !empty($question) ? ($question['is_mandatory'] == 0 ? "selected" : "") : ""}}>NO</option>
                        </select>
                        <span class="error error-is_mandatory"></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="/plugins/json/jquery.beautify-json.js"></script>
@endsection
