@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Profile Questions
            </div>
            <div>
                 <a href="{{route('admin::profile-questions.edit')}}"> <button class="btn btn-primary">Add new question</button></a>
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection
