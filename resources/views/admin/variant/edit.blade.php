@extends('admin.master')

@section('content')
    <div class="container">
        <form class="form" method="post" action="{{route('admin::products.variants.update', ['productId' => $product->id])}}">
            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{$variant->id}}" />
            <div class="flex gutter-between">
                <div>
                    <div class="regular light-grey">
                        <a href="{{route('admin::products.edit', $product->id)}}"><i class="fa fa-angle-left" aria-hidden="true"></i> {{$product->name}}</a>
                    </div>
                    <div class="h3 bold margin-t-10">
                        {{$variant->value}} : {{$variant->sku}}
                    </div>
                </div>

                <div>
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </div>
            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-4-12">
                        @include('admin.variant._sidebar')
                    </div>
                    <div class="col-8-12">
                        @include('admin.variant.form')
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
