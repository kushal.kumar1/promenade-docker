<div class="card">
    <div class="flex gutter-sm">
        <div class="col-4-12">
            <img src="{{Image::getSrc($product->primaryImage,75)}}" />
        </div>
        <div class="col-8-12">
            <div class="h6">
                {{$product->name}}
            </div>
            <div class="margin-t-10">
                {{$product->variants->count()}} variants
            </div>
        </div>
    </div>
</div>

@if($product->variants->isNotEmpty())
    <div class="margin-t-30">
        <div class="card">
            <h2 class="h5 bold">Variants</h2>
            <hr class="hr-light margin-t-20" />
            @foreach($product->variants as $key => $variant)
                <div>
                    <a href="{{route('admin::products.variants.edit', ['productId' => $variant->product_id,'varaintId' => $variant->id])}}"><div class="margin-t-20">
                        <span class="bold">{{$variant->value}}</span> : {{$variant->sku}} : Rs {{$variant->price}}
                    </div></a>
                    @if($key != $product->variants->count()-1)
                        <hr class="hr-light margin-t-20" />
                    @endif
                </div>
            @endforeach
        </div>
    </div>
@endif
