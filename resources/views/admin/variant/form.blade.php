<div class="card">
    <h2 class="h5 bold">Options & Pricing</h2>
    <div class="margin-t-30">
        <div class="flex gutter-sm">
            <div class="col-6-12">
                <label class="label">Type</label>
                @if(isset($variant))
                  <input class="form-control" type="text" value="{{$variant->value}}" readonly name="value"/>
                @else
                <select class="form-control" name="value">
                  @foreach($types as $type)
                    <option value="{{$type}}" {{(isset($variant) && $variant->value == $type) ? "selected":"" }}>
                      {{$type}}
                    </option>
                  @endforeach
                </select>
                @endif
            </div>
            <div class="col-6-12">
                <label class="label">SKU</label>
                <input class="form-control" type="text" name="sku" value="{{isset($variant) ? $variant->sku : " " }}" readonly/>
            </div>
        </div>

        <div class="margin-t-20">
            <div class="flex gutter-sm">
                <div class="col-6-12">
                    <label class="label">MRP</label>
                    <input class="form-control" type="text" name="mrp" value="{{isset($variant) ? $variant->mrp : " " }}"/>
                </div>
                <div class="col-6-12">
                    <label class="label">PRICE</label>
                    <input class="form-control" type="text" name="price" value="{{isset($variant) ? $variant->price : " " }}"/>
                </div>
            </div>
        </div>

        <div class="margin-t-20">
            <div class="flex gutter-sm">
                <div class="col-6-12">
                    <label class="label">Min Price</label>
                    <input class="form-control" type="text" name="min_price" value="{{isset($variant) ? (($variant->min_price == 0) ? $variant->price : $variant->min_price) : " " }}"/>
                </div>
                <div class="col-6-12">
                    <label class="label">Max Price</label>
                    <input class="form-control" type="text" name="max_price" value="{{isset($variant) ? (($variant->max_price == 0) ? $variant->price : $variant->max_price) : " " }}"/>
                </div>
            </div>
        </div>

        <div class="margin-t-20">
          <div class="flex gutter-sm">
              {{-- <div class="col-6-12">
                  <label class="label">UOM</label>
                  <select class="form-control" name="uom">
                    @foreach($measureUnits as $uom)
                      <option value="{{$uom}}" {{(isset($variant) && $variant->uom == $uom) ? "selected":"" }}>
                        {{$uom}}
                      </option>
                    @endforeach
                  </select>
              </div> --}}
              <div class="col-6-12">
                  <label class="label">Quantity</label>
                  <input class="form-control" type="text" name="quantity" value="{{isset($variant) ? $variant->quantity : " " }}"/>
              </div>
          </div>
        </div>
        <div class="margin-t-20">
          <div class="flex gutter-sm">
            <div class="col-6-12">
                <label class="label">MOQ</label>
                <input class="form-control" type="text" name="moq" value="{{isset($variant) ? $variant->moq : 1 }}"/>
            </div>
          </div>
        </div>
    </div>
</div>
