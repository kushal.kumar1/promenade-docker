@extends('admin.master')

@section('content')

    <div class="container">
        <div class="h1">
            Shipments
        </div>
        @include('admin.grid')
    </div>

@endsection
