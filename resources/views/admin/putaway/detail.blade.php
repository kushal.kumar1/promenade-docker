@extends('admin.master')

@section('content')

    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::putaway.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Putaway</a>
        </div>
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    <div class="flex cross-center">
                        <h1 class="h1 margin-r-20 bold">#{{$putaway->id}}</h1>
                        <div class="tag margin-r-10 uppercase">{{$putaway->status}}</div>
                    </div>
                    <div class="regular grey-light margin-t-10">
                        Created At: {{(new DateTime($putaway->created_at))->format('F d, Y \a\t h:i A')}}
                    </div>
                    <div class="regular grey-light margin-t-10">
                        Created By: {{$putaway->employee->name}}
                    </div>
                </div>
                <div>
                    @if($putaway->status == \Niyotail\Models\Putaway\Putaway::STATUS_OPEN)
                        <form class="form" method="post" action="{{route('admin::putaway.close', $putaway->id)}}">
                            <button type="submit" class="btn btn-danger">
                                Close
                            </button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        @if($putaway->status == \Niyotail\Models\Putaway\Putaway::STATUS_OPEN)
        <div class="flex gutter-sm">
            <div class="col-12-12">
                <div class="card margin-t-30">
                    <h2 class="bold">ADD ITEM</h2>
                    <form class="form" method="post"
                          action="{{route('admin::putaway.put-item',['id' => $putaway->id])}}" autocomplete="off">
                        <div class="margin-t-20">
                            <div class="flex gutter">
                                <div class="col-12-12">
                                    <label class="label">Search Product by barcode</label>
                                    <select class="form-control product-search-js" name="product_id"></select>
                                </div>
                                <div class="col-12-12">
                                    <label class="label">Storage</label>
                                    <select class="form-control storages-search" name="storage_id"></select>
                                </div>
                                <div class="col-12-12">
                                    <label class="label">Quantity in units</label>
                                    <input class="form-control" type="text" name="quantity"/>
                                </div>
                                <div class="col-12-12">
                                    <button class="btn btn-primary">ADD</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            <div class="col-12-12">
                <div id="grid-container">
                    @include('admin.grid')
                </div>
            </div>
        </div>

    </div>
@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/putaway.js')}}"></script>
@endsection
