@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Putaway List
            </div>
            <div>
                <form class="form" method="post" action="{{route('admin::putaway.create')}}" data-destination="{{route('admin::putaway.detail', ['id' => '#id#'])}}">
                    <button class="btn btn-primary">Initiate</button>
                </form>
            </div>
        </div>
        @include('admin.grid')
    </div>
@endsection
