@extends('admin.master')

@section('content')

@if (!empty($vendor))
    <form class="form" action="{{route('admin::vendors.update', ['id' => $vendor->id])}}" method="POST">
@else 
    <form class="form" action="{{route('admin::vendors.create')}}" method="POST" data-destination="{{route("admin::vendors.index")}}">
@endif
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::vendors.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to All Vendors</a>
        </div>
        
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    @if (!empty($vendor))
                        <h1 class="h1 margin-r-20 bold">{{$vendor->name}}</h1>
                    @else 
                        <h1 class="h1 margin-r-20 bold">Create a new vendor</h1>
                    @endif
                </div>

                <div>
                    <button type="submit" class="btn btn-success">
                        @if(!empty($vendor))
                            Update
                        @else
                            Create
                        @endif
                    </button>
                </div>
            </div>
        </div>

        <div class="flex gutter">
            <div class="col-7-12 margin-t-30">
                <div class="card">
                    <div class="flex gutter">
                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Name</label>
                                <input class="form-control" name="name" value="{{!empty($vendor) && isset($vendor)? $vendor->name : ""}}" />
                                <span class="error error-name"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Trading Name</label>
                                <input class="form-control" name="trade_name" value="{{!empty($vendor) && isset($vendor)? $vendor->trade_name : ""}}" />
                                <span class="error error-trade_name"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Legal Name</label>
                                <input class="form-control" name="legal_name" value="{{!empty($vendor) && isset($vendor)? $vendor->legal_name : ""}}" />
                                <span class="error error-legal_name"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Nickname</label>
                                <input class="form-control" name="nickname" value="{{!empty($vendor) && isset($vendor)? $vendor->nickname : ""}}" />
                                <span class="error error-nickname"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-5">
                                <label class="label">Incoorporation Type</label>
                                <select class="form-control" name="incorporation_type" value="{{!empty($vendor) && isset($vendor->incorporation_type) ? $vendor->incorporation_type : ""}}" />
                                    <option default disabled selected>Select one of the following.</option>
                                    @foreach ($incorporationTypeSelect as $type)
                                        <option value="{{ $type }}" @if (!empty($vendor) && $vendor->incorporation_type === $type) selected @endif>{{ $type }}</option>
                                    @endforeach
                                </select>
                                <span class="error error-incorporation_type"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-5">
                                <label class="label">Type</label>
                                <select class="form-control" name="type" value="{{!empty($vendor) && isset($vendor->type) ? $vendor->type : ""}}" />
                                    <option default disabled selected>Select one of the following.</option>
                                    @foreach ($labelSelect as $type)
                                        <option value="{{ $type }}" @if (!empty($vendor) && $vendor->type === $type) selected @endif>{{ $type }}</option>
                                    @endforeach
                                </select>
                                <span class="error error-incorporation_type"></span>
                            </div>
                        </div>

                       <div class="col-6-12">
                            <div class="margin-t-5">
                                <label class="label">Status</label>
                                <select class="form-control" name="status" value="{{!empty($vendor) && isset($vendor->status) ? $vendor->status : ""}}" />
                                    <option default disabled selected>Select one of the following.</option>
                                    <option value="1" @if (!empty($vendor) && $vendor->status === 1) selected @endif>Active</option>
                                    <option value="0" @if (!empty($vendor) && $vendor->status === 0) selected @endif>Disable</option>
                                </select>
                                <span class="error error-incorporation_type"></span>
                            </div>
                        </div>
                       
                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">GSTIN</label>
                                <input class="form-control" name="GSTIN" value="{{!empty($vendor) && isset($vendor->GSTIN)? $vendor->GSTIN : ""}}" />
                                <span class="error error-GSTIN"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">PAN</label>
                                <input class="form-control" name="PAN" value="{{!empty($vendor) && isset($vendor->PAN)? $vendor->PAN : ""}}" />
                                <span class="error error-PAN"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Website</label>
                                <input class="form-control" name="website" value="{{!empty($vendor) && isset($vendor->website)? $vendor->website : ""}}" />
                                <span class="error error-website"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Email</label>
                                <input class="form-control" name="email" value="{{!empty($vendor) && isset($vendor->email)? $vendor->email : ""}}" />
                                <span class="error error-email"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-5-12 margin-t-30">
                <div class="card">
                    <div class="flex gutter">
                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Contact Person</label>
                                <input class="form-control" name="contact_person" value="{{!empty($vendor) && isset($vendor->contact_person)? $vendor->contact_person : ""}}" />
                                <span class="error error-contact_person"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Contact Phone</label>
                                <input class="form-control" name="contact_phone" value="{{!empty($vendor) && isset($vendor->contact_phone)? $vendor->contact_phone : ""}}" />
                                <span class="error error-contact_phone"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card hidden margin-t-30">
                    <div class="flex gutter">
                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Bank Name</label>
                                <input class="form-control" name="bank_name" value="{{!empty($vendor) && isset($vendor->bank_name)? $vendor->bank_name : ""}}" />
                                <span class="error error-bank_name"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">IFSC Code</label>
                                <input class="form-control" name="ifsc_code" value="{{!empty($vendor) && isset($vendor->ifsc_code)? $vendor->ifsc_code : ""}}" />
                                <span class="error error-ifsc_code"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Account Number</label>
                                <input class="form-control" name="account_number" value="{{!empty($vendor) && isset($vendor->account_number)? $vendor->account_number : ""}}" />
                                <span class="error error-account_number"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Account Type</label>
                                <input class="form-control" name="account_type" value="{{!empty($vendor) && isset($vendor->account_type)? $vendor->account_type : ""}}" />
                                <span class="error error-account_type"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Credit Days</label>
                                <input class="form-control" name="credit_days" value="{{!empty($vendor) && isset($vendor->credit_days)? $vendor->credit_days : ""}}" />
                                <span class="error error-credit_days"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Credit Limit</label>
                                <input class="form-control" name="credit_limit" value="{{!empty($vendor) && isset($vendor->credit_limit)? $vendor->credit_limit : ""}}" />
                                <span class="error error-credit_limit"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/user.js')}}"></script>
@endsection
