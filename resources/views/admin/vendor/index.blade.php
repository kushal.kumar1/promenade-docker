@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Vendors
            </div>
            <div>
                <a class="btn btn-primary" href="{{route('admin::vendors.add')}}">Add Vendor</a>
            </div>
        </div>

        @include('admin.grid')
    </div>

@endsection
