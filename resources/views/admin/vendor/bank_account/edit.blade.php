@extends('admin.master')

@section('content')

<div class="container">
    <div class="regular light-grey">
        <a href="{{route('admin::vendors.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to All Vendors</a>
    </div>
    
    <div class="margin-t-10">
        <div class="flex gutter-between">
            <div>
                <h1 class="h1 margin-r-20 bold">{{$vendor->name}}</h1>
            </div>

            <div>
            </div>
        </div>
    </div>

    <div class="flex gutter margin-t-30">
        <div class="col-4-12">
            <div class="card address-card add-new-address" data-modal="#add-account">
                <button class="btn btn-primary">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add Account
                </button>
            </div>
        </div>

        @if (!empty ($accounts))
            @foreach ($accounts as $account)
                <div class="col-4-12">
                    <div class="card address-card">
                        <div class="address-card">
                            <h4><b>{{$account->name}}</b></h4>
                            <br />
                            <p>
                                {{$account->bank}}
                            </p>
                            <br />
                            <p>Number: {{$account->number}}</p>
                            <br />
                            <p>IFSC: {{$account->ifsc}}</p>
                            <br />
                            <br />

                            <div class="action">
                                <div class="flex">
                                    <button class="btn btn-success margin-t-5 margin-r-10" data-modal="#edit-account-{{$account->id}}">Edit</button>
                                    <form class="form" id="delete-account-form" method="post" action="{{route('admin::vendors.bank_account.delete', [
                                                'id' => $vendor->id,
                                                'accountId' => $account->id
                                            ])}}">
                                        <button type="button" class="btn btn-danger margin-t-5 delete-account">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="edit-account-{{$account->id}}" class="modal">
                    <div class="modal-content">
                        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h5 class="h6 title text-center">Edit Account</h5>

                        <form class="form" method="post" action="{{route('admin::vendors.bank_account.update', [
                                    'id' => $vendor->id,
                                    'accountId' => $account->id
                                ])}}" autocomplete="off">
                            {{csrf_field()}}

                            <div class="margin-t-30">
                                <div class="flex gutter">
                                    <div class="col-12-12">
                                        <div class="margin-t-15">
                                            <label class="label">Name</label>
                                            <input class="form-control" name="name" value="{{isset($account)? $account->name : ""}}" />
                                            <span class="error error-name"></span>
                                        </div>
                                    </div>

                                    <div class="col-6-12">
                                        <div class="margin-t-5">
                                            <label class="label">Status</label>
                                            <select class="form-control" name="status" value="{{isset($account)? $account->status : ""}}" />
                                                <option default disabled selected>Select one of the following.</option>
                                                <option value="1" @if (!empty ($account) && $account->status === 1) selected @endif>Active</option>
                                                <option value="0" @if (!empty ($account) && $account->status === 0) selected @endif>Disable</option>
                                            </select>
                                            <span class="error error-incorporation_type"></span>
                                        </div>
                                    </div>

                                    <div class="col-6-12">
                                        <div class="margin-t-5">
                                            <label class="label">Type</label>
                                            <select class="form-control" name="type" value="{{isset($account)? $account->type : ""}}" />
                                                <option default disabled selected>Select one of the following.</option>
                                                <option value="savings" @if (!empty ($account) && $account->type === "savings") selected @endif>Savings</option>
                                                <option value="current" @if (!empty ($account) && $account->type === "current") selected @endif>Current</option>
                                            </select>
                                            <span class="error error-type"></span>
                                        </div>
                                    </div>

                                    <div class="col-12-12">
                                        <div class="margin-t-15">
                                            <label class="label">Bank</label>
                                            <input class="form-control" name="bank" value="{{isset($account)? $account->bank : ""}}" />
                                            <span class="error error-bank"></span>
                                        </div>
                                    </div>

                                    <div class="col-12-12">
                                        <div class="margin-t-15">
                                            <label class="label">IFSC</label>
                                            <input class="form-control" name="ifsc" value="{{isset($account)? $account->ifsc : ""}}" />
                                            <span class="error error-ifsc"></span>
                                        </div>
                                    </div>

                                    <div class="col-12-12">
                                        <div class="margin-t-5">
                                            <label class="label">Account Number</label>
                                            <input class="form-control" name="number" type="password" value="{{isset($account)? $account->number : ""}}" id="account_number" />
                                            <span class="error error-number"></span>
                                        </div>
                                    </div>
                                    <div class="col-12-12">
                                        <div class="margin-t-5">
                                            <label class="label">Confirm Account Number</label>
                                            <input class="form-control" name="confirm_number" value="{{isset($account)? $account->number : ""}}" id="confirm_account_number"  />
                                            <span class="error error-number"></span>
                                        </div>
                                    </div>
                                    <div class="col-6-12">
                                        <div class="margin-t-15">
                                            <label class="label">ICICI Beneficiary ID</label>
                                            <input class="form-control" name="icici_beneficiary_id" value="{{isset($account)? $account->icici_beneficiary_id : ""}}" />
                                            <span class="error error-icici_beneficiary_id"></span>
                                        </div>
                                    </div>

                                    <div class="col-6-12">
                                        <div class="margin-t-15">
                                            <label class="label">ICICI Beneficiary Nickname</label>
                                            <input class="form-control" name="icici_beneficiary_nickname" value="{{isset($account)? $account->icici_beneficiary_nickname : ""}}" />
                                            <span class="error error-icici_beneficiary_nickname"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="flex gutter-between margin-t-40">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary button-success">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

<div id="add-account" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title text-center">Add Account</h5>

        <form class="form" method="post" action="{{route('admin::vendors.bank_account.create', ['id' => $vendor->id])}}" autocomplete="off">
            {{csrf_field()}}

            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <div class="margin-t-15">
                            <label class="label">Name</label>
                            <input class="form-control" name="name" />
                            <span class="error error-name"></span>
                        </div>
                    </div>

                    <div class="col-6-12">
                        <div class="margin-t-5">
                            <label class="label">Status</label>
                            <select class="form-control" name="status" />
                                <option default disabled selected>Select one of the following.</option>
                                <option value="1">Active</option>
                                <option value="0">Disable</option>
                            </select>
                            <span class="error error-incorporation_type"></span>
                        </div>
                    </div>

                    <div class="col-6-12">
                        <div class="margin-t-5">
                            <label class="label">Type</label>
                            <select class="form-control" name="type" />
                                <option default disabled selected>Select one of the following.</option>
                                <option value="savings">Savings</option>
                                <option value="current">Current</option>
                            </select>
                            <span class="error error-type"></span>
                        </div>
                    </div>

                    <div class="col-12-12">
                        <div class="margin-t-15">
                            <label class="label">Bank</label>
                            <input class="form-control" name="bank" />
                            <span class="error error-bank"></span>
                        </div>
                    </div>

                    <div class="col-12-12">
                        <div class="margin-t-15">
                            <label class="label">IFSC</label>
                            <input class="form-control" name="ifsc" />
                            <span class="error error-ifsc"></span>
                        </div>
                    </div>

                    <div class="col-12-12">
                        <div class="margin-t-5">
                            <label class="label"> Account Number</label>
                            <input class="form-control" name="number" type="password" id="account_number" />
                            <span class="error error-number"></span>
                        </div>
                    </div>

                    <div class="col-12-12">
                        <div class="margin-t-5">
                            <label class="label">Confirm Account Number</label>
                            <input class="form-control" name="confirm_number" id="confirm_account_number" />
                            <span class="error error-number"></span>
                        </div>
                    </div>

                    <div class="col-6-12">
                        <div class="margin-t-15">
                            <label class="label">ICICI Beneficiary ID</label>
                            <input class="form-control" name="icici_beneficiary_id" />
                            <span class="error error-icici_beneficiary_id"></span>
                        </div>
                    </div>

                    <div class="col-6-12">
                        <div class="margin-t-15">
                            <label class="label">ICICI Beneficiary Nickname</label>
                            <input class="form-control" name="icici_beneficiary_nickname" />
                            <span class="error error-icici_beneficiary_nickname"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Add</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/user.js')}}"></script>
    <script>
        $('.delete-account').on('click', function (e) {
            dialog(
                'Delete bank account',
                'Are you sure you want to delete bank account details',
                function() {
                    $(".delete-account").attr('disabled', false);
                    const $element = $('#delete-account-form'),
                        $form = $element.parent();
                    $form.submit();
                },
                function () {
                    $(".delete-account").attr('disabled', false);
                }
            );

        })

        $("#account_number,#confirm_account_number ").bind('cut copy paste', function(e) {
            e.preventDefault();
        });
    </script>
@endsection