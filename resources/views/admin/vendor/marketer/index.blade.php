@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                {{$vendor->name}} - Marketers
            </div>
            <div>
                <a class="btn btn-primary" href="{{route('admin::vendors.marketers.add', $vendor->id)}}">Add Marketer</a>
            </div>
        </div>

        @include('admin.grid')
    </div>

@endsection
