@extends('admin.master')

@section('content')


@if(!empty($map))
<form class="" action="{{route('admin::vendors.marketers.update', [$vendor->id, $marketer->id])}}" method="POST"
      enctype="multipart/form-data"
      data-destination="{{route('admin::vendors.marketers', $vendor->id)}}">
    @else
<form class="" enctype="multipart/form-data" action="{{route('admin::vendors.marketers.save', $vendor->id)}}" method="POST" data-destination="{{route('admin::vendors.marketers', $vendor->id)}}">
@endif
    @csrf
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::vendors.marketers', $vendor->id)}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to marketers</a>
        </div>
        
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <h2 class="h2">{{$vendor->name}}</h2>
            </div>
            <div class="flex gutter-between">
                @if(!empty($map))
                    <div>Update vendor marketer mapping</div>
                    <div>
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                    @else
                    <div>Add a new Marketer</div>
                    <div>
                        <button type="submit" class="btn btn-success">
                            Create
                        </button>
                    </div>
                @endif
            </div>
        </div>

        <div class="flex gutter">
            <div class="col-7-12 margin-t-30">
                <div class="card">
                    <div class="flex gutter">

                        <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                        <div class="col-12-12">
                            <label class="label margin-t-20">Marketer</label>
                            <select name="marketer_id" required class="form-control marketer">
                                @if(!empty($map))
                                <option value="{{$map->marketer_id}}">{{$map->marketer->name}}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-6-12">
                            <label class="label margin-t-20">Relation</label>
                            <select name="relation" required class="form-control">
                                <option value="">-- Select Vendor Marketer Relation --</option>
                                @foreach($relations as $relation)
                                    <option @if(!empty($map) && $map->relation == $relation) selected @endif value="{{$relation}}">{{$relation}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-6-12">
                            <label class="label margin-t-20">Channel</label>
                            <select name="channel" required class="form-control">
                                <option value="">-- Select Channel --</option>
                                @foreach($channels as $channel)
                                    <option @if(!empty($map) && $map->channel == $channel) selected @endif value="{{$channel}}">{{$channel}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Minimum order value (INR)</label>
                                <input class="form-control" required type="number"
                                       min="0"
                                       name="mov"
                                       @if(!empty($map)) value="{{$map->mov}}" @endif/>
                                <span class="error error-mpv"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Lead time (in days)</label>
                                <input class="form-control" required name="lead_time" type="number" min="0"
                                       @if(!empty($map)) value="{{$map->lead_time}}" @endif/>
                                <span class="error error-lead_time"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Credit Term</label>
                                <input class="form-control" required name="credit_term" type="number" min="0"
                                       @if(!empty($map)) value="{{$map->credit_term}}" @endif/>
                                <small class="font-secondary">0 = Advance (Default)<br> 999 = Bill to bill <br> Any other number for days</small>
                                <span class="error error-credit_term"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Cash discount</label>
                                <input class="form-control" required name="cash_discount" type="number" min="0"
                                       @if(!empty($map)) value="{{$map->cash_discount}}" @endif/>
                                <small class="font-secondary">0-100 for percentage <br> Default = 0</small>
                                <span class="error error-cash_discount"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">TOT Signed</label>
                                <select name="tot_signed" required class="form-control">
                                    <option @if(!empty($map) && $map->tot_signed == 0) selected @endif value="0">No</option>
                                    <option @if(!empty($map) && $map->tot_signed == 1) selected @endif value="1">Yes</option>
                                </select>
                                <span class="error error-cash_discount"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">TOT Signed Copy (Upload)</label>
                                <input type="file" name="tot_file" class="form-control">
                                @if(!empty($map) && !empty($map->tot_file))<a target="_blank" href="{{route('admin::vendors.marketers.download_tot', $map->id)}}">Download
                                 <small>{{$map->tot_file}}</small></a>@endif
                                <span class="error error-cash_discount"></span>
                            </div>
                        </div>

                        <div class="col-12-12">
                            <div class="margin-t-15">
                                <label class="label">Comments</label>
                                <textarea rows="3" class="form-control" name="comments">@if(!empty($map) && $map->cash_discount_terms == 1){{$map->cash_discount_terms}}@endif</textarea>
                                <span class="error error-cash_discount"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Vendor Priority</label>
                                <input class="form-control" required name="priority" type="number" min="1" max="5"
                                       @if(!empty($map)) value="{{$map->priority}}" @endif/>
                                <span class="error error-priority"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <label class="label">Status</label>
                            <select class="form-control" name="status">
                                <option @if(!empty($map) && $map->status == 1) selected @endif value="1">
                                    Active</option>
                                <option @if(!empty($map) && $map->status == 0) selected @endif value="0">
                                    Inactive</option>
                            </select>
                        </div>

                        <div class="col-4-12">
                            <label class="label">Date from</label>
                            <input type="date" class="date-picker form-control" name="date_from"
                                   @if(!empty($map)) value="{{\Carbon\Carbon::parse($map->date_from)->format('Y-m-d')}}" @endif>
                        </div>

                        <div class="col-4-12">
                            <label class="label">Date to</label>
                            <input type="date" class="date-picker form-control" name="date_to"
                                   @if(!empty($map) && !empty($map->date_to)) value="{{\Carbon\Carbon::parse($map->date_to)->format('Y-m-d')}}" @endif>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')


<script>
    class vendorMarketerMapClass {

        constructor() {
            enableMarketerSearch();
        }
    }


    function enableMarketerSearch()
    {
        return $('.marketer').select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/marketers',
                dataType: 'json',
                processResults: function(data) {
                    data = $.map(data, function(obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    }

    new vendorMarketerMapClass();
</script>

@endsection
