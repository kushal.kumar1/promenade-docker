@extends('admin.master')

@section('content')

<div class="container">
    <div class="regular light-grey">
        <a href="{{route('admin::vendors.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to All Vendors</a>
    </div>
    
    <div class="margin-t-10">
        <div class="flex gutter-between">
            <div>
                <h1 class="h1 margin-r-20 bold">{{$vendor->name}}</h1>
            </div>

            <div>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    @if ($message = Session::get('error'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif
    <div class="flex gutter margin-t-30">
        <div class="col-4-12">
            <div class="card address-card add-new-address" data-modal="#add-address">
                <button class="btn btn-primary">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Add Address
                </button>
            </div>        
        </div>

        @if (!empty ($addresses))
            @foreach ($addresses as $address)
                <div class="col-4-12">
                    <div class="card address-card">
                        <div class="address-card">
                            <h4><b>{{isset($vendor)? $address->name : ""}}</b></h4>
                            <br />
                            <p>
                                {{isset($vendor)? $address->address : ""}}, 
                                {{isset($vendor)? $address->city : ""}}, 
                                {{isset($vendor)? $address->state : ""}}
                                ({{isset($vendor)? $address->pincode : ""}})
                            </p>
                            <br />
                            <span>State Code: {{isset($vendor)? $address->state_code : ""}}</span>
                            <br />
                            <br />
                            <span>GSTIN: {{isset($vendor)? $address->gstin : ""}}</span>

                            <div class="action">
                                <div class="flex">
                                    <button class="btn btn-success margin-t-5 margin-r-10" data-modal="#edit-address-{{$address->id}}">Edit</button>
                                    <form class="form" id="delete-address-form" method="post" action="{{route('admin::vendors.address.delete', [
                                                'id' => $vendor->id,
                                                'addressId' => $address->id
                                            ])}}">
                                        <button type="button" class="btn btn-danger margin-t-5 delete-address">Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="edit-address-{{$address->id}}" class="modal">
                    <div class="modal-content">
                        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h5 class="h6 title text-center">Edit Address</h5>

                        <form class="form" method="post" action="{{route('admin::vendors.address.update', [
                                'id' => $vendor->id,
                                'addressId' => $address->id
                            ])}}" autocomplete="off">
                            {{csrf_field()}}

                            <div class="margin-t-30">
                                <div class="flex gutter">
                                    <div class="col-12-12">
                                        <label class="label">Name</label>
                                        <input class="form-control" name="name" value="{{ $address->name ?? "" }}" />
                                    </div>

                                    <div class="col-12-12">
                                        <label class="label">Address</label>
                                        <input class="form-control" name="address" value="{{ $address->address ?? "" }}" />
                                    </div>
                                    
                                    <div class="col-12-12">
                                        <label class="label">GSTIN</label>
                                        <input class="form-control" name="gstin" value="{{ $address->gstin ?? "" }}" />
                                    </div>

                                    <div class="col-6-12">
                                        <label class="label">Pincode</label>
                                        <input class="form-control" name="pincode" value="{{ $address->pincode ?? "" }}"/>
                                    </div>

                                    <div class="col-6-12">
                                        <label class="label">City</label>
                                        <input class="form-control" name="city" value="{{ $address->city ?? "" }}"/>
                                    </div>

                                    <div class="col-6-12">
                                        <label class="label">State</label>
                                        <input class="form-control" name="state" value="{{ $address->state ?? "" }}"/>
                                    </div>

                                    <div class="col-6-12">
                                        <label class="label">State Code</label>
                                        <input class="form-control" name="state_code" value="{{ $address->state_code ?? "" }}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="flex gutter-between margin-t-40">
                                <button  type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary button-success">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

<div id="add-address" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title text-center">Add Address</h5>

        <form class="form" method="post" action="{{route('admin::vendors.address.create', ['id' => $vendor->id])}}" autocomplete="off">
            {{csrf_field()}}

            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Name</label>
                        <input class="form-control" name="name" />
                    </div>

                    <div class="col-12-12">
                        <label class="label">Address</label>
                        <input class="form-control" name="address" />
                    </div>
                    
                    <div class="col-12-12">
                        <label class="label">GSTIN</label>
                        <input class="form-control" name="gstin" />
                    </div>

                    <div class="col-6-12">
                        <label class="label">Pincode</label>
                        <input class="form-control" name="pincode"/>
                    </div>

                    <div class="col-6-12">
                        <label class="label">City</label>
                        <input class="form-control" name="city"/>
                    </div>

                    <div class="col-6-12">
                        <label class="label">State</label>
                        <input class="form-control" name="state"/>
                    </div>

                    <div class="col-6-12">
                        <label class="label">State Code</label>
                        <input class="form-control" name="state_code"/>
                    </div>
                </div>
            </div>

            <div class="flex gutter-between margin-t-40">
                <button  type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Add</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/user.js')}}"></script>
    <script>
        $('.delete-address').on('click', function (e) {
            dialog(
                'Delete address',
                'Are you sure you want to delete the address',
                function() {
                    $(".delete-address").attr('disabled', false);
                    $('#delete-address-form').submit();
                },
                function () {
                    $(".delete-address").attr('disabled', false);
                }
            );

        })
    </script>
@endsection
