@extends('admin.master')

@section('content')

<div class="container">
    <div class="regular light-grey">
        <a href="{{route('admin::agents.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Agents</a>
    </div>
    <div class="margin-t-10">
        <div class="flex gutter-between">
            <div>
                <h1 class="h1 margin-r-20 bold">{{$agent->name}}</h1>
            </div>
            <div>
                <form class="form" action="{{route('admin::agents.status.update')}}" method="post">
                    <input type="hidden" name="id" value="{{$agent->id}}">
                    @if ($agent->status == 1)
                    <input type="hidden" name="status" value="0">
                    <button type="button" class="btn btn-danger agent-disable"><i class="fa fa-ban" aria-hidden="true"></i>
                        Disable Account
                    </button>
                    @else
                    <input type="hidden" name="status" value="1">
                    <button type="button" class="btn btn-success agent-enable">Enable Account</button>
                    @endif
                </form>
            </div>
        </div>
    </div>
    <div class="margin-t-30">
        <div class="flex gutter">
            <div class="col-8-12">
                <div class="card">
                    <div class="flex gutter">
                        <div class="col-2-12">
                            <img src="/sample.png" />
                        </div>
                        <div class="col-10-12">
                            <div class="flex gutter-between">
                                <div class="h5 bold">{{$agent->name}}</div>
                                <div>
                                    <a data-modal="#edit-agent" class="btn btn-sm">Edit Profile</a>
                                </div>
                            </div>
                            <div class="margin-t-5">
                                {{$agent->mobile}}
                            </div>
                            <div class="margin-t-5">
                                {{$agent->email}}
                            </div>
                            <div class="margin-t-5">
                                <span class="bold">Agent Type:</span> {{ucfirst($agent->type)}}
                            </div>
                            <div class="margin-t-5">
                                <span class="bold">Created At:</span> {{$agent->created_at}}
                            </div>
                            @if(!empty($agent->otp))
                              <div class="margin-t-5">
                                <span class="bold">OTP:</span> {{$agent->otp}}
                              </div>
                            @endif
                            <div class="margin-t-20">
                                @if($agent->verified)
                                    <div class="inline small tag tag-success"><i class="fa fa-check" aria-hidden="true"></i> Verified
                                    </div>
                                    @else
                                    <div class="inline small tag tag-warning"><i class="fa fa-times" aria-hidden="true"></i> OTP Verified
                                    </div>
                                    @endif
                                    @if($agent->status)
                                        <div class="inline small tag tag-success"><i class="fa fa-check" aria-hidden="true"></i> Active
                                        </div>
                                        @else
                                        <div class="inline small tag tag-warning"><i class="fa fa-times" aria-hidden="true"></i> Inactive
                                        </div>
                                        @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="margin-t-20">
                    <div class="card">
                        <div class="flex gutter-between">
                            <div class="h5 bold">Beat Marketer Matrix</div>
                            <button class="btn btn-sm btn-primary" data-modal="#add-matrix">Add</button>
                        </div>
                        @if($agent->beatMarketers->isNotEmpty())
                            <div class="margin-t-30">
                                <div class="flex gutter-sm">
                                    <div class="col-1-12 bold">
                                        Id
                                    </div>
                                    <div class="col-2-12 bold">
                                        Beat
                                    </div>
                                    <div class="col-5-12 bold">
                                        Marketer
                                    </div>
                                    <div class="col-4-12 bold">
                                        Action
                                    </div>
                                    @foreach($agent->beatMarketers as $map)
                                        <div class="col-1-12">
                                            {{$map->id}}
                                        </div>
                                        <div class="col-2-12">
                                            {{$map->beat->name}}
                                        </div>
                                        <div class="col-5-12">
                                            {{$map->marketer->name}}
                                        </div>
                                        <div class="col-4-12">
                                            <form class="form" action="{{route('admin::agents.map.delete')}}" method="post">
                                                <input class="hidden" name="id" value="{{$map->id}}" />
                                                <input class="hidden" name="agent_id" value="{{$agent->id}}" />
                                                <button class="btn btn-sm btn-remove-map" type="button">Remove</button>
                                            </form>
                                        </div>
                                        @endforeach
                                </div>
                            </div>
                        @else
                            <div class="margin-t-20">
                                No Records Found!
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="edit-agent" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h6 title text-center">Edit Agent</h5>
            <form class="form" method="post" action="{{route('admin::agents.update')}}" autocomplete="off">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$agent->id}}" />
                @include('admin.agent._form')
            </form>
        </div>
    </div>

    <div id="add-matrix" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h6 title text-center">Add Beat & Marketers</h5>
            <form class="form" method="post" action="{{route('admin::agents.map.update')}}" autocomplete="off">
                {{csrf_field()}}
                <input type="hidden" name="agent_id" value="{{$agent->id}}" />
                <div class="margin-t-20">
                    <label class="label">Beat</label>
                    <select class="form-control beat-search" name="beat_id"></select>
                </div>
                <div class="margin-t-20">
                    <label class="label">Marketers</label>
                    <select class="form-control search-beat-marketers" name="marketers[]" disabled></select>
                </div>
                <div class="flex gutter-between margin-t-40">
                    <button class="btn" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-primary button-success">Save</button>
                </div>
            </form>
        </div>
    </div>
    @endsection

    @section('js')
    <script type="text/javascript" src="{{mix('admin/js/agent.js')}}"></script>
    @endsection
