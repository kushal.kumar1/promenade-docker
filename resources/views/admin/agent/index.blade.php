@extends('admin.master')

@section('content')

<div class="container">
  <div class="flex gutter-between">
    <div class="h1">
      Agents
    </div>
    <div>
      <button class="btn btn-primary" data-modal="#add-agent">Add Agent</button>
    </div>
  </div>
  <div id="add-agent" class="modal">
    <div class="modal-content">
      <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
      <h5 class="h6 title text-center">Add Agent</h5>
      <form class="form" method="post" action="{{route('admin::agents.store')}}" autocomplete="off">
        {{csrf_field()}}
        @include('admin.agent._form')
      </form>
    </div>
  </div>
  @include('admin.grid')
</div>

@endsection
