<div class="margin-t-30">
  <div class="flex gutter">
    <div class="col-12-12">
      <label class="label">Name</label>
      <input class="form-control" name="name" value="{{!empty($agent) ? $agent->name : ""}}"/>
      <span class="error error-name"></span>
    </div>
    <div class="col-6-12">
      <label class="label">Code</label>
      <input class="form-control" name="code" value="{{!empty($agent) ? $agent->code : ""}}"/>
      <span class="error error-code"></span>
    </div>
    <div class="col-6-12">
      <label class="label">Agent Type</label>
      <select name="type" class="form-control payment-method">
        @if(empty($agent))
          <option value="">
            Select Agent Type
          </option>
        @endif
        @foreach ($agentTypes as $agentType)
          <option value="{{$agentType}}" {{!empty($agent) && ($agentType == $agent->type) ? 'selected' : ''}}>
            {{$agentType}}
          </option>
        @endforeach
      </select>
      <span class="error error-type"></span>
    </div>
  </div>
</div>
<div class="margin-t-25">
  <label class="label">Mobile</label>
  <input class="form-control mobile-country-picker" name="mobile" value="{{!empty($agent) ? $agent->mobile : ""}}"/>
  <span class="error error-mobile"></span>
</div>
<div class="margin-t-25">
  <label class="label">Email</label>
  <input class="form-control" name="email" value="{{!empty($agent) ? $agent->email : ""}}"/>
  <span class="error error-email"></span>
</div>
<div class="flex gutter-between margin-t-40">
  <button class="btn" data-dismiss="modal">Cancel</button>
  <button class="btn btn-primary button-success">Save</button>
</div>
