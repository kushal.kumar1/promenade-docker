<div id="modal-stores" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Choose Store</h1>
        <div class="margin-t-20">
            <label class="label">Select A Store</label>
            <select id="select2-stores" class="form-control" style="width:100%;"></select>
            <button id="button-open-cart" class="btn btn-primary margin-t-20">Open Return Cart</button>
        </div>
    </div>
</div>

@section('js')
<script>
    function storeTemplate(store) {
        if (!store.id) {
            return store.text;
        }
        let html = '<div class="padding-10">';
        html += `<h5 class="h6 bold">${store.name}</h5>`;
        html += `<h6 class="margin-t-5">${store.address}</h6>`;
        html += '</div>';

        return $(html);
    };

    function storeSelection(store){
        return store.name + ", " + store.address;
    }
    $('#select2-stores').select2({
        minimumInputLength: 2,
        ajax: {
            url: '{{route("admin::search.stores")}}',
            dataType: 'json',
            processResults: (data) => { return { results: data }; }
        },
        templateResult: storeTemplate,
        templateSelection: storeSelection
    });

    $('#button-open-cart').on('click', function(){
        let cartLink = '{{route("admin::stores.return-cart", ["id" => "#"])}}';
        const stores = $('#select2-stores').select2('data');
        if (stores.length > 0) {
            window.location = cartLink.replace("#", stores[0].id);
        }
    });
</script>
@append
