@extends('admin.master')

@section('content')

    <div class="container">
        <div class="h1">
            Order Items
        </div>
        @include('admin.grid')
    </div>

@endsection
