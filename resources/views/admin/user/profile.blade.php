@extends('admin.master')

@section('content')

    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::users.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Users</a>
        </div>
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    <h1 class="h1 margin-r-20 bold">{{$user->name}}</h1>

                </div>
                <div>
                    <form class="form" action="{{route('admin::users.status.update')}}" method="post">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        @if ($user->status == 1)
                            <input type="hidden" name="status" value="0">
                            <button type="button" class="btn btn-danger user-disable"><i class="fa fa-ban"
                                                                                             aria-hidden="true"></i>
                                Disable Account
                            </button>
                        @else
                            <input type="hidden" name="status" value="1">
                            <button type="button" class="btn btn-success user-enable">Enable Account</button>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="flex gutter">
            <div class="col-8-12">
                <div class="card margin-t-30">
                    <div class="flex gutter">
                        <div class="col-2-12">
                            <img src="/sample.png"/>
                        </div>
                        <div class="col-10-12">
                            <div class="flex gutter-between">
                                <div class="h5 bold">{{$user->name}}</div>
                                <div>
                                    <a data-modal="#edit-user" class="btn btn-sm">Edit Profile</a>
                                </div>
                            </div>
                            <div class="margin-t-5">
                                {{$user->mobile}}
                            </div>
                            <div class="margin-t-5">
                                {{$user->email}}
                            </div>
                            @if($user->verified)
                                <div class="inline small tag tag-success margin-t-5"><i class="fa fa-check"
                                                                                        aria-hidden="true"></i> Verified
                                </div>
                            @else
                                <div class="inline small tag tag-warning margin-t-5"><i class="fa fa-times"
                                                                                        aria-hidden="true"></i> OTP
                                    Verified
                                </div>
                            @endif
                        </div>
                        @if(!empty($user->otp))
                          <div>
                            OTP: {{$user->otp}}
                          </div>
                        @endif
                        <div class="col-12-12">
                            <form class="form" action="{{route('admin::users.notes.save')}}" method="post"
                                  data-destination="false">
                                <input type="hidden" name="id" value="{{$user->id}}">
                                <label class="label">Customer Note</label>
                                <textarea id="user-note" class="form-control" name="notes" placeholder="Add a note"
                                          rows="3">{{$user->notes}}</textarea>
                            </form>
                        </div>
                    </div>
                </div>
                @include('admin.user._recent_orders')
            </div>
            <div class="col-4-12">
                <div class="card margin-t-30">
                  <div class="h5 bold">
                      Stores
                  </div>
                    @if($user->stores->isNotEmpty())
                        @foreach($user->stores as $store)
                            <div class="margin-t-20">
                                <a href="{{route('admin::stores.profile', $store->id)}}" class="link uppercase">{{$store->name}}</a>
                            </div>

                            <div class="margin-t-5">
                                <span>{{$store->legal_name}}</span>
                              <div>
                                 {{$store->address}}
                              </div>
                              <div>
                                {{$store->pincode}}
                              </div>
                            </div>
                          @if(!$loop->last)
                            <hr class="hr hr-light margin-t-10" />
                          @endif
                        @endforeach
                    @else
                        <div class="margin-t-20">
                            This user is not associated with any store!
                        </div>
                    @endif
                </div>
                <div class="card margin-t-20">
                    <div class="h5 bold">
                        Badges
                    </div>
                    <form id="form-badges" class="form"
                          action="{{route('admin::users.badges.update', ['id' => $user->id])}}" method="post"
                          data-destination="false">
                        <div class="margin-t-20">
                            <select class="form-control badges-select" name="badges[]" multiple style="width:100%">
                                @foreach ($user->badges as $badge)
                                    <option value="{{$badge->id}}" selected>{{$badge->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('admin.user._edit_user')
    <div id="modal-store" class="modal">
        <div class="modal-content">
            <span class="close" data-close="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <div class="store-form-container">
            </div>
        </div>
    </div>
    @include('admin.templates.store')
@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/user.js')}}"></script>
@endsection
