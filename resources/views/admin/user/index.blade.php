@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Users
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#add-user">Add User</button>
            </div>
        </div>
        <div id="add-user" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 title text-center">Add Customer</h5>
                <form class="form" method="post" action="{{route('admin::users.store')}}" autocomplete="off">
                    {{csrf_field()}}
                    <div class="margin-t-30">
                        <div class="flex gutter">
                            <div class="col-6-12">
                                <label class="label">First Name</label>
                                <input class="form-control" name="first_name" />
                            </div>
                            <div class="col-6-12">
                                <label class="label">Last Name</label>
                                <input class="form-control" name="last_name" />
                            </div>
                        </div>
                    </div>
                    <div class="margin-t-25">
                        <label class="label">Mobile</label>
                        <input class="form-control mobile-country-picker" name="mobile"/>
                    </div>
                    <div class="margin-t-25">
                        <label class="label">Email</label>
                        <input class="form-control" name="email"/>
                    </div>
                    <div class="flex gutter-between margin-t-40">
                        <button class="btn" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection
