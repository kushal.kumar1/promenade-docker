<div id="edit-user" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title text-center">Edit Customer</h5>
        <form class="form" method="post" action="{{route('admin::users.update')}}" autocomplete="off">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$user->id}}" />
            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">First Name</label>
                        <input class="form-control" name="first_name" value="{{$user->first_name}}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Last Name</label>
                        <input class="form-control" name="last_name" value="{{$user->last_name}}"/>
                    </div>
                </div>
            </div>
            <div class="margin-t-25">
                <label class="label">Mobile</label>
                <input class="form-control mobile-country-picker" type="text" name="mobile" value="{{$user->mobile}}"/>
            </div>
            <div class="margin-t-25">
                <label class="label">Email</label>
                <input class="form-control" name="email" value="{{$user->email}}"/>
            </div>
            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn button-reject" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Update</button>
            </div>
        </form>
    </div>
</div>
