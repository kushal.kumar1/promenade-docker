<nav class="sidebar bg-primary">
  <div class="sidebar-header bg-primary-dark white flex gutter-between cross-center padding-h-30 no-wrap">
    Niyotail
    <span class="small pointer sidebar-close visible-sm"><i class="fa fa-times" aria-hidden="true"></i></span>
  </div>
  <ul class="list sidebar-menu margin-t-20 menu">
    @canany(['view-business-dashboard','view-operations-dashboard'])
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu" >
        <div class="col-10-12">
          Dashboards
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        @can('view-business-dashboard')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::dashboard')}}">
            <div class="col-10-12">
              Business
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-briefcase" aria-hidden="true"></i>
          </span>
          </a>
        </li>
        @endcan
        @can('view-operations-dashboard')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::dashboard.ops')}}">
            <div class="col-10-12">
              Operations
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-wrench" aria-hidden="true"></i>
          </span>
          </a>
        </li>
        @endcan
        {{-- @can('view-business-dashboard')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::dashboard.agents')}}">
            <div class="col-10-12">
              Agent
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-users" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan --}}
          @can('view-brands-dashboard')
            <li>
              <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::dashboard.brands')}}">
                <div class="col-10-12">
                  Brands Performance
                </div>
                <span class="col-2-12 icon">
              <i class="fa fa-star" aria-hidden="true"></i>
            </span>
              </a>
            </li>
          @endcan
      </ul>
    </li>
  @endcanany
    @canany(['view-order','view-shipment','view-return','view-manifest','view-picklist'])
      <li>
        <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
          <div class="col-10-12">
            Outbound
          </div>
          <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
        </a>
        <ul class="list submenu slidedown">
          @can('view-order')
            <li>
              <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::store.orders.index')}}">
                <div class="col-10-12">
                  Store Orders
                </div>
                <span class="col-2-12 icon">
              <i class="fa fa-book" aria-hidden="true"></i>
            </span>
              </a>
            </li>
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::orders.index')}}">
              <div class="col-10-12">
                Sale Orders
              </div>
              <span class="col-2-12 icon">
              <i class="fa fa-book" aria-hidden="true"></i>
            </span>
            </a>
          </li>
          @endcan
            @can('view-picklist')
              <li>
                <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::picklists.index')}}">
                  <div class="col-10-12">
                    Picklists
                  </div>
                  <span class="col-2-12 icon">
                  <i class="fa fa-list" aria-hidden="true"></i>
                </span>
                </a>
              </li>
            @endcan
          @can('view-shipment')
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::shipments.index')}}">
              <div class="col-10-12">
                Shipments
              </div>
              <span class="col-2-12 icon">
              <i class="fa fa-truck" aria-hidden="true"></i>
            </span>
            </a>
          </li>
          @endcan
          @can('view-manifest')
              <li>
                  <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::manifests.index')}}">
                      <div class="col-10-12">
                          Dispatch Manifests
                      </div>
                      <span class="col-2-12 icon"><i class="fa fa-list" aria-hidden="true"></i></span>
                  </a>
              </li>
          @endcan
        </ul>
      </li>
    @endcanany
      <li>
        <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
          <div class="col-10-12">
            Inbound
          </div>
          <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
        </a>
        <ul class="list submenu slidedown">
{{--          <li>--}}
{{--            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::putaway.index')}}">--}}
{{--              <div class="col-10-12">--}}
{{--                Putaway--}}
{{--              </div>--}}
{{--              <span class="col-2-12 icon">--}}
{{--              <i class="fa fa-step-forward" aria-hidden="true"></i>--}}
{{--            </span>--}}
{{--            </a>--}}
{{--          </li>--}}
          @can('view-return')
            <li>
              <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::returns.index')}}">
                <div class="col-10-12">
                  Returns
                </div>
                <span class="col-2-12 icon">
              <i class="fa fa-step-backward" aria-hidden="true"></i>
            </span>
              </a>
            </li>
          @endcan
        </ul>
      </li>

      <li>
        <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
          <div class="col-10-12">
            Stock Transfers
          </div>
          <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
        </a>
        <ul class="list submenu slidedown">
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::stock-transfer.inbound')}}">
              <div class="col-10-12">
                Inbound
              </div>
              <span class="col-2-12 icon">
              <i class="fa fa-step-backward" aria-hidden="true"></i>
            </span>
            </a>
          </li>

          <li>
              <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::stock-transfer.outbound')}}">
                <div class="col-10-12">
                  Outbound
                </div>
                <span class="col-2-12 icon">
              <i class="fa fa-step-forward" aria-hidden="true"></i>
            </span>
              </a>
            </li>

        </ul>
      </li>
    @can('view-product')

{{--    @can('view-audit')--}}
{{--      <li>--}}
{{--        <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::audits.index')}}">--}}
{{--          <div class="col-10-12">--}}
{{--            Audits--}}
{{--          </div>--}}
{{--          <span class="col-2-12 icon"><i class="fa fa-hand-spock-o" aria-hidden="true"></i></span>--}}
{{--        </a>--}}
{{--      </li>--}}
{{--    @endcan--}}
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Products
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::products.index')}}">
            <div class="col-10-12">
              All Products
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-product-hunt" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @can('view-variant')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::variants.index')}}">
            <div class="col-10-12">
              All Variants
            </div>
            <span class="col-2-12 icon">
              V
            </span>
          </a>
        </li>
        @endcan
        @can('view-brand')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::brands.index')}}">
            <div class="col-10-12">
              Brands
            </div>
            <span class="col-2-12 icon">
              B
            </span>
          </a>
        </li>
        @endcan
        @can('view-marketer')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::marketers.index')}}">
            <div class="col-10-12">
              Marketers
            </div>
            <span class="col-2-12 icon">
              M
            </span>
          </a>
        </li>
        @endcan
        @can('view-collection')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::collections.index')}}">
            <div class="col-10-12">
              Categories
            </div>
            <span class="col-2-12 icon">
              C
            </span>
          </a>
        </li>
        @endcan
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::product_request.list')}}">
            <div class="col-10-12">
              Product Request
            </div>
            <span class="col-2-12 icon">
              PR
            </span>
          </a>
        </li>
        <li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::product-group.index')}}">
            <div class="col-10-12">
              Product Group
            </div>
            <span class="col-2-12 icon">
              PG
            </span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::category_l2.index')}}">
            <div class="col-10-12">
              Category L2
            </div>
            <span class="col-2-12 icon">
              L2
            </span>
          </a>
        </li>
      </ul>
    </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
            <div class="col-10-12">
              Inventories
            </div>
            <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
          </a>
          <ul class="list submenu slidedown">
              <li>
                <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::flat-inventory.product-wise-count')}}">
                  <div class="col-10-12">
                    Inventory Count
                  </div>
                  <span class="col-2-12 icon">
              <i class="fa fa-industry" aria-hidden="true"></i>
            </span>
                </a>
              </li>
              <li>
                <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::flat-inventory.storages')}}">
                  <div class="col-10-12">
                    Storages
                  </div>
                  <span class="col-2-12 icon">
              <i class="fa fa-database" aria-hidden="true"></i>
            </span>
                </a>
              </li>
          </ul>
        </li>
    @endcan
    @can('view-transaction')
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Transactions
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::transactions.index', ['type'=>'pending'])}}">
              <div class="col-10-12">
                  Pending
              </div>
              <span class="col-2-12 icon">
                PT
              </span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::transactions.index', ['type'=>'cheque_approval'])}}">
              <div class="col-10-12">
                  Cheque Approvals
              </div>
              <span class="col-2-12 icon">
                CA
              </span>
          </a>
        </li>
      </ul>
    </li>
    @endcan

      @can('view-vendors')
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Product demands
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::product-demands.index')}}">
            <div class="col-12-12">
              Demands
            </div>
          </a>
        </li>
      </ul>
    </li>

    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Vendors
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::vendors.index')}}">
              <div class="col-12-12">
                  All Vendors
              </div>
          </a>
        </li>

        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::vendors.addresses.index')}}">
              <div class="col-12-12">
                  Vendors Address
              </div>
          </a>
        </li>

        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::vendors.bank_accounts.index')}}">
              <div class="col-12-12">
                  Vendors Bank Account
              </div>
          </a>
        </li>
      </ul>
    </li>
      @endcan

{{--    @can(['view-rule'])--}}
{{--    <li>--}}
{{--        <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::rules.index')}}">--}}
{{--            <div class="col-10-12">--}}
{{--                Rule Engine--}}
{{--            </div>--}}
{{--            <span class="col-2-12 icon">--}}
{{--                <i class="fa fa-gift" aria-hidden="true"></i>--}}
{{--            </span>--}}
{{--        </a>--}}
{{--    </li>--}}
{{--    @endcan--}}
    @canany(['view-store','view-user','view-beat'])
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Stores
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        @can('view-store')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::stores.index')}}">
            <div class="col-10-12">
              All Stores
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-handshake-o" aria-hidden="true"></i>
            </span>
          </a>
        </li>
      @endcanany

        @canany(['view-employee','view-agent','view-role'])
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::users.index')}}">
            <div class="col-10-12">
              All Users
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-users" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
        @can('view-beat')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::beats.index')}}">
            <div class="col-10-12">
              Beats
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-road" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
      </ul>
    </li>

    @endcanany
      <li>
        <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
          <div class="col-10-12">
            Purchases
          </div>
          <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
        </a>
        <ul class="list submenu slidedown">
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::purchase-order.index')}}">
              <div class="col-10-12">
                Purchase Orders
              </div>
              <span class="col-2-12 icon">
                PO
              </span>
            </a>
          </li>
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::purchase-invoices.cart-index')}}">
              <div class="col-10-12">
                Vendor Carts
              </div>
              <span class="col-2-12 icon">
                VC
              </span>
            </a>
          </li>
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::purchase-invoices.index')}}">
              <div class="col-10-12">
                Purchase Invoice
              </div>
              <span class="col-2-12 icon">
                PI
              </span>
            </a>
          </li>
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::grn.index')}}">
              <div class="col-10-12">
                Goods receipt note
              </div>
              <span class="col-2-12 icon">
                GRN
              </span>
            </a>
          </li>
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::debit-notes')}}">
              <div class="col-10-12">
                Debit Notes
              </div>
              <span class="col-2-12 icon">
                DN
              </span>
            </a>
          </li>

        </ul>
      </li>

    <!--Modification Requests-->
    @canany(['create-modification-requests', 'view-modification-requests', 'view-agent'])
        <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
                <div class="col-10-12">
                    Modification Requests
                </div>
                <span class="col-2-12 icon">
                    <i class="fa fa-caret-down" aria-hidden="true"></i>
                </span>
            </a>
            <ul class="list submenu slidedown">
                @foreach (\Niyotail\Models\ModificationRequest::getTypes() as $modificationRequestType)
                    <li>
                        <a class="padding-h-30 padding-v-20 flex gutter-between"
                           href="{{route('admin::modification-requests.index', ['modificationRequestType' => $modificationRequestType])}}">
                            <div class="col-10-12">
                                {{ucwords(str_replace("_"," ",$modificationRequestType))}}
                            </div>
                            {{--                                <span class="col-2-12 icon">--}}
                            {{--                                    <i class="fa fa-step-backward" aria-hidden="true"></i>--}}
                            {{--                                </span>--}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @endcanany

    <!--Audits-->
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Audits
        </div>
        <span class="col-2-12 icon">
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
              </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between"
             href="{{route('admin::audits.index')}}">
            <div class="col-10-12">
              Audits
            </div>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between"
             href="{{route('admin::audits.hold-list')}}">
            <div class="col-10-12">
              Hold List
            </div>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between"
             href="{{route('admin::audits.storage-report')}}">
            <div class="col-10-12">
              Storage Report
            </div>
          </a>
        </li>
      </ul>
  </li>

    @can('view-agent')
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Agents
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
       <ul class="list submenu slidedown">
           <li>
             <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::agents.index')}}">
               <div class="col-10-12">
                 All Agents
               </div>
               <span class="col-2-12 icon">
                 <i class="fa fa-user-secret" aria-hidden="true"></i>
               </span>
             </a>
           </li>
       </ul>
    </li>
    @endcan
    @canany(['view-employee','view-role'])
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Permissions
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down " aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        @can('view-employee')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::employees.index')}}">
            <div class="col-10-12">
              Employees
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-users" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
        @can('view-role')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::roles.index')}}">
            <div class="col-10-12">
              Roles
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-id-badge" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
      </ul>
    </li>
    @endcanany
    @can('view-report')
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Reports
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.purchase-invoice.details')}}">
            <div class="col-10-12">
              Daily Purchase Report
            </div>
            <span class="col-2-12 icon">
            <i class="fa fa-bullhorn" aria-hidden="true"></i>
          </span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.product-inventory-snapshot')}}">
            <div class="col-10-12">
              Product Inventory Snapshots
            </div>
            <span class="col-2-12 icon">PS</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.inventory')}}">
            <div class="col-10-12">
              Inventory
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-gift" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.order-item')}}">
            <div class="col-10-12">
              OrderItem
            </div>
            <span class="col-2-12 icon">OI</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::rate-credit-note.index')}}">
            <div class="col-10-12">
              Rate Credit Notes
            </div>
            <span class="col-2-12 icon">RCN</span>
          </a>
        </li>
        @can('cancel-order-item')
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.processing-order-item')}}">
              <div class="col-10-12">
                Processing Order Items
              </div>
              <span class="col-2-12 icon">POI</span>
            </a>
          </li>
        @endcan
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.return-order-item')}}">
            <div class="col-10-12">
              Return Items
            </div>
            <span class="col-2-12 icon">RI</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.invoice')}}">
            <div class="col-10-12">
              Invoices
            </div>
            <span class="col-2-12 icon">IR</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.inventory-snapshot')}}">
            <div class="col-10-12">
              Inventory Snapshots
            </div>
            <span class="col-2-12 icon">IS</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.pi.items')}}">
            <div class="col-10-12">
              Purchase Invoice Items
            </div>
            <span class="col-2-12 icon">PII</span>
          </a>
        </li>
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::reports.grn-items')}}">
            <div class="col-10-12">
              GRN Items
            </div>
            <span class="col-2-12 icon">GRNI</span>
          </a>
        </li>
      </ul>
    </li>
    @endcan
    @canany(['view-attribute','view-badge','view-warehouse','view-tax_class','view-importer'])
    <li>
      <a class="padding-h-30 padding-v-20 flex gutter-between has-submenu">
        <div class="col-10-12">
          Settings
        </div>
        <span class="col-2-12 icon">
          <i class="fa fa-caret-down" aria-hidden="true"></i>
        </span>
      </a>
      <ul class="list submenu slidedown">

        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::tags.index')}}">
            <div class="col-10-12">
              Tags
            </div>
            <span class="col-2-12 icon">
          <i class="fa fa-tags" aria-hidden="true"></i>
        </span>
          </a>
          </a>
        </li>

        @can('view-pincode')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::pincode.index')}}">
            <div class="col-10-12">
              Pincodes
            </div>
            <span class="col-2-12 icon">
          <i class="fa fa-bullhorn" aria-hidden="true"></i>
        </span>
          </a>
          </a>
        </li>
        @endcan
        @can('view-attribute')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::attributes.index')}}">
            <div class="col-10-12">
              Attributes
            </div>
            <span class="col-2-12 icon">
              A
            </span>
          </a>
        </li>
        @endcan
        @can('view-badge')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::badges.index')}}">
            <div class="col-10-12">
              Badges
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-id-badge" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
        @can('view-warehouse')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::warehouses.index')}}">
            <div class="col-10-12">
              Warehouses
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-gift" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
        @can('view-tax_class')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::tax-classes.index')}}">
            <div class="col-10-12">
              Tax Classes
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-money" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
        @can('view-importer')
        <li>
          <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::importers.index')}}">
            <div class="col-10-12">
              Importer Actions
            </div>
            <span class="col-2-12 icon">
              <i class="fa fa-file-excel-o" aria-hidden="true"></i>
            </span>
          </a>
        </li>
        @endcan
          <li>
            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::bank.index')}}">
              <div class="col-10-12">
                Banks
              </div>
              <span class="col-2-12 icon">
              <i class="fa fa-bank" aria-hidden="true"></i>
            </span>
            </a>
          </li>
{{--        @can('view-profile-questions')--}}
{{--          <li>--}}
{{--            <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::profile-questions.index')}}">--}}
{{--              <div class="col-10-12">--}}
{{--                Profile Questions--}}
{{--              </div>--}}
{{--              <span class="col-2-12 icon">--}}
{{--                <i class="fa fa-file-excel-o" aria-hidden="true"></i>--}}
{{--              </span>--}}
{{--            </a>--}}
{{--          </li>--}}
{{--        @endcan--}}
      </ul>
    </li>
    @endcanany
{{--    @can('view-app_banner')--}}
{{--    <li>--}}
{{--      <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::app-banners.index')}}">--}}
{{--        <div class="col-10-12">--}}
{{--          App Banners--}}
{{--        </div>--}}
{{--        <span class="col-2-12 icon">--}}
{{--          <i class="fa fa-bullhorn" aria-hidden="true"></i>--}}
{{--        </span>--}}
{{--      </a>--}}
{{--      </a>--}}
{{--    </li>--}}
{{--    @endcan--}}
{{--    @can('view-app_banner')--}}
{{--    <li>--}}
{{--      <a class="padding-h-30 padding-v-20 flex gutter-between" href="{{route('admin::dashboard-image.index')}}">--}}
{{--        <div class="col-10-12">--}}
{{--          Dashboard Images--}}
{{--        </div>--}}
{{--        <span class="col-2-12 icon">--}}
{{--          <i class="fa fa-bullhorn" aria-hidden="true"></i>--}}
{{--        </span>--}}
{{--      </a>--}}
{{--      </a>--}}
{{--    </li>--}}
{{--    @endcan--}}

  </ul>
</nav>
