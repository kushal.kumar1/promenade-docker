@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                {{!empty($title) ? $title : 'Rate Credit Notes'}}
            </div>
        </div>
        @include('admin.grid')
    </div>
@endsection
