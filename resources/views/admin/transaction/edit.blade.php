@extends('admin.master')

@section('content')
    <div class="container">
        <h2 class="h5 bold margin-t-10">Edit Transaction</h2>
        <div class="margin-t-20">
          <div class="flex gutter">
            <div class="col-3-12">
              @include('admin.transaction.store')
            </div>
            <div class="col-9-12">
              <div class="card">
                    <div class='margin-t-20 flex gutter'>
                      <div class="col-6-12">
                        <label class="label">Transaction / Reference Id</label>
                        <input class="form-control" name="transaction_id" type="text" value="{{$transaction->id}}" disabled/>
                      </div>
                      <div class="col-6-12">
                          <label class="label">Description</label>
                          <textarea class="form-control" name="description" disabled>{{!empty($transaction->description) ? $transaction->description : '' }}</textarea>
                      </div>
                    </div>
                    @if(!empty($transaction->image))
                      <div class='margin-t-20 flex gutter'>
                        <img src="{{Image::getSrc($transaction, 500)}}" class="bd" />
                      </div>
                    @endif
                    <div class="margin-t-20 flex gutter">
                        @if($transaction->type == 'credit')
                            <div class="col-4-12">
                                <label class="label">Payment Method</label>
                                <select name="payment_method" class="form-control payment-method">
                                    <option value="{{$transaction->payment_method}}">
                                        {{$transaction->payment_method}}
                                    </option>
                                </select>
                            </div>
                            <div class="col-4-12">
                                <label class="label">Type</label>
                                <select name="type" class="form-control payment-type">
                                    <option value="{{$transaction->type}}">
                                        {{$transaction->type}}
                                    </option>
                                </select>
                            </div>
                            <div class="col-4-12">
                                <label class="label">Payment Tag</label>
                                <select name="payment_tag" id="select2-payment-tag" class="form-control payment-tag"></select>
                            </div>
                        @else
                            <div class="col-6-12">
                                <label class="label">Payment Method</label>
                                <select name="payment_method" class="form-control payment-method">
                                    <option value="{{$transaction->payment_method}}">
                                        {{$transaction->payment_method}}
                                    </option>
                                </select>
                            </div>
                            <div class="flex col-6-12">
                                <label class="label">Type</label>
                                <select name="type" class="form-control payment-type">
                                    <option value="{{$transaction->type}}">
                                        {{$transaction->type}}
                                    </option>
                                </select>
                            </div>
                        @endif
                    </div>
                    @if (!empty($transaction->payment_details) && (strpos($transaction->payment_details, 'cheque_number') !== false))
                      <?php $cheque = json_decode($transaction->payment_details); ?>
                      <div class="flex gutter cheque-options">
                        <div class="col-4-12">
                          <label class="label">Cheque Number</label>
                          @if(empty($transaction->chequeMetas))
                            <input class="form-control" name="cheque_number" value="{{$cheque->cheque_number}}" type="text" disabled/>
                          @else
                            <input class="form-control" name="cheque_number" value="{{$transaction->chequeMetas->cheque_number}}" type="text" disabled/>
                          @endif
                        </div>
                        <div class="col-4-12">
                          <label class="label">Cheque Date</label>
                            @if(empty($transaction->chequeMetas))
                            <input id="cheque-datetime" class="form-control" type="text" name="cheque_date"
                                value="{{ $cheque->cheque_date }}" disabled/>
                                @else
                            <input id="cheque-datetime" class="form-control" type="text" name="cheque_date"
                              value="{{ $transaction->chequeMetas->cheque_issue_date }}" disabled/>
                            @endif
                        </div>
                        <div class="col-4-12">
                          <label class="label">Bank</label>
                            @if(empty($transaction->chequeMetas))
                                <input class="form-control" type="text" name="cheque_issuer"
                                       value="{{ $cheque->cheque_issuer }}" disabled/>
                            @else
                            <select class="form-control" name="cheque_issuer" disabled>
                                <option value="">Select Cheque Bank Account</option>
                                @foreach(\Niyotail\Models\Bank::all() as $bank)
                                    <option @if($bank->id == $transaction->chequeMetas->bank_id) selected @endif value="{{$bank->id}}">{{$bank->name}}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                      </div>
                      @endif
                      @if (!empty($transaction->payment_details) && (strpos($transaction->payment_details, 'reference_id') !== false))
                      <div class="flex gutter reference-options">
                        <div class="col-6-12">
                          <label class="label">UPI/Netbanking Reference Id</label>
                          <?php $ref = json_decode($transaction->payment_details); ?>
                          <input class="form-control" name="reference_id" value="{{ $ref->reference_id }}" type="text" disabled/>
                        </div>
                      </div>
                      @endif
                      <div class="margin-t-20">
                        <div class="flex gutter-sm">
                            <div class="col-6-12">
                              <label class="label">Amount</label>
                              <input type="number" class="form-control amount" name="amount" value="{{$transaction->amount}}" disabled/>
                            </div>
                            @if(!empty($transaction->createdBy))
                            <div class="col-6-12">
                              <label class="label">Agent Name</label>
                              <input type="number" class="form-control" name="agent_id" value="{{$transaction->createdBy->id}}" hidden/>
                              <input type="text" class="form-control" value="{{$transaction->createdBy->name}}" disabled/>
                            </div>
                            @endif
                        </div>
                      </div>
                      <?php $debitTransactions = $transaction->settledDebitTransactions->isNotEmpty() ? $transaction->settledDebitTransactions : null ?>
                      @if(!empty($debitTransactions))
                        <div class="margin-t-30">
                          <label class="label bold">Settled Debit Transactions/Invoices</label>
                          <div class="flex gutter-sm">
                            <div class="col-2-12 bold">
                              Number
                            </div>
                            <div class="col-2-12 bold">
                              Financial Year
                            </div>
                            <div class="col-2-12 bold">
                              Order ID/Type
                            </div>
                            <div class="col-2-12 bold">
                              Transaction Amount
                            </div>
                            <div class="col-2-12 bold">
                              Settled Amount
                            </div>
                            <div class="col-2-12 bold">
                              Remaining Amount
                            </div>
                          </div>
                          @foreach ($debitTransactions as $debitTransaction)
                            <div class="flex gutter-sm">
                              <div class="col-2-12">
                                  @if (!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice')
                                    {{ $debitTransaction->source->reference_id }}
                                  @else
                                    {{ $debitTransaction->transaction_id }}
                                  @endif
                              </div>
                              <div class="col-2-12">
                                  @if (!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice')
                                    {{ $debitTransaction->source->financial_year }}
                                  @endif
                              </div>
                              <div class="col-2-12">
                                  @if (!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice')
                                    {{ $debitTransaction->source->order_id }}
                                  @else
                                    {{ $debitTransaction->payment_method }}
                                  @endif
                              </div>
                              <div class="col-2-12">
                                &#8377; {{number_format($debitTransaction->amount, 2)}}
                              </div>
                              <div class="col-2-12">
                                &#8377; {{number_format($debitTransaction->pivot->amount, 2)}}
                              </div>
                              <div class="col-2-12 bold">
                                <?php $remainingAmount = $debitTransaction->amount - $debitTransaction->settledAmount->amount ?>
                                &#8377; {{number_format($remainingAmount, 2)}}
                              </div>
                            </div>
                          @endforeach
                        </div>
                      @endif


                      @if($transaction->status == 'pending')
                          @can(['approve-reject-transaction'])
                          <form class="margin-t-20 form approve-reject-form" method="post" action="{{route('admin::transactions.update')}}">
                              {{csrf_field()}}
                              <input type="hidden" name="transaction_id" value="{{$transaction->id}}" />
                              <input id='approve-reject-payment-tag' type="hidden" name="payment_tag" value="" />
                              <div>
                                <label class="label">Remarks</label>
                                <textarea class="form-control" name="remarks"></textarea>
                              </div>
                              <div class="flex gutter-between">
                                <input type="hidden" class="action_type" name="action_type"/>
                                <button id="reject-btn" type="button" class="btn btn-primary button-success">REJECT</button>
                                <button id="accept-btn" type="button" class="btn btn-primary button-success">APPROVE</button>
                              </div>
                            </form>
                          @endcan

                          @can(['delete-transaction'])
                            @if(in_array($transaction->payment_method, ['cheque_bounce', 'cheque', 'cash', 'UPI'])
                                  && empty($transaction->source_type)
                                  && ($transaction->type != 'debit')
                                  && !$transaction->trashed())
                              <form class="margin-t-20 form" id="deleteTransactionForm" method="post" action="{{route('admin::transactions.delete')}}" data-destination="{{route('admin::transactions.index')}}">
                                {{csrf_field()}}
                                <input type="hidden" name="transaction_id" value="{{$transaction->id}}" />
                                <div>
                                  <label class="label">Delete Remarks</label>
                                  <textarea class="form-control" id="deleteReason" name="remarks"></textarea>
                                </div>
                                <div class="flex gutter-between">
                                  <button type="button" id="deleteTransactionFormBtn" class="btn btn-danger button-success">Delete Transaction</button>
                                </div>
                              </form>
                            @endif
                          @endcan

                      @endif
                    </div>
                    </div>
                  </div>
              </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $('.payment-tag').select2({
        minimumInputLength: 0,
        ajax: {
            delay: 0,
            url: '{{route("admin::search.payment_tags")}}',
            dataType: 'json',
            processResults: (data) => {
                return {results: data};
            }
        },
        placeholder: '{{$transaction->paymentTag->name ?? 'Payment Tag not set'}}',
        templateResult: function (paymentTag) {
            if (!paymentTag || (paymentTag.text === "Searching…")) {
                return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
            }

            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${paymentTag.name}</h5>`;
            html += '</div>';

            return $(html);
        },
        templateSelection: function (paymentTag) {
            $(paymentTag.element).attr('id', paymentTag.id);
            $(paymentTag.element).attr('value', paymentTag.id);

            if(!paymentTag.name)
                return `${paymentTag.text}`;
            return `${paymentTag.name}`;
        }
    });
    $('.payment-tag').on('change', function (e) {
        $('#approve-reject-payment-tag').val($('.payment-tag').find(':selected').val())
    });

$('#reject-btn').on('click', function(){
    $('.action_type').val('reject');
    $('.approve-reject-form').submit()
});

$('#accept-btn').on('click', function(){
    $('.action_type').val('accept');
    $('.approve-reject-form').submit()
});

class TransactionActions {
  constructor() {
      $('#deleteTransactionFormBtn').on('click', (e) => this.submitDeleteTransactionForm());
  }

  submitDeleteTransactionForm() {
    if(!$("#deleteReason").val()) {
        $("#deleteReason").focus();
        const toast = new Toast('Please enter a valid remark to delete this transaction.');
        toast.show();
        return false;
    }

    dialog(
        'Complete Invoice',
        'Are you sure you want to delete the transaction?',
        function() {
            $('#deleteTransactionForm').submit();
        }
    );
  }
}

new TransactionActions();

</script>
@endsection
