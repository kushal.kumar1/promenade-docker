<div class="margin-t-100">
    <div class="flex settle-transactions hidden">
    </div>
    <div class='flex form-control settle-transactions-display' contenteditable="true">
    </div>
    @if (empty($transactions))
      <span>No Debit Transactions found!</span>
    @else
      <div class="grid">
        <table>
            <thead>
              <tr>
                <th>
                  Number
                </th>
                <th>
                  Financial Year
                </th>
                <th>
                  Order ID/Type
                </th>
                <th>
                  Amount
                </th>
                <th>
                  Reamining Amount
                </th>
                <th>
                  Settlement Type
                </th>
                <th>
                  Settlement Amount
                </th>
              </tr>
            </thead>
        @foreach ($transactions as $transaction)
              <tbody>
                <tr>
                  <td>
                    @if (!empty($transaction->source_type) && $transaction->source_type == 'invoice')
                      {{ $transaction->source->reference_id }}
                    @else
                      {{ $transaction->transaction_id }}
                    @endif
                  </td>
                  <td>
                    @if (!empty($transaction->source_type) && $transaction->source_type == 'invoice')
                      {{ $transaction->source->financial_year }}
                    @endif
                  </td>
                  <td>
                    @if (!empty($transaction->source_type) && $transaction->source_type == 'invoice')
                      {{ $transaction->source->order_id }}
                    @else
                      {{ $transaction->payment_method }}
                    @endif
                  </td>
                  <td>
                    &#8377; {{ $transaction->amount }}
                  </td>
                  <td>
                    <?php $remainingAmount = $transaction->amount - (!empty($transaction->settledAmount) ? $transaction->settledAmount->amount : 0) ?>
                    &#8377; {{ $remainingAmount }}
                  </td>
                  <td>
                    <label class="flex cross-center">
                        <div class="radio margin-r-10">
                            <input type="radio" name="settle-type-{{ $transaction->id }}" value="full" onclick="settlementTypeFullSelect({{ $transaction->id }}, {{ $remainingAmount }})"/>
                            <span class="indicator"></span>
                        </div>
                        <span>Full</span>
                    </label>
                    <label class="flex cross-center">
                        <div class="radio margin-r-10">
                            <input type="radio" name="settle-type-{{ $transaction->id }}" value="partial" onclick="settlementTypePartialSelect({{ $transaction->id }}, {{ $remainingAmount }})"/>
                            <span class="indicator"></span>
                        </div>
                        <span>Partial</span>
                    </label>
                  </td>
                  <td>
                    <div class="flex">
                      {{-- <input class="amount-detail-{{$invoice->id}}" name="settle_invoices[{{$invoice->id}}][amount]" type="number" step="0.01"/> --}}
                      <input class="amount-detail-{{$transaction->id}}" type="number" step="0.01"/>
                      <?php
                            if (!empty($transaction->source_type) && $transaction->source_type == 'invoice') {
                              $referenceID = $transaction->source->reference_id;
                            } else {
                              $referenceID = $transaction->transaction_id;
                            }
                      ?>
                      <button type="button" class="btn btn-primary" onclick="addTransactionSettlementAmount({{$transaction->id}}, '{{ $referenceID }}', {{$remainingAmount}})">Add</button>
                    </div>
                  </td>
                </tr>
              </tbody>
        @endforeach
      </table>
    </div>
    @endif
</div>
