<h2 class="h5 bold margin-t-10">Add Transaction</h2>
<div class="margin-t-20">
  <div class="flex gutter">
    <div class="col-3-12">
      @include('admin.transaction.store')
    </div>
    <div class="col-9-12">
      <div class="card">
        <form class="form" method="post" action="{{route('admin::transactions.store')}}" autocomplete="off">
            {{csrf_field()}}
            <input type="hidden" name="store_id" value="{{$store->id}}" />

            <div class="margin-t-20 flex gutter">
                <div class="col-4-12">
                    <label class="label">Payment Method</label>
                    <select name="payment_method" class="form-control payment-method">
                        <option value="">
                            Select Payment Method
                        </option>
                        @foreach ($payment_methods as $method)
                            <option value="{{$method}}">
                                {{$method}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4-12">
                    <label class="label">Type</label>
                    <select name="type" class="form-control payment-type">
                        <option value="">
                            Please select a payment method first
                        </option>
                    </select>
                </div>
                <div class="col-4-12">
                    <label class="label">Payment Tag</label>
                    <select name="payment_tag" id="select2-payment-tag" class="form-control payment-tag">
                        <option value="">
                            Please select a payment method first
                        </option>
                    </select>
                </div>
            </div>
            <div class='margin-t-20 flex gutter'>
              <div class="col-6-12">
                <label class="label">Transaction / Reference Id</label>
                <input class="form-control" name="transaction_id" type="text"/>
              </div>
              <div class="col-6-12">
                  <label class="label">Description</label>
                  <textarea class="form-control" name="description"></textarea>
              </div>
            </div>
            <div id="action-card" class="card margin-t-10">
              <div class="flex gutter cheque-options" style="display: none">
                <div class="col-4-12">
                  <label class="label">Cheque Number</label>
                  <input class="form-control cheque_number" name="cheque_number" type="number"/>
                </div>
                <div class="col-4-12">
                  <label class="label">Cheque Date</label>
                  <input id="cheque-datetime" class="form-control cheque_date" type="text" name="cheque_date"/>
                </div>
                <div class="col-4-12">
                  <label class="label">Issued By</label>
                  <select name="cheque_issuer" class="form-control cheque_issuer">
                    <option value="">
                      Please Select a Bank
                    </option>
                      @foreach($banks as $bank)
                          <option value="{{$bank->id}}">{{$bank->name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>
              <div class="flex gutter reference-options" style="display: none">
                <div class="col-6-12">
                  <label class="label">UPI/Netbanking Reference Id</label>
                  <input class="form-control" name="reference_id" type="text"/>
                </div>
              </div>
              <div class="margin-t-20 credit-options" hidden>
                <label class="label">Invoices/Transactions</label>
                <div class="margin-t-10">
                  <div class="flex gutter-sm">
                      <div class="col-6-12">
                          <label class="flex cross-center">
                              <div class="radio margin-r-10">
                                  <input type="radio" name="auto_settle" value="1" onclick="autoSelect()"/>
                                  <span class="indicator"></span>
                              </div>
                              <span>Auto Select</span>
                          </label>
                      </div>
                      <div class="col-6-12">
                          <label class="flex cross-center">
                              <div class="radio margin-r-10">
                                  <input type="radio" name="auto_settle" value="0" onclick="manualSelect()"/>
                                  <span class="indicator"></span>
                              </div>
                              <span>Choose Invoice/Transaction</span>
                          </label>
                      </div>
                  </div>
                </div>
            </div>
            <div class="margin-t-20">
              <div class="flex gutter-sm">
                  <div class="col-6-12">
                    <label class="label">Amount</label>
                    <input type="number" class="form-control amount" name="amount" step="0.01"/>
                  </div>
                  <div class="col-6-12">
                    <label class="label">Assign Agent</label>
                    <select class="form-control" id="agents-search" name="agent_id"></select>
                  </div>
              </div>
            </div>
            <div class="margin-t-20 cheque-bounce-options" style="display: none">
              <div class="flex gutter-sm">
                @if (!empty($chequeTransactions))
                  @foreach ($chequeTransactions as $chequeTransaction)
                    <label class="flex cross-center">
                        <?php $chequeDetails = json_decode($chequeTransaction->payment_details); ?>
                        <div class="radio margin-r-10">
                            <input type="radio" name="cheque_transaction_id" value="{{$chequeTransaction->id}}" onclick="bouncedChequeSelected({{$chequeTransaction->amount }}, {{$chequeDetails->cheque_number}}, '{{$chequeDetails->cheque_date}}', '{{$chequeDetails->cheque_issuer}}')"/>
                            <span class="indicator"></span>
                        </div>
                        <label> Amount: </label><span class="bold"><pre> Rs. {{$chequeTransaction->amount }}   </pre></span>
                        <label> Cheque Number: </label> <span class="bold"><pre>   {{$chequeDetails->cheque_number}}  </pre></span>
                        <label> Dtd: </label> <span class="bold"><pre>   {{$chequeDetails->cheque_date}}  </pre></span>
                        <label> Bank: </label><span class="bold"><pre>   {{$chequeDetails->cheque_issuer}}  </pre></span>
                    </label>
                  @endforeach
                @else
                    <div class="col-4-12">
                      <label class="label">Cheque Number</label>
                      <input class="form-control cheque_number" name="cheque_number" type="number"/>
                    </div>
                    <div class="col-4-12">
                      <label class="label">Cheque Date</label>
                      <input id="cheque-bounce-datetime" class="form-control cheque_date" type="text" name="cheque_date"/>
                    </div>
                    <div class="col-4-12">
                      <label class="label">Issued By</label>
                      <select name="cheque_issuer" class="form-control cheque_issuer">
                        <option value="">
                          Please Select a Bank
                        </option>
                          @foreach($banks as $bank)
                              <option value="{{$bank->id}}">{{$bank->name}}</option>
                          @endforeach
                      </select>
                    </div>
                @endif
              </div>
            </div>
          </div>
              <div class="margin-t-5 transactions" style="display: none">
                <!-- <?php $invoices = $store->invoices->isNotEmpty() ? $store->invoices : null ?> -->
                {{-- @include('admin.transaction.invoice_collection') --}}

                <?php $transactions = $store->transactions->isNotEmpty() ? $store->transactions->where('type', 'debit') : null ?>
                @include('admin.transaction.transaction_collection')
              </div>
              <div class="flex gutter-between margin-t-40">
                <button type="submit" class="btn btn-primary button-success">Submit</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
