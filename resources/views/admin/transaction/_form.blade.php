<div id="add-transaction" class="modal">
  <div class="modal-content">
    <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
    <h5 class="h6 title bold text-center">Add Transaction</h5>
    <form class="form" method="post" action="{{route('admin::transactions.store')}}" autocomplete="off">
      {{csrf_field()}}
      <input type="hidden" name="store_id" value="{{$store->id}}" />
      <div class="margin-t-30">
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Transaction / Reference Id</label>
            <input class="form-control" name="transaction_id" type="text"/>
            </select>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Description</label>
            <textarea class="form-control" name="description"></textarea>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Payment Method</label>
                <select name="payment_method" class="form-control payment-method">
                  <option value="">
                    Select Payment Method
                  </option>
                  @foreach ($payment_methods as $method)
                    <option value="{{$method}}">
                      {{$method}}
                    </option>
                  @endforeach
                </select>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Type</label>
            <select name="type" class="form-control payment-type">
              <option value="">
                Please select a payment method first
              </option>
            </select>
          </div>
        </div>
        <div class="flex gutter">
          <div class="col-12-12">
            <label class="label">Amount</label>
            <input type="number" class="form-control" name="amount"/>
          </div>
        </div>
        <div class="flex gutter-between margin-t-40">
          <button type="button" class="btn button-reject" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary button-success">Submit</button>
        </div>
    </form>
  </div>
</div>
