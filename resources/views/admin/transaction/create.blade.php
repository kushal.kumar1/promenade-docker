@extends('admin.master')
@section('css')
  <link rel="stylesheet" href="/plugins/zebra-datetimepicker/css/bootstrap/zebra_datepicker.min.css">
  <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
@endsection
@section('content')
    <div class="container">
      @include('admin.transaction.form')
    </div>
@endsection
@section('js')
<script src="/plugins/zebra-datetimepicker/zebra_datepicker.min.js"></script>
<script type="text/javascript">
$('#cheque-datetime').Zebra_DatePicker({
    format: 'd M, Y',
    show_icon: false,
    default_position: 'below'
});

$('#cheque-bounce-datetime').Zebra_DatePicker({
    format: 'd M, Y',
    show_icon: false,
    default_position: 'below'
});

$('.payment-method').on('change', function(e) {
      var lookup = {!! json_encode($mappedPaymentMethods) !!};
      let selector = $(this).val();
      $(".payment-type").empty();
      $('.payment-tag').empty();
      let credit = lookup['credit'];
      let debit = lookup['debit'];
      $('.amount').removeAttr('readonly');
      //TODO Remove after renaming
      // $('.settle-invoices').empty();
      // $('.settle-invoices-display').empty();
      $('.settle-transactions').empty();
      $('.settle-transactions-display').empty();
      if (credit.includes(selector)) {
          $('.payment-type').append("<option value='credit'>credit</option>");
          $('.credit-options').show();
          $('.payment-tag').select2({
              minimumInputLength: 0,
              ajax: {
                  delay: 125,
                  url: '{{route("admin::search.payment_tags")}}',
                  dataType: 'json',
                  processResults: (data) => {
                      return {results: data};
                  }
              },
              templateResult: function (paymentTag) {
                  if (!paymentTag || (paymentTag.text === "Searching…")) {
                      return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                  }

                  let html = '<div class="padding-10">';
                  html += `<h5 class="h6 bold">${paymentTag.name}</h5>`;
                  html += '</div>';

                  return $(html);
              },
              templateSelection: function (paymentTag) {
                  $(paymentTag.element).attr('id', paymentTag.id);
                  $(paymentTag.element).attr('value', paymentTag.id);
                  return `${paymentTag.name}`;
              }
          });
          // $('.payment-tag').prop('required', true);
          $('.payment-tag').trigger('click');
      } else if (debit.includes(selector)) {
          $('.payment-type').append("<option value='debit'>debit</option>");
          $('.credit-options').hide();
          $('.payment-tag').append("<option value='null'>Only applicable for Credit Payment Methods</option>");
          $('.payment-tag').prop('required', false);
      } else {
          $('.credit-options').hide();
          $('.payment-type').append("<option value=''>Please select a payment method first</option>");
          $('.payment-tag').append("<option value='null'>Please select a payment method first</option>");
          $('.payment-tag').prop('required', false);
      }

      if (credit.includes(selector)) {
        $('.cheque-bounce-options').hide();
        $('.cheque-bounce-options :input').prop('disabled', true);
        switch(selector) {
            case "cheque":
              $('.cheque-options').show();
              $('.cheque-options :input').prop('disabled', false);
              $('.reference-options').hide();
              break;
            case "cash_discount":
            case "cash":
              $('.cheque-options').hide();
              $('.cheque-options :input').prop('disabled', true);
              $('.reference-options').hide();
              break;
            default:
              $('.reference-options').show();
              $('.cheque-options :input').prop('disabled', true);
              $('.cheque-options').hide();
              break;
        }
      } else {
        $('.cheque-options').hide();
        $('.cheque-options :input').prop('disabled', true);
        $('.reference-options').hide();
      }

      if (debit.includes(selector)) {
        $('.cheque-options :input').prop('disabled', true);
        $('.reference-options').hide();
        switch(selector) {
            case "cheque_bounce":
              $('.cheque-options :input').prop('disabled', false);
              $('.cheque-bounce-options').show();
              $('.cheque-bounce-options :input').prop('disabled', false);
              break;
            default:
              $('.cheque-bounce-options').hide();
              $('.cheque-bounce-options :input').prop('disabled', true);
              break;
        }
      }
});

function bouncedChequeSelected(amount, chequeNumber, chequeDate, chequeIssuer) {
  $('.amount').val(amount);
  $('.amount').attr('readonly', 'true');
  $('.cheque_date').val(chequeDate);
  $('.cheque_number').val(chequeNumber);
  $('.cheque_issuer').val(chequeIssuer);
}

function autoSelect() {
  $('.amount').removeAttr('readonly');
  //TODO Remove
  //$('.invoices').hide();
  // $('.settle-invoices').empty();
  // $('.settle-invoices-display').empty();

  $('.transactions').hide();
  $('.settle-transactions').empty();
  $('.settle-transactions-display').empty();
}

function manualSelect() {
  //TODO Remove
  //$('.invoices').show();
  $('.transactions').show();
  $('.amount').attr('readonly', 'true');
}

//TODO Remove
// function settlementTypeFullSelect(invoiceID, invoiceRemainingAmount) {
//   $('.amount-detail-'+invoiceID).val(''+invoiceRemainingAmount);
//   $('.amount-detail-'+invoiceID).attr('readonly', 'true');
// }
//
// function settlementTypePartialSelect(invoiceID, invoiceRemainingAmount) {
//   $('.amount-detail-'+invoiceID).removeAttr('readonly');
//   $('.amount-detail-'+invoiceID).val(''+invoiceRemainingAmount);
// }
// function updateAmountInfo(invoiceID, invoiceRefNum) {
//   var sum = 0;
//   $('.single-invoice').each(function() {
//     if(!isNaN(this.value) && this.value.length!=0) {
//       sum += parseFloat(this.value);
//     }
//   });
//   $('.amount').val(sum.toFixed(2));
// }

function settlementTypeFullSelect(transactionID, transactionRemainingAmount) {
  $('.amount-detail-'+transactionID).val(''+transactionRemainingAmount);
  $('.amount-detail-'+transactionID).attr('readonly', 'true');
}

function settlementTypePartialSelect(transactionID, transactionRemainingAmount) {
  $('.amount-detail-'+transactionID).removeAttr('readonly');
  $('.amount-detail-'+transactionID).val(''+transactionRemainingAmount);
}

function updateAmountInfo(transactionID, invoiceRefNum) {
  var sum = 0;
  $('.single-transaction').each(function() {
    if(!isNaN(this.value) && this.value.length!=0) {
      sum += parseFloat(this.value);
    }
  });
  $('.amount').val(sum.toFixed(2));
}

//TODO Remove
// function addTransactionSettlementAmount(invoiceID, invoiceRefNum, remainingAmount) {
//   var selectedSettleOption = $("input[name='settle-type-"+ invoiceID +"']:checked").val();
//   var newAmount = $('.amount-detail-'+invoiceID).val();
//   var previousAmount = 0;
//   if ($('.amount-settle-'+invoiceID).val()) {
//     previousAmount = $('.amount-settle-'+invoiceID).val();
//   } else {
//     $('.settle-invoices').append("<input type='text' class='single-invoice amount-settle-"+invoiceID+"' name='settle_invoices["+invoiceID+"][amount]' hidden/>");
//   }
//   var totalAmount = 0;
//   if (selectedSettleOption === 'full') {
//     totalAmount = parseFloat(newAmount);
//   } else {
//     totalAmount = parseFloat(newAmount) + parseFloat(previousAmount);
//   }
//   if (totalAmount <= remainingAmount && newAmount > 0) {
//     $('.amount-settle-'+invoiceID).val(''+totalAmount);
//     updateAmountInfo(invoiceID, invoiceRefNum);
//     if($('span').is('.remove-'+invoiceID)) {
//         $('.remove-'+invoiceID).parent().html(invoiceRefNum + " - Rs. " + totalAmount +
//                                             "<span class='remove-" + invoiceID +"' onclick='removeInvoiceFromSettlement("+invoiceID+
//                                             ", \"" + invoiceRefNum + "\"" +
//                                             ")'" +
//                                             ">      X</span>");
// 	  } else {
//         $(".settle-invoices-display").append("<div class='tag'>" + invoiceRefNum + " - Rs. " + totalAmount +
//                                             "<span class='remove-" + invoiceID +"' onclick='removeInvoiceFromSettlement("+invoiceID+
//                                             ", \"" + invoiceRefNum + "\"" +
//                                             ")'" +
//                                             ">      X</span></div>");
//     }
//     $('.amount-detail-'+invoiceID).val('0.00')
//   }
// }
//
// function removeInvoiceFromSettlement(invoiceID, invoiceRefNum) {
//   $('.remove-'+invoiceID).parent().remove();
//   $('.amount-settle-'+invoiceID).parent().remove();
//   updateAmountInfo(invoiceID, invoiceRefNum);
// }
//
// $('.settle-invoices-display').keydown(function () {
//   return false;
// });

function addTransactionSettlementAmount(transactionID, invoiceRefNum, remainingAmount) {
  var selectedSettleOption = $("input[name='settle-type-"+ transactionID +"']:checked").val();
  var newAmount = $('.amount-detail-'+transactionID).val();
  var previousAmount = 0;
  if ($('.amount-settle-'+transactionID).val()) {
    previousAmount = $('.amount-settle-'+transactionID).val();
  } else {
    $('.settle-transactions').append("<input type='text' class='single-transaction amount-settle-"+transactionID+"' name='settle_transactions["+transactionID+"][amount]' hidden/>");
  }
  var totalAmount = 0;
  if (selectedSettleOption === 'full') {
    totalAmount = parseFloat(newAmount);
  } else {
    totalAmount = parseFloat(newAmount) + parseFloat(previousAmount);
  }
  if (totalAmount <= remainingAmount && newAmount > 0) {
    $('.amount-settle-'+transactionID).val(''+totalAmount);
    updateAmountInfo(transactionID, invoiceRefNum);
    if($('span').is('.remove-'+transactionID)) {
        $('.remove-'+transactionID).parent().html(invoiceRefNum + " - Rs. " + totalAmount +
                                            "<span class='remove-" + transactionID +"' onclick='removeTransactionFromSettlement("+transactionID+
                                            ", \"" + invoiceRefNum + "\"" +
                                            ")'" +
                                            ">      X</span>");
	  } else {
        $(".settle-transactions-display").append("<div class='tag'>" + invoiceRefNum + " - Rs. " + totalAmount +
                                            "<span class='remove-" + transactionID +"' onclick='removeTransactionFromSettlement("+transactionID+
                                            ", \"" + invoiceRefNum + "\"" +
                                            ")'" +
                                            ">      X</span></div>");
    }
    $('.amount-detail-'+transactionID).val('0.00')
  }
}

function removeTransactionFromSettlement(transactionID, invoiceRefNum) {
  $('.remove-'+transactionID).parent().remove();
  $('.amount-settle-'+transactionID).parent().remove();
  updateAmountInfo(transactionID, invoiceRefNum);
}

$('.settle-transactions-display').keydown(function () {
  return false;
});

$('#agents-search').select2({
    placeholder: "Search Agent by name or code",
    minimumInputLength: 2,
    ajax: {
        url: '/search/agents',
        dataType: 'json',
        processResults: function(data) {
            data = $.map(data, function(obj) {
                obj.id = obj.id;
                obj.text = obj.name;
                return obj;
            });
            return {
                results: data
            };
        }
    }
});

</script>
@endsection
