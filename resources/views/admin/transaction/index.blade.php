@extends('admin.master')

@section('content')

<div class="container">
  <div class="regular light-grey">
    <a href="{{route('admin::stores.profile', $store->id)}}"><i class="fa fa-angle-left" aria-hidden="true"></i> {{$store->name}} </a>
  </div>
  <div class="margin-t-10">
    <div class="flex gutter-between">
      <div class="h2 bold">
        Transactions
      </div>
      <div>
        {{--<button class="btn btn-primary" data-modal="#add-transaction">Add Transaction</button>--}}
        <a class="btn btn-primary" href="{{route('admin::transactions.create', ['store_id' => $store->id])}}">Add Transaction</a>
      </div>
    </div>
  </div>
  <div class="margin-t-5">
    Store: {{$store->name}}
  </div>
  <div class="margin-t-5">
    Beat: {{$store->beat->name}}
  </div>
  <div class="margin-t-5">
    Total Outstanding: <span class="bold">Rs. {{!empty($store->totalBalance) ? $store->totalBalance->balance_amount : 0}}</span>
  </div>
  <div class="margin-t-5">
    Uncleared Amount: <span class="bold">Rs. {{!empty($store->totalPendingBalance) ? number_format($store->totalPendingBalance->balance_amount) : 0}}</span>
  </div>
  <div class="margin-t-5">
    Credit Limit:  <span class="bold">Rs. {{$store->credit_limit_value}}</span>
  </div>
  @include('admin.grid')
</div>
{{--@include('admin.transaction._form')--}}
@endsection

@section('js')
<script type="text/javascript">
$('.payment-method').on('change', function(e) {
      var lookup = {!! json_encode($mappedPaymentMethods) !!};
      let selector = $(this).val();
      $(".payment-type").empty();
      let credit = lookup['credit'];
      let debit = lookup['debit'];
      if (credit.includes(selector)) {
          $('.payment-type').append("<option value='credit'>credit</option>");
      } else if (debit.includes(selector)) {
          $('.payment-type').append("<option value='debit'>debit</option>");
      } else {
          $('.payment-type').append("<option value=''>Please select a payment method first</option>");
      }
});
</script>
@endsection
