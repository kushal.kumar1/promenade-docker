<div class="margin-t-100">
    <div class="flex settle-invoices hidden">
    </div>
    <div class='flex form-control settle-invoices-display' contenteditable="true">
    </div>
    @if (empty($invoices))
      <span>No invoices found!</span>
    @else
      <div class="grid">
        <table>
            <thead>
              <tr>
                <th>
                  Number
                </th>
                <th>
                  Financial Year
                </th>
                <th>
                  Order ID
                </th>
                <th>
                  Invoice Amount
                </th>
                <th>
                  Reamining Settlement Amount
                </th>
                <th>
                  Settlement Type
                </th>
                <th>
                  Settlement Amount
                </th>
              </tr>
            </thead>
        @foreach ($invoices as $invoice)
              <tbody>
                <tr>
                  <td>
                    {{ $invoice->reference_id }}
                  </td>
                  <td>
                    {{ $invoice->financial_year }}
                  </td>
                  <td>
                    {{ $invoice->order_id }}
                  </td>
                  <td>
                    &#8377; {{ $invoice->amount }}
                  </td>
                  <td>
                    <?php $remainingAmount = $invoice->amount - $invoice->invoiceTransactions->pluck('pivot')->sum('amount') ?>
                    &#8377; {{ $remainingAmount }}
                  </td>
                  <td>
                    <label class="flex cross-center">
                        <div class="radio margin-r-10">
                            <input type="radio" name="settle-type-{{ $invoice->id }}" value="full" onclick="settlementTypeFullSelect({{ $invoice->id }}, {{ $remainingAmount }})"/>
                            <span class="indicator"></span>
                        </div>
                        <span>Full</span>
                    </label>
                    <label class="flex cross-center">
                        <div class="radio margin-r-10">
                            <input type="radio" name="settle-type-{{ $invoice->id }}" value="partial" onclick="settlementTypePartialSelect({{ $invoice->id }}, {{ $remainingAmount }})"/>
                            <span class="indicator"></span>
                        </div>
                        <span>Partial</span>
                    </label>
                  </td>
                  <td>
                    <div class="flex">
                      <!-- <input class="amount-detail-{{$invoice->id}}" name="settle_invoices[{{$invoice->id}}][amount]" type="number" step="0.01"/> -->
                      <input class="amount-detail-{{$invoice->id}}" type="number" step="0.01"/>
                      <button type="button" class="btn btn-primary" onclick="addTransactionSettlementAmount({{$invoice->id}}, '{{ $invoice->reference_id }}', {{$remainingAmount}})">Add</button>
                    </div>
                  </td>
                </tr>
              </tbody>
        @endforeach
      </table>
    </div>
    @endif
</div>
