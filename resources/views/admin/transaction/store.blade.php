<div class="card">
  <div class="flex gutter">
    <div class="col-12-12">
      <div class="flex gutter-between">
        <div class="h5 bold">
          <?php
            $link="#";
            $link = route("admin::stores.profile", $store->id);
          ?>
          <a href="{{$link}}" target='_blank' class='link'>{{$store->name}}</a>
        </div>
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Address:</span>{{$store->address}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Landmark:</span>{{$store->landmark}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Pincode:</span>{{$store->pincode}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">BEAT:</span>{{!empty($store->beat_id) ? $store->beat->name: "Null"}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">GSTIN:</span>{{!empty($store->gstin) ? $store->gstin: "No record"}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Contact Person:</span>{{!empty($store->contact_person) ? $store->contact_person: "No record"}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Contact Mobile:</span>{{!empty($store->contact_mobile) ? $store->contact_mobile: "No record"}}
      </div>
      <div class="margin-t-5">
        <span class="bold margin-r-10">Credit Allowed :</span>
        <span class="inline small tag {{$store->credit_allowed ? "tag-success" : "tag-warning"}} margin-t-5">{{$store->credit_allowed ? "Yes": "No"}}</span>
      </div>
      {{-- <hr class="hr hr-light margin-t-20" /> --}}
      @if($store->lat && $store->lng)
        <div class="margin-t-20">
          <div class="bold">
            Google Location:
          </div>
          {{$store->location}}
        </div>
      @endif
      <div class="margin-t-10">
        @if($store->status)
          <div class="inline small tag tag-success margin-t-5"><i class="fa fa-check" aria-hidden="true"></i> Active
          </div>
          @else
          <div class="inline small tag tag-warning margin-t-5"><i class="fa fa-times" aria-hidden="true"></i> Inactive
          </div>
          @endif
          @if($store->verified)
            <div class="inline small tag tag-success margin-t-5"><i class="fa fa-check" aria-hidden="true"></i> Verified
            </div>
            @else
            <div class="inline small tag tag-warning margin-t-5"><i class="fa fa-times" aria-hidden="true"></i> Unverified
            </div>
            @endif
      </div>
    </div>
  </div>
  <hr class="hr hr-light margin-t-20" />
  <div class="margin-t-20">
    <div class="flex gutter text-center">
      <div class="col-4-12">
        Credit Limit @if(!empty($store->creditLimit))<span class="tag uppercase small">{{$store->creditLimit->type}}</span>@endif
        <div class="margin-t-10 bold">
          Rs. {{number_format($store->credit_limit_value)}}
        </div>
      </div>
      <div class="col-4-12">
        Total Outstanding<br />
        <div class="margin-t-10 bold">
          Rs. {{!empty($store->totalBalance) ? number_format($store->totalBalance->balance_amount) : 0}}
        </div>
      </div>
      <div class="col-4-12">
        Uncleared Balance<br />
        <div class="margin-t-10 bold">
          Rs. {{!empty($store->totalPendingBalance) ? number_format($store->totalPendingBalance->balance_amount) : 0}}
        </div>
      </div>
    </div>
  </div>
</div>
