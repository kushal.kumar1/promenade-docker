@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Stock Transfer Items <br>
                <span class="h6 tag tag-completed">Status : {{strtoupper($stockTransfer->status)}}</span>
            </div>
            <div>
                <form class="complete-transfer-form form" action="{{route('admin::stock-transfers.complete')}}" method="POST">
                    <input type="hidden" name="id" value="{{$stockTransfer->id}}">
                </form>
                <form class="confirm-transfer-form form" action="{{route('admin::stock-transfers.confirm')}}" method="POST">
                    <input type="hidden" name="id" value="{{$stockTransfer->id}}">
                </form>
                @if($stockTransfer->to_warehouse_id == app(\Niyotail\Helpers\MultiWarehouseHelper::class)->getSelectedWarehouse())
                    <button class="btn btn-success complete-transfer" @if(!$stockTransfer->canComplete()) disabled="disabled" @endif>Complete Transfer</button>
                @else
                    <button class="btn btn-success confirm-transfer" @if(!$stockTransfer->canConfirm()) disabled="disabled" @endif>Confirm Transfer</button>
                @endif
                <form action="{{route('admin::stock-transfers.pdf')}}" style="display: inline">
                    <input type="hidden" name="id" value="{{$stockTransfer->id}}">
                    <button type="submit" class="btn btn-primary" @if($stockTransfer->status == \Niyotail\Models\StockTransfer::STATUS_INITIATED) disabled="disabled" @endif>Download PDF</button>
                </form>

            </div>
        </div>
        @include('admin.grid')
    </div>
@endsection

@section('js')
    <script>
        $('.complete-transfer').on('click', function (){
           let $form = $('.complete-transfer-form');
           dialog("Complete Transfer?", 'Are you sure you want to complete this transfer?', function (){
              $form.submit();
           });
        });
        $('.confirm-transfer').on('click', function (){
            let $form = $('.confirm-transfer-form');
            dialog("Confirm Transfer?", 'Are you sure you want to confirm this transfer?', function (){
                $form.submit();
            });
        });
    </script>
@endsection