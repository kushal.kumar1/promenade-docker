@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Outbound Stock Transfer
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-stock-transfer-add">Create Transfer</button>
            </div>
        </div>
        @include('admin.grid')

        <div id="modal-stock-transfer-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h5">Create Stock Transfer</h5>
                <form class="form margin-t-30" method="post" action="{{route('admin::stock-transfer.start')}}"
                      autocomplete="off">
                    <label class="label">Destination Warehouse</label>
                    <select class="form-control" name="warehouse_id">
                        @foreach ($warehouses as $warehouse)
                            <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                        @endforeach
                    </select>
                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>

        <div id="modal-stock-transfer-update" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h5">Update Stock Transfer</h5>
                <form class="form margin-t-30" method="post" action="{{route('admin::stock-transfer.update')}}"
                      autocomplete="off">
                    <input type="hidden" name="id"  id="stock-transfer-id">
                    <label class="label">Destination Warehouse</label>
                    <input type="text" class="form-control" disabled="disabled" id="destination">
                    <label class="label margin-t-5">Vehicle Number</label>
                    <input type="text" class="form-control"  id="vehicle_number" name="vehicle_number">
                    <label class="label margin-t-5">EWAY Bill Number</label>
                    <input type="text" class="form-control"  id="eway_bill_no" name="eway_bill_no">
                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $('#modal-stock-transfer-update').on('modalOpened', function (event, relatedTarget){
            $(this).find('#stock-transfer-id').val($(relatedTarget).data('id'));
            $(this).find('#destination').val($(relatedTarget).data('destination'));
            $(this).find('#vehicle_number').val($(relatedTarget).data('vehicle_number'));
            $(this).find('#eway_bill_no').val($(relatedTarget).data('eway_bill_no'));
        });
    </script>
@endsection