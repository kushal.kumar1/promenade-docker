@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Bank
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-bank-add">Add Bank</button>
            </div>
        </div>
        @include('admin.bank._partials.bank-modal')
        {{--modal for editing bank--}}
        <div id="modal-bank-edit" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 text-center">Edit Bank</h5>
                <form class="form" method="post" action="{{route('admin::bank.update')}}" autocomplete="off">
                    <input type="hidden" id="bank_id" name="id">
                    <div class="margin-t-30">
                        <div class="flex gutter">
                            <div class="col-12-12">
                                <label class="label">Name</label>
                                <input readonly class="form-control" name="name" id='name' value=""/>
                            </div>
                            <div class="col-12-12">
                                <label class="label">Is Active</label>
                                <select class="form-control" name="is_active" id="is_active">
                                    <option value="1"> Active</option>
                                    <option value="0"> Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>

        @include('admin.grid')
        <script>
            $('#modal-bank-edit').on('modalOpened', function (event, relatedTarget) {
                $(this).find('#bank_id').val($(relatedTarget).data('id'));
                $(this).find('#name').val($(relatedTarget).data('name'));
                $(this).find('#is_active').val($(relatedTarget).data('is_active'));
            });
            $('#modal-bank-add').on('modalClosed', function (event, relatedTarget) {
                $(this).find('.form').trigger('reset');
            });
        </script>
    </div>
@endsection