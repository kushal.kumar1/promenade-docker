<div id="modal-bank-add" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text-center">Add Bank</h5>
        <form class="form" method="post" action="{{route('admin::bank.store')}}" autocomplete="off">
            {{csrf_field()}}
            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Name</label>
                        <input class="form-control" name="name" value=""/>
                    </div>
                    <div class="col-12-12">
                        <label class="label">Is Active</label>
                        <select class="form-control" name="is_active">
                            <option value="1"> Active</option>
                            <option value="0"> Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Save</button>
            </div>
        </form>
    </div>
</div>