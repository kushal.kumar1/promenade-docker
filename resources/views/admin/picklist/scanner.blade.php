@extends('admin.picklist.layout')

@section('css')
    <style>
        .picked-items-overlay, .suggestions-overlay {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.6);
        }

        .picked-items-overlay .icon-loading, .suggestions-overlay .icon-loading {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

    </style>
@append


@section('main-section')
    <div class="margin-t-20">
        <div class="flex gutter no-wrap">
            <div class="col-6-12">
                <div class="card">
                    <div>
                        <label class="label">Scan Products by barcode</label>
                        <input id="item-search" type="text" class="form-control" data-id="{{$picklist->id}}" autocomplete="off"/>
                    </div>
                    <div id="item-suggestions" class="relative">
                    </div>
                </div>
            </div>

            <div class="col-6-12">
                <div class="card relative">
                    <div class="regular bold h5 uppercase">Picked Items</div>
                    <hr class="hr hr-light margin-t-20"/>
                    <div id="picked-items" class="relative">
                        @include('admin.picklist.scanner.picked_items')
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('admin.picklist.scanner.skip')
@endsection
@section('js')
    <script type="text/javascript" src="{{mix('admin/js/picklist.js')}}"></script>
@endsection