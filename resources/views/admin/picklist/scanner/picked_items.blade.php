<div class="picked-items-overlay hidden">
    <div class="icon-loading">
        <i class="fa fa-2x fa-spinner fa-spin"></i>
    </div>
</div>
@if($pickedProducts->isNotEmpty())
@foreach($pickedProducts as $product)
<div class="item margin-t-20" data-product_id="{{$product->id}}">
    <div class="flex gutter">
        <div class="col-10-12">
            <div class="flex no-wrap">
                <img src="{{Image::getSrc($product->featuredImage, 80)}}" class="margin-r-10"/>
                <div>
                    <h6 class="regular">{{$product->name}}</h6>
                    <div class="margin-t-5 margin-b-5">
                        <div class="small">
                            <span class="bold">PID:</span> <span>{{$product->id}}</span>
                            | <span class="bold">Barcode: </span> <span>{{$product->barcode}}</span>
                        </div>

                        <div class="margin-t-5">
                            <span class="tag tag-success margin-r-10">Picked Qty: {{$product->picklistItems->where('status', 'picked')->count()}}</span>

                        @if($product->picklistItems->where('status', 'skipped')->count() > 0)
                            <span class="tag tag-cancelled margin-r-10">Skipped Qty: {{$product->picklistItems->where('status', 'skipped')->count()}}</span>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-2-12">
            <button class="btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></button>
{{--            <label class="label bold">Picked Qty</label>--}}
{{--            <div class="flex gutter-between">--}}
{{--                <div class="flex no-wrap">--}}
{{--                    <input type="number" min="1" class="form-control quantity margin-r-10" value="{{$product->picklistItems->where('status', 'picked')->count()}}" data-old="{{$product->picklistItems->where('status', 'picked')->count()}}" autocomplete="off"/>--}}
{{--                    <button class="btn btn-remove"><i class="fa fa-trash" aria-hidden="true"></i></button>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
    <hr class="hr hr-light"/>
</div>
@endforeach
@else
    <div class="item padding-30 text-center">
        No Product scanned!
    </div>
@endif
@include('admin.picklist.scanner.skip_items')