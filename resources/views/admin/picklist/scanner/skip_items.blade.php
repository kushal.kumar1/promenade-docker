<div id="modal-skip-items" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text- bold">Skip Product</h5>
        <form class="form" method="post" action="#" autocomplete="off">
            <div class="margin-t-30">
                <div class="flex gutter-sm">
                    <div class="col-4-12">
                        Product
                    </div>
                    <div class="col-2-12">
                        Qty
                    </div>
                    <div class="col-4-12">
                        Reason
                    </div>
                </div>
                <div class="flex gutter-sm">
                    <div class="col-4-12">
                        Britannia Chocolate Cake 55g Pouch Rs 15
                    </div>
                    <div class="col-2-12">
                        <input type="quantity" value="3" name="quantity" class="form-control"/>
                    </div>
                    <div class="col-4-12">
                        <select name="reason" class="form-control">
                            @foreach(\Niyotail\Models\PicklistItem::getConstants('SKIP') as $skipReason)
                                <option value="{{$skipReason}}">{{strtoupper($skipReason)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-2-12">Add Row</div>
                </div>
            </div>
            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Save</button>
            </div>
        </form>
    </div>
</div>
