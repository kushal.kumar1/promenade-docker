<div class="suggestions-overlay hidden">
    <div class="icon-loading">
        <i class="fa fa-2x fa-spinner fa-spin"></i>
    </div>
</div>
<hr class="hr hr-light margin-t-20"/>
@if($products->isNotEmpty())
    @foreach($products as $product)
        <div class="item margin-t-20">
            <div class="flex gutter-sm">
                <div class="col-2-12" style="height:70px;">
                    <img src="{{Image::getSrc($product->featuredImage, 80)}}" width="80"/>
                </div>
                <div class="col-10-12">
                    <div class="flex gutter-between">
                        <div>
                            <h6 class="regular">{{$product->name}}</h6>
                            <div class="margin-t-5 small">
                                <span class="bold"> PID: </span><span>{{$product->id}}</span> |
                                <span class="bold">Barcode: </span><span>{{$product->barcode}}</span>
                            </div>
                        </div>
                        <div>
                            <span class="tag">Pending Qty: {{$product->picklist_items_count}}</span>
                        </div>
                    </div>
                    <div class="margin-t-20">
                        <form>
                            <input type="hidden" name="picklist_id" value="{{$picklistId}}"/>
                            <input type="hidden" name="product_id" value="{{$product->id}}"/>
                            <div class="flex gutter-between cross-end">
                                <div class="col-6-12">
                                    <label class="label bold">Picked Qty</label>
                                    <input type="number" class="form-control quantity" min="1"
                                           max="{{$product->picklist_items_count}}" name="quantity" required/>
                                </div>
                                <div class="col-6-12 text-right">
                                    <button type="button" class="btn js-add-skip-row btn-sm">+ SKIP ITEMS</button>
                                </div>
                            </div>
                            <div id="js-skip-rows"></div>

                            <div class="margin-t-20">
                                <button type="button" class="btn js-btn-submit btn-success">SUBMIT</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div class="margin-t-20">
        No Products Found
    </div>
@endif