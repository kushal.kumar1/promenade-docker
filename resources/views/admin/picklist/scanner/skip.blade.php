<div class="templates hidden">
    <div class="skip-row margin-t-30">
        <div class="flex gutter-sm cross-end">
            <div class="col-4-12">
                <label class="label bold">Skipped Qty</label>
                <input type="number" class="form-control" min="1" name="skip[{# index #}][quantity]" required/>
            </div>
            <div class="col-6-12">
                <label class="label bold">Reason</label>
                <select name="skip[{# index #}][reason]" class="form-control" required>
                    @foreach(\Niyotail\Models\PicklistItem::getConstants('SKIP') as $skipReason)
                        <option value="{{$skipReason}}">{{strtoupper($skipReason)}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-2-12">
                <button class="btn btn-sm js-delete-skip-row" type="button"><i class="fa fa-trash"
                                                                               aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>