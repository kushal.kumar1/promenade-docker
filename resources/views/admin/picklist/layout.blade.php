@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between cross-center">
            <div>
                <div class="regular light-grey">
                    <a href="{{route('admin::picklists.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i>
                        Picklists</a>
                </div>

                <div class="flex cross-center margin-t-10">
                    <div class="h1 margin-r-20 bold">
                        #{{$picklist->id}}
                    </div>
                    <span class="tag tag-{{$picklist->status}}">{{$picklist->status}}</span>
                </div>

                <div class="margin-t-10">
                    <span class="bold uppercase">Picker:</span> {{empty($picklist->picker) ? "None" : $picklist->picker->name}}
                </div>
            </div>

            <div>
                <div class="flex">
                    <a href="{{route('admin::picklists.pdf', $picklist->id)}}" class="btn btn-primary margin-r-10"
                       target="_blank">
                        <i class="fa fa-print" aria-hidden="true"></i> Print
                    </a>
                    @if($picklist->canClose())
                    <form class="form margin-r-10" method="post" action="{{route('admin::picklists.close')}}"
                          data-destination="{{route('admin::picklists.shipments',$picklist->id)}}">
                        <input type="hidden" name="id" value="{{$picklist->id}}"/>
                        <button class="btn btn-success">
                            Close Picklist
                        </button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
        <ul class="tabs">
            <li class="tab {{\Illuminate\Support\Facades\Route::currentRouteName() == "admin::picklists.items" ? "active" : ""}}">
                <a href="{{route('admin::picklists.items', ['id' => $picklist->id])}}">Items</a></li>
            @if($picklist->allowScan())
            <li class="tab {{\Illuminate\Support\Facades\Route::currentRouteName() == "admin::picklists.scanner" ? "active" : ""}} ">
                <a href="{{route('admin::picklists.scanner', ['id' => $picklist->id])}}">Scanner</a></li>
            @endif
            <li class="tab {{\Illuminate\Support\Facades\Route::currentRouteName() == "admin::picklists.shipments" ? "active" : ""}} ">
                <a href="{{route('admin::picklists.shipments',['id' => $picklist->id])}}">Shipments</a></li>
        </ul>

        @yield('main-section')
    </div>

    @include('admin.picklist.partials._add_agent')
    @include('admin.picklist.partials._add_employee')
@endsection
`
@section('js')
    <script type="text/javascript">
        @if(empty($picklist->picker))
        Modal.show($('#modal-assign-picker'));
        @endif

        $('.picker-search').select2({
            minimumInputLength: '0',
            ajax: {
                url: '/search/pickers',
                dataType: 'json',
                processResults: (data) => {
                    data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = `${obj.name} (Picklists Assigned: ${obj.picklists_count})`;
                        return obj;
                    });

                    return {
                        results: data
                    };
                }
            }
        })
    </script>
@endsection
