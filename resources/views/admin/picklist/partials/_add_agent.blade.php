<div id="modal-add-agent" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text-center">Assign Agent</h5>
        <form class="form" method="post" action="{{route('admin::picklists.agent.update')}}" autocomplete="off">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$picklist->id}}"/>
            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Agent</label>
                        <select class="form-control agent-search" type="text" name="agent_id">
                        </select>
                    </div>
                </div>
            </div>
            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Save</button>
            </div>
        </form>
    </div>
</div>