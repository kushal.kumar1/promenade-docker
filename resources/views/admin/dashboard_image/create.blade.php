@extends('admin.master')

@section('css')
  <link rel="stylesheet" href="/plugins/zebra-datetimepicker/css/bootstrap/zebra_datepicker.min.css">
@endsection

@section('content')
<div class="container">
  <form class="form-app-banner form form-multipart" method="post" action="{{route('admin::dashboard-image.store')}}" data-destination="{{route('admin::dashboard-image.edit', ['id' => "#id#"])}}">
    <div class="header-actions">
      <button type="button" class="btn btn-discard margin-r-10">Discard</button>
      <button type="submit" class="btn btn-primary" data-form=".form-app-banner">Save</button>
    </div>
    {{csrf_field()}}
    <div class="regular light-grey">
      <a href="{{route('admin::dashboard-image.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Agent Dashboard Images</a>
    </div>
    <div class="h1 margin-t-10">
      Add Image
    </div>
    @include('admin.dashboard_image.form')
  </form>
</div>
@endsection

@section('js')
<script src="/plugins/zebra-datetimepicker/zebra_datepicker.min.js"></script>
<script>
  $('.valid_from').Zebra_DatePicker({
      format: 'Y-m-d',
      show_icon: false,
      default_position: 'below'
  });
</script>

@endsection
