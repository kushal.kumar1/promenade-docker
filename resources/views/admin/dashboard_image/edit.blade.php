@extends('admin.master')

@section('css')
  <link rel="stylesheet" href="/plugins/zebra-datetimepicker/css/bootstrap/zebra_datepicker.min.css">
@endsection

@section('content')
<div class="container">
  <div class="header-actions">
    <button type="button" class="btn btn-discard margin-r-10">Discard</button>
    <button type="button" class="btn btn-primary form-button" data-form=".form-app-banner">Save</button>
  </div>
  <div class="regular light-grey">
    <a href="{{route('admin::dashboard-image.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Dashboard Images</a>
  </div>
  <div class="h3 margin-t-10 bold">
  </div>
  <form class="form form-app-banner form-multipart" method="post" action="{{route('admin::dashboard-image.update')}}" data-actions=".header-actions" autocomplete="off">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{$dashboardImage->id}}" />
    @include('admin.dashboard_image.form')
  </form>
</div>
@endsection

@section('js')
<script src="/plugins/zebra-datetimepicker/zebra_datepicker.min.js"></script>
<script>
  $('.valid_from').Zebra_DatePicker({
      format: 'Y-m-d H:i',
      direction: 1,
      show_icon: false,
      default_position: 'below'
  });
</script>

@endsection
