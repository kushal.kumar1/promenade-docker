<div class="margin-t-20">
  <div class="flex gutter">
    <div class="col-4-12">
      <h2 class="h6 bold margin-t-10">Upload Banner</h2>
      <p class="margin-t-10">
        Upload Image and change status for Agent dashboard image.
      </p>
    </div>
    <div class="col-8-12">
      <div class="card">
        <div>
          @if(!empty($dashboardImage))
          <img src="{{Image::getSrc($dashboardImage, 300)}}" class="bd" />
          @endif
          <div {{!empty($dashboardImage) ? 'class=margin-t-20' : ''}}>
            <label class="label">Upload Image</label>
            <input type="file" name="file" class="form-control">
            <span class="error error-file"></span>
          </div>
        </div>
        <div class="margin-t-20">
          <div class="flex gutter">
            <div class="col-6-12">
              <label class="label">Valid From</label>
              <input name="valid_from" class="form-control valid_from" @if(!empty($dashboardImage)) value="{{$dashboardImage->valid_from}}"
              @endif />
              <span class="error error-valid_from"></span>
            </div>
          </div>
        </div>
        <div class="margin-t-20">
          <div class="flex gutter">
            <div class="col-6-12">
              <label class="label">Status</label>
              <select class="form-control" name="status">
                <option value="1" @if(!empty($dashboardImage) && $dashboardImage->status == 1) selected @endif>Active</option>
                <option value="0" @if(!empty($dashboardImage) && $dashboardImage->status == 0) selected @endif>Inactive</option>
              </select>
              <span class="error error-status"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
