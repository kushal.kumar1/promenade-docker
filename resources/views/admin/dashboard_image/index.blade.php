@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Agent Dashboard Images
        </div>
        <div>
          <a href="{{route('admin::dashboard-image.create')}}">
            <button class="btn btn-primary">Add Image</button>
          </a>
        </div>
    </div>
    @include('admin.grid')
@endsection
