@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Payments
            </div>
            <div>
                {{-- <a href="{{route('admin::taxes.create')}}"> <button class="btn btn-primary">Add Tax</button></a> --}}
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection
