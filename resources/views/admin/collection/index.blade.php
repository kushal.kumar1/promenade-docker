@extends('admin.master')

@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="h1">
            Categories
        </div>
        <div class="margin-t-30">
            <div class="flex gutter">
                <div class="col-4-12">
                    <div class="card">
                        @include('admin.collection._tree', ['collections' => $collectionTree])
                    </div>
                </div>
                <div class="col-8-12">
                    <div id="container-collection" data-action-create="{{route('admin::collections.store')}}" data-action-update="{{route('admin::collections.update')}}">

                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('admin.templates.collection')

@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/collection.js')}}"></script>
@endsection
