<div class="card custom">
    <div class="card-header">

     @if (!isset($minimal))
        <div class="flex">
            <div class="col-12-12">
                <p class="regular">Showing based on order from
                    <strong id="startDate">{{$filters['start_date']}}</strong>
                    to
                    <strong id="endDate">{{$filters['end_date']}}</strong>
                </p>
            </div>

            <div class="col-12-12 margin-t-20">

                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Stores</strong>
                    <br>
                    <span id="selected-stores">
                    @isset($stores)
                            @foreach($stores as $store)
                                <span data-type="store" data-id="{{$store['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"><i
                                            class="fa fa-times"></i> {{substr($store['name'], 0, 10)}}</span>
                            @endforeach
                        @endisset
                    </span>
                </div>

                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Brands</strong>
                    <br>
                    <span id="selected-brands">
                            @isset($brands)
                            @foreach($brands as $brand)
                                <span data-type="brand" data-id="{{$brand['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"><i
                                            class="fa fa-times"></i> {{$brand['name']}}</span>
                            @endforeach
                        @endisset
                        </span>
                </div>

                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Beat</strong>
                    <br>
                    <span id="selected-beats">
                            @isset($beats)
                            @foreach($beats as $beat)
                                <span data-type="beat" data-id="{{$beat['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"><i
                                            class="fa fa-times"></i> {{$beat['name']}}</span>
                            @endforeach
                        @endisset
                        </span>
                </div>


                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Products</strong>
                    <br>
                    <span id="selected-products">
                            @isset($products)
                            @foreach($products as $product)
                                <span data-type="product" data-id="{{$product['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"
                                      title="{{$product['name']}}"><i class="fa fa-times"></i> {{substr($product['name'], 0, 15)}}</span>
                            @endforeach
                        @endisset
                        </span>
                </div>


                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Collections</strong>
                    <br>
                    <span id="selected-collections">
                            @isset($collections)
                            @foreach($collections as $collection)
                                <span data-type="collection" title="{{$collection['name']}}"
                                      data-id="{{$collection['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"><i
                                            class="fa fa-times"></i> {{substr($collection['name'], 0, 15)}}</span>
                            @endforeach
                        @endisset
                        </span>
                </div>


                <div class="col-2-12 pull-left margin-t-20 selectedFilters">
                    <strong>Marketers</strong>
                    <br>
                    <span id="selected-marketers">
                            @isset($marketers)
                            @foreach($marketers as $marketer)
                                <span data-type="marketer" title="{{$marketer['name']}}" data-id="{{$marketer['id']}}"
                                      class="remove-filter margin-t-5 btn btn-sm btn-primary"><i
                                            class="fa fa-times"></i> {{substr($marketer['name'], 0, 15)}}</span>
                            @endforeach
                        @endisset
                        </span>
                </div>

            </div>
        </div>
        @endif

        <form method="get" action="{{$route}}" id="filterForm" autocomplete="off" @if(!isset($minimal)) class="margin-t-40" @endif>
            <input type="hidden" name="start_date" id="start_date" value="{{ $filters['start_date'] }}"/>
            <input type="hidden" name="end_date" id="end_date" value="{{ $filters['end_date'] }}"/>

            <div class="flex gutter-between">
                <div class="filters regular">
                    <div class="filter">
                        <div class="flex gutter-between">
                            <span class="filter-heading">DATE RANGE</span>
                        </div>

                        <div id="daterange">
                            <span></span>
                            <input class="form-control" type="text" disabled>
                        </div>
                    </div>

                    <div class="filter">
                        <div class="flex gutter-between">
                            <span class="filter-heading">STORE TYPE</span>
                        </div>

                        <select name="store_type" id="store_type" class="form-control" style="width:100%;">
                        </select>
                    </div>

                    <div class="filter">
                        <div class="flex gutter-between">
                            <span class="filter-heading">BEATS</span>
                        </div>

                        <select multiple name="beat_ids[]" id="beats-filter" class="form-control filter_input"
                                style="width:100%;">
                            @isset($beats)
                                @foreach($beats as $beat)
                                    <option selected="selected" value="{{$beat['id']}}">{{$beat['name']}}
                                        , {{$beat['long_name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="filter">
                        <div class="flex gutter-between">
                            <span class="filter-heading">STORES</span>
                        </div>

                        <select multiple name="store_ids[]" id="stores-filter" class="form-control filter_input"
                                style="width:100%;">
                            @isset($stores)
                                @foreach($stores as $store)
                                    <option selected="selected" value="{{$store['id']}}">{{$store['name']}}
                                        , {{$store['address']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                </div>
            </div>

            <div class="filters regular margin-t-20">
                <div class="filter">
                    <div class="flex gutter-between">
                        <span class="filter-heading">MARKETERS</span>
                    </div>

                    <select multiple name="marketer_ids[]" id="marketers-filter" class="form-control filter_input"
                            style="width:100%;">
                        @isset($marketers)
                            @foreach($marketers as $marketer)
                                <option selected="selected" value="{{$marketer['id']}}">{{$marketer['name']}}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>

                <div class="filter">
                    <div class="flex gutter-between">
                        <span class="filter-heading">BRANDS</span>
                    </div>

                    <select multiple name="brand_ids[]" id="brands-filter" class="form-control filter_input"
                            style="width:100%;">
                        @isset($brands)
                            @foreach($brands as $brand)
                                <option selected="selected" value="{{$brand['id']}}">{{$brand['name']}}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>

                <div class="filter">
                    <div class="flex gutter-between">
                        <span class="filter-heading">COLLECTIONS</span>
                    </div>

                    <select multiple name="collection_ids[]" id="collections-filter" class="form-control filter_input"
                            style="width:100%;">
                        @isset($collections)
                            @foreach($collections as $collection)
                                <option selected="selected"
                                        value="{{$collection['id']}}">{{$collection['name']}}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>

                <div class="filter">
                    <div class="flex gutter-between">
                        <span class="filter-heading">PRODUCTS</span>
                    </div>

                    <select multiple name="product_ids[]" id="products-filter" class="form-control filter_input"
                            style="width:100%;">
                        @isset($products)
                            @foreach($products as $product)
                                <option selected="selected" value="{{$product['id']}}">{{$product['name']}}</option>
                            @endforeach
                        @endisset
                    </select>
                </div>

            </div>

            @if (!isset($minimal))
                <div class="margin-t-20">
                    <button type="submit" id="submitFilterForm" class="btn btn-primary button-success small">UPDATE
                        FILTERS
                    </button>
                </div>
            @endif

        </form>
    </div>
</div>


<script>
    let queryParams = {
        start_date: '{{ $filters['start_date'] }}',
        end_date: '{{ $filters['end_date'] }}'
    };

    const getDataFromAPI = async (href, params = {}, type = 'GET', data = [], isIncludeQueryParams = true) => {
        if (isIncludeQueryParams) {
            href += '?' + $.param(queryParams)

            if (!$.isEmptyObject(params)) {
                href += '&' + $.param(params)
            }
        }

        try {
            const response = await $.ajax(href, {
                type,
                data,
                cache: false,
                traditional: true
            });

            if (response) return response
        } catch (err) {
            return err;
        }
    };

    const hideEmptySelection = () => {
        $(".selectedFilters").each(function(){
            if(!$(this).find('.remove-filter').length)
            {
                $(this).hide();
            }
            else
            {
                $(this).show();
            }
        })
    };
</script>

<script>
    $(function () {

        const dateRangeHolder = $('#daterange');

        var selectedDateTime = '{{\Carbon\Carbon::parse($filters["start_date"])->format("Y-m-d")}}';
        selectedDateTime += ' - {{\Carbon\Carbon::parse($filters["end_date"])->format("Y-m-d")}}';
        dateRangeHolder.find('input').val(selectedDateTime);


        // - Date Range Picker
        function dateRangeCallback(startDate, endDate) {
            queryParams['start_date'] = startDate.format('YYYY-MM-DD');
            queryParams['end_date'] = endDate.format('YYYY-MM-DD');

            dateRangeHolder.find('input').val(queryParams['start_date'] + ' - ' + queryParams['end_date']);
            $('#start_date').val(queryParams['start_date'])
            $('#end_date').val(queryParams['end_date'])

            $('#startDate').html(queryParams['start_date'])
            $('#endDate').html(queryParams['end_date'])

            $('form#filterForm').submit();
        }

        dateRangeHolder.daterangepicker({
            startDate: moment(),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                format: 'YYYY-MM-DD'
            },
            maxDate: '{{ date('Y-m-d') }}'
        }, dateRangeCallback);

        // - Initialize Select Search
        const beatsFilterHolder = $('#beats-filter');
        const productsFilterHolder = $('#products-filter');
        const storesFilterHolder = $('#stores-filter');
        const brandsFilterHolder = $('#brands-filter');
        const collectionsFilterHolder = $('#collections-filter');
        const marketersFilterHolder = $('#marketers-filter');

        brandsFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Brands',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=brands&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: brand => {
                if (!brand.id) {
                    return brand.text;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${brand.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: store => store.text || store.name,
        });

        productsFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Products',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=products&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: product => {
                if (!product.id) {
                    return product.text;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${product.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: product => product.text || product.name,
        });

        beatsFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Beats',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=beats&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: beat => {
                if (!beat.id) {
                    return beat.text;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${beat.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: beat => beat.text || beat.name,
        });

        storesFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Stores',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=stores&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: store => {
                if (!store.id) {
                    return store.text;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${store.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: store => store.text || store.name,
        });

        collectionsFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Categories',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=collections&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: collection => {
                if (!collection.id) {
                    return collection.name;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${collection.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: collection => collection.text || collection.name,
        });

        marketersFilterHolder.select2({
            multiple: true,
            minimumInputLength: 2,
            placeholder: 'Enter & Select Marketers',
            ajax: {
                url: function () {
                    return '{{route("admin::dashboard.filters")}}?type=marketers&' + $.param(queryParams);
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: marketers => {
                if (!marketers.id) {
                    return marketers.name;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${marketers.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: marketers => marketers.text || marketers.name,
        });

        hideEmptySelection();

        $('#store_type').on('change', function (e) {
            queryParams['store_type'] = $(this).val();
        });

        // - DOM Event Handlers
        $('.filter_input').on('change', function (e) {
            const name = e.target.name.replace('[]', '')
            queryParams[name] = $(this).val();

            var selectedSelectId = '';
            var dataType = '';
            switch (name) {
                case 'store_ids':
                    selectedSelectId = '#selected-stores';
                    dataType = 'store';
                    break;
                case 'marketer_ids':
                    selectedSelectId = '#selected-marketers';
                    dataType = 'marketer';
                    break;
                case 'brand_ids':
                    selectedSelectId = '#selected-brands';
                    dataType = 'brand';
                    break;
                case 'product_ids':
                    selectedSelectId = '#selected-products';
                    dataType = 'product';
                    break;
                case 'beat_ids':
                    selectedSelectId = '#selected-beats';
                    dataType = 'beat';
                    break;
                case 'collection_ids':
                    selectedSelectId = '#selected-collections';
                    dataType = 'collection';
                    break;
            }

            var selectedStores = $(this).select2('data');
            var html = '';
            for (var i = 0; i < selectedStores.length; i++) {
                var thisStore = selectedStores[i];
                var storeName = thisStore.text || thisStore.name;
                html += '<span data-type="' + dataType + '" data-id="' + thisStore.id + '" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;' + storeName.substr(0, 15) + '</span>';
            }

            $(selectedSelectId).html(html);
            $(selectedSelectId).parent(".selectedFilters").show();
            hideEmptySelection();
            $('form#filterForm').submit();
        });



        const fetchStoreTypes = async () => {
            try {
                const data = await getDataFromAPI('{{route("admin::dashboard.filters")}}', {
                    'type': 'store_types'
                });

                if (data && data.length) {
                    let html = [];
                    var storeType = '{{$store_type}}';

                    $.each(data, (key, value) => {
                        let text = '';
                        if (value == 'all') {
                            text = 'All Stores';
                        } else if (value == 'retail') {
                            text = 'Retail/Other Stores'
                        } else if (value == '1k') {
                            text = '1K Stores';
                        } else if (value == 'club_1k') {
                            text = 'Club 1K Store';
                        }

                        if (storeType == value) {
                            html.push(`<option value="${value}" selected="selected">${text}</option>`);
                        } else {
                            html.push(`<option value="${value}">${text}</option>`);
                        }

                        $('#store_type').html(html.join(''));
                        queryParams['store_type'] = storeType;
                    });
                }
            } catch (err) {
                console.log('Something went wrong.');
            }
        };

        fetchStoreTypes();
    });


    $(document).on('click', '.remove-filter', function () {
        var filterType = $(this).attr("data-type");
        var id = $(this).attr("data-id");

        switch (filterType) {
            case "product":
                $("#products-filter").find("option[value='" + id + "']").remove();
                $("#products-filter").trigger('change');
                break;
            case "marketer":
                $("#marketers-filter").find("option[value='" + id + "']").remove();
                $("#marketers-filter").trigger('change');
                break;
            case "brand":
                $("#brands-filter").find("option[value='" + id + "']").remove();
                $("#brands-filter").trigger('change');
                break;
            case "beat":
                $("#beats-filter").find("option[value='" + id + "']").remove();
                $("#beats-filter").trigger('change');
                break;
            case "store":
                $("#stores-filter").find("option[value='" + id + "']").remove();
                $("#stores-filter").trigger('change');
                break;
            case "collection":
                $("#collections-filter").find("option[value='" + id + "']").remove();
                $("#collections-filter").trigger('change');
                break;
        }

        $(this).remove();
        $("#submitFilterForm").click();
    });
</script>