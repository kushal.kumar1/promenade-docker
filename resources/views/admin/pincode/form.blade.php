@extends('admin.master')

@section('content')
<div class="container">
    <div class="light-grey">
        <a href="{{route('admin::pincode.index')}}">
            <i class="fa fa-angle-left" aria-hidden="true"></i> Pincode
        </a>
    </div>
    <div class="h3 margin-t-10">{{empty($pincode) ? "Create Pincode" : "pincode #$pincode->id"}}</div>

    <div class="margin-t-40">
        <form class="form" method="POST"
            action="{{empty($pincode) ? route('admin::pincode.store') : route('admin::pincode.update', ['id' => $pincode->id])}}"
            data-destination="{{route("admin::pincode.index")}}">
            <!-- @if (empty($rule)) data-destination="{{route('admin::rules.detail', ['id' => '#id#'])}}" @endif> -->
            <div class="flex gutter">
                <div class="col-8-12">
                    <div class="card" id="card-basic">
                        <div class="margin-t-25">
                            <div class="flex gutter-sm">
                                <div class="col-6-12">
                                  <label class="label">Pincode</label>
                                  <input class="form-control" type="number" name="pincode"
                                    value="{{empty($pincode) ? null : $pincode->pincode}}" @if(!empty($pincode)) disabled @endif>
                                </div>

                                <div class="col-6-12">
                                  <div class="margin-t-5">
                                      <label class="label">City</label>
                                      <select class="form-control" name="city_id">
                                          <option default disabled selected>Select one of the following.</option>
                                          @foreach ($cities as $city)
                                            <option value="{{$city->id}}"  @if (!empty($pincode) && $city->id === $pincode->city_id ) selected @endif>{{$city->name}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <button type="submit" class=" btn btn-success margin-t-25">
                @if(!empty($pincode))
                    Update
                @else
                    Create
                @endif
                </button>
            
        </form>
    </div>
</div>

@endsection

@section('js')
