@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Pincodes
        </div>
        <div>
          <a href="{{route('admin::pincode.create')}}">
            <button class="btn btn-primary">Add Pincode</button>
          </a>
        </div>
    </div>
    @include('admin.grid')
@endsection