<div class="templates hidden">
    <div class="selector-select2">
        <label class="label">ANY ONE FROM</label>
        <select class="form-control"
            multiple="multiple"
            style="width:100%"
            name="conditions[{# conditionIndex #}][expressions][{# expressionIndex #}][values][]"></select>
    </div>
    <div class="flex gutter-sm selector-number">
        <div class="col-6-12">
            <label class="label">MIN VALUE</label>
            <input class="form-control" type="text"
                name="conditions[{# conditionIndex #}][expressions][{# expressionIndex #}][min_value]"
                value="{# value #}" />
        </div>
        <div class="col-6-12">
            <label class="label">MAX VALUE</label>
            <input class="form-control" type="text"
                name="conditions[{# conditionIndex #}][expressions][{# expressionIndex #}][max_value]"
                value="{# max_value #}" />
        </div>
    </div>
</div>