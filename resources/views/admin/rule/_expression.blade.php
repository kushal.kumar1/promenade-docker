<div class="templates hidden">
    <div class="rule-expression margin-t-25">
        <div class="flex gutter-sm cross-end">
            <div class="col-12-12 col-sm-3-12">
                <label class="label">ATTRIBUTE</label>
                <select class="form-control select2-expression-name"
                    name="conditions[{# conditionIndex #}][expressions][{# expressionIndex #}][name]"
                    style="width:100%">
                </select>
            </div>
            <div class="col-12-12 col-sm-auto values-wrapper"></div>
            <div class="remove-wrapper hidden">
                <button type="button" class="btn btn-remove-expression">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
        <hr class="hr hr-light margin-t-25" />
    </div>
</div>