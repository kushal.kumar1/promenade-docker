<div class="templates hidden">
    <div class="rule-condition card margin-t-40">
        <input type="hidden" name="conditions[{# conditionIndex #}][id]" value="{# id #}" />
        <div class="flex gutter-between cross-center">
            <h2 class="bold"><span class="condition-separator">OR </span>WHEN [Condition]</h2>
            <div>
                <button type="button" class="btn btn-sm btn-remove-condition hidden">
                    <i class="fa fa-remove"></i> Remove
                </button>
            </div>
        </div>
        <hr class="hr hr-light margin-t-25" />
        <div class="expressions margin-t-25"></div>
        <div class="margin-t-25 text-center">
            <button type="button" class="btn btn-sm btn-expression">
                <i class="fa fa-plus"></i> <span class="bold">AND</span>
            </button>
        </div>
    </div>
</div>
