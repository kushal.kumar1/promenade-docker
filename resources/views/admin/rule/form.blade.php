@extends('admin.master')

@section('css')
<link rel="stylesheet" href="/plugins/zebra-datetimepicker/css/bootstrap/zebra_datepicker.min.css">
<style>
    #datetime-start:read-only,
    #datetime-end:read-only {
        background-color: #fff;
    }
</style>
@append

@section('content')
<div class="container">
    <div class="light-grey">
        <a href="{{route('admin::rules.index')}}">
            <i class="fa fa-angle-left" aria-hidden="true"></i> Rules
        </a>
    </div>
    <div class="h3 margin-t-10">{{empty($rule) ? "Create Rule" : "Rule #$rule->id"}}</div>

    <div class="margin-t-40">
        <form class="form" method="POST"
            action="{{empty($rule) ? route('admin::rules.store') : route('admin::rules.detail.update', ['id' => $rule->id])}}"
            @if (empty($rule)) data-destination="{{route('admin::rules.detail', ['id' => '#id#'])}}" @endif>
            <div class="flex gutter">
                <div class="col-8-12">
                    <div class="card" id="card-basic">
                        <label class="label">NAME</label>
                        <input class="form-control h4" type="text" name="name"
                            value="{{empty($rule) ? null : $rule->name}}" />
                        <hr class="hr hr-light margin-t-25" />
                        <div class="margin-t-25">
                            <label class="label">TYPE</label>
                            <div class="margin-t-10">
                                <div class="flex gutter-sm">
                                    <div class="col-3-12">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="type" value="product"
                                                    {{empty($rule) ? '' : 'disabled'}} />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>PRODUCT</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="type" value="order"
                                                    {{empty($rule) ? '' : 'disabled'}} />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>ORDER</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="type" value="pricing"
                                                    {{empty($rule) ? '' : 'disabled'}} />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>PRICING</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr hr-light margin-t-25">
                        <div class="margin-t-25">
                            <div class="flex gutter-sm">
                                <div class="col-4-12">
                                    <label class="label">STATUS</label>
                                    <select class="form-control" name="status">
                                        <option value="1" {{!empty($rule) && $rule->status == 1 ? 'selected' : ''}}>
                                            Active</option>
                                        <option value="0" {{!empty($rule) && $rule->status == 0 ? 'selected' : ''}}>
                                            Inactive</option>
                                    </select>
                                </div>
                                <div class="col-4-12">
                                    <label class="label">Postion</label>
                                    <input type="number" class="form-control" name="position"
                                        value="{{empty($rule) ? 1 : $rule->position}}" />
                                </div>
                                <div class="col-4-12">
                                    <label class="label">Last & Final Offer</label>
                                    <select class="form-control" name="should_stop">
                                        <option value="1"
                                            {{!empty($rule) && $rule->should_stop == 1 ? 'selected' : ''}}>
                                            Yes</option>
                                        <option value="0"
                                            {{!empty($rule) && $rule->should_stop == 0 ? 'selected' : ''}}>
                                            No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr class="hr hr-light margin-t-25" />
                        <div class="margin-t-25">
                            <div class="flex gutter">
                                <div class="col-6-12">
                                    <label class="label">START AT</label>
                                    <input id="datetime-start" class="form-control" type="text" name="start_at"
                                        value="{{empty($rule) ? (new DateTime())->format('d M, Y h:i A') : (new DateTime($rule->start_at))->format('d M, Y h:i A')}}" />
                                </div>
                                <div class="col-6-12">
                                    <label class="label">END AT</label>
                                    <input id="datetime-end" class="form-control" type="text" name="end_at"
                                        value="{{empty($rule) || empty($rule->end_at) ? null : (new DateTime($rule->end_at))->format('d M, Y h:i A')}}" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="conditions"></div>
                    <div class="margin-t-25 text-right">
                        <button type="button" class="btn btn-sm" id="btn-condition">
                            <i class="fa fa-plus"></i> <span class="bold">OR</span>
                        </button>
                    </div>

                    <div id="action-card" class="card margin-t-40">
                        <h2 class="bold">DO [Action]</h2>
                        <hr class="hr hr-light margin-t-25" />
                        <div class="margin-t-25">
                            <label class="label">OFFER</label>
                            <div class="margin-t-10">
                                <div class="flex gutter-sm">
                                    <div class="col-3-12 offer offer-product offer-order">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="percentage" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>% DISCOUNT</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12 offer offer-product offer-order">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="flat" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>FLAT DISCOUNT</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12 offer offer-product offer-order">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="free" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>Buy X get Y</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12 offer offer-product offer-order offer-pricing">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="new_price" min="0" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>NEW PRICE (INR)</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12 offer offer-pricing">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="margin" min="0" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>MARGIN (%)</span>
                                        </label>
                                    </div>
                                    <div class="col-3-12 offer offer-pricing">
                                        <label class="flex cross-center">
                                            <div class="radio margin-r-10">
                                                <input type="radio" name="offer" value="markup" min="0" />
                                                <span class="indicator"></span>
                                            </div>
                                            <span>MARKUP (%)</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr hr-light margin-t-25" />
                        <div class="margin-t-25" id="action"></div>
                        <div id="limits-wrapper">
                            <hr class="hr hr-light margin-t-25" />
                            <h2 class="bold margin-t-25">LIMITS</h2>
                            <div class="margin-t-25">
                                <div class="flex gutter-sm">
                                    <div class="col-12-12">
                                        <label class="label">MAX OFFER (price / item limit)</label>
                                        <input type="number" class="form-control" name="max_offer"
                                            value="{{empty($rule) ? null : $rule->max_offer}}" min="1" />
                                    </div>
                                    <div class="col-6-12">
                                        <label class="label">MAX USAGE PER USER</label>
                                        <input type="number" class="form-control" name="user_limit"
                                            value="{{empty($rule) ? null : $rule->user_limit}}" min="1" />
                                    </div>
                                    <div class="col-6-12">
                                        <label class="label">MAX TOTAL USAGE</label>
                                        <input type="number" class="form-control" name="total_limit"
                                            value="{{empty($rule) ? null : $rule->total_limit}}" min="1" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary margin-t-25">Save</button>
        </form>
    </div>
</div>
@include('admin.rule._condition')
@include('admin.rule._expression')
@include('admin.rule._selectors')
@include('admin.rule._actions')
@endsection

@section('js')
<script src="/plugins/zebra-datetimepicker/zebra_datepicker.min.js"></script>
<script>
    $('#datetime-start').Zebra_DatePicker({
        direction: true,
        format: 'd M, Y h:i A',
        pair: $('#datetime-end'),
        show_icon: false,
        show_clear_date: false,
        default_position: 'below'
    });

    $('#datetime-end').Zebra_DatePicker({
        direction: 1,
        format: 'd M, Y h:i A',
        show_icon: false,
        default_position: 'below'
    });
    var ruleData = {!! empty($rule) ? '{type: "product"}' : $rule->toJson() !!};
</script>
<script type="text/javascript" src="{{mix('admin/js/rule.js')}}"></script>
@append
