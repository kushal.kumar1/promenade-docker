<div class="templates hidden">
    <div class="action-free">
        <div class="flex gutter-sm">
            <div class="col-3-12">
                <label class="label">For Every {<span class="bold">Quantity</span>}</label>
                <input type="number" class="form-control" name="source_quantity" value="{# source_quantity #}" min="1" />
            </div>
            <div class="col-3-12">
                <label class="label">Get {<span class="bold">Quantity</span>} of Y</label>
                <input type="number" class="form-control" name="target_quantity" value="{# target_quantity #}" min="1" />
            </div>
            <div class="col-6-12">
                <label class="label">Choose Y [Y is different from X]</label>
                <input type="hidden" name="target_type" value="variant" />
                <select class="form-control selector-variant" name="targets[]"></select>
            </div>
        </div>
    </div>
    <div class="action-price">
        <div class="flex gutter-sm">
            <div class="col-4-12">
                <label class="label">OFFER VALUE</label>
                <input type="number" class="form-control" name="offer_value" value="{# value #}" step="0.01"/>
            </div>
        </div>
    </div>
</div>
