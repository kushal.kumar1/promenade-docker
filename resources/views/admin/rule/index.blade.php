@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Rules
        </div>
        <div>
            <a class="btn btn-primary" href="{{route('admin::rules.create')}}">Add Rule</a>
        </div>
    </div>

    @include('admin.grid')
</div>

@endsection