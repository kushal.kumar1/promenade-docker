<header class="header gutter-between">
    <div class="col-8-12">
        <div class="flex">
            <div class="sidebar-button-holder margin-r-10">
                <span class="sidebar-open pointer">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </span>
            </div>
            <div>
                <select class="form-control select-warehouse" name="warehouse_id">
                    @foreach($userWarehouses as $warehouse)
                        <option value="{{$warehouse->id}}" @if(session()->get('selected-warehouse') == $warehouse->id) selected @endif>{{$warehouse->full_name}}</option>
                    @endforeach
                </select>
            </div>
            @yield('header_heading')
        </div>
    </div>

    <div class="col-4-12 text-right">
        <a href="{{route('admin::profile')}}"><span>Hey, {{Auth::guard('admin')->user()->name}}</span></a>
        <span class="padding-10">/</span>
        <a href="{{route('admin::logout')}}">Logout <i class="fa fa-power-off" aria-hidden="true"></i></a>
    </div>
</header>
