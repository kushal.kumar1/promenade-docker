<thead>
<tr>
    @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
    <td>

    </td>
    @endif
    <td>#ID</td>
    <td valign="bottom" style="min-width:200px;" class="product_details @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated())) padding-0 @endif">
        @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
        <table class="col-12-12 padding-0">
            <tr>
                <td>Product</td>
            </tr>
            <tr>
                <td style="padding-top:0px;">
                    <select class="select2" id="addItemToInvoice">
                        <option value="">--Select item--</option>
                        @foreach($invoice->items as $item)
                            @php
                                $isRequested = $item->is_requested;
                                if($isRequested) {
                                    $product = $item->productRequest;
                                    $mrp = $product->mrp;
                                }
                                else {
                                    $product = $item->product;
                                    $mrp = $item->productVariant->mrp;
                                }

                                $itemDetails = $item->toArray();
                                $itemDetails['name'] = $product->name;
                                $itemDetails['sku'] = $isRequested ? 'req'.$product->id : $item->sku;
                                $itemDetails['barcode'] = $product->barcode;
                            @endphp
                            <option value="{{$item->id}}">{{$itemDetails['name'].' - '.$itemDetails['sku'].' '.$itemDetails['barcode']}}</option>
                        @endforeach
                        {{--<option value="add_new">Request new item</option>--}}
                    </select>
                </td>
            </tr>
        </table>
            @else
        Product Details
        @endif
    </td>
    <td valign="bottom">Barcode</td>
    <td valign="bottom">SKU</td>
    <td valign="top" class="padding-0">
        <table>
            <tr>
                <td colspan="3">Total values</td>
            </tr>
            <tr>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Taxable<br>
                    <span class="total_taxable"></span>
                </td>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Invoice<br>
                    <span class="total_invoice"></span>
                </td>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Effective<br>
                    <span class="total_effective"></span>
                </td>
            </tr>
        </table>
    </td>
    @foreach($itemFields as $group => $groupDetails)
    <td class="padding-0" style="vertical-align: top">
        <table>
            <tr>
                <td colspan="{{count($groupDetails['fields'])}}">{{$groupDetails['label']}}</td>
            </tr>
            <tr>
                @foreach($groupDetails['fields'] as $itemField)
                @php
                    $readonly = false;
                    if(in_array($itemField, [
                        'tax', 'taxes', 'total_invoiced_cost', 'total_effective_cost', 'total_taxable'
                    ])) {
                        $readonly = true;
                    }

                    if(in_array($itemField, ['sku', 'purchase_invoice_cart_id', 'is_requested', 'product_id', 'id', 'created_at', 'updated_at']))
                    {
                        continue;
                    }

                    $name = ucwords(str_replace(" ", '', $itemField));
                    $name = str_replace("_", " ", $name);
                @endphp
                <td style="vertical-align: top;width:130px; overflow: hidden;" title="{{$name}}">{{$name}}</td>
                @endforeach
            </tr>
        </table>
    </td>
    @endforeach

    <td valign="top" class="padding-0">
        <table>
            <tr>
                <td colspan="3">Per unit values</td>
            </tr>
            <tr>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Taxable<br>
                    <span class=""></span>
                </td>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Invoice<br>
                    <span class="unit_invoice"></span>
                </td>
                <td style="vertical-align: top;width:130px; overflow: hidden;">Effective<br>
                    <span class="unit_effective"></span>
                </td>
            </tr>
        </table>
    </td>
</tr>
</thead>