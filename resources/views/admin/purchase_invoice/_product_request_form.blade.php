<form id="productRequestForm">
    @php
        if(isset($cart) && $cart->purchaseOrder) {
            $poItems = $cart->purchaseOrder->items;
        }
    @endphp
    @if(isset($cart))
        <input type="hidden" name="source" value="{{isset($data->source) ? $data->source : 'purchase_invoice_cart'}}">
        <input type="hidden" name="source_id" value="{{isset($data->source) ? $data->source : $cart->id}}">
    @endif

    @if (isset($data) && !empty($data->id)) 
        <input type="hidden" name="id" value="{{ $data->id }}" />
    @endif

    <div class="col-12-12 margin-t-25" style="display: none;" id="productRequestSection">
        <h5 class="h5 bold">Create Product Request</h5>

        @if(!empty($poItems) && $poItems->where('demand_item_id', '!=', null)->count() > 0)
            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Product Group</label>
                <select class=" form-control margin-t-5" name="group_id" id="groupIdSelect">
                    <option value="">--New Group--</option>
                    @foreach($poItems as $item)
                        <option value="{{$item->demand->group_id}}">{{$item->demand->group_id}} {{$item->demand->group->name}}</option>
                    @endforeach
                </select>
            </div>
        @elseif(isset($invoice_items) && count($invoice_items) > 0)
            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Product Group</label>
                <select class="form-control select2 margin-t-5" name="group_id">
                    @foreach($invoice_items as $item)
                    <option value="{{$item['group_id']}}">{{$item['group_id']}} {{$item['group_name']}}</option>
                    @endforeach
                </select>
            </div>
        @else
            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Product Group</label>
                <select class="form-control margin-t-5" name="group_id" id="groupIdSelect">
                    @if (!empty($data) && isset($data->group))
                        <option value="{{ $data->group->id }}" selected="selected">{{$data->group->name}}</option>
                    @endif
                </select>
            </div>
        @endisset

        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">Product Name</label>
            <input class="form-control margin-t-5" required name="name" value="{{isset($data->name) ? $data->name : ""}}">
        </div>

        <div class="flex gutter-between">
            <div class="col-4-12">
                <div class="form-group margin-t-10">
                    <label class="flex padding-v-10">Auto Assign Barcode?</label>
                    <input type="checkbox" name="auto_assign_barcode" id="auto_assign_barcode" @if(!empty($data) && $data->auto_assign_barcode) checked @endif />
                </div>
            </div>

            <div class="col-8-12">
                <div class="form-group margin-t-10">
                    <label class="flex padding-v-10">Barcode</label>
                    <input class="form-control margin-t-5" required type="number" onwheel="this.blur()"
                        oninput="validity.valid||(value='');" name="barcode" value="{{isset($data->barcode) ? $data->barcode : ""}}"
                        @if (!empty($data) && $data->auto_assign_barcode) disabled @endif
                    >
                </div>
            </div>
        </div>

        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">Product Description</label>
            <textarea name="description" class="form-control margin-t-5" rows="2">{{isset($data->description) ? $data->description : ""}}</textarea>
        </div>


        <div class="col-12-12">
            <div class="flex gutter-between gutter-sm">
                <div class="col-3-12">
                    <div class="form-group margin-t-10">
                        <label class="flex padding-v-10">Unit MRP</label>
                        <input type="number" step="any" class="form-control margin-t-5" required name="mrp" value="{{isset($data->unit_mrp) ? $data->unit_mrp : ""}}">
                    </div>
                </div>
                
                @isset($measureUnits)
                    <div class="col-3-12">
                        <div class="form-group margin-t-10">
                            <label class="flex padding-v-10">UOM</label>
                            <select class="form-control margin-t-5 select2" required name="uom">
                                @foreach($measureUnits as $key=>$val)
                                    <option value="{{$key}}" @if (!empty($data) && $data->uom === $val) selected="selected" @endif>{{ucwords($val)}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                <div class="col-3-12">
                    <div class="form-group margin-t-10">
                        <label class="flex padding-v-10">Outer Qty.</label>
                        <input type="number" step="any" class="form-control margin-t-5"  name="outer_units" value="{{isset($data->outer_units) ? $data->outer_units : ""}}">
                    </div>
                </div>
                <div class="col-3-12">
                    <div class="form-group margin-t-10">
                        <label class="flex padding-v-10">Case Qty.</label>
                        <input type="number" step="any" class="form-control margin-t-5" name="case_units" value="{{isset($data->case_units) ? $data->case_units : ""}}">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">Tax Class</label>
            <select class="form-control margin-t-5" required name="tax_class_id" id="taxClassIdSelect">
                @foreach($taxClasses as $tax)
                    <option value="{{$tax->id}}" @if(isset($data->tax_class_id) && $data->tax_class_id === $tax->id) selected @endif>{{$tax->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">Brand</label>
            <select class="form-control margin-t-5" required name="brand_id" id="brandSelect">
                @if (!empty($data) && isset($data->brand))
                    <option value="{{ $data->brand->id }}" selected="selected">{{ $data->brand->name }}</option>
                @endif
            </select>
        </div>

        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">Collection</label>
            <select class="form-control margin-t-5" required name="collection_id" id="collectionSelect">
                @if (!empty($data) && isset($data->collection))
                    <option value="{{ $data->collection->id }}" selected="selected">{{ $data->collection->name }}</option>
                @endif
            </select>
        </div>
        <div class="form-group margin-t-10">
            <label class="flex padding-v-10">CL4_ID</label>
            <select class="form-control margin-t-5" required name="cl4_id" id="clIdSelect">
                @if (!empty($data) && isset($data->newCatL4))
                    <option value="{{ $data->newCatL4->id }}" selected="selected">{{ $data->newCatL4->name }}</option>
                @endif
            </select>
        </div>

        <div class="form-group margin-t-10">
            <div class="flex gutter">
                @if (isset($data->file)) 
                    <div class="col-2-12">
                        <input type="hidden" name="imagePATH" value="{{ $data->file }}" />
                        <img src="{{ asset('storage/media/product_request_images/'.$data->file) }}" height="100" />
                    </div>
                @endif

                <div class="col-10-12">
                    @if (!empty ($data) && !isset($data->file))
                        <label class="flex padding-v-10" for="file">Upload Reference Image</label>
                    @else
                        <label class="flex padding-v-10" for="file">Change Reference Image</label>
                    @endif
                    <input id="file" type="file" name="file" accept="image/*,application/pdf" class="form-control">
                </div>
            </div>
        </div>

        @if(!empty($isCart))
            <input type="hidden" name="invoice_id" @isset($invoice) value="{{$invoice->id}}" @endisset>
        @else
            <input type="hidden" name="purchase_invoice_cart_id" @isset($cart) value="{{$cart->id}}" @endisset>
        @endif

        <div class="col-12-12">
            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">HSN Code.</label>
                <input class="form-control margin-t-5" name="hsn_code" value="{{isset($data->hsn_code) ? $data->hsn_code : ""}}">
            </div>
        </div>

        @if (isset($data->id))
            <button class="center btn btn-success margin-t-10">Update</button>
        @else
            <button class="center btn btn-success margin-t-10">Submit</button>
        @endif
    </div>
</form>

<script>
$(function(){
    initSelect2();
});

function initSelect2()
{
    $(".select2").select2();
    @isset($poItems)
        $('#groupIdSelect').select2();
            @else
        $('#groupIdSelect').select2({
            minimumInputLength: 2,
            placeholder: 'Select group',
            ajax: {
                url: '{{route("admin::search.groups")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: group => {
                if (!group.id) {
                    return group.name;
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${group.id+' '+group.name}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: group => group.id+' '+group.text,
        });
    @endif
    $('#brandSelect').select2({
        minimumInputLength: 2,
        placeholder: 'Enter & Select Brand',
        ajax: {
            url: '{{route("admin::search.brands")}}',
            dataType: 'json',
            processResults: (data) => { return { results: data }; }
        },
        templateResult: brand => {
            if (!brand.id) {
                return brand.text;
            }

            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${brand.name}</h5>`;
            html += '</div>';

            return $(html);
        },
        templateSelection: brand => brand.text || brand.name,
    });

    $('#collectionSelect').select2({
        minimumInputLength: 2,
        placeholder: 'Enter & Select Category',
        ajax: {
            url: '{{route("admin::search.collections")}}',
            dataType: 'json',
            processResults: (data) => { return { results: data }; }
        },
        templateResult: collection => {
            if (!collection.id) {
                return collection.text;
            }

            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${collection.name}</h5>`;
            html += '</div>';

            return $(html);
        },
        templateSelection: collection => collection.text || collection.name,
    });

    $('#clIdSelect').select2({
        minimumInputLength: 2,
        placeholder: 'Enter & Select New Categry',
        ajax: {
            url: '{{route("admin::search.cl-id")}}',
            dataType: 'json',
            processResults: (data) => { return { results: data }; }
        },
        templateResult: newCatL4 => {
            if (!newCatL4.id) {
                return newCatL4.text;
            }

            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${newCatL4.name}</h5>`;
            html += '</div>';

            return $(html);
        },
        templateSelection: newCatL4 => newCatL4.text || newCatL4.name,
    });


    $(document).on('change', '#auto_assign_barcode', function () {
        let input = $('input[name=barcode]');

        if(this.checked) {
            input.attr('disabled', true);
            input.val('');
        } else {
            input.attr('disabled', false);
        }
    });
}
</script>