@if($invoice->all_grn->count())
    <table class="col-12-12 margin-t-25 parent">
            <thead>
            <tr>
                <td class="padding-5">
                    GRN ID
                </td>
                <td class="padding-5">
                    Reference ID
                </td>
                <td>Download</td>
                <td>Import Status</td>
                <td class="padding-5">
                    Created at
                </td>
            </tr>
            </thead>
            <tbody>
            @foreach($invoice->all_grn as $grn)
                <tr>
                    <td class="padding-5">
                        {{$grn->id}}
                    </td>
                    <td class="padding-5">
                        {{$grn->getPrettyGrnReferenceId()}}
                    </td>
                    <td class="padding-5">
                        <a href="{{route('admin::purchase-invoice.grn', [$grn->id])}}" class="link" target="_blank">Download GRN</a>
                    </td>
                    <td>
                        @if(!$grn->inventory_imported)
                        <button class="btn btn-primary import-grn"
                                data-href="{{route('admin::purchase-invoice.grn.import', $grn->id)}}">Import inventory</button>
                        @else
                        Import complete
                        @endif
                    </td>
                    <td class="padding-5">
                        {{\Carbon\Carbon::parse($grn->created_at)->format('jS F Y, h:m A')}}
                    </td>
                </tr>
            @endforeach
            </tbody>
    </table>
@endif