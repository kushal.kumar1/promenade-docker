<style>
    .padding-5 {
        padding:5px;
    }
    .select2-selection.select2-selection--single:focus {
        background: #FFF;
    }
    tbody td{
        border: 1px solid black;
    }
    thead th{
        border: 1px solid black;
    }
    .pb-10 {
        padding-bottom: 10px;
    }
</style>
<table style="margin-top:25px;width: 100%; text-align: left;">
    <thead>
        <tr class="bg-grey-light" style="border: 1px solid black">
            @if($invoice->canBeUpdated())
                <th></th>
            @endif
            <th class="padding-5"></th>
            <th class="padding-5"></th>
            <th class="padding-5" style="width: 150px;">Import Product</th>
            <th class="padding-5">MRP</th>
            <th class="padding-5" width="150px">Qty (units)</th>
            <th class="padding-5">Import Quantity</th>
            <th class="padding-5 text-right">Unit invoiced cost</th>
            <th class="padding-5 text-right">Unit post tax discount</th>
            <th class="padding-5 text-right">Taxes</th>
            <th class="padding-5 text-right">Margin %</th>
            <th class="padding-5 text-right">Total effective cost</th>
            <th class="padding-5 text-right">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($invoice_items as $item)
        <tr class="item" style="border-bottom: 1px solid #eee" {{--data-barcode="{{$item['barcode']}}"--}} id="item_{{$item['invoice_item_id']}}">
            @if($invoice->canBeUpdated())
            <td class="text-center">
                @if(!$item['is_complete'])
                <input type="checkbox" class="grn_item_select" value="{{$item['invoice_item_id']}}" name="item[id]"/>
                @endif
            </td>
            @endif
            <td class="padding-5">
                @if($item['product_id'])<b>PID</b>: {{$item['product_id']}}@endif
            </td>
            <td class="padding-5">
                <b>GID</b>: {{$item['group_id']}}
            </td>

            <td class="padding-5">
                    @if(!$item['is_complete'])
                        <select class="form-control select2 group_products"
                                disabled
                                data-group-id="{{$item['group_id']}}" style="max-width: 200px;">
                            @foreach($item['products'] as $product)
                                <option value="{{$product['product_id']}}"
                                        data-is-requested="{{$product['is_requested'] ? '1' : '0'}}">
                                    @if($product['is_requested']) Requested - @endif
                                    {{$product['product_id']}} - {{$product['name']}} - {{$product['barcode']}}
                                </option>
                            @endforeach
                        </select>
                    @else
                        @foreach($item['products'] as $pid=>$product)
                            @if($pid == $item['product_id'])
                                {{$product['name']}}
                            @endif
                        @endforeach
                    @endif
                </td>

            <td class="padding-5">
                &#8377;{{$item['productVariant']->mrp}}
            </td>

            <td class="padding-10" style="width:120px;">
                @foreach($item['quantity'] as $type=>$qty)
                    {{ucwords($type)}}: {{$qty}}<br>
                @endforeach
            </td>

            <td class="padding-5">
                @if(!$item['is_complete'])
                @foreach($item['products'] as $product)
                    @if($product['variants'])
                        <table class="grn_variant"
                                data-product-id="{{$product['product_id']}}"
                                data-group-id="{{$item['group_id']}}"
                               @if($loop->index > 0) style="display: none;" @endif>
                            @foreach($product['variants'] as $variant)
                                <tr>
                                    <td style="border: none; padding-bottom:10px;">{{$variant['sku']}}</td>
                                    <td style="border: none;padding-bottom:10px;">
                                        <input
                                                onwheel="this.blur()"
                                                oninput="validity.valid||(value='');"
                                                disabled
                                                class="grn_qty"
                                                style="width:100px"
                                                type="number"
                                                min="{{$variant['min']}}"
                                                max="{{$variant['max']}}"
                                                data-sku="{{$variant['sku']}}"
                                        />
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                @endforeach
                @else
                    Import completed
                @endif
            </td>
            <td class="padding-5 text-right">
                <span class='current-unit-cost'>
                    {{$item['unit_invoiced_cost']}}
                </span>
                @if($invoice->canBeUpdated())
                    <div class="edit-window" style="display: none;">
                        <div class="flex gutter-between">
                            <div class="inline-flex">
                                Final Rate
                            </div>
                            <div class="inline-flex">
                                <input type="number" step="any" class='edited-cost'
                                       value="{{$item['unit_invoiced_cost']}}">
                            </div>
                        </div>
                        <div class="flex gutter-between">
                            <div class="inline-flex">
                                Quantity ({{$item['ordered_sku']}})
                            </div>
                            <div class="inline-flex">
                                <input min="1" type="number" step="any" class='edited-quantity'
                                       value="{{$item['variant_quantity']}}">
                            </div>
                        </div>
                        <div class="margin-t-10">
                        <button data-invoice='{{$invoice->id}}' data-id='{{ $item['invoice_item_id'] }}' class="btn btn-sm btn-warning update-item-price">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <button class="btn btn-sm btn-warning exit-edit-window">
                                Cancel
                            </button>
                        </div>
                    </div>
                <div class="inline-block">
                    <button class="btn btn-sm btn-danger edit-cost-btn">
                        <i class="fa fa-pencil"></i> Edit
                    </button>
                    <button class="btn btn-sm btn-danger remove-item-btn"  data-id="{{$item['invoice_item_id']}}">
                        <i class="fa fa-trash"></i>
                    </button>
                </div>
                @endif
            </td>
            <td class="padding-5 text-right">{{$item['post_tax_discount']}}</td>
            <td class="padding-5 text-right">GST{{$item['taxes']}}</td>
            <td class="padding-5 text-right">{{$item['margin']}}</td>
            <td class="padding-5 text-right">{{$item['effective_cost']}}</td>
            <td>
                <button type="button" class="btn btn-primary" data-modal="#modal-barcode-{{$item['product_id']}}">Print Labels</button>
                <div id="modal-barcode-{{$item['product_id']}}" class="modal">
                    <div class="modal-content">
                        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                        <h5 class="h6 title text-center">Generate Labels</h5>
                        <form class="label-form" method="post" action="{{route('admin::purchase-invoices.generate-labels',['id'=>$item['invoice_item_id'],'productId'=>$item['product_id']])}}" autocomplete="off">
                            {{csrf_field()}}
                            <div class="margin-t-20">
                                <label class="label">Number of Labels</label>
                                <input class="form-control qty js-barcode-qty" name="qty" id="qty" type="number" step="1" min="1" max="{{$item['quantity']['received']}}">
                                <span class="small margin-t-5">Please note entered value cannot exceed received quantity. In case of odd number one extra label will be generated.</span>
                            </div>
                            <div class="margin-t-20 text-right">
                                <input type="hidden" name="received_quantity" value="{{$item['quantity']['received']}}">
                                <button class="btn btn-sm btn-primary barcode-print" >Generate</button>
                            </div>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>

</table>

<button class="btn btn-primary" style="display: none;" id="add-new-item-btn">Add new item</button>