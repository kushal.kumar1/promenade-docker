<div class="card margin-t-25">
    <div class="flex gutter-between">
        <h3 class="h3">Short items</h3>
    </div>
    <table cellpadding="10" style="margin-top:25px;width: 100%; text-align: left;">
        <thead>
        <tr>
            <th>Product Name</th>
            <th>HSN</th>
            <th>Quantity</th>
            <th>MRP</th>
            <th>Tax Class</th>
            <th>Discount</th>
            <th>Total Invoice Cost</th>
            <th>Post Tax Discount</th>
            <th>Effective Cost</th>
            @if($invoice->canBeUpdated())
            <th>Action</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($invoice->shortItems as $item)
            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->hsn}}</td>
                <td>{{$item->quantity}}</td>
                <td>{{$item->mrp}}</td>
                <td>{{$item->taxClass->name}}</td>
                <td>{{$item->discount}}</td>
                <td>{{$item->invoice_cost}}</td>
                <td>{{$item->post_tax_discount}}</td>
                <td>{{$item->effective_cost}}</td>
                @if($invoice->canBeUpdated())
                <td>
                    <button type="button" class="btn btn-sm btn-primary" onclick="$('#editShortProduct_{{$item->id}}').toggle();">Edit</button>
                </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@if($invoice->canBeUpdated())
@foreach($invoice->shortItems as $shortItem)
    <div class="card margin-t-25" style="display: none" id="editShortProduct_{{$shortItem->id}}">
@include('admin.purchase_invoice._short_item_form')
    </div>
@endforeach
@endif