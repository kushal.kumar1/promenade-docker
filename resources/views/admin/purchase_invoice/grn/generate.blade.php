@extends('admin.master')

@section('css')
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input.disabled
        {
            pointer-events: none;
            background: #e4e4e4;
        }

        /* Firefox */
        input[type=number], input[type=text], input[type=date] {
            -moz-appearance: textfield;
            padding: 4px;
            font-size: 12px;
        }

        .center
        {
            margin-right: auto;
            margin-left: auto;
            display: inherit;
        }

        .col-1-12
        {
            padding: 15px 0px;
        }

        .col-1-12:last-child input
        {
            border-right: 1px solid #c7c7c7;
            border-radius:0px;
        }

        .col-1-12 input
        {
            border-top: 1px solid #c7c7c7;
            border-left: 1px solid #c7c7c7;
            border-bottom: 1px solid #c7c7c7;
            border-radius:0px;
        }

        .card {
            overflow-x: scroll;
        }

        table
        {
            width: max-content;
        }

        table thead
        {
            background: #dedede;
        }

        table thead td
        {
            padding: 8px 6px;
            font-weight: bold;
            font-size: 13px;
        }

        table td
        {
            min-width: 60px;
            border-right: 1px solid #fff;
        }

        table.parent>tbody>tr:first-child
        {
            border-top: 1px solid #ddd;
        }

        table.parent>tbody>tr
        {
            background: #FAFAFA;
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
            font-size:13px;
        }

        table.parent>tbody>tr td
        {
            border-color: #DDD;
        }

        table tbody td.padding-5, span.padding-5
        {
            padding:5px;
        }

        td.product_details
        {
            width: max-content;
        }

        table td table td
        {
            width: 90px;
        }

        table td table td[colspan]
        {
            background: #aaa;
            border:0px;
            text-align: center;
        }

        table td table td:last-child
        {
            border:0px;
        }

        .variant {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #b4b4b4;
            transition: background 0.8s;
        }

        .variant-disabled {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #f3c7c7;
            color: #b4b4b4;
        }

        .variant:hover {
            border: 1px solid #d4d4d4;
            background: #fafafa radial-gradient(circle, transparent 1%, #fafafa 1%) center/15000%;
        }

        .variant:active {
            background-size: 100%;
            transition: background 0s;
            background-color: #e5e5e5;
        }

        .cart-overlay {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.6);
        }

        .cart-overlay .icon-loading {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        .item-error {
            color: #f55753;
            font-size: 14px;
            font-weight: bold;
            border: 1px dashed #fb9492;
            padding: 5px 10px;
        }

        #floating-product-selector
        {
            position: fixed;
            background: #FFF;
            z-index: 3;
            bottom: 0px;
            padding: 20px;
            max-width: 500px;
            left: 30p;
            border: 1px solid #ddd;
            box-shadow: 1px 1px 13px #00000054;
            margin-left: auto;
            margin-right: auto;
            left: 0px;
            right: 0px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .invoice_table
        {
            width: 100%;
        }

        .invoice_table tr
        {
            background: #fafafa;
            border-bottom: 1px solid #FFF;
        }
        .invoice_table tr td:first-child
        {
            font-weight: bold;
            background: #efefef;
        }
        .invoice_table tr td
        {
            padding:10px;
        }

        .has-error
        {
            border:4px solid red !important
        }

        .modal > .modal-content {
            overflow-x: hidden;
        }

    </style>
@append

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="regular light-grey">
                <a href="{{route('admin::purchase-invoices.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to Purchase Invoices</a>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 0px;">
        <div class="card">
            <div class="flex gutter-between">
                <h2 class="h2">Purchase Invoice</h2>
                @if(!$invoice->canBeUpdated())
                    <a href="{{route('admin::purchase-invoices.returnItems', $invoice->id)}}" class="btn btn-primary">Return
                        items</a>
                @else
                    <a href="{{route('admin::modification-requests.index',[
                            "modificationRequestType"=>"purchase_invoice",
                            "purchaseInvoiceId"=>$invoice->id,
                            "deeplink"=>true
                        ])}}" class="btn btn-primary">Create Modification Request</a>
                @endif
            </div>

            @include('admin.purchase_invoice.detail')
        </div>

        <div class="card margin-t-25">
            <div class="flex gutter-between">
                @if($invoice->canBeUpdated())
                <h3 class="h3">Select items to generate GRN</h3>
                <button id="request-product" class="flex-end btn btn-primary">Request new product</button>
                @else
                    <h3 class="h3">Invoice items</h3>
                @endif
            </div>
            @include('admin.purchase_invoice.grn.invoice-items')
            @if($invoice->canBeUpdated())
            <div class="col-12-12 margin-t-10">
                <label class="col-12-12 pull-left padding-v-10">Delivery Date</label>
                <br>
                <div class="form-group margin-r-10" style="float:left; width:200px;">
                    <input id="grnDeliveryDate" disabled class=" form-control" min="{{\Carbon\Carbon::parse($invoice->invoice_date)->format('Y-m-d')}}" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}" type="date" >
                </div>
                <div class="form-group" style="float:left; width:200px;">
                    <button id="generateGrnButton" disabled class="btn btn-success generate-grn" type="button">Generate GRN</button>
                </div>
            </div>
            @endif
        </div>

        @include('admin.purchase_invoice.grn.short-items')

        <div class="card margin-t-25">
            <h3 class="h4">Received GRN</h3>
            @include('admin.purchase_invoice.grn.grn-list')
        </div>

        <div class="card margin-t-25">
            <h3 class="h4">Debit note</h3>
            @include('admin.purchase_invoice.debit_note.debit_note_list')
        </div>

    </div>


    <div id="modal-product-request-form" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal">X</span>
            @include('admin.purchase_invoice._product_request_form')
        </div>
    </div>


@endsection

@section('js')
<script>
    class GenerateGRN {

        grnDeliveryDate = $("#grnDeliveryDate");
        generateGrnButton = $("#generateGrnButton");
        grnItemsSelect = $(".grn_item_select");
        barcodeSearch = $("#barcode-search");
        addNewItemBtn = $("#add-new-item-btn");
        productRequestSection = $("#productRequestSection");
        groupProductSelect = $(".group_products");
        grnItemCheck = $(".grn_item_select");

        constructor() {
            this.grnItemsSelect.on('change', () => this.onGrnItemSelectChange())
            this.generateGrnButton.on('click', () => this.generateGrnClick())
            $(".import-grn").on('click', (e) => this.importGrnClick($(e.currentTarget)))
            this.barcodeSearch.on('change', () => this.handleBarcodeChange())
            this.groupProductSelect.on('change', () => this.handleGroupProductChange())
            this.grnItemCheck.on('change', (e) => this.handleGrnItemCheck($(e.currentTarget)))
            $("#request-product").on('click', (e) => this.requestProductButtonClick($(e.currentTarget)))
            $('#productRequestForm').on('submit', (e) => this.submitProductRequest(e, $(e.currentTarget)));
            $('#completeInvoiceFormBtn').on('click', (e) => this.submitCompleteInvoiceForm());

            $(".edit-cost-btn").on('click', (e) => this.showEditPriceForm($(e.currentTarget)))
            $(".exit-edit-window").on('click', (e) => this.hideEditPriceForm($(e.currentTarget)))
            $(".update-item-price").on('click', (e) => this.updateItemEffectiveCost($(e.currentTarget)))
            $(".remove-item-btn").on('click', (e) => this.onRemoveItemButtonClick($(e.currentTarget)))
            $(".delete-purchase-invoice").on('click', (e) => this.onDeleteInvoiceBtnClick($(e.currentTarget)))

            $("#upload-scanned-pi-button").on('click', () => {
                $("#scanned-pi-field").click();
            })
            $("#scanned-pi-field").on('change', this.handleUploadFieldChange);

        }

        handleUploadFieldChange() {
            $("#upload-scanned-pi-button").attr('disabled', true);
            // Get form
            const form = $('#scanned-pi-form')[0];
            // Create an FormData object
            const data = new FormData(form);
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                url: $("#scanned-pi-form").attr('action'),
                data: data,
                contentType: false,
                processData: false,
                success: function (data) {
                    const isReload = confirm('file uploaded successfully! Refresh now');
                    $("#upload-scanned-pi-button").attr('disabled', false);
                    if(isReload) {
                        location.reload();
                    }
                },
                error: function (e) {
                    this.onError(e);
                    $("#upload-scanned-pi-button").attr('disabled', false);
                }
            });
        }

        onDeleteInvoiceBtnClick($target) {
            var self = this;
            dialog(
                'Delete Invoice',
                'Are you sure you want to delete the invoice?',
                function() {
                    $target.prop('disabled', true);
                    $.ajax('{{route("admin::purchase-invoices.delete", $invoice->id)}}', {
                        method: 'POST',
                    }).done((res) => {
                        let cartLink = '{{route("admin::purchase-invoices.cart", ["id" => "#"])}}';
                        window.location = cartLink.replace("#", res.id);
                    }).fail((error) => {
                        self.onError(error);
                        $target.prop('disabled', false);
                    });
                },
                function() {}
            );
        }


        onRemoveItemButtonClick($target) {
            var self = this;
            dialog(
                'Remove import',
                'Are you sure you want to set receiving qty to 0?',
                function() {
                    const itemId = $target.attr('data-id');
                    self.resetItemReceivingQty(itemId, $target);
                    return false;
                },
                function() {}
            );
        }

        submitCompleteInvoiceForm() {
            dialog(
                'Complete Invoice',
                'Are you sure you want to complete the invoice?',
                function() {
                    $('#completeInvoiceForm').submit();
                },
                function() {}
            );
        }

        showEditPriceForm($target) {
            $target.parent().parent().find('.edit-window').show();
            $target.parent().find('.current-unit-cost').hide();
            $target.hide();
        }

        hideEditPriceForm($target) {
            $target.parent().parent().parent().find('.edit-window').hide();
            $target.parent().parent().parent().find('.current-unit-cost').show();
            $target.parent().parent().parent().find('.edit-cost-btn').show();
        }

        resetItemReceivingQty(itemId, $target) {
            var self = this;
            $target.prop('disabled', true);
            $.ajax('{{route("admin::purchase-invoices.resetItemImport")}}', {
                method: 'POST',
                data: {
                    item_id: itemId,
                }
            }).done((res) => {
                location.reload();
            }).fail((error) => {
                self.onError(error);
                $target.prop('disabled', false);
            });
        }

        updateItemEffectiveCost($target) {
            $target.prop('disabled', true);
            const itemId = $target.data('id');
            const newCost = $target.parent().parent().find('input.edited-cost').val();
            const newQty = $target.parent().parent().find('input.edited-quantity').val();
            if(Number(newCost) == 0 || Number(newQty) == 0) {
                const toast = new Toast('Cost and quantity cannot be 0');
                toast.show();
                return false;
            }

            $.ajax('{{route("admin::purchase-invoices.editItemPrice", [$invoice->id])}}', {
                method: 'POST',
                data: {
                    new_cost: newCost,
                    new_qty: newQty,
                    item_id: itemId
                }
            }).done((res) => {
                location.reload();
            }).fail((error) => {
                self.onError(error);
                $target.prop('disabled', false);
            });

        }

        handleGrnItemCheck(thisCheckbox) {
            const thisVal = thisCheckbox.val();
             if(thisCheckbox.prop('checked') === true) {
                 $('#item_'+thisVal).find('select.group_products').prop('disabled', false);
                 $('#item_'+thisVal).find('input.grn_qty').prop('disabled', false);
             }
             else {
                 $('#item_'+thisVal).find('select.group_products').prop('disabled', true);
                 $('#item_'+thisVal).find('input.grn_qty').prop('disabled', true);
             }
        }

        handleGroupProductChange() {
            const groupId = this.groupProductSelect.data('group-id');
            const productId = this.groupProductSelect.val();
            const isRequested = this.groupProductSelect.find('option:selected').data('is-requested');
            $("table.grn_variant[data-group-id='"+groupId+"']").hide();
            const grnVariantTable = $("table.grn_variant[data-group-id='"+groupId+"'][data-product-id='"+productId+"']");
            grnVariantTable.show();
        }

        requestProductButtonClick() {
            this.productRequestSection.show();
            Modal.show($("#modal-product-request-form"));
        }

        handleBarcodeChange() {
            const barcode = this.barcodeSearch.val();
            this.addNewItemBtn.hide();
            if(barcode != '') {
                $("tr.item").hide();
                const searchedItemRow = $("tr.item[data-barcode='" + barcode + "']");
                searchedItemRow.show();
            }
            else {
                $("tr.item").show();
            }
        }

        onGrnItemSelectChange() {
            $(".grn_item_select:checked").length > 0 ?
                this.manageGrnSubmitSectionState(true) :
                this.manageGrnSubmitSectionState(false);
        }

        manageGrnSubmitSectionState(isActive) {
            this.grnDeliveryDate.prop('disabled', !isActive);
            this.generateGrnButton.prop('disabled', !isActive);
        }

        generateGrnClick()
        {
            let selectedItems = new Array();
            const deliveryDate = this.grnDeliveryDate.val();
            if(!deliveryDate)
            {
                const toast = new Toast('Please add GRN delivery date');
                toast.show();
                return false;
            }
            this.grnItemsSelect.each(function(){
                const itemId = $(this).val();
                const row = $("tr#item_"+itemId);
                const isChecked = $(this).prop('checked') === true;
                if(isChecked)
                {
                    const groupProductSelect = row.find('select.group_products');
                    const productId = groupProductSelect.val();
                    const isRequested = groupProductSelect.find('option:selected').data('is-requested');
                    let items = [];
                    const parent = $(this).closest('tr.item');
                    const grnQtyFields = parent.find('input.grn_qty');
                    grnQtyFields.each(function(){
                        const sku = $(this).attr('data-sku');
                        const qty = $(this).val();
                        if(qty > 0) {
                            items.push({sku: sku, quantity: qty});
                        }
                    });
                    selectedItems.push({
                        'item_id': itemId,
                        'product_id': productId,
                        'is_requested': isRequested,
                        'quantity': items});
                }
            });

            let self = this;
            dialog(
                'Create GRN',
                'Are you sure you want to create a GRN?',
                function() {
                    self.generateGrnForSelectedItems(deliveryDate, selectedItems)
                },
                function() {}
            );
        }

        importGrnClick($target) {
            dialog(
                'Import GRN',
                'Are you sure you want to import all items in the GRN?',
                function() {
                    const link = $target.attr('data-href');
                    location.href = link;
                },
                function() {}
            );
        }

        generateGrnForSelectedItems(deliveryDate, selectedItems)
        {
            let self = this;
            if(selectedItems.length)
            {
                $(".generate-grn").prop('disabled', true);
                $("#grnDeliveryDate").prop('disabled', true);
                $.ajax('{{route("admin::purchase-invoice.generate_grn", [$invoice->id])}}', {
                    method: 'POST',
                    data: {
                        delivery_date: deliveryDate,
                        items: selectedItems
                    }
                }).done((res) => {
                    if(res.success)
                    {
                        location.reload();
                    }
                    else
                    {
                        self.manageGrnSubmitSectionState(true);
                    }
                }).fail((error) => {
                    self.onError(error);
                    self.manageGrnSubmitSectionState(true);
                });
            }
        }

        onError(error){
            let message = error.responseJSON.message;
            if (message == undefined) {
                message = "An unexpected error has occurred";
            }
            const toast = new Toast(message);
            toast.show();
        }

        submitProductRequest(e, $form)
        {
            e.preventDefault();
            this.searchTimer = setTimeout(() => {
                $.ajax('{{route("admin::product_request.create")}}?'+$form.serialize(), {
                    method: 'POST',
                }).done((response) => {
                    $("#productRequestSection").hide();
                    $("#productRequestForm").find('input').val('');
                    const toast = new Toast("Product request created successfully. You will be able to add the product once it is approved");
                    toast.show();
                    location.reload();
                });
                this.searchRequest.fail((error) => this.onError(error));
            }, 100);
        }
    }

    new GenerateGRN();
</script>
@endsection