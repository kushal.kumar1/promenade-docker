{{--@if(isset($invoice) && !$invoice->canBeUpdated())--}}
    <table class="margin-t-25 invoice_table">
        <tbody>
        @if($invoice->purchase_order_id)
        <tr>
            <td>PO</td>
            <td><a href="{{route('admin::purchase-order.single', [$invoice->purchase_order_id])}}" class="link">{{$po->vendor->name}}</a></td>
        </tr>
            @else
        <tr>
            <td>Vendor</td>
            <td><b>#{{$invoice->vendor->id}}</b> {{$invoice->vendor->name}}</td>
        </tr>
        @endif
        <tr>
            <td>Vendor Address</td>
            <td>{{ $invoice->vendorAddress->address }} <br>GSTIN - <b>{{ $invoice->vendorAddress->gstin }}</b></td>
        </tr>
        <tr>
            <td>Warehouse</td>
            <td>{!! $invoice->warehouse->legal_name."<br>".$invoice->warehouse->address !!}
                <br>GSTIN - <b>{{ $invoice->warehouse->gstin }}</b>
            </td>
        </tr>
        @if($invoice->is_direct_store_purchase && $invoice->storePurchaseInvoice)
        <tr>
            <td>Store</td>
            <td>{{ $invoice->storePurchaseInvoice->store->name }} ({{$invoice->storePurchaseInvoice->store->id}})</td>
        </tr>
        @endif
        <tr>
            <td>NT Ref ID</td>
            <td>{{ $invoice->reference_id }}</td>
        </tr>
        <tr>
            <td>Vendor Ref ID</td>
            <td>{{ $invoice->vendor_ref_id }}</td>
        </tr>
        <tr>
            <td>Invoice Date</td>
            <td>{{ \Carbon\Carbon::parse($invoice->invoice_date)->format('d-M-Y') }}</td>
        </tr>
        <tr>
            <td>EWAY bill number</td>
            <td>{{$invoice->eway_number}}</td>
        </tr>
        <tr>
            <td>Platform</td>
            <td>@if($invoice->platform) {{$invoice->platform->platform}} @endif</td>
        </tr>
        <tr>
            <td>Taxable</td>
            <td>{{$invoice->getPrettyTaxableAttribute()}}</td>
        </tr>
        <tr>
            <td>Post tax discount</td>
            <td>{{$invoice->getPrettyPostTaxDiscountAttribute()}}</td>
        </tr>
        <tr>
            <td>Tax</td>
            <td>
                {!! implode(',  ', $taxBreakups) !!},
                <b>Total </b>:
                {{number_format($invoice->tax, 2)}}
                &nbsp;&nbsp;
                <a href="{{route('admin::purchase-invoices.calculate', $invoice->id)}}" class="btn btn-primary btn-sm">Re-Calculate</a>
            </td>
        </tr>
        <tr>
            <td>TCS</td>
            <td>{{number_format($invoice->tcs, 2)}}</td>
        </tr>
        <tr>
            <td>Total</td>
            <td>{{number_format($invoice->total, 2)}}</td>
        </tr>
        <tr>
            <td>Total amount mentioned on invoice</td>
            <td>{{$invoice->total_invoice_value}}</td>
        </tr>
        <tr>
            <td>Scanned Copy</td>
            <td>
                <a href="{{route('admin::purchase-invoices.get-copy', $invoice->id)}}"
                   target="_blank" class="btn btn-link"
                >
                    <i class="fa fa-download"></i>
                    Download</a>

                <form id="scanned-pi-form" class="hidden form-multipart"
                      method="post"
                      action="{{route('admin::purchase-invoices.upload-copy', $invoice->id)}}">
                    @csrf
                    <input type="file" name="file" accept="image/*,application/pdf" class="form-control" id="scanned-pi-field">
                </form>

                <button type="button" class="btn btn-primary" id="upload-scanned-pi-button">
                    <i class="fa fa-upload"></i>
                    Add new scanned copy</button>
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <h5>{{ucwords($invoice->status)}}</h5>
                @can('revert-invoice')
                    @if($invoice)
                        <div class="flex margin-t-10">
                            <button class="btn btn-danger delete-purchase-invoice">Revert Invoice</button>
                        </div>
                    @endif
                @endcan
                @if($invoice->canBeUpdated())
                    <div class="flex gutter mt-10">
                        <form class="form" method="post" id='completeInvoiceForm'
                              action="{{route("admin::purchase-invoices.complete", [$invoice->id])}}"
                              data-destination="{{route("admin::purchase-invoices.editInvoice", [$invoice->id])}}"
                        >
                            @csrf
                            <div class="flex margin-t-10">
                                    <label>Debit Note Date <small>(Today if left blank)</small></label>
                                    <input class="form-control margint-t-10" name="debit_note_date" type="date" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}" min="{{ \Carbon\Carbon::parse($invoice->invoice_date)->format('Y-m-d') }}" placeholder="Select date">
                                <br>
                            </div>
                            <div class="flex margin-t-10">
                                <button type="button" id="completeInvoiceFormBtn" class="margin-t-10 btn btn-primary">
                                    <i class="fa fa-check"></i>
                                    Mark as complete
                                </button>
                            </div>
                            
                        </form>
                    </div>
                @endif
{{--                @if($is_debit_note_allowed)--}}
{{--                    <div class="flex gutter">--}}
{{--                        <form class="" method="post" id='completeInvoiceForm'--}}
{{--                              action="{{route("admin::purchase-invoices.create-debit-note", [$invoice->id])}}"--}}
{{--                              data-destination="{{route("admin::purchase-invoices.editInvoice", [$invoice->id])}}"--}}
{{--                        >--}}
{{--                            @csrf--}}
{{--                            <button type="submit" class="margin-t-10 btn btn-primary">--}}
{{--                                Generate debit note--}}
{{--                            </button>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                @endif--}}
            </td>
        </tr>
        <tr>
            <td>Expected payment date</td>
            <td>
                @if($invoice->forecast_payment_date)
                    {{\Carbon\Carbon::parse($invoice->forecast_payment_date)->format('d-M-Y')}}
                    <br>
                    <br>
                @endif
                <form class="form" method="post" id='completeInvoiceForm'
                      action="{{route("admin::purchase-invoices.update_forecast_payment_date", [$invoice->id])}}"
                      data-destination="{{route("admin::purchase-invoices.editInvoice", [$invoice->id])}}"
                >
                    @csrf
                    <div class="flex">
                        <input type='hidden' name='id' value='{{$invoice->id}}'>
                        <input class="col-4-12 form-control" name="forecast_payment_date" type="date" min="{{\Carbon\Carbon::today()->format('Y-m-d')}}" placeholder="Select date">
                    </div>
                    <div class="flex">
                        <button class="btn btn-primary margin-t-5">Save</button>
                    </div>
                </form>
            </td>
        </tr>

        <tr>
            <td>Payment date</td>
            <td>
                @if($invoice->payment_date)
                    {{\Carbon\Carbon::parse($invoice->payment_date)->format('d-M-Y')}}
                    <br>
                    <br>
                @endif
                <form class="form" method="post" id='completeInvoiceFormPaymentDate'
                      action="{{route("admin::purchase-invoices.update_payment_date", [$invoice->id])}}" >
                    @csrf
                    <div class="flex">
                        <input type='hidden' name='id' value='{{$invoice->id}}'>
                        <input class="col-4-12 form-control" name="payment_date" type="date" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}" placeholder="Select date">
                    </div>
                    <div class="flex">
                        <button class="btn btn-primary margin-t-5">Save</button>
                    </div>
                </form>
            </td>
        </tr>
        @if($invoice->status == 'completed')
        <tr>
            <td>Discount Platform</td>
            <td>
                <form class="form" method="post" id='completeInvoiceForm'
                      action="{{route("admin::purchase-invoices.update_discount_platform", [$invoice->id])}}"
                      data-destination="{{route("admin::purchase-invoices.editInvoice", [$invoice->id])}}"
                >
                    @csrf
                    <div class="flex">
                        <select name="discount_platform_id" class="col-4-12 form-control">
                            <option value="">--Select discount platform--</option>
                            @foreach($discount_platforms as $discountPlatform)
                            <option @if(!empty($invoice->discountPlatform && $invoice->discountPlatform->id == $discountPlatform->id)) selected @endif value="{{$discountPlatform->id}}">{{$discountPlatform->platform}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="flex">
                        <button class="btn btn-primary margin-t-5">Save</button>
                    </div>
                </form>
            </td>
        </tr>
        @endif
        </tbody>
    </table>
{{--
@endif--}}
