@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Purchase Invoices
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-vendors">Create Purchase Invoice</button>
            </div>
        </div>
        @include('admin.grid')
    </div>

    @include('admin.purchase_invoice.cart._select_vendor')

@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/purchase-invoice.js')}}"></script>
@endsection