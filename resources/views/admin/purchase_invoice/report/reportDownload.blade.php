@extends('admin.master')

@section('css')
<link rel="stylesheet" href="/plugins/zebra-datetimepicker/css/bootstrap/zebra_datepicker.min.css">
<style>
    #datetime-start:read-only,
    #datetime-end:read-only {
        background-color: #fff;
    }
</style>
@append

@section('content')
<div class="container">
    <div class="h3 margin-t-10">Report Download</div>

    <div class="margin-t-40">
        <form class="" method="POST"
        action="{{route('admin::report.dailyPurchaseReportDownload')}}">
            @csrf()
            <div class="flex gutter">
                <div class="col-8-12">
                    <div class="card" id="card-basic">
                        <div class="margin-t-25">
                            <div class="flex gutter">
                                <div class="col-6-12">
                                    <label class="label">START AT</label>
                                    <input id="datetime-start" class="form-control" type="date" name="start_at"
                                        value="{{\Carbon\Carbon::yesterday()->format('Y-m-d')}}" />
                                </div>
                                <div class="col-6-12">
                                    <label class="label">END AT</label>
                                    <input id="datetime-end" class="form-control" type="date" name="end_at"
                                        value="{{\Carbon\Carbon::today()->format('Y-m-d')}}" />
                                    <p>Records of end date are not included in the report</p>
                                </div>
                                <div class="col-6-12">
                                    <label class="label">Report Based</label>
                                    <select class="form-control" name="report_as">
                                        <option value="created_at" >created at</option>
                                        <option value="invoice_at" >invoiced at</option>
                                    </select>
                                    <span class="error error-status"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary margin-t-25" >Download</button>
        </form>
    </div>
</div>
@endsection
