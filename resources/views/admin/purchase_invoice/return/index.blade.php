@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="regular light-grey">
                <a href="{{route('admin::purchase-invoices.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to Purchase Invoice</a>
            </div>
        </div>
    </div>

    @include('admin.grid')

@endsection