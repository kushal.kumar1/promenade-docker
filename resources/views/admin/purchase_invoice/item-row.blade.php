<tr class="item"
    data-product-id="{{$itemDetails['product_id']}}"
    data-sku="{{$itemDetails['sku']}}"
    data-barcode="{{$itemDetails['barcode']}}"
>

    @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
        <td style="padding:6px;">
            <button type="button" class="row-toggle btn btn-sm btn-primary">Edit</button>
            <button type="button" class="row-toggle-cancel btn btn-sm btn-warning">Cancel</button>
            <input type="hidden" class="is-active-row">
            <input type="hidden" name="items[{{$itemDetails['sku']}}][product_id]" value="{{$itemDetails['product_id']}}">
            <input type="hidden" name="items[{{$itemDetails['sku']}}][sku]" value="{{$itemDetails['sku']}}">
            <input type="hidden" name="items[{{$itemDetails['sku']}}][is_requested]" value="{{$itemDetails['is_requested']}}">
            @isset($itemDetails['variant_qty'])
                <input type="hidden" class="{{$itemDetails['sku']}} variant_qty" name="items[{{$itemDetails['sku']}}][variant_qty]" value="{{$itemDetails['variant_qty']}}">
            @endisset

            @if(!empty($mrp))
                <input type="hidden" class="{{$itemDetails['sku']}} mrp" value="{{$mrp}}">
            @endif

        </td>
    @endif
        <td class="padding-5 product_details">
            #{{$itemDetails['product_id']}}
        </td>
    <td class="padding-5 product_details">
        {{$itemDetails['name']}}
    </td>
    <td class="padding-5">
        {{$itemDetails['barcode']}}
    </td>
    <td class="padding-5">
        @if(empty($itemDetails['is_requested']))
        {{$itemDetails['sku']}}
        @endif
    </td>

    <td class="padding-0">
        <table>
            <tr>
                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} taxable_cost">
                        @if(isset($itemDetails['total_taxable']))
                            {{round($itemDetails['total_taxable']*$itemDetails['quantity'], 2)}}
                        @endif
                    </span>
                </td>

                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} invoice_cost">
                        @if(isset($itemDetails['total_invoiced_cost']))
                            {{round($itemDetails['total_invoiced_cost']*$itemDetails['quantity'], 2)}}
                        @endif
                    </span>
                </td>

                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} effective_cost">
                        @if(isset($itemDetails['total_effective_cost']))
                            {{round($itemDetails['total_effective_cost']*$itemDetails['quantity'], 2)}}
                        @endif
                    </span>
                </td>
            </tr>
        </table>
    </td>

    @foreach($itemFields as $group => $groupDetails)
        <td class="padding-0">
            <table>
                <tr>
                    @foreach($groupDetails['fields'] as $itemField)
                        @php
                            $readonly = false;
                            if(in_array($itemField, [
                                'tax', 'taxes', 'total_invoiced_cost', 'total_effective_cost', 'total_taxable'
                            ])) {
                                $readonly = true;
                            }

                            if(in_array($itemField, ['sku', 'purchase_invoice_cart_id', 'is_requested', 'product_id', 'id', 'created_at', 'updated_at']))
                            {
                                continue;
                            }

                            $type = "number";
                            if(in_array($itemField, ['expiry_date', 'manufacturing_date'])) {$type = "date";}
                            if($itemField == "vendor_batch_number") $type = 'text';

                            $max = null;
                            if($type == "date")
                            {
                                if($itemField == "manufacturing_date")
                                {
                                    $min = \Carbon\Carbon::today()->subMonth(3)->format('Y-m-d');
                                    $max = \Carbon\Carbon::today()->format('Y-m-d');
                                }
                                else
                                {
                                    $min = \Carbon\Carbon::today()->format('Y-m-d');
                                }
                            }
                            else if($type == "number")
                            {
                                $min = 0;
                            }

                            $name = ucwords(str_replace(" ", '', $itemField));
                            $name = str_replace("_", " ", $name);
                            if($itemField == "taxes")
                            {
                                $name = "Tax Rate (%)";
                            }

                            $required = '';
                            if(in_array($itemField, ['quantity', 'base_cost']))
                            {
                                if($itemField == 'quantity') $min = 1;
                                $required = 'required';
                            }

                            $value = '';
                            if(isset($itemDetails[$itemField]))
                            {
                                $value = $itemDetails[$itemField];
                                if(is_numeric($value))
                                {
                                    $value = round($value, 2);
                                }
                            }
                            else if($itemField == 'taxes')
                            {
                                if(!empty($taxes))
                                {
                                    $taxPercentage = $taxes->where('name', 'IGST')
                                    ->first()->percentage;
                                    $value = $taxPercentage;
                                }
                            }

                        @endphp
                        <td style="width:130px;max-width: 130px;">
                            <input {{$required}}
                                @if($readonly) readonly  @endif
                                @if(isset($min)) min="{{$min}}" @endif
                                @if(isset($max)) max="{{$max}}" @endif
                                onwheel="this.blur()"
                                data-original="{{$value}}"
                                @if($type == 'number')
                                    oninput="validity.valid||(value='');"
                                @endif
                                type="{{$type}}"
                                name="items[{{$itemDetails['sku']}}][{{$itemField}}]"
                                class="@if($type == 'number') text-right @endif {{$itemDetails['sku']}} {{$itemField}} form-control"
                                value="{{$value}}">
                        </td>
                    @endforeach
                </tr>
            </table>
        </td>
    @endforeach

    <td class="padding-0">
        <table>
            <tr>
                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} unit_taxable">
                        @if(isset($itemDetails['total_taxable']) && isset($itemDetails['variant_qty']))
                            {{round($itemDetails['total_taxable']/$itemDetails['variant_qty'], 2)}}
                        @endif
                    </span>
                </td>

                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} unit_invoiced">
                        @if(isset($itemDetails['total_invoiced_cost']) && isset($itemDetails['variant_qty']))
                            {{round($itemDetails['total_invoiced_cost']/$itemDetails['variant_qty'], 2)}}
                        @endif
                    </span>
                </td>

                <td class="padding-5" style="width:130px;max-width: 130px;">
                    <span class="{{$itemDetails['sku']}} unit_effective">
                        @if(isset($itemDetails['total_effective_cost']) && isset($itemDetails['variant_qty']))
                            {{round($itemDetails['total_effective_cost']/$itemDetails['variant_qty'], 2)}}
                        @endif
                    </span>
                </td>
            </tr>
        </table>
    </td>

</tr>