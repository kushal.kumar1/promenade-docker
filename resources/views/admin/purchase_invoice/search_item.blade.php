@foreach ($products as $product)
    @if($product instanceof \Niyotail\Models\ProductRequest)
        <div class="product" data-product-name="{{$product->name}}">
            <div class="btn btn-default product-request col-12-12"
                 data-sku="req{{$product->id}}"
                 data-product_id="{{$product->id}}"
                 data-barcode="{{$product->barcode}}">
                <h6 class="small"><span class="tag">REQUESTED</span><br>{{$product->brand->name}}</h6>
                <h4 class="regular bold">{{$product->name}}</h4>
                <p>{{$product->cost}}</p>
            </div>
        </div>
    @else
        <div class="product" data-barcode="{{$product->barcode}}" data-image="{{Image::getSrc($product->primaryImage, 80)}}" data-id="{{$product->id}}" data-product-name="{{$product->name}}">
            <div class="flex cross-center no-wrap gutter-sm">
                <div>
                    <img src="{{Image::getSrc($product->primaryImage, 80)}}" width="70" />
                </div>
                <div class="col-12-12">
                    <h6 class="small">{{$product->brand->name}}</h6>
                    <h4 class="regular bold">{{$product->name}}</h4>
                </div>
            </div>
            <div>
                <div class="flex gutter-sm">
                    @foreach ($product->variants as $variant)
                        <div class="col-4-12">
                            <div class="variant text-center" data-barcode="{{$product->barcode}}" data-cost="{{$variant->getFinalPrice()}}" data-product_id="{{$variant->product_id}}" data-id="{{$variant->id}}" data-sku="{{$variant->sku}}">
                                <div>&#8377; {{round($variant->getFinalPrice() - $variant->discount, 2)}}
                                    @if($variant->discount > 0)
                                        <span style="text-decoration: line-through;color:#ff6961">&#8377;{{$variant->getFinalPrice()}}</span>
                                    @endif
                                </div>
                                <div class="margin-t-5 capitalize">
                                    {{$variant->value}} <span class="small">({{$variant->quantity}} {{$variant->uom}})</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@endforeach
