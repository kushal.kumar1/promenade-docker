<div class="cart-overlay hidden">
  <div class="icon-loading">
    <i class="fa fa-2x fa-spinner fa-spin"></i>
  </div>
</div>
<div class="flex gutter-between">
  <h2 class="regular bold">Short Items Summary</h2>
  <div>
    <span class="small bold">QTY:</span> {{empty($cart) ? 0 : $cart->shortItems->sum('quantity')}} | <span class="small bold">ITEMS:</span> {{empty($cart) ? 0 : $cart->shortItems->count()}}
  </div>
</div>

<div class="flex gutter-between">
  <h2 class="regular bold">CART SUMMARY</h2>
  <div>
    <span class="small bold">QTY:</span> {{empty($cart) ? 0 : $cart->items->sum('quantity')}} | <span class="small bold">ITEMS:</span> {{empty($cart) ? 0 : $cart->items->unique('sku')->count()}}
  </div>
</div>
@if (!empty($cart) && ($cart->items->isNotEmpty() || $cart->shortItems->isNotEmpty()))
<div class="margin-t-20">
  <div class="flex gutter-sm">
    <div class="col-8-12 bold h6">
      Total
    </div>
    <div class="col-4-12 text-right bold h6">
        {{$cart->pretty_total}}
    </div>
  </div>
</div>
<hr class="hr hr-light margin-t-20" />
@endif
<div class="flex gutter-between margin-t-20">
  <h2 class="regular bold">CART ITEMS</h2>
</div>
@if (!empty($cart) && ($cart->items->isNotEmpty() || $cart->shortItems->isNotEmpty()))
<div class="cart-items">
  @foreach($cart->shortItems as $item)
    <hr class="hr hr-light margin-t-20" />
    <div class="flex gutter-sm short-item" data-id="{{$item->id}}">
      <div class="col-12-12">
        <div class="flex gutter-sm">
          <div class="col-8-12">
            <h6 class="regular"><span class="tag tag-warning">Short</span>
              {{$item->name}} X {{$item->quantity}} | ({{$item->taxClass->name}})
            </h6>
            <div class="margin-t-5" style="line-height: 20px;">
              <br>
              <span class="regular">
                <b>Discount:</b> &#8377; {{number_format($item->discount, 2)}} |
                <b>Invoice Cost:</b> &#8377; {{number_format($item->invoice_cost, 2)}}
              </span>
              <br>
              <span class="regular">
                <b>Post Tax Discount:</b> &#8377; {{number_format($item->post_tax_discount, 2)}} |
                <b>Effective Cost:</b> &#8377; {{number_format($item->effective_cost, 2)}}
              </span>
              <br>
            </div>
          </div>
          <div class="col-4-12 text-right">
            &#8377; {{$item->effective_cost}}
          </div>
          <div class="col-12-12">
            <span class="tag tag-cancelled pull-right cursor remove">Delete</span>
          </div>
        </div>
      </div>
    </div>
  @endforeach
  @foreach ($cart->items->sortByDesc('created_at') as $item)
  @include('admin.purchase_invoice.cart._item')
  @endforeach
</div>

<hr class="hr hr-light margin-t-20" />
<div class="margin-t-20 text-right">
  <form class="form" id="createInvoiceForm" method="post" action="{{route('admin::purchase-invoices.cart.create_invoice', [$cart->id])}}" data-destination="{{route('admin::purchase-invoices.index')}}">
    @csrf
    <input type="hidden" name="cart_id" value="{{$cart->id}}">
    @if(empty($productRequest) || (isset($productRequest) &&
        $productRequest->where('status', '!=', 'product_created')->count() < 1) && $cart->items->count() > 0)

      @php
        if($cart->items->where('received_quantity', '>', 0)->count()) {
            $reqDeliveryDate = true;
        }
      @endphp

      <div class="flex gutter-between">
        <div class="flex">
          <label>TCS Amount</label>
          <input class="form-control margin-t-5" type="number" step="2" name="tcs">
        </div>
      @if(!empty($reqDeliveryDate))
        <div class="flex">
          <label>Delivery date</label>
          <input required class="form-control margin-t-5" type="date" name="delivery_date">
        </div>
      @endif
      <div class="flex margin-t-10">
        <label>Eway Bill Number</label>
        <input  class="form-control margin-t-5" type="text"
                name="eway_number" @if($cart->getCartTotal() >= 50000) required @endif value="{{$cart->eway_number}}">
      </div>
        
      </div>
      <div class="flex gutter-between margin-t-30">
        <button type="submit" class="btn btn-primary create-invoice">Create Invoice</button>
      </div>
      @else
      <p>*** Invoice must have at lease 1 product | Invoice cannot be created with items pending for approval</p>
    @endif
  </form>
</div>
@else
<p class="margin-t-10">No items in the cart</p>
@endif
