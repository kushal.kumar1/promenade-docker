@isset($products)
@foreach ($products as $product)
    <hr class="hr hr-light margin-t-20" />
    <div class="product margin-t-20">
        <div class="flex cross-center no-wrap gutter-sm">
            <div>
                <img src="{{Image::getSrc($product->primaryImage, 80)}}" width="70" />
            </div>
            <div class="col-12-12">
                <span class="regular bold"> PID:</span> <span class="regular">{{$product->id}}</span>
                @if (!empty($product->barcode))
                    | <span class="regular bold">Barcode: </span><span class="regular">{{$product->barcode}}</span>
                @endif
                @if($product->brand)
                <h6 class="small">{{$product->brand->name}}</h6>
                @endif

                <h4 class="regular padding-v-10 bold">
                    <span class="tag @if($product->status == 1) tag-completed @else tag-cancelled @endif">
                        {{$product->status == 1 ? 'Active' : 'Inactive'}}
                    </span>
                    &nbsp;
                    {{$product->name}}</h4>
                <div class="margin-t-5">
                </div>
            </div>
        </div>
        <div>
            <div class="flex gutter-sm">
                @php
                    $poVariant = null;
                    if(!empty($product->po_sku)) {
                        $poVariant = $product->variants->where('sku', $product->po_sku)->first();
                    }
                @endphp
                @if(!empty($poVariant))
                    <div class="col-4-12">
                        <div class="variant text-center"
                             data-cost="{{$product->po_cost}}"
                             data-qty="{{$product->po_qty}}"
                             data-sku="{{$product->po_sku}}"
                             data-id="{{$poVariant->id}}">
                            <div>Cost &#8377; {{number_format($product->po_cost, 2)}}</div>
                            <div class="margin-t-5 capitalize">
                                <span class="bold">Qty:</span>  {{$product->po_qty}} | <span class="bold">#PO:</span> {{$product->po_id}}
                            </div>
                        </div>
                    </div>
                @else
                    @foreach ($product->variants as $variant)
                        <div class="col-4-12">
                            <div class="variant text-center" data-id="{{$variant->id}}">
                                <div>MRP &#8377; {{number_format($variant->mrp, 2)}}
                                </div>
                                <div class="margin-t-5 capitalize">
                                    {{$variant->value}} <span class="small">({{$variant->quantity}} {{$variant->uom}})</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endforeach
@endisset
@isset($requestedProducts)
@foreach ($requestedProducts as $req)
    <hr class="hr hr-light margin-t-20" />
    <div class="product product-requested margin-t-20" data-name="{{$req->name}}" data-id="{{$req->id}}">
        <div class="flex cross-center no-wrap gutter-sm">
            <div>
                <span class="tag tag-success">REQUESTED</span>
            </div>
            <div class="col-12-12">
                <h4 class="regular bold">{{$req->name}}</h4>
                @if (!empty($req->barcode))
                    <span class="regular bold">Barcode: </span><span class="regular">{{$req->barcode}}</span>
                @endif
                <div class="margin-t-5">
                </div>
            </div>
        </div>
    </div>
@endforeach
@endisset
@isset($orderItems)
    @php
        $groupedOrderItems = $orderItems->groupBy('product_id');
    @endphp
    @foreach ($groupedOrderItems as $items)
        @php
            $orderItem = $items->first();
            $product = $orderItem->product;
        @endphp
        <hr class="hr hr-light margin-t-20" />
        <div class="product margin-t-20">
            <div class="flex cross-center no-wrap gutter-sm">
                <div>
                    <img src="{{Image::getSrc($product->primaryImage, 80)}}" width="70" />
                </div>
                <div class="col-12-12">
                    <span class="regular bold"> PID:</span> <span class="regular">{{$product->id}}</span>
                    @if (!empty($product->barcode))
                        | <span class="regular bold">Barcode: </span><span class="regular">{{$product->barcode}}</span>
                    @endif
                    | <span class="regular bold">Sent Quantity: </span><span class="regular">{{$items->count()}}</span>
                    @if($product->brand)
                        <h6 class="small">{{$product->brand->name}}</h6>
                    @endif

                    <h4 class="regular padding-v-10 bold">
                    <span class="tag @if($product->status == 1) tag-completed @else tag-cancelled @endif">
                        {{$product->status == 1 ? 'Active' : 'Inactive'}}
                    </span>
                        &nbsp;
                        {{$product->name}}</h4>
                    <div class="margin-t-5">
                    </div>
                </div>
            </div>
            <div>
                <div class="flex gutter-sm">

                            <div class="col-4-12">
                                <div class="variant text-center" data-id="{{$orderItem->productVariant->id}}">
                                    <div>Unit Cost &#8377; {{number_format($orderItem->price, 2)}}
                                    </div>
                                    <div class="margin-t-5 capitalize">
                                        {{$orderItem->productVariant->value}} <span class="small">({{$orderItem->productVariant->quantity}} {{$orderItem->productVariant->uom}}) (MRP : &#8377; {{number_format($orderItem->productVariant->mrp)}})</span>
                                    </div>
                                </div>
                            </div>

                </div>
            </div>
        </div>
    @endforeach
@endisset
