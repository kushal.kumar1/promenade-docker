@extends('admin.master')

@section('css')
    <style>
        .variant {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #b4b4b4;
            transition: background 0.8s;
        }

        .variant-disabled {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #f3c7c7;
            color: #b4b4b4;
        }

        .variant:hover {
            border: 1px solid #d4d4d4;
            background: #fafafa radial-gradient(circle, transparent 1%, #fafafa 1%) center/15000%;
        }

        .variant:active {
            background-size: 100%;
            transition: background 0s;
            background-color: #e5e5e5;
        }

        .quantity.form-control {
            width: 70px;
            padding: 5px;
            text-align: center;
        }

        .cart-overlay {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.6);
        }

        .cart-overlay .icon-loading {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        .item-error {
            color: #f55753;
            font-size: 14px;
            font-weight: bold;
            border: 1px dashed #fb9492;
            padding: 5px 10px;
        }
    </style>
@append

@section('content')
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::purchase-invoices.cart-index')}}">
                <i class="fa fa-angle-left" aria-hidden="true"></i> Back to purchase carts
            </a>
        </div>

        <div class="margin-t-20">
            <div class="flex gutter no-wrap">
                <div class="col-12-12">
                    <div class="card">
                        <div class="flex">
                            <span class="tag tag-success">{{$cart->status}}</span>
                        </div>
                        <h4 class="margin-t-10"><strong>Purchase Invoice Cart</strong></h4>
                        <p>Vendor: {{$cart->vendor->name}}</p>
                        <hr class="hr hr-light margin-t-20"/>
                        <form id="purchaseInvoiceCart">
                            <div class="flex gutter-between gutter-sm">
                                <div class="col-3-12 margin-t-20">
                                    <div class="form-group">
                                        <label class="label">Warehouse</label>
                                        <select class="flex form-control" id="warehouse-id" name="warehouse_id">
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{$warehouse->id}}" @if($warehouse->id == $cart->warehouse_id) selected @endif>{{$warehouse->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="flex cross-center margin-t-30">
                                            <div class="radio margin-r-10">
                                                <input type="checkbox" name="is_local_purchase" id="isLocalPurchase"
                                                       value="1" @if($cart->is_local_purchase) checked @endif @if($cart->warehouse_id != 1) disabled="disabled" @endif/>
                                                <span class="indicator"></span>
                                            </div>
                                            <span>Is local purchase</span>
                                        </label>
                                    </div>
                                    <div class="form-group margin-t-20">
                                        <label class="label">Vendor Ref Id</label>
                                        <input type="text" name="vendor_ref_id" value="{{$cart->vendor_ref_id}}"
                                               class="form-control" @if($cart->is_local_purchase || $cart->type == \Niyotail\Models\PurchaseInvoiceCart::TYPE_STOCK_TRANSFER) disabled="disabled" @endif id="vendor_ref_id"/>
                                    </div>
                                </div>
                                <div class="col-3-12 margin-t-20 ">
                                    <div class="form-group">
                                        <label class="label">Purchase Platform</label>
                                        <select class="flex form-control" required name="platform_id">
                                            <option value="">Select platform</option>
                                            @foreach($purchasePlatforms as $platform)
                                                <option @if($cart->platform_id == $platform->id) selected
                                                        @endif value="{{$platform->id}}">{{$platform->platform}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="flex cross-center margin-t-30">
                                            <div class="radio margin-r-10">
                                                <input type="checkbox"
                                                       id="isDirectStorePurchase" value="1" name="is_direct_store_purchase"
                                                       @if($cart->is_direct_store_purchase) checked @else disabled="disabled" @endif/>
                                                <span class="indicator"></span>
                                            </div>
                                            <span>Is direct store purchase</span>
                                        </label>
                                    </div>
                                    <div class="form-group margin-t-20">
                                        <label class="label">Total Invoice Value</label>
                                        <input type="number" step="0.01" name="total_invoice_value" value="{{$cart->total_invoice_value}}" class="form-control" required @if($cart->type == \Niyotail\Models\PurchaseInvoiceCart::TYPE_STOCK_TRANSFER) readonly="readonly" @endif/>
                                    </div>
                                </div>
                                <div class="col-3-12 margin-t-20 ">
                                    <div class="form-group">
                                        <label class="label">Vendor Address</label>
                                        <select class="flex form-control" name="vendor_address_id"
                                                id="vendorAddressSelect">
                                            <option>Select address</option>
                                            @foreach($vendor_addresses as $address)
                                                <option
                                                        @isset($cart)
                                                        @if($cart->vendor_address_id == $address->id)
                                                        selected
                                                        @endif
                                                        @endisset
                                                        value="{{$address->id}}">
                                                    @isset($address->name) {{$address->name}} - @endif
                                                    {{$address->address}} - {{$address->gstin}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="margin-t-20 col-3-12">
                                    @if($cart->id && $cart->vendor_ref_id)
                                        {{--                                    <div class="flex ">--}}
                                        {{--                                        <button type="button" class="btn btn-danger" id="deleteIvoicesCart">--}}
                                        {{--                                            Delete Cart--}}
                                        {{--                                        </button>--}}
                                        {{--                                    </div>--}}
                                    @endif
                                    <div class="form-group">
                                        <label class="label">Invoice Date</label>
                                        <input type="date" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}"
                                               name="invoice_date" class="form-control"
                                               value="{{$cart->invoice_date}}" @if($cart->type == \Niyotail\Models\PurchaseInvoiceCart::TYPE_STOCK_TRANSFER) readonly @endif/>
                                    </div>
                                    <div class="form-group margin-t-30" id="store-selector">
                                        <label class="label">Select Store</label>
                                        <select class="flex form-control" name="store_id" id="select2-stores">
                                            @if($cart->store_id)
                                                <option selected value="{{$cart->store_id}}">{{$cart->store_id}}
                                                    - {{$cart->store->name}}</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" name="vendor_id" value="{{$cart->vendor_id}}">
                                <div class="col-12-12 text-right">
                                    @if ($cart->vendor_address_id && $cart->warehouse_id && $cart->vendor_ref_id && $cart->invoice_date)
                                        @if(!empty($cart->scanned_file))
                                            <a href="{{route('admin::purchase-invoices.cart.get-copy', $cart->id)}}"
                                               target="_blank" class="btn btn-link"
                                            >Download</a>
                                        @endif
                                        <button type="button" class="btn btn-success" id="upload-scanned-pi-button">
                                            <i class="fa fa-upload"></i> Upload scanned Invoice
                                        </button>
                                    @endif
                                    <button type="submit" class="btn btn-primary place-order">Update Invoice</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <form id="scanned-pi-form" class="hidden form-multipart"
              method="post"
              action="{{route('admin::purchase-invoices.cart.upload-copy', $cart->id)}}">
            @csrf
            <input type="file" name="file" accept="image/*,application/pdf" class="form-control" id="scanned-pi-field">
        </form>

        @if (
            $cart->vendor_address_id && $cart->warehouse_id && $cart->vendor_ref_id && $cart->invoice_date
        )
            @if($cart->source == \Niyotail\Models\PurchaseInvoiceCart::SOURCE_SYSTEM && !empty($cart->scanned_file) || $cart->source == \Niyotail\Models\PurchaseInvoiceCart::SOURCE_IMPORTER)
                <div class="margin-t-20">
                    <div class="flex gutter no-wrap">
                        <div class="col-6-12">
                            <div class="card">
                                <div class="">
                                    <label class="label">Search Products</label>
                                    <input id="products-search" type="text" class="form-control"/>
                                </div>
                                <div id="product-suggestions">
                                    <hr class="hr hr-light margin-t-20"/>
                                    <div class="flex gutter-sm">
                                        <button id="add-new-product" class="margin-t-25 btn btn-success">Add new item
                                        </button>
                                        <button id="add-short-product" class="margin-t-25 btn btn-success">Add short
                                            item
                                        </button>
                                    </div>
                                    <hr class="hr hr-light margin-t-20"/>
                                    <div id="product-suggestions-result-holder">

                                    </div>
                                </div>
                                @include('admin.purchase_invoice._product_request_form', ['isCart'=>true])
                                @include('admin.purchase_invoice._short_item_form', ['isCart'=>true])
                            </div>
                            <div class="card margin-t-25">
                                <label><strong>Requested Products</strong></label>
                                @foreach($productRequest as $req)
                                    <p class="margin-t-10">
                                        {{$loop->index + 1}}. {{$req->name}}
                                    </p>
                                    <div>
                                    <span class="inline-block tag
                                        @if($req->status == 'pending')
                                            tag-cancelled
                                            @else
                                            tag-success
                                        @endif
                                            ">{{$req->status}}
                                    </span>
                                        @if($req->status == "pending")
                                            <form class="form inline-block" method="POST"
                                                  action="{{route('admin::product_request.delete', $req->id)}}"
                                                  data-destination="{{route('admin::purchase-invoices.cart', $cart->id)}}">
                                                <button type="submit" class="tag tag-cancelled">Delete</button>
                                            </form>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-6-12">
                            <div class="card">
                                <div class="cart relative">
                                    @include('admin.purchase_invoice.cart._items')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>

    @include('admin.purchase_invoice.cart._itemForm')
@endsection

@section('js')
    <script>
        function storeTemplate(store) {
            if (!store.id) {
                return store.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${store.id} - ${store.name}</h5>`;
            html += '</div>';

            return $(html);
        };

        function storeSelection(store) {
            if (typeof store.name !== 'undefined') {
                return store.id + ' - ' + store.name;
            }
            return store.text;
        }


        $(function () {
            $("#vendorAddressSelect").select2();
            $("#productRackSelect").select2();
            $('#select2-stores').select2({
                minimumInputLength: 2,
                ajax: {
                    url: '{{route("admin::search.stores")}}',
                    dataType: 'json',
                    processResults: (data) => {
                        return {results: data};
                    }
                },
                templateResult: storeTemplate,
                templateSelection: storeSelection
            });
            @if(!$cart->is_direct_store_purchase)
            $('#store-selector').hide();
            @endif

            $('#warehouse-id').change(function (){
                let selectedWarehouseId = $(this).val();
                if(selectedWarehouseId === '1'){
                    $("#isDirectStorePurchase").prop('disabled', false);
                    $("#isLocalPurchase").prop('disabled', false);
                }else{
                    $("#isDirectStorePurchase").prop('disabled', true);
                    $("#isLocalPurchase").prop('disabled', true);
                    $("#isLocalPurchase").prop('checked', false);
                }
                $("#isDirectStorePurchase").trigger('change');
                $("#isLocalPurchase").trigger('change');
            });

            $("#isDirectStorePurchase").change(function () {
                if ($("#isDirectStorePurchase:checked").length) {
                    $('#store-selector').show();
                    $('#select2-stores').prop('required', true);
                } else {
                    $('#store-selector').hide();
                    $('#select2-stores').val(0).change();
                    $('#select2-stores').prop('required', false);
                }
            });

            $("#isLocalPurchase").change(function () {
                if ($(this).is(':checked')) {
                    $('#vendor_ref_id').prop('disabled', true);
                    $("#isDirectStorePurchase").prop('checked', true);
                    $("#isDirectStorePurchase").trigger('change');
                }else {
                    $('#vendor_ref_id').prop('disabled', false);
                    $("#isDirectStorePurchase").prop('checked', false);
                    $("#isDirectStorePurchase").trigger('change');
                }
            });
        })

        class Cart {
            constructor() {
                $('#products-search').on('input', (e) => this.onQueryChanged(e));
                $('#product-suggestions').on('click', '.variant', (e) => this.loadVariant($(e.currentTarget)));
                $('#product-suggestions').on('click', '.product-requested', (e) => this.loadVariant($(e.currentTarget), true));

                // $('#addItemForm').on('change', 'input', () => this.calculateTotalValues());

                $("#modal-item-form").on('click', '.close', (e) => this.clearForm());

                $('#addItemForm').on('click', '.continue-item-form-button', (e) => this.getItemValues(e));
                // $('#addItemForm').on('click', '.continue-item-form-button', (e) => this.showConfirmQtyView(e));

                $('#addItemForm').on('click', '.back-item-form-button', (e) => this.hideConfirmQtyView(e));

                $('#addItemForm').on('submit', (e) => this.addItem(e));

                $('#productRequestForm').on('submit', (e) => this.submitProductRequest(e, $(e.currentTarget)));
                $('#shortProductForm').on('submit', (e) => this.submitShortProduct(e, $(e.currentTarget)));

                $('#purchaseInvoiceCart').on('submit', (e) => this.submitCartForm(e, $(e.currentTarget)));

                $('.cart').on('click', '.quantity-subtract, .quantity-add', (e) => this.onQuantityChange(e));
                $('.cart').on('change', '.quantity', (e) => this.onQuantityChange(e));
                $('.cart').on('click', '.cart-item .remove', (e) => this.removeItem($(e.currentTarget).closest('.cart-item')));
                $('.cart').on('click', '.short-item .remove', (e) => this.removeShortItem($(e.currentTarget).closest('.short-item')));
                $('.cart').on('click', '.toggle-details', (e) => this.toggleDetails($(e.currentTarget).closest('.cart-item').find('.details')));
                $('.cart').on('click', '.edit-item', (e) => this.populateItemForm($(e.currentTarget)));

                $('.cart').on('click', '.create-invoice', (e) => this.createInvoiceClick());
                $("#add-new-product").on('click', () => this.toggleAddNewProductForm());
                $("#add-short-product").on('click', () => this.toggleAddShortProductForm());

                $("input[name='verify-product']").on('change', () => this.isItemVerified());
                $("#verifyItemButton").on('click', () => this.showItemFormIfVerified())

                $("#upload-scanned-pi-button").on('click', () => {
                    $("#scanned-pi-field").click();
                })
                $("#scanned-pi-field").on('change', this.handleUploadFieldChange);
                $('#deleteIvoicesCart').on('click', (e) => this.deleteInvoiceCart());


                this.validateMfgAndExpiryDates();
                this.initiateItemFormToggle();

            }

            deleteInvoiceCart() {
                dialog(
                    'Delete Cart',
                    'Are you sure you want to delete a cart?',

                    function () {
                        $.ajax({
                            url: "{{route('admin::purchase-invoices.cart.delete',$cart->id)}}",
                            type: 'post',
                            success: function () {
                                const toast = new Toast('Cart Succefully Deleted');
                                toast.show();
                                window.location.href = "{{route('admin::purchase-invoices.cart-index')}}";
                            },
                            error: (error) => {
                                showAllErrorsInToast(error);
                            }
                        });
                    });
            }

            handleUploadFieldChange() {
                $("#upload-scanned-pi-button").attr('disabled', true);
                // Get form
                const form = $('#scanned-pi-form')[0];
                // Create an FormData object
                const data = new FormData(form);
                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                    url: $("#scanned-pi-form").attr('action'),
                    data: data,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        const isReload = confirm('file uploaded successfully! Refresh now');
                        $("#upload-scanned-pi-button").attr('disabled', false);
                        if (isReload) {
                            location.reload();
                        }
                    },
                    error: function (e) {
                        this.onError(e);
                        $("#upload-scanned-pi-button").attr('disabled', false);
                    }
                });
            }

            initiateItemFormToggle() {
                $(".collapse-handle").click(function () {
                    const identifier = $(this).data('for');
                    if (identifier) {
                        $(".collapse-container#" + identifier).toggle();
                    }
                });
            }

            toggleAddNewProductForm() {
                $("#productRequestSection").toggle();
                $('#products-search').val('');
                $("#product-suggestions-result-holder").html('');
                if ($("#productRequestSection:visible").length) {
                    $("#add-new-product").html('Hide add new item form');
                } else {
                    $("#add-new-product").html('Add new item')
                }
            }

            toggleAddShortProductForm() {
                $("#shortProductRequestSection").toggle();
                $('#products-search').val('');
                $("#product-suggestions-result-holder").html('');
                if ($("#shortProductRequestSection:visible").length) {
                    $("#add-short-product").html('Hide add short item form');
                } else {
                    $("#add-short-product").html('Add short item')
                }
            }

            validateAndShowItemForm() {
                $("#itemFormArea").hide();
                $("input[name='verify-product']").prop('checked', false);
                $("#itemFormChecks").show();
                this.isItemVerified();
            }

            fillValidateItemFormInfo(name, barcode, mrp, unitMrp = null, isUnit) {
                $('#error').hide();
                $(".verify_product_name").html(name);
                $(".verify_product_barcode").html(barcode);
                $(".verify_variant_case_mrp").html(mrp);
                if (unitMrp) {
                    $(".verify_variant_single_mrp").html(unitMrp);
                    $(".verify_variant_unit_mrp").html('(' + unitMrp + '/unit)');
                }
                if(!isUnit){
                    $("#case-outer-field").css("display", "block");
                }
                else{
                    $("#case-outer-field").css("display", "none");
                }
            }

            showItemFormIfVerified() {
                const isVerified = this.isItemVerified();
                if (isVerified) {
                    $("#itemFormArea").show();
                    $("#itemFormChecks").hide();
                }
                else{
                    $('#error').text("Wrong MRP or Barcode entered. Please Try Again !!").show();
                }
            }

            isItemVerified() {
                let isVerified = true;
                if($.trim($(".verify_product_barcode").html()) !== $("#verify-product-barcode").val())
                {

                    isVerified = false;
                }
                if($.trim($(".verify_variant_single_mrp").html()) !== $("#verify-product-mrp").val())
                {
                    isVerified = false;
                }
                if ($("#case-outer-field").is(":visible")) {
                    if($.trim($(".verify_variant_case_mrp").html()) !== $("#verify-product-case-mrp").val())
                    {
                        isVerified = false;
                    }
                }
                return isVerified;

            }

            validateMfgAndExpiryDates() {
                $("input[name='manufacturing_date']").change(function () {
                    verifyDates($("input[name='manufacturing_date']"), $("input[name='expiry_date']"));
                })

                $("input[name='expiry_date']").change(function () {
                    verifyDates($("input[name='manufacturing_date']"), $("input[name='expiry_date']"));
                });

                function verifyDates($mfgDateField, $expiryDateField) {
                    const mfgDate = $mfgDateField.val();
                    const expiryDate = $expiryDateField.val();
                    let mDate = new Date(mfgDate);
                    let today = new Date();
                    if (mDate < today) {
                        mDate = today;
                    }
                    if (mfgDate && expiryDate) {
                        let xDate = new Date(expiryDate);
                        if (xDate <= mDate) {
                            const xDateString = formatDate(mDate);
                            $expiryDateField.val(xDateString);
                            $expiryDateField.attr("min", xDateString);
                            const toast = new Toast("Expiry date updated to " + xDateString);
                            toast.show();
                        }
                    } else if (mfgDate && !expiryDate) {
                        $expiryDateField.attr('min', formatDate(mDate));
                    }
                }
            }

            toggleDetails($target) {
                $target.toggleClass('hidden ');
            }

            populateItemForm($target) {
                this.hideConfirmQtyView();
                const detailPath = $target.data('edit-path');
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax(detailPath, {
                        method: 'GET'
                    });
                    this.searchRequest.done((item) => {
                        this.showFullProductForm(item);
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }, 100);
            }

            resetProductRequest() {
                $("#add-new-product").html('Add new item');
                $("#productRequestSection").hide();
            }

            onQueryChanged(e) {
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    const term = e.target.value;
                    $('#product-suggestions-result-holder').html('searching for "' + term + '"');
                    if (term) {
                        this.resetProductRequest();
                        if (this.searchRequest) {
                            this.searchRequest.abort();
                        }
                        this.searchRequest = $.ajax('{{route("admin::purchase-invoices.cart.suggestion", $cart->id)}}', {
                            method: 'GET',
                            data: {term: term}
                        });
                        this.searchRequest.done((products) => {
                            $("#productRequestSection").hide();
                            if (products.length) {
                                $('#product-suggestions-result-holder').html(products);
                            } else {
                                $('#product-suggestions-result-holder').html('');
                            }
                        });
                        this.searchRequest.fail((error) => this.onError(error));
                    }
                }, 100);
            }

            onQuantityChange(e) {
                const $cartItem = $(e.currentTarget).closest('.cart-item');
                let quantity = parseInt($cartItem.find('.quantity').val());
                let delay = 0;
                if ($(e.currentTarget).hasClass('quantity-add')) {
                    quantity++;
                    delay = 200;
                } else if ($(e.currentTarget).hasClass('quantity-subtract')) {
                    quantity--;
                    delay = 200;
                }

                if (quantity > 0) {
                    $cartItem.find('.quantity').val(quantity);
                    clearTimeout(this.updateTimer);
                    this.updateTimer = setTimeout(() => this.updateItem($cartItem), delay);
                }
            }

            loadVariant($variant, isRequested = false) {
                this.hideConfirmQtyView();
                const variantId = $variant.data('id');
                const poCost = $variant.data('cost');
                const poQty = $variant.data('qty');
                const poSku = $variant.data('sku');
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::variant.details")}}', {
                        method: 'GET',
                        data: {id: variantId}
                    });
                    this.searchRequest.done((variant) => {
                        const allVariants = variant.product.all_variants;
                        const unitVariant = allVariants.find((thisVar) => {
                            return thisVar.value == 'unit';
                        }) || null;
                        let taxPercent = 0;
                        let cessPercent = 0;
                        const taxes = variant.product.taxes;
                        const igst = taxes.find(tax => {
                            return tax.name == 'IGST'
                        });
                        const cess = taxes.find(tax => {
                            return tax.name == 'CESS'
                        });
                        if (cess) cessPercent = cess.percentage;
                        if (igst) taxPercent = igst.percentage;
                        this.showAddProductForm(
                            variant.product.name,
                            variant.sku,
                            variant.id,
                            variant.product.id,
                            isRequested,
                            taxPercent, cessPercent,
                            variant.mrp,
                            poCost, poQty, poSku
                        );
                        let isUnit = false;
                        if(unitVariant){
                            if(variant.mrp === unitVariant.mrp){
                                isUnit = true;
                            }
                        }
                        this.fillValidateItemFormInfo(variant.product.name,
                            variant.product.barcode,
                            variant.mrp,
                            unitVariant ? unitVariant.mrp : null,
                            isUnit
                        )
                        this.validateAndShowItemForm();
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }, 100);
            }

            showAddProductForm(name, sku = null, variantId = null, productId, isRequested,
                               taxPercent = 0, cessPercent = 0, mrp = 0, poCost = 0, poQty = 0, poSku = null) {
                $("input[name='value_type']").val('total_units').trigger('change');
                $("input[name='price_type']").val('all_inclusive').trigger('change');
                $("input[name='base_cost']").prop('disabled', true);
                $("input[name='final_rate']").prop('disabled', false);
                let title = name;
                if (poCost > 0 && poQty > 0 && poSku) {
                    title += '<br>Qty - ' + poQty + ' | Cost - ' + poCost + '/' + poSku;
                }

                $("#productNameText").html(title);
                $("#skuText").html(sku);
                $("form#addItemForm input[name='variant_id']").val(variantId);
                $("form#addItemForm input[name='product_id']").val(productId);
                $("form#addItemForm input[name='is_requested']").val(isRequested ? 1 : 0);
                $("form#addItemForm input[name='sku']").val(sku);
                $("form#addItemForm input[name='taxes']").val(Number(taxPercent) + Number(cessPercent));
                $("form#addItemForm input[name='min_price']").val(mrp);

                if (poCost > 0 && poQty > 0) {
                    $("form#addItemForm input[name='quantity']").val(poQty);
                    $("form#addItemForm input[name='base_cost']").val(poCost);
                    this.calculateTotalValues();
                }

                Modal.show($("#modal-item-form"));
            }

            showFullProductForm(item) {
                const ids = ['id', 'purchase_invoice_cart_id', 'product_id', 'quantity', 'cost', 'discount'];
                for (const [key, value] of Object.entries(item)) {
                    if ($(`input[name='${key}']`).length) {
                        let formattedVal = value && floatVal(value, key) ? floatVal(value) : value;

                        if (ids.indexOf(key) !== -1) {
                            // formattedVal = Math.round(formattedVal);
                        }
                        console.log(key, formattedVal);
                        $(`input[name='${key}']`).val(formattedVal);
                    }
                }

                const mrp = item.product_variant.mrp;

                $("input[name='min_price']").val(mrp);

                $("#valueType").val(item.value_type);
                $("#priceType").val(item.price_type);
                $("#discountType").val(item.discount_type);
                $("#postTaxDiscountType").val(item.post_tax_discount_type);

                Modal.show($("#modal-item-form"));
                $("#itemFormArea").show();
                $("#itemFormChecks").hide();
            }

            getItemValues() {
                const qty = $("form#addItemForm input[name='quantity']").val();
                const sku = $("form#addItemForm input[name='sku']").val();
                const mfgDate = $("form#addItemForm input[name='manufacturing_date']").val();
                const expDate = $("form#addItemForm input[name='expiry_date']").val();
                const vendorBatchNo = $("form#addItemForm input[name='vendor_batch_number']").val();
                const unitsType = $("form#addItemForm select[name='value_type']").val();
                const priceType = $("form#addItemForm select[name='price_type']").val();
                const cost = $("form#addItemForm input[name='cost']").val();
                const discountType = $("form#addItemForm select[name='discount_type']").val();
                const cashDiscount = $("form#addItemForm input[name='cash_discount']").val();
                const discount = $("form#addItemForm input[name='discount']").val();
                const postTaxDiscountType = $("form#addItemForm select[name='post_tax_discount_type']").val();
                const postTaxDiscount = $("form#addItemForm input[name='post_tax_discount']").val();
                const isDirectStorePurchase = $("input[name='is_direct_store_purchase']:checked").length > 0 ? 1 : 0;

                if(!mfgDate || !expDate) {
                    alert('Manufacturing date and expiry date should not be blank.');
                    return false;
                }

                let self = this;
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    $(".continue-item-form-button").prop('disabled', true);
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoices.valuePreview")}}', {
                        method: 'GET',
                        data: {
                            vendor_address_id: $("select[name='vendor_address_id']").val(),
                            is_direct_store_purchase: isDirectStorePurchase,
                            sku: sku,
                            quantity: qty,
                            unit_type: unitsType,
                            price_type: priceType,
                            cost: cost,
                            discount_type: discountType,
                            cash_discount: cashDiscount,
                            discount: discount,
                            post_tax_discount_type: postTaxDiscountType,
                            post_tax_discount: postTaxDiscount
                        }
                    });
                    this.searchRequest.done((response) => {
                        $(".continue-item-form-button").prop('disabled', false);
                        $("#item-preview").html(response);
                        self.showConfirmQtyView();
                    });
                    this.searchRequest.fail((error) => {
                        $(".continue-item-form-button").prop('disabled', false);
                        this.onError(error);
                    });
                }, 100);
            }

            showConfirmQtyView() {

                const qty = $("form#addItemForm input[name='quantity']").val();
                $("form#addItemForm input[name='invoiced_quantity']").val(qty);

                $("#itemFormArea").hide();
                $("#itemFormQtyConfirm").show();
            }

            hideConfirmQtyView() {
                $("#itemFormArea").show();
                $("#itemFormQtyConfirm").hide();
                $("#item-preview").empty();
            }

            addItem(e) {
                e.preventDefault();
                const formSData = $("#addItemForm").serializeArray();
                formSData.push({name: 'base_cost', value: $('input[name="base_cost"]').val()});
                let formDataObj = {};
                $.each(formSData, function (i, field) {
                    formDataObj[field.name] = field.value;
                });

                if(!formDataObj.expiry_date || !formDataObj.manufacturing_date) {
                    alert('Manufacturing date and expiry date should not be blank.');
                    return false;
                }

                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    $(".save-item-form-button").prop('disabled', true);
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoices.cart.addItem", [$cart->id])}}', {
                        method: 'POST',
                        data: formDataObj
                    });
                    this.searchRequest.done((response) => {
                        this.getItemsView();
                        this.clearForm();
                        const toast = new Toast("Item added/updated successfully!");
                        this.hideConfirmQtyView();
                        toast.show();
                        $(".save-item-form-button").prop('disabled', false);
                    });
                    this.searchRequest.fail((error) => {
                        this.onError(error);
                        $(".save-item-form-button").prop('disabled', false);
                    });
                }, 100);
            }

            clearForm() {
                $("#modal-item-form").find('input[name!="purchase_invoice_cart_id"]').val('');
                $("input[name='cost']").val('');
                Modal.hide($("#modal-item-form"));
            }

            getItemsView() {
                $('.cart-overlay').removeClass('hidden');
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoices.cart.items", [$cart->id])}}', {
                        method: 'get'
                    });
                    this.searchRequest.done((response) => {
                        $('.cart').html(response);
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }, 100);
            }

            updateItem($cartItem) {
                $('.cart-overlay').removeClass('hidden');
                const id = $cartItem.data('id');
                const quantity = parseInt($cartItem.find('.quantity').val());
                const old = parseInt($cartItem.find('.quantity').data('old'));
                const updateRequest = $.ajax('{{route("admin::purchase-invoices.cart.update_item")}}', {
                    method: 'POST',
                    data: {
                        item_id: id,
                        quantity: quantity,
                        cart_id: '{{$cart->id}}'
                    }
                });
                updateRequest.done((cart) => {
                    $('.cart').html(cart);
                });
                updateRequest.fail((error) => {
                    $cartItem.find('.quantity').val(old);
                    this.onError(error)
                });
            }

            removeItem($cartItem) {
                $('.cart-overlay').removeClass('hidden');
                const id = $cartItem.data('id');
                const request = $.ajax('{{route("admin::purchase-invoices.cart.remove_item")}}', {
                    method: 'POST',
                    data: {
                        item_id: id,
                        cart_id: '{{$cart->id}}'
                    }
                });
                request.done((cart) => {
                    $('.cart').html(cart);
                });
                request.fail((error) => this.onError(error));
            }

            removeShortItem($cartItem) {
                $('.cart-overlay').removeClass('hidden');
                const id = $cartItem.data('id');
                const request = $.ajax('{{route("admin::purchase-invoices.cart.remove_short_item", ['cart_id' => $cart->id])}}', {
                    method: 'POST',
                    data: {
                        item_id: id,
                        cart_id: '{{$cart->id}}'
                    }
                });
                request.done(() => {
                    this.getItemsView();
                });
                request.fail((error) => this.onError(error));
            }

            submitProductRequest(e, $form) {
                e.preventDefault();
                clearTimeout(this.searchTimer);

                let submitButton = $form.find('button');
                submitButton.attr('disabled', true);
                var formData = new FormData($form[0]);

                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::product_request.create")}}', {
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: true,
                        dataType: "json",
                    });
                    this.searchRequest.done((response) => {
                        $("#productRequestSection").hide();
                        $("#productRequestForm").find('input').val('');
                        submitButton.attr('disabled', false);
                        const toast = new Toast("Product request created successfully. You will be able to add the product once it is approved");
                        toast.show();
                        location.reload();
                    });
                    this.searchRequest.fail((error) => {
                        submitButton.attr('disabled', false);
                        this.onError(error)
                    });
                }, 100);
            }

            submitShortProduct(e, $form) {
                e.preventDefault();
                clearTimeout(this.searchTimer);

                let submitButton = $form.find('button');
                // submitButton.attr('disabled', true);
                const formData = new FormData($form[0]);

                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoices.cart.store.short-item", [$cart->id])}}', {
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        async: true,
                        dataType: "json",
                    });
                    this.searchRequest.done((response) => {
                        $("#shortProductRequestSection").hide();
                        $("#shortProductForm").find('input').val('');
                        submitButton.attr('disabled', false);
                        const toast = new Toast("Short product added successfully.");
                        toast.show();
                        this.getItemsView();
                    });
                    this.searchRequest.fail((error) => {
                        submitButton.attr('disabled', false);
                        this.onError(error)
                    });
                }, 100);
            }

            submitCartForm(e, $form) {
                e.preventDefault();
                clearTimeout(this.searchTimer);
                $(".place-order").attr('disabled', true);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoices.cart.update", [$cart->id])}}?' + $form.serialize(), {
                        method: 'POST',
                    });
                    this.searchRequest.done((response) => {
                        const toast = new Toast("Draft invoice saved successfully!");
                        toast.show();
                        location.reload();
                        $(".place-order").attr('disabled', false);
                    });
                    this.searchRequest.fail((error) => {
                        this.onError(error);
                        $(".place-order").attr('disabled', false);
                    });
                }, 100);
            }

            onError(error) {
                $('.cart-overlay').addClass('hidden');
                let message = '';

                if (error.responseJSON.statusText !== 'abort') {
                    if (typeof error.responseJSON.message === 'undefined') {
                        message = "An unexpected error has occurred";
                    }
                }

                if (typeof error.responseJSON.message !== 'undefined' && error.responseJSON.message.length) {
                    message = error.responseJSON.message;
                }

                if (message.length) {
                    const toast = new Toast(message);
                    toast.show();
                }
            }

            createInvoiceClick(e) {
                $(".create-invoice").attr('disabled', true);

                dialog(
                    'Create Invoice',
                    'Are you sure you want to create a purchase invoice?',
                    function () {
                        $(".create-invoice").attr('disabled', false);
                        $("#createInvoiceForm").submit();
                    },
                    function () {
                        $(".create-invoice").attr('disabled', false);
                    }
                );

            }
        }

        function getFloatVal($val) {
            if (typeof $val !== 'undefined' && $val.length && $val > 0) {
                return parseFloat($val).toFixed(6);
            }

            return 0;
        }

        function floatVal(n) {
            if (isNaN(Math.round(n))) return false;
            if (Math.round(n) !== Number(n) || ((Math.round(n) === Number(n)) && (n !== Number(n).toString()))) {
                return Number(n).toFixed(6);
            }
        }

        Date.prototype.addDays = function (days) {
            let date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }

        function formatDate(date) {
            let d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        new Cart();
    </script>
@append
