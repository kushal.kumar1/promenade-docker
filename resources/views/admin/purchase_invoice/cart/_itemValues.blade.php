<h3>Item Details</h3>
<table class="col-12-12 margin-t-10" cellpadding="2">
    <thead class="bg-grey-light">
        <tr>
            <td>Units</td>
            <td>Base cost</td>
            <td>Taxable</td>
            <td>Invoice cost</td>
            <td>Effective cost</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{number_format($values['total_units'], 0)}}</td>
            <td>{{number_format($values['total_base_cost'], 2)}}</td>
            <td>{{number_format($values['total_taxable'], 2)}}</td>
            <td>{{number_format($values['total_invoice'], 2)}}</td>
            <td>{{number_format($values['total_effective'], 2)}}</td>
        </tr>
    </tbody>
</table>

<table class="col-12-12 margin-t-10" cellpadding="2">
    <thead class="bg-grey-light">
    <tr>
        <td>Discount</td>
        <td>Tax</td>
        <td>Post tax discount</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>{{number_format($values['total_discount'], 2)}}</td>
        <td>{{number_format($values['total_tax'], 2)}}</td>
        <td>{{number_format($values['total_post_tax_discount'], 2)}}</td>
    </tr>
    </tbody>
</table>