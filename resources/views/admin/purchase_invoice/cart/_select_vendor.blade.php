<div id="modal-vendors" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Choose Vendor</h1>
        <div class="margin-t-20">
            <label class="label">Select A Vendor</label>
            <select id="select2-stores" class="form-control" style="width:100%;" data-url="{{route("admin::search.vendors")}}"></select>
            <button id="button-open-cart" class="btn btn-primary margin-t-20" data-url="{{route("admin::purchase-invoices.cart.create")}}" data-destination="{{route("admin::purchase-invoices.cart", ["id" => "#"])}}">Open Purchase Invoice Cart</button>
        </div>
    </div>
</div>