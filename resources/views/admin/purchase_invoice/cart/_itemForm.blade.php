<div id="modal-item-form" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <div id="itemFormChecks">
            <h1 class="h5">Please verify the item</h1>
            <hr>
            <label class="flex cross-center padding-10">
                <span>Product name - <span class="verify_product_name"></span></span>
            </label>
            <label>
                <div class="flex cross-center padding-10 form-control-group">
                <span class="margin-r-20">Barcode - <span class="verify_product_barcode hidden"></span></span>
                    <input class= type="text" name="verify-product" id="verify-product-barcode">
                </div>
            </label>
            <div class="flex padding-10 form-control-group">
                <span class="margin-r-10">MRP/Unit -
                    <span class="verify_variant_single_mrp hidden"></span>
                    <span class="verify_variant_unit_mrp hidden"></span>
                </span>
                <div>
                    <input type="text" name="verify-product" id="verify-product-mrp">
                </div>
            </div>
            <div id="case-outer-field" class="flex cross-center padding-10 margin-r-10 form-control-group" style="display:none;">

                <span class="margin-r-20">Case/Outer MRP -
                    <span class="verify_variant_case_mrp hidden"></span>
                </span>

                    <input type="text" name="verify-product" id="verify-product-case-mrp">

            </div>
            <div id="error" class="t-alert" role="alert"></div>
            <button class="margin-t-25 btn btn-primary" id="verifyItemButton">Proceed</button>
            <hr>
        </div>
        <form id="addItemForm">
            <div id="itemFormArea" style="display: none">
                <h1 class="h5">Add product details</h1>
                <hr class="hr-light">
                <p><strong id="productNameText"></strong></p>
                <p><strong id="skuText"></strong></p>
                <hr class="hr-light">
                <div class="">
                    <input type="hidden" name="purchase_invoice_cart_id" value="{{$cart->id}}">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="variant_id" value="">
                    <input type="hidden" name="product_id" value="">
                    <input type="hidden" name="is_requested" value="">
                    <input type="hidden" name="sku" value="">
                    <input type="hidden" name="min_price" value="">

                    <button type="button" class="continue-item-form-button btn btn-primary margin-t-20">Continue</button>
                    <hr class="hr-light">

                    @foreach($itemFields as $group => $groupDetails)
                        <table class="col-12-12 margin-t-10">
                            @php
                                $collapsibleSets = ['discounts', 'post_tax_discount', 'post_invoice_discount'];
                            @endphp
                            <thead
                                    @if(in_array($group, $collapsibleSets))
                                    class="collapse-handle" data-for="{{$group}}"
                                    @endif>
                                <tr>
                                    <th colspan="2" class="margin-t-20 padding-10 bg-grey-light">
                                        {{$groupDetails['label']}}
                                        @if(in_array($group, $collapsibleSets))
                                            <i class="fa fa-angle-down"></i>
                                        @endif
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="collapse-container fa-border"
                                   @if(in_array($group, $collapsibleSets))
                                   style="display: none;"
                                   @endif
                                   id="{{$group}}">
                                @if($group == 'base_cost')
                                    <tr>
                                        <td class="col-6-12 padding-10">
                                            All values are entered for
                                        </td>
                                        <td class="col-6-12 padding-10">
                                            <select class="form-control" name="value_type" id="valueType">
                                                <option value="total_units">Total units</option>
                                                <option value="per_unit">Per unit</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-6-12 padding-10">
                                            Cost type
                                        </td>
                                        <td class="col-6-12 padding-10">
                                            <select class="form-control" name="price_type" id="priceType">
                                                <option value="all_inclusive">Final rate (all incl. of discounts and taxes)</option>
                                                <option value="base_price">Base rate</option>
                                            </select>
                                        </td>
                                    </tr>
                                    @elseif($group == 'discounts')
                                    <tr>
                                        <td class="col-6-12 padding-10">
                                            Discount value type
                                        </td>
                                        <td class="col-6-12 padding-10">
                                            <select class="form-control" name="discount_type" id="discountType">
                                                <option value="flat">Flat</option>
                                                <option value="percentage">Percentage</option>
                                            </select>
                                        </td>
                                    @elseif($group == 'post_tax_discount')
                                    <tr>
                                        <td class="col-6-12 padding-10">
                                            Discount value type
                                        </td>
                                        <td class="col-6-12 padding-10">
                                            <select class="form-control" name="post_tax_discount_type" id="postTaxDiscountType">
                                                <option value="flat">Flat</option>
                                                <option value="percentage">Percentage</option>
                                            </select>
                                        </td>
                                @endif
                                @foreach($groupDetails['fields'] as $itemField)
                                    @php
                                        $readonly = false;
                                        if(in_array($itemField, [
                                            'tax', 'taxes', 'total_invoiced_cost', 'total_effective_cost', 'total_taxable'
                                        ])) {
                                            $readonly = true;
                                        }

                                        if(in_array($itemField, ['sku', 'purchase_invoice_cart_id', 'is_requested', 'product_id', 'id', 'created_at', 'updated_at']))
                                        {
                                            continue;
                                        }

                                        $type = "number";
                                        if(in_array($itemField, ['expiry_date', 'manufacturing_date'])) {$type = "date";}
                                        if($itemField == "vendor_batch_number" || $itemField == "product_rack") $type = 'text';

                                        $max = null;
                                        if($type == "date")
                                        {
                                            if($itemField == "manufacturing_date")
                                            {
                                                $min = \Carbon\Carbon::today()->subMonth(6)->format('Y-m-d');
                                                $max = \Carbon\Carbon::today()->format('Y-m-d');
                                            }
                                            else
                                            {
                                                $min = \Carbon\Carbon::today()->format('Y-m-d');
                                            }
                                        }
                                        else if($type == "number")
                                        {
                                            $min = 0;
                                        }

                                        $name = ucwords(str_replace(" ", '', $itemField));
                                        $name = str_replace("_", " ", $name);
                                        if($itemField == "taxes")
                                        {
                                            $name = "Tax Rate (%)";
                                        }

                                        $required = '';
                                        if(in_array($itemField, ['quantity', 'base_cost']))
                                        {
                                            if($itemField == 'quantity') $min = 1;
                                            $required = 'required';
                                        }
                                    @endphp
                                    <tr>
                                        <td class="col-6-12 padding-10">
                                            {{$name}}
                                        </td>
                                        <td class="col-6-12 padding-10">
                                            @if($itemField == 'product_rack' && !empty($warehouseRacks))
                                            <select name="product_rack" class="form-control" id="productRackSelect">
                                                <option value="">--Select--</option>
                                                @foreach($warehouseRacks as $rack)
                                                    <option value="{{$rack->id}}">{{$rack->reference}}</option>
                                                @endforeach
                                            </select>
                                            @else
                                            <input type="{{$type}}"
                                                   @if($type == 'number')
                                                   onwheel="this.blur()"
                                                   oninput="validity.valid||(value='');"
                                                   @endif
                                                   @if($readonly) readonly @endif
                                                   {{$required}}
                                                   @if($type == "number") step="any" @endif
                                                   name="{{$itemField}}"
                                                   @if(isset($min)) min="{{$min}}" @endif
                                                   @if(isset($max)) max="{{$max}}" @endif
                                                   class="form-control-group">
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach

                    <button type="button" class="continue-item-form-button btn btn-primary margin-t-20">Continue</button>
                </div>
            </div>
            <div id="itemFormQtyConfirm" style="display: none;">

                <div class="col-12 margin-t-10" id="item-preview">

                </div>
            
                <table class="col-12-12 margin-t-10">
                    @php
                        $collapsibleSets = ['discounts', 'post_tax_discount', 'post_invoice_discount'];
                    @endphp
                    <thead>
                        <tr>
                            <th colspan="2" class="margin-t-20 padding-10 bg-grey-light">
                                Item Quantity
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-6-12 padding-10">
                                Invoiced qty.
                            </td>
                            <td class="col-6-12 padding-10">
                                <input type="{{$type}}"
                                       onwheel="this.blur()"
                                       oninput="validity.valid||(value='');"
                                       name="invoiced_quantity" disabled
                                       class="form-control-group">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-6-12 padding-10">
                                Received qty.
                            </td>
                            <td class="col-6-12 padding-10">
                                <input type="{{$type}}"
                                       onwheel="this.blur()"
                                       oninput="validity.valid||(value='');"
                                       name="received_quantity"
                                       min="0"
                                       class="form-control-group">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button type="button" class="back-item-form-button btn btn-default margin-t-20 margin-r-10">Back</button>
                <button type="submit" class="save-item-form-button btn btn-primary margin-t-20">Add item to cart</button>
            </div>
        </form>
    </div>
</div>