<hr class="hr hr-light margin-t-20" />
<div class="cart-item margin-t-20" data-id="{{$item->id}}">
    <div class="flex gutter-sm">
        <div class="col-2-12" style="height:70px;">
            <img src="{{Image::getSrc($item->product->primaryImage, 80)}}" width="80" />
        </div>
        <div class="col-10-12">
            <div class="flex gutter-sm">
                <div class="col-8-12">
                    <h6 class="regular">{{$item->product->name}}</h6>
                    <div class="margin-t-5">
                        @if(!$item->is_requested)
                        <span class="capitalize">{{$item->productVariant->value}}</span>
                        <span class="small">[{{$item->productVariant->quantity}} {{$item->productVariant->uom}}]</span>
                        @endif
                    </div>
                    <div class="margin-t-5" style="line-height: 20px;">
                    @if(!$item->is_requested)
                      <span class="regular bold"> PID:</span> <span class="regular">{{$item->product->id}}</span>
                          @if (!empty($item->product->barcode))
                            | <span class="regular bold">Barcode: </span><span class="regular">{{$item->product->barcode}}</span>
                          @endif
                        @else
                        <span class="regular bold"> PID:</span> <span class="regular">{{$item->productRequest->id}}</span>
                        @if (!empty($item->productRequest->barcode))
                            | <span class="regular bold">Barcode: </span><span class="regular">{{$item->productRequest->barcode}}</span>
                        @endif
                    @endif
                        <br>
                        <span class="regular"><b>Cost:</b>
                            &#8377; {{number_format($item->cost/$item->quantity, 2)}}
                            | &#8377; {{number_format($item->cost/($item->quantity*$item->productVariant->quantity), 2)}} / unit
                        </span>
                        <br>
                        <span class="regular">
                            <b>MRP:</b>
                            @php
                                $allVariants = $item->product->allVariants;
                                $unitVariant = $allVariants->where('value', 'unit')->first();
                            @endphp
                            &#8377; {{number_format($item->productVariant->mrp, 2)}} / {{$item->productVariant->value}}
                            @if($unitVariant)
                            | &#8377; {{number_format($unitVariant->mrp, 2)}} / unit
                            @endif
                            <br>
                            <b>Chain margin:</b> {{$item->productVariant->mrp ? round(1 - (($item->cost/$item->quantity)/$item->productVariant->mrp), 4)*100 : 0}}%
                            <br>
                            <b>Invoiced Qty:</b> {{$item->quantity}} | <b>Received Qty:</b> {{$item->received_quantity}}
                        </span>
                    </div>
                </div>
                <div class="col-4-12 text-right">
                    &#8377; {{$item->pretty_total}}
                </div>
                <div class="col-12-12">
                    <span class="tag tag-success cursor toggle-details">Toggle DETAILS</span>
                    <span class="tag tag-success cursor edit-item" data-edit-path="{{route('admin::purchase-invoices.cart.item', [$cart->id, $item->id])}}">Edit</span>
                    <span class="tag tag-cancelled pull-right cursor remove">Delete</span>
                </div>
                <div class="col-12-12 hidden details">
                    @php
                        $itemArr = $item->toArray();
                    @endphp
                    @foreach($itemArr as $key=>$value)
                        @php
                            if(is_array($value)) continue;
                            if(in_array($key, ['id', 'purchase_invoice_cart_id', 'product_id', 'created_at', 'updated_at']))
                            {
                                continue;
                            }
                            $name = ucwords(str_replace("_", " ", $key));
                        @endphp
                        <div class="col-12-12 flex gutter-between fa-border">
                            <span class="col-6-12 padding-10 bg-grey-light">{{$name}}</span>
                            <span class="col-6-12 text-right padding-10">{{$value}}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-12-12">
          @if(!empty($cartErrors[$item->id]))
          <div>
              <span class="item-error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{$cartErrors[$item->id]}}</span>
          </div>
        @endif
        </div>
    </div>
</div>
