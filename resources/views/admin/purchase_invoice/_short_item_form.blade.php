<form id="shortProductForm"
@if(isset($shortItem))
    class="form" method="post"
      action="{{route('admin::purchase-invoices.cart.update.short-item', [$invoice->id, $shortItem->id])}}"
      data-destination="{{route('admin::purchase-invoices.editInvoice', $invoice->id)}}"
@endif
>
    @if(isset($cart))
    <input type="hidden" name="source" value="{{isset($data->source) ? $data->source : 'purchase_invoice_cart'}}">
    <input type="hidden" name="source_id" value="{{isset($data->source) ? $data->source : $cart->id}}">
    @endif

    @if(isset($shortItem))
        <input type="hidden" name="id" value="{{$shortItem->id}}">
    @endif

        <div class="col-12-12 margin-t-25" @if(!isset($shortItem)) style="display: none;" id="shortProductRequestSection" @endif>
            <h5 class="h5 bold">@if(isset($shortItem)) Edit @else Add @endisset short product</h5>
            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Product Name</label>
                <input class="form-control margin-t-5" required name="name"
                       value="{{isset($shortItem->name) ? $shortItem->name : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">HSN</label>
                <input class="form-control margin-t-5" required name="hsn" value="{{isset($shortItem->hsn) ? $shortItem->hsn : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Quantity</label>
                <input class="form-control margin-t-5" required name="quantity" type="number"
                       value="{{isset($shortItem->quantity) ? $shortItem->quantity : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">MRP</label>
                <input class="form-control margin-t-5" name="mrp" type="number" step="any"
                       value="{{isset($shortItem->mrp) ? $shortItem->mrp : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Tax Class</label>
                <select class="form-control margin-t-5" required name="tax_class" id="taxClassIdSelect">
                    @foreach($taxClasses as $tax)
                        <option value="{{$tax->id}}">{{$tax->name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group margin-t-30">
                <label class="danger"><strong>All values below are to be entered for total quantity</strong></label>
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Discount</label>
                <input class="form-control margin-t-5" name="discount" type="number" step="any"
                       value="{{isset($shortItem->discount) ? $shortItem->discount : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Post tax discount</label>
                <input class="form-control margin-t-5" name="post_tax_discount" type="number" step="any"
                       value="{{isset($shortItem->post_tax_discount) ? $shortItem->post_tax_discount : ""}}">
            </div>

            <div class="form-group margin-t-10">
                <label class="flex padding-v-10">Effective Cost</label>
                <input class="form-control margin-t-5" name="effective_cost" type="number" step="any"
                       value="{{isset($shortItem->effective_cost) ? $shortItem->effective_cost : ""}}">
            </div>

            @if(isset($shortItem))
                <button type="button" onclick="$('#editShortProduct_{{$shortItem->id}}').toggle();" class="center btn btn-primary margin-t-30">Cancel</button>
            @endif
            <button class="center btn btn-success margin-t-30">Submit</button>
        </div>
</form>