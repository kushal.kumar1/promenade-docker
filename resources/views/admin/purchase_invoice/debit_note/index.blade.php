@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Debit Notes
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection

