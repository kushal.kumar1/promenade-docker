@if(!empty($invoice->debit_note))
    <table class="col-12-12 margin-t-25 parent">
        <thead>
        <tr>
            <td class="padding-5">
                Debit Note ID
            </td>
            <td>Download</td>
            <td class="padding-5">
                Date
            </td>
            <td class="padding-5">
                Created at
            </td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="padding-5">
                {{$invoice->debit_note->id}}
            </td>
            <td class="padding-5">
                <a href="{{route('admin::purchase-invoices.debit-note-pdf', [$invoice->debit_note->id])}}" class="link" target="_blank">Download</a>
            </td>
            <td class="padding-5">
                {{\Carbon\Carbon::parse($invoice->debit_note->generated_at)->format('jS F Y')}}
            </td>
            <td class="padding-5">
                {{\Carbon\Carbon::parse($invoice->debit_note->created_at)->format('jS F Y, h:m A')}}
            </td>
        </tr>
        </tbody>
    </table>
@endif