@extends('admin.master')

@section('css')
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input.disabled
        {
            pointer-events: none;
            background: #e4e4e4;
        }

        /* Firefox */
        input[type=number], input[type=text], input[type=date] {
            -moz-appearance: textfield;
            padding: 4px;
            font-size: 12px;
        }

        .center
        {
            margin-right: auto;
            margin-left: auto;
            display: inherit;
        }

        .col-1-12
        {
            padding: 15px 0px;
        }

        .col-1-12:last-child input
        {
            border-right: 1px solid #c7c7c7;
            border-radius:0px;
        }

        .col-1-12 input
        {
            border-top: 1px solid #c7c7c7;
            border-left: 1px solid #c7c7c7;
            border-bottom: 1px solid #c7c7c7;
            border-radius:0px;
        }

        .card {
            overflow-x: scroll;
        }

        table
        {
            width: max-content;
        }

        table thead
        {
            background: #dedede;
        }

        table thead td
        {
            padding: 8px 6px;
            font-weight: bold;
            font-size: 13px;
        }

        table td
        {
            min-width: 60px;
            border-right: 1px solid #fff;
        }

        table.parent>tbody>tr:first-child
        {
            border-top: 1px solid #ddd;
        }

        table.parent>tbody>tr
        {
            background: #FAFAFA;
            border-bottom: 1px solid #ddd;
            border-right: 1px solid #ddd;
            border-left: 1px solid #ddd;
            font-size:13px;
        }

        table.parent>tbody>tr td
        {
            border-color: #DDD;
        }

        table tbody td.padding-5, span.padding-5
        {
            padding:5px;
        }

        td.product_details
        {
            width: max-content;
        }

        table td table td
        {
            width: 90px;
        }

        table td table td[colspan]
        {
            background: #aaa;
            border:0px;
            text-align: center;
        }

        table td table td:last-child
        {
            border:0px;
        }

        .variant {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #b4b4b4;
            transition: background 0.8s;
        }

        .variant-disabled {
            height: 100%;
            cursor: pointer;
            padding: 5px 15px;
            border-radius: 5px;
            border: 1px dashed #f3c7c7;
            color: #b4b4b4;
        }

        .variant:hover {
            border: 1px solid #d4d4d4;
            background: #fafafa radial-gradient(circle, transparent 1%, #fafafa 1%) center/15000%;
        }

        .variant:active {
            background-size: 100%;
            transition: background 0s;
            background-color: #e5e5e5;
        }

        .cart-overlay {
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            position: absolute;
            background-color: rgba(255, 255, 255, 0.6);
        }

        .cart-overlay .icon-loading {
            top: 50%;
            left: 50%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        .item-error {
            color: #f55753;
            font-size: 14px;
            font-weight: bold;
            border: 1px dashed #fb9492;
            padding: 5px 10px;
        }

        #floating-product-selector
        {
            position: fixed;
            background: #FFF;
            z-index: 3;
            bottom: 0px;
            padding: 20px;
            max-width: 500px;
            left: 30p;
            border: 1px solid #ddd;
            box-shadow: 1px 1px 13px #00000054;
            margin-left: auto;
            margin-right: auto;
            left: 0px;
            right: 0px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .invoice_table
        {
            width: 100%;
        }

        .invoice_table tr
        {
            background: #fafafa;
            border-bottom: 1px solid #FFF;
        }
        .invoice_table tr td:first-child
        {
            font-weight: bold;
            background: #efefef;
        }
        .invoice_table tr td
        {
            padding:10px;
        }

        .has-error
        {
            border:4px solid red !important
        }

    </style>
@append

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="regular light-grey">
                <a href="{{route('admin::purchase-invoices.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to Purchase Invoices</a>
            </div>
        </div>
    </div>

    <div class="container" style="padding-top: 0px;">

        @if(isset($invoice))
            @if($invoice->canBeUpdated())
            <div class="flex gutter">
                <form class="form" method="post"
                      action="{{route("admin::purchase-invoices.complete", [$invoice->id])}}"
                      data-destination="{{route("admin::purchase-invoices.editInvoice", [$invoice->id])}}"
                >
                    @csrf
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-check"></i>
                        Mark as complete
                    </button>
                </form>
            </div>
            <hr>
            @endif
        @endif

        @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
        <form class="form" method="post" action="{{ route('admin::purchase-invoice.save') }}" data-destination="{{ route('admin::purchase-invoices.index') }}">
        @endif
            @csrf
            <div class="card">
                <h3 class="h3">
                    <strong>
                        @isset($invoice)
                            @if($invoice->canBeUpdated())
                                Edit
                            @endif
                        @else Create @endisset
                        Purchase Invoice</strong>
                </h3>
                @isset($invoice)
                    <input type="hidden" name="invoice[id]" value="{{$invoice->id}}">
                @endisset

                @if(!empty($po))
                    <input type="hidden" name="invoice[purchase_order_id]" value="{{$po->id}}">
                @endif

                @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
                @if(!empty($po))
                    <p class="margin-t-5">Vendor - {{ $po->vendor->name }}</p>
                    <input type="hidden" name="invoice[vendor_id]" value="{{$po->vendor->id}}">
                    @else
                    <p class="margin-t-5">Vendor - {{ $invoice->vendor->name }}</p>
                    <p class="padding-v-10"><span class="tag tag-success">{{$invoice->reference_id}}</span></p>
                    <input type="hidden" name="invoice[vendor_id]" value="{{$invoice->vendor->name}}">
                @endif
                <div class="flex gutter-between margin-t-20">
                    <div class="col-2-12">
                        <div class="form-group">
                            <label class="flex padding-v-10 bold">Select Vendor Address</label>
                            <select
                                    @if(isset($invoice) && !$invoice->canBeUpdated()) disabled @endif
                                    class="flex form-control" name="invoice[vendor_address_id]" id="vendorAddressSelect">
                                @foreach($vendor_addresses as $address)
                                    <option
                                            @isset($invoice)
                                                @if($invoice->vendor_address_id == $address->id)
                                                    selected
                                                @endif
                                            @endisset
                                            value="{{$address->id}}">
                                        @isset($address->name) {{$address->name}} - @endif
                                        {{$address->address}} - {{$address->gstin}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2-12">
                        <div class="form-group">
                            <label class="flex padding-v-10 bold">Select Warehouse Address</label>
                            <select
                                    @if(isset($invoice) && !$invoice->canBeUpdated()) disabled @endif
                                    class="flex form-control" name="invoice[warehouse_id]" id="warehouseSelect">
                                @foreach($warehouses as $address)
                                    <option
                                            @isset($invoice)
                                                @if($invoice->warehouse_id == $address->id)
                                                    selected
                                                @endif
                                            @endisset
                                            value="{{$address->id}}">
                                        {{$address->name}} - {{$address->address}} - {{$address->gstin}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-2-12">
                        <div class="form-group">
                            <label class="flex padding-v-10 bold">Vendor Ref ID</label>
                            <input @if(isset($invoice) && !$invoice->canBeUpdated()) disabled @endif type="text" @isset($invoice) value="{{$invoice->vendor_ref_id}}" @endisset required name="invoice[vendor_ref_id]">
                        </div>
                    </div>
                    <div class="col-2-12">
                        <div class="form-group">
                            <label class="flex padding-v-10 bold">Invoice Date</label>
                            <input max="{{\Carbon\Carbon::today()->format('Y-m-d')}}"  @if(isset($invoice) && !$invoice->canBeUpdated()) disabled @endif type="date" @isset($invoice) value="{{$invoice->invoice_date}}" @endisset name="invoice[invoice_date]">
                        </div>
                    </div>
                </div>
                @endif

                @include('admin.purchase_invoice.detail')
            </div>
            <div class="card margin-t-25 padding-10">
                <table class="parent">
                    @include('admin.purchase_invoice.item-list-header')
                    <tbody>
                    @if(isset($invoice) && $invoice->items->count())
                    @foreach($invoice->items as $item)
                        @php
                            $grnItems = $item->grnItems->groupBy('sku');
                            $itemDetails = $item->toArray();
                            $isRequested = $item->is_requested;
                            if($isRequested) {
                                $product = $item->productRequest;
                                $mrp = $product->mrp;
                            }
                            else {
                                $product = $item->product;
                                $productVariant = $item->productVariant;
                                $mrp = $productVariant->mrp;
                                $itemDetails['variant_qty'] = $productVariant->quantity;
                                $itemDetails['ordered_variant'] = $productVariant->value;
                                $itemDetails['product_variants'] = $product->allVariants->keyBy('value')->sortByDesc('quantity');
                            }


                            $itemDetails['checked'] = true;
                            $itemDetails['grn_max'] = $item->getMaxGrnQty();
                            $itemDetails['name'] = $product->name;
                            $itemDetails['barcode'] = $product->barcode;
                            $itemDetails['sku'] = $isRequested ? 'req'.$product->id : $item->sku;
                            $itemDetails['is_requested'] = $isRequested;
                            $itemDetails['product_id'] = $product->id;
                            $itemDetails['grn_items'] = $grnItems;

                        @endphp
                        @include('admin.purchase_invoice.item-row')
                    @endforeach
                    @endif

                    </tbody>
                </table>

                @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
                <button class="margin-t-25 btn btn-success" type="submit">Save Invoice</button>
                    @else
                    <div class="col-12-12 margin-t-10">
                        <label class="col-12-12 pull-left padding-v-10">Delivery Date</label>
                        <br>
                        <div class="form-group margin-r-10" style="float:left; width:200px;">
                            <input disabled class=" form-control" min="{{\Carbon\Carbon::parse($invoice->invoice_date)->format('Y-m-d')}}" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}" type="date" id="grnDeliveryDate">
                        </div>
                        <div class="form-group" style="float:left; width:200px;">
                            <button disabled class="btn btn-success generate-grn" type="button">Generate GRN</button>
                        </div>
                    </div>
                @endif
            </div>

        @if(!isset($invoice) || (isset($invoice) && $invoice->canBeUpdated()))
        </form>
        @endif
    </div>

<div id="floating-product-selector" style="display: none;">
    <div class="product-html-holder">

    </div>
    <button id="exit-product-selector" class="center btn btn-danger margin-t-25">
        <i class="fa fa-times"></i>
        Close</button>
</div>

@endsection

@section('js')
    <script>
        $(function(){
            $("#vendorAddressSelect").select2();
            $("#warehouseSelect").select2();
            $("#addItemToInvoice").select2();
            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>

    <script>
        const invoiceStatus = '@isset($invoice->status) {{$invoice->status}} @endisset';
        const invoiceId = '@isset($invoice->id) {{$invoice->id}} @endisset';
        const invoiceItems = JSON.parse('@isset($invoice->items) {!! json_encode($invoice->items) !!} @endisset');

        class CreatePI {

            constructor() {
                $("#exit-product-selector").on('click', () => this.onExitSearchView());
                // $('.product-html-holder').on('click', '.product-request', (e) => this.addItem($(e.currentTarget), true));

                $("#productRequestForm").on('submit', (e) => this.submitProductRequest(e, $(e.currentTarget)));

                $("#addItemToInvoice").on('change', (e) => this.onAddNewProductSelectChange($(e.currentTarget)))

                this.initRowToggle();
                this.enableItemEditing();
            }

            initRowToggle()
            {
                $('.row-toggle').unbind();
                $('.row-toggle').on('click', (e) => this.enableEditing($(e.currentTarget)));
            }

            setTaxColumns(sourceState, destState)
            {
                if(+sourceState === +destState)
                {
                    // hide igst
                    $("input.cgst").parent().show();
                    $("input.sgst").parent().show();
                    $('div.cgst').show();
                    $('div.sgst').show();
                    $('div.igst').hide();
                    $("input.igst").val('');
                    $("input.igst").parent().hide();
                }
                else
                {
                    // hide c / s gst
                    $('div.cgst').hide();
                    $('div.sgst').hide();
                    $('div.igst').show();
                    $("input.cgst").val('');
                    $("input.cgst").parent().hide();
                    $("input.sgst").val('');
                    $("input.sgst").parent().hide();
                    $("input.igst").parent().show();
                }
            }

            enableEditing($target)
            {
                const parent = $target.closest('tr.item');
                const sku = parent.attr('data-sku');
                const quantity = getFloatVal($(`.${sku}.quantity`).val());
                const baseCost = getFloatVal($(`.${sku}.base_cost`).val());
                // if(!quantity || !baseCost)
                // {
                //     const toast = new Toast("Base cost and quantity is required.");
                //     toast.show();
                //     return false;
                // }

                const statusField = $target.parent('td').find('.is-active-row');
                const currentStatus = statusField.val();
                statusField.val(currentStatus == 1 ? 0 : 1);
                this.enableItemEditing();
            }

            enableItemEditing()
            {
                if(invoiceStatus.trim() == "completed")
                {
                    $("td>input:not(.grn_qty)").each(function(){
                        const html = '<span class="padding-5">'+$(this).val()+'</span>';
                        $(this).parent("td").html(html);
                        $(this).remove();
                    });
                }
                else
                {
                    $('button[type="submit"]').prop('disabled', false);
                    let self = this;
                    $('.is-active-row').each(function(){
                        const isActiveRowRef = $(this);
                        let isActivate = $(this).val() == 1;
                        const parent = $(this).closest('tr.item');
                        const sku = parent.attr('data-sku');

                        parent.find('.row-toggle-cancel').hide();
                        if(isActivate)
                        {
                            $('button[type="submit"]').prop('disabled', true);
                            const $rowToggleCancelBtnRef = parent.find('.row-toggle-cancel');
                            $rowToggleCancelBtnRef.show();
                            $rowToggleCancelBtnRef.unbind();
                            $rowToggleCancelBtnRef.click(function(){
                                parent.find('input[data-original]').each(function(){
                                    const originalVal = $(this).attr('data-original');
                                    $(this).val(originalVal);
                                    $(this).addClass('disabled');
                                    $rowToggleCancelBtnRef.hide();
                                    parent.find('.row-toggle').html('Edit');
                                    isActiveRowRef.val(0);
                                    self.calculateTotal(sku);
                                    let allSaved = true;
                                    $('.is-active-row').each(function(){
                                        if($(this).val() == 1 && allSaved)
                                        {
                                            allSaved = false;
                                        }
                                    });
                                    if(allSaved)
                                    {
                                        $('button[type="submit"]').prop('disabled', false);
                                    }
                                });
                            });
                        }
                        self.enableItemRowEventHandlers(sku);
                        parent.find('.row-toggle').html(isActivate ? 'Done' : 'Edit');
                        parent.find("input[type!='checkbox']").addClass('disabled');
                        if(isActivate)
                        {
                            parent.find("input[type!='checkbox']").removeClass('disabled');
                        }
                    });
                }
            }

            enableItemRowEditing($row)
            {

            }

            enableItemRowEventHandlers(sku)
            {
                this.calculateTotal(sku);
                $('input.'+sku).unbind();
                $('input.'+sku).keyup(() => {
                    this.calculateTotal(sku);
                });
                const mfgDateField = $(`.${sku}.manufacturing_date`);
                const expDateField = $(`.${sku}.expiry_date`);
                mfgDateField.unbind();
                mfgDateField.change(function(){
                    this.verifyDates(mfgDateField, expDateField);
                })
                expDateField.unbind();
                expDateField.change(function() {
                    this.verifyDates(mfgDateField, expDateField);
                });
            }

            calculateTotal(sku) {
                const quantity = getFloatVal($(`.${sku}.quantity`).val());
                const baseCost = getFloatVal($(`.${sku}.base_cost`).val());
                const tradeDiscount = getFloatVal($(`.${sku}.trade_discount`).val());
                const schemeDiscount = getFloatVal($(`.${sku}.scheme_discount`).val());
                const qpsDiscount = getFloatVal($(`.${sku}.qps_discount`).val());
                const cashDiscount = getFloatVal($(`.${sku}.cash_discount`).val());
                const programScheme = getFloatVal($(`.${sku}.program_schemes`).val());
                const otherDiscount = getFloatVal($(`.${sku}.other_discount`).val());

                const postTaxDiscount = getFloatVal($(`.${sku}.post_tax_discount`).val());
                const rewardCashback = getFloatVal($(`.${sku}.reward_points_cashback`).val());

                const postInvoiceScheme = getFloatVal($(`.${sku}.post_invoice_schemes`).val());
                const postInvoiceQps = getFloatVal($(`.${sku}.post_invoice_qps`).val());
                const postInvoiceClaim = getFloatVal($(`.${sku}.post_invoice_claim`).val());
                const postInvoiceIncentive = getFloatVal($(`.${sku}.post_invoice_incentive`).val());
                const postInvoiceOther = getFloatVal($(`.${sku}.post_invoice_other`).val());
                const postInvoiceTot = getFloatVal($(`.${sku}.post_invoice_tot`).val());

                const taxPercent = getFloatVal($(`.${sku}.taxes`).val());

                const variantQty = $(`.${sku}.variant_qty`).length ? $(`.${sku}.variant_qty`).val() : 0;

                const values = this.calculateValues(
                    baseCost, tradeDiscount, schemeDiscount, qpsDiscount, cashDiscount,
                    programScheme, otherDiscount, postTaxDiscount, rewardCashback,
                    postInvoiceIncentive, postInvoiceOther, postInvoiceTot,
                    postInvoiceScheme, postInvoiceQps, postInvoiceClaim,
                    taxPercent, variantQty
                );

                $(`.${sku}.tax`).val(values.tax);
                $(`.${sku}.total_taxable`).val(values.total_taxable_cost.toFixed(2));
                $(`.${sku}.total_invoiced_cost`).val(values.total_invoice_cost.toFixed(2));
                $(`.${sku}.total_effective_cost`).val(values.total_effective_cost.toFixed(2));

                const taxableCost = getFloatVal(values.total_taxable_cost * quantity);
                const InvoiceCost = getFloatVal(values.total_invoice_cost * quantity);
                const EffectiveCost = getFloatVal(values.total_effective_cost * quantity);
                $(`.${sku}.taxable_cost`).html(taxableCost);
                $(`.${sku}.invoice_cost`).html(InvoiceCost);
                $(`.${sku}.effective_cost`).html(EffectiveCost);

                const unitTaxableCost = getFloatVal(values.unit_taxable_cost);
                const unitInvoiceCost = getFloatVal(values.unit_invoiced_cost);
                const unitEffectiveCost = getFloatVal(values.unit_effective_cost);
                $(`.${sku}.unit_taxable`).html(unitTaxableCost > 0 ? unitTaxableCost : '');
                $(`.${sku}.unit_invoiced`).html(unitInvoiceCost > 0 ? unitInvoiceCost : '');
                $(`.${sku}.unit_effective`).html(unitEffectiveCost > 0 ? unitEffectiveCost : '');

                const mrp = +$(`.${sku}.mrp`).val();
                const parent = $('tr.item[data-sku="' + sku + '"]');
                let $btnRef = parent.find('.row-toggle');
                const isPriceOk = this.validatePricesAgainstMrp(mrp,
                    baseCost,
                    values.total_taxable_cost,
                    values.total_invoice_cost,
                    values.total_effective_cost,
                    $btnRef
                );
                if(!isPriceOk)
                {
                    parent.addClass('has-error');
                }
                else
                {
                    parent.removeClass('has-error');
                }

                this.setTotalValues();
            }

            setTotalValues()
            {
                let total_taxable = 0;
                let total_invoice = 0;
                let total_effective = 0;
                $(".taxable_cost").each(function(){
                    total_taxable = total_taxable + Number($(this).html());
                })
                $(".invoice_cost").each(function(){
                    total_invoice = total_invoice + Number($(this).html());
                })
                $(".effective_cost").each(function(){
                    total_effective = total_effective + Number($(this).html());
                })

                $(".total_taxable").html(total_taxable);
                $(".total_invoice").html(total_invoice);
                $(".total_effective").html(total_effective);
            }


            verifyDates(mfgDateField, expDateField)
            {
                const mfgDate =  mfgDateField.val();
                const expiryDate = expDateField.val();
                let mDate = new Date(mfgDate);
                let today = new Date();
                if(mDate < today)
                {
                    mDate = today;
                }
                if(mfgDate && expiryDate)
                {
                    let xDate = new Date(expiryDate);
                    if(xDate <= mDate)
                    {
                        const xDateString = formatDate(mDate);
                        expDateField.val(xDateString);
                        expDateField.attr("min", xDateString);
                        const toast = new Toast("Expiry date updated to "+xDateString);
                        toast.show();
                    }
                }
                else if(mfgDate && !expiryDate)
                {
                    expDateField.attr('min', formatDate(mDate));
                }
            }

            calculateValues(
                baseCost, tradeDiscount, schemeDiscount, qpsDiscount, cashDiscount,
                programScheme, otherDiscount, postTaxDiscount, rewardCashback,
                postInvoiceScheme, postInvoiceQps, postInvoiceClaim, postInvoiceIncentive, postInvoiceOther, postInvoiceTot,
                taxPercent, variantQty
            )
            {
                let totalTaxableCost = baseCost - (
                    Number(tradeDiscount) +
                    Number(schemeDiscount) +
                    Number(qpsDiscount) +
                    Number(cashDiscount) +
                    Number(programScheme) +
                    Number(otherDiscount)
                );

                const tax = (totalTaxableCost*(taxPercent/100)).toFixed(2);

                let totalInvoiceCost = (Number(tax) + Number(totalTaxableCost)) - (Number(postTaxDiscount) + Number(rewardCashback));

                let totalEffectiveCost = totalInvoiceCost - (
                    Number(postInvoiceScheme) +
                    Number(postInvoiceQps) +
                    Number(postInvoiceClaim) +
                    Number(postInvoiceIncentive) +
                    Number(postInvoiceOther) +
                    Number(postInvoiceTot)
                );

                let unitTaxableCost = 0;
                let unitInvoicedCost = 0;
                let unitEffectiveCost = 0;
                if(variantQty > 0)
                {
                    unitTaxableCost = totalTaxableCost/variantQty;
                    unitInvoicedCost = totalInvoiceCost/variantQty;
                    unitEffectiveCost = totalEffectiveCost/variantQty;
                }

                return {
                    'total_taxable_cost': totalTaxableCost,
                    'tax': tax,
                    'total_invoice_cost': totalInvoiceCost,
                    'total_effective_cost': totalEffectiveCost,
                    'unit_taxable_cost': unitTaxableCost,
                    'unit_invoiced_cost': unitInvoicedCost,
                    'unit_effective_cost': unitEffectiveCost
                };
            }

            validatePricesAgainstMrp(mrp,
                                     baseCost,
                                     totalTaxableCost,
                                     totalInvoicedCost,
                                     totalEffectiveCost,
                                     $btnRef
            )
            {
                if(mrp > 0) {
                    if(+baseCost > mrp || +totalTaxableCost > mrp || +totalInvoicedCost > mrp ||
                        +totalEffectiveCost > mrp)
                    {
                        const toast = new Toast("Base / Taxable / Invoice / Effective cost should not exceed " +
                            "MRP "+mrp);
                        toast.show();
                        $("button[type='submit']").prop('disabled', true);
                        return false;
                    }
                }
                return true;
            }

            onExitSearchView()
            {
                $(".product-html-holder").empty();
                $("#productRequestSection").hide();
                $("#floating-product-selector").hide('slide');
                $("#searchByBarcode").val('');
            }

            onAddNewProductSelectChange($target)
            {
                const selectedProduct = $target.val();
                if(selectedProduct === "add_new")
                {
                    $("#productRequestSection").show();
                    $("#floating-product-selector").show('slide');
                    $("#addItemToInvoice").val('').trigger('change');
                }
                else
                {
                    const itemToAdd = invoiceItems.find((item) => {
                        return item.id == selectedProduct;
                    }) || null;

                    if(!itemToAdd)
                    {
                        return false;
                    }
                    let sku = itemToAdd.sku;
                    let barcode = '';
                    if(itemToAdd.is_requested)
                    {
                       sku = 'req'+itemToAdd.product_id;
                       barcode = itemToAdd.product_request.barcode;
                    }
                    else
                    {
                        barcode = itemToAdd.product.barcode;
                    }

                    this.addItem(sku, itemToAdd.product_id, barcode);
                }
            }

            addItem(sku, productId, barcode) {
                let existingItem = $('.item[data-barcode="'+barcode+'"][data-product-id="'+productId+'"][data-sku="'+sku+'"]');

                if(existingItem.length &&  existingItem.length === 1)
                {
                    existingItem.find('.row-toggle').click();
                    const existingQty = existingItem.find('.quantity').val();
                    const increment = 1;
                    existingItem.find('.quantity').val(existingQty ? Number(existingQty) + increment : 1);
                    existingItem.find('.row-toggle').click();
                    const toast = new Toast('Quantity increased by 1');
                    toast.show();
                }
                // else
                // {
                //     const itemProps = {
                //         product_id: productId,
                //         sku: sku,
                //         name: productName,
                //         barcode: barcode,
                //         quantity: 1,
                //         is_requested: isRequested ? 1 : 0
                //     };
                //     this.prependItemRow(itemProps);
                // }
                this.calculateTotal(sku);
                $("#addItemToInvoice").val('').trigger('change');
                this.enableRowBySku(sku);
                this.onExitSearchView();
            }

            enableRowBySku(sku)
            {
                const $rowRef = $("tr.item[data-sku='"+sku+"']")
                $rowRef.find('.is-active-row').val(1);
                $rowRef.find("input[type!='checkbox']").removeClass('disabled');
            }

            clearProductRequestForm()
            {
                $("#productRequestForm").find('input[name!="invoice_id"]').val('');
                $("#productRequestForm").find('select').val('');
            }

            submitProductRequest(e, $form)
            {
                e.preventDefault();
                $form.find('input[name="barcode"]').val($("#searchByBarcode").val());
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::product_request.create")}}?'+$form.serialize(), {
                        method: 'POST',
                    });
                    this.searchRequest.done((response) => {

                        const itemProps = {
                            product_id: response.id,
                            sku: null,
                            name: response.name,
                            barcode: response.barcode,
                            quantity: 1
                        };
                        this.prependItemRow(itemProps);

                        this.onExitSearchView();
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }, 100);
            }

            prependItemRow(itemProps)
            {
                clearTimeout(this.searchTimer);
                this.searchTimer = setTimeout(() => {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::purchase-invoice.item_html")}}', {
                        method: 'GET',
                        data: itemProps
                    });
                    this.searchRequest.done((productRowHtml) => {
                        $('table.parent>tbody').prepend(productRowHtml);
                        this.initRowToggle();
                        $('table.parent>tbody>tr:first-child').find('.is-active-row').val(1);
                        this.enableItemEditing();
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }, 100);
            }

            onError(error){
                $('.cart-overlay').addClass('hidden');
                let message = error.responseJSON.message;
                if (message == undefined) {
                    message = "An unexpected error has occurred";
                }
                const toast = new Toast(message);
                toast.show();
            }

        }

        function getFloatVal($val)
        {
            if(typeof $val !== 'undefined' && $val > 0)
            {
                return parseFloat($val).toFixed(2);
            }

            return 0;
        }

        function floatVal(n){
            if(isNaN(Math.round(n))) return false;
            if(Math.round(n) !== Number(n) || ((Math.round(n) === Number(n)) && (n !== Number(n).toString())))
            {
                return Number(n).toFixed(2);
            }
        }

        Date.prototype.addDays = function(days) {
            let date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }

        function formatDate(date) {
            let d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }

        new CreatePI();
    </script>
@endsection