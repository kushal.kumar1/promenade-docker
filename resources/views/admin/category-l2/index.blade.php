@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <h1 class="h1">Category L2</h1>
            <div>
                <a class="btn btn-primary" href="{{route('admin::category_l2.create')}}">Add Category L2</a>
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection
