@extends('admin.master')

@section('content')

@if (!empty($category))
<form class="form" action="{{route('admin::category_l2.update', ['id' => $category->id])}}" method="POST">
@else 
<form class="form" action="{{route('admin::category_l2.create')}}" method="POST" data-destination="{{route("admin::category_l2.index")}}">
@endif
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::category_l2.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Back to All Category L2</a>
        </div>
        
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    @if (!empty($category))
                        <h1 class="h1 margin-r-20 bold">{{$category->name}}</h1>
                    @else 
                        <h1 class="h1 margin-r-20 bold">Create a new category l2</h1>
                    @endif
                </div>

                <div>
                    <button type="submit" class="btn btn-success">
                        @if(!empty($category))
                            Update
                        @else
                            Create
                        @endif
                    </button>
                </div>
            </div>
        </div>

        <div class="flex gutter">
            <div class="col-12-12 margin-t-30">
                <div class="card">
                    <div class="flex gutter">
                        <div class="col-6-12">
                            <div class="margin-t-15">
                                <label class="label">Name</label>
                                <input class="form-control" name="name" value="{{!empty($category) ? $category->name : ""}}" />
                                <span class="error error-name"></span>
                            </div>
                        </div>

                        <div class="col-6-12">
                            <div class="margin-t-5">
                                <label class="label">Collection</label>
                                <select id="select2-category-l2" name="category_l1_id" class="form-control" style="width:100%;">
                                    @if (!empty($category) && !empty($category->collection))
                                    <option value="{{ $category->collection->id }}" selected>{{ $category->collection->name }}</option>
                                    @endif
                                </select>
                                <span class="error error-category_l1_id"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    function categoryTemplate(category) {
        if (!category.id) {
            return category.name;
        }
        let html = '<div class="padding-10">';
        html += `<h5 class="h6 bold">${category.name}</h5>`;
        html += '</div>';

        return $(html);
    };

    function categorySelection(category){
        return category.text;
    }
    
    $('#select2-category-l2').select2({
        minimumInputLength: 2,
        ajax: {
            url: '{{route("admin::search.cl2")}}',
            dataType: 'json',
            processResults: (data) => { return { results: data }; }
        },
        templateResult: categoryTemplate,
        templateSelection: categorySelection
    });

</script>
@append
