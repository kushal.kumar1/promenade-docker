@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Storages
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-storage-add">Add Storage</button>
            </div>
        </div>
        @include('admin.grid')

        <div id="modal-storage-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h5">Add Storage</h5>
                <form class="form margin-t-30" method="post" action="{{route('admin::storages.store')}}" autocomplete="off">
                    {{csrf_field()}}
                    <label class="label">Name</label>
                    <input class="form-control" name="name" />
                    <label class="label margin-t-30">Warehouse</label>
                    <select class="form-control" name="warehouse_id">
                        @foreach ($warehouses as $warehouse)
                            <option value="{{$warehouse->id}}">{{$warehouse->name}}</option>
                        @endforeach
                    </select>
                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
