@if($shipment->status == 'pending_rto')
<form class="form" method="POST" action="{{route('admin::shipments.rto')}}">
  <input type="hidden" name="id" value="{{$shipment->id}}">
  <input type="hidden" name="type" value="reject">
  <button type="button" class="btn btn-sm btn-danger btn-reject-rto"><i class="fa fa-truck" aria-hidden="true"></i> Reject RTO</button>
</form>
@endif
@if($shipment->status != 'pending_retry')
<form class="form" method="POST" action="{{route('admin::shipments.rto')}}">
  <input type="hidden" name="id" value="{{$shipment->id}}">
  <button type="button" class="btn btn-sm btn-danger btn-rto"><i class="fa fa-truck" aria-hidden="true"></i> @if($shipment->status == 'pending_rto') Approve @endif RTO</button>
</form>
@endif
