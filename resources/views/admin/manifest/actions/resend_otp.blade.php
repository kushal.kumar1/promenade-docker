@if($shipment->status == 'dispatched')
<form class="form" method="POST" action="{{route('admin::shipments.resend-otp')}}">
  <input type="hidden" name="id" value="{{$shipment->id}}">
  <button type="button" class="btn btn-sm btn-primary btn-resend-otp"><i class="fa" aria-hidden="true"></i> Resend OTP</button>
</form>
@endif
