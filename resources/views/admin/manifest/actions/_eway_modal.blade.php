<div id="eway_bill_modal-{{$shipment->id}}" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text-center">Enter E-way Bill Number</h5>
        <form class="form" method="post" action="{{route('admin::invoice.assign.eway_bill')}}" autocomplete="off">
            <input type="hidden" name="shipment_id" value="{{$shipment->id}}">
            {{csrf_field()}}
            <div class="margin-t-30">
              <label>E-Way Bill Number</label>
                <input class="form-control margin-t-5" name="eway_number" @if(!empty($shipment->invoice->eway_bill_number)) value="{{$shipment->invoice->eway_bill_number}}" @endif>
            </div>
            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Save</button>
            </div>
        </form>
    </div>
</div>