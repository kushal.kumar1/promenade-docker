@if($shipment->status == 'pending_retry')
<form class="form" method="POST" action="{{route('admin::shipments.retry')}}">
  <input type="hidden" name="id" value="{{$shipment->id}}">
  <input type="hidden" name="type" value="reject">
  <button type="button" class="btn btn-primary btn-sm btn-reject-retry"><i class="fa fa-refresh" aria-hidden="true"></i> Reject Retry</button>
</form>
@endif
@if($shipment->status != 'pending_rto')
<form class="form" method="POST" action="{{route('admin::shipments.retry')}}">
  <input type="hidden" name="id" value="{{$shipment->id}}">
  <input type="hidden" name="type" value="approve">
  <button type="button" class="btn btn-primary btn-sm btn-retry"><i class="fa fa-refresh" aria-hidden="true"></i> @if($shipment->status == 'pending_retry') Approve @endif Retry</button>
</form>
@endif
