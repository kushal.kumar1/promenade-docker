<form class="form" method="POST" action="{{route('admin::manifests.remove-shipment')}}">
  <input type="hidden" name="id" value="{{$manifest->id}}">
  <input type="hidden" name="shipment_id" value="{{$shipment->id}}">
  <button type="button" class="btn btn-sm btn-danger btn-remove-shipment">Remove Shipment</button>
</form>
