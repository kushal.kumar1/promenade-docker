<div id="otp-modal" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text-center">Enter OTP</h5>
        <form class="form" method="post" action="{{route('admin::manifests.deliver-shipments')}}" autocomplete="off">
            <input type="hidden" name="id" value="{{$manifest->id}}">
            {{csrf_field()}}
            <div class="margin-t-30">
                <div class="flex gutter" id="otp-fields">

                </div>
                <p class="small margin-t-5 danger">Leave blank for Stock Transfer Order Shipment</p>
            </div>

            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Deliver</button>
            </div>
        </form>
    </div>
</div>