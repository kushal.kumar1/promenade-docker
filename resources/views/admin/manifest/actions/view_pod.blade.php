@if(!empty($shipment->pod_name) && $shipment->status == 'dispatched')
<a href="{{\Niyotail\Helpers\Image::getSrc($shipment)}}" class="btn btn-sm btn-success" target="_blank">
    <i class="fa" aria-hidden="true"></i> View POD
</a>
@endif
