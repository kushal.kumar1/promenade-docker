@extends('admin.master')

@section('content')
<div class="container">
  <div class="regular light-grey">
    <a href="{{route('admin::manifests.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Dispatch Manifests</a>
  </div>
  <h1 class="h1 margin-t-10">New Dispatch Manifest</h1>
    @include('admin.grid')
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('input[name=select_all]').on('click', function() {
    if ($(this).is(":checked")) {
      $('.item-checkbox').prop('checked', true);
    } else if ($(this).is(":not(:checked)")) {
      $('.item-checkbox').prop('checked', false);
    }
  })
</script>
@endsection
