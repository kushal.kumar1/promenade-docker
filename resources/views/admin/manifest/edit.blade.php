@extends('admin.master')
@section('css')
    <style>
        .input-loader {
            position: absolute;
            top: 23px;
            right: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-beat">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::manifests.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Dispatch
                Manifests</a>
        </div>
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    <h1 class="h1 margin-r-20 bold">#{{$manifest->id}}</h1>
                    <div class="margin-t-5 small grey-light">
                        Generated: {{(new DateTime($manifest->created_at))->format('d M  Y, h:i A')}}
                        by {{$manifest->createdBy->name}}
                    </div>
                </div>
                <div class="flex">
                    @if($manifest->canDispatch())
                        <form class="form margin-r-10" method="post" action="{{route('admin::manifests.dispatch')}}"
                              data-destination="{{route('admin::manifests.edit',['id' => $manifest->id])}}">
                            <input type="hidden" name="id" value="{{$manifest->id}}">
                            <button type="button" class="btn btn-success btn-dispatch">Dispatch</button>
                        </form>
                    @endif
                    @if($manifest->canCancel())
                        <form class="form" method="post" action="{{route('admin::manifests.cancel')}}">
                            <input type="hidden" name="id" value="{{$manifest->id}}">
                            <button type="button" class="btn btn-danger btn-cancel">
                                Cancel
                            </button>
                        </form>
                    @endif
                    @if($manifest->status == "dispatched" && $manifest->shipments->whereIn('status', "dispatched")->count())
                        <div class="margin-r-10">
                            <button type="button" class="btn btn-primary mark-deliver" data-modal="#otp-modal"
                                    disabled="disabled">Mark as Delivered
                            </button>
                        </div>
                    @endif
                    @if($manifest->canClose() && $manifest->status != "closed")
                        <form class="form" method="post" action="{{route('admin::manifests.close')}}">
                            <input type="hidden" name="id" value="{{$manifest->id}}">
                            <button type="button" class="btn btn-success btn-close">Close Manifest</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <div class="margin-t-20">
            <div class="flex gutter no-wrap">
                <div class="col-9-12">
                    <div class="card">
                        <div class="flex gutter bold">
                            <div class="col-1-12">
                                @if($manifest->status == "dispatched")
                                    <label class="checkbox checkbox-item">
                                        <input type="checkbox" class="item-checkbox" name="select_all"
                                               autocomplete="off">
                                        <span class="indicator"></span>
                                    </label>
                                @endif
                            </div>
                            <div class="col-3-12">
                                Shipment / Order
                            </div>
                            <div class="col-3-12">
                                Store / Beat
                            </div>
                            <div class="col-1-12">
                                Boxes
                            </div>
                            <div class="col-2-12">
                                Invoice / Qty
                            </div>
                            <div class="col-2-12">
                                Actions
                            </div>
                        </div>
                        <hr class="hr hr-light margin-t-20">
                        @foreach($manifest->shipments as $shipment)
                            <div class="margin-t-20 shipment" data-shipment_id="{{$shipment->id}}">
                                <div class="flex gutter">
                                    <div class="col-1-12">
                                        @if($shipment->status == "dispatched")
                                            <label class="checkbox checkbox-item">
                                                <input type="checkbox" class="item-checkbox" name="shipment_id"
                                                       value="{{$shipment->id}}" autocomplete="off">
                                                <span class="indicator"></span>
                                            </label>
                                        @endif
                                    </div>
                                    <div class="col-3-12">
                                        <div class="grey-light">
                                            Shipment Id: #{{$shipment->id}}
                                        </div>
                                        <h3 class="margin-t-5">Order: <a
                                                    href="{{route('admin::orders.edit', $shipment->order->id)}}"
                                                    class="link" target="_blank">{{$shipment->order->reference_id}}</a>
                                        </h3>
                                        <div class="margin-t-5">
                                            <span class="tag tag-{{$shipment->status}}">{{$shipment->status}}</span>
                                        </div>
                                    </div>
                                    <div class="col-3-12">
                                        <div>
                                            <a href="{{route('admin::stores.profile', $shipment->order->store_id)}}"
                                               target="_blank" class="link">{{$shipment->order->store->name}}
                                                / {{$shipment->order->store->beat->name}}</a>
                                        </div>
                                    </div>
                                    <div class="col-1-12 relative">
                                        @if($manifest->canDispatch())
                                            <input type="number" value="{{$shipment->boxes}}" class="form-control"
                                                   name="boxes" autocomplete="off" min="0"/>
                                            <div class="hidden input-loader">
                                                <div class="icon-loading">
                                                    <i class="fa fa-spinner fa-spin"></i>
                                                </div>
                                            </div>
                                        @else
                                            {{$shipment->boxes}}
                                        @endif
                                    </div>
                                    <div class="col-2-12">
                                        <div>
                                            <a href="{{route('admin::invoices.index', $shipment->invoice->id)}}"
                                               target="_blank" class="link">{{$shipment->invoice->reference_id}}</a>
                                        </div>
                                        {{-- <div class="margin-t-5">
                                          {{$shipment->order_items_by_sku->count()}} Items / {{$shipment->items_count}} qty
                                        </div> --}}
                                    </div>
                                    <div class="col-2-12 flex gutter-xsm">
                                        @php
                                            $actions = $manifest->getAvailableActions($shipment)
                                        @endphp
                                        @if(!empty($actions))
                                            @foreach($actions as $action)
                                                @include("admin.manifest.actions.$action")
                                            @endforeach
                                        @else
                                            N/A
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @include('admin.manifest.actions._eway_modal')
                        @endforeach
                    </div>
                </div>
                <div class="col-3-12">
                    <div class="card">
                        <div class="flex gutter-between">
                            <div class="h6 bold">
                                Summary
                            </div>
                            @if($manifest->status != "generated")
                                <div>
                                    <a href="{{route("admin::manifests.pdf", $manifest->id)}}" target="_blank">
                                        <button class="btn btn-sm"><i class="fa fa-print" aria-hidden="true"></i> Print
                                        </button>
                                    </a>
                                </div>
                            @endif
                        </div>
                        <div class="margin-t-30">
                            <div class="flex gutter-sm">
                                <div class="col-6-12">
                                    All Invoices
                                </div>
                                <div class="col-6-12 text-right">
                                    <a href="{{route("admin::manifests.invoices.pdf", $manifest->id)}}" target="_blank">
                                        <button class="btn btn-sm btn-success"><i class="fa fa-print"
                                                                                  aria-hidden="true"></i> Print
                                        </button>
                                    </a>
                                </div>
                                <div class="col-6-12">
                                    Status
                                </div>
                                <div class="col-6-12 text-right">
                                    <span class="tag tag-{{$manifest->status}}">{{$manifest->status}}</span>
                                </div>
                                <div class="col-6-12">
                                    Shipments
                                </div>
                                <div class="col-6-12 text-right">
                                    {{$manifest->shipments->count()}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-t-20">
                        <div class="card">
                            <h2 class="h6 bold">Additional Details</h2>
                            @if($manifest->status == "generated")
                                <form class="form" method="post" action="{{route('admin::manifests.update-details')}}"
                                      autocomplete="off">
                                    <input type="hidden" name="id" value="{{$manifest->id}}"/>
                                    <div class="margin-t-10">
                                        <label class="label">Warehouse Locations</label>
                                        <select class="form-control beat-locations-search" type="text"
                                                name="locations[]" id="locations" multiple="multiple">
                                            @if($manifest->locations->isNotEmpty())
                                                @foreach($manifest->locations as $location)
                                                    <option value="{{$location->id}}" selected="selected">
                                                        {{$location->reference}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="margin-t-20">
                                        <label class="label">Vehicle Number</label>
                                        <input class="form-control" type="text" name="vehicle_number"
                                               value="{{$manifest->vehicle_number}}"/>
                                    </div>
                                    <div class="margin-t-10">
                                        <label class="label">Delivery Agent</label>
                                        <select class="form-control" type="text" name="agent_id" id="agents-search">
                                            @if($manifest->agent_id)
                                                <option value="{{$manifest->agent_id}}">
                                                    {{$manifest->agent->name}}
                                                </option>
                                            @endif
                                        </select>
                                    </div>
                                    <button class="btn btn-sm btn-primary margin-t-10">Update</button>
                                </form>
                            @else
                                <div class="margin-t-20">
                                    <div class="flex gutter-between">
                                        <div>
                                            Warehouse Locations
                                        </div>
                                        <div>
                                            @if($manifest->locations->isNotEmpty())
                                                {{implode(', ', $manifest->locations->pluck('reference')->toArray())}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-t-20">
                                    <div class="flex gutter-between">
                                        <div>
                                            Vehicle
                                        </div>
                                        <div>
                                            {{!empty($manifest->vehicle_number) ? $manifest->vehicle_number : "NA"}}
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-t-10">
                                    <div class="flex gutter-between">
                                        <div>
                                            Delivery Agent
                                        </div>
                                        <div>
                                            {{!empty($manifest->agent) ? $manifest->agent->name :"NA"}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.manifest.actions.otp-shipment')
@endsection
@section('js')
    <script type="text/javascript" src="{{mix('admin/js/manifest.js')}}"></script>
    <script>
        $('.btn-retry').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Retry Shipment', 'Are you sure you want to move this shipment to RETRY status?', function () {
                $form.submit();
            });
        });

        $('.btn-rto').on('click', function () {
            $form = $(this).closest('.form');
            dialog('RTO Shipment', 'Are you sure you want to move this shipment to RTO status?', function () {
                $form.submit();
            });
        });

        $('.btn-reject-retry').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Retry Shipment', 'This was previously marked pending. Are you sure you want to move this shipment back to DISPATCHED status?', function () {
                $form.submit();
            });
        });

        $('.btn-reject-rto').on('click', function () {
            $form = $(this).closest('.form');
            dialog('RTO Shipment', 'This was previously marked pending. Are you sure you want to move this shipment back to DISPATCHED status?', function () {
                $form.submit();
            });
        });

        $('.btn-dispatch').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Dispatch Manifest', 'Are you sure you want to DISPATCH this manifest?', function () {
                $form.submit();
            });
        });

        $('.btn-close').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Close Manifest', 'Are you sure you want to CLOSE this manifest?', function () {
                $form.submit();
            });
        });

        $('.btn-cancel').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Cancel Manifest', 'Are you sure you want to CANCEL this manifest?', function () {
                $form.submit();
            });
        });

        $('.btn-resend-otp').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Cancel Manifest', 'Are you sure you want to RESEND OTP for this shipment?', function () {
                $form.submit();
            });
        });

        $('.btn-remove-shipment').on('click', function () {
            $form = $(this).closest('.form');
            dialog('Remove Shipment', 'Are you sure you want to remove this shipment from the manifest?', function () {
                $form.submit();
            });
        });
        $('input[name=shipment_id]').on('change', function () {
            if (this.checked) {
                $('.mark-deliver').attr('disabled', false);
            } else {
                if ($('input[name=shipment_id]:checked').length == 0) {
                    $('.mark-deliver').attr('disabled', true);
                }
            }
        });
        $('#otp-modal').on('modalOpened', function () {
            let shipments = [];
            $('input[name=shipment_id]:checked').each(function(){
                shipments.push($(this).val());
            })
            for (let i = 1; i <= shipments.length; i++) {
                let otpField = `<div class="col-12-12"><label class="label">OTP For ${shipments[i-1]}</label><input class="form-control" name="otps[${shipments[i-1]}]" type="number"/></div>`;
                $(this).find('#otp-fields').append(otpField);
            }
        });
        $('#otp-modal').on('modalClosed', function (){
            $(this).find('#otp-fields').children().remove();
        });

    </script>
@endsection
