@extends('admin.master')

@section('content')

<div class="container">
  <div class="flex gutter-between">
    <div class="h1">
      Dispatch Manifests
    </div>
    <div>
      <a href="{{route('admin::manifests.create')}}">
        <button class="btn btn-primary">Create</button>
      </a>
    </div>
  </div>
  @include('admin.grid')
</div>

@endsection
