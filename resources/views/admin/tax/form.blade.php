@extends('admin.master')
@section('css')
    <style>
        select.form-control {
            height: 34px;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::tax-class.edit', ['id'=>$tax->tax_class_id])}}"><i class="fa fa-angle-left"
                                                                                        aria-hidden="true"></i> {{$tax->taxClass->name}}
            </a>
        </div>
        <div class="h1 margin-t-10">
            Edit Tax
        </div>
    </div>
    <div class="container flex main-center">
        <div class="col-8-12 bg-white padding-25">
            <form class="form" method="post"
                  action="{{!empty($tax)?route('admin::taxes.update'):route('admin::taxes.store')}}">
                {{csrf_field()}}
                @if(!empty($tax))
                    <input type="hidden" name="id" value="{{$tax->id}}">
                    <input type="hidden" name="tax_class_id" value="{{$tax->tax_class_id}}">
                @endif
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">NAME</label>
                        <input class="form-control" name="name" value="{{!empty($tax)?$tax->name:''}}"/>
                        <span class="error error-name"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">PERCENTAGE</label>
                        <input class="form-control" name="percentage" value="{{!empty($tax)?$tax->percentage:''}}"
                               type="number" step="any"/>
                        <span class="error error-percentage"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Source State</label>
                        <select class="form-control" name="source_state_id">
                            <option value="">Select</option>
                            @foreach($states as $state)
                                <option value="{{$state->id}}" @if($tax->source_state_id == $state->id) selected @endif>{{$state->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-source_state_id"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Supply Country</label>
                        <select class="form-control" name="supply_country_id">
                            <option value="">Select</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}" @if($tax->supply_country_id == $country->id) selected @endif>{{$country->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-supply_country_id"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Supply State</label>
                        <select class="form-control" name="supply_state_id">
                            <option value="">Select</option>
                            @foreach($states as $state)
                                <option value="{{$state->id}}" @if($tax->supply_state_id == $state->id) selected @endif>{{$state->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-supply_state_id"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Minimum Price</label>
                        <input class="form-control" name="min_price" value="{{!empty($tax)?$tax->min_price:''}}"
                               type="number" step="any"/>
                        <span class="error error-min_price"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Maximum Price</label>
                        <input class="form-control" name="max_price" value="{{!empty($tax)?$tax->max_price:''}}"
                               type="number" step="any"/>
                        <span class="error error-max_price"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">START DATE</label>
                        <input class="form-control" name="start_date"
                               value="{{!empty($tax)?(\Carbon\Carbon::parse($tax->start_date)->toDateString()):''}}"
                               type="date">
                        <span class="error error-start_date"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">END DATE</label>
                        <input class="form-control" name="end_date"
                               value="{{!empty($tax->end_date)?(\Carbon\Carbon::parse($tax->end_date)->toDateString()):''}}"
                               type="date">
                        <span class="error error-end_date"></span>
                    </div>
                </div>
                <hr class="hr-light margin-t-25"/>
                <div class="margin-t-25 text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>

@endsection
