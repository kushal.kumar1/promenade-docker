@extends('admin.master')

@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="h1">
            Brands
        </div>
        <div class="margin-t-30">
            <div class="flex gutter">
                <div class="col-4-12">
                    <div class="card">
                        @include('admin.brand._tree', ['brands' => $brandTree])
                    </div>
                </div>
                <div class="col-8-12">
                    <div id="container-brand" data-action-create="{{route('admin::brands.store')}}" data-action-update="{{route('admin::brands.update')}}">
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('admin.templates.brand')

@endsection

@section('js')
    <script type="text/javascript" src="{{mix('admin/js/brand.js')}}"></script>
@endsection
