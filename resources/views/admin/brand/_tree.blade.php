<?php
function printList($elements,array $selected=[])
{
    foreach ($elements as $element){
        if(in_array($element['id'],$selected)){
            echo "<li id='".$element['id']."' class='jstree-clicked'>";
        }else{
            echo "<li id='".$element['id']."'>";
        }
        echo $element['name'];
        if(!empty($element['children']))
        {
            echo "<ul>";
            printList($element['children'],$selected);
            echo "</ul>";
        }
        echo "</li>";
    }
}
if(empty($selectedBrand))
    $selectedBrand=[];
?>
<div id="tree-brands">
    <ul>
        {{printList($brands,$selectedBrand)}}
    </ul>
</div>
