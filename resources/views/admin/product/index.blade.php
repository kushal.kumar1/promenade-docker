@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <h1 class="h1">Products</h1>
            <div>
                <a class="btn btn-primary" href="{{route('admin::products.create')}}">Add Product</a>
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection
