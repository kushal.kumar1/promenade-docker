<div class="card">
    <table class="table-view">
        <tbody>
        @if(!empty($result))
            @foreach($result as $key=>$count)
                <tr>
                    <td><span class="bold">{{strtoupper($key)}}</span> : <span @if($count < 10) class="bold danger"
                                                                               @else class="bold primary" @endif>{{$count}}</span>
                    </td>
                </tr>
            @endforeach
        @else
            "Select Product to get count!"
        @endif
        </tbody>
    </table>
</div>