@extends('admin.master')

@section('content')
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::products.inventory', ['id'=>$inventory->product_id])}}"><i class="fa fa-angle-left" aria-hidden="true"></i>
                {{$inventory->batch_number}}</a>
        </div>
        <div class="flex gutter-between margin-t-5">
            <h1 class="h2">Inventory Detail</h1>
        </div>
        @include('admin.grid')
    </div>
@endsection