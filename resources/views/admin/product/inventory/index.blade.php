@extends('admin.master')

@section('content')

    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::products.edit', ['id'=>$product->id])}}"><i class="fa fa-angle-left" aria-hidden="true"></i>
                {{$product->name}}</a>
        </div>

        <div class="flex gutter-between margin-t-5">
            <h2 class="h2">Inventories</h2>
        </div>
        @include('admin.grid')

        <div id="modal-inventory-status" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 text-center">Update Status</h5>
                <form class="form" method="post" action="{{route('admin::inventory.status.update')}}"
                      autocomplete="off">
                    <input type="hidden" name="id" id="inventory_id">
                    <div class="margin-t-30">
                        <div class="flex gutter">
                            <div class="col-12-12">
                                <label class="label">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option value="0">Inactive</option>
                                    <option value="1">Active</option>
                                </select>
                            </div>
                            <div class="col-12-12 hidden" id="inactive_reason_div">
                                <label class="label">Inactive Reason</label>
                                <select class="form-control" name="inactive_reason" id="inactive_reason">
                                    @foreach($inactiveReasons as $reason)
                                        <option value="{{$reason}}">{{$reason}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        let modal = $('#modal-inventory-status');
        $(document).on('click', '.btn-status', function (e) {
            let inventoryId = $(this).data('id');
            let status = $(this).data('status');
            let inactiveReason = $(this).data('inactive_reason');
            modal.find(`#status option[value="${status}"]`).attr("selected", "selected");
            modal.find('#inventory_id').val(inventoryId);
            if (modal.find("#status").val() !== "1") {
                $('#inactive_reason_div').removeClass('hidden');
                modal.find(`#inactive_reason option[value="${inactiveReason}"]`).attr("selected", "selected");
                $('#inactive_reason').removeAttr("disabled");
            } else {
                $('#inactive_reason_div').addClass('hidden');
                $('#inactive_reason').attr("disabled", "disabled");
            }
            Modal.show(modal);
        });

        $('#status').on('change', function (e) {
            if ($(this).val() === "1") {
                $('#inactive_reason_div').addClass('hidden');
                $('#inactive_reason').attr("disabled", "disabled");
            } else {
                $('#inactive_reason_div').removeClass('hidden');
                $('#inactive_reason').removeAttr("disabled");
            }
        });
    </script>
@endsection
