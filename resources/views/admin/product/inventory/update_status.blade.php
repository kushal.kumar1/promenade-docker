@extends('admin.master')

@section('content')
    <div class="container">
        <div class="h3">Update Inventory Status</div>
        <div class="margin-t-5">
            <form class="form" method="POST"
                  action="{{route('admin::inventory.status.update.command')}}">
                <div class="flex gutter">
                    <div class="col-8-12 margin-t-10">
                        <div class="card" id="card-basic">
                            <div class="margin-t-10">
                                <div class="flex gutter-sm">
                                    <div class="col-4-12">
                                        <label class="label">Warehouse</label>
                                        <select class="form-control" name="warehouse_id" required id="choose_warehouse">
                                            <option value="" disabled selected>Select Warehouse</option>
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{$warehouse->id}}"
                                                        @if(session()->get('selected-warehouse') == $warehouse->id) selected @endif>{{$warehouse->full_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">Product</label>
                                            <select class="inventory-product-search" name="product_id" required>
                                                <option value="" disabled selected>Choose the Product</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">Current Status</label>
                                            <select class="form-control" name="old_status" required>
                                                <option value="" disabled selected>Choose the current status</option>
                                                @foreach($flatInventoryStatus as $status)
                                                    <option value="{{$status}}">{{$status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">New Status</label>
                                            <select class="form-control" name="new_status" required>
                                                <option value="" disabled selected>Choose the new status</option>
                                                @foreach($flatInventoryStatus as $status)
                                                    <option value="{{$status}}">{{$status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">Quantity</label>
                                            <input type="number" class="form-control" name="quantity" required>

                                        </div>
                                    </div>
                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">Manufacturing Date</label>
                                            <input type="date" class="form-control" name="mfd">
                                        </div>
                                    </div>

                                    <div class="col-4-12">
                                        <div class="margin-t-5">
                                            <label class="label">Storage(Label)</label>
                                            <select class="warehouse-storage-search" name="storage_id">
                                                <option value="" selected>Choose the Storage</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4-12" id="results">
                        @include('admin.product.inventory.status_wise_count')
                    </div>
                </div>
                <button type="submit" class=" btn btn-success margin-t-25">
                    Update
                </button>

            </form>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('.inventory-product-search').select2({
            minimumInputLength: '1',
            ajax: {
                url: '/inventory/products',
                dataType: 'json',
                processResults: (data) => {
                    data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = `(PID: ${obj.id}) ${obj.name}`
                        return obj;
                    });

                    return {
                        results: data
                    };
                }
            }
        }).on('select2:select', function (e) {
            let selectedPID = $(this).val();
            let warehouseId = $('#choose_warehouse').val();
            renderInventoryCount(selectedPID, warehouseId);
        })

        let renderInventoryCount = (pid, warehouseId) => {
            $.ajax({
                url: '/inventory/status-count',
                type: "GET",
                data: {product_id: pid, warehouse_id: warehouseId},
                success: function (data) {
                    $('#results').html(data);
                }
            })
        }

        $('.warehouse-storage-search').select2({
            minimumInputLength: '1',
            ajax: {
                url: '/inventory/storages',
                dataType: 'json',
                processResults: (data) => {
                    data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = `${obj.label} (WAREHOUSEID: ${obj.warehouse_id})`
                        return obj;
                    });

                    return {
                        results: data
                    };
                }
            }
        })

    </script>
@endsection
