@extends('admin.master')
@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
    <link rel="stylesheet" href="/plugins/json/beautify-json.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
@endsection
@section('content')
    <div class="container">
        <form class="form form-product" method="post" action="{{route('admin::products.update')}}"
              data-actions=".header-actions" autocomplete="off">
            <div class="header-actions">
                <button type="button" class="btn btn-discard margin-r-10">Discard</button>
                <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
            </div>
            {{csrf_field()}}
            <div class="regular light-grey">
                <a href="{{route('admin::products.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i>
                    Products</a>
            </div>
            <div class="flex gutter-between">
                <div class="h3 margin-t-10 bold">
                    {{$product->name}}
                </div>
                <div>
                    @if(!empty($product->status) && empty($product->is1KMall()))
                        <button type="button" class="btn btn-secondary sync-price" data-id="{{$product->id}}" data-url="{{route('admin::products.sync.price')}}">Sync
                            Price
                        </button>
                    @endif
                    @if(!empty($product->status))
                        <button type="button" class="btn btn-secondary sync-product" data-id="{{$product->id}}" data-url="{{route('admin::products.sync')}}">Sync
                            Product
                        </button>
                    @endif
                    @if(!empty($product->barcode))
                        <button type="button" class="btn btn-secondary" data-modal="#modal-barcode">Print Labels
                        </button>
                    @endif
                </div>
            </div>

            @include('admin.product.form')
        </form>
        <form id="form-upload" class="form form-multipart" method="post"
              action="{{route('admin::products.images.store',['id' => $product->id])}}">
            {{csrf_field()}}
            <input id="upload-input" type="file" name="images[]" multiple class="hidden"/>
        </form>
    </div>
    @include('admin.templates.attribute')
    <div id="modal-barcode" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h6 title text-center">Generate Labels</h5>
            <div class="margin-t-20">
                <label class="label">Number of Labels</label>
                <input class="form-control qty js-barcode-qty" name="qty" type="number" step="2" min="2"></input>
                <span class="small margin-t-5">Kindly enter in multiples of 2</span>
            </div>
            <div class="margin-t-20 text-right">
                <button class="btn btn-sm btn-primary barcode-print" data-id="{{$product->id}}">Generate</button>
            </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{asset('plugins/Sortable.min.js')}}"></script>
    <script type="text/javascript" src="{{mix('admin/js/product.js')}}"></script>
    <script type="text/javascript" src="/plugins/json/jquery.beautify-json.js"></script>
    <script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
    <script type="text/javascript">
        var simplemde = new SimpleMDE({showIcons: ["table"]});
        simplemde.codemirror.on('change', function () {
            $('.description').val(simplemde.value());
        });
    </script>
@endsection
