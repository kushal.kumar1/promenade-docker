@extends('admin.master')
@section('css')
    <link rel="stylesheet" href="/plugins/jstree/themes/default/style.min.css">
@endsection
@section('content')
    <div class="container">
        <form class="form form-product" method="post" action="{{route('admin::products.store')}}" data-actions=".header-actions" data-destination="{{route("admin::products.edit", ['id' => "#id#"])}}">
            <div class="header-actions">
                <button type="button" class="btn btn-discard margin-r-10">Discard</button>
                <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
            </div>
            {{csrf_field()}}
            <div class="regular light-grey">
                <a href="{{route('admin::products.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Products</a>
            </div>
            <div class="h1 margin-t-10">
                Add Product
            </div>
            @include('admin.product.form')
        </form>
    </div>
    @include('admin.templates.attribute')
@endsection

@section('js')
    <script src="{{asset('plugins/Sortable.min.js')}}"></script>
    <script type="text/javascript" src="{{mix('admin/js/product.js')}}"></script>
@endsection
