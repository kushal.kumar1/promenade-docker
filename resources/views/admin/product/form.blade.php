@if (!empty($product))
  <input type="hidden" name="id" value="{{$product->id}}"/>
@endif
<div class="flex margin-t-10 gutter">
  <div class="col-12-12">
    <div class="card">
      <h2 class="h6 bold">Basic Information</h2>
      <div class="margin-t-25">
        <div class="flex gutter-sm">
          <div class="col-12-12">
            <label class="label">Name</label>
            <input class="form-control" name="name" value="{{isset($product)? $product->name : ""}}"/>
            <span class="error error-name"></span>
          </div>
        </div>
      </div>
      {{-- @if (!empty($product))
                <div class="margin-t-25">
                    <label class="label">Slug</label>
                    <input class="form-control" name="slug" value="{{$product->slug}}" disabled/>
      <span class="error error-slug"></span>
    </div>
    @endif --}}
      <div class="margin-t-25">
        <label class="label">Description</label>
        <textarea class="form-control description" name="description"
                  rows="7">{{!empty($product) ? $product->description : ""}}</textarea>
      </div>
      <div class="margin-t-25">
        <div class="flex gutter-sm">
          <div class="col-6-12">
            <label class="label">HSN / SAC CODE</label>
            <input type="text" name="hsn_sac_code" class="form-control"
                   value="{{isset($product) ? $product->hsn_sac_code : ""}}"/>
            <span class="error error-hsn_sac_code"></span>
          </div>
          <div class="col-6-12">
            <label class="label">Select Brand</label>
            <select class="brand-search" name="brand_id">
              @if(!empty($product) && !empty($product->brand))
                <option value="{{$product->brand_id}}">{{$product->brand->name}}</option>
              @endif
            </select>
          </div>
          <div class="col-6-12">
            <label class="label">Tax Class</label>
            <select name="tax_class_id" class="form-control">
              @foreach($taxClasses as $taxClass)
                @if(!empty($product)&& $product->tax_class_id == $taxClass->id)
                  <option value="{{$taxClass->id}}" selected>{{$taxClass->name}}</option>
                @else
                  <option value="{{$taxClass->id}}">{{$taxClass->name}}</option>
                @endif
              @endforeach
            </select>
          </div>
          <div class="col-6-12">
            <label class="label">Tags</label>
            <select id="tags-search" class="form-control custom-select2"
                    data-url="{{route('admin::search.tags')}}" data-placeholder="Search Tags" name="tags[]"
                    multiple>
              @if(!empty($product->tags))
                @foreach($product->tags as $tag)
                  <option value="{{$tag->id}}" selected>
                    {{$tag->name}}
                  </option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="col-6-12">
            <label class="label">STATUS</label>
            @if(empty($product))
              <input type="hidden" name="status" value="0">
            @endif
            <select class="form-control" name="status" style="width:100%" id="status">
                    @if(empty($product)) disabled @endif >
              <option value="1" {{!empty($product) && $product->status ? 'selected' : ""}}>
                Active
              </option>
              <option value="0" {{(!empty($product) && !$product->status) || empty($product) ? 'selected' : ""}}>
                Inactive
              </option>
            </select>
            <span class="error error-status"></span>
          </div>
          <div class="col-6-12">
            @if(empty($product->barcode) && !empty($product->id))
              <div>
                <button type="button" class="btn btn-primary generate-barcode"
                        data-url="{{route("admin::products.generate.barcode", $product->id)}}">Generate
                  Barcode
                </button>
              </div>
            @else
              <label class="label">Barcode</label>
              <input type="text" name="barcode" class="form-control"
                     value="{{isset($product) ? $product->barcode : ""}}"/>
            @endif

          </div>
          <div class="col-6-12">
            <label class="label">Marketer Code</label>
            <input type="text" name="code" class="form-control"
                   value="{{isset($product) ? $product->code : ""}}"/>
          </div>
          <div class="col-6-12">
            <label class="label">Allow Back Orders (JIT)</label>
            <select name="allow_back_orders" class="form-control">
              <option value="0" {{(isset($product) && $product->allow_back_orders == 0) ? 'selected': ''}}>No</option>
              <option value="1" {{(isset($product) && $product->allow_back_orders == 1) ? 'selected': ''}}>Yes</option>
            </select>
          </div>
          <div class="col-6-12">
            <label class="label">UOM</label>
            <select class="form-control" name="uom">
              @foreach($measureUnits as $uom)
                <option value="{{$uom}}" {{(isset($product) && $product->uom == $uom) ? "selected":"" }}>
                  {{$uom}}
                </option>
              @endforeach
            </select>
          </div>

          <div class="col-6-12">
            <label class="label">Placement</label>
            <select class="form-control custom-select2" data-url="{{route('admin::search.racks')}}"
                    data-placeholder="Select Racks" name="racks[]" multiple>
              @if(!empty($product->warehouseRacks))
                @foreach($product->warehouseRacks as $rack)
                  <option value="{{$rack->id}}" selected>
                    {{$rack->reference}}
                  </option>
                @endforeach
              @endif
            </select>
          </div>

        </div>
      </div>
    </div>

    <div class="card margin-t-20">
      <div class="flex gutter-between">
        <div>
          <h2 class="h6 bold">Attributes</h2>
          <p class="margin-t-10 small">
            Add attributes like color, material etc. for this product.
          </p>
        </div>
      </div>
      <hr class="hr hr-light margin-t-20"/>
      <div id="attributes">
        @if (!empty($product))
          @if($product->attributes->isNotEmpty())
            @foreach ($product->attributes as $key => $attribute)
              <div class="attribute margin-t-20">
                <div class="flex gutter">
                  <div class="col-3-12">
                    {{$attribute->name}}
                  </div>
                  <div class="col-7-12">
                    <input class="form-control" type="text"
                           name="attributes[{{$attribute->id}}][value]"
                           value="{{$attribute->pivot->value}}"/>
                  </div>
                  <div class="col-2-12 remove">
                    <button type="button" class="btn"><i class="fa fa-trash-o"
                                                         aria-hidden="true"></i></button>
                  </div>
                </div>
              </div>
            @endforeach
          @else
            <div class="margin-t-20 text-center">
              No attributes found for this product!
            </div>
          @endif
        @endif
      </div>
      <hr class="hr hr-light margin-t-20"/>
      <div class="margin-t-20">
        <div class="flex gutter">
          <div class="col-5-12">
            <select class="form-control attribute-select" style="width:100%"></select>
          </div>
          <div class="col-7-12">
            <button id="attribute-add" type="button" class="btn">Add Attribute</button>
          </div>
        </div>
      </div>
    </div>

    @if(!empty($product))
      <div class="card margin-t-20">
        <div class="flex gutter-between cross-center">
          <h2 class="h6 bold">Images</h2>
          <div>
            <a href="#" class="upload btn btn-sm" data-target="#upload-input">Upload Images</a>
          </div>
        </div>
        <hr class="hr hr-light margin-t-20"/>
        <div class="margin-t-20">
          @if($product->images->isNotEmpty())
            <div id="image-list" class="flex gutter"
                 data-destination="{{route('admin::products.images.update',$product->id)}}">
              @foreach($product->images as $image)
                <div class="relative" data-id="{{$image->id}}">
                  <img src="{{Image::getSrc($image,150)}}" class="bd"/>
                  <button type="button" class="product-image-delete btn btn-raw top-right"
                          data-product-id="{{$product->id}}" data-id="{{$image->id}}"><i
                            class="fa fa-trash" aria-hidden="true"></i></button>
                </div>
              @endforeach
            </div>
          @else
            <div class="text-center padding-v-20">
              No Images Found!
            </div>
          @endif
        </div>
      </div>
    @endif

    <div class="card margin-t-20">
      <div class="flex gutter-between cross-center">
        <h2 class="h6 bold">Product Group & Category</h2>
      </div>
      <hr class="hr hr-light margin-t-20"/>
      <div class="margin-t-20">
        <label class="label">Select group</label>
        <select name="group_id" id="groupIdSelect" class="form-control custom-select2"
                data-url="{{route("admin::search.groups")}}" data-placeholder="Select Groups">
          @if(!empty($product) && !empty($product->group))
            <option value="{{$product->group->id}}">{{$product->group->name}}</option>
          @endif
        </select>
      </div>
      <hr class="hr hr-light margin-t-20"/>
      <div class="margin-t-20">
        <label class="label">Select CL2</label>
        <select name="cl2_id" id="cl2Select" class="form-control custom-select2"
                data-url="{{route("admin::search.cl2")}}" data-placeholder="Select CL2">
          @if(!empty($product) && !empty($product->cl2))
            <option value="{{$product->cl2->id}}">{{$product->cl2->name}}</option>
          @endif
        </select>
      </div>
      <hr class="hr hr-light margin-t-20"/>
      <div class="margin-t-20">
        <label class="label">Select CL4</label>
        <select name="cl4_id" id="cl4Select" class="form-control custom-select2"
                data-url="{{route("admin::search.cl-id")}}" data-placeholder="Select CL4">
          @if(!empty($product) && !empty($product->newCatL4))
            <option value="{{$product->newCatL4->id}}">{{$product->newCatL4->name}}</option>
          @endif
        </select>
      </div>
    </div>


    @if (!empty($product))
      <div class="card margin-t-20">
        <div class="flex gutter-between cross-center">
          <h2 class="h6 bold">Variants</h2>
          <div>
            <a class="btn btn-sm"
               href="{{route('admin::products.variants.create', ['productId' => $product->id])}}">Add
              Variant</a>
          </div>
        </div>
        <hr class="hr hr-light margin-t-20"/>
        <div class="margin-t-20">
          @if($product->variants->isNotEmpty())
            <div class="flex gutter-sm">
              <div class="col-1-12 bold">
                Type
              </div>
              <div class="col-1-12 bold">
                MOQ
              </div>
              <div class="col-2-12 bold">
                SKU
              </div>
              <div class="col-2-12 bold">
                MRP
              </div>
              <div class="col-2-12 bold">
                Price
              </div>
              <div class="col-2-12 bold">
                Min & Max Price
              </div>
              <div class="col-2-12 bold">
                Action
              </div>
            </div>
            <div class="variants">
              @foreach($product->variants as $key => $variant)
                <div class="variant margin-t-20" data-id="{{$variant->id}}">
                  <div class="flex gutter-sm">
                    <div class="col-1-12">
                      {{$variant->value}} ({{$variant->quantity}} {{$variant->uom}})
                      @if($variant->trashed())
                        <span class="tag tag-warning">Deleted</span>
                      @endif
                    </div>
                    <div class="col-1-12">
                      {{$variant->moq}}
                    </div>
                    <div class="col-2-12">
                      {{$variant->sku}}
                    </div>
                    <div class="col-2-12">
                      {{$variant->pretty_mrp}}
                    </div>
                    <div class="col-2-12">
                      {{$variant->pretty_price}}
                    </div>
                    <div class="col-2-12">
                      {{$variant->pretty_min_price}} & {{$variant->pretty_max_price}}
                    </div>
                    <div class="col-2-12">
                      @if($variant->trashed())
                        <span class="btn btn-sm btn-success restore"><i class="fa fa-refresh"
                                                                        aria-hidden="true"></i> Restore</span>
                      @else
                        <a class="btn btn-sm margin-r-10"
                           href="{{route('admin::products.variants.edit', ['productId' => $product->id, 'variantId'=>$variant->id])}}">Edit</a>
                        <span class="btn btn-sm remove"><i class="fa fa-trash-o"
                                                           aria-hidden="true"></i></span>
                      @endif
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          @else
            <div class="text-center padding-v-20">
              No Variant Found!
            </div>
          @endif
        </div>
      </div>
    @endif

    {{-- Note: commented as not used right now --}}
    {{-- <div class="card margin-t-20">
              <h2 class="h6 bold">Search Engine Optimization</h2>
              <div class="margin-t-25">
                  <label class="label">Page Title</label>
                  <input class="form-control" name="meta_title" value="{{isset($product) ? $product->meta_title : ""}}"/>
  </div>
  <div class="margin-t-25">
    <label class="label">Meta Description</label>
    <textarea class="form-control" name="meta_description" rows="4">{{isset($product) ? $product->meta_description : ""}}</textarea>
  </div>
  </div> --}}
    @if (!empty($product) && $product->variants->isNotEmpty())
      <div class="card margin-t-20">
        <div class="flex gutter-between">
          <h2 class="h6 bold">Inventory for  {{$warehouse->name}}</h2>
        </div>
        <div class="margin-t-20">
            <hr class="hr hr-light margin-t-5">
            <div class="margin-t-10">
              <div class="flex gutter-between bold">
                <div class="col-2-12">
                  Current Stock (pcs)
                </div>
                <div class="col-2-12">
                  Inventory Assigned to orders
                </div>
              </div>
              <div class="margin-t-5">
                <div class="flex gutter-between">
                  <div class="col-2-12">
                    {{$product->current_inventory}}
                  </div>
                  <div class="col-2-12">
                    {{$product->blocked_inventory}} (pcs)
                  </div>
                </div>
              </div>
              {{-- No. of items (pcs) : {{$stock}}<br/><br/>
              <span class="small bold">Latest Unit Cost (Rs) :</span> <span class="regular"> {{$latestInventoryCost}} </span> | <span class="small bold">Avg Unit Cost (Rs) :</span> <span class="regular"> {{$avgInventoryCost}} </span><br/>
              <span class="small bold">Min Unit Cost (Rs) :</span> <span class="regular"> {{$minInventoryCost}} </span> | <span class="small bold">Max Unit Cost (Rs) :</span> <span class="regular"> {{$maxInventoryCost}} </span><br/> --}}
            </div>
        </div>
      </div>
    @endif
  </div>
</div>