<h2 class="h3 margin-t-40">Activity Logs</h2>
<div class="timeline">
    @if($model->logs && $model->logs->isNotEmpty())
        @foreach ($model->logs as $log)
        <div class="timeline-block">
            <div class="card">
                <div class="flex gutter-between">
                    @if($log->causer_id)
                        <h5 class=""><span class="bold">{{$log->causer->name}}</span> {{$log->message}} {{$log->subject_type}}</h5>
                        @endif
                        <div>
                            {{(new DateTime($log->created_at))->format('F d, Y \a\t h:i A')}}
                        </div>
                </div>
                <hr class="hr hr-light margin-t-20" />
                <div class="margin-t-20 properties relative">
                    {{json_encode($log->properties)}}
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="timeline-block">
            No logs found!
        </div>
        @endif
</div>
