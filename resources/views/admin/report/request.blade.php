@extends('admin.master')
@section('content')

    <div class="container">
        <div class="flex gutter-between">
        <div class="h1">
            Report
        </div>
        <div>
            <button class="btn btn-primary" data-modal="#modal-report-request">Request</button>
        </div>
        </div>
        <div>
        @include('admin.grid')
        </div>

    </div>
    <div id="modal-report-request" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h6 text-center">Create Report Request</h5>
            <form class="form" method="post" action="{{route("admin::reports.store")}}" autocomplete="off">
                <label class="label margin-t-20">From Date</label>
                <input type="date" name="from_date" class="form-control"/>
                <span class="error error-from_date"></span>
                <label class="label margin-t-20">To Date</label>
                <input type="date" name="to_date" class="form-control"/>
                <span class="error error-to_date"></span>
                <label class="label margin-t-20">Type</label>
                <select class="form-control" name="type">
                    @foreach($types as $type)
                        <option value="{{$type}}">{{$type}}</option>
                    @endforeach
                </select>
                <div class="flex gutter-between margin-t-30">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary button-success">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection