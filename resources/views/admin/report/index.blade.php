@extends('admin.master')

@section('content')

    <div class="container">
      <div class="flex gutter-between">
        <div class="h1">
            {{$name}}
        </div>
      </div>
      @include('admin.grid')
    </div>

@endsection

@section('js')
  <script type="text/javascript">

    $('.grid').on('click', '.regenerate-invoice', function(){
      event.preventDefault();
      $(this).prop('disabled',true);
      $(this).find('.fa').removeClass('hidden');
      let id = $(this).data('id');
      let request = $.ajax({
        method: 'post',
        url: '{{route('admin::invoices.regenerate')}}',
        data: {id: id}
      });

      request.done(function(response) {
        $('.regenerate-invoice').prop('disabled',false).find('.fa').addClass('hidden');
        var toast = new Toast("Invoice Generated");
        toast.show();
      });

      request.fail(function(response) {
        $('.regenerate-invoice').prop('disabled',false).find('.fa').addClass('hidden');
        var toast = new Toast("Some Error Occured");
        toast.show();
      });

      // $(this).prop('disabled',false);
    });
  </script>
@endsection
