@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div>
                <a href="{{route('admin::product_request.list')}}">
                    <i class="fa fa-angle-left" aria-hidden="true"></i> Back to product requests
                </a>
            </div>

            <div class="col-6-12 text-right">
                <a class="btn btn-primary inline-block" href="{{ route('admin::product_request.approve', $product->id) }}">Approve</a>
                <form class="form inline-block" method="POST"
                     action="{{route('admin::product_request.delete', $product->id)}}"
                     data-destination="{{route('admin::product_request.list')}}">
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        </div>

        <div class="card margin-t-30">
            @include('admin.purchase_invoice._product_request_form', [
                'isCart' => true,
                'data' => $product,
                'measureUnits' => $measureUnits
            ])
        </div>
    </div>
@endsection

@section ('js')
    <script>
        $(document).ready(function () {
            class ManagePI {
                constructor () {
                    $('#productRequestSection').show();
                    $("#productRequestForm").on('submit', (e) => this.submitProductRequest(e, $(e.currentTarget)));
                }

                submitProductRequest(e, $form)
                {
                    e.preventDefault();
                    clearTimeout(this.searchTimer);

                    let submitButton = $form.find('button');
                    submitButton.attr('disabled', true);

                    var formData = new FormData($form[0]);

                    this.searchTimer = setTimeout(() => {
                        if (this.searchRequest) {
                            this.searchRequest.abort();
                        }

                        this.searchRequest = $.ajax('{{route("admin::product_request.update")}}',{
                            method: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false, 
                            async: true, 
                            dataType: "json", 
                        });

                        this.searchRequest.done((response) => {
                            submitButton.attr('disabled', false);
                            const toast = new Toast("Product Request Updated!");
                            toast.show();
                            //location.reload();
                        });

                        this.searchRequest.fail((error) => {
                            submitButton.attr('disabled', false);
                            this.onError(error);
                        });
                    }, 100);
                }

                onError(error){
                    let message = error.responseJSON.message;

                    if (message == undefined) {
                        message = "An unexpected error has occurred";
                    }

                    const toast = new Toast(message);
                    toast.show();
                }
             }

            new ManagePI();
        })
    </script>
@endsection