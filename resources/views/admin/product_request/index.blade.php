@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Product Requests
            </div>
        </div>

        @if(Session::has('message'))
            <p class="alert margin-t-30 {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        @include('admin.grid')
    </div>

@endsection

