<div class="templates hidden">
    <div class="toast">
        <i class="toast-icon"></i>
        <div class="toast-content">
            <p class="toast-heading"></p>
            <p class="toast-message"></p>
        </div>
        <button class="toast-dismiss">&times;</button>
    </div>
</div>