<div class="templates hidden">
    <div class="template-container">
        <form class="form form-address" autocomplete="off" method="post" action="{# action #}" data-success="{# success #}">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{# id #}">
            <input type="hidden" name="customer_id" value="{# customer_id #}">
            <input type="hidden" name="lead_id" value="{# lead_id #}">
            <div class="h5 bold text-center">
                 {# heading #}
            </div>
            <div class="margin-t-30">
                <div class="flex gutter-sm">
                    <div class="col-6-12">
                        <label class="label">First Name</label>
                        <input class="form-control" type="text" name="first_name" value="{# first_name #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Last Name</label>
                        <input class="form-control" type="text" name="last_name" value="{# last_name #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Company</label>
                        <input class="form-control" type="text" name="company" value="{# company #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Mobile</label>
                        <input class="form-control mobile-country-picker" type="text" name="mobile" value="{# mobile #}"/>
                    </div>
                    <div class="col-12-12">
                        <label class="label">Address1</label>
                        <input class="form-control" type="text" name="address1" value="{# address1 #}"/>
                    </div>
                    <div class="col-12-12">
                        <label class="label">Address2</label>
                        <input class="form-control" type="text" name="address2" value="{# address2 #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">City</label>
                        <input class="form-control" type="text" name="city" value="{# city #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Pin Code</label>
                        <input class="form-control" type="text" name="pincode" value="{# pincode #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">State</label>
                        <select class="form-control" type="text" name="state" data-value="{# state #}">
                            @foreach($states as $state)
                                <option value="{{$state->name}}">
                                    {{$state->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Country</label>
                        <select class="form-control" type="text" name="country_id" data-value="{# country_id #}">
                            <option value="101">
                                India
                            </option>
                        </select>
                    </div>
                    <div class="col-12-12">
                        <input type="checkbox" name="default" value="1" data-value="{# default #}"/>
                        <label class="label">Set As Default Address</label>
                    </div>
                    <div class="col-6-12">
                        <button type="submit" class="btn btn-primary col-12-12">Save</button>
                    </div>
                    <div class="col-6-12">
                        <button type="button" data-close="modal" class="btn col-12-12">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
