<div class="templates hidden">
    <div class="attribute margin-t-20">
        <div class="flex gutter">
            <div class="col-5-12">
                {# name #}
            </div>
            <div class="col-6-12">
                <input class="form-control" type="text" name="attributes[{# id #}][value]" value="{# value #}" />
            </div>
            <div class="col-1-12 remove">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
            </div>
        </div>
    </div>
</div>
