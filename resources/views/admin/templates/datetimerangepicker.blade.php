<?php $cfg = $filter->getConfig(); ?>
<div class="datetimerangepicker">
    <input class="form-control" name="{{$filter->getInputName()}}" value="{{$filter->getValue()}}" readonly="readonly"/>
</div>

<script type="text/javascript">
    $('.datetimerangepicker>input[name="{{$filter->getInputName()}}"]').dateRangePicker({
        "autoClose": true,
        setValue: function (s) {
            if (!$(this).is(':disabled') && s != $(this).val()) {
                $(this).val(s);
            }
        },
    }).bind('datepicker-change', function (event, obj) {
        $(this).closest('form').submit();
    });
</script>
