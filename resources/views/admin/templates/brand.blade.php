<div class="templates hidden">
    <div class="brand">
        <form class="form form-multipart" method="post" action="{# action #}" >
            <div class="card">
                {{csrf_field()}}
                <input class="form-control" type="hidden" name="id" value="{# id #}"/>
                <input class="form-control" type="hidden" name="parent_id" value="{# parent_id #}"/>
                <input class="form-control" type="hidden" name="left_node_id" value="{# left_node_id #}"/>
                <div class="flex gutter-between cross-center uppercase">
                    <h2 class="h5">{# heading #}</h2>

                    <div>
                        <button type="submit" class="btn btn-primary">{# button_name #}</button>
                    </div>
                </div>
                <hr class="hr hr-light margin-t-20"/>
                <label class="label margin-t-20">NAME</label>
                <input class="form-control" name="name" value="{# name #}"/>
                <div class="margin-t-30">
                    <div class="flex gutter-sm">
                        <div class="col-6-12">
                            <label class="label">Visibility</label>
                            <select class="form-control" name="visible" data-value="{# is_visible #}">
                                <option value="1">Visible</option>
                                <option value="0">Invisible</option>
                            </select>
                        </div>
                        <div class="col-6-12">
                            <label class="label">Status</label>
                            <select class="form-control" name="status" data-value="{# status #}">
                                <option value="1">Active</option>
                                <option value="0">Inactive</option>
                            </select>
                        </div>
                        <div class="col-12-12">
                            <label class="label margin-t-20">Marketer</label>
                            <select name="marketer_id" class="form-control marketer"></select>
                        </div>
                        <div class="col-12-12">
                            <img src="{# logo #}" class="bd"/>
                            <div>
                                <label class="label">Logo Upload </label>
                                <input type="file" name="logo" class="form-control">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
