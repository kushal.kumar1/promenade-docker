<div class="templates hidden">
    <div class="condition margin-t-20">
        <div class="flex gutter">
            <div class="col-4-12 param">
                {# param_label #}
                <input type="hidden" name="conditions[{# id #}][param]" value="{# param_value #}" />
                <input type="hidden" name="conditions[{# id #}][param_label]" value="{# param_label #}" />
            </div>
            <div class="col-3-12 operator">
                {# operator_label #}
                <input type="hidden" name="conditions[{# id #}][operator]" value="{# operator_value #}" />
                <input type="hidden" name="conditions[{# id #}][operator_label]" value="{# operator_label #}" />
            </div>
            <div class="col-4-12 value">
                {# value_label #}
            </div>
            <div class="col-1-12">
                <i class="fa fa-trash" aria-hidden="true"></i>
            </div>
        </div>
    </div>

</div>
