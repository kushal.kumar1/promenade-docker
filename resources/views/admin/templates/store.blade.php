<div class="templates hidden">
    <div class="template-container">
        <form class="form form-store" autocomplete="off" method="post" action="{# action #}" data-success="{# success #}">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{# id #}">
            <input type="hidden" name="customer_id" value="{# customer_id #}">
            <div class="h5 bold text-center">
                 {# heading #}
            </div>
            <div class="margin-t-30">
                <div class="flex gutter-sm">
                    <div class="col-12-12">
                        <label class="label">Legal Name</label>
                        <input class="form-control" type="text" name="legal_name" value="{# legal_name #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">GST</label>
                        <input class="form-control" type="text" name="gstin" value="{# gstin #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">PAN</label>
                        <input class="form-control" type="text" name="pan" value="{# pan #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">latitude</label>
                        <input class="form-control" type="text" name="lat" value="{# lat #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">longitude</label>
                        <input class="form-control" type="text" name="lng" value="{# lng #}"/>
                    </div>
                    <div class="col-12-12">
                        <label class="label">address</label>
                        <textarea class="form-control" name="address" rows="3">{# address #}</textarea>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Pin Code</label>
                        <input class="form-control" type="text" name="pincode" value="{# pincode #}"/>
                    </div>
                    <div class="col-6-12">
                        <label class="label">City</label>
                        <select class="form-control" type="text" name="city_id" data-value="{# city_id #}">
                            @foreach($cities as $city)
                                <option value="{{$city->id}}">
                                    {{$city->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-12-12">
                        <label class="label">Status</label>
                        <select class="form-control" type="text" name="status" data-value="{# status #}">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="col-6-12">
                        <button type="submit" class="btn btn-primary col-12-12">Save</button>
                    </div>
                    <div class="col-6-12">
                        <button type="button" data-close="modal" class="btn col-12-12">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
