@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <h1 class="h1">Importer Actions</h1>
            <div>
                <button class="btn btn-primary" data-modal="#modal-importer-add">Add Action</button>
            </div>
        </div>
        @include('admin.grid')

        <div id="modal-importer-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 text-center">Add Importer Action</h5>
                <label class="label margin-t-30">Samples</label>
                <div>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-products.csv")}}">Products</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-variants.csv")}}">Variants</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-purchase-invoice-cart.csv")}}">Vendor Cart</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cart.csv")}}">Cart</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-vendor_marketer_mapping.csv")}}">Vendor Marketer Mapping</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-product-group.csv")}}">Product Group</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-marketer.csv")}}">Marketer</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-brand.csv")}}">Brand</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cl1.csv")}}">Cl1</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cl2.csv")}}">Cl2</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cl3.csv")}}">Cl3</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cl4.csv")}}">Cl4</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-cl4-cost-map.csv")}}">Cl4 Cost Map</a>

                    <a class="tag margin-t-5" href="{{asset("samples/sample-wholesale-cart.csv")}}">Wholesale Cart Importer</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-stock-transfer-outbound.csv")}}">Stock Transfer Outbound</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-stock-transfer-sent-quantity.csv")}}">Stock Transfer Sent Importer</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-stock-transfer-inbound.csv")}}">Stock Transfer Inbound</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-inventory-migration.csv")}}">Inventory Migration</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-store-credit-limit.csv")}}">Store Credit Limit</a>
                    <a class="tag margin-t-5" href="{{asset("samples/sample-warehouse-storage.csv")}}">Warehouse Storage</a>

                    <a class="tag margin-t-5" href="{{asset("samples/sample-product-demand.csv")}}">Product Demand</a>

                </div>
                <form class="form form-multipart" method="post" action="{{route("admin::importers.store")}}" autocomplete="off">
                    @csrf
                    <label class="label margin-t-20">Type</label>
                    <select class="form-control" name="type">
                        @foreach(\Niyotail\Models\ImporterAction::getConstants('TYPE') as $type)
                            <option value="{{$type}}">{{ucwords(str_replace('-', ' ', $type))}}</option>
                        @endforeach
                    </select>
                    <label class="margin-t-20 label">Choose File</label>
                    <input type="file" class="form-control" name="file" />
                    <div class="flex gutter-between margin-t-30">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
