@forelse ($stores as $store)
<hr class="hr hr-light margin-t-25" />
<div class="margin-t-25">
    <div class="flex gutter-sm gutter-between no-wrap">
        <div>
            <h1 class="bold">{{$store->name}}</h1>
            <h2 class="margin-t-5">{{$store->address}}</h2>
            @if (!empty($store->landmark))
            <div class="margin-t-5"><span class="small uppercase">LANDMARK: </span> {{$store->landmark}}</div>
            @endif
            @if (!empty($store->distance))
            <div class="margin-t-5">
                <span class="small uppercase">DISTANCE: </span>
                {{round($store->distance, 2)}} KM
            </div>
            @endif
            @if (!empty($store->contact_mobile))
            <div class="margin-t-5">
                <span class="small uppercase">{{empty($store->contact_person) ? 'Phone' : $store->contact_person}}:
                </span>
                {{$store->contact_mobile}}
            </div>
            @endif
        </div>
        <div class="flex no-wrap">
            @if (!empty($store->contact_mobile))
            <a class="btn btn-raw margin-r-5" target="_blank" href="tel:{{$store->contact_mobile}}">
                <i class="fa fa-lg fa-phone" aria-hidden="true"></i>
            </a>
            @endif
            @if (!empty($store->lat) && !empty($store->lng))
            <a class="btn btn-raw" target="_blank"
                href="https://www.google.com/maps/search/?api=1&query={{$store->lat}},{{$store->lng}}">
                <i class="fa fa-lg fa-map-o" aria-hidden="true"></i>
            </a>
            @endif
        </div>
    </div>
</div>
@empty
<hr class="hr hr-light margin-t-25" />
<p class="margin-t-25">Matching stores not found</p>
@endforelse