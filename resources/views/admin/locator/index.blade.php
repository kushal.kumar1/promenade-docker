<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <title>Niyotail - Store Locator</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet">
    <link rel="stylesheet" href="/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ mix('admin/css/app.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    @yield('css')
    <script type="text/javascript">
      $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    </script>
  </head>

  <body>
    <div class="modal-backdrop"></div>
    <div id="dialog" class="modal">
      <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title">Reset all settings</h5>
        <p class="margin-t-20 message">
          This will restore all system settings to factory defults. Are you sure that you want to proceed?
        </p>
        <div class="flex gutter-between margin-t-20">
          <button class="btn button-reject">Cancel</button>
          <button class="btn btn-primary button-success">Yes</button>
        </div>
      </div>
    </div>
    @include('admin.templates.toast')
    <div class="content">
      <div class="card">
        <label class="label">SEARCH STORES</label>
        <input id="search-input" type="text" class="form-control" />
        <div class="stores"></div>
      </div>
    </div>
    <script type="text/javascript" src="{{mix('admin/js/app.js')}}"></script>
    <script type="text/javascript" src="{{mix('admin/js/locator.js')}}"></script>

  </body>

</html>