@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <h1 class="h1">Purchase Orders</h1>
        </div>
        @include('admin.grid')
    </div>

@endsection
