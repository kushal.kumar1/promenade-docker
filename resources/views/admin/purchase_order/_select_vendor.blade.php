<div id="modal-vendors" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Choose Vendor</h1>
        <div class="margin-t-20">
            <form class="form" data-destination="{{route('admin::purchase-order.index')}}" method="post" action="{{route('admin::purchase-order.create')}}">
                @csrf
            <label class="label">Select A Vendor</label>
            <select id="select2-vendors" name="vendor_id" required class="form-control" style="width:100%;"></select>
            <button type="submit" id="button-create-order" class="btn btn-primary margin-t-20">Create purchase order</button>
            </form>
        </div>
    </div>
</div>

@section('js')
    <script>
        function vendorTemplate(vendor) {
            if (!vendor.id) {
                return vendor.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${vendor.name}</h5>`;
            if(vendor.addresses.length)
            {
                html += `<h6 class="margin-t-5">${vendor.addresses[0].address}</h6>`;
            }
            html += '</div>';

            return $(html);
        };

        function vendorSelection(vendor){
            return vendor.name;
        }
        $('#select2-vendors').select2({
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.vendors")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: vendorTemplate,
            templateSelection: vendorSelection
        });

        {{--$('#button-create-order').on('click', function(){--}}
        {{--    let cartLink = '{{route("admin::purchase-invoices.cart", ["id" => "#"])}}';--}}
        {{--    const vendors = $('#select2-vendors').select2('data');--}}
        {{--    if (vendors.length > 0) {--}}
        {{--        window.location = cartLink.replace("#", vendors[0].id);--}}
        {{--    }--}}
        {{--});--}}
    </script>
@append