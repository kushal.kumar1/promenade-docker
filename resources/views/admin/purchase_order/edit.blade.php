@extends('admin.master')

@section('content')
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::purchase-orders.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Purchase Orders</a>
        </div>
        <div>
            <h1 class="h1 margin-t-20 bold">Purchase Order</h1>
        </div>
        <div class="margin-t-25">
            <div class="flex gutter">
                <div class="col-12-12">
                    <div class="card">
                        <div class="flex gutter-between">
                            <div><h2 class="h5 bold ">Details</h2></div>
                            <div class="text-right">
                                <a target="_blank" class="btn btn-sm btn-primary" href="{{route('admin::purchase-orders.pdf',['id'=>$purchaseOrder->id])}}">Print</a>
                            </div>
                        </div>
                        <hr class="hr-dark margin-t-10">
                        <div class="margin-t-30 flex gutter-between">
                            <div class="bold">Vendor</div>
                            <div class="pull-right ">{{$purchaseOrder->vendor->name}}</div>
                        </div>
                        <div class="margin-t-10 flex gutter-between">
                            <div class="bold">No of products</div>
                            <div class="pull-right">{{$purchaseOrder->items->count()}}</div>
                        </div>
                        <div class="margin-t-10 flex gutter-between">
                            <div class="bold">Status</div>
                            <div class="pull-right tag tag-{{$purchaseOrder->status}}">{{$purchaseOrder->status}}</div>
                        </div>
                        <div class="margin-t-10 flex gutter-between">
                            <div class="bold">Created By</div>
                            <div class="pull-right">{{$purchaseOrder->createdBy->name}}</div>
                        </div>
                        <div class="margin-t-10 flex gutter-between">
                            <div class="bold">Created On</div>
                            <div class="pull-right">{{$purchaseOrder->pretty_created_at}}</div>
                        </div>
                        <div class="margin-t-10 flex gutter-between">
                            <div class="bold">Product Requisition</div>
                            <div class="pull-right "><a href="{{route('admin::product-demands.edit',['id'=>$purchaseOrder->product_demand_id])}}" class="link bold"> {{$purchaseOrder->product_demand_id}}</a></div>
                        </div>

                        <hr>

                        <div class="margin-t-10 flex gutter-between">
                            <a href="{{route("admin::purchase-invoices.create", $purchaseOrder->id)}}" class="btn btn-success">
                                <i class="fa fa-plus-circle"></i>
                                Create Purchase Invoice</a>
                            @if($purchaseOrder->purchaseInvoices->count() >= 1)
                                <a href="{{route('admin::purchase-invoices.index', ['po_id' => $purchaseOrder->id])}}" class="btn btn-primary">View Purchase Invoice</a>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="col-12-12">
                   <div class="grid card">
                       {!!$grid!!}
                   </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
