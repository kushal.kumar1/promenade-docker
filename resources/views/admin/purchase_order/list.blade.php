@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Purchase Orders
            </div>

            <div>
                <!-- #TODO: Check permission if needed -->
                <button class="btn btn-primary" id="create-purchase-order" data-modal="#modal-vendors">Create Purchase Order</button>
            </div>
        </div>

        @include('admin.grid')
    </div>

    @include('admin.purchase_order._select_vendor')
@endsection