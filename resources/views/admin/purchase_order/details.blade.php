@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Purchased Orders
            </div>

            <div>

            </div>
        </div>

        <div class="margin-t-25">
            <div class="flex gutter">
                <div class="col-12-12">
                    <div class="card">
                        <div class="flex gutter-between cross-center">
                            <h2 class="h5 bold">Order Details</h2>
                        </div>

                        <hr class="hr hr-light margin-t-20" />

                        <div class="margin-t-20">
                            <table cellpadding="10">
                                <tr>
                                    <td>ID</td>
                                    <td>{{ $purchaseOrder->id }}</td>
                                </tr>
                                <tr>
                                    <td>Vendor</td>
                                    <td>{{ $purchaseOrder->vendor->name ?? '-' }}</td>
                                </tr>
                                @if($purchaseOrder->vendor)
                                    <tr>
                                        <td>Vendor Address</td>
                                        <td>
                                            @if($purchaseOrder->isEditable())
                                            <select class="vendor-address-id form-control">
                                                <option value="">--Select Address--</option>
                                                @foreach($purchaseOrder->vendor->addresses as $address)
                                                <option value="{{$address->id}}" @if($purchaseOrder->vendor_address_id == $address->id) selected @endif>{{$address->address}} - {{$address->gstin}}</option>
                                                @endforeach
                                            </select>
                                            @else
                                            {{$purchaseOrder->vendorAddress->address}} - {{$purchaseOrder->vendorAddress->gstin}}
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>Niyo Warehouse</td>
                                    <td>
                                        @if($purchaseOrder->isEditable())
                                        <select class="warehouse-id form-control">
                                            <option value="">--Select Warehouse--</option>
                                            @foreach($warehouses as $warehouse)
                                                <option value="{{$warehouse->id}}" @if($purchaseOrder->warehouse_id == $warehouse->id) selected @endif>
                                                    {{$warehouse->name}} - {{$warehouse->address}} - {{$warehouse->gstin}}</option>
                                            @endforeach
                                        </select>
                                        @else
                                            {{$purchaseOrder->warehouse->address}} - {{$purchaseOrder->warehouse->gstin}}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Estimated Delivery Date</td>
                                    <td>
                                        @if($purchaseOrder->isEditable())
                                            <input class="estimated-delivery-date form-control" name="delivery_date" type="date" value="{{$purchaseOrder->estimated_delivery_date}}">
                                        @else
                                            @if($purchaseOrder->estimated_delivery_date)
                                            {{\Carbon\Carbon::parse($purchaseOrder->estimated_delivery_date)->format('jS M, Y')}}
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Total items</td>
                                    <td>{{ $purchaseOrder->items->count() ?? '0' }}</td>
                                </tr>
                                <tr>
                                    <td>Created By</td>
                                    <td>{{ $purchaseOrder->createdBy->name ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Created At</td>
                                    <td>{{ \Carbon\Carbon::parse($purchaseOrder->created_at)->format('d-M-Y') ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td><span class="tag tag-{{$purchaseOrder->status}}">{{$purchaseOrder->status}}</span></td>
                                </tr>
                                @if($purchaseOrder->file)
                                    <tr>
                                        <td>PDF</td>
                                        <td>
                                            <a class="btn btn-success" href="{{route('admin::purchase-order.get-file', $purchaseOrder->id)}}">Download PDF</a>
                                        </td>
                                    </tr>
                                @endif
                            </table>

                            @if($purchaseOrder->isEditable())
                                @if($purchaseOrder->vendor_address_id && $purchaseOrder->warehouse_id)
                                <button class="place-order-item btn margin-t-10 btn-success" type="button" data-id="{{ $purchaseOrder->id }}">Place purchase order</button>
                                @else
                                <button class="update-purchase-order btn margin-t-10 btn-success" type="button" data-id="{{ $purchaseOrder->id }}">Save purchase order</button>
                                @endif
                            @endif

                            @if (!$purchaseOrder->isEditable() && !$purchaseOrder->file)
                                <a href="{{route('admin::purchase-order.generate-file', $purchaseOrder->id)}}" class="generate-file margin-t-10 btn btn-success">Generate Order PDF</a>
                            @endif

                            @if (!$purchaseOrder->isEditable() && $purchaseOrder->status === 'placed')
                                <button class="generate-invoice margin-t-10 btn btn-success" type="button" data-id="{{ $purchaseOrder->id }}">Generate purchase invoice</button>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-12-12">
                    <div class="card">
                        <div class="flex gutter-between cross-center">
                            <div>
                                <h2 class="h5 bold">Order Items ({{ $purchaseOrder->items->count() ?? '0' }})</h2>
                            </div>

                            @if ($purchaseOrder->isEditable())
                            <div>
                                <button class="add-new-item btn btn-md btn-primary" type="button" data-modal="#modal-add-new-item">
                                    Add New Item
                                </button>
                            </div>
                            @endif
                        </div>

                        <hr class="hr hr-light margin-t-20" />

                        <div class="grid">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="padding-v-10">#</th>
                                        <th class="padding-v-10">GID</th>
                                        <th class="padding-v-10">Products</th>
                                        <th class="padding-v-10">Quantity</th>
                                        <th class="padding-v-10">Get cost by</th>
                                        <th class="padding-v-10">Cost factor</th>
                                        <th class="padding-v-10">Cost/Unit</th>
                                        <th class="padding-v-10">Total cost</th>
                                        @if ($purchaseOrder->isEditable())
                                        <th class="padding-v-10">Actions</th>
                                        @endif
                                    </tr>
                                </thead>

                                <tbody>
                                    @if (!empty($purchaseOrder->items))
                                        @foreach ($purchaseOrder->items as $item)
                                            <tr id="edit-row-{{ $item->id }}">
                                                <th scope="row">{{ $loop->index + 1 }}</th>
                                                <td>{{ $item->demandItem->group_id }}</td>
                                                <td>
                                                    @if ($purchaseOrder->isEditable())
                                                    <select class="select2-products form-control" style="width:100%;" disabled>
                                                        @foreach ($item->demandItem->group->products as $product)
                                                            @foreach($product->allVariants as $variant)
                                                                @if($variant->value == 'unit')
                                                                <option @if($item->sku == $variant->sku) selected @endif value="{{$variant->sku}}">{{$variant->sku}} - {{$product->name}} (Qty = {{$variant->quantity}}, MRP = {{$variant->mrp}}))</option>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                        @foreach ($item->demandItem->group->products as $product)
                                                            @php
                                                                $inventories = $product->inventories;
                                                                $firstInventory = null;
                                                                if($inventories->count())
                                                                {
                                                                    $firstInventory = $inventories->first();
                                                                }
                                                            @endphp

                                                            <input type="hidden" class="product_id" value="{{$product->id}}">

                                                            @if($firstInventory)
                                                                <input type="hidden" class="inventory"
                                                                       data-product-id="{{$product->id}}"
                                                                       data-id="{{ $item->id }}"
                                                                       value="{{$firstInventory->unit_cost_price}}">
                                                            @endisset

                                                            @foreach($product->allVariants as $variant)
                                                                <input type="hidden"
                                                                       data-sku="{{$variant->sku}}"
                                                                       class="variant_qty"
                                                                       data-id="{{ $item->id }}" value="{{$variant->quantity}}">

                                                                    <input type="hidden"
                                                                           data-sku="{{$variant->sku}}"
                                                                           data-qty="{{$variant->quantity}}"
                                                                           class="mrp"
                                                                           data-id="{{ $item->id }}" value="{{$variant->mrp}}">
                                                            @endforeach
                                                        @endforeach
                                                    @else
                                                    {{$item->product->name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($purchaseOrder->isEditable())
                                                        <input class="form-control" type="number" min="0" data-id="{{ $item->id }}"
                                                               value="{{ $item->quantity ? $item->quantity : $item->demand->reorder_qty }}"
                                                               name="quantity"
                                                               style="width: 70px" disabled />
                                                    @else
                                                        {{ $item->quantity }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <select class="form-control price_type" data-id="{{ $item->id }}" disabled>
                                                        <option value="">--Select--</option>
                                                        <option @if($item->price_calculation_type == 'margin') selected @endif value="margin">Margin on MRP</option>
                                                        <option @if($item->price_calculation_type == 'markup') selected @endif value="markup">Markup</option>
                                                        <option @if($item->price_calculation_type == 'latest_unit_cost') selected @endif value="latest_unit_cost">Latest unit cost</option>
                                                        <option @if($item->price_calculation_type == 'manual') selected @endif value="manual">Add Manually</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    @if ($purchaseOrder->isEditable())
                                                        <input class="form-control price_calculation_factor" type="number" min="0" data-id="{{ $item->id }}" value="{{$item->price_calculation_factor}}" name="price_calculation_factor"
                                                               style="width: 70px; @if($item->price_calculation_type == 'latest_unit_cost') display:none; @endif" disabled />
                                                    @else
                                                        {{ $item->cost_per_unit }}
                                                    @endif
                                                </td>
                                                <td>
                                                    <span class="cost" data-id="{{ $item->id }}">
                                                        {{ $item->cost_per_unit }}
                                                    </span>
                                                    {{--@if ($purchaseOrder->isEditable())
                                                        <input class="form-control" type="number" min="0" data-id="{{ $item->id }}" value="" name="cost" style="width: 70px" disabled />
                                                    @else
                                                        {{ $item->cost_per_unit }}
                                                    @endif--}}
                                                </td>
                                                <td class="total-amount">
                                                    {{ $item->total_cost }}
                                                </td>
                                                @if ($purchaseOrder->isEditable())
                                                <td>
                                                    <button class="edit-item btn btn-sm btn-type" type="button" data-id="{{ $item->id }}">Edit</button>
                                                    <button class="save-item btn btn-sm btn-primary" type="button" data-id="{{ $item->id }}" style="display:none">Save</button>
                                                    <button class="cancel-item btn btn-sm btn-type btn-danger" type="button" data-id="{{ $item->id }}" style="display:none">Cancel</button>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                @if (!empty($purchaseOrder->purchaseInvoices) && $purchaseOrder->purchaseInvoices->count() > 0)
                    <div class="col-12-12">
                        <div class="card">
                            <div class="flex gutter-between cross-center">
                                <div>
                                    <h2 class="h5 bold">Purchased Invoices ({{ $purchaseOrder->purchaseInvoices->count() ?? '0' }})</h2>
                                </div>
                            </div>

                            <hr class="hr hr-light margin-t-20" />

                            <div class="grid">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th class="padding-v-10">#</th>
                                            <th class="padding-v-10">Vendor</th>
                                            <th class="padding-v-10">Invoice Id</th>
                                            <th class="padding-v-10">Invoice Date</th>
                                            <th class="padding-v-10">Total</th>
                                            <th class="padding-v-10">Status</th>
                                            <th class="padding-v-10">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($purchaseOrder->purchaseInvoices as $item)
                                            <tr>
                                                <th scope="row">{{ $loop->index + 1 }}</th>
                                                <td>
                                                    {{ $item->vendor->name }}
                                                </td>
                                                <td>
                                                    {{ $item->reference_id }}
                                                <td>
                                                    {{$item->invoice_date}}
                                                </td>
                                                <td>
                                                    {{ $item->total }}
                                                </td>
                                                <td>
                                                    {{ $item->status }}
                                                </td>
                                                <td>
                                                    <a href="{{route('admin::purchase-invoices.editInvoice', ['id' => $item->id])}}" class="btn btn-primary">View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
         </div>
    </div>

    <div id="modal-add-new-item" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal">X</span>

            <h1 class="h5">Add New Item</h1>

            <div class="margin-t-20">
                <div class="form-group">
                    <label for="select2-groups">Select A Group</label>
                    <select id="select2-groups" class="form-control" style="width:100%;"></select>
                </div>

                <div class="margin-t-20 margin-b-5">
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <input type="number" id="new-quantity" class="form-control" name="new-quantity" value="0" min="0" />
                    </div>
                </div>

                <button id="button-submit" class="btn btn-primary margin-t-20">Add item to purchase order</button>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            const vendorId = '{{$purchaseOrder->vendor_id}}';
            const submitButton = $('#button-submit');
            const placeOrderButton = $('.place-order-item');
            const addNewItemButton = $('.add-new-item');
            let prevEditItemValue;

            // const calculateAndRenderTotal = (quantity, amount, activeRowId) => {
            //     const row = $('#edit-row-'+activeRowId);
            //     row.find('.total-amount').html('').append(Number(quantity) * Number(amount));
            // };

            const toggleOrderItemsButton = bool => {
                submitButton.prop('disabled', bool);
                placeOrderButton.prop('disabled', bool);
                addNewItemButton.prop('disabled', bool);
                $('.save-item').prop('disabled', bool);
                $('.edit-item').prop('disabled', bool);
                $('.cancel-item').prop('disabled', bool);
            };

            $('#select2-groups').select2({
                minimumInputLength: 2,
                ajax: {
                    url: '{{route("admin::search.groups")}}',
                    dataType: 'json',
                    processResults: (data) => {
                        return {
                            results: data
                        };
                    }
                },
                templateResult: (product) => product.id+' - '+product.name,
                templateSelection: (product) => product.id+' - '+product.name,
            });

            $(".price_type").change(function(){
                const id = $(this).data('id');
                const row = $('#edit-row-'+id);
                const pcFactor = row.find('input[name=price_calculation_factor]');
                const priceType = row.find('select.price_type');
                const costHolder = row.find('span.cost');
                const productId = row.find('select.select2-products').val();

                if(priceType.val() && !productId)
                {
                    alert('please select a product');
                    priceType.val('');
                    return false;
                }

                // costHolder.html('');
                // row.find('.total-amount').html('');
                // console.log('here');
                pcFactor.show();
                // pcFactor.val('');
                if(priceType.val() == 'latest_unit_cost')
                {
                    pcFactor.hide();
                }
            });

            $(".price_calculation_factor").change(function() {
                const id = $(this).data('id');
                const row = $('#edit-row-'+id);
                const costHolder = row.find('span.cost');
                const productId = row.find('input.product_id').val();
                const sku = row.find('select.select2-products').val();
                const variantQty = row.find('input.variant_qty[data-sku="'+sku+'"]');
                const mrp = (row.find('input.mrp[data-sku="'+sku+'"]').val()/variantQty.val()).toFixed();
                const inventory = row.find('input.inventory[data-product-id="'+productId+'"]');
                const pcFactor = row.find('input[name=price_calculation_factor]');
                const priceType = row.find('select.price_type');
                const quantity = row.find('input[name=quantity]');

                let cost = 0;
                if(priceType.val() == 'margin') {
                    cost = (mrp*(1-(pcFactor.val()/100))).toFixed(2);
                    costHolder.html(cost);
                }
                if(priceType.val() == 'markup') {
                    cost = (mrp/(1 + (pcFactor.val()/100))).toFixed(2);
                    costHolder.html(cost);
                }
                if(priceType.val() == 'latest_unit_cost') {
                    // multiple by sku quantity
                    cost = (Number(inventory.val())*Number(variantQty)).toFixed(2);
                    costHolder.html(cost);
                }

                if(priceType.val() == 'manual') {
                    cost = pcFactor.val();
                    costHolder.html(cost);
                }

                if(Number(cost) > Number(mrp))
                {
                    alert('cost cannot be greater than MRP')
                    cost = mrp.val();
                    pcFactor.val(cost);
                    costHolder.html(cost);
                }

                const totalCost = (Number(quantity.val()) * Number(cost) * Number(variantQty.val())).toFixed(2)
                row.find('.total-amount').html(totalCost);
            });

            $('.cancel-item').on('click', function () {

                console.log(prevEditItemValue);

                const id = $(this).data('id');
                const row = $('#edit-row-'+id);

                const quantity = row.find('input[name=quantity]');
                // const cost = row.find('input[name=cost]');
                const pcFactor = row.find('input[name=price_calculation_factor]');
                const product = row.find('input.product_id');
                const sku = row.find('select.select2-products');
                const priceType = row.find('select.price_type');
                const editBtn = row.find('button.edit-item').show();
                const saveBtn = row.find('button.save-item').hide();
                const cancelBtn = row.find('button.cancel-item').hide();

                $('button.edit-item').prop('disabled', false);
                // calculateAndRenderTotal(prevEditItemValue.quantity, prevEditItemValue.pc_factor, id);

                quantity.val(prevEditItemValue.quantity || "0");
                pcFactor.val(prevEditItemValue.pc_factor || "0");
                product.val(prevEditItemValue.product || "");
                sku.val(prevEditItemValue.sku || "");
                priceType.val(prevEditItemValue.price_type || "");

                sku.prop('disabled', true);
                quantity.prop('disabled', true);
                priceType.prop('disabled', true);
                pcFactor.prop('disabled', true);

                // cost.prop('disabled', true)

                toggleOrderItemsButton(false);
            });

            $('.edit-item').on('click', function () {
                toggleOrderItemsButton(true);

                const id = $(this).data('id');
                const row = $('#edit-row-'+id);
                const quantity = row.find('input[name=quantity]');
                // const cost = row.find('input[name=cost]');

                const product = row.find('input.product_id');
                const productSelect =  row.find('select.select2-products');
                const inventory = row.find('input.inventory[data-product-id="'+product.val()+'"]');
                const priceType = row.find('select.price_type');
                const pcFactor = row.find('input[name=price_calculation_factor]');

                if(inventory.length === 0)
                {
                    priceType.find('option[value="latest_unit_cost"]').prop('disabled', true);
                }
                else
                {
                    priceType.find('option[value="latest_unit_cost"]').prop('disabled', false);
                }

                const saveBtn = row.find('button.save-item')
                const editBtn = row.find('button.edit-item')
                const cancelBtn = row.find('button.cancel-item')

                $(".price_type").trigger('change');

                prevEditItemValue = {
                    pc_factor: pcFactor.val(),
                    product: product.val(),
                    sku: productSelect.find(':selected').val(),
                    quantity: quantity.val(),
                    price_type: priceType.val()
                };

                priceType.prop('disabled', false);
                quantity.prop('disabled', false);
                pcFactor.prop('disabled', false);
                editBtn.hide();
                saveBtn.prop('disabled', false);
                saveBtn.show();
                cancelBtn.prop('disabled', false);
                cancelBtn.show();
                productSelect.prop('disabled', false);
            });

            const disableElements = (id) => {
                const row = $('#edit-row-'+id);
                const quantity = row.find('input[name=quantity]');
                // const cost = row.find('input[name=cost]');
                const product = row.find('select.select2-products');
                const saveBtn = row.find('button.save-item');
                const editBtn = row.find('button.edit-item');
                const cancelBtn = row.find('button.cancel-item');
                const priceType = row.find('select.price_type');
                const pcFactor = row.find('input[name=price_calculation_factor]');

                priceType.prop('disabled', true);
                quantity.prop('disabled', true);
                pcFactor.prop('disabled', true);
                editBtn.show();
                saveBtn.prop('disabled', true);
                saveBtn.hide();
                cancelBtn.prop('disabled', true);
                cancelBtn.hide();
                product.prop('disabled', true);
                toggleOrderItemsButton(false);
            }

            $('.save-item').on('click', async function () {
                toggleOrderItemsButton(true);

                let id = $(this).data('id'),
                    row = $('#edit-row-'+id),
                    product_id = row.find('input.product_id').val(),
                    sku = row.find('select.select2-products').val(),
                    quantity = row.find('input[name=quantity]').val(),
                    priceType = row.find('select.price_type').val(),
                    pcFactor = row.find('input[name=price_calculation_factor]').val();

                if (!product_id) {
                    alert('Please select appropriate product.');
                    $(this).prop('disabled', false);
                    return false;
                }

                if (!sku) {
                    alert('Please select appropriate SKU.');
                    $(this).prop('disabled', false);
                    return false;
                }

                if (!quantity || quantity < 1) {
                    alert('Please enter appropriate quantity value.');
                    $(this).prop('disabled', false);
                    return false;
                }

                if (!priceType) {
                    alert('Please select a price type.');
                    $(this).prop('disabled', false);
                    return false;
                }

                if (priceType !== 'latest_unit_cost' && (!pcFactor || pcFactor < 1)) {
                    alert('Please enter appropriate price factor.');
                    $(this).prop('disabled', false);
                    return false;
                }

                await $.ajax({
                    type: 'POST',
                    url: "{{route('admin::purchase-order.update-item', ['id' => $purchaseOrder->id])}}",
                    data: {
                        id,
                        product_id: Number(product_id),
                        sku: sku,
                        quantity: Number(quantity),
                        price_type: priceType,
                        pc_factor: pcFactor
                    },
                    success: (response) => {
                        var toast = new Toast('Item updated successfully.');
                        toast.show();
                        disableElements(id);
                        row.find('.cost').html(Number(response.cost_per_unit).toFixed(2));
                        row.find('.total-amount').html(Number(response.total_cost).toFixed(2));
                        // window.location.reload();
                    },
                    error: (error) => {
                        toggleOrderItemsButton(false);
                        showAllErrorsInToast(error);
                    }
                });
            });

            $(".update-purchase-order").on('click', () => {

                let vendorAddressId = $("select.vendor-address-id").val();
                let warehouseId = $("select.warehouse-id").val();
                let deliveryDate = $('input.estimated-delivery-date').val();
                if (!vendorAddressId || vendorAddressId.length === 0) {
                    alert('Please select vendor address id');
                    return false;
                }

                if (!warehouseId || warehouseId === "0") {
                    alert('Please select warehouse id');
                    return false;
                }

                if (!deliveryDate || deliveryDate === "0") {
                    alert('Please select an estimated delivery date');
                    return false;
                }

                $(".update-purchase-order").prop('disabled', true);

                $.ajax({
                    type: 'POST',
                    url: "{{route('admin::purchase-order.update-purchase-order', ['id' => $purchaseOrder->id])}}",
                    data: {
                        warehouse_id: warehouseId,
                        vendor_address_id: vendorAddressId,
                        delivery_date: deliveryDate
                    },
                    success: (response) => {
                        var toast = new Toast('Order updated successfully.');
                        toast.show();
                        window.location.reload();
                    },
                    error: (error) => {
                        toggleOrderItemsButton(false);
                        showAllErrorsInToast(error);
                        $(".update-purchase-order").prop('disabled', false);
                    }
                });
            });

            submitButton.on('click', () => {
                let group = $('#select2-groups').select2('data'),
                    quantity = $("input[name=new-quantity]").val();

                if (!group || group.length === 0) {
                    alert('Please search and select appropriate group.');
                    return false;
                }

                if (!quantity || quantity === "0") {
                    alert('Quantity must be greater than 0.');
                    return false;
                }

                toggleOrderItemsButton(true);

                $.ajax({
                    type: 'POST',
                    url: "{{route('admin::purchase-order.add-item', ['id' => $purchaseOrder->id])}}",
                    data: {
                        group_id: group[0].id,
                        quantity: Number(quantity)
                    },
                    success: (response) => {
                        var toast = new Toast('Item updated successfully.');
                        toast.show();
                        window.location.reload();
                    },
                    error: (error) => {
                        toggleOrderItemsButton(false);
                        showAllErrorsInToast(error);
                    }
                });
            });

            placeOrderButton.on('click', () => {
                dialog(
                    'Place Order',
                    'Are you sure you want to place this order?',
                    function() {
                        toggleOrderItemsButton(true);

                        $.ajax({
                            type: 'POST',
                            url: "{{route('admin::purchase-order.place-order', ['id' => $purchaseOrder->id])}}",
                            success: (response) => {
                                var toast = new Toast('Order Purchased successfully.');
                                toast.show();
                                window.location.reload();
                            },
                            error: (error) => {
                                toggleOrderItemsButton(false);
                                showAllErrorsInToast(error);
                            }
                        });
                    },
                    function() {
                        return false;
                    }
                );
            });

            $('.generate-invoice').on('click', function () {
                // - Validations
                $(this).prop('disabled', true);

                $.ajax({
                    type: 'POST',
                    url: "{{route('admin::purchase-order.create-purchase-invoice', ['id' => $purchaseOrder->id])}}",
                    success: (response) => {
                        var toast = new Toast('Invoice generated successfully.');
                        toast.show();
                        // - Redirection?
                        location.href = '/purchase-invoice/cart/'+vendorId;
                    },
                    error: (error) => {
                        $(this).prop('disabled', false);
                        showAllErrorsInToast(error);
                    }
                });
            });

            $('input[name=quantity]').change(function () {
                const id = $(this).data('id');
                const row = $('#edit-row-'+id);
                calculateAndRenderTotal($(this).val(), row.find('input[name=cost]').val(), id);
            });

            $('input[name=cost]').change(function () {
                const id = $(this).data('id');
                const row = $('#edit-row-'+id);
                calculateAndRenderTotal(row.find('input[name=quantity]').val(), $(this).val(), id);
            });
        });
    </script>
@endsection