@extends('admin.master')
@section('css')
    <style>
    .tag {
        margin: 5px 5px 5px 0;
    }
    </style>
@endsection

@section('content')
    <div class="container">
        <form class="form" method="post" action="{{route('admin::marketers.update')}}" data-actions=".header-actions" data-destination="{{route("admin::marketers.edit", ['id' => "#id#"])}}" autocomplete="off">
        {{csrf_field()}}
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
        </div>
            <div class="regular light-grey">
                <a href="{{route('admin::marketers.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Marketers</a>
            </div>
            <div class="h1 margin-t-10">
                {{$marketer->code}}
            </div>
            <input type="hidden" name="id" value="{{$marketer->id}}" />
          @include('admin.marketer.form')
        </form>
        <div class="margin-t-20">
            <div class="flex gutter">
                <div class="col-5-12">
                    <h2 class="h6 bold margin-t-10">Brands</h2>
                    <p class="margin-t-10">
                        To assign marketer a brand, use the brand section in admin panel<br />
                    </p>
                </div>
                <div class="col-7-12">
                    <div class="card">
                        @if($marketer->brands->isNotEmpty())
                        <div>
                            @foreach($marketer->brands as $brand)
                                <span class="tag">{{$brand->name}}</span>
                            @endforeach
                        </div>
                        @else
                        <div>
                             <i class="fa fa-frown-o" aria-hidden="true"></i> Assign some brands from the brand section!
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
