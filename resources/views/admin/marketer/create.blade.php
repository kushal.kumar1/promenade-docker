@extends('admin.master')

@section('content')
    <div class="container">
        <form class="form" method="post" action="{{route('admin::marketers.store')}}" data-actions=".header-actions" data-destination="{{route("admin::marketers.edit", ['id' => "#id#"])}}" autocomplete="off">
        {{csrf_field()}}
        <div class="header-actions">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="submit" class="btn btn-primary" data-form=".form-product">Save</button>
        </div>
            <div class="regular light-grey">
                <a href="{{route('admin::marketers.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Marketers</a>
            </div>
            <div class="h1 margin-t-10">
                Add Marketer
            </div>
          @include('admin.marketer.form')
        </form>
    </div>
@endsection
