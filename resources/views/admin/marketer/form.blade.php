<div class="margin-t-20">
    <div class="flex gutter">
        <div class="col-5-12">
            <h2 class="h6 bold margin-t-10">Basic Information</h2>
            <p class="margin-t-10">
                Add / Modify basic information.
            </p>
        </div>
        <div class="col-7-12">
            <div class="card">
                <div>
                    <label class="label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{!empty($marketer) ? $marketer->name : ""}}" />
                    <span class="error error-name"></span>
                </div>
                <div class="margin-t-20">
                    <label class="label">Alias / Short Name</label>
                    <input type="text" name="alias" class="form-control" value="{{!empty($marketer) ? $marketer->alias : ""}}" />
                    <span class="error error-alias"></span>
                </div>
                <div class="margin-t-20">
                    <label class="label">Code</label>
                    <input type="text" name="code" class="form-control" value="{{!empty($marketer) ? $marketer->code : ""}}" />
                    <span class="error error-code"></span>
                </div>
                <div class="margin-t-20">
                    <label class="label">Level</label>
                    <select class="form-control" name="level_id">
                        @foreach($levels as $level)
                        <option @if(!empty($marketer) && $marketer->level_id == $level->id) selected @endif value="{{$level->id}}">{{$level->name}}</option>
                        @endforeach
                    </select>
                    <span class="error error-level_id"></span>
                </div>
            </div>
        </div>
    </div>
</div>
