@extends('admin.master')

@section('content')
<div class="container">
  <form class="form-warehouse form" method="post" action="{{route('admin::warehouses.store')}}" data-actions=".header-actions" data-destination="{{route("admin::warehouses.edit", ['id' => "#id#"])}}">
    <div class="header-actions">
      <button type="button" class="btn btn-discard margin-r-10">Discard</button>
      <button type="submit" class="btn btn-primary" data-form=".form-warehouse">Save</button>
    </div>
    {{csrf_field()}}
    <div class="regular light-grey">
      <a href="{{route('admin::warehouses.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Warehouses</a>
    </div>
    <div class="h1 margin-t-10">
      Add Warehouse
    </div>
      @include('admin.warehouse.form')
  </form>
</div>
@endsection
