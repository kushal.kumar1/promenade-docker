@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Warehouses
        </div>
        <div>
          <a href="{{route('admin::warehouses.create')}}">
            <button class="btn btn-primary">Add Warehouse</button>
          </a>
        </div>
    </div>
    @include('admin.grid')

    {{-- <div id="modal-warehouse-add" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h5">Add Warehouse</h5>
            <form class="form margin-t-30" method="post" action="{{route('admin::warehouses.store')}}" autocomplete="off">
                {{csrf_field()}}
                <label class="label">Name</label>
                <input class="form-control" name="name" />
                <label class="label">Email</label>
                <input class="form-control" name="email" type="email"/>
                <label class="label">Type</label>
                <select class="form-control" name="type">
                    @foreach($warehouseTypes as $type)
                            <option value="{{$type}}">{{$type}}</option>
                    @endforeach
                </select>
                <div class="flex gutter-between margin-t-40">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary button-success">Save</button>
                </div>
            </form>
        </div>
    </div> --}}
</div>

@endsection
