  <div class="margin-t-20">
    <div class="flex gutter">
      <div class="col-5-12">
        <h2 class="h6 bold margin-t-10">Basic Information</h2>
        <p class="margin-t-10">
          Add / Modify basic information.
        </p>
      </div>
      <div class="col-7-12">
        <div class="card">
          <div>
            <label class="label">Name</label>
            <input type="text" name="name" class="form-control" value="{{!empty($warehouse) ? $warehouse->name : ""}}" />
            <span class="error error-name"></span>
          </div>
          <div class="margin-t-20">
            <label class="label">Email</label>
            <input type="text" name="email" class="form-control" value="{{!empty($warehouse) ? $warehouse->email : ""}}" />
          </div>

          <div class="margin-t-20">
            <label class="label">Status</label>
            <select class="form-control" name="status" style="width:100%">
              <option value="1" {{!empty($warehouse) && $warehouse->status ? 'selected' : ""}}>
                Active
              </option>
              <option value="0" {{!empty($warehouse) && !$warehouse->status ? 'selected' : ""}}>
                Inactive
              </option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="margin-t-20">
    <div class="flex gutter">
      <div class="col-5-12">
        <h2 class="h6 bold margin-t-10">Legal Details</h2>
        <p class="margin-t-10">
          Add / Modify legal details for invoicing related tasks.
        </p>
      </div>
      <div class="col-7-12">
        <div class="card">
          <div>
            <label class="label">Legal Name</label>
            <input type="text" name="legal_name" class="form-control" value="{{!empty($warehouse) ? $warehouse->legal_name : ""}}" />
          </div>
          <div class="margin-t-20">
            <label class="label">Invoice Code</label>
            <input type="text" name="invoice_code" class="form-control" value="{{!empty($warehouse) ? $warehouse->invoice_code : ""}}" />
          </div>
          <div class="margin-t-20">
            <div class="flex gutter-sm">
              <div class="col-6-12">
                <label class="label">GSTIN</label>
                <input type="text" name="gstin" class="form-control" value="{{!empty($warehouse) ? $warehouse->gstin : ""}}" />
              </div>
              <div class="col-6-12">
                <label class="label">PAN</label>
                <input type="text" name="pan" class="form-control" value="{{!empty($warehouse) ? $warehouse->pan : ""}}" />
              </div>
            </div>
          </div>

          <div class="margin-t-20">
            <label class="label">Address</label>
            <textarea name="address" class="form-control" rows="5">{{!empty($warehouse) ? $warehouse->address : ""}}</textarea>
          </div>
          <div class="margin-t-20">
            <div class="flex gutter-sm">
              <div class="col-6-12">
                <label class="label">Pin Code</label>
                <input type="text" name="pincode" class="form-control" value="{{!empty($warehouse) ? $warehouse->pincode : ""}}" />
              </div>
              <div class='col-6-12'>
                  <label class="label">City</label>
                  <select class="form-control" name="city_id" style="width:100%">
                    @foreach($cities as $city)
                      <option value="{{$city->id}}">
                        {{$city->name}}
                      </option>
                    @endforeach
                  </select>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
