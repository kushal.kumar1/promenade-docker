@extends('admin.master')

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-warehouse">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::warehouses.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Warehouses</a>
        </div>
        <div class="h3 margin-t-10 bold">
            {{$warehouse->name}}
        </div>
        <form class="form form-warehouse" method="post" action="{{route('admin::warehouses.update')}}" data-actions=".header-actions">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$warehouse->id}}" />
            @include('admin.warehouse.form')
        </form>
    </div>
@endsection
