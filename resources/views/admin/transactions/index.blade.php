@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
          @if($type == 'pending')
            Pending Transactions
          @elseif($type == 'cheque_approval')
            Cheque Pending Approvals
          @else
            Transactions
          @endif
        </div>
    </div>
    @include('admin.grid')
</div>

@endsection
