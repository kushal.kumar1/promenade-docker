@foreach($order->shipments->sortByDesc('id') as $shipment)
  <div class="card margin-t-20 shipment" data-shipment="{{json_encode($shipment)}}">
    <div class="flex gutter-between">
      <div class="flex cross-center">
        <h2 class="h5 bold margin-r-20"><i class="fa fa-truck" aria-hidden="true"></i> Shipment #{{$shipment->id}}</h2>
        <div class="tag tag-{{$shipment->status}}">{{$shipment->status}}</div>
      </div>
      @if ($shipment->canCancel())
      <div>
        <button type="button" class="btn btn-sm shipment-cancel btn-danger" data-shipment-id="{{$shipment->id}}" data-url="{{route('admin::shipments.cancel')}}"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
      </div>
      @endif
      @if ($shipment->canDeliver())
        <div>
          <button type="button" class="btn btn-sm  btn-success"  data-modal="#otp-modal" data-id="{{$shipment->id}}"><i class="fa fa-check" aria-hidden="true"></i> Mark as Delivered</button>
        </div>
      @endif
    </div>
    <div class="margin-t-5 small">
      <i class="fa fa-clock-o" aria-hidden="true"></i> {{(new DateTime($shipment->created_at))->format('F d, Y \a\t h:i A')}}
    </div>
    @if($shipment->boxes)
    <div class="small">
        Boxes: {{$shipment->boxes}}
    </div>
    @endif

    @if(!empty($shipment->manifests) && !empty($shipment->manifests->sortByDesc('id')->first()))
    <div class="small">
      @if($shipment->manifests->sortByDesc('id')->first()->agent)
        Delivery Agent: {{$shipment->manifests->sortByDesc('id')->first()->agent->name}}
        @else
        Delivery Agent: Not Assigned
      @endif
    </div>
    @endif

    <div class="margin-t-20 grey-light bold">
      Items:
    </div>
    <div class="margin-t-5">
      @php
      $groupedShipmentItems=$shipment->orderItems->groupBy('sku');
      @endphp
      @foreach ($groupedShipmentItems as $items)
      <div class="margin-t-5">{{$items->sum('quantity')}} x {{$items->first()->product->name}} ({{$items->first()->variant}})</div>
      @endforeach
    </div>


    @if(!empty($shipment->invoice) && $shipment->status != "cancelled")
      <hr class="hr hr-light margin-t-20" />
      <div class="margin-t-20">
        <div class="flex gutter-sm cross-center">
          <div class="col-6-12">
            <div class="grey-light">
              <span class="bold margin-r-10">Invoice:</span>{{$shipment->invoice->reference_id}}
            </div>
            <div class="grey-light margin-t-5">
              <span class="bold margin-r-10">Amount:</span>Rs. {{number_format($shipment->invoice->amount,2)}}
            </div>
          </div>
          <div class="col-6-12 text-right">
            <a target="_blank" class="btn btn-sm btn-primary" href="{{route('admin::invoices.index',['id'=>$shipment->invoice->id])}}"><i class="fa fa-print" aria-hidden="true"></i> Print Invoice</a>
            @if(!empty($shipment->pod_name))
                <a target="_blank"  class="btn btn-sm btn-primary" href="{{\Niyotail\Helpers\Image::getSrc($shipment)}}">POD</a>
            @endif
          </div>
        </div>
      </div>
    @endif
    @foreach($shipment->returns as $return)
      @include('admin.order.details._return')
    @endforeach
    @if ($shipment->canReturn())
      <hr class="hr hr-light margin-t-20" />
      <div class="margin-t-20">
        <div class="flex gutter-between cross-center">
          <div class="h5 bold">
            Return Items
          </div>
          <div>
            <a class="btn btn-danger" href="{{route('admin::returns.create',$shipment->id)}}">
              Add Return
            </a>
          </div>
        </div>
      </div>
    @endif
  </div>
  @include('admin.order.details.otp')
  @endforeach
