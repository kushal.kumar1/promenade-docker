<hr class="hr hr-light margin-t-20" />
<div class="margin-t-20">
  <div class="flex gutter-between">
    <div class="flex cross-center">
      <h2 class="h5 bold margin-r-20"><i class="fa fa-step-backward" aria-hidden="true"></i>
        Return #{{$return->id}}
      </h2>
      <div class="tag tag-{{$return->status}}">{{$return->status}}</div>
    </div>
    <div class="flex gutter-xsm">
      @if(!empty($return->canCancel()))
        <div>
          <form class="form" method="POST" action="{{route('admin::returns.cancel')}}">
            <input type="hidden" name="id" value="{{$return->id}}">
            <div class="col-4-12"><button type="submit" class="btn btn-sm btn-danger">Cancel</button></div>
          </form>
        </div>
      @endif
      @if(!empty($return->canComplete()))
        <div>
{{--          <form class="form" method="POST" action="{{route('admin::returns.complete')}}">--}}
{{--            <input type="hidden" name="id" value="{{$return->id}}">--}}
{{--            <div class="col-4-12"><button type="submit" class="btn btn-sm btn-success">Complete</button></div>--}}
{{--          </form>--}}
          <a class="btn btn-sm btn-success" href="{{route('admin::returns.confirm-complete', $return->id)}}">Complete</a>
        </div>
      @endif
    </div>
  </div>
  <div class="margin-t-5 small">
    <i class="fa fa-clock-o" aria-hidden="true"></i> {{(new DateTime($return->created_at))->format('F d, Y \a\t h:i A')}}
  </div>
  <div class="margin-t-20 grey-light bold">
      Items:
  </div>
  @foreach ($return->orderItemsBySku as $key => $orderItems)
    @php
        $completeQty = 0;
        $partialQtyDetails = '';
        $variant = $orderItems->first()->productVariant;
        $product = $variant->product;
        foreach($orderItems as $orderItem)
        {
            if(!empty($orderItem->returnOrderItem) && $orderItem->returnOrderItem->units > 0)
            {
                if($orderItem->returnOrderItem->units != $variant->quantity)
                {
                    $partialQtyDetails = ($orderItem->returnOrderItem->units)." ".$product->uom;
                }
                else
                {
                    $completeQty++;
                }
            }
            else
            {
                $completeQty++;
            }
        }
    @endphp
  <div class="margin-t-5">
      @if($completeQty)
        <span class="tag tag-pending">{{$completeQty." x ".$variant->value}}</span>
      @endif
      @if($partialQtyDetails != '')
        <span class="tag tag-pending">{{$partialQtyDetails}}</span>
      @endif
      {{$orderItems->first()->product->name}}
  </div>
  @endforeach
  @if($return->remarks)
    <div class="margin-t-10 grey-light">
      <span class="bold">Remarks:</span> {{$return->remarks}}
    </div>
  @endif
  @if(!empty($return->creditNote))
  <div class="margin-t-10">
    <div class="flex gutter-sm">
      <div class="col-6-12">
        <div class="grey-light">
          <span class="bold margin-r-10">Credit Note:</span>{{$return->creditNote->reference_id}}
        </div>
        <div class="grey-light margin-t-5">
          <span class="bold margin-r-10">Amount:</span>Rs. {{number_format($return->creditNote->amount,2)}}
        </div>
      </div>
      <div class="col-6-12 text-right">
        <a target="_blank" class="btn btn-sm btn-primary" href="{{route('admin::returns.credit-note',['id'=>$return->creditNote->id])}}"></i>Print Credit Note</a>
      </div>
    </div>
  </div>
  @endif
</div>
