@if ($order->canCancel())
    <div id="cancel-order" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h5 bold">Cancel Order</h5>
            <hr class="hr hr-light">
            <form class="margin-t-20 form" method="post" action="{{route('admin::orders.cancel')}}">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$order->id}}">
                <label class="label">Reason</label>
                <select class="form-control" name="reason">
                    @foreach(config('settings.cancellation_reasons') as $value)
                        <option value="{{$value}}">{{$value}}</option>
                    @endforeach
                </select>
                <div class="flex gutter-between margin-t-40">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary button-success">Cancel Order</button>
                </div>
            </form>
        </div>
    </div>
@endif