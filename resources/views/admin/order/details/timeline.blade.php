<h2 class="h3 margin-t-40">Order Logs</h2>
<div class="timeline">
  <div class="card margin-t-20">
    <form class="form" action="{{route("admin::orders.remarks.store", ['id' => $order->id])}}" method="post">
      <label class="label">POST A COMMENT</label>
      <div class="flex gutter-sm">
        <div class="col-10-12">
          <input class="form-control" type="text" name="remarks" value="">
        </div>
        <div class="col-2-12">
          <button type="submit" class="btn col-12-12">POST</button>
        </div>
      </div>
    </form>
  </div>

  @if($order->logs->isNotEmpty())
    @foreach ($order->logs as $log)
    <?php $descriptions = json_decode($log->description)->data; ?>
    <div class="timeline-block">
      <div class="card">
        <div class="flex gutter-between">
          @if($log->employee_id)
            <h5 class="h5">{{$log->employee->name}} updated a status</h5>
            @endif
            <div>
              {{(new DateTime($log->created_at))->format('F d, Y \a\t h:i A')}}
            </div>
        </div>
        <div class="margin-t-20">
          <ul class="list-simple">
            @foreach ($descriptions as $description)
            <li>{{$description}}</li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
    @endforeach
    @else
    <div class="timeline-block">
      No logs found!
    </div>
    @endif
</div>
