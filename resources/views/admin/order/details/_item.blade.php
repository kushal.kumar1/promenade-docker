<div class="margin-t-20 item">
    <div class="flex gutter-xsm">
        <div class="col-1-12">
            <img class="bd" src="{{Image::getSrc($items->first()->product->primary_image, 50)}}" />
        </div>
        <div class="col-4-12">
            <a href="{{route('admin::products.edit',$items->first()->product->id)}}" class="link" target="_blank">
                {{$items->first()->product->name}}
            </a>
            <div class="small">
                SKU: {{$items->first()->sku}} | Variant: {{$items->first()->variant}} ({{$items->first()->productVariant->quantity}} {{$items->first()->productVariant->uom}})
            </div>
{{--            @if($items->first()->status != "cancelled")--}}
{{--              <?php--}}
{{--                  $assignedStock = 0;--}}
{{--                  foreach($items as $item) {--}}
{{--                          $assignedStock += $item->inventoryTransactions->where('type','sale')->sum('quantity');--}}
{{--                  }--}}
{{--              ?>--}}
{{--              <div class="margin-t-5 tag">--}}
{{--                  Assigned Stock: {{abs($assignedStock)}}--}}
{{--              </div>--}}
{{--            @endif--}}
{{--            @if($items->first()->canCancel())--}}
{{--            @can('cancel-order-item')--}}
{{--            <div class="small margin-t-5">--}}
{{--                <button type="button" data-toggle="modal" data-modal="#cancel-item-modal" data-sku='{{$items->first()->sku}}' data-max-qty="{{$items->sum('quantity')}}" class="cancel-item btn btn-danger btn-sm">Cancel Item</button>--}}
{{--            </div>--}}
{{--            @endcan--}}
{{--            @endif--}}
        </div>
        <div class="col-2-12">
            &#8377; {{$items->first()->pretty_price}}
        </div>
        <div class="col-1-12">
            {{$items->sum('quantity')}}
        </div>
        <div class="col-2-12">
            @if ($order->canUpdate())
                @can('update-order-discount')
                  <input type="text" name="items[{{$items->first()->sku}}][discount]" value="{{$items->sum('discount')}}" class="form-control" />
                @else
                    {{-- TODO:: Refactor --}}
                  @if ($items->first()->product->brand->marketer->code == 'MDLZ')
                    <input type="text" name="items[{{$items->first()->sku}}][discount]" value="{{$items->sum('discount')}}" class="form-control" />
                  @else
                    {{$items->sum('discount')}}
                  @endif
                @endcan
            @else
              {{$items->sum('discount')}}
            @endif
        </div>
        <div class="col-2-12 text-right">
            &#8377; {{$items->sum('total')}}
        </div>
    </div>
</div>
