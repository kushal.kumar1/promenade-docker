@extends('admin.master')
@section('css')
    <style>

        .tag.tag-wholesale {
            background-color: #3b4752;
            color: white;
        }

        .tag.tag-ars {
            background-color: #337ab7;
            color: white;
        }

        .tag.tag-new_store {
            background-color: #f8d053;
            color: white;
        }

        .tag.tag-1k_mall {
            background-color: #5bc0de;
            color: white;
        }
    </style>
@append
@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                {{!empty($title) ? $title : 'Orders'}}
            </div>
            @if(Route::getCurrentRoute()->getName() != 'admin::orders.picklist')
                @can('create-picklist')
                    <div>
                        <a href="{{route('admin::orders.picklist')}}">
                            <button class="btn btn-primary">Create Picklist</button>
                        </a>
                    </div>
                @endcan
            @endif
        </div>
        @include('admin.grid')
    </div>
@endsection

