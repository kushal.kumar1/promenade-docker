@extends('admin.master')

@section('content')
<div class="container">
  <div class="regular light-grey">
    <a href="{{route('admin::agentOrder.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Agent Orders</a>
  </div>
  <div class="margin-t-10">
    <div class="flex gutter-between">
      <div>
        <div class="flex cross-center">
          <h1 class="h1 margin-r-20 bold">{{$order->reference_id}}</h1>
          <div class="tag tag-{{$order->status}} margin-r-10 uppercase">{{$order->status}}</div>
        </div>
        <div class="regular grey-light margin-t-10">{{(new DateTime($order->created_at))->format('F d, Y \a\t h:i A')}}</div>
      </div>
    </div>
  </div>
  <div class="margin-t-25">
    <div class="flex gutter">
      <div class="col-8-12">
        <div class="card">
            <div class="flex gutter-between cross-center">
              <h2 class="h5 bold">Order Items</h2>
            </div>
            <hr class="hr hr-light margin-t-20" />
            <div class="margin-t-20">
              <div class="items">
                <div class="flex gutter-xsm bold">
                  <div class="col-1-12">

                  </div>
                  <div class="col-4-12">
                    Item
                  </div>
                  <div class="col-2-12">
                    Price
                  </div>
                  <div class="col-1-12">
                    Qty
                  </div>
                  <div class="col-2-12">
                    Discount
                  </div>
                  <div class="col-2-12 text-right">
                    Total
                  </div>
                </div>
                <hr class="hr hr-light margin-t-20" />
                @foreach ($order->itemsBySku as $sku => $items)
                @include('admin.order.details._item')
                @endforeach
              </div>
            </div>
          <div class="margin-t-20">
            <div class="flex gutter-sm">
              <div class="col-6-12">
              </div>
              <div class="col-6-12">
                <div class="flex gutter-sm">
                  <div class="col-6-12 grey-light text-right">
                    Subtotal
                  </div>
                  <div class="col-6-12 text-right">
                    Rs. {{$order->subtotal}}
                  </div>
                  <div class="col-6-12 grey-light text-right">
                    Discount
                  </div>
                  <div class="col-6-12 text-right">
                    Rs. {{$order->discount}}

                  </div>
                  <div class="col-6-12 grey-light text-right">
                    Total Tax
                  </div>
                  <div class="col-6-12 text-right">
                    Rs. {{$order->tax}}
                  </div>
                  <div class="col-6-12 grey-light text-right h6 bold">
                    Total
                  </div>
                  <div class="col-6-12 text-right h6 bold">
                    Rs. {{$order->total}}
                  </div>
                  <div class="col-6-12 grey-light text-right">
                    Payment Method
                  </div>
                  <div class="col-6-12 text-right">
                    {{$order->payment_method}}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr class="margin-t-20 hr hr-light" />

        </div>
      </div>
      <div class="col-4-12">
        <div class="card">
            <div>
              <h1 class="h5 bold">Store</h1>
              <h1 class="h5 margin-t-20"><a href="{{route('admin::stores.profile', $order->store->id)}}" class="link uppercase" target="_blank">{{$order->store->name}}</a></h1>
              <div class="margin-t-5">
                Beat: {{$order->store->beat->name}}
                @if(!empty($order->createdBy))
                  <div class="margin-t-5"><span class="bold">Order By :</span> {{$order->createdBy->name}} </div>
                  <div class="margin-t-5">{{$order->createdBy->mobile}}</div>
                  <div class="margin-t-5">
                    <span class="tag uppercase small">{{$order->created_by_type}}</span>
                  </div>
                  @endif
                <div class="margin-t-20">
                  <div class="flex gutter-between">
                    <button class="btn btn-sm btn-secondary bold" data-modal="#assign-store">ASSIGN STORE</button>
                  </div>
                </div>


              </div>
            </div>
        </div>
      </div>
    </div>


    <div id="assign-agent" class="modal">
      <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h5 bold">Assign Agent</h5>
        <hr class="hr hr-light">
        <form class="margin-t-20 form" method="post" action="{{route('admin::orders.assign.agent', $order->id)}}">
          {{csrf_field()}}
          <select class="form-control" name="agent_id" id="agents-search" style="width:100%">
          </select>
          <div class="flex gutter-between margin-t-40">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary button-success">Save</button>
          </div>
        </form>
      </div>
    </div>

    <div id="assign-store" class="modal">
      <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h5 bold">Assign Store</h5>
        {{-- <hr class="hr hr-light"> --}}
        <form class="margin-t-30 form" method="post" action="{{route('admin::orders.assign.store', $order->id)}}">
          {{csrf_field()}}
          <select class="form-control" name="store_id" id="store-search" style="width:100%">
            {{-- <option selected value="{{$order->store->id}}">{{$order->store->name}}, {{$order->store->address}}</option> --}}
          </select>
          <div class="flex gutter-between margin-t-40">
            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary button-success">Save</button>
          </div>
        </form>
      </div>
    </div>
    @endsection

    @section('js')
    <script type="text/javascript" >
        function storeTemplate(store) {
            if (!store.id) {
                return store.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">#${store.id} ${store.name} </h5>`;
            html += `<h6 class="margin-t-5">${store.address}</h6>`;
            html += `<h6 class="small margin-t-5">Beat - ${store.beat.name}</h6>`;
            html += '</div>';

            return $(html);
        };

        function storeSelection(store){
            return store.name + ", " + store.address;
        }

        $('#store-search').select2({
            // placeholder: "Search Store by name",
            minimumInputLength: 2,
            ajax: {
                  url: '/search/stores',
                  dataType: 'json',
                  processResults: (data) => { return { results: data }; }
              },
            templateResult: storeTemplate,
            templateSelection: storeSelection
        });

    </script>
    @endsection
