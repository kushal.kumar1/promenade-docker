@extends('admin.master')

@section('content')
<div class="container">
  <div class="regular light-grey">
    <a href="{{route('admin::orders.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Orders</a>
  </div>
  <div class="margin-t-10">
    <div class="flex gutter-between">
      <div>
        <div class="flex cross-center">
          <h1 class="h1 margin-r-20 bold">{{$order->reference_id}}</h1>
          <div class="tag tag-{{$order->status}} margin-r-10 uppercase">{{$order->status}}</div>
        </div>
        <div class="regular grey-light margin-t-10">
          {{(new DateTime($order->created_at))->format('F d, Y \a\t h:i A')}}
        </div>
      </div>
      <div>
        @if ($order->canCancel())
        <button type="button" class="btn btn-danger" data-modal="#cancel-order">
          Cancel Order
        </button>
        @endif
      </div>
    </div>
  </div>
  <div class="margin-t-25">
    <div class="flex gutter">
      <div class="col-8-12">
        <div class="card">
{{--          <form class="form" method="post" action="{{route("admin::orders.items.update", ['id' => $order->id])}}" autocomplete="off">--}}
            <div class="flex gutter-between cross-center">
              <h2 class="h5 bold">Order Items</h2>
{{--              @can('update-order')--}}
{{--              @if ($order->canUpdate())--}}
{{--              <button type="submit" class="btn btn-sm btn-primary">--}}
{{--                Update Items--}}
{{--              </button>--}}
{{--              @endif--}}
{{--              @endcan--}}
            </div>
            <hr class="hr hr-light margin-t-20" />
            <div class="margin-t-20">
              <div class="items">
                <div class="flex gutter-xsm bold">
                  <div class="col-1-12">
                  </div>
                  <div class="col-4-12">
                    Item
                  </div>
                  <div class="col-2-12">
                    Price
                  </div>
                  <div class="col-1-12">
                    Qty
                  </div>
                  <div class="col-2-12">
                    Discount
                  </div>
                  <div class="col-2-12 text-right">
                    Total
                  </div>
                </div>
                <hr class="hr hr-light margin-t-20" />
                @foreach ($order->ItemsByStatusAndSku as $status => $itemsBySku)
                <div class="margin-t-20 items-by-status">
                  <div class="flex gutter-between bold primary">
                    <span class="toggle-items pointer"><i class="fa fa-minus-circle" aria-hidden="true"></i> Status: <span class="uppercase">{{$status}}</span></span>
                    <span>Items: {{count($itemsBySku)}} / Qty: {{$itemsBySku->flatten()->count()}}</span>
                  </div>
                  <hr class="hr hr-light margin-t-20" />
                  <div class="items-by-sku">
                    @foreach ($itemsBySku as $sku => $items)
                            @include('admin.order.details._item')
                    @endforeach
                    <hr class="hr hr-light margin-t-20" />
                  </div>
                </div>
                @endforeach
              </div>
            </div>
{{--          </form>--}}
          <div class="margin-t-20">
            <div class="flex gutter-sm">
              <div class="col-6-12">
              </div>
              <div class="col-6-12">
                <div class="flex gutter-sm">
                  <div class="col-6-12 grey-light text-right">
                    Subtotal
                  </div>
                  <div class="col-6-12 text-right">
                    {{$order->pretty_subtotal}}
                  </div>
                  <div class="col-6-12 grey-light text-right">
                    Discount
                  </div>
                  <div class="col-6-12 text-right">
                    {{$order->pretty_discount}}

                  </div>
                  <div class="col-6-12 grey-light text-right">
                    Total Tax
                  </div>
                  <div class="col-6-12 text-right">
                    {{$order->pretty_tax}}
                  </div>
                  <div class="col-6-12 grey-light text-right h6 bold">
                    Total
                  </div>
                  <div class="col-6-12 text-right h6 bold">
                    {{$order->pretty_total}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @include('admin.order.details.shipment')
        @include('admin.order.details.timeline')
      </div>

      <div class="col-4-12">
        <div class="card">
          <div>
            <h1 class="h5 bold">Store</h1>
            <h1 class="h5 margin-t-20"><a href="{{route('admin::stores.profile', $order->store->id)}}" class="link uppercase" target="_blank">{{$order->store->name}}</a></h1>
            <div class="margin-t-5">
              Beat: {{$order->store->beat->name}}
              @if(!empty($order->createdBy))
                <div class="margin-t-20">
                  <div class="flex gutter-between">
                    <span class="bold">Order By :</span>
                  </div>
                </div>
                <div class="margin-t-5">{{$order->createdBy->name}} </div>
                <div class="margin-t-5">{{$order->createdBy->mobile}}</div>
                <div class="margin-t-5">
                  <span class="tag uppercase small">{{$order->created_by_type}}</span>
                </div>
                @endif
            </div>
            <hr class="hr hr-light margin-t-20" />
            <div class="margin-t-20">
              <div class="flex gutter-between">
                <div class="h5 bold">
                  Shipping Address
                </div>
              </div>
              <div class="margin-t-20">
                {{$order->shippingAddress->name}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->mobile}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->location}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->address}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->landmark}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->city->name}}, {{$order->shippingAddress->pincode}}
              </div>
              <div class="margin-t-5">
                {{$order->shippingAddress->city->state->name}}
              </div>
            </div>
            <hr class="hr hr-light margin-t-20" />
            <div class="margin-t-20">
              <div class="h5 bold">
                Billing Address
              </div>
              @if ($order->shippingAddress->id == $order->billingAddress->id)
              <div class="margin-t-20">
                Same as shipping address
              </div>
              @else
              <div class="margin-t-20">
                {{$order->billingAddress->name}}
              </div>
              <div class="margin-t-5">
                {{$order->billingAddress->address}}
              </div>
              <div class="margin-t-5">
                {{$order->billingAddress->city->name}}, {{$order->billingAddress->city->state->name}} - {{$order->billingAddress->pincode}}
              </div>
              <div class="margin-t-5">
                {{$order->billingAddress->mobile}}
              </div>
              @endif
            </div>
          </div>
        </div>

        @if(!empty($order->additionalInfo))
        <div class="card margin-t-20">
          <h1 class="h5 bold">Additional Info</h1>
          <div class="margin-t-20">
            @if(!empty($order->additionalInfo->pos_order_id))
                <div class="flex gutter-between">
                  <div class="col-6-12">Pos Order Id</div>
                  <div class="col-6-12">{{$order->additionalInfo->pos_order_id}}</div>
                </div>
            @endif
              @if(!empty($order->additionalInfo->stock_transfer_type))
                <div class="flex gutter-between">
                  <div class="col-6-12">Stock Transfer Type</div>
                  <div class="col-6-12">{{strtoupper($order->additionalInfo->stock_transfer_type)}}</div>
                </div>
              @endif
          </div>
        </div>
        @endif

        <div class="margin-t-20">
          @if($order->invoices->isNotEmpty())
            <div class="card">
              <h2 class="h5 bold">Invoices</h2>
              <hr class="hr hr-light margin-t-20" />
              <div class="margin-t-20">
                @foreach($order->invoices as $invoice)
                  <div class="flex gutter cross-center">
                    <div class="col-8-12">
                      <div class="grey-light">
                        <span class="bold margin-r-10">{{$invoice->reference_id}}</span>
                      </div>
                    </div>
                    <div class="col-4-12">
                      <div class="text-right">
                        <a target="_blank" class="btn btn-sm btn-primary" href="{{route('admin::invoices.index',['id'=>$invoice->id])}}">Print</a>
                      </div>
                    </div>
                  </div>
                  @endforeach
              </div>
            </div>
            @endif
        </div>
      </div>
    </div>

    @include('admin.order.details._cancel')
    @endsection
    @section('js')
    <script type="text/javascript" src="{{mix('admin/js/order.js')}}"></script>
    @endsection
