@extends('admin.master')

@section('content')
    <div class="container">
        <div class="h4">({{$storage->id}}) {{$storage->label}} Details</div>
        <div class="card margin-t-10">
            <div class="flex gutter-between">
                @foreach($storageData as $data)
                    <div class="card margin-t-10">
                        <h4 class="card-header">(PID : <span class="bold">{{$data->product_id}}</span>) <span
                                    class="bold">{{$data->product}}</span></h4>
                        <hr>
                        <div class="card-content margin-t-10">
                            @foreach(\Niyotail\Models\FlatInventory::getConstants('STATUS') as $status)
                                <span style="display: block;" @if($data->$status > 0) class="bold" @endif>{{strtoupper(str_replace('_', ' ', $status))}} : <span
                                            class="margin-r-5">{{$data->$status}}</span></span>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection