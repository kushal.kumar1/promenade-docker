@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
               Product Wise Inventory Count
            </div>
        </div>
        @include('admin.grid')
    </div>
@endsection