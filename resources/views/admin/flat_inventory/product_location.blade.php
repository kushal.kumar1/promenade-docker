@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div>
                <div class="h1">
                    <a href="{{route('admin::flat-inventory.product-wise-count')}}"><i class="fa fa-angle-left" aria-hidden="true"></i>
                        {{$product->name}}</a>
                </div>
                <div class="flex cross-left margin-t-10">
                    <div class="h1 bold">
                        #{{$product->id}}
                    </div>
                </div>
                <div class="margin-t-10">
                    <span class="bold uppercase align-left">BARCODE:</span> {{$product->barcode}}
                </div>
            </div>
        </div>
        @include('admin.grid')
    </div>
@endsection