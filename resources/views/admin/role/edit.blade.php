@extends('admin.master')

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-role">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::roles.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Roles</a>
        </div>
        <form class="form form-role" method="post" action="{{route('admin::roles.update')}}" data-actions=".header-actions">
            <input type="hidden" name="id" value="{{$role->id}}">
            {{csrf_field()}}
        <div class="h3 margin-t-10 bold flex">
            <input name="name" value="{{$role->name}}" class="form-control col-4-12">
        </div>
        <div class="margin-t-20">
            <div class="flex gutter">
                <div class="col-4-12">
                    <div class="card">
                        <h2 class="h5 bold">Permissions</h2>
                        <?php $groupedPermissions =  $permissions->groupby('type'); ?>
                        @foreach ($groupedPermissions as $type => $permissions)
                            <hr class="hr-light margin-t-20">
                            <div class="permission-type pointer margin-t-20" data-permission="#{{$type}}">
                                <span class="capitalize">{{$type}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-8-12">
                    @foreach ($groupedPermissions as $type => $permissions)
                        <div class="card permissions hidden" id="{{$type}}">
                            <h2 class="h5 bold capitalize">{{$type}} Permissions</h2>
                            <hr class="hr hr-light margin-t-20" />
                            <div class="margin-t-30">
                                <div class="flex gutter">
                                    @foreach($permissions as $permission)
                                        <label class="col-4-12">
                                            <span class="checkbox">
                                            @if($role->permissions->where('id',$permission->id)->isNotEmpty())
                                            <input name="permissions[]" value="{{$permission->id}}" type="checkbox" checked>
                                            @else
                                            <input name="permissions[]" value="{{$permission->id}}" type="checkbox">
                                            @endif
                                            <span class="indicator"></span>
                                            </span>
                                            {{$permission->label}}
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        </form>
    </div>
    <script>
        $('.permission-type').on('click', function(){
            $('.permission-type').removeClass('bold');
            $(this).addClass('bold');
            var target = $(this).data('permission');
            $('.permissions').addClass('hidden');
            $(target).removeClass('hidden');
        })
        $('.permission-type').first().trigger('click');
    </script>
@endsection
