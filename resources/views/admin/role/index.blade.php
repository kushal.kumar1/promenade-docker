@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Roles
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-employee-add">Add Role</button>
            </div>
        </div>
        <div id="modal-employee-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 text-center">Add Role</h5>
                <form class="form" method="post" action="{{route('admin::roles.store')}}" autocomplete="off">
                    {{csrf_field()}}
                    <div class="margin-t-30">
                        <div class="col-12-12">
                            <label class="label">Name</label>
                            <input class="form-control" name="name" />
                        </div>
                    </div>

                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>

        @include('admin.grid')
    </div>

@endsection
