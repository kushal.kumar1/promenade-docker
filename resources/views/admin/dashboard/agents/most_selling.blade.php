<div class="card margin-t-20">
    <div class="h6 bold font-secondary">Top 10 SKUs</div>
    <hr class="hr hr-light margin-t-20" />

    <div class="grid">
        @if ($topSkus->isNotEmpty())
            <table class="table">
                <tr>
                    <th class="padding-v-10"></th>
                    <th class="padding-v-10">Product</th>
                    <th class="padding-v-10">Cost</th>
                </tr>

                @foreach ($topSkus as $topSku)
                    <tr>
                        <th class="light-grey">{{ $loop->index + 1 }}</th>
                        <td>{{ $topSku->name }}</td>
                        <td class="light-grey">&#8377; {{ number_format($topSku->total) }}</td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="margin-t-20">You don't have any products with-in specified filters.</p>
        @endif
    </div>
</div>

