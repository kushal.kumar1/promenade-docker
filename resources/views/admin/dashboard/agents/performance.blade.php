<div class="grid margin-t-20">
    <table class="table table-bordered table-hover">
        <tr>
            <th></th>
            <th class="padding-v-10">Sales</th>
            <th class="padding-v-10">Unique Stores</th>
            <th class="padding-v-10">Unique Products</th>
            <th class="padding-v-10">Points</th>
        </tr>

        @if ($agentId != '0')
            <tr class="bg-blue bold">
                <th class="padding-h-30">TARGETS</th>
                <td>&#8377; {{ number_format($targets['sales']) }}</td>
                <td>{{ number_format($targets['unique_stores']) }}</td>
                <td>-</td>
                <td>{{ number_format($targets['points']) }}</td>
            </tr>
        @endif
        <tr>
            <th class="padding-h-30">ORDERED</th>
            <td>&#8377; {{ number_format($sales['ordered']) }}</td>
            <td>{{ number_format($unique_stores['ordered']) }}</td>
            <td>{{ number_format($unique_products['ordered']) }}</td>
            <td>{{ number_format($points['ordered'] ?? null) }}</td>
        </tr>

        <tr>
            <th class="padding-h-30">INVOICED</th>
            <td>&#8377; {{ number_format($sales['invoiced']) }}</td>
            <td>{{ number_format($unique_stores['invoiced']) }}</td>
            <td>{{ number_format($unique_products['invoiced']) }}</td>
            <td>{{ number_format($points['invoiced'] ?? null) }}</td>
        </tr>

        <tr class="bold">
            <th class="padding-h-30">DELIVERED</th>
            <td>&#8377; {{ number_format($sales['delivered']) }}</td>
            <td>{{ number_format($unique_stores['delivered']) }}</td>
            <td>{{ number_format($unique_products['delivered']) }}</td>
            <td>{{ number_format($points['delivered'] ?? null) }}</td>
        </tr>
    </table>
</div>
