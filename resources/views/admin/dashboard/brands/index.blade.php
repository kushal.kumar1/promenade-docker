@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Brand Performance
        </div>
    </div>

    <div class="flex gutter-between">
        <div class="card col-12-12 margin-t-30">
            <div class="col-12-12">
            {{-- <div class="flex gutter-between"> --}}
                <a href="{{ route('admin::dashboard.brands.heatmap') }}" class="link medium" title="Switch to Tabular view">Switch to Heat Map View</a>
            </div>
            <div class="col-12-12 margin-t-20">
                <strong>Showing based on order from
                {{\Carbon\Carbon::parse($filters['start_date'])->format("jS M, Y")}}
                 to
                {{\Carbon\Carbon::parse($filters['end_date'])->format("jS M, Y")}}
                </strong>
                <button class="pull-right btn btn-success" data-modal="#modal-filters">Filter Results</button>
            </div>
            @isset($stores)
            <div class="col-2-12 pull-left margin-t-20">
                <strong>Stores</strong>
                <br>
                @foreach($stores as $store)
                    <span data-type="store" data-id="{{$store['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i> {{substr($store['name'], 0, 10)}}</span>
                @endforeach
            </div>
            @endisset
            @isset($brands)
                <div class="col-2-12 pull-left margin-t-20">
                    <strong>Brands</strong>
                    <br>
                    @foreach($brands as $brand)
                        <span data-type="brand" data-id="{{$brand['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i> {{$brand['name']}}</span>
                    @endforeach
                </div>
            @endisset
            @isset($beats)
                <div class="col-2-12 pull-left margin-t-20">
                    <strong>Beat</strong>
                    <br>
                    @foreach($beats as $beat)
                        <span data-type="beat" data-id="{{$beat['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i> {{$beat['name']}}</span>
                    @endforeach
                </div>
            @endisset
            @isset($products)
                <div class="col-2-12 pull-left margin-t-20">
                    <strong>Products</strong>
                    <br>
                    @foreach($products as $product)
                        <span data-type="product" data-id="{{$product['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary" title="{{$product['name']}}"><i class="fa fa-times"></i> {{substr($product['name'], 0, 15)}}</span>
                    @endforeach
                </div>
            @endisset
            @isset($collections)
                <div class="col-2-12 pull-left margin-t-20">
                    <strong>Collections</strong>
                    <br>
                    @foreach($collections as $collection)
                        <span data-type="collection" title="{{$collection['name']}}" data-id="{{$collection['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i> {{substr($collection['name'], 0, 15)}}</span>
                    @endforeach
                </div>
            @endisset
            @isset($marketers)
                <div class="col-2-12 pull-left margin-t-20">
                    <strong>Marketers</strong>
                    <br>
                    @foreach($marketers as $marketer)
                        <span data-type="marketers" title="{{$marketer['name']}}" data-id="{{$marketer['id']}}" class="remove-filter margin-t-5 btn btn-sm btn-primary"><i class="fa fa-times"></i> {{substr($marketer['name'], 0, 15)}}</span>
                    @endforeach
                </div>
            @endisset
        </div>
    </div>

    @include('admin.grid')

</div>
<div id="modal-filters" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 text-center">Filter Brand Performance Data</h5>
        <form method="get" action="{{route('admin::dashboard.brands')}}" autocomplete="off">

            <div class="margin-t-30">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">From (date)</label>
                        <input name="start_date" value="{{$filters['start_date']}}" type="date" class="form-control date-picker">
                    </div>

                    <div class="col-12-12">
                        <label class="label">To (date)</label>
                        <input name="end_date" value="{{$filters['end_date']}}" type="date" class="form-control date-picker">
                    </div>

                    <div class="col-12-12">
                        <label class="label">Brand(s)</label>
                        <select multiple name="brand_ids[]" id="select2-brands" class="form-control" style="width:100%;">
                            @isset($brands)
                                @foreach($brands as $brand)
                                    <option selected="selected" value="{{$brand['id']}}">{{$brand['name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Marketer(s)</label>
                        <select multiple name="marketer_ids[]" id="select2-marketers" class="form-control" style="width:100%;">
                            @isset($marketers)
                                @foreach($marketers as $marketer)
                                    <option selected="selected" value="{{$marketer['id']}}">{{$marketer['name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Store(s)</label>
                        <select multiple name="store_ids[]" id="select2-stores" class="form-control" style="width:100%;">
                            @isset($stores)
                                @foreach($stores as $store)
                                    <option selected="selected" value="{{$store['id']}}">{{$store['name']}}, {{$store['address']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Beat(s)</label>
                        <select multiple name="beat_ids[]" id="select2-beats" class="form-control" style="width:100%;">
                            @isset($beats)
                                @foreach($beats as $beat)
                                    <option selected="selected" value="{{$beat['id']}}">{{$beat['name']}}, {{$beat['long_name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Product(s)</label>
                        <select multiple name="product_ids[]" id="select2-products" class="form-control" style="width:100%;">
                            @isset($products)
                                @foreach($products as $product)
                                    <option selected="selected" value="{{$product['id']}}">{{$product['name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Collection(s)</label>
                        <select multiple name="collection_ids[]" id="select2-collections" class="form-control" style="width:100%;">
                            @isset($collections)
                                @foreach($collections as $collection)
                                    <option selected="selected" value="{{$collection['id']}}">{{$collection['name']}}</option>
                                @endforeach
                            @endisset
                        </select>
                    </div>

                </div>
            </div>

            <div class="flex gutter-between margin-t-40">
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary button-success">Update</button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('js')
    <script>
        function storeTemplate(store) {
            if (!store.id) {
                return store.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${store.name}</h5>`;
            html += `<h6 class="margin-t-5">${store.address}</h6>`;
            html += '</div>';

            return $(html);
        };

        function storeSelection(store){
            return store.text || store.name + ", " + store.address;
        }

        $('#select2-stores').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.stores")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: storeTemplate,
            templateSelection: storeSelection
        });

        function beatTemplate(beat) {
            if (!beat.id) {
                return beat.name;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${beat.name}</h5>`;
            html += `<h6 class="margin-t-5">${beat.long_name}</h6>`;
            html += '</div>';

            return $(html);
        };

        function beatSelection(beat){
            return beat.text || beat.name + ", " + beat.long_name;
        }

        $('#select2-beats').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.beats")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: beatTemplate,
            templateSelection: beatSelection
        });

        function productTemplate(product) {
            if (!product.id) {
                return product.name;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${product.name}</h5>`;
            html += '</div>';

            return $(html);
        };

        function productSelection(product){
            return product.text || product.name;
        }

        $('#select2-products').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.products")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: productTemplate,
            templateSelection: productSelection
        });

        function brandTemplate(brand) {
            if (!brand.id) {
                return brand.name;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${brand.name}</h5>`;
            html += '</div>';

            return $(html);
        }

        function brandSelection(brand){
            return brand.text || brand.name;
        }

        $('#select2-brands').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.brands")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: brandTemplate,
            templateSelection: brandSelection
        });

        function marketerTemplate(marketer) {
            if (!marketer.id) {
                return marketer.name;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${marketer.name}</h5>`;
            html += '</div>';

            return $(html);
        }

        function marketerSelection(marketer){
            return marketer.text || marketer.name;
        }

        $('#select2-marketers').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.marketers")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: marketerTemplate,
            templateSelection: marketerSelection
        });

        //collections
        function collectionTemplate(collection) {
            if (!collection.id) {
                return collection.name;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${collection.name}</h5>`;
            html += '</div>';

            return $(html);
        };

        function collectionSelection(collection){
            return collection.text || collection.name;
        }

        $('#select2-collections').select2({
            multiple: true,
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.collections")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: collectionTemplate,
            templateSelection: collectionSelection
        });

        $(function(){
            $(".remove-filter").click(function(){
                var filterType = $(this).attr("data-type");
                var id = $(this).attr("data-id");

                switch (filterType)
                {
                    case "product":
                        $("#select2-products").find("option[value='"+id+"']").remove();
                        $("#select2-products").trigger('change');
                        break;
                    case "brand":
                        $("#select2-brands").find("option[value='"+id+"']").remove();
                        $("#select2-brands").trigger('change');
                        break;
                    case "beat":
                        $("#select2-beats").find("option[value='"+id+"']").remove();
                        $("#select2-beats").trigger('change');
                        break;
                    case "store":
                        $("#select2-stores").find("option[value='"+id+"']").remove();
                        $("#select2-stores").trigger('change');
                        break;
                    case "collection":
                        $("#select2-collections").find("option[value='"+id+"']").remove();
                        $("#select2-collections").trigger('change');
                        break;
                }

                $(this).remove();
                $("button[type='submit']").click();
            });
        });

    </script>
@endsection
