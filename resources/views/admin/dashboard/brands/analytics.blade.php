@extends ('admin.master')

@section ('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
        integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
        crossorigin=""/>

    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
        integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
        crossorigin=""></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/leaflet/MarkerCluster.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/leaflet/MarkerCluster.Default.css') }}" />
    <style>
        .grid-reset
        {
            display: none!important;
        }
    </style>
@endsection

@section ('header_heading')
    @can ('view-business-dashboard')
        <div class="h5">
            Brands Performance
        </div>
    @endcan
@endsection

@section ('content')

<div class="white-bg">
    <div class="container">

        @include('admin.includes.filters', ['route' => route('admin::dashboard.brands')])

        <div class="card custom margin-t-20">
            <div class="card-header">
                <div class="flex gutter-between">
                    <div class="button-group">
                        <button class="button switchView @if($view_type === 'map-view') active @endif" data-loading="Loading..." data-view="map-view">MAP VIEW</button>
                        <button class="button switchView @if($view_type === 'graph-view') active @endif" data-loading="Loading..." data-view="graph-view">GRAPH VIEW</button>
                        <button class="button switchView @if($view_type === 'table-view') active @endif" data-loading="Loading..." data-view="table-view">TABLE VIEW</button>
                    </div>
                    <div class="col-4-12">
                        <div class="row">
                            <div class="col-6-12 pull-right">
                                <div class="form-group">
                                    <select class="form-control" id="resultTypeSelector">
                                        <option value="">Select type</option>
                                        @foreach($chart_types as $key=>$chart_type)
                                            <option @if(in_array($key, $selected_chart_type)) selected @endif value="{{$key}}">{{$chart_type}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-4-12 margin-t-5 pull-right">
                                <label>Select Type</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-content tab-content">

                <div id="map-view" class="view-type" style="display: @if($view_type !== 'map-view') none @endif ;">
                    <div id="map-container"></div>
                    <div id="heatmap-container"></div>
                </div>


                <div id="graph-view" class="view-type" style="display: @if($view_type !== 'graph-view') none @endif ;">
                    <div class="grid card">
                        <div class="col-12-12 margin-t-20">
                            <canvas id="brands-chart" height="300" width="1000" style="margin: 0 auto"></canvas>
                        </div>
                    </div>
                </div>

                <div id="table-view" class="view-type" style="display: @if($view_type !== 'table-view') none @endif ;">
                    <div class="grid card">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section ('js')
    <script src="{{ asset('plugins/leaflet/heatmap.js') }}"></script>
    <script src="{{ asset('plugins/leaflet/leaflet-heatmap.js') }}"></script>
    <script src="{{ asset('plugins/leaflet/leaflet.markercluster.js') }}"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>

    <script>
    $(function(){
        let brandChart = {};
        let MAP = null;
        let markersLayer = null;
        let resultType = $("#resultTypeSelector").val();

        const viewTypeEndpoints = {
            "table-view": '{{route('admin::dashboard.brands.grid')}}',
            'chart-view': '{{route('admin::dashboard.chart')}}',
            'map-view': '{{route('admin::dashboard.brands.heatmap')}}',
        };

        var switchView = $(".switchView");

        switchView.click(function () {
            switchView.removeClass("active");
            $(this).addClass("active");
            var viewType = $(this).attr("data-view");
            $(".view-type").hide();
            $("#"+viewType).show();
            filterForm.submit();
        });

        $("#resultTypeSelector").change(function(){
            resultType = $(this).val();
            filterForm.submit();
        });


        // - Initialize Map

        let markers = [];
        let features = [];

        const baseLayer = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
            attribution: 'By - Niyotail &copy;',
            maxZoom: 20,
            maxNativeZoom: 20,
            tileSize: 512,
            zoomOffset: -1,
        });

        var heatmapLayer = new HeatmapOverlay({
            gradient: {
                '0.1': 'yellow',
                '0.35':  'blue',
                '0.65': 'cyan',
                '1':  'red'
            },
            minOpacity: .7,
            radius: 20,
            scaleRadius: false,
            useLocalExtrema: true,
            lat: 'lat',
            lng: 'lng',
            valueField: 'orders',
            scale: 'zoom'
        });

        function initMarkersLayer() {
            if(markersLayer) markersLayer = null;
            markersLayer = new L.markerClusterGroup({
                chunkedLoading: true,
                removeOutsideVisibleBounds: true,
                showCoverageOnHover: true,
                zoomToBoundsOnClick: true,
                spiderfyOnMaxZoom: true,
            });
        }


        const initMap = () => {
            if(MAP) MAP.remove();
            initMarkersLayer();
            MAP = new L.Map('map-container', {
                center: new L.LatLng(28.42372200, 76.94036100),
                zoom: 5,
                maxZoom: 20,
                minZoom: 4,
                layers: [
                    baseLayer,
                    heatmapLayer,
                    markersLayer
                ]
            });
        }

        const renderHeatMapToMap = async () => {
            if (MAP.hasLayer(heatmapLayer)) {
                heatmapLayer._reset();

                heatmapLayer.setData({
                    max: 100,
                    min: 0,
                    data: features
                })

                heatmapLayer._update();
            }
        };

        const addFeaturesToMap = () => {
            if (markers.length) {
                if (!MAP.hasLayer(markersLayer)) {
                    MAP.addLayer(markersLayer);
                }
            } else MAP.removeLayer(markersLayer)
        };

        const renderLatestDataToMap = () => {
            addFeaturesToMap();
            renderHeatMapToMap();
        };

        // - Map Event Listners
        // MAP.whenReady(() => MAP.addLayer(heatmapLayer));

        const initChartData = () => {
            brandChart = new Chart(document.getElementById("brands-chart").getContext("2d"), {
                type: 'line',
                data: {
                    labels: [],
                    datasets: []
                },
            });
        };

        initChartData();

        const filterForm = $("form#filterForm");

        filterForm.submit(function(e){
            e.preventDefault();

            let formArray = $(this).serializeArray();
            let currentViewType = $('.switchView.active').attr("data-view");

            if(!resultType)
            {
                alert('Select at-least one view type');
                return false;
            }
            formArray.push({'name':'types[]', 'value': resultType});

            let viewDataEndpoint = null;
            switch (currentViewType)
            {
                case "table-view":
                    viewDataEndpoint = viewTypeEndpoints["table-view"];
                    break;
                case "map-view":
                    viewDataEndpoint = viewTypeEndpoints["map-view"];
                    break;
                case "graph-view":
                    viewDataEndpoint = viewTypeEndpoints["chart-view"];
                    break;
            }

            formArray.push({'name':'view-type', 'value': currentViewType});

            let formData = $.param(formArray);
            window.history.pushState('', '', 'brands?'+formData);

            $.ajax({
                url : viewDataEndpoint,
                type: 'GET',
                data : formData
            }).done(function(response){
                if(currentViewType === "table-view")
                {
                    manageGridViewResponse(response);
                }
                else if(currentViewType === 'graph-view')
                {
                    manageChartViewResponse(response);
                }
                else if(currentViewType === 'map-view')
                {
                    manageHeatmapResponse(response);
                }
            });
        });

        filterForm.submit();

        function manageGridViewResponse(response)
        {
            $("#table-view .grid.card").html(response);
        }

        function manageHeatmapResponse(response)
        {
            if (response) {
                markers = [];
                features = [];

                if (response.length) {
                    initMap();

                    response.map(async order => {
                        let html = '';
                        let feature;

                        switch (resultType) {
                            case 'brand_counts':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Total Brands - ${order.brands_count}</p>
                                    <p>Total Orders - ${order.order_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.brands_count),
                                }
                            break;

                            case 'gross_revenues':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Revenue - ${order.revenue}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.revenue),
                                }
                            break;

                            case 'net_revenues':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Item Total - ${order.item_total}</p>
                                    <p>Total Credit Notes - ${order.total_credit_notes}</p>
                                    <p>Net Revenue - ${order.net_revenue}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.net_revenue),
                                }
                                break;

                            case 'order_counts':
                            case 'order_totals':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.count}</p>
                                    <p>Total - ${order.total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.total),
                                }
                            break;

                            case 'order_item_units':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Units Count - ${order.units_count}</p>
                                    <p>Unique Items - ${order.unique_items}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.units_count),
                                }
                            break;

                            case 'return_totals':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Return Total - ${order.return_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.return_total),
                                }
                            break;

                            case 'top_brands':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Brands Count - ${order.brands_count}</p>
                                    <p>Order Total - ${order.order_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.brands_count),
                                }
                            break;

                            case 'top_marketers':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Marketers - ${order.marketers_count}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Unique Items - ${order.unique_items}</p>
                                    <p>Total Items - ${order.total_items}</p>
                                    <p>Order Total - ${order.order_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.marketers_count),
                                }
                            break;

                            case 'rto_totals': 
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>RTO Total - ${order.rto_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.rto_total),
                                }
                            break;

                            case 'top_stores':
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                    <p>Order Count - ${order.order_count}</p>
                                    <p>Unique Items - ${order.unique_items}</p>
                                    <p>Total Items - ${order.total_items}</p>
                                    <p>Order Total - ${order.order_total}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.order_total),
                                }
                            break;

                            default:
                                html = `
                                    <p>Store Name - ${order.name}</p>
                                `;

                                feature = {
                                    lat: Number(order.lat),
                                    lng: Number(order.lng),
                                    orders: Number(order.order_total) ? Number(order.order_total) : 0,
                                }
                        }

                        let marker = L.marker([Number(order.lat), Number(order.lng)], {
                            elevation: 260.0,
                            title: order.name
                        }).bindPopup(html);

                        marker.addTo(markersLayer);
                        markers.push(marker);
                        features.push(feature);
                    })
                }

                renderLatestDataToMap();
            }
        }

        function manageChartViewResponse(response)
        {
            if (response && response.labels) {
                brandChart.data.labels = response.labels;
                brandChart.data.datasets = [];
                for(var i = 0; i < response.datasets.length; i++)
                {
                    var thisDataset = response.datasets[i];
                    brandChart.data.datasets.push({
                        label: thisDataset.label,
                        data: thisDataset.data,
                        borderColor: thisDataset.borderColor,
                        fill: thisDataset.fill
                    });
                }

                brandChart.update();
            }
        }
    });
    </script>
@endsection
