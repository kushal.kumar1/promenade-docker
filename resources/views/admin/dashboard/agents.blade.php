@extends('admin.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
    <div class="container">
        <h1 class="h1">Hey, {{ auth()->guard('admin')->user()->name }}</h1>

        @can('view-business-dashboard')
            <div class="margin-t-30 card">
                <form class="agent-dashboard-form" method="GET" action="">
                    <div class="flex gutter">
                        <div class="col-3-12">
                            <label class="label">Agent</label>

                            <select name="agentId" class="form-control select-agent">
                                <option value="0">All</option>
                                <?php $agents = $agents->whereIn('id', [1,2,3,4,5,9,40]); ?> 
                                @foreach ($agents as $agent)
                                    <option value="{{ $agent->id }}" {{ $agent->id == $agentId ? 'selected': ''}}>{{ $agent->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-3-12">
                            <label class="label">Date Range</label>

                            <div id="daterange" class="select-date-range flex space-between">
                                <div>
                                    <i class="fa fa-calendar"></i>&nbsp;
                                    <span></span>
                                    <input type="hidden" name="daterange" value="">
                                </div>

                                <div>
                                    <i class="fa fa-caret-down"></i>
                                </div>
                            </div>

                        </div>

                        <div class="col-4-12"></div>
                    </div>
                </form>

                <hr class="hr hr-light margin-t-20" />

                <div class="margin-t-40">
                    @include('admin.dashboard.agents.performance')
                </div>
            </div>

            @include('admin.dashboard.agents.most_selling')
    </div>
    @else
        <p class="margin-t-20">
            You don't have dashboard permissions!
        </p>
        @endcan
        </div>
@endsection


@section('js')
    @can('view-dashboard')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    @endcan

    <script type="text/javascript">
        $(function() {

            $('.select-agent').on('change', function() {
                $('.agent-dashboard-form').submit();
            });

            var start = moment();
            var end = moment();

            function callback(start, end) {
                const daterange = start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY');
                $('#daterange span').html(daterange);
                $('#daterange input').val(daterange);
                $('.agent-dashboard-form').submit();
            }

            $('#daterange').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, callback);

            @if($default === true)
                $('#daterange span').html(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
            @else
                $('#daterange span').html("{{ \Carbon\Carbon::parse($startDate)->format('d-m-Y').' - '.\Carbon\Carbon::parse($endDate)->format('d-m-Y') }}");
            @endif
        });
    </script>
@endsection
