@extends('admin.master')

@section('header_username', Auth::guard('admin')->user()->name)

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('header_heading')
    @can('view-business-dashboard')
        <span class="refresh-button cursor">
            <i class="fa fa-refresh margin-r-5"></i>
            <span>Refresh</span>
        </span>
    @endcan
@endsection

@section('content')
    <div class="white-bg">
        <div class="container">
            @can('view-business-dashboard')

                @include('admin.includes.filters', ['route' => route('admin::dashboard'), 'minimal' => 1])

                <!-- Performance  -->
                <div class="card custom margin-t-40">
                    <div class="card-content">
                        <div class="stats-loading">
                            <div class="loader"></div>
                        </div>

                        <ul class="blocks-list performance-holder has-b-border"></ul>
                    </div>
                </div>
                <!-- KPI -->
                <div class="card custom margin-t-40">
                    <div class="card-header">
                        KPI
                    </div>

                    <div class="card-content">
                        <div class="kpi-loading">
                            <div class="loader"></div>
                        </div>

                        <ul class="blocks-list kpi-holder"></ul>
                    </div>
                </div>

                <!-- Chart -->
{{--                    <div class="card custom margin-t-40">--}}
{{--                        <div class="card-header">--}}
{{--                            <div class="flex">--}}
{{--                                <div class="col-6-12">--}}
{{--                                    <div class="filters">--}}
{{--                                        <div class="filter">--}}
{{--                                            <div id="chart-daterange">--}}
{{--                                                <span></span>--}}
{{--                                                <input class="form-control white-bg" type="text" name="chart-daterange" value="" disabled>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-6-12">--}}
{{--                                    <select class="form-control" id="chartTypeSelect" multiple>--}}
{{--                                        @foreach($chart_types as $key=>$chart_type)--}}
{{--                                            <option @if(in_array($key, $selected_chart_type)) selected @endif value="{{$key}}">{{$chart_type}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="card-content padding-30">--}}
{{--                            <div class="chart-loader">--}}
{{--                                <div class="loader"></div>--}}
{{--                            </div>--}}

{{--                            <div class="chart-holder">--}}
{{--                                <canvas id="dashboard-chart" height="300" width="1000" style="margin: 0 auto"></canvas>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                <!-- Date-wise Data -->

                <div class="card custom margin-t-40">
                    <div class="card-header">
                        Date-wise Summary
                    </div>

                    <div class="card-content">
                        <div class="grid" style="margin-top: 0">
                            <table style="margin-top: 0;" class="data-table-content">
                                <thead>
                                    <tr>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Orders
                                        </th>
                                        <th>
                                            Stores
                                        </th>
                                        <th>
                                            New Stores
                                        </th>
                                        <th>
                                            Order Value
                                        </th>
                                        <th>
                                            Net Revenue
                                        </th>
                                        <th>
                                            Returns
                                        </th>
                                        <th>
                                            Collection
                                        </th>
                                        <th>
                                            Gross Margin
                                        </th>
                                    </tr>
                                </thead>

                                <tbody class="datewise-content">

                                </tbody>
                            </table>

                            <div class="kpi-loading">
                                <div class="loader"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Most Selling -->
                <div class="flex gutter-between margin-t-20 col-on-sm-md">
                    <div class="col-6-12 col-md-12">
                        <div class="card custom" style="width: 98%">
                            <div class="card-header">
                                <div class="flex gutter-between">
                                <h4>Top Selling Products</h4>

                                <p class="small t-grey">[ Showing <span id="total_products">0</span> Products]</p>
                                </div>
                            </div>

                            <div class="card-content">
                                <div class="selling-products-loading">
                                    <div class="loader"></div>
                                </div>

                                <ul class="blocks-list vertical top-selling-products"></ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-6-12 col-md-12">
                        <div class="card custom"
                            style="
                                width: 98%;
                                margin-left: 2%
                            "
                        >
                            <div class="card-header">
                                <div class="flex gutter-between">
                                <h4>Top Retailers</h4>

                                <p class="small t-grey">[ Showing <span id="total_retailers">0</span> Retailers]</p>
                                </div>
                            </div>

                            <div class="card-content">
                                <div class="selling-retailers-loading">
                                    <div class="loader"></div>
                                </div>

                                <ul class="blocks-list vertical top-retailers"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        $( document ).ready(function() {

            // - Loaders
            const performanceLoader = $('.stats-loading');
            const kpiLoader = $('.kpi-loading');

            const topProductsLoader = $('.selling-products-loading');
            const topRetailersLoader = $('.selling-retailers-loading');

            // - Holders
            const performanceHolder = $('.performance-holder');
            const kpiHolder = $('.kpi-holder');
            const topProducts = $('.top-selling-products');
            const topRetailers = $('.top-retailers');
            const dateWiseHolder = $('.datewise-content');

            // - Variables
            const refreshButton = $('.refresh-button');
            const dateRangeHolder =  $('#daterange');

            let queryParams = {
                start_date: moment().format('DD-MM-YYYY'),
                end_date: moment().format('DD-MM-YYYY')
            };

            // const chartLoader = $('.chart-loader');
            // const chartHolder = $('.chart-holder');
            // const chartDateRangeHolder = $('#chart-daterange');
            // let chart;
            //
            //
            // $("#chartTypeSelect").select2({
            //     placeholder: "Chart options",
            //     allowClear: true
            // });
            //
            // $("#chartTypeSelect").change(function () {
            //     fetchChartData();
            // });
            //
            // let graphQueryParams = {
            //     start_date: moment().subtract(29, 'days').format('DD-MM-YYYY'),
            //     end_date: moment().format('DD-MM-YYYY'),
            //     daterange: moment().subtract(29, 'days').format('DD-MM-YYYY')+' - '+moment().format('DD-MM-YYYY'),
            //     types: $('#chartTypeSelect').select2("val")
            // };
            //
            // const toggleChartLoader = (isLoading = true) => {
            //     if (isLoading) {
            //         chartHolder.hide();
            //         chartLoader.show();
            //         chartLoader.children().css('display', 'block');
            //     } else {
            //         chartHolder.show();
            //         chartLoader.hide();
            //         chartLoader.children().css('display', 'none');
            //     }
            // };

            {{--const fetchChartData = async () => {--}}
            {{--    toggleChartLoader();--}}

            {{--    let href = '{{ route("admin::dashboard.chart") }}';--}}

            {{--    graphQueryParams.types = $('#chartTypeSelect').select2("val");--}}
            {{--    graphQueryParams = {...queryParams, ...graphQueryParams};--}}

            {{--    if (!$.isEmptyObject(graphQueryParams)) {--}}
            {{--        href += '?' + $.param(graphQueryParams)--}}
            {{--    }--}}

            {{--    try {--}}
            {{--        let data = await $.ajax(href, {--}}
            {{--            type: 'GET',--}}
            {{--            cache: false,--}}
            {{--        });--}}

            {{--        if (data) {--}}
            {{--            chart.data = data;--}}
            {{--            chart.update();--}}
            {{--        }--}}
            {{--    } catch (err) {--}}
            {{--        console.log('Something went wrong');--}}
            {{--    } finally {--}}
            {{--        toggleChartLoader (false);--}}
            {{--    }--}}
            {{--};--}}

            // - Initialize Chart
            {{--const initChartData = () => {--}}
            {{--    chart = new Chart(document.getElementById("dashboard-chart").getContext("2d"), {--}}
            {{--        type: 'line',--}}
            {{--        data: {--}}
            {{--            labels: [],--}}
            {{--            datasets: []--}}
            {{--        },--}}
            {{--        options: {--}}
            {{--            legend: {--}}
            {{--                position:'bottom',--}}
            {{--                usePointStyle:true,--}}
            {{--                labels: {--}}
            {{--                    padding: 30--}}
            {{--                }--}}
            {{--            },--}}
            {{--            maintainAspectRatio: true,--}}
            {{--            responsive: true,--}}
            {{--            title: {--}}
            {{--                display: false--}}
            {{--            },--}}
            {{--            tooltips: {--}}
            {{--                mode: 'index',--}}
            {{--                intersect: false--}}
            {{--            },--}}
            {{--            hover: {--}}
            {{--                mode: 'nearest',--}}
            {{--                intersect: true--}}
            {{--            },--}}
            {{--            scales: {--}}
            {{--                yAxes: [{--}}
            {{--                    type: 'linear',--}}
            {{--                    display: true,--}}
            {{--                    position: 'left',--}}
            {{--                    id: 'y-axis-1',--}}
            {{--                }, {--}}
            {{--                    type: 'linear',--}}
            {{--                    display: true,--}}
            {{--                    position: 'right',--}}
            {{--                    id: 'y-axis-2',--}}
            {{--                    gridLines: {--}}
            {{--                        drawOnChartArea: false,--}}
            {{--                    },--}}
            {{--                }],--}}
            {{--            }--}}
            {{--        }--}}
            {{--    });--}}
            {{--};--}}

            {{--function graphDateCallback(startDate, endDate) {--}}
            {{--    graphQueryParams['start_date'] = startDate.format('DD-MM-YYYY');--}}
            {{--    graphQueryParams['end_date'] =  endDate.format('DD-MM-YYYY');--}}
            {{--    graphQueryParams['daterange'] = startDate.format('DD-MM-YYYY')+' - '+endDate.format('DD-MM-YYYY');--}}
            {{--    $('#chart-daterange input').val(startDate.format('DD-MM-YYYY') + ' - ' + endDate.format('DD-MM-YYYY'));--}}
            {{--    fetchChartData();--}}
            {{--}--}}

            {{--chartDateRangeHolder.daterangepicker({--}}
            {{--    startDate: moment().subtract(29, 'days'),--}}
            {{--    endDate: moment(),--}}
            {{--    ranges: {--}}
            {{--        'Today': [moment(), moment()],--}}
            {{--        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],--}}
            {{--        'Last 7 Days': [moment().subtract(6, 'days'), moment()],--}}
            {{--        'Last 30 Days': [moment().subtract(29, 'days'), moment()],--}}
            {{--        'This Month': [moment().startOf('month'), moment().endOf('month')],--}}
            {{--        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]--}}
            {{--    },--}}
            {{--    locale: {--}}
            {{--        format: 'DD-MM-YYYY'--}}
            {{--    },--}}
            {{--    maxDate: '{{ date('d-m-Y') }}'--}}
            {{--}, graphDateCallback);--}}

            {{--chartDateRangeHolder.find('input').val(graphQueryParams['daterange']);--}}

            {{--initChartData();--}}
            {{--fetchChartData(true);--}}

            // - Functions
            const togglePerformanceLoader = (isLoading = true) => {
                if (isLoading) {
                    performanceLoader.show();
                    performanceLoader.children().css('display', 'block');
                    performanceHolder.hide();
                } else {
                    performanceLoader.hide();
                    performanceLoader.children().css('display', 'none');
                    performanceHolder.show();
                }
            };

            const toggleTopProductsLoader = (isLoading = true) => {
                if (isLoading) {
                    topProducts.hide();
                    topProductsLoader.show();
                    topProductsLoader.children().css('display', 'block');
                } else {
                    topProductsLoader.hide();
                    topProductsLoader.children().css('display', 'none');
                    topProducts.show();
                }
            };

            const toggleTopRetailersLoader = (isLoading = true) => {
                  if (isLoading) {
                    topRetailers.hide();
                    topRetailersLoader.children().css('display', 'block');
                    topRetailersLoader.show();
                } else {
                    topRetailers.show();
                    topRetailersLoader.hide();
                    topRetailersLoader.children().css('display', 'none');
                }
            }

            const toggleKPILoader = (isLoading = true) => {
                if (isLoading) {
                    kpiHolder.hide();
                    dateWiseHolder.hide();
                    kpiLoader.show();
                    kpiLoader.children().css('display', 'block');
                } else {
                    kpiHolder.show();
                    dateWiseHolder.show();
                    kpiLoader.hide();
                    kpiLoader.children().css('display', 'none');
                }
            };

            const getDataFromAPI = async (href, params = {}, type = 'GET' , data = []) => {
                href += '?' + $.param(queryParams)

                if (!$.isEmptyObject(params)) {
                    href += '&' + $.param(params)
                }

                try {
                    const response = await $.ajax(href, {
                        type,
                        data,
                        cache: false,
                    });

                    if (response) {
                        if (response.data) {
                            return response.data;
                        } else return response;
                    }
                } catch (err) {
                    return err;
                }
            };

            const fetchTopProductsData = async () => {
                const href = '{{ route("admin::api.business.stats") }}';

                const data = await getDataFromAPI (href, {
                    'top_list_type': 'most_selling_products'
                });

                if (data && data.length) {
                    let html = [];

                    $.each(data, function (index, value) {
                        html.push(`
                            <li class="block">
                                <a href="${value.url}">
                                    <div class="flex gutter-sm">
                                        <div class="col-12-12">
                                            <div class="flex">
                                                <img src="${value.image_url}" height="50px" class="padding-10 margin-r-10" />

                                                <div>
                                                    <h3 class="primary bold">${value.name}</h3>

                                                    <div class="flex gutter-between margin-t-10">
                                                        <div>
                                                            <span class="is-15 margin-r-10">
                                                            QTY - <span class="bold">${value.quantity} ${value.variant}</span>
                                                            </span>

                                                            <span class="is-15">
                                                            Amount - <span class="bold">&#8377; ${value.order_item_price}</span>
                                                            </span>
                                                        </div>

                                                        <div>
                                                            <span class="small">
                                                            SKU -
                                                            <span>${value.sku}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        `);
                    });

                    topProducts.html(html.join(""));
                    $('#total_products').html(data.length);
                    toggleTopProductsLoader(false);
                } else {
                    toggleTopProductsLoader(false);
                }
            };

            const fetchTopRetailersData = async () => {
                let href = "{{ route('admin::api.business.stats') }}";

                const data = await getDataFromAPI (href, {
                    'top_list_type': 'most_selling_retailers'
                });

                if (data && data.length) {
                    let html = [];

                    $.each(data, function (index, value) {
                        html.push(`
                            <li class="block">
                                <a href="${value.url}">
                                <h3 class="primary bold">${value.name}</h3>

                                <div class="flex gutter-between margin-t-10">
                                    <div>
                                        <span class="is-15 margin-r-10">
                                            Beat - <span class="bold">${value.beat_name}</span>
                                        </span>

                                        <span class="is-15">
                                            Amount -
                                            <span class="bold">&#8377;${value.order_item_price}</span>
                                        </span>
                                    </div>

                                    <div>
                                        <span class="small">
                                            Address - <span>${value.address}</span>
                                        </span>
                                    </div>
                                </div>
                                </a>
                            </li>
                        `);
                    });

                    topRetailers.html(html.join(""));
                    $('#total_retailers').html(data.length);

                    toggleTopRetailersLoader(false);
                } else {
                    toggleTopRetailersLoader(false);
                }
            };

            const fetchKPIData = async () => {
                let href = '{{ route("admin::api.business.stats") }}';

                try {
                    const data = await getDataFromAPI (href);

                    if (data) {
                        if (data.kpi) {
                            let html = [];

                            $.each(data.kpi, function (index, value) {
                                let classes = '';
                                value.name = value.name.toUpperCase();

                                if (value.name == 'AVERAGE REVENUE PER ORDER' || value.name == 'AVERAGE ORDER PER STORE PER MONTH') {
                                    classes += ' t-success';
                                }

                                html.push(`
                                    <li class="block">
                                        <div class="flex gutter-between">
                                            <p class="block-heading">${value.name}</p>
                                            ${value.percent ? `<p class="t-success bold">[${value.percent.value}]</p>` : ''}
                                        </div>

                                        <h4 class="block-number ${classes}" id="data-orders">
                                            ${value.value}
                                        </h4>
                                    </li>
                                `);
                            });
                            kpiHolder.html(html.join(""));
                        }

                        if (data.table && data.table.length) {
                            let html = [];

                            $.each(data.table, function (index, value) {
                                html.push(`
                                    <tr>
                                        <td>
                                            <p class="bold">${value.date}</p>
                                        </td>

                                        <td>
                                            <p>${value.orders}</p>
                                        </td>

                                        <td>
                                            <p>${value.stores}</p>
                                        </td>

                                        <td>
                                            <p>${value.new_stores}</p>
                                        </td>

                                        <td>
                                            <p>${value.order_value}</p>
                                        </td>

                                        <td>
                                            <p class="t-success">${value.net_revenue}</p>
                                        </td>

                                        <td>
                                            <p>${value.returns}</p>
                                        </td>

                                        <td>
                                            <p>${value.collections}</p>
                                        </td>

                                        <td>
                                            <p>${value.gross_margin}</p>
                                            ${(value.gross_margin_percent === '0.00%') ? (
                                                `<span class="small bold t-alert">[ ${value.gross_margin_percent} ]</span>`
                                            ) : (
                                                `<span class="small bold t-success">[ ${value.gross_margin_percent} ]</span>`
                                            )}
                                        </td>
                                    </tr>
                                `);
                            });

                            dateWiseHolder.html(html.join(""));
                        } else {
                            let html = [];

                            html.push(`
                                <tr>
                                    <td style="display:block;width:100%;clear:both;" class="bold">
                                        <p>No Data Found</p>
                                    </td>
                                </tr>
                            `);

                            dateWiseHolder.html(html.join(""));
                        }
                    }
                } catch (err) {
                    console.log('Something went wrong');
                } finally {
                    toggleKPILoader(false);
                }
            };

            const fetchPerformanceData = async () => {
                let href = '{{ route("admin::api.business.stats") }}';

                const data = await getDataFromAPI (href, {
                    stats_type: 'performance'
                });

                if (data && data.length) {
                    let html = [];

                    $.each(data, function (index, value) {
                        let classes = '';
                        value.name = value.name.toUpperCase();

                        if (value.name == 'ORDERS' || value.name == 'NET REVENUE') {
                            classes += ' t-success';
                        } else if (value.name == 'PENDING TRANSACTIONS') {
                            classes += ' t-alert';
                        } else if (value.name == 'BOUNCED CHEQUES') {
                            classes += ' t-warning';
                        }

                        html.push(`
                            <li class="block">
                                <div class="flex gutter-between">
                                    <p class="block-heading">${value.name}</p>
                                    ${value.percent ? `<p class="t-success bold">[${value.percent.value}]</p>` : ''}
                                </div>

                                <h4 class="block-number ${classes}" id="data-orders">
                                    ${value.value}
                                </h4>
                            </li>
                        `);
                    });
                    performanceHolder.html(html.join(""));
                }

                togglePerformanceLoader(false);
            }

            const refreshData = () => {
                togglePerformanceLoader();
                toggleTopRetailersLoader();
                toggleTopProductsLoader();
                toggleKPILoader();

                fetchPerformanceData();
                fetchKPIData();
                fetchTopProductsData();
                fetchTopRetailersData();
                // fetchChartData();
            };

            // - DOM Event Handlers
            $('.filter_input, #store_type').on('change', function (e) {
                const name = e.target.name.replace('[]', '');
                queryParams[name] = $(this).val();
                queryParams['store_type'] = $('#store_type').val();
                refreshData();
            });

            $("#filterForm").submit(function(e) {
                e.preventDefault();
            });

            refreshButton.click(() => {
                refreshData();
            });

            // - Global Date Range Picker
            function dateRangeCallback(startDate, endDate) {
                queryParams['start_date'] = startDate.format('DD-MM-YYYY');
                queryParams['end_date'] = endDate.format('DD-MM-YYYY');
                $('#daterange input').val(queryParams['start_date']+' - '+queryParams['end_date']);
                refreshData();
            }

            dateRangeHolder.daterangepicker({
                startDate: moment(),
                endDate: moment(),
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                locale: {
                    format: 'DD-MM-YYYY'
                },
                maxDate: '{{ date('d-m-Y') }}'
            }, dateRangeCallback);

            // - Init Data.
            refreshData();
        });
    </script>
@endsection
