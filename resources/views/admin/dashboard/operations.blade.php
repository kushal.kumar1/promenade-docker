@extends('admin.master')

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
<div class="container">
  <h1 class="h1">Operations Dashboard</h1>
  <div class="margin-t-40">
    <div class="flex gutter">
      <div class="col-4-12">
        <div class="card h-100 card-blue">
          <h1 class="h6 bold">Shipment Alerts</h1>
          <div class="margin-t-25">
            <div class="flex gutter">
              <div class="col-10-12">
                Generated
              </div>
              <div class="col-2-12">
                {{$shipments['generated']}}
              </div>
            </div>
            <div class="flex gutter">
              <div class="col-10-12">
                Dispatched
              </div>
              <div class="col-2-12">
                {{$shipments['dispatched']}}
              </div>
            </div>
            <div class="flex gutter">
              <div class="col-10-12">
                Retry
              </div>
              <div class="col-2-12">
                {{$shipments['retry']}}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4-12">
        <div class="card card-teal h-100">
          <h1 class="h6 bold">Order Alerts</h1>
          <div class="margin-t-25">
            <div class="flex gutter">
              <div class="col-10-12">
                Confirmed
              </div>
              <div class="col-2-12">
                  {{$orders['confirmed']}}
              </div>
            </div>
            <div class="flex gutter">
              <div class="col-10-12">
                Manifested
              </div>
              <div class="col-2-12">
                {{$orders['manifested']}}
              </div>
            </div>
            <div class="flex gutter">
              <div class="col-10-12">
                Transit
              </div>
              <div class="col-2-12">
                {{$orders['transit']}}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4-12">
        <div class="card card-yellow h-100">
          <h1 class="h6 bold">Return Alerts</h1>
          <div class="margin-t-25">
            <div class="flex gutter">
              <div class="col-10-12">
                Pending Return
              </div>
              <div class="col-2-12">
                {{$pendingReturns}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="white-bg">
  <div class="container text-center">
    <iframe style="width: 100%; height: 900px" src="https://jarvis.1knetworks.com/public/dashboards/nuHITVWhTVoVWGxZvPTaxoLanUIgWL6foGFYcRG7?org_slug=default">

    </iframe>
  </div>
</div>
@endsection
