@extends('admin.master')

@section('content')
    <div class="white-bg">
        <div class="container text-center">
            <iframe style="width: 100%; height: 900px" src="https://jarvis.1knetworks.com/public/dashboards/itG9FQY6T3GSepNkE4St5FZ6cRYD50y6VDc2PYNx?org_slug=default&p_date_to={{\Carbon\Carbon::today()->format('Y-m-d')}}%2000:00:00&p_date_from=2021-10-01%2000:00:00">

            </iframe>
        </div>
    </div>
@endsection
