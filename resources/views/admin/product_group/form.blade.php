@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
        <div>
            <a href="{{route('admin::product-group.index')}}">
                <i class="fa fa-angle-left" aria-hidden="true"></i> Back to product group
            </a>
        </div>
        </div>

        <form class="form inline-block" method="POST"
            autocomplete="off" data-destination="{{route('admin::product-group.index')}}"
            action="@if(!empty($group->id)) {{route('admin::product-group.update', $group->id)}} @else {{route('admin::product-group.store')}} @endif">

            <div class="card margin-t-30 full-width">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Name</label>
                        <input type="text" name="name" class="form-control" value="{{!empty($group) ? $group->name : ""}}" required />
                        <span class="error error-name"></span>
                    </div>

                    <div class="col-12-12">
                        <label class="label">Brand</label>
                        <select name="brand_id" id="select2-brands" class="form-control" style="width:100%;">
                            @isset($group->brand)
                                <option selected="selected" value="{{$group->brand->id}}">{{$group->brand->name}}</option>
                            @endisset
                        </select>
                    </div>
                </div>

                <div class="margin-t-20">
                    <button type="submit" class="btn btn-primary">
                        {{!empty($group->id) ? "Update" : "Save"}}
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section ('js')
    <script>
        $(document).ready(function () {
            function brandTemplate(brand) {
                if (!brand.id) {
                    return brand.name;
                }
                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${brand.name}</h5>`;
                html += '</div>';

                return $(html);
            }

            function brandSelection(brand){
                return brand.text || brand.name;
            }

            $('#select2-brands').select2({
                multiple: false,
                minimumInputLength: 2,
                ajax: {
                    url: '{{route("admin::search.brands")}}',
                    dataType: 'json',
                    processResults: (data) => { return { results: data }; }
                },
                templateResult: brandTemplate,
                templateSelection: brandSelection
            });
        })
    </script>
@endsection