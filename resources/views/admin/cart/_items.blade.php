<div class="cart-overlay hidden">
  <div class="icon-loading">
    <i class="fa fa-2x fa-spinner fa-spin"></i>
  </div>
</div>
<div class="flex gutter-between">
  <h2 class="regular bold">CART SUMMARY</h2>
  <div>
    <span class="small bold">QTY:</span> {{empty($cart) ? 0 : $cart->items->sum('quantity')}} | <span class="small bold">ITEMS:</span> {{empty($cart) ? 0 : $cart->items->unique('sku')->count()}}
  </div>
</div>
@if (!empty($cart) && $cart->items->isNotEmpty())
<div class="margin-t-20">
  <div class="flex gutter-sm">
    <div class="col-8-12">
      Subtotal
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_subtotal}}
    </div>
    <div class="col-8-12">
      Discount
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_discount}}
    </div>
    <div class="col-8-12">
      Tax
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_tax}}
    </div>
    <div class="col-8-12 bold h6">
      Total
    </div>
    <div class="col-4-12 text-right bold h6">
        {{$cart->pretty_total}}
    </div>
  </div>
</div>
<hr class="hr hr-light margin-t-20" />
@endif
<div class="flex gutter-between margin-t-20">
  <h2 class="regular bold">CART ITEMS</h2>
</div>
@if (!empty($cart) && $cart->items->isNotEmpty())
<div class="cart-items">
  @foreach ($cart->modified_items->sortByDesc('created_at') as $item)
  @include('admin.cart._item')
  @endforeach
</div>
<hr class="hr hr-light margin-t-20" />

@if(false)
<div class="margin-t-20">
  <div class="flex gutter-sm">
    <div class="col-8-12 text-right">
      Subtotal
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_subtotal}}
    </div>
    <div class="col-8-12 text-right">
      Discount
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_discount}}
    </div>
    <div class="col-8-12 text-right">
      Tax
    </div>
    <div class="col-4-12 text-right">
        {{$cart->pretty_tax}}
    </div>
    <div class="col-8-12 text-right bold h6">
      Total
    </div>
    <div class="col-4-12 text-right bold h6">
        {{$cart->pretty_total}}
    </div>
  </div>
</div>
<hr class="hr hr-light margin-t-20" />
@endif
@if($cart->type == \Niyotail\Models\Cart::TYPE_RETAIL)
<div class="flex gutter margin-t-5">
  <div class="col-4-12 h6 bold">
    Tag
  </div>
  <div class="col-8-12">
    <select class="form-control" id="tag" name="tag" style="width: 200px" autocomplete="off">
        <option value="0">--Please select a tag---</option>
        @foreach($tags as $tag)
          <option value="{{$tag->name}}" {{!empty($cart->tag) && ($cart->tag == $tag->name) ? "selected" : ""}}>{{$tag->name}}</option>
        @endforeach
    </select>
  </div>
</div>
@else
  <div class="flex gutter margin-t-5">
    <div class="col-4-12 h6 bold">
      Stock Transfer Type
    </div>
    <div class="col-8-12">
      {{strtoupper(json_decode($cart->additional_info)->st_type)}}
    </div>
  </div>
  @endif

<div id="pos_order_id" class="flex gutter margin-t-5 hidden">
  <div class="col-4-12 h6 bold">
    Pos Order Reference ID
  </div>
  <div class="col-8-12">
    <input class="form-control" name="additional_info[pos_order_id]" required>
    <span class="error error-pos-order-id"></span>
  </div>
</div>

<hr class="hr hr-light margin-t-20" />
<div class="margin-t-20 text-right">
  <button type="button" class="btn btn-primary place-order">Place Order</button>
</div>
@else
<p class="margin-t-10">No items in the cart</p>
@endif
