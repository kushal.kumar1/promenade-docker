<hr class="hr hr-light margin-t-20"/>
<div class="cart-item margin-t-20" data-id="{{$item->id}}">
    <div class="flex gutter-sm">
        <div class="col-2-12" style="height:70px;">
            <img src="{{Image::getSrc($item->product->primaryImage, 80)}}" width="80"/>
        </div>
        <div class="col-10-12">
            <div class="flex gutter-sm">
                <div class="col-8-12">
                    <h6 class="regular">{{$item->product->name}}</h6>
                    <div class="margin-t-5">
                        <span class="capitalize">{{$item->productVariant->value}}</span>
                        <span class="small">[{{$item->productVariant->quantity}} {{$item->productVariant->uom}}]</span>
                    </div>
                    <div class="margin-t-5">
                        <span class="regular bold"> PID:</span> <span class="regular">{{$item->product->id}}</span>
                        @if (!empty($item->product->barcode))
                            | <span class="regular bold">Barcode: </span><span
                                    class="regular">{{$item->product->barcode}}</span>
                        @endif
                    </div>

                    <div class="margin-t-20">
                        <div class="flex gutter-sm">
                            <div class="flex cross-center no-wrap gutter-sm qty">
                                <div>
                                    <button type="button" class="btn btn-sm quantity-subtract">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <input type="number" min="1" class="form-control quantity"
                                       step="{{($item->product->uom != 'pc') ? 0.001 : 1}}"
                                       value="{{($item->product->uom != 'pc') ? $item->quantity : intVal($item->quantity) }}"
                                       data-old="{{($item->product->uom != 'pc') ? $item->quantity : intVal($item->quantity) }}"/>
                                <div>
                                    <button type="button" class="btn btn-sm quantity-add">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div>
                                <button type="button" class="btn btn-sm remove">
                                    <i class="fa fa-trash margin-r-5"></i> Remove
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-4-12 text-right">
                    &#8377; {{$item->pretty_total}}
                    @if($item->discount > 0)
                        <div style="text-decoration: line-through;color:#ff6961">
                            &#8377;{{$item->total + $item->discount}}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>