@extends('admin.master')

@section('css')
<style>
    .variant {
        height: 100%;
        cursor: pointer;
        padding: 5px 15px;
        border-radius: 5px;
        border: 1px dashed #b4b4b4;
        transition: background 0.8s;
    }

    .variant-disabled {
      height: 100%;
      cursor: pointer;
      padding: 5px 15px;
      border-radius: 5px;
      border: 1px dashed #f3c7c7;
      color: #b4b4b4;
    }

    .variant:hover {
        border: 1px solid #d4d4d4;
        background: #fafafa radial-gradient(circle, transparent 1%, #fafafa 1%) center/15000%;
    }

    .variant:active {
        background-size: 100%;
        transition: background 0s;
        background-color: #e5e5e5;
    }

    .quantity.form-control {
        width: 70px;
        padding: 5px;
        text-align: center;
    }

    .cart-overlay, .suggestions-overlay {
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        position: absolute;
        background-color: rgba(255, 255, 255, 0.6);
    }

    .cart-overlay .icon-loading, .suggestions-overlay .icon-loading {
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%, -50%);
    }

    .item-error {
      color: #f55753;
      font-size: 14px;
      font-weight: bold;
      border: 1px dashed #fb9492;
      padding: 5px 10px;
    }
</style>
@append

@section('content')
<div class="container">
    <div class="flex gutter-between">
        <div class="regular light-grey">
            <a href="{{route('admin::stores.profile', ['id' => $store->id])}}">
                <i class="fa fa-angle-left" aria-hidden="true"></i> {{$store->name}}
            </a>
        </div>
{{--        <div>--}}
{{--            <label class="label">Warehouse: </label>--}}
{{--            <select class="form-control" name="warehouse_id" style="width: 200px" onchange="window.location='{{route('admin::stores.cart', ['id' => $store->id])}}?warehouse_id=' + this.value;" autocomplete="off">--}}
{{--                @foreach($warehouses as $warehouse)--}}
{{--                    <option value="{{$warehouse->id}}" {{$warehouse->id == $warehouseId ? "selected" : ""}}>{{$warehouse->name}}</option>--}}
{{--                @endforeach--}}
{{--            </select>--}}
{{--        </div>--}}
    </div>

    <div class="margin-t-20">
        <div class="flex gutter no-wrap">
            <div class="col-6-12">
                <div class="card">
                    @include('admin.cart._store_info')
                    <hr class="hr hr-light margin-t-20" />
                    <div class="margin-t-20">
                        <label class="label">Search Products</label>
                        <input id="products-search" type="text" class="form-control" />
                    </div>
                    <div id="product-suggestions" class="relative"></div>
                </div>
            </div>
            <div class="col-6-12">
                <div class="card">
                    <form class="form form-cart" action="{{route('admin::store.orders.store')}}" method="POST"
                        data-destination="{{route('admin::store.orders.edit', ['id' => '#id#'])}}" autocomplete="off">
                        <input type="hidden" name="store_id" value="{{$store->id}}">
                        <input type="hidden" name="payment_method" value="cash">
                        <div class="cart relative">
                            @include('admin.cart._items')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    class Cart {
        constructor(storeId, warehouseId) {
            this.storeId = storeId;
            this.warehouseId = warehouseId;
            this.initSearchAgents();
            $('#products-search').on('input', (e) => this.onQueryChanged(e));
            $('#product-suggestions').on('click', '.variant', (e) => this.addItem($(e.currentTarget)));
            $('.cart').on('click', '.quantity-subtract, .quantity-add', (e) => this.onQuantityChange(e));
            $('.cart').on('change', '.quantity', (e) => this.onQuantityChange(e));
            $('.cart').on('click', '.cart-item .remove', (e) => this.removeItem($(e.currentTarget).closest('.cart-item')));
            $('.form-cart').on('click','.place-order',(e) => this.confirmToPlaceOrder(e));
            $('.form-cart').on('change', '#tag', (e) => this.removeOnChangeTag(e));
        }

        removeOnChangeTag(e){
            if(e.target.value == '1k_mall'){
                $('#pos_order_id').removeClass("hidden");
            }else{
                $("input[name='additional_info[pos_order_id]']").val(null);
                $('#pos_order_id').addClass("hidden");
            }
        }

        initSearchAgents() {
          $('#agents-search').select2({
              placeholder: "Search Agent by name or code",
              minimumInputLength: 2,
              ajax: {
                  url: function (params) {
                        return '/search/agents?type=sales';
                      },
                  dataType: 'json',
                  processResults: function(data) {
                      data = $.map(data, function(obj) {
                          obj.id = obj.id;
                          obj.text = obj.name;
                          return obj;
                      });
                      return {
                          results: data
                      };
                  }
              }
          });
        }

        onQueryChanged(e) {
            $('.suggestions-overlay').removeClass('hidden');
            clearTimeout(this.searchTimer);
            this.searchTimer = setTimeout(() => {
                const term = e.target.value;
                if (term) {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::carts.suggestions")}}', {
                        method: 'GET',
                        data: { term: term, store_id: this.storeId }
                    });
                    this.searchRequest.done((products) => $('#product-suggestions').html(products));
                    this.searchRequest.fail((error) => this.onError(error));
                }
            }, 100);
        }

        onQuantityChange(e) {
            const $cartItem = $(e.currentTarget).closest('.cart-item');
            let quantity = parseFloat($cartItem.find('.quantity').val());
            let delay = 0;
            if ($(e.currentTarget).hasClass('quantity-add')) {
                quantity++;
                delay = 200;
            } else if ($(e.currentTarget).hasClass('quantity-subtract')) {
                quantity--;
                delay = 200;
            }

            if (quantity > 0) {
                $cartItem.find('.quantity').val(quantity);
                clearTimeout(this.updateTimer);
                this.updateTimer = setTimeout(() => this.updateItem($cartItem), delay);
            }
        }

        addItem($variant) {
            $('.cart-overlay').removeClass('hidden');
            const id = $variant.data('id');
            const source = $variant.data('source');
            const request = $.ajax('{{route("admin::carts.add-item")}}', {
                method: 'POST',
                data: {
                    id: id,
                    source: source,
                    quantity: 1,
                    store_id: this.storeId,
                    warehouse_id: this.warehouseId
                }
            });
            request.done((cart) => {$('.cart').html(cart); this.initSearchAgents();});
            request.fail((error) => this.onError(error));
        }

        updateItem($cartItem) {
            $('.cart-overlay').removeClass('hidden');
            const id = $cartItem.data('id');
            const quantity = parseFloat($cartItem.find('.quantity').val());
            const old = parseFloat($cartItem.find('.quantity').data('old'));
            const updateRequest = $.ajax('{{route("admin::carts.update-item")}}', {
                method: 'POST',
                data: {
                    id: id,
                    quantity: quantity,
                    store_id: this.storeId
                }
            });
            updateRequest.done((cart) => {$('.cart').html(cart); this.initSearchAgents();});
            updateRequest.fail((error) => {$cartItem.find('.quantity').val(old); this.onError(error)});
        }

        removeItem($cartItem) {
            $('.cart-overlay').removeClass('hidden');
            const id = $cartItem.data('id');
            const request = $.ajax('{{route("admin::carts.remove-item")}}', {
                method: 'POST',
                data: {
                    id: id,
                    store_id: this.storeId
                }
            });
            request.done((cart) => {$('.cart').html(cart); this.initSearchAgents();});
            request.fail((error) => this.onError(error));
        }

        onError(error){
            $('.cart-overlay').addClass('hidden');
            let message = error.responseJSON.message;
            if (message == undefined) {
                message = "An unexpected error has occurred";
            }
            const toast = new Toast(message);
            toast.show();
        }

        confirmToPlaceOrder(e) {
            let tag = $('#tag').val();
            let posOrderId = $("input[name='additional_info[pos_order_id]']").val();
            if(tag === '1k_mall' && !posOrderId){
                $('.error-pos-order-id').html("Pos Order Reference ID is required");
                $('.error-pos-order-id').css('display', 'block');
            }else{
                $('.place-order').attr('disabled', 'disabled');
                dialog('Confirm Order', 'Are you sure you want to place this order?', function() {
                    $('.form-cart').submit()
                }, function() {
                    $('.place-order').removeAttr('disabled');
                });
            }
        }
    }

    new Cart({{$store->id}}, {{$warehouseId}});
</script>
@append
