<div class="suggestions-overlay hidden">
    <div class="icon-loading">
        <i class="fa fa-2x fa-spinner fa-spin"></i>
    </div>
</div>
@foreach ($products as $product)
<hr class="hr hr-light margin-t-20" />
<div class="product margin-t-20">
    <div class="flex cross-center no-wrap gutter-sm">
        <div>
            <img src="{{Image::getSrc($product->primaryImage, 80)}}" width="70" />
        </div>
        <div class="col-12-12">
            <span class="regular bold"> PID:</span> <span class="regular">{{$product->id}}</span> | <span class="regular bold">Stock: </span> <span class="regular">{{$product->current_inventory}}</span>
            @if (!empty($product->barcode))
              | <span class="regular bold">Barcode: </span><span class="regular">{{$product->barcode}}</span>
            @endif
            <h6 class="small">{{$product->brand->name}}</h6>
            <h4 class="regular bold">{{$product->name}}</h4>
            <div class="margin-t-5">
            </div>
        </div>
    </div>
    <div>
        <div class="flex gutter-sm">
            @foreach ($product->variants as $variant)
            <div class="col-4-12">
                <div class="variant text-center" data-id="{{$variant->id}}">
                    <div>&#8377; {{round($variant->getFinalPrice() - $variant->discount, 2)}}
                      @if($variant->discount > 0)
                      <span style="text-decoration: line-through;color:#ff6961">&#8377;{{$variant->getFinalPrice()}}</span>
                      @endif
                    </div>
{{--                    <div class="small">--}}
{{--                      OUT OF STOCK--}}
{{--                    </div>--}}
                  <div class="margin-t-5 capitalize">
                      {{$variant->value}} <span class="small">({{$variant->quantity}} {{$variant->uom}})</span> | <span class="small">MOQ: {{$variant->moq}}</span>
                  </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endforeach
