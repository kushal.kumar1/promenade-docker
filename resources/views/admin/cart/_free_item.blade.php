<div class="flex gutter-sm margin-t-10 cross-center">
  <div class="col-2-12 text-center" style="height:70px;">
    <img src="{{Image::getSrc($item->product->primaryImage, 50)}}"/>
  </div>
  <div class="col-10-12">
    <div class="flex gutter-sm">
      <div class="col-8-12">
        <h6 class="regular margin-t-10">{{$item->product->name}} ({{$item->productVariant->value}})</h6>
        <div>
          Qty: {{$item->quantity}}
        </div>
        <div class="margin-t-10 tag tag-cancelled">
            <i class="fa fa-rocket" aria-hidden="true"></i> PROMO : {{$item->rules->first()->name}}
        </div>
      </div>
      <div class="col-4-12 text-right">
        &#8377; {{$item->total}}
        @if($item->discount > 0)
          <div style="text-decoration: line-through;color:#ff6961">&#8377;{{$item->total + $item->discount}}</div>
          @endif
      </div>
    </div>
  </div>
  <div class="col-12-12">
    @if(!empty($cartErrors[$item->id]))
      <div>
        <span class="item-error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{$cartErrors[$item->id]}}</span>
      </div>
      @endif
  </div>
</div>
