<div class="flex gutter-between no-wrap">
    <div>
        <h2 class="regular bold">{{$store->name}}</h2>
        <h6 class="margin-t-5">{{$store->address}}</h6>
    </div>
    <div>
        <div>
            <span class="small bold">BALANCE:</span>
            &#8377; {{empty($store->totalBalance) ? 0 : $store->totalBalance->balance_amount}}
        </div>
    </div>
</div>
