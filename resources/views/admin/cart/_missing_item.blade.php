<div class="flex gutter-sm margin-t-10 cross-center">
    <div class="col-1-12">
        <i class="fa fa-exclamation-triangle item-error"
           aria-hidden="true"></i>
    </div>
    <div class="col-2-12 text-center" style="height:70px;">
        <img src="{{Image::getSrc($item->product->primaryImage, 50)}}"/>
    </div>
    <div class="col-9-12">
        <div class="flex gutter-sm">
            <div class="col-12-12">
                <h6 class="regular margin-t-10"> {{$item->product->name}} ({{$item->value}})
                </h6>
            </div>
        </div>
    </div>
</div>