@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Employees
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-employee-add">Add Employee</button>
            </div>
        </div>
        <div id="modal-employee-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h6 text-center">Add Employee</h5>
                <form class="form" method="post" action="{{route('admin::employees.store')}}" autocomplete="off">
                    {{csrf_field()}}
                    <div class="margin-t-30">
                        <div class="flex gutter">
                            <div class="col-12-12">
                                <label class="label">Name</label>
                                <input class="form-control" name="name" />
                            </div>
                            <div class="col-12-12">
                                <label class="label">Email</label>
                                <input class="form-control" name="email" />
                            </div>
                            <div class="col-12-12">
                                <label class="label" id="mobile">Mobile</label>
                                <div class="form-control-group">
                                    <span class="form-control-addon">+91</span>
                                    <input type="number" name="mobile" class="form-control" id="mobile"/>
                                </div>
                            </div>
                            <div class="col-12-12">
                                <label class="label">Password</label>
                                <input class="form-control" name="password" type="password" />
                            </div>
                        </div>
                    </div>

                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>

        @include('admin.grid')
    </div>

@endsection
