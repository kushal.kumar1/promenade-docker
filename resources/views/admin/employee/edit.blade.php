@extends('admin.master')

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-employee">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::employees.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Employees</a>
        </div>
        <div class="h3 margin-t-10 bold">
            {{$employee->name}}
        </div>
        <form class="form form-employee" method="post" action="{{route('admin::employees.update')}}" data-actions=".header-actions">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$employee->id}}" />
            <div class="margin-t-20">
                <div class="flex gutter">
                    <div class="col-5-12">
                        <h2 class="h6 bold margin-t-10">Account Information</h2>
                        <p class="margin-t-10">
                            Change user details & password.
                        </p>
                    </div>
                    <div class="col-7-12">
                        <div class="card">
                            <div>
                                <label class="label">Name</label>
                                <input type="text" name="name" class="form-control" value="{{$employee->name}}"/>
                            </div>
                            <div class="margin-t-25">
                                <label class="label">Email</label>
                                <input type="text" name="email" class="form-control" value="{{$employee->email}}"/>
                            </div>
                            <div class="margin-t-25">
                                <label class="label" id="mobile">Mobile</label>
                                <div class="form-control-group">
                                    <span class="form-control-addon">+91</span>
                                    <input type="number" name="mobile" class="form-control" value="{{substr($employee->mobile,3)}}" id="mobile"/>
                                </div>
                            </div>
                            <div class="margin-t-20">
                                <label class="label">Status</label>
                                <select name="status" class="form-control">
                                    <option value="1" {{$employee->status ?'selected':''}}>
                                        Active
                                    </option>
                                    <option value="0" {{$employee->status ?'':'selected'}}>
                                        Inactive
                                    </option>
                                </select>
                            </div>

                            <div class="margin-t-20">
                                <label class="label">Assign Warehouses</label>
                                <select class="form-control warehouse-search" name="warehouses[]" multiple="multiple">
                                    @foreach($employee->warehouses as $warehouse)
                                        <option value="{{$warehouse->id}}" selected>{{$warehouse->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="margin-t-25">
                                <label class="margin-r-20">
                                    <span class="checkbox">
                                    <input name="change-password" type="checkbox">
                                    <span class="indicator"></span>
                                    Change Password
                                    </span>
                                </label>
                                {{-- <label>
                                    <span class="checkbox">
                                    <input name="status" type="checkbox" class="form-control" {{$employee->status?'checked':''}}>
                                    <span class="indicator"></span>
                                    Active
                                    </span>
                                </label> --}}
                            </div>
                            <div class="margin-t-10 hidden">
                                <label class="label">New Password</label>
                                <input type="password" name="password" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="margin-t-40">
                <div class="flex gutter">
                    <div class="col-5-12">
                        <h2 class="h6 bold margin-t-10">Assign Roles</h2>
                        <p class="margin-t-10">
                            Assign multiple roles to the user.
                        </p>
                    </div>
                    <div class="col-7-12">
                        <div class="card">
                            <h2 class="h6 bold">Select Roles</h2>
                            @foreach($roles as $role)
                                <div class="margin-t-20">
                                    <label>
                                        <span class="checkbox">
                                        <input name="roles[]" type="checkbox" value="{{$role->id}}" {{($employee->roles->where('id',$role->id)->isNotEmpty()) ? "checked": ""}}>
                                        <span class="indicator"></span>
                                        </span>
                                        {{$role->name}}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        $('input[name="change-password"]').on('change', function(){
            $('input[name="password"]').parent().toggleClass('hidden');
        })
    </script>
@endsection
