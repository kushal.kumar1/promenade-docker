@extends('admin.master')

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-employee">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::dashboard')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Dashboard</a>
        </div>
        <form class="form form-employee" method="post" action="{{route('admin::update.profile')}}" data-actions=".header-actions">
            {{csrf_field()}}
            <div class="margin-t-20">
                <div class="flex gutter">
                    <div class="col-5-12">
                        <h2 class="h6 bold margin-t-10">Account Information</h2>
                        <p class="margin-t-10">
                            Change your details & password.
                        </p>
                    </div>
                    <div class="col-7-12">
                        <div class="card">
                            <div>
                                <label class="label">Name</label>
                                <input type="text" name="name" class="form-control" value="{{$employee->name}}"/>
                                <span class="error error-name"></span>
                            </div>
                            <div class="margin-t-10 hidden">
                                <label class="label">New Password</label>
                                <input type="password" name="password" class="form-control"/>
                                <span class="error error-password"></span>
                            </div>
                            <div class="margin-t-25">
                                <label class="margin-r-20">
                                    <span class="checkbox">
                                    <input name="change-password" type="checkbox">
                                    <span class="indicator"></span>
                                    Change Password
                                    </span>
                                </label>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <script>
        $('input[name="change-password"]').on('change', function(){
            $('input[name="password"]').parent().toggleClass('hidden');
        })
    </script>
@endsection
