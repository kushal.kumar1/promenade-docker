<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Niyotail - Administration Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/plugins/jquery-date-range-picker/daterangepicker.min.css">
    <link rel="stylesheet" href="/plugins/intl-tel-input/css/intlTelInput.css">
    <link rel="stylesheet" href="{{ mix('admin/css/app.css') }}">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.3/moment.min.js"></script>
    <script type="text/javascript" src="/plugins/jquery-date-range-picker/jquery.daterangepicker.min.js"></script>
    @yield('css')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</head>

<body>
<div class="modal-backdrop"></div>
<div class="ajax-loader"></div>
<div id="dialog" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
        <h5 class="h6 title bold">Reset all settings</h5>
        <p class="margin-t-20 message">
            This will restore all system settings to factory defaults. Are you sure that you want to proceed?
        </p>
        <div class="flex gutter-between margin-t-20">
            <button class="btn button-reject">Cancel</button>
            <button class="btn btn-primary button-success">Yes</button>
        </div>
    </div>
</div>
@include('admin.templates.toast')
@include('admin.sidebar')
@include('admin.header')
<div class="container-wrapper">
    @yield('content')
    <div id="notification-container"></div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script type="text/javascript" src="/plugins/intl-tel-input/js/intlTelInput.min.js"></script>
<script type="text/javascript" src="/plugins/intl-tel-input/js/utils.js"></script>
<script type="text/javascript" src="{{mix('admin/js/app.js')}}"></script>
<script type="text/javascript" src="{{mix('admin/js/search.js')}}"></script>
<script>
    $(function () {
        var url = window.location.href;
        var $match = $('.sidebar-menu a[href="' + url + '"]');
        if ($match.closest('ul .submenu').length > 0) {
            $match.closest('ul .submenu').addClass('active');
            $match.css('color', '#fff');
        }
    });

    $(document).on('click', '[data-toggle="popover"]', function () {
        $(this).next().toggle();
    });

    $(document).on('change', '.select-warehouse', function () {
        let request = $.ajax(`{{route('admin::select-warehouse')}}`, {
            type: "POST",
            data: {'warehouse_id': $(this).val()},
            cache: false
        });

        request.done((response) => {
            window.location.reload();
        });
        request.fail((error) => {
            // window.location.reload();
        });
    });
</script>
@yield('js')

</body>

</html>
