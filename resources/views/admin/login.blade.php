<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Niyotail - Administration Panel</title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/admin/css/app.css') }}">
    <link rel="stylesheet" href="/chopper/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
    <div class="container flex">
        <div class="login-form">
            <div class="h3 bold text-center margin-t-40">
                <img src="/niyotail.png" width="200px"/>
            </div>
            <div class="bg-white padding-30 margin-t-20">
                <div class="h6 bold text-center">
                    LOGIN
                </div>
                <form class="form" method="post" action="{{route('admin::authenticate')}}">
                    {{csrf_field()}}
                    <div class="margin-t-30">
                        <label class="label">Email</label>
                        <input class="form-control" type="email" name="email"/>
                    </div>
                    <div class="margin-t-25">
                        <label class="label">Password</label>
                        <input class="form-control" name="password" type="password"/>
                    </div>
                    <div class="margin-t-40 text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <div class="small text-center margin-t-20 bold">
                &copy; Odicea Distribution Technologies Pvt. Ltd.
            </div>
        </div>
    </div>

    @include('admin.templates.toast')
    <script type="text/javascript" src="{{mix('admin/js/app.js')}}"></script>
</body>
</html>
