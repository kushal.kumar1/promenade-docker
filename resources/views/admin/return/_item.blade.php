@php
$item = $items->first();
$product = $item->product;
$uom = $product->uom;
$orderVariant = $item->productVariant;
$isVisible = false;
$lastVariantQty = 0;
@endphp
<div class="flex gutter item">
  <div class="col-1-12">
    <img class="bd" src="{{Image::getSrc($item->product->primaryImage, 60)}}" width="60" />
  </div>
  <div class="col-4-12">
    <h3>{{$item->product->name}}</h3>
    <div class="margin-t-5 small">
      SKU: {{$item->sku}}
    </div>
  </div>
  <div class="col-3-12">
  <div class="flex">
    <input type="hidden" name="items[{{$loop->index}}][original_type]" value="{{$item->variant}}">
    @include('admin.return.common._quantity-input', ['class' => 'col-4-12', 'pre' => 'items['.$loop->index.']'])
  </div>
    {{--<input class="form-control quantity" type="number" name="items[{{$loop->index}}][quantity]" value="{{$items->sum('quantity')}}" max="{{$items->sum('quantity')}}" min="1" disabled/>--}}
  </div>
  <div class="col-3-12">
        <select class="form-control reason" name="items[{{$loop->index}}][reason]" disabled>
            <option value="">-- Select --</option>
            @foreach(config('settings.return_reasons') as $reason)
                <option value="{{$reason}}">
                    {{$reason}}
                </option>
            @endforeach
        </select>
  </div>
  <div class="col-1-12 text-right">
    <label class="checkbox checkbox-item">
      <input type="checkbox" class="item-checkbox" name="items[{{$loop->index}}][sku]" value="{{$item->sku}}" />
      <span class="indicator"></span>
    </label>
  </div>
</div>
