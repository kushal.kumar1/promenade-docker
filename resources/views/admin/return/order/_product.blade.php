@php
$item = $items->first();
$product = $item->product;
$isNonReturnable = $product->tags->where('id', config('settings.non_returnable_tag_id'))->count();
@endphp
@if(!$isNonReturnable)
  @php
  $uom = $product->uom;
  $orderVariant = $item->productVariant;
  $variantQty = $orderVariant->quantity;
  $isVisible = false;
  $lastVariantQty = 0;
  @endphp
  <div class="flex gutter item margin-t-20 variant" data-sku="{{$sku}}" data-shipment="{{$shipment->id}}">
    <div class="col-1-12">
      <img class="bd" src="{{Image::getSrc($product->primaryImage, 60)}}" width="60" />
    </div>
    <div class="col-8-12">
      <h3>{{$product->name}}</h3>
      <div class="margin-t-5 small">
         SKU: {{$item->sku}} ({{$orderVariant->quantity.' '.$product->uom}}) | Ordered Qty:{{$items->count()}} (Total: {{$orderVariant->quantity*$items->count().' '.$product->uom}})
      </div>
    </div>
    <div class="col-2-12 text-right">
        <button type="submit" class="btn btn-primary add-item">Add</button>
    </div>
    <div class="col-12-12 padding-0">
      <div class="flex">
        @include('admin.return.common._quantity-input', ['class' => 'col-3-12', 'pre'=> ''])
        <div class="col-3-12 padding-10">
          <div class="form-group">
            <label>Reason</label>
            <select class="form-control reason" name="reason">
              <option value="">-- Select --</option>
              @foreach(config('settings.return_reasons') as $reason)
                <option value="{{$reason}}">
                  {{$reason}}
                </option>
              @endforeach
            </select>
          </div>
        </div>

      </div>
    </div>
  </div>
@endif