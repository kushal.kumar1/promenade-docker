@extends('admin.master')

@section('css')
<style>
    .variant {
        height: 100%;
        cursor: pointer;
        padding: 5px 15px;
        border-radius: 5px;
        border: 1px dashed #b4b4b4;
        transition: background 0.8s;
    }

    .variant-disabled {
      height: 100%;
      cursor: pointer;
      padding: 5px 15px;
      border-radius: 5px;
      border: 1px dashed #f3c7c7;
      color: #b4b4b4;
    }

    .variant:hover {
        border: 1px solid #d4d4d4;
        background: #fafafa radial-gradient(circle, transparent 1%, #fafafa 1%) center/15000%;
    }

    .variant:active {
        background-size: 100%;
        transition: background 0s;
        background-color: #e5e5e5;
    }

    .quantity.form-control {
        width: 70px;
        padding: 5px;
        text-align: center;
    }

    .cart-overlay {
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        position: absolute;
        background-color: rgba(255, 255, 255, 0.6);
    }

    .cart-overlay .icon-loading {
        top: 50%;
        left: 50%;
        position: absolute;
        transform: translate(-50%, -50%);
    }

    .item-error {
      color: #f55753;
      font-size: 14px;
      font-weight: bold;
      border: 1px dashed #fb9492;
      padding: 5px 10px;
    }
</style>
@append

@section('content')
<div class="container">
    <div class="regular light-grey">
        <a href="{{route('admin::stores.profile', ['id' => $store->id])}}">
            <i class="fa fa-angle-left" aria-hidden="true"></i> {{$store->name}}
        </a>
    </div>
    <div class="margin-t-20">
        <div class="flex gutter no-wrap">
            <div class="col-6-12">
                <div class="card">
                    @include('admin.cart._store_info')
                    <hr class="hr hr-light margin-t-20" />
                    <div class="margin-t-20">
                        <div class="flex gutter">
                            <div class="col-3-12">
                              <select class="search-basis form-control" name="search_type">
                                <option value="product">Search By Product</option>
                                <option value="invoice">Search By Invoice</option>
                              </select>
                            </div>
                            <div class="col-9-12">
                              <input id="products-search" type="text" class="form-control" placeholder="Search by product Name, ID or Barcode" />
                            </div>
                        </div>
                    </div>
                    <div id="product-suggestions"></div>
                </div>
            </div>
            <div class="col-6-12">
                <div class="card">
                    <form class="form form-cart" action="{{route('admin::return.orders.store')}}" method="POST"
                        data-destination="{{route('admin::returns.index')}}"
                        autocomplete="off">
                        <input type="hidden" name="store_id" value="{{$store->id}}">
                        <div class="cart relative">
                            @include('admin.return.order._items')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    class ReturnCart {
        constructor(storeId) {
            this.storeId = storeId;
            $('#products-search').on('input', (e) => this.onQueryChanged(e));
            $('#product-suggestions').on('click', '.variant .add-item', (e) => this.addItem(e));
            $('.search-basis').on('change', (e) => this.onSearchBasisChange(e));
            $('.cart').on('click', '.quantity-subtract, .quantity-add', (e) => this.onQuantityChange(e));
            $('.cart').on('change', '.quantity', (e) => this.onQuantityChange(e));
            $('.cart').on('click', '.cart-item .remove', (e) => this.removeItem($(e.currentTarget).closest('.cart-item')));
            $('.form-cart').on('click','.place-order',(e) => $('.form-cart').submit());
        }

        onQueryChanged(e) {
            clearTimeout(this.searchTimer);
            this.searchTimer = setTimeout(() => {
                const term = e.target.value;
                const search_type = $('.search-basis').val();
                $('#product-suggestions').html(`<hr class="hr hr-light margin-t-20"><div class="card margin-t-20 text-center"><div class="loader" style="display: block;"></div><br>Searching for ${search_type} "${term}"</div>`);
                if (term) {
                    if (this.searchRequest) {
                        this.searchRequest.abort();
                    }
                    this.searchRequest = $.ajax('{{route("admin::return.carts.suggestions")}}', {
                        method: 'GET',
                        data: { term: term, store_id: this.storeId, search_type: search_type }
                    });
                    this.searchRequest.done((products) => {
                        $('#product-suggestions').html(products)
                    });
                    this.searchRequest.fail((error) => this.onError(error));
                }
            }, 500);
        }

        onQuantityChange(e) {
            const $cartItem = $(e.currentTarget).closest('.cart-item');
            let quantity = parseInt($cartItem.find('.quantity').val());
            let delay = 0;
            if ($(e.currentTarget).hasClass('quantity-add')) {
                quantity++;
                delay = 200;
            } else if ($(e.currentTarget).hasClass('quantity-subtract')) {
                quantity--;
                delay = 200;
            }

            if (quantity > 0) {
                $cartItem.find('.quantity').val(quantity);
                clearTimeout(this.updateTimer);
                this.updateTimer = setTimeout(() => this.updateItem($cartItem), delay);
            }
        }

        addItem(e) {
            const $variant = $(e.currentTarget).closest('.variant');
            $('.cart-overlay').removeClass('hidden');
            const sku = $variant.data('sku');
            const shipment_id = $variant.data('shipment');
            const unit = $variant.find('.unit').val() ? parseFloat($variant.find('.unit').val()) : 0;
            const outer = $variant.find('.outer').val() ? parseFloat($variant.find('.outer').val()) : 0;
            const caseConst = $variant.find('.case').val() ? parseFloat($variant.find('.case').val()) : 0;
            const reason = $variant.find('.reason').val();
            const request = $.ajax('{{route("admin::return.carts.add-item")}}', {
                method: 'POST',
                data: {
                    sku: sku,
                    shipment_id: shipment_id,
                    unit: unit,
                    outer: outer,
                    case: caseConst,
                    reason: reason,
                    store_id: this.storeId
                }
            });
            request.done((cart) => { $('.cart').html(cart); });
            request.fail((error) => this.onError(error));
        }

        updateItem($cartItem) {
            $('.cart-overlay').removeClass('hidden');
            const id = $cartItem.data('id');
            const reason = $cartItem.data('reason');
            const old = parseInt($cartItem.find('.quantity').data('old'));
            const quantity = parseInt($cartItem.find('.quantity').val());
            const updateRequest = $.ajax('{{route("admin::return.carts.update-item")}}', {
                method: 'POST',
                data: {
                  id: id,
                  units: quantity,
                  reason: reason,
                  store_id: this.storeId
                }
            });
            updateRequest.done((cart) => {$('.cart').html(cart); });
            updateRequest.fail((error) => {$cartItem.find('.quantity').val(old); this.onError(error)});
        }

        removeItem($cartItem) {
            $('.cart-overlay').removeClass('hidden');
            const id = $cartItem.data('id');
            const request = $.ajax('{{route("admin::return.carts.remove-item")}}', {
                method: 'POST',
                data: {
                    id: id,
                    store_id: this.storeId
                }
            });
            request.done((cart) => {$('.cart').html(cart); this.initSearchAgents();});
            request.fail((error) => this.onError(error));
        }

        onError(error){
            $('.cart-overlay').addClass('hidden');
            let message = error.responseJSON.message;
            if (message == undefined) {
                message = "An unexpected error has occurred";
            }
            const toast = new Toast(message);
            toast.show();
        }

        onSearchBasisChange(e) {
          $('#products-search').val('');
          const search_type = $('.search-basis').val();
          if (search_type == 'product') {
            $('#products-search').attr('placeholder', 'Search by product Name, ID or Barcode');
          } else {
            $('#products-search').attr('placeholder', 'Search by invoice reference');
          }
        }
    }

    new ReturnCart({{$store->id}});
</script>
@append
