@foreach ($shipments as $shipment)
<hr class="hr hr-light margin-t-20" />
<div class="card margin-t-20">
    <div class="flex">
        <div class="col-4-12">
            <strong>Order ID: </strong> {{$shipment->order->reference_id}}
        </div>
        <div class="col-4-12">
            <strong>Invoice ID: </strong> {{$shipment->invoice->reference_id}}
        </div>
        <div class="col-4-12">
            <strong>Invoice Date: </strong> {{\Carbon\Carbon::parse($shipment->invoice->created_at)->format('d-M-Y')}}
        </div>
    </div>
    <div class="flex">
        @foreach ($shipment->orderItemsBySku as $sku => $items)
        @include('admin.return.order._product')
        @endforeach
    </div>
</div>
@endforeach
@if($shipments->count() < 1)
    <hr class="hr hr-light margin-t-20" />
    <div class="card danger margin-t-20 text-center">
        <strong>No shipments found for the searched item.</strong>
    </div>
@endempty
