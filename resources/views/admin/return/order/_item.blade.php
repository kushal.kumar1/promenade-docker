<hr class="hr hr-light margin-t-20" />
<div class="cart-item margin-t-20" data-id="{{$item->id}}" data-sku="{{$item->sku}}" data-shipment="{{$item->shipment_id}}" data-reason="{{$item->reason}}">
    <div class="flex gutter-sm">
        <div class="col-2-12" style="height:70px;">
            <img src="{{Image::getSrc($item->productVariant->product->primaryImage, 80)}}" width="80" />
        </div>
        <div class="col-10-12">
            <div class="flex gutter-sm">
                <div class="col-8-12">
                    <h6 class="regular">{{$item->productVariant->product->name}}</h6>
                    <div class="margin-t-5">
                        <span class="capitalize">({{$item->productVariant->sku}})</span>
                        <span class="capitalize">{{$item->productVariant->value}}</span>
                        <span class="small">[{{$item->productVariant->quantity}} {{$item->productVariant->uom}}]</span>
                    </div>
                    <p class="margin-t-20 warning bold">Qty shown is in units</p>
                    <div class="margin-t-20">
                        <div class="flex gutter-sm">
                            <div class="flex cross-center no-wrap gutter-sm qty">
                                <div>
                                    <button type="button" class="btn btn-sm quantity-subtract">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <input type="number" min="1" class="form-control quantity"
                                    value="{{$item->units}}" data-old="{{$item->units}}"/>
                                <div>
                                    <button type="button" class="btn btn-sm quantity-add">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                            <div>
                                <button type="button" class="btn btn-sm remove">
                                    <i class="fa fa-trash margin-r-5"></i> Remove
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4-12">
                  <div class="margin-t-20">
                    <strong>Reason: </strong> {{$item->reason}}
                    <strong>Units: </strong> {{$item->units}}
                  </div>
                </div>
            </div>
        </div>
        <div class="col-12-12">
          @if(!empty($cartErrors[$item->id]))
            <div>
                <span class="item-error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{$cartErrors[$item->id]}}</span>
            </div>
          @endif
        </div>
    </div>
</div>
