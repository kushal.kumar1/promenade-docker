<div class="cart-overlay hidden">
  <div class="icon-loading">
    <i class="fa fa-2x fa-spinner fa-spin"></i>
  </div>
</div>
<div class="flex gutter-between">
  <h2 class="regular bold">RETURN CART ITEMS</h2>
  <div>
    <span class="small bold">QTY:</span> {{empty($cart) ? 0 : $cart->items->sum('quantity')}} | <span class="small bold">ITEMS:</span> {{empty($cart) ? 0 : $cart->items->unique('sku')->count()}}
  </div>
</div>
@if (!empty($cart) && $cart->items->isNotEmpty())
<div class="cart-items">
  @foreach ($cart->itemsByShipment->sortByDesc('updated_at') as $shipmentID => $items)
    <div class="card margin-t-20">
      <div class="margin-t-20">
        <?php $shipment = $items->first()->shipment; ?>
        <div class="flex">
            <div class="col-4-12">
                <strong>Order ID: </strong> {{$shipment->order->reference_id}}
            </div>
            <div class="col-4-12">
                <strong>Invoice ID: </strong> {{$shipment->invoice->reference_id}}
            </div>
            <div class="col-4-12">
                <strong>Invoice Date: </strong> {{\Carbon\Carbon::parse($shipment->invoice->created_at)->format('d-M-Y')}}
            </div>
        </div>
      </div>
      @foreach ($items as $item)
        @include('admin.return.order._item')
      @endforeach
    </div>
  @endforeach
</div>
<hr class="hr hr-light margin-t-20" />
<div class="margin-t-20 text-right">
  <button type="button" class="btn btn-primary place-order" {{!empty($cartErrors) ? "disabled" : ""}}>Submit Returns</button>
</div>
@else
<p class="margin-t-10">No items in the cart</p>
@endif
