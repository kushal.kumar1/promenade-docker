@extends('admin.master')

@section('content')
<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            Returns
        </div>
        <div>
          @can('create-return')
            <button class="btn btn-primary" data-modal="#modal-stores">Create Return</button>
          @endcan
        </div>
    </div>
{{--    <div class="flex danger  gutter-between padding-25 margin-t-30 bg-primary-dark">--}}
{{--        <strong>DO NOT COMPLETE ANY RETURN ORDER INITIATED BY RAJ KUMAR &amp; MANI</strong>--}}
{{--    </div>--}}

    @include('admin.grid')
</div>

@include('admin.components._stores_return_modal')

@endsection
