@extends('admin.master')

@section('content')
<div class="container">
<div class="regular light-grey">
  <a href="{{route('admin::orders.edit', $shipment->order->id)}}"><i class="fa fa-angle-left" aria-hidden="true"></i> #{{$shipment->order->id}}</a>
</div>
  <h1 class="h1 margin-t-10">Create Return Order</h1>
  <div class="margin-t-30">
    @if($shipment->orderItems->isNotEmpty())
      <form class="form" method="post" action="{{route('admin::returns.store')}}" data-destination="{{route('admin::orders.edit',$shipment->order->id)}}" autocomplete="off">
        <input type="hidden" name="shipment_id"  value="{{$shipment->id}}"/>
        <input type="hidden" name="order_id"  value="{{$shipment->order->id}}"/>
        <div class="flex gutter">
          {{ csrf_field() }}
          <div class="col-12-12">
            <div class="card">
              <h2 class="h5 bold">Items</h2>
              <hr class="hr hr-light margin-t-20" />
              <div class="margin-t-20">
                <div class="flex gutter bold">
                  <div class="col-1-12">
                  </div>
                  <div class="col-4-12">
                    Item Details
                  </div>
                  <div class="col-3-12">
                    Quantity
                  </div>
                  <div class="col-3-12">
                    Reason
                  </div>
                  <div class="col-1-12 text-right">
                    <div class="text-right">
                      <label class="checkbox">
                        <input type="checkbox" class="item-all-checkbox" name="all" value="all" />
                        <span class="indicator"></span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <hr class="hr hr-light margin-t-20" />
              <div class="margin-t-30">
                @foreach ($shipment->orderItemsBySku as $sku => $items)
                @include('admin.return._item')
                @endforeach
              </div>
              <div class="margin-t-40">
                <label class="label">Reason:</label>
                <span class="small margin-t-5">This is reason applied to all items in return order. For different item level reason, choose the reason corressponding to that item.</span>
                <select class="form-control" name="reason">
                    <option value="">-- Select --</option>
                    @foreach(config('settings.return_reasons') as $reason)
                        <option value="{{$reason}}">
                            {{$reason}}
                        </option>
                    @endforeach
                </select>
                <span class="error error-reason margin-t-5"></span>
              </div>
              <div class="margin-t-40">
                <label class="label">Remarks:</label>
                <textarea class="form-control" name="remarks" rows="4"></textarea>
              </div>
              <div class="margin-t-30">
                <div class="text-right">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      @else
      <div class="card">
          <span> No items eligible for return.</span>
      </div>
      @endif
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
  $('.item-checkbox').on('click', function() {
    $input = $(this).closest('.item').find('.quantity');
    $reason = $(this).closest('.item').find('.reason');
    $input.attr('disabled', !this.checked);
    $reason.attr('disabled', !this.checked);

  });

  $('.item-all-checkbox').on('click', function() {
    let sel = this.checked;
    $('.item-checkbox').each(function() {
      this.checked = sel;
      $input = $(this).closest('.item').find('.quantity');
      $input.attr('disabled', !this.checked);
    });
  });
</script>
@endsection
