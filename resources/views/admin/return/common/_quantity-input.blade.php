{{--@if($uom !== 'pc')--}}
{{--    <div class="col-6-12 padding-10">--}}
{{--        <div class="form-group">--}}
{{--            <label>Return qty in {{$uom}}</label>--}}
{{--            <input class="form-control unit" name="unit" type="number" step="any" min="0" max="{{$items->count()*$orderVariant->quantity}}" value="">--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@else--}}
    @foreach($product->allVariants->sortByDesc('quantity') as $variant)
        @php
            if($variant->sku == $orderVariant->sku) {
                $max = 0;
                foreach ($items as $item) {
                    if($item->status !== \Niyotail\Models\OrderItem::STATUS_PARTIAL_RETURNED) {
                        $max ++;
                    }
                }

                $isVisible = true;
            }
            else
            {
                if($variant->value == "unit" && $item->status == \Niyotail\Models\OrderItem::STATUS_PARTIAL_RETURNED) {
                    $max = $lastVariantQty - $item->returnOrderItems->sum('units');
                }
                else {
                  $max = ($lastVariantQty/$variant->quantity) - 1;
                }
            }

            $lastVariantQty = $variant->quantity;

            $name = $variant->value;
            if(!empty($pre))
            {
                $name = $pre.'['.$variant->value.']';
            }

            if($max < 1) continue;

        @endphp
        @if($isVisible)
            <div class="{{$class}} padding-10">
                <div class="form-group">
                    <label>{{$variant->value}}</label>
                    <input class="form-control {{$variant->value}}" name="{{$name}}" type="number" min="0" max="{{$max}}" value="">
                </div>
            </div>
        @endif
    @endforeach
{{--@endif--}}