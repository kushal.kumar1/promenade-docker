@extends('admin.master')

@section('content')
    <div class="container">

        <div class="flex gutter-between">
            <a href="" class="btn btn-sm btn-primary">Back to order</a>
        </div>
        <div class="h1 margin-t-10">
            Return #{{$returnOrder->id}}
        </div>

        <div class="flex danger  gutter-between padding-25 margin-t-30 bg-primary-dark">
            <strong>DO NOT COMPLETE ANY RETURN ORDER INITIATED BY RAJ KUMAR &amp; MANI</strong>
        </div>

        <div class="card margin-t-30">
            <h3 class="h3">
                Items
            </h3>
            <div class="col-12 padding-v-10">
                <hr class="hr hr-light">
            </div>
            <form class="form" method="POST"
                  action="{{route('admin::returns.mark-complete', $returnOrder->id)}}"
                  data-destination="{{route('admin::orders.edit', $returnOrder->order_id)}}">
                @csrf
                <input name="id" value="{{$returnOrder->id}}" type="hidden">
                <div class="flex gutter-xsm bold">
                    <div class="col-1-12"></div>
                    <div class="col-1-12">
                        Barcode
                    </div>
                    <div class="col-3-12">Product</div>
                    <div class="col-1-12">Ordered SKU</div>
                    <div class="col-2-12">
                        Total Units
                    </div>
                    <div class="col-4-12 text-center">Reason</div>
                </div>
                <div class="col-12 padding-v-10">
                    <hr class="hr hr-light">
                </div>
                @foreach($groupedItems as $sku=>$items)

                    @php
                        $totalReturned = 0;
                        foreach ($items as $item) {
                            $orderItem = $item->orderItem;
                            $returnItems = $orderItem->completedReturnOrderItems->where('return_order_id', '!=', $returnOrder->id);
                            $totalUnitsReturned = $returnItems->sum('units');
                            $totalReturned += $totalUnitsReturned;
                        }
                    @endphp

                    <div class="flex gutter-xsm">
                        <div class="col-1-12">{{$loop->index + 1}}</div>
                        <div class="col-1-12">
                            {{$items->first()->orderItem->product->barcode}}
                        </div>
                        <div class="col-3-12">{{$items->first()->orderItem->product->name}}</div>
                        <div class="col-1-12">{{$items->first()->orderItem->sku}}</div>

                        <div class="col-2-12">
                            <input type="hidden" name="return_item[{{$loop->index}}][sku]" value="{{$sku}}">
                            <input type="number" name="return_item[{{$loop->index}}][quantity]"
                                   class="form-control"
                                   max="{{$items->first()->orderItem->productVariant->quantity*$items->count() - $totalReturned}}"
                                   value="{{$items->sum('units')}}">
                        </div>
                        <div class="col-4-12 text-center">
                            <div class="form-group">
                                <select class="form-control reason" name="return_item[{{$loop->index}}][reason]">
                                    <option value="">-- Select --</option>
                                    @foreach(config('settings.return_reasons') as $reason)
                                        <option @if($reason == $items->first()->reason) selected @endif value="{{$reason}}">
                                            {{$reason}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12-12 padding-v-10">
                        <hr class="hr hr-light">
                    </div>
                @endforeach
                <div class="flex gutter-between">
                    <button type="submit" class="btn btn-primary">Complete</button>
                </div>
            </form>
        </div>
    </div>
@endsection
