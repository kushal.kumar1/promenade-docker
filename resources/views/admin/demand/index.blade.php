@extends('admin.master')

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Auto replenish demand
            </div>

            <div>
                <!-- #TODO: Check permission if needed -->
                <a class="btn btn-primary" href="{{route('admin::demand-generation.generateDemand')}}">
                    <i class="fa fa-refresh"></i> Generate demand</a>
                <button disabled class="btn btn-primary" id="create-purchase-order" data-modal="#modal-vendor-search">Create Purchase Order</button>
            </div>
        </div>
        <div id="gridHolder">
            @include('admin.grid')
        </div>
    </div>

    <div id="modal-vendor-search" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal">X</span>

            <h1 class="h5">Choose Vendor</h1>
            
            <div class="margin-t-20">
                <label class="label">Select A Vendor</label>
                <select id="select2-vendors" class="form-control" style="width:100%;"></select>
                <button id="button-submit" class="btn btn-primary margin-t-20">Create</button>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script type="text/javascript">
        class DemandList {
            constructor() {

                $('body').on('DOMSubtreeModified', '#gridHolder', function(){
                    $('input[name="demands[]"]').on('change', () => {
                        if (getSelectedCheckbox().length > 0) {
                            $('#create-purchase-order').prop('disabled', false);
                        } else $('#create-purchase-order').prop('disabled', true);

                        function getSelectedCheckbox() {
                            return $("input[name='demands[]']:checked").map(function () {
                                return this.value;
                            }).get();
                        }
                    });
                });

                this.initSelect2();

                $('input[name="demands[]"]').on('change', () => {
                    if (getSelectedCheckbox().length > 0) {
                        $('#create-purchase-order').prop('disabled', false);
                    } else $('#create-purchase-order').prop('disabled', true);

                    function getSelectedCheckbox() {
                        return $("input[name='demands[]']:checked").map(function () {
                            return this.value;
                        }).get();
                    }
                });

                $('#button-submit').on('click', () => {
                    $('#button-submit').prop('disabled', true)
                    let demand_item_ids = $("input[name='demands[]']:checked").map(function () {
                        return this.value;
                    }).get();

                    let vendors = $('#select2-vendors').select2('data');

                    if (!demand_item_ids || demand_item_ids.length === 0) {
                        alert('Please select atleast 1 order.');
                        // - Close Modal.
                        return false;
                    }

                    if (!vendors || vendors.length === 0) {
                        alert('Please search and select appropriate vendor.');
                        return false;
                    }

                    $.ajax({
                        type: 'POST',
                        url: "{{route('admin::purchase-order.submit')}}",
                        data: {
                            demand_item_ids,
                            vendor_id: vendors[0].id
                        },
                        // dataType: "json",
                        success: (response) => {
                            var toast = new Toast('Item updated successfully.');
                            toast.show();
                            console.log(response);
                            location.href = '/purchase-order/'+response.id;
                        },
                        error: (error) => {
                            $('#button-submit').prop('disabled', false);
                            showAllErrorsInToast(error);
                        }
                    });
                });
            }

            handleDemandSelect() {

            }

            initSelect2() {
                $('#select2-vendors').select2({
                    minimumInputLength: 2,
                    ajax: {
                        url: '{{route("admin::search.vendors")}}',
                        dataType: 'json',
                        processResults: (data) => {
                            return {
                                results: data
                            };
                        }
                    },
                    templateResult: (vendor) => vendor.name,
                    templateSelection: (vendor) => vendor.name,
                });
            }
        }
        new DemandList();
    </script>
@endsection
