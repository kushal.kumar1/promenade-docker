@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                {{ucwords(str_replace("_"," ",$modificationRequestType))}}
            </div>
            <div>
                <button id="create-new-request-btn" class="btn btn-primary" data-modal="#create-modification-request">
                    Create New Request
                </button>
            </div>
        </div>
        <div id="create-modification-request" class="modal" tabindex="-1" role="form">
            <div id="modal-content" class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center">Create New Request</h5>
                    <span class="close" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </span>
                </div>
                @switch($modificationRequestType)
                    @case(\Niyotail\Models\ModificationRequest::TYPE_TRANSACTION)
                    <form id="delete-modification-request-form"
                          class="form" method="post" autocomplete="off"
                          action="{{route(
                                "admin::modification-requests.transaction.delete",
                                ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_TRANSACTION]
                            )}}">
                        {{csrf_field()}}
                        <input type="hidden" id="delete-request-id" value="" name="modificationRequestId" required>
                    </form>
                    <form id="create-modification-request-form"
                          class="form" method="post"
                          action="{{route(
                                "admin::modification-requests.transaction.create",
                                ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_TRANSACTION]
                            )}}"
                          autocomplete="off">
                        {{csrf_field()}}
                        @include("admin.modification_request.transaction_form")
                    </form>
                    @break
                    @case(\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE)
                    <form id="delete-modification-request-form"
                          class="form" method="post" autocomplete="off"
                          action="{{route(
                                "admin::modification-requests.purchase-invoice.delete",
                                ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE]
                            )}}">
                        {{csrf_field()}}
                        <input type="hidden" id="delete-request-id" value="" name="modificationRequestId" required>
                    </form>
                    <form id="create-modification-request-form"
                          class="form" method="post"
                          action="{{route(
                                "admin::modification-requests.purchase-invoice.create",
                                ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE]
                            )}}"
                          autocomplete="off">
                        {{csrf_field()}}
                        @include("admin.modification_request.purchase_invoice_form")
                    </form>
                    @break
                    @case(\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM)
                    <form id="delete-modification-request-form"
                          class="form" method="post" autocomplete="off"
                          action="{{route(
                                "admin::modification-requests.purchase-invoice-item.delete",
                                ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM]
                            )}}">
                        {{csrf_field()}}
                        <input type="hidden" id="delete-request-id" value="" name="modificationRequestId" required>
                    </form>
                    <form id="create-modification-request-form"
                          class="form" method="post"
                          action="{{route(
                                    "admin::modification-requests.purchase-invoice-item.create",
                                    ["modificationRequestType"=>\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM]
                                )}}"
                          autocomplete="off">
                        {{csrf_field()}}
                        @include("admin.modification_request.purchase_invoice_item_form")
                    </form>
                    @break
                @endswitch
            </div>
        </div>
        @include('admin.grid')
    </div>

@endsection

@section('js')
    <script>
        $(function() {
            if({{(int)empty($deeplink)}} !== 1) {
                modificationFormDeeplink();
            }
        });

        $('#create-new-request-btn').click(function (e) {
            modificationFormReset();
        });
    </script>
@append