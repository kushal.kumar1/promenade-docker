<div class="modal-body margin-t-30">
    <div class="flex gutter">
        <div class="margin-t-20" style="width:100%;">
            <label class="label">Transaction Id</label>
            <select id="select2-transaction-id" class="form-control" name="transaction_id" style="width:100%;" required></select>
        </div>

        <div class="col-12-12">
            <label class="label">Reason for Deleting</label>
            <input class="form-control" name="delete_reason" value="" required/>
        </div>
        <div class="col-12-12">
            <label class="label">Employee</label>
            <input type="hidden" class="form-control" name="employee_id" value="{{auth()->user()->id}}"
                   readonly/>
            <input class="form-control" name="employee_name"
                   value='{{strval(auth()->user()->id)." (".auth()->user()->name.")" }}' readonly/>
            <span class="error error-type"></span>
        </div>
    </div>
</div>

<div class="modal-footer flex gutter-between margin-t-40">
    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
    <button class="btn btn-primary button-success" id="button-save">Save</button>
</div>


@section('js')
    <script>

        let select2_transaction_id = $('#select2-transaction-id');

        function modificationFormReset() {
            $('#create-modification-request-form').trigger("reset");
            select2_transaction_id.val('').trigger('change');
        }


        function modificationFormDeeplink() {
            console.log('deeplinking here');
        }

        select2_transaction_id.select2({
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.transactions")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {
                        results: data
                    };
                }
            },
            templateResult: function (transaction) {
                if (!transaction || (transaction.text && transaction.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${transaction.transaction_id}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (transaction) {
                $(transaction.element).attr('id', transaction.transaction_id);
                $(transaction.element).attr('transaction_id', transaction.transaction_id);
                $(transaction.element).attr('value', transaction.transaction_id);
                return transaction.transaction_id
            },

        });
    </script>
@append