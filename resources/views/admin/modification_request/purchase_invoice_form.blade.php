<div class="margin-t-30">
    <div class="flex gutter">
        <div class="margin-t-20" style="width:100%;">
            <label class="label">Purchase Invoice Id</label>
            <select id="select2-purchase-invoice-id" class="form-control" name="purchase_invoice_id"
                    style="width:100%;" required></select>
        </div>

        {{-- edit or delete option --}}
        {{--        <div class="col-12-12 flex gutter-between" >--}}
        {{--            <button id="edit-purchase-invoice-btn" class="btn btn-primary" disabled>Edit</button>--}}
        {{--            <button id="delete-purchase-invoice-btn" class="btn btn-primary" style="display:none;" disabled>Delete</button>--}}
        {{--        </div>--}}

        <div>
            <input type="hidden" class="form-control" name="action_type" id="action-input" value="" hidden/>
        </div>

        {{--        delete reason--}}
        {{--        <div id="delete-reason" class="collapse col-12-12" style="display:none">--}}
        {{--            <div class="col-12-12">--}}
        {{--                <label class="label">Delete Reason</label>--}}
        {{--                <input type="text" id="input-delete-reason" class="form-control required" name="delete_reason"--}}
        {{--                       style="width:100%;"/>--}}
        {{--            </div>--}}
        {{--        </div>--}}

        {{--        edit div--}}
        <div id="edit-purchase-invoice" class="collapse col-12-12">
            <div class="col-12-12">
                <label class="label">Vendor Id</label>
                <select id="select2-vendor-id" class="form-control required" name="vendor_id"
                        style="width:100%;">

                </select>
            </div>
            <div class="col-12-12 margin-t-10">
                <label class="label">Vendor Address Id</label>
                <select id="select2-vendor-address-id" class="form-control" name="vendor_address_id" style="width:100%;"
                ></select>
            </div>
            <div class="col-12-12  margin-t-10">
                <label class="label">Date</label>
                <input type="date" id="pi-date" max="{{\Carbon\Carbon::today()->format('Y-m-d')}}"
                       name="date" class="form-control"
                       value=""/>
            </div>
            <input type="hidden" class="form-control" name="image" id="uploaded-image-name" value="" hidden
                   style="display: none"/>
            <input type="file" name="image_upload" accept="image/*,application/pdf" class="form-control"
                   id="image-selector" style="display: none">
            <div class="col-12-12  margin-t-10">
                <label class="label">Image</label>
                <div class="col-12-12 flex gutter-between">
                    <button class="btn btn-primary" id="select-image-btn">Select</button>
                    <input id="selected-image-name" class="form-control" value="" readonly
                           style="max-width:50%;"/>
                    <button class="btn btn-primary" id="upload-image-btn" disabled>Upload</button>
                </div>
            </div>
            <div class="col-12-12  margin-t-10">
                <label class="label">Platform</label>
                <select id="select2-platform-id" class="form-control" name="platform_id" style="width:100%;"></select>
            </div>
            <div class="col-12-12  margin-t-10">
                <label class="label">EWAY Number</label>
                <input type="text" class="form-control" name="eway_number" style="width:100%;"/>
            </div>
            <div class="col-12-12  margin-t-10">
                <label class="label">Forecast Payment Date</label>
                <input type="date" name="forecast_payment_date" class="form-control"
                       id="input-forecast-payment-date" value=""/>
            </div>
        </div>

        <div class="col-12-12 ">
            <label class="label">Employee</label>
            <input type="hidden" class="form-control" name="employee_id" value="{{auth()->user()->id}}"
                   readonly/>
            <input class="form-control" name="employee_name"
                   value='{{strval(auth()->user()->id)." (".auth()->user()->name.")" }}' readonly/>
            <span class="error error-type"></span>
        </div>
    </div>
</div>

<div class="modal-footer flex gutter-between margin-t-40">
    <button id="button-cancel" class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
    <button id="button-save" class="btn btn-primary button-success">Save</button>
</div>


@section('js')
    <script>
        // let editButton = $('#edit-purchase-invoice-btn');
        // let deleteButton = $('#delete-purchase-invoice-btn');
        let saveButton = $('#button-save');

        let select2_purchase_invoice_id = $('#select2-purchase-invoice-id');
        let select2_vendor_id = $('#select2-vendor-id');
        let select2_vendor_address_id = $('#select2-vendor-address-id');
        let select2_platform_id = $('#select2-platform-id');
        let input_date = $('#pi-date');
        let forecast_payment_date = $('#input-forecast-payment-date');

        let imageSelector = $('#image-selector');
        let selectedImageName = $('#selected-image-name');
        let uploadedImageName = $('#uploaded-image-name');
        let selectImageBtn = $('#select-image-btn');
        let uploadImageBtn = $('#upload-image-btn');

        let editPurchaseInvoiceDiv = $('#edit-purchase-invoice');
        // let deleteReasonDiv = $('#delete-reason');
        // let inputDeleteReason = $('#input-delete-reason');

        function modificationFormReset() {
            $('#create-modification-request-form').trigger("reset");

            // editButton.prop('disabled', true);
            // deleteButton.prop('disabled', true);
            saveButton.prop('disabled', false);
            saveButton.text('Save');

            select2_purchase_invoice_id.prop('disabled', false);
            select2_purchase_invoice_id.val('').trigger('change');
            select2_vendor_id.val('').trigger('change');
            select2_vendor_address_id.val('').trigger('change');
            select2_platform_id.val('').trigger('change');
            selectImageBtn.prop('disabled', false);
            selectImageBtn.text('Select')
            uploadImageBtn.prop('disabled', true);
            uploadImageBtn.text('Upload');
            selectedImageName.val('');

            $('#action-input').val("edit");

            // deleteReasonDiv.css("display", "none");
            // editPurchaseInvoiceDiv.css("display", "none");
        }

        function modificationFormDeeplink() {
            console.log('deeplinking purchaseInvoice');
            const purchaseInvoice = {!! empty($purchaseInvoice)? "{}" : $purchaseInvoice->toJson() !!};
            $('#create-new-request-btn').trigger('click');

            let purchaseInvoiceOption = new Option(
                purchaseInvoice.id + " (" + purchaseInvoice.vendor_ref_id + ")",
                purchaseInvoice.id,
                true, true
            )

            purchaseInvoiceOption.purchaseInvoice = purchaseInvoice;
            select2_purchase_invoice_id.append(purchaseInvoiceOption).trigger('change');
            select2_purchase_invoice_id.prop('disabled', true);



            //  TODO
            let option = new Option(purchaseInvoice.vendor.name, purchaseInvoice.vendor_id, false, true);
            select2_vendor_id.append(option).trigger('change');
            let optionVa = new Option(
                purchaseInvoice.vendor_address.address + ' - ' + purchaseInvoice.vendor_address.name,
                purchaseInvoice.vendor_address_id,
                false, true);
            select2_vendor_address_id.append(optionVa).trigger('change');
            input_date.val(purchaseInvoice.invoice_date);

            if (purchaseInvoice.platform_id) {
                let optionPlatform = new Option(purchaseInvoice.platform.platform, purchaseInvoice.platform_id, false, true);
                select2_platform_id.append(optionPlatform).trigger('change');
            }

            // editButton.prop('disabled', false);


        }

        const Upload = function (file) {
            this.file = file;
        };

        Upload.prototype.getType = function () {
            return this.file.type;
        };
        Upload.prototype.getSize = function () {
            return this.file.size;
        };
        Upload.prototype.getName = function () {
            return this.file.name;
        };
        Upload.prototype.doUpload = function () {
            var that = this;
            var formData = new FormData();

            formData.append("upload_image", this.file, this.getName());

            $.ajax({
                type: "POST",
                url: `{{
                        route('admin::modification-requests.purchase-invoice.upload-invoice', [
                            'modificationRequestType' => \Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE
                        ])}}`,
                enctype: 'multipart/form-data',
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', that.progressHandling, false);
                    }
                    return myXhr;
                },
                success: function (data) {
                    uploadedImageName.val(data);

                    uploadImageBtn.text("Remove");
                    uploadImageBtn.prop('btn-action', 'delete');
                    uploadImageBtn.prop('disabled', false);

                    saveButton.prop('disabled', false);
                },
                error: function (error) {
                    uploadedImageName.val('');
                    console.log(error);
                    alert("Image Upload failed, try again");

                    uploadImageBtn.prop('disabled', false);
                },
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                timeout: 60000
            });
        };

        // editButton.click(function (e) {
        //     e.preventDefault();
        //     e.stopPropagation();
        //
        //     $('#action-input').val("edit");
        //     saveButton.text('Save Changes');
        //
        //     select2_vendor_id.prop('required', true);
        //     select2_vendor_address_id.prop('required', true);
        //     input_date.prop('required', true);
        //     // input_image.prop('required', true);
        //     // forecast_payment_date.prop('required', true);
        //
        //     inputDeleteReason.prop('required', false);
        //
        //     editButton.prop('disabled', true);
        //     // deleteButton.prop('disabled', false);
        //     saveButton.prop('disabled', false);
        //
        //     if (deleteReasonDiv.is(':visible')) {
        //         deleteReasonDiv.toggle();
        //     }
        //     if (!editPurchaseInvoiceDiv.is(':visible')) {
        //         editPurchaseInvoiceDiv.toggle();
        //     }
        // });
        // deleteButton.click(function (e) {
        //     e.preventDefault();
        //     e.stopPropagation();
        //
        //     $('#action-input').val("delete");
        //     saveButton.text('Delete Purchase Invoice');
        //
        //     select2_vendor_id.prop('required', false);
        //     select2_vendor_address_id.prop('required', false);
        //     input_date.prop('required', false);
        //     // input_image.prop('required', false);
        //     forecast_payment_date.prop('required', false);
        //
        //     inputDeleteReason.prop('required', true);
        //
        //     editButton.prop('disabled', false);
        //     deleteButton.prop('disabled', true);
        //     saveButton.prop('disabled', false);
        //
        //     if (editPurchaseInvoiceDiv.is(':visible')) {
        //         editPurchaseInvoiceDiv.toggle();
        //     }
        //     if (!deleteReasonDiv.is(':visible')) {
        //         deleteReasonDiv.toggle();
        //     }
        // });

        selectImageBtn.click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            imageSelector.trigger('click');
        });

        uploadImageBtn.click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            uploadImageBtn.prop('disabled', false);
            if (uploadImageBtn.prop('btn-action') === 'upload') {
                new Upload(imageSelector[0].files[0]).doUpload();
                uploadImageBtn.prop('disabled', true);
            }
            if (uploadImageBtn.prop('btn-action') === 'delete') {
                selectedImageName.val('');
                uploadImageBtn.text('Upload')
                uploadImageBtn.prop('disabled', true);
                selectImageBtn.text('Select')

                saveButton.prop('disabled', false);
            }
        })

        imageSelector.on('change', function (e) {
            selectedImageName.val(imageSelector[0].files[0].name);
            uploadImageBtn.prop('btn-action', 'upload');
            uploadImageBtn.prop('disabled', false);
            saveButton.prop('disabled', true);
        })

        select2_purchase_invoice_id.select2({
            minimumInputLength: 1,
            ajax: {
                url: '{{route("admin::search.purchase_invoices")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (purchaseInvoice) {
                if(!purchaseInvoice  || (purchaseInvoice.text && purchaseInvoice.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${purchaseInvoice.id} (${purchaseInvoice.vendor_ref_id})</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (purchaseInvoice) {
                if (purchaseInvoice.element && purchaseInvoice.element.purchaseInvoice) {
                    $(purchaseInvoice.element).attr('id', purchaseInvoice.element.purchaseInvoice.id);
                    $(purchaseInvoice.element).attr('value', purchaseInvoice.element.purchaseInvoice.id);

                    return `${purchaseInvoice.element.purchaseInvoice.id} (${purchaseInvoice.element.purchaseInvoice.vendor_ref_id})`;
                }

                $(purchaseInvoice.element).attr('id', purchaseInvoice.id);
                $(purchaseInvoice.element).attr('value', purchaseInvoice.id);

                return `${purchaseInvoice.id} (${purchaseInvoice.vendor_ref_id})`;
            },
        });

        select2_vendor_id.select2({
            minimumInputLength: 1,
            ajax: {
                url: '{{route("admin::search.vendors")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (vendor) {
                if(!vendor || (vendor.text && vendor.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${vendor.id} | ${vendor.name} (${vendor.type})</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (vendor) {
                console.log(1)
                console.log(vendor)

                $(vendor.element).attr('id', vendor.id);
                $(vendor.element).attr('vendor_id', vendor.id);
                $(vendor.element).attr('value', vendor.id);

                if (vendor.text) {
                    return `${vendor.id} | ${vendor.text}`;
                } else {
                    return `${vendor.id} | ${vendor.name} (${vendor.type})`;
                }
            }
        });

        select2_platform_id.select2({
            minimumInputLength: 0,
            ajax: {
                url: '{{route("admin::search.platforms")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (platform) {
                if(!platform  || (platform.text && platform.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${platform.id} | ${platform.platform}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (platform) {
                $(platform.element).attr('id', platform.id);
                $(platform.element).attr('platform_id', platform.id);
                $(platform.element).attr('value', platform.id);

                if (platform.text) {
                    return `${platform.id} | ${platform.text}`;
                } else {
                    return `${platform.id} | ${platform.platform}`;
                }
            }
        });

        select2_vendor_address_id.select2({
            minimumInputLength: 0,
            ajax: {
                url: function () {
                    const vendor_id = select2_vendor_id.find(':selected')[0].getAttribute('vendor_id');
                    const url = new URL('{{route("admin::search.vendor_addresses")}}');
                    url.searchParams.set('vendor_id', vendor_id);
                    return url.toString();
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                },
            },
            templateResult: function (vendorAddress) {
                if(!vendorAddress  || (vendorAddress.text && vendorAddress.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${vendorAddress.id} | ${vendorAddress.address}</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (vendorAddress) {
                $(vendorAddress.element).attr('id', vendorAddress.id);
                $(vendorAddress.element).attr('vendor_address_id', vendorAddress.id);
                $(vendorAddress.element).attr('value', vendorAddress.id);

                if (vendorAddress.text) {
                    return `${vendorAddress.id} | ${vendorAddress.text}`;
                } else {
                    return `${vendorAddress.id} | ${vendorAddress.address}`;
                }
            }
        });

        select2_purchase_invoice_id.on('select2:select', function (e) {
            const purchaseInvoice = e.params.data;
            console.log('here')

            let option = new Option(purchaseInvoice.vendor.name, purchaseInvoice.vendor_id, false, true);
            select2_vendor_id.append(option).trigger('change');
            let optionVa = new Option(
                purchaseInvoice.vendor_address.address + ' - ' + purchaseInvoice.vendor_address.name,
                purchaseInvoice.vendor_address_id,
                false, true);
            select2_vendor_address_id.append(optionVa).trigger('change');
            input_date.val(purchaseInvoice.invoice_date);

            if (purchaseInvoice.platform_id) {
                let optionPlatform = new Option(purchaseInvoice.platform.platform, purchaseInvoice.platform_id, false, true);
                select2_platform_id.append(optionPlatform).trigger('change');
            }

            // editButton.prop('disabled', false);
            // deleteButton.prop('disabled', false);
        });

        select2_vendor_id.on('select2:select', function (e) {
            select2_vendor_address_id.val('').trigger('change');
            select2_vendor_address_id.select2('open');
        });
    </script>
@append