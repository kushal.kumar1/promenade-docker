<div class="modal-body margin-t-30">
    <div class="flex gutter">
        <div class="margin-t-20" style="width:100%;">
            <label class="label">Purchase Invoice Id</label>
            <select id="select2-purchase-invoice-id" name="purchase_invoice_id" class="form-control"
                    style="width:100%;" required></select>
        </div>
        <div id="purchase-invoice-item" class="margin-t-20" style="width:100%;">
            <label class="label">Purchase Invoice Item Id</label>
            <select id="select2-purchase-invoice-item-id" name="purchase_invoice_item_id" class="form-control"
                    style="width:100%;" required disabled></select>
        </div>

        {{--                edit or delete option--}}
        <div class="col-12-12 flex gutter-between">
            <button id="edit-purchase-invoice-item-btn" class="btn btn-primary" disabled>Edit</button>
            <button id="delete-purchase-invoice-item-btn" class="btn btn-primary" disabled>Delete</button>
        </div>

        <div>
            <input type="hidden" class="form-control" name="action_type" id="action-input" value="" hidden/>
        </div>

        {{--        delete reason--}}
        <div id="delete-reason" class="collapse col-12-12" style="display:none">
            <div class="col-12-12">
                <label class="label">Delete Reason</label>
                <input type="text" id="input-delete-reason" class="form-control required" name="delete_reason"
                       style="width:100%;"/>
            </div>
        </div>

        {{--                edit div--}}
        <div id="edit-purchase-invoice-item" class="collapse col-12-12" style="display:none">
            <div class="flex gutter">
                <div class="col-6-12  margin-t-10" disabled>
                    <div class="col-12-12">
                        <label class="label">Current Values</label>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Product Id</label>
                        <input id="old-product-id" class="form-control" value="" disabled/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Quantity</label>
                        <input id="old-quantity" class="form-control" value="" disabled/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Base Cost</label>
                        <input id="old-base-cost" class="form-control" value="" disabled/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Scheme Discount</label>
                        <input id="old-scheme-discount" class="form-control" value="" disabled/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Other Discount</label>
                        <input id="old-other-discount" class="form-control" value="" disabled/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Post Tax Discount</label>
                        <input id="old-post-tax-discount" class="form-control" value="" disabled/>
                    </div>
                </div>

                <div class="col-6-12  margin-t-10">
                    <div class="col-12-12">
                        <label class="label">New Values</label>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Product Id</label>
                        <select id="select2-product-id" name="product_id" class="form-control"
                                style="width:100%;"></select>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Quantity</label>
                        <input id="input-quantity" class="form-control" name="quantity" value="" required/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Base Cost</label>
                        <input id="input-base-cost" class="form-control" name="base_cost" value="" required/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Scheme Discount</label>
                        <input id="input-scheme-discount" class="form-control" name="scheme_discount" value=""
                               required/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Other Discount</label>
                        <input id="input-other-discount" class="form-control" name="other_discount" value="" required/>
                    </div>
                    <div class="col-12-12  margin-t-10">
                        <label class="label">Post Tax Discount</label>
                        <input id="input-post-tax-discount" class="form-control" name="post_tax_discount" value=""
                               required/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12-12">
            <label class="label">Employee</label>
            <input type="hidden" class="form-control" name="employee_id" value="{{auth()->user()->id}}"
                   readonly/>
            <input class="form-control" name="employee_name"
                   value='{{strval(auth()->user()->id)." (".auth()->user()->name.")" }}' readonly/>
            <span class="error error-type"></span>
        </div>
    </div>
</div>

<div class="modal-footer flex gutter-between margin-t-40">
    <button class="btn btn-secondary" data-dismiss="modal" type="button">Cancel</button>
    <button id="button-save" class="btn btn-primary button-success" disabled>Save</button>
</div>




@section('js')
    <script>
        let editButton = $('#edit-purchase-invoice-item-btn');
        let deleteButton = $('#delete-purchase-invoice-item-btn');
        let saveButton = $('#button-save');

        let purchaseInvoiceItemDiv = $('#purchase-invoice-item');

        let select2_purchase_invoice_id = $('#select2-purchase-invoice-id');
        let select2_purchase_invoice_item_id = $('#select2-purchase-invoice-item-id');
        let select2_product_id = $('#select2-product-id');


        let deleteReasonDiv = $('#delete-reason');
        let inputDeleteReason = $('#input-delete-reason');
        let editPurchaseInvoiceItemDiv = $('#edit-purchase-invoice-item')


        function modificationFormReset() {
            $('#create-modification-request-form').trigger("reset");

            console.log()

            editButton.prop('disabled', true);
            deleteButton.prop('disabled', true);
            saveButton.prop('disabled', true);
            saveButton.text('Save');

            deleteReasonDiv.prop("style", "display:none;");
            editPurchaseInvoiceItemDiv.prop("style", "display:none;");

            select2_purchase_invoice_id.val('').trigger('change');
            select2_purchase_invoice_item_id.val('').trigger('change');
            select2_product_id.val('').trigger('change');
        }


        function modificationFormDeeplink() {
            console.log('deeplinking here');
        }


        editButton.click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            $('#action-input').val("edit");
            saveButton.text('Save Changes');

            select2_product_id.prop('required', true);
            $('#input-quantity').prop('required', true);
            $('#input-base-cost').prop('required', true);

            inputDeleteReason.prop('required', false);

            editButton.prop('disabled', true);
            deleteButton.prop('disabled', false);
            saveButton.prop('disabled', false);

            if (deleteReasonDiv.is(':visible')) {
                deleteReasonDiv.toggle();
            }
            if (!editPurchaseInvoiceItemDiv.is(':visible')) {
                editPurchaseInvoiceItemDiv.toggle();
            }
        });
        deleteButton.click(function (e) {
            e.preventDefault();
            e.stopPropagation();


            $('#action-input').val("delete");
            saveButton.text('Delete Purchase Invoice Item');

            $(select2_product_id).prop('required', false);
            $('#input-quantity').prop('required', false);
            $('#input-base-cost').prop('required', false);
            $('#input-scheme-discount').prop('required', false);
            $('#input-other-discount').prop('required', false);
            $('#input-post-tax-discount').prop('required', false);

            inputDeleteReason.prop('required', true);

            editButton.prop('disabled', false);
            deleteButton.prop('disabled', true);
            saveButton.prop('disabled', false);

            if (editPurchaseInvoiceItemDiv.is(':visible')) {
                editPurchaseInvoiceItemDiv.toggle();
            }
            if (!deleteReasonDiv.is(':visible')) {
                deleteReasonDiv.toggle();
            }
        });

        saveButton.click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            if ($('#action-input').val() === "delete") {
                if (!confirm("Are you sure you want to delete the Purchase Invoice Item!")) {
                    return;
                }
            }
            $('#create-modification-request-form').submit();
        });


        select2_purchase_invoice_id.select2({
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.purchase_invoices")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (purchaseInvoice) {
                if (!purchaseInvoice || (purchaseInvoice.text && purchaseInvoice.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${purchaseInvoice.id} (vendor_ref_id: ${purchaseInvoice.vendor_ref_id})</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (purchaseInvoice) {
                $(purchaseInvoice.element).attr('purchase_invoice_id', purchaseInvoice.id);
                $(purchaseInvoice.element).attr('id', purchaseInvoice.id);
                $(purchaseInvoice.element).attr('value', purchaseInvoice.id);

                return `${purchaseInvoice.id} (vendor_ref_id: ${purchaseInvoice.vendor_ref_id})`;
            }
        });
        select2_purchase_invoice_item_id.select2({
            minimumInputLength: 0,
            ajax: {
                url: function () {
                    const purchaseInvoiceId = select2_purchase_invoice_id.find(':selected')[0].getAttribute('id');
                    const url = new URL('{{route("admin::search.purchase_invoice_items")}}');
                    url.searchParams.set('purchase_invoice_id', purchaseInvoiceId);
                    return url.toString();
                },
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (purchaseInvoiceItem) {
                if (!purchaseInvoiceItem || (purchaseInvoiceItem.text && purchaseInvoiceItem.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${purchaseInvoiceItem.id} | ${purchaseInvoiceItem.product.name} (product id: ${purchaseInvoiceItem.product_id})</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (purchaseInvoiceItem) {
                $(purchaseInvoiceItem.element).attr('id', purchaseInvoiceItem.id);
                $(purchaseInvoiceItem.element).attr('value', purchaseInvoiceItem.id);

                return `${purchaseInvoiceItem.id} | ${purchaseInvoiceItem.product.name} (product id: ${purchaseInvoiceItem.product_id})`;
            }
        });
        select2_product_id.select2({
            minimumInputLength: 1,
            ajax: {
                delay: 125,
                url: '{{route("admin::search.products_2")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: function (product) {
                if (!product || (product.text === "Searching…")) {
                    return $('<div class="padding-10"><h5 class="h6 bold">Searching...</h5></div>');
                }

                let html = '<div class="padding-10">';
                html += `<h5 class="h6 bold">${product.name} (${product.id})</h5>`;
                html += '</div>';

                return $(html);
            },
            templateSelection: function (product) {
                if (product.element && product.element.product) {
                    $(product.element).attr('id', product.element.product.id);
                    $(product.element).attr('value', product.element.product.id);

                    return `${product.element.product.name} (${product.element.product.id})`;
                }

                $(product.element).attr('id', product.id);
                $(product.element).attr('value', product.id);
                return `${product.name} (${product.id})`;
            }
        });

        select2_purchase_invoice_id.on('select2:select', function (e) {
            const purchaseInvoice = e.params.data;

            select2_purchase_invoice_item_id.prop('disabled', false);
            select2_purchase_invoice_item_id.val(null).trigger('change');
            select2_purchase_invoice_item_id.select2("open");

            editButton.prop('disabled', false);
            deleteButton.prop('disabled', false);
            saveButton.prop('disabled', false);

            if (editPurchaseInvoiceItemDiv.is(':visible')) {
                editPurchaseInvoiceItemDiv.toggle();
            }
            if (deleteReasonDiv.is(':visible')) {
                deleteReasonDiv.toggle();
            }
        });
        select2_purchase_invoice_item_id.on('select2:select', function (e) {
            const purchaseInvoiceItem = e.params.data;

            select2_product_id.val(null).trigger("change");

            let productOption = new Option(
                `${purchaseInvoiceItem.product.name} (${purchaseInvoiceItem.product_id})`,
                purchaseInvoiceItem.product_id,
                true, true);
            productOption.product = purchaseInvoiceItem.product;

            select2_product_id.append(productOption).trigger("change");

            $('#input-quantity').val(purchaseInvoiceItem.quantity);
            $('#input-base-cost').val(purchaseInvoiceItem.base_cost);
            $('#input-scheme-discount').val(purchaseInvoiceItem.scheme_discount);
            $('#input-other-discount').val(purchaseInvoiceItem.other_discount);
            $('#input-post-tax-discount').val(purchaseInvoiceItem.post_tax_discount);

            $('#old-product-id').val(`${purchaseInvoiceItem.product.name} (${purchaseInvoiceItem.product.id})`);
            $('#old-quantity').val(purchaseInvoiceItem.quantity);
            $('#old-base-cost').val(purchaseInvoiceItem.base_cost);
            $('#old-scheme-discount').val(purchaseInvoiceItem.scheme_discount);
            $('#old-other-discount').val(purchaseInvoiceItem.other_discount);
            $('#old-post-tax-discount').val(purchaseInvoiceItem.post_tax_discount);

            editButton.prop('disabled', false);
            deleteButton.prop('disabled', false);
        });
    </script>
@append