<div class="margin-t-20">
  <div class="flex gutter">
    <div class="col-4-12">
      <h2 class="h6 bold margin-t-10">Upload Banner</h2>
      <p class="margin-t-10">
        Upload Banner, change position and validity for banner.
      </p>
    </div>
    <div class="col-8-12">
      <div class="card">
        <div>
          @if(!empty($appBanner))
          <img src="{{Image::getSrc($appBanner, 300)}}" class="bd" />
          @endif
          <div {{!empty($appBanner) ? 'class=margin-t-20' : ''}}>
            <label class="label">Upload Banner</label>
            <input type="file" name="file" class="form-control">
            <span class="error error-file"></span>
          </div>
        </div>
        <div class="margin-t-20">
          <div class="flex gutter daterange">
            <div class="col-6-12">
              <label class="label">Valid From</label>
              <input name="valid_from" class="form-control valid_from" @if(!empty($appBanner)) value="{{$appBanner->valid_from}}"
              @endif />
              <span class="error error-valid_from"></span>
            </div>
            <div class="col-6-12">
              <label class="label">Valid To</label>
              <input name="valid_to" class="form-control valid_to" @if(!empty($appBanner)) value="{{$appBanner->valid_to}}"
              @endif />
              <span class="error error-valid_to"></span>
            </div>
          </div>
        </div>
        <div class="margin-t-20">
          <label class="label">Link</label>
          <input name="link" class="form-control" @if(!empty($appBanner)) value="{{$appBanner->link}}" @endif />
          <span class="error error-link"></span>
        </div>
        <div class="margin-t-20">
          <div class="flex gutter">
            <div class="col-6-12">
              <label class="label">Position</label>
              <input type="text" name="position" class="form-control" value="{{!empty($appBanner) ? $appBanner->position : ""}}" />
              <span class="error error-position"></span>
            </div>
            <div class="col-6-12">
              <label class="label">Status</label>
              <select class="form-control" name="status">
                <option value="1" @if(!empty($appBanner) && $appBanner->status == 1) selected @endif>Active</option>
                <option value="0" @if(!empty($appBanner) && $appBanner->status == 0) selected @endif>Inactive</option>
              </select>
              <span class="error error-status"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
