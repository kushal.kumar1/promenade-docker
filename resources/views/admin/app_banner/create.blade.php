@extends('admin.master')

@section('content')
<div class="container">
  <form class="form-app-banner form form-multipart" method="post" action="{{route('admin::app-banners.store')}}" data-destination="{{route("admin::app-banners.edit", ['id' => "#id#"])}}">
    <div class="header-actions">
      <button type="button" class="btn btn-discard margin-r-10">Discard</button>
      <button type="submit" class="btn btn-primary" data-form=".form-app-banner">Save</button>
    </div>
    {{csrf_field()}}
    <div class="regular light-grey">
      <a href="{{route('admin::app-banners.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> App Banners</a>
    </div>
    <div class="h1 margin-t-10">
      Add Banner
    </div>
    @include('admin.app_banner.form')
  </form>
</div>
@endsection

@section('js')
<script>
  $('.daterange').dateRangePicker({
    startOfWeek: 'monday',
    format: 'YYYY-MM-DD HH:mm:ss',
    autoClose: true,
    getValue: function() {
      if ($('.valid_from').val() && $('.valid_to').val())
        return $('.valid_from').val() + ' to ' + $('.valid_to').val();
      else
        return '';
    },
    setValue: function(s, s1, s2) {
      $('.valid_from').val(s1);
      $('.valid_to').val(s2);
    },
    time: {
      enabled: true,
    },
    defaultTime: moment().startOf('day').toDate(),
    defaultEndTime: moment().endOf('day').toDate()
  });
</script>

@endsection
