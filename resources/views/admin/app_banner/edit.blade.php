@extends('admin.master')

@section('content')
<div class="container">
  <div class="header-actions">
    <button type="button" class="btn btn-discard margin-r-10">Discard</button>
    <button type="button" class="btn btn-primary form-button" data-form=".form-app-banner">Save</button>
  </div>
  <div class="regular light-grey">
    <a href="{{route('admin::app-banners.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> App Banners</a>
  </div>
  <div class="h3 margin-t-10 bold">
    {{$appBanner->name}}
  </div>
  <form class="form form-app-banner form-multipart" method="post" action="{{route('admin::app-banners.update')}}" data-actions=".header-actions" autocomplete="off">
    {{csrf_field()}}
    <input type="hidden" name="id" value="{{$appBanner->id}}" />
    @include('admin.app_banner.form')
  </form>
</div>
@endsection

@section('js')
<script>
  $('.daterange').dateRangePicker({
    startOfWeek: 'monday',
    format: 'YYYY-MM-DD HH:mm:ss',
    autoClose: true,
    getValue: function() {
      if ($('.valid_from').val() && $('.valid_to').val())
        return $('.valid_from').val() + ' to ' + $('.valid_to').val();
      else
        return '';
    },
    setValue: function(s, s1, s2) {
      $('.valid_from').val(s1);
      $('.valid_to').val(s2);
    },
    time: {
      enabled: true,
    },
    defaultTime: moment().startOf('day').toDate(),
    defaultEndTime: moment().endOf('day').toDate()
  });
</script>

@endsection
