@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            App Banners
        </div>
        <div>
          <a href="{{route('admin::app-banners.create')}}">
            <button class="btn btn-primary">Add Banner</button>
          </a>
        </div>
    </div>
    @include('admin.grid')
@endsection