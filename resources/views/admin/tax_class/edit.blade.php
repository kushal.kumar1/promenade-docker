@extends('admin.master')
@section('css')
    <style>
        select.form-control {
            height: 34px;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-taxClass">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::tax-classes.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Tax
                Classes</a>
        </div>
        <div class="flex gutter-between">
            <div class="h3 margin-t-10 bold">
                {{$taxClass->name}}
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-tax-add">Add Tax</button>
            </div>
        </div>
        <form class="form form-taxClass" method="post" action="{{route('admin::tax-class.update')}}"
              data-actions=".header-actions">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$taxClass->id}}"/>
            <div class="margin-t-20">
                <div class="flex gutter">
                    <div class="col-12-12">
                        <div class="card">
                            {{csrf_field()}}
                            <div class="flex gutter">
                                <div class="col-6-12">
                                    <label class="label">Name</label>
                                    <input class="form-control" name="name" value="{{$taxClass->name}}"/>
                                </div>
                                <div class="col-6-12">
                                    <label class="label">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="1" @if($taxClass->status == 1) selected @endif>Active</option>
                                        <option value="0" @if($taxClass->status == 0) selected @endif>In Active</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @include('admin.grid')
    </div>
    <div id="modal-tax-add" class="modal">
        <div class="modal-content">
            <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
            <h5 class="h5">Add Tax</h5>
            <form class="form margin-t-30" method="post" action="{{route('admin::taxes.store')}}"
                  autocomplete="off">
                {{csrf_field()}}
                <input type="hidden" name="tax_class_id" value="{{$taxClass->id}}">
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Name</label>
                        <input class="form-control" name="name"/>
                        <span class="error error-name"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Percentage</label>
                        <input type="number" class="form-control" name="percentage" step="any"/>
                        <span class="error error-percentage"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-12-12">
                        <label class="label">Source State</label>
                        <select class="form-control" name="source_state_id">
                            <option value="">Select</option>
                            @foreach($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-source_state_id"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Supply Country</label>
                        <select class="form-control" name="supply_country_id">
                            <option value="">Select</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-supply_country_id"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Supply State</label>
                        <select class="form-control" name="supply_state_id">
                            <option value="">Select</option>
                            @foreach($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                        </select>
                        <span class="error error-supply_state_id"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Min Price</label>
                        <input type="number" class="form-control" name="min_price">
                        <span class="error error-min_price"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">Max Price</label>
                        <input type="number" class="form-control" name="max_price">
                        <span class="error error-max_price"></span>
                    </div>
                </div>
                <div class="flex gutter">
                    <div class="col-6-12">
                        <label class="label">Start Date</label>
                        <input type="date" class="form-control" name="start_date">
                        <span class="error error-start_date"></span>
                    </div>
                    <div class="col-6-12">
                        <label class="label">End Date</label>
                        <input type="date" class="form-control" name="end_date">
                        <span class="error error-end_date"></span>
                    </div>
                </div>
                <div class="flex gutter-between margin-t-40">
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary button-success">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection
