@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div class="h1">
                Tax Classes
            </div>
            <div>
                <button class="btn btn-primary" data-modal="#modal-taxClass-add">Add Tax Class</button>
            </div>
        </div>
        @include('admin.grid')

        <div id="modal-taxClass-add" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <h5 class="h5">Add Tax Class</h5>
                <form class="form margin-t-30" method="post" action="{{route('admin::tax-class.store')}}"
                      autocomplete="off">
                    {{csrf_field()}}
                    <label class="label">Name</label>
                    <input class="form-control" name="name"/>
                    <span class="error error-name"></span>
                    <label class="label margin-t-5">Status</label>
                    <select class="form-control" name="status">
                        <option value="1">Active</option>
                        <option value="0">In Active</option>
                    </select>
                    <span class="error error-status"></span>
                    <div class="flex gutter-between margin-t-40">
                        <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary button-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
