@extends('admin.master')

@section('content')

<div class="container">
  <div class="flex gutter-between">
    <div class="h1">
      Beats
    </div>
    <div>
      <a href="{{route('admin::beats.create')}}">
        <button class="btn btn-primary">Add Beat</button>
      </a>
    </div>
  </div>
  @include('admin.grid')
</div>

@endsection