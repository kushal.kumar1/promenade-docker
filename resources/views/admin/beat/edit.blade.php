@extends('admin.master')

@section('content')
    <div class="container">
        <div class="header-actions hidden">
            <button type="button" class="btn btn-discard margin-r-10">Discard</button>
            <button type="button" class="btn btn-primary form-button" data-form=".form-beat">Save</button>
        </div>
        <div class="regular light-grey">
            <a href="{{route('admin::beats.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Beats</a>
        </div>
        <div class="h3 margin-t-10 bold">
            {{$beat->name}}
        </div>
        <form class="form form-beat" method="post" action="{{route('admin::beats.update')}}" data-actions=".header-actions" autocomplete="off">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$beat->id}}" />
            @include('admin.beat.form')
        </form>
    </div>
@endsection
