@extends('admin.master')

@section('content')
<div class="container">
  <form class="form-beat form" method="post" action="{{route('admin::beats.store')}}" data-actions=".header-actions" data-destination="{{route("admin::beats.edit", ['id' => "#id#"])}}">
    <div class="header-actions">
      <button type="button" class="btn btn-discard margin-r-10">Discard</button>
      <button type="submit" class="btn btn-primary" data-form=".form-beat">Save</button>
    </div>
    {{csrf_field()}}
    <div class="regular light-grey">
      <a href="{{route('admin::beats.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Beats</a>
    </div>
    <div class="h1 margin-t-10">
      Add Beat
    </div>
      @include('admin.beat.form')
  </form>
</div>
@endsection
