<div class="margin-t-20">
    <div class="flex gutter">
        <div class="col-5-12">
            <h2 class="h6 bold margin-t-10">Basic Information</h2>
            <p class="margin-t-10">
                Add / Modify basic information.
            </p>
        </div>
        <div class="col-7-12">
            <div class="card">
                <div>
                    <label class="label">Name</label>
                    <input type="text" name="name" class="form-control" value="{{!empty($beat) ? $beat->name : ""}}" />
                    <span class="error error-name"></span>
                </div>
                <div class="margin-t-20">
                    <label class="label">Long Name</label>
                    <input type="text" name="long_name" class="form-control" value="{{!empty($beat) ? $beat->long_name : ""}}" />
                    <span class="error error-name"></span>
                </div>
                <div class="margin-t-20">
                    <label class="label">Status</label>
                    <select class="form-control" name="status" style="width:100%">
                      <option value="1" {{!empty($beat) && $beat->status ? 'selected' : ""}}>
                        Active
                      </option>
                      <option value="0" {{!empty($beat) && !$beat->status ? 'selected' : ""}}>
                        Inactive
                      </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="margin-t-20">
    <div class="flex gutter">
        <div class="col-5-12">
            <h2 class="h6 bold margin-t-10">Restrict Brand Sales</h2>
            <p class="margin-t-10">
              Add Brands which are not allowed to sale in this beat. For nested brands, you can add only top level brand.<br />
            </p>
            <p class="margin-t-5">
              Example: Adding Cadbury will block all childrens brands like halls, silk, bournvita etc.
            </p>
        </div>
        <div class="col-7-12">
            <div class="card">
                <div>
                    <label class="label">Brands</label>
                    <select class="form-control brand-search" name="brands[]" multiple style="width:100%">
                        @if(!empty($beat))
                            @foreach($beat->exceptions as $exception)
                                <option value="{{$exception->id}}" selected>{{$exception->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
