<div class="container">
        @if(Session::has('message'))
            <p class="alert margin-t-30 {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        <form class="form" id="demandItemsForm" method="post" action="{{route('admin::product-demands.items.save', $demand->id)}}"
              data-destination="{{route('admin::product-demands.edit', $demand->id)}}">
            @csrf
            <div class="grid card" style="margin-top: 0px;">
            <strong class="h3">Demand Items</strong>
            <table class="table table-striped">
                <thead>
                    <tr>
                        @if($demand->status == \Niyotail\Models\ProductDemand::STATUS_GENERATED)
                            <th><input id="selectAll" class="checkbox" type="checkbox"></th>
                        @endif
                        <th>PID</th>
                        <th>Name</th>
                        <th>Total Qty. Sold</th>
                        <th>Daily Unit Sale</th>
                        <th>Available Stock</th>
                        <th>Stock Days</th>
                        <th>Order Quantity</th>
                        <th>Marketer</th>
{{--                        <th>Vendor</th>--}}
                    </tr>
                </thead>
                <tbody>
                    @foreach($demand->items as $item)
                    <tr data-row-id='{{$item->id}}'>
                        @if($demand->status == \Niyotail\Models\ProductDemand::STATUS_GENERATED)
                        <td>
                            <input name="items[{{$item->id}}][selected]" class="enable-row checkbox" type="checkbox" data-id="{{$item->id}}">
                        </td>
                        @endif
                        <td>{{$item->group->id}}</td>
                        <td>{{$item->group->name}}</td>
                        <td>{{$item->total_quantity_sold}}</td>
                        <td>{{$item->daily_avg_unit_sale}}</td>
                        <td>{{$item->available_stock}}</td>
                        <td>{{$item->stock_days}}</td>
                            @if($demand->status == \Niyotail\Models\ProductDemand::STATUS_GENERATED)
                                <td><input disabled name="items[{{$item->id}}][reorder_quantity]" class="form-control" type="number" value="{{$item->reorder_quantity}}"></td>
                                @elseif($demand->status == \Niyotail\Models\ProductDemand::STATUS_FINALIZED)
                                <td>{{$item->reorder_quantity}}</td>
                            @endif
                        <td>{{$item->group->brand->marketer->name}}</td>
{{--                        <td>--}}
{{--                            <select class="form-control" disabled name="items[{{$item->id}}][vendor_id]">--}}
{{--                                @foreach($item->product->brand->marketer->vendorMarketerMaps as $map)--}}
{{--                                    <option value="{{$map->vendor->id}}">{{$map->vendor->name}}</option>--}}
{{--                                @endforeach--}}
{{--                            </select>--}}
{{--                        </td>--}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
                @if($demand->status == \Niyotail\Models\ProductDemand::STATUS_GENERATED)
            <div class="block">
                <input type="hidden" name="finalize_demand" value="0">
                <button class="margin-t-30 btn btn-primary" type="submit">Save demand items</button>
                <button id="finalizeDemandItems" class="pull-right margin-t-30 btn btn-success" type="button">Finalize demand items for PO</button>
            </div>
                @endif
        </div>
        </form>

</div>

@section('js')
    <script>
        $(function() {
            $("#selectAll").change(selectAllRows);
            $(".enable-row").change(handleRowStates);
            $("#selectAll").click();
        });

        $("#finalizeDemandItems").click(function() {
            dialog(
                'Finalize demand items',
                "Are you sure you want to finalize demand items? Demand items won't be editable post this action",
                function () {
                    $("input[name='finalize_demand']").val(1);
                    $("#demandItemsForm").submit();
                },
                function () {
                    $("input[name='finalize_demand']").val(0);
                }
            );
        });

        function handleRowStates() {
            $(".enable-row").each(function() {
                const state = $(this).prop('checked');
                const dataID = $(this).attr('data-id');
                const row = $("table").find(`tr[data-row-id='${dataID}']`);
                row.find('input[type="number"]').prop('disabled', !state);
                row.find('select').prop('disabled', !state);
            });
        }

        function selectAllRows() {
            const state = $(this).prop('checked');
            $(".enable-row").prop('checked', state);
            handleRowStates();
        }
    </script>
@endsection