@extends('admin.master')

@section('content')

    <div class="container">

        <a class="btn btn-sm btn-primary" href="{{route('admin::product-demands.edit', $demand->id)}}">Back to demand</a>
        <div class="card margin-t-30 pull-left">
            <form id="demandItemForm" class="form" method="post"
                  action="{{route('admin::product-demands.items.assign-vendor', $demand->id)}}">
                @csrf
            @foreach($formattedItems->groupBy('marketer_id') as $items)
                <div class="col-12-12 pull-left" style="margin-bottom: 50px;">
                    <div class="col-12-12 pull-left" >
                        <div class="col-6-12 pull-left bold">
                            Marketer #{{$items->first()['marketer_id']}} - {{$items->first()['marketer_name']}}
                            ({{$items->count()}} items)
                        </div>
                        @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                        <div class="col-6-12 bold pull-right text-right">
                            <button type="button" class="btn btn-sm btn-success inline-flex">Add new vendor mapping</button>
                        </div>
                            @endif
                    </div>
                    @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                    @if($items->first()['has_vendors'])
                    <div class="col-12-12 pull-left">
                        <p class="padding-v-10">Assign vendor to below items</p>
                        @foreach($items->first()['vendors'] as $vendor)
                        <button class="apply_vendor tag tag-confirmed" type="button"
                                data-marketer="{{$items->first()['marketer_id']}}"
                                data-vendor="{{$vendor['id']}}">{{$vendor['name']}} - (P{{$vendor['priority']}})</button>
                        @endforeach
                        <div class="col-12-12 pull-left margin-t-5">
                            <p><small>This will only update the vendors for items where vendor is not selected. If you want to apply to all products, first click "clear all" and then apply</small></p>
                        </div>
                    </div>
                    @endif
                    @endif
                    <div class="col-12-12 pull-left">
                        <table class="table col-12-12 " style="margin:15px 0px 0px; width: 100%;" cellpadding="10">
                            <tr class="col-12-12 bold fa-border">
                                @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                                <td>
                                    <input type="checkbox" data-marketer="{{$items->first()['marketer_id']}}" class="checkbox check_marketer_items">
                                </td>
                                @endif
                                <td>GID</td>
                                <td>Group</td>
                                <td>Units</td>
                                <td>Reorder Qty.</td>
                                <td>Brand</td>
                                <td>Marketer</td>
                                <td>Vendor
                                    @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)<button type="button" data-marketer="{{$items->first()['marketer_id']}}" class="clear_vendor btn btn-sm btn-primary pull-right">Clear all</button>
                                        @endif
                                </td>
                            </tr>
                            @foreach($items as $item)
                                <tr class="col-12-12 fa-border row_{{$items->first()['marketer_id']}}" style="margin-top: 10px;">
                                    @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                                    <td>
                                        <input data-id="{{$item['id']}}" data-marketer="{{$item['marketer_id']}}"
                                               name="item[{{$item['id']}}][id]" value="{{$item['id']}}" type="checkbox"
                                               @if($item['vendor_id']) checked @endif
                                               class="rowCheck checkbox">
                                    </td>
                                    @endif
                                    <td>{{$item['group_id']}}</td>
                                    <td>{{$item['group_name']}}</td>
                                    <td>{{$item['units']}}</td>
                                    <td>{{$item['reorder_quantity']}}</td>
                                    <td>{{$item['brand_name']}}</td>
                                    <td>{{$item['marketer_name']}}</td>
                                        @if($demand->status != \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                                    <td>
                                        @if($item['has_vendors'])
                                            <select @if(!$item['vendor_id']) disabled @endif data-id="{{$item['id']}}" data-marketer="{{$item['marketer_id']}}" class="select2 marketer_{{$item['marketer_id']}} form-control vendor_select" name="item[{{$item['id']}}][vendor_id]">
                                                <option value="">-- select vendor --</option>
                                                @foreach($item['vendors'] as $vendor)
                                                    <option @if($item['vendor_id'] == $vendor['id']) selected @endif value="{{$vendor['id']}}">{{$vendor['id']}} - {{$vendor['name']}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                    </td>
                                            @else
                                    <td>{{$item['vendor_name']}}</td>
                                    @endif
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <div class="col-12-12 pull-left" style="margin-bottom: 40px;">
                    <hr>
                </div>
            @endforeach
                <div class="col-12-12 pull-left margin-t-25">

                    @if($demand->status == \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                        <a href="{{route('admin::product-demands.preview.po', $demand->id)}}" class="btn btn-success pull-right">Preview PO</a>
                        @else
                        <input type="hidden" name="finalize_demand" value="0">
                        <button type="submit" id="finalizeVendor" class="btn btn-success pull-right">Finalise vendor assignment</button>
                        <button type="submit" class="btn btn-primary pull-right margin-r-10">Update demand items</button>
                    @endif
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
<script>
$(function() {
    $(".clear_vendor").click(function() {
        const marketerId = $(this).data('marketer');
        const marketerClass = 'marketer_'+marketerId;
        $(".vendor_select."+marketerClass).val('');
    });
    $(".apply_vendor").click(function() {
        const marketerId = $(this).data('marketer');
        const marketerClass = 'marketer_'+marketerId;
        const vendorId = $(this).data('vendor');
        $(".vendor_select."+marketerClass).each(function() {
            const val = $(this).val();
            const id = $(this).data('id');
            const rowCheck = $("input[data-id='"+id+"'][type='checkbox']");
            const rowSelected = rowCheck.prop('checked');
            if(val == "" && rowSelected) {
                $(this).val(vendorId);
            }
        });
    });
    $(".check_marketer_items").click(function() {
        const marketerId = $(this).data('marketer');
        const marketerItems = $("input[data-marketer='"+marketerId+"'][type='checkbox']");
        const vendorSelects = $("select.marketer_"+marketerId);
        const state = $(this).prop('checked');
        marketerItems.prop('checked', state);
        vendorSelects.prop('required', state);
        vendorSelects.prop('disabled', !state);
        if(!state) {
            vendorSelects.val('');
        }
    });
    $(".rowCheck").click(function() {
        const rowId = $(this).data('id');
        const vendorSelect = $("select.vendor_select[data-id='"+rowId+"']");
        const state = $(this).prop('checked');
        vendorSelect.prop('required', state);
        vendorSelect.prop('disabled', !state);
        if(!state) {
            vendorSelect.val('');
        }
    });
    $("#finalizeVendor").click(function(e) {
        e.preventDefault();
        const uncheckedCount = $("input.rowCheck:not(:checked)").length;
        if(uncheckedCount > 0) {
            dialog(
                'Error!',
                "Please assign vendor to all the items before finalizing vendor assignment"
            );
            return false;
        }

        dialog(
            'Finalize vendor assignment demand items',
            "Are you sure you want to finalize vendor assignment? Vendor cannot be updated once assignment is finalized.",
            function () {
                $("input[name='finalize_demand']").val(1);
                $("form#demandItemForm").submit();
                $('button').prop('disabled', true);
            },
            function () {
                $("input[name='finalize_demand']").val(0);
            }
        );
    });
});

</script>
@endsection

