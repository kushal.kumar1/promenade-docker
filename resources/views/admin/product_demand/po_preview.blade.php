@extends('admin.master')

@section('content')

    <div class="container pull-left" style="margin-top: 0px; padding-bottom: 0px;">
        <a class="btn btn-sm btn-primary" href="{{route('admin::product-demands.preview.po-items', $demand->id)}}">Back to demand items</a>
    </div>

    <form class="form" id="poGenerationForm" method="post" action="{{route('admin::product-demands.po.create', $demand->id)}}"
          data-destination="{{route("admin::purchase-order.index")}}>
        @csrf
    <div class="container pull-left" style="padding-top: 0px;">
        @foreach($formattedItems as $po)
        <div class="card col-12-12 pull-left margin-t-25">
            <p class="h4 bold">PO #{{$loop->index + 1}}</p>
            <div class="col-12-12 pull-left margin-t-10">
                <div class="col-12-12 pull-left">
                    <table class="table col-12-12 " style="margin:0px 0px 0px; width: 100%;" cellpadding="10">
                        <tr class="col-12-12 fa-border">
                            <td>Vendor</td>
                            <td colspan="2" class="bold">#{{$po['mapping']->vendor->id}} {{$po['mapping']->vendor->name}}</td>
                            <td>Marketer</td>
                            <td colspan="2" class="bold">#{{$po['mapping']->marketer->id}} {{$po['mapping']->marketer->name}}</td>
                        </tr>
                        <tr class="col-12-12 fa-border">
                            <td>Relation - {{$po['mapping']->relation}}</td>
                            <td>Channel - {{$po['mapping']->channel}}</td>
                            <td>MOV - {{number_format($po['mapping']->mov)}}</td>
                            <td>Lead Time - {{$po['mapping']->lead_time}}</td>
                            <td>Credit Term - {{$po['mapping']->credit_term}}</td>
                            <td>Cash Discount - {{$po['mapping']->cash_discount}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-12-12 pull-left">
                    <table class="table col-12-12 " style="margin:15px 0px 0px; width: 100%;" cellpadding="10">
                        <tr class="col-12-12 bold fa-border">
                            <td>GID</td>
                            <td>Group</td>
                            <td>Units</td>
                            <td>Reorder Qty.</td>
                        </tr>
                        @foreach($po['items'] as $item)
                        <tr class="fa-border">
                            <td>{{$item['group_id']}}</td>
                            <td>{{$item['group_name']}}</td>
                            <td>{{$item['units']}}</td>
                            <td>{{$item['reorder_quantity']}}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        @endforeach
        <button class="margin-t-10 pull-right btn btn-primary" id="createPoFromDemand">Create Purchase Order</button>
    </div>
    </form>

@endsection

@section('js')
<script>
    $(function() {
        $("#createPoFromDemand").click(function(e) {
            e.preventDefault();
            dialog(
                'Are you sure you want to generate PO',
                "This will generate PO for all the items to which a vendor is assigned.",
                function () {
                    $('button').prop('disabled', true);
                    $('form#poGenerationForm').submit();
                },
                function () {
                    $('button').prop('disabled', false);
                }
            );
        });
    });
</script>
@endsection

