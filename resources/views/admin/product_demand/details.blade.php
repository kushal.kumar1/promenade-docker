@extends('admin.master')

@section('content')


    <div class="container">
        <a href="{{ route('admin::product-demands.index') }}" class="btn btn-primary">Back</a>
    </div>

    <div class="container" style="padding-top: 0px;">
        @if(Session::has('message'))
            <p class="alert margin-t-30 {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif

        @if(empty($demand) || (!empty($demand) && $demand->status == 'initiated'))
            @include('admin.product_demand.form')
        @endif

        @if(!empty($demand))
            @if($demand->status != 'initiated')
                @include('admin.product_demand.demand')
            @endif
        @endif
    </div>



    @if(!empty($demand))
        @if($demand->status == 'initiated')
            <div class="container">
                <h3><strong>Demand Items</strong></h3>
                @include('admin.grid')
            </div>
        @endif

        @if($demand->status == 'generated' || $demand->status == 'finalized')
            @include('admin.product_demand.items')
        @endif
    @endif

@endsection

@section('js')
    <script>
        $(function() {
            $(".select2").select2();
        });
    </script>
@endsection

