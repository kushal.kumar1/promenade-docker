@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div>
                <div class="h1">
                    Product Demands
                </div>
            </div>

            <div>
                <a href="{{ route('admin::product-demands.create') }}" class="btn btn-primary">Create demand</a>
            </div>
        </div>

        @if(Session::has('message'))
            <p class="alert margin-t-30 {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
        
        @include('admin.grid')
    </div>

@endsection

