<form class="card form inline-block col-12-12 " method="POST" id="demandForm"
      autocomplete="off" data-destination="{{route('admin::product-demands.index')}}"
      action="{{route('admin::product-demands.save', !empty($demand) ? $demand->id : null)}}">
    @csrf
    <div class="flex">
        <div class="h3">
            <strong>@if(!empty($demand)) Update @else Create @endif Product Demand</strong>
        </div>
    </div>

    <div class="flex">
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Channel <span class="danger">*</span></label>
                <select class="form-control" name="channel">
                    <option @if(!empty($demand) && $demand->channel == 'primary') selected @endif value="primary">Primary (1K to store)</option>
                    <option @if(!empty($demand) && $demand->channel == 'secondary') selected @endif value="secondary">Secondary (1K store to customer)</option>
                </select>
            </div>
        </div>
        <div class="col-4-12 padding-10">
            <div class="form-group">
                <label class="label margin-t-20">Sale Days <span class="danger">*</span></label>
                <input @if(!empty($demand) && !empty($demand->sale_days)) value="{{$demand->sale_days}}" @endif  type="number" name="sale_days" required class="form-control" placeholder="No. of sale days" min="1">
            </div>
        </div>
    </div>

    <div class="flex">
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Reorder days <span class="danger">*</span></label>
                <input @if(!empty($demand)) value="{{$demand->reorder_days}}" @endif type="number" name="reorder_days" required class="form-control" placeholder="No. of days">
            </div>
        </div>
        
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Inventory days <span class="danger">*</span></label>
                <input @if(!empty($demand)) value="{{$demand->inventory_days}}" @endif type="number" name="inventory_days" required class="form-control" placeholder="No. of days">
            </div>
        </div>

        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Choose Warehouse<span class="danger">*</span></label>
                <select class="form-control" name="warehouse_id">
                    @foreach($userWarehouses as $warehouse)
                        <option value="{{$warehouse->id}}" @if(session()->get('selected-warehouse') == $warehouse->id) selected @endif>{{$warehouse->full_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="flex">
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Vendor</label>
                <select class="form-control select2" name="vendor_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\Vendor::all() as $vendor)
                        <option @if(!empty($demand) && $demand->vendor_id == $vendor->id) selected @endif value="{{$vendor->id}}">{{ $vendor->id  }} - {{$vendor->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Marketer</label>
                <select class="form-control select2" name="marketer_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\Marketer::all() as $marketer)
                        <option @if(!empty($demand) && $demand->marketer_id == $marketer->id) selected @endif value="{{$marketer->id}}">{{ $marketer->id  }} - {{$marketer->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-4-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">Brand</label>
                <select class="form-control select2" name="brand_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\Brand::all() as $brand)
                        <option @if(!empty($demand) && $demand->brand_id == $brand->id) selected @endif value="{{$brand->id}}">{{ $brand->id  }} - {{$brand->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="flex">
        <div class="col-3-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">CL1</label>
                <select class="form-control select2" name="cl1_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\NewCatL1::all() as $catL1)
                        <option @if(!empty($demand) && $demand->cl1_id == $catL1->id) selected @endif value="{{$catL1->id}}">{{ $catL1->id  }} - {{$catL1->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-3-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">CL2</label>
                <select class="form-control select2" name="cl2_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\NewCatL2::all() as $catL2)
                        <option @if(!empty($demand) && $demand->cl2_id == $catL2->id) selected @endif value="{{$catL2->id}}">{{ $catL2->id  }} - {{$catL2->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-3-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">CL3</label>
                <select class="form-control select2" name="cl3_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\NewCatL3::all() as $catL3)
                        <option @if(!empty($demand) && $demand->cl3_id == $catL3->id) selected @endif value="{{$catL3->id}}">{{ $catL3->id  }} - {{$catL3->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-3-12 padding-10 pull-left">
            <div class="form-group">
                <label class="label margin-t-20">CL4</label>
                <select class="form-control select2" name="cl4_id">
                    <option value="">-- None --</option>
                    @foreach(\Niyotail\Models\NewCatL4::all() as $catL4)
                        <option @if(!empty($demand) && $demand->cl4_id == $catL4->id) selected @endif value="{{$catL4->id}}">{{ $catL4->id  }} - {{$catL4->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    @if(!empty($demand))
    <input type="hidden" name="generate_demand" value="0">
    @endif

    <div class="block">
        <button type="submit" class="btn btn-primary margin-t-20">Submit</button>
        @if(!empty($demand))
        <button type="button" id="generateDemand" class="pull-right btn btn-success margin-t-20">Finalize Demand</button>
        @endif
    </div>
</form>


@section('js')
<script>
$(function() {
    $(".select2").select2();

    $("#generateDemand").click(function() {
        dialog(
            'Generate Demand',
            "Are you sure you want to generate demand items? Demand won't be editable post this action",
            function () {
                $("input[name='generate_demand']").val(1);
                $("#demandForm").submit();
            },
            function () {
                $("input[name='generate_demand']").val(0);
            }
        );
    });
});
</script>
@endsection