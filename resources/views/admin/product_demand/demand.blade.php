<div class="grid">
    <table class="table table-striped" style="padding-top: 0px; margin-top: 0px;">
        <tbody>
        <tr>
            <td colspan="2">
                <strong class="h3">Demand Details</strong>
                @if(!empty($demand))
                <span class="pull-right tag tag-completed">{{strtoupper($demand->status)}}</span>
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2">
                @if(!empty($demand) && $demand->status == \Niyotail\Models\ProductDemand::STATUS_FINALIZED)
                    <a href="{{route('admin::product-demands.preview.po-items', $demand->id)}}" class="btn btn-success">Assign vendor to items</a>
                @endif
                @if(!empty($demand) && $demand->status == \Niyotail\Models\ProductDemand::VENDOR_ASSIGNED)
                    <a href="{{route('admin::product-demands.preview.po-items', $demand->id)}}" class="btn btn-success">View demand items</a>
                @endif
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-top: 0px;">
                    <tr>
                        <td style="font-weight: bold">Date from</td>
                        <td>{{$demand->date_from}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Date to</td>
                        <td>{{$demand->date_to}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Sale days</td>
                        <td>{{$demand->sale_days}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Inventory days</td>
                        <td>{{$demand->inventory_days}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Reorder days</td>
                        <td>{{$demand->reorder_days}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Created By</td>
                        <td>{{$demand->createdBy->name}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Created at</td>
                        <td>{{$demand->created_at}}</td>
                    </tr>
                </table>
            </td>
            <td>
                <table style="margin-top: 0px;">
                    <tr>
                        <td style="font-weight: bold">Vendor</td>
                        <td>{{$demand->vendor ? $demand->vendor->id." - ".$demand->vendor->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Marketer</td>
                        <td>{{$demand->markete ? $demand->marketer->id." - ".$demand->marketer->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">Brand</td>
                        <td>{{$demand->brand ? $demand->brand->id." - ".$demand->brand->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">CL1</td>
                        <td>{{$demand->calL1 ? $demand->calL1->id." - ".$demand->calL1->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">CL2</td>
                        <td>{{$demand->calL2 ? $demand->calL2->id." - ".$demand->calL2->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">CL3</td>
                        <td>{{$demand->calL3 ? $demand->calL3->id." - ".$demand->calL3->name : ''}}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold">CL4</td>
                        <td>{{$demand->calL4 ? $demand->calL4->id." - ".$demand->calL4->name : ''}}</td>
                    </tr>
                </table>
            </td>
        </tr>

        </tbody>
    </table>
</div>