@extends('admin.master')

@section('content')

<div class="container">
    <div class="flex gutter-between">
        <div class="h1">
            {{!empty($title) ? $title : 'Orders'}}
        </div>
        <div>
            <button class="btn btn-primary" data-modal="#modal-stores">Create Order</button>
        </div>
    </div>
    @include('admin.grid')
</div>

@include('admin.components._stores_modal')

@endsection
