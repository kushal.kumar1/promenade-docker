<div class="margin-t-20 item">
    <div class="flex gutter-xsm">
        <div class="col-1-12">
            <img class="bd" src="{{Image::getSrc($item->product->primary_image, 50)}}" />
        </div>
        <div class="col-4-12">
            <a href="{{route('admin::products.edit',$item->product->id)}}" class="link" target="_blank">
                {{$item->product->name}}
            </a>
            <div class="small">
                SKU: {{$item->sku}} | Variant: {{$item->variant}} ({{$item->productVariant->quantity}} {{$item->productVariant->uom}})
            </div>
        </div>
        <div class="col-2-12">
             {{$item->pretty_price}}
        </div>
        <div class="col-1-12">
            {{$item->quantity}}
        </div>
        <div class="col-2-12">
            {{$item->pretty_discount}}
        </div>
        <div class="col-2-12 text-right">
            {{$item->total}}
        </div>
    </div>
</div>
