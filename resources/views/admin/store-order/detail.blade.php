@extends('admin.master')

@section('content')
    <div class="container">
        <div class="regular light-grey">
            <a href="{{route('admin::store.orders.index')}}"><i class="fa fa-angle-left" aria-hidden="true"></i> Store Orders</a>
        </div>
        <div class="margin-t-10">
            <div class="flex gutter-between">
                <div>
                    <div class="flex cross-center">
                        <h1 class="h1 margin-r-20 bold">#{{$storeOrder->id}}</h1>
                        <div class="tag tag-{{$storeOrder->status}} margin-r-10 uppercase">{{$storeOrder->status}}</div>
                    </div>
                    <div class="regular grey-light margin-t-10">
                        {{(new DateTime($storeOrder->created_at))->format('F d, Y \a\t h:i A')}}
                    </div>
                </div>
                <div>
{{--                    @if ($storeOrder->canCancel())--}}
{{--                        <button type="button" class="btn btn-danger" data-modal="#cancel-order">--}}
{{--                            Cancel Order--}}
{{--                        </button>--}}
{{--                    @endif--}}
                </div>
            </div>
        </div>
        <div class="margin-t-25">
            <div class="flex gutter">
                <div class="col-8-12">
                    <div class="card">
                        <div class="flex gutter-between cross-center">
                            <h2 class="h5 bold">Order Items</h2>
                        </div>
                        <hr class="hr hr-light margin-t-20"/>
                        <div class="margin-t-20">
                            <div class="items">
                                <div class="flex gutter-xsm bold">
                                    <div class="col-1-12">
                                    </div>
                                    <div class="col-4-12">
                                        Item
                                    </div>
                                    <div class="col-2-12">
                                        Price
                                    </div>
                                    <div class="col-1-12">
                                        Qty
                                    </div>
                                    <div class="col-2-12">
                                        Discount
                                    </div>
                                    <div class="col-2-12 text-right">
                                        Total
                                    </div>
                                </div>
                                <hr class="hr hr-light margin-t-20"/>
                                @foreach ($storeOrder->items as $item)
                                    <div class="margin-t-20 items-by-status">
                                        @include('admin.store-order._item')
                                        <hr class="hr hr-light margin-t-20"/>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="margin-t-20">
                            <div class="flex gutter-sm">
                                <div class="col-6-12">
                                </div>
                                <div class="col-6-12">
                                    <div class="flex gutter-sm">
                                        <div class="col-6-12 grey-light text-right">
                                            Subtotal
                                        </div>
                                        <div class="col-6-12 text-right">
                                           &#8377; {{$storeOrder->pretty_subtotal}}
                                        </div>
                                        <div class="col-6-12 grey-light text-right">
                                            Discount
                                        </div>
                                        <div class="col-6-12 text-right">
                                            &#8377; {{$storeOrder->pretty_discount}}
                                        </div>
                                        <div class="col-6-12 grey-light text-right">
                                            Total Tax
                                        </div>
                                        <div class="col-6-12 text-right">
                                            &#8377; {{$storeOrder->pretty_tax}}
                                        </div>
                                        <div class="col-6-12 grey-light text-right h6 bold">
                                            Total
                                        </div>
                                        <div class="col-6-12 text-right h6 bold">
                                            &#8377; {{$storeOrder->pretty_total}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-4-12">
                    <div class="card">
                        <div>
                            <h1 class="h5 bold">Store</h1>
                            <h1 class="h5 margin-t-20"><a
                                        href="{{route('admin::stores.profile', $storeOrder->store->id)}}"
                                        class="link uppercase" target="_blank">{{$storeOrder->store->name}}</a></h1>
                            <div class="margin-t-5">
                                Beat: {{$storeOrder->store->beat->name}}
                                @if(!empty($storeOrder->createdBy))
                                    <div class="margin-t-20">
                                        <div class="flex gutter-between">
                                            <span class="bold">Order By :</span>
                                        </div>
                                    </div>
                                    <div class="margin-t-5">{{$storeOrder->createdBy->name}} </div>
                                    <div class="margin-t-5">{{$storeOrder->createdBy->mobile}}</div>
                                    <div class="margin-t-5">
                                        <span class="tag uppercase small">{{$storeOrder->created_by_type}}</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(!empty($posOrderId))
                    <div class="card margin-t-20">
                        <h1 class="h5 bold">Additional Info</h1>
                        <div class="margin-t-20">
                            <div class="flex gutter-between">
                                <div class="col-6-12">Pos Order Id</div>
                                <div class="col-6-12">{{$posOrderId}}</div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(!empty($stockTransferType))
                        <div class="card margin-t-20">
                            <h1 class="h5 bold">Additional Info</h1>
                            <div class="margin-t-20">
                                <div class="flex gutter-between">
                                    <div class="col-6-12">Stock Transfer Type</div>
                                    <div class="col-6-12">{{strtoupper($stockTransferType)}}</div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
@endsection
