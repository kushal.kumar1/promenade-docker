<div id="modal-audit-excess" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <label class="h4 font-secondary">Are you sure you want to mark excess?</label>
            <span class="close" data-dismiss="modal">X</span>
        </div>
        <div class="margin-t-20">
            <form id="excess-form" method="post" action="" >
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="margin-t-30">
                            <div class="flex gutter">
                                <div class="col-12-12">
                                    <label>Excess Quantity</label>
                                    <input class="form-control" name="quantity" placeholder="Quantity" required/>
                                </div>
                                <div class="col-12-12">
                                    <label>Reason</label>
                                    <select id="select2-reason" class="form-control" name="reason_id" type="submit" required></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer margin-t-40">
                    <button type="submit" class="btn btn-primary">Mark Excess</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('js')
    <script>
        function storageTemplate(storage) {
            if (!storage.id) {
                return storage.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${storage.label}</h5>`;
            html += `<h6 class="margin-t-5">${storage.id}</h6>`;
            html += '</div>';
            return $(html);
        }

        function storageSelection(storage){
            return storage.label + ", " + storage.id;
        }
        $('#select2-storage').select2({
            minimumInputLength: 2,
            ajax: {
                url: '{{route("admin::search.storages")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: storageTemplate,
            templateSelection: storageSelection
        });

        function reasonTemplate(reason) {
            if (!reason.id) {
                return reason.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${reason.reason_text}</h5>`;
            html += `<h6 class="margin-t-5">${reason.id}</h6>`;
            html += '</div>';

            return $(html);
        }

        function reasonSelection(reason) {
            return reason.reason_text + ", " + reason.id;
        }
        $('#select2-reason').select2({
            minimumInputLength: 0,
            ajax: {
                url: "{!! route("admin::search.reasons", ["source"=>"audit", "type"=>"excess"]) !!}",
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: reasonTemplate,
            templateSelection: reasonSelection
        });

        $('#button-assign').on('click', function(){
            let auditItemLink = '{{route("admin::audits.detail", ["id" => "#"])}}';
            const assign = $('#select2-storage').select2('data');
            if (assign.length > 0) {
                window.location = auditItemLink.replace("#", assign[0].id);
            }
        });
        $('#popup').on('click', function(){
            var s = $(this).attr('data-auditItem');
            $("#modal-audit-excess #product_id").val(s);
        })

    </script>
@append