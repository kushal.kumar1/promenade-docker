<div id="modal-audit-dismiss" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <label class="h4 font-secondary">Are you sure you want to dismiss?</label>
            <span class="close" data-dismiss="modal">X</span>
        </div>
        <div class="margin-t-20">
            <form method="post" action="" id="dismiss-form">
                {{csrf_field()}}
                <div class="modal-footer margin-t-40">
                    <button type="submit" class="btn btn-danger">Dismiss</button>
                </div>
            </form>
        </div>
    </div>
</div>