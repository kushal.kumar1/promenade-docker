<div id="modal-audit-reject" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>

        <form id = "reject-form" method="post" action="">
            {{csrf_field()}}
            <h1 class="h5">Choose a Reason for rejecting</h1>
            <div class="margin-t-20">
                <label class="label">Select a Reason</label>
                <select id="select2-reason-reject" class="form-control" style="width:100%;" name="reasonId" required></select>
                <button class="btn btn-primary margin-t-20">Reject</button>
            </div>
        </form>
    </div>
</div>

@section('js')
    <script>
        function reasonTemplate(reason) {
            if (!reason.id) {
                return reason.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${reason.reason_text}</h5>`;
            html += '</div>';

            return $(html);
        }

        function reasonSelection(reason) {
            return reason.reason_text
        }

        $('#select2-reason-reject').select2({
            minimumInputLength: 0,
            ajax: {
                url: "{!! route("admin::search.reasons", ["source"=>"audit", "type"=>"reject"]) !!}",
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: reasonTemplate,
            templateSelection: reasonSelection
        });
    </script>
@append