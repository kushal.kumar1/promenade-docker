<div id="modal-audit-assign" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>

        <form id = "assign-form" method="post" action="">
            {{csrf_field()}}
            <h1 class="h5">Choose an Employee</h1>
            <div class="margin-t-20">
                <label class="label">Select an Employee</label>
                <input hidden value = [auditItemIds]  >
                <select id="select2-employee" class="form-control" style="width:100%;" name="assignedTo" required></select>
                <button class="btn btn-primary margin-t-20">Assign</button>
            </div>
        </form>
    </div>
</div>

@section('js')
    <script>
        function employeeTemplate(employee) {
            if (!employee.id) {
                return employee.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${employee.name}</h5>`;
            html += `<h6 class="margin-t-5">${employee.id}</h6>`;
            html += '</div>';

            return $(html);
        }

        function employeeSelection(employee) {
            return employee.name + ", " + employee.id;
        }

        $('#select2-employee').select2({
            minimumInputLength: 1,
            ajax: {
                url: '{{route("admin::search.employees")}}',
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: employeeTemplate,
            templateSelection: employeeSelection
        });
    </script>
@append
