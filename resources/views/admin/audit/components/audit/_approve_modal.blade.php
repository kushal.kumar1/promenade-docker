<div id="modal-audit-approve" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Are you sure you want to approve this data?</h1>
        <div class="margin-t-20">
            <form id = "form-audit-approve" method="post" action="">
                {{csrf_field()}}
                <div class="flex gutter-between popup">
                    <button type="submit" class="btn btn-success">Approve</button>
                </div>
            </form>
        </div>
    </div>
</div>