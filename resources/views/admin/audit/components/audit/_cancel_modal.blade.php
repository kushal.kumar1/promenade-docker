<div id="modal-audit-cancel" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Are you sure you want to cancel this Audit?</h1>
        <div class="margin-t-20">
            <form id="form-audit-cancel" method="post" action="">
                {{csrf_field()}}
                <h1 class="h5">Choose a Reason for cancelling</h1>
                <div class="margin-t-20">
                    <label class="label">Select a Reason</label>
                    <select id="select2-reason-cancel-audit" class="form-control" style="width:100%;"
                            name="reasonId" required></select>
                    <button id="button-cancel" class="btn btn-primary margin-t-20">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('js')
    <script>
        function reasonTemplate(reason) {
            if (!reason.id) {
                return reason.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${reason.reason_text}</h5>`;
            html += '</div>';

            return $(html);
        }

        function reasonSelection(reason) {
            return reason.reason_text
        }

        $('#select2-reason-cancel-audit').select2({
            minimumInputLength: 0,
            ajax: {
                url: "{!! route("admin::search.reasons", ["source"=>"audit", "type"=>"cancel"]) !!}",
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: reasonTemplate,
            templateSelection: reasonSelection
        });
    </script>
@append