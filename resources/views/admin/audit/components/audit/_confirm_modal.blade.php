<div id="modal-audit-confirm" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Are you sure you want to confirm?</h1>
        <div class="margin-t-20">
            <form id="form-audit-confirm" method="post" action="">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="flex gutter-between margin-t-20 popup">
                        <button type="submit" class="btn btn-success">Confirm</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>