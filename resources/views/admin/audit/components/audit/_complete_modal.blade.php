<div id="modal-audit-complete" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Are you sure you want to complete this Audit?</h1>
        <div class="margin-t-20">
            <form id="form-audit-complete" method="post" action="">
                {{csrf_field()}}
                <div class="flex gutter-between popup">
                    <button type="submit" class="btn btn-success">Complete</button>
                </div>
            </form>
        </div>
    </div>
</div>