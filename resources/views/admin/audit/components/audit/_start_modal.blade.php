<div id="modal-audit-start" class="modal">
    <div class="modal-content">
        <span class="close" data-dismiss="modal">X</span>
        <h1 class="h5">Are you sure you want to start the Audit?</h1>
        <div class="margin-t-20">
            <form id="form-audit-start" method="post" action="">
                {{csrf_field()}}
                <div class="flex gutter-between margin-t-20 popup">
                    <button type="submit" class="btn btn-success">Start</button>
                </div>
            </form>
        </div>
    </div>
</div>