<div id="modal-audit-release" class="modal">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close">&times;</span>
            <label class="h4 font-secondary">Are you sure you want to release?</label>
            <span class="close" data-dismiss="modal">X</span>
        </div>
        <div class="margin-t-20">
            <form method="post" action="" id="release-form">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <div class="margin-t-30">
                            <div class="flex gutter">
                                <div class="col-12-12">
                                    <input class="form-control" name="quantity" placeholder="Quantity"/>
                                </div>
                                <div class="col-12-12">
                                    <label>Select Destination Storage</label>
                                    <select id="select-storage-release" class="form-control" name="destinationStorageId" type="submit" required></select>
                                </div>
                                <div class="col-12-12">
                                    <label>Reason for Release</label>
                                    <select id="select-reason-release" class="form-control" name="reasonId" type="submit" required></select>
                                </div>
                                <div class="col-12-12">
                                    <input class="form-control" id="remarks" placeholder="Remarks/Additional Info"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer margin-t-40">
                    <button type="submit" class="btn btn-primary">Release</button>
                </div>
            </form>
        </div>
    </div>
</div>
@section('js')
    <script>
        function storageTemplate(storage) {
            if (!storage.id) {
                return storage.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${storage.label}</h5>`;
            html += `<h6 class="margin-t-5">${storage.id}</h6>`;
            html += '</div>';
            return $(html);
        }

        function storageSelection(storage){
            return storage.label + ", " + storage.id;
        }
        $('#select-storage-release').select2({
            minimumInputLength: 1,
            ajax: {
                url: '{{route("admin::search.storages")}}',
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: storageTemplate,
            templateSelection: storageSelection
        });

        function reasonTemplate(reason) {
            if (!reason.id) {
                return reason.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${reason.reason_text}</h5>`;
            html += `<h6 class="margin-t-5">${reason.id}</h6>`;
            html += '</div>';

            return $(html);
        }

        function reasonSelection(reason) {
            return reason.reason_text + ", " + reason.id;
        }
        $('#select-reason-release').select2({
            minimumInputLength: 0,
            ajax: {
                url: "{!! route("admin::search.reasons", ["source"=>"audit", "type"=>"release"]) !!}",
                dataType: 'json',
                processResults: (data) => { return { results: data }; }
            },
            templateResult: reasonTemplate,
            templateSelection: reasonSelection
        });

    </script>
@append