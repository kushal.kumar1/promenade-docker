@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div>
                <label class="h1">Storage Report</label>
            </div>
        </div>
        @include('admin.grid')
    </div>
    @include('admin.audit.components._dismiss_modal')
@endsection

@section('js')
    <script>
        @foreach(session('messages') ?? [] as $message)
        (new Toast('{{$message['message'] ?? ''}}', '{{$message['type'] ?? ''}}')).show();
        @endforeach

        $('.dismiss-audit-item').click(function (e) {
            $('#dismiss-form').attr('action', $(e.target).attr('data-action'));
        });
    </script>
@append