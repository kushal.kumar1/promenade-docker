@extends('admin.master')

@section('content')

    <div class="container">
        <div class="flex gutter-between">
            <div>
                <label class="h1">Hold List</label>
            </div>
        </div>
        @include('admin.grid')
    </div>
    @include('admin.audit.components._excess_modal')
    @include('admin.audit.components._loss_modal')
    @include('admin.audit.components._release_modal')
@endsection

@section('js')
    <script>
        @foreach(session('messages') ?? [] as $message)
        (new Toast('{{$message['message'] ?? ''}}', '{{$message['type'] ?? ''}}')).show();
        @endforeach


        $(document).click('audit-item-action', function (e) {
            $($(e.target).attr('data-form')).attr('action', $(e.target).attr('data-action'));
        });
    </script>
@append