@extends('admin.master')
<head>
    <style>
        .popup {
            display: inline;
            width: 100%;
            margin: 0 auto 0 30px;
            padding: 5px;
        }

        .tag {
            padding: 4px 8px;
            font-size: 11px;
            line-height: 1;
            text-shadow: none;
            background-color: #e1e1e1;
            font-weight: 600;
            color: #575757;
            border-radius: 0.25em;
            text-transform: uppercase;
        }

        .tag.tag-started {
            background-color: #1dbb99;
            color: #fff;
        }

        .tag.tag-approved {
            background-color: #1dbb99;
            color: #fff;
        }
    </style>
</head>

@section('content')
    <div class="container">
        <div class="flex gutter-between">
            <h1 class="h1">Audits</h1>
            <div>
                <button class="btn btn-primary" data-modal="#modal-audit-list">Create New Audit</button>
            </div>
        </div>

        @include('admin.grid')
        @include('admin.audit.components.audit._start_modal')
        @include('admin.audit.components.audit._cancel_modal')

        <div id="modal-audit-list" class="modal">
            <div class="modal-content">
                <span class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></span>
                <label class="h3 text-center">Create New Audit</label>
                <form method="post" action="{{route('admin::audits.create')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="margin-t-30">
                            <div class="flex gutter">
                                <div class="col-12-12">
                                    <input class="form-control" name="name" placeholder="Audit Name"/>
                                </div>
                                <div>
                                    <label class="h6">Type :&emsp;</label>
                                    <input type="radio" id="productwise" name="type" value="product">
                                    <label for="productwise">Product</label>
                                </div>
                                <div>
                                    <input type="radio" id="storagewise" name="type" value="storage">
                                    <label for="storagewise">Storage</label>

                                </div>
                                <div class="col-12-12">
                                    <label class="label">Select a Reason</label>
                                    <select id="select2-reason" class="form-control" style="width:100%;"
                                            name="reasonId"></select>
                                </div>
                                <div class="flex gutter-between cross-center popup">
                                    <label class="h6">File: </label>
                                    <input id="file" type="file" name="file"
                                           value="Upload Audit List" required>
                                </div>
                                <div class="flex gutter-between margin-t-5 popup">
                                    <button type="submit" class="btn btn-primary button-success">Create Audit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        @foreach(session('messages') ?? [] as $message)
        (new Toast('{{$message['message'] ?? ''}}', '{{$message['type'] ?? ''}}')).show();
        @endforeach

        $(document).click('.cancel-audit', function (e) {
            $('#form-audit-cancel').attr('action', $(e.target).attr('data-action'));
        });

        function reasonTemplate(reason) {
            if (!reason.id) {
                return reason.text;
            }
            let html = '<div class="padding-10">';
            html += `<h5 class="h6 bold">${reason.reason_text}</h5>`;
            html += '</div>';

            return $(html);
        }

        function reasonSelection(reason) {
            return reason.reason_text
        }

        $('#select2-reason').select2({
            minimumInputLength: 0,
            ajax: {
                url: "{!! route("admin::search.reasons", ["source"=>"audit", "type"=>"create"]) !!}",
                dataType: 'json',
                processResults: (data) => {
                    return {results: data};
                }
            },
            templateResult: reasonTemplate,
            templateSelection: reasonSelection
        });
    </script>
@append
