@extends('admin.master')
<head>
    <style>
        .tag {
            padding: 4px 8px;
            font-size: 11px;
            line-height: 1;
            text-shadow: none;
            background-color: #e1e1e1;
            font-weight: 600;
            color: #575757;
            border-radius: 0.25em;
            text-transform: uppercase;
        }

        .tag.tag-assigned {
            background-color: #ecbd05;
            color: #fff;
        }

        .tag.tag-pending_approval {
            background-color: #f55753;
            color: #fff;
        }

        .tag.tag-pending_action {
            background-color: #f55753;
            color: #fff;
        }

        .tag.tag-resolved {
            background-color: #2fa90e;
            color: #fff;
        }

        .tag.tag-started {
            background-color: #1dbb99;
            color: #fff;
        }

        .tag.tag-approved {
            background-color: #1dbb99;
            color: #fff;
        }
    </style>
</head>
@section('content')

    <div class="container">

        <div class="flex gutter-between cross-center">
            <div>
                <div class="regular light-grey">
                    <a href="{{route('admin::audits.index')}}">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        Audits
                    </a>
                </div>
                <div class="flex cross-center margin-t-10">
                    <div class="h1 margin-r-20 bold">
                        {{$audit->name}}
                    </div>
                    <span class="tag margin-t-10 tag-{{$audit->status}}">{{$audit->status}}</span>
                </div>
                <div class="margin-t-20">
                    <label class="h5">Created By - </label>
                    <span class="h5">{{$audit->createdBy->name}}</span>
                </div>
            </div>
            <div>
                @if(\Niyotail\Services\Audit\AuditService::canChangeAuditStatus($audit, \Niyotail\Models\Audit\Audit::STATUS_CONFIRMED))
                    <button class="btn btn-success confirm-audit" data-modal='#modal-audit-confirm'
                            data-action='{{route('admin::audits.confirm', ['audit'=>$audit])}}'>
                        Confirm Audit
                    </button>
                @endif
                @if(\Niyotail\Services\Audit\AuditService::canChangeAuditStatus($audit, \Niyotail\Models\Audit\Audit::STATUS_STARTED))
                    <button class="btn btn-success start-audit" data-modal='#modal-audit-start'
                            data-action='{{route('admin::audits.start', ['audit'=>$audit])}}'>
                        Start Audit
                    </button>
                @endif
                @if(\Niyotail\Services\Audit\AuditService::canChangeAuditStatus($audit, \Niyotail\Models\Audit\Audit::STATUS_APPROVED))
                    @if($audit->type == \Niyotail\Models\Audit\Audit::TYPE_PRODUCT)
                        <button class="btn btn-success approve-audit" data-modal='#modal-audit-approve'
                                data-action='{{route('admin::audits.approve', ['audit'=>$audit])}}'>
                            Approve Audit
                        </button>
                    @endif
                @endif
                @if(\Niyotail\Services\Audit\AuditService::canChangeAuditStatus($audit, \Niyotail\Models\Audit\Audit::STATUS_COMPLETED))
                    <button class="btn btn-success complete-audit" data-modal='#modal-audit-complete'
                            data-action='{{route('admin::audits.complete', ['audit'=>$audit])}}'>
                        Complete Audit
                    </button>
                @endif
                @if(\Niyotail\Services\Audit\AuditService::canChangeAuditStatus($audit, \Niyotail\Models\Audit\Audit::STATUS_CANCELLED))
                    <button class="btn btn-danger cancel-audit" data-modal="#modal-audit-cancel"
                            data-action='{{route('admin::audits.cancel', ['audit'=>$audit])}}'>
                        Cancel Audit
                    </button>
                @endif
            </div>
        </div>
        <button style="display: none;" id="bulk-assign" data-modal='#modal-audit-assign'></button>
        <button style="display: none;" id="bulk-reject" data-modal='#modal-audit-reject'></button>
        <button style="display: none;" id="bulk-cancel" data-modal='#modal-audit-item-cancel'></button>
        @include('admin.grid')
    </div>
    @include('admin.audit.components.audit._approve_modal')
    @include('admin.audit.components._assign_modal')
    @include('admin.audit.components.audit._cancel_modal')
    @include('admin.audit.components.audit._confirm_modal')
    @include('admin.audit.components._excess_modal')
    @include('admin.audit.components._hold_modal')
    @include('admin.audit.components._loss_modal')
    @include('admin.audit.components._reject_modal')
    @include('admin.audit.components._release_modal')
    @include('admin.audit.components._cancel_modal')
    @include('admin.audit.components.audit._start_modal')
    @include('admin.audit.components.audit._complete_modal')
@endsection


@section('js')
    <script>
        @foreach(session('messages') ?? [] as $message)
        (new Toast('{{$message['message'] ?? ''}}', '{{$message['type'] ?? ''}}')).show();
        @endforeach

        $(document).click('audit-item-action', function (e) {
            $($(e.target).attr('data-form')).attr('action', $(e.target).attr('data-action'));
        });

        $('.approve-audit').click(function (e) {
            $('#form-audit-approve').attr('action', $(e.target).attr('data-action'));
        });
        $('.start-audit').click(function (e) {
            $('#form-audit-start').attr('action', $(e.target).attr('data-action'));
        });
        $('.cancel-audit').click(function (e) {
            $('#form-audit-cancel').attr('action', $(e.target).attr('data-action'));
        });
        $('.complete-audit').click(function (e) {
            $('#form-audit-complete').attr('action', $(e.target).attr('data-action'));
        });
        $('.confirm-audit').click(function (e) {
            $('#form-audit-confirm').attr('action', $(e.target).attr('data-action'));
        });

        $(".grid").on("click", ".bulk-actions .action > a, .btn-custom > a", function (e) {
            e.preventDefault();
            console.log(e);
            var $grid = $(this).closest(".grid")
            var checkboxes = $grid.find(".select-checkbox");
            var selectedCheckboxes = checkboxes.filter(":checked");

            // console.log(selectedCheckboxes);
            if (e.target.innerText === 'Bulk Assign') {
                $('#assign-form').attr('action', "{{route('admin::audits.bulk-assign', ['audit'=>$audit->id])}}");
                $('#assign-form input:hidden[name="selected[]"]').remove()
                for (var i = 0; i < selectedCheckboxes.length; i++) {
                    console.log(selectedCheckboxes[i].value);
                    $('<input>', {
                        type: 'hidden',
                        name: 'selected[]',
                        value: selectedCheckboxes[i].value
                    }).appendTo($('#assign-form'));
                }
                $("#bulk-assign").trigger('click');
            }
            if (e.target.innerText === 'Bulk Cancel') {
                $('#cancel-form').attr('action', "{{route('admin::audits.bulk-cancel', ['audit'=>$audit->id])}}");
                $('#cancel-form input:hidden[name="selected[]"]').remove()
                for (var i = 0; i < selectedCheckboxes.length; i++) {
                    console.log(selectedCheckboxes[i].value);
                    $('<input>', {
                        type: 'hidden',
                        name: 'selected[]',
                        value: selectedCheckboxes[i].value
                    }).appendTo($('#cancel-form'));
                }
                $("#bulk-cancel").trigger('click');
            }
            if (e.target.innerText === 'Bulk Reject') {
                $('#reject-form').attr('action', "{{route('admin::audits.bulk-reject', ['audit'=>$audit->id])}}");
                $('#reject-form input:hidden[name="selected[]"]').remove()
                for (var i = 0; i < selectedCheckboxes.length; i++) {
                    console.log(selectedCheckboxes[i].value);
                    $('<input>', {
                        type: 'hidden',
                        name: 'selected[]',
                        value: selectedCheckboxes[i].value
                    }).appendTo($('#reject-form'));
                }
                $("#bulk-reject").trigger('click');
            }

            return false;
        })
    </script>
@append
