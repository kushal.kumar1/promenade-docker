<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
</head>

<body style="margin: 0;">
<div>
    Modification Requests Process Update<br><br>
    <table style="border-style:solid; border-width:1px; border-color:#000000;">
        <tr>
            <th style="border-style:solid; border-width:1px; border-color:#000000;">Modification Request Type</th>
            <th style="border-style:solid; border-width:1px; border-color:#000000;">ID</th>
            <th style="border-style:solid; border-width:1px; border-color:#000000;">Action</th>
            <th style="border-style:solid; border-width:1px; border-color:#000000;">Status</th>
            <th style="border-style:solid; border-width:1px; border-color:#000000;">Employee</th>
        </tr>

        @foreach($modificationRequests as $modificationRequest)
            <tr>
                <td style="border-style:dotted; border-width:1px; border-color:#000000;">{{$modificationRequest->getModificationRequestType()}}</td>
                <td style="border-style:dotted; border-width:1px; border-color:#000000;">{{$modificationRequest->id}}</td>
                @if($modificationRequest->getModificationRequestType() === \Niyotail\Models\ModificationRequest::TYPE_TRANSACTION)
                    <td style="border-style:dotted; border-width:1px; border-color:#000000;">delete</td>
                @else
                    <td style="border-style:dotted; border-width:1px; border-color:#000000;">{{$modificationRequest->action}}</td>
                @endif
                <td style="border-style:dotted; border-width:1px; border-color:#000000;">{{$modificationRequest->status}}</td>
                <td style="border-style:dotted; border-width:1px; border-color:#000000;">{{$modificationRequest->employee_id}}</td>
            </tr>
        @endforeach

    </table>

</div>
</body>

</html>