<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
</head>

<body style="margin: 0;">
<div>
    A new Modification Request has been created<br><br>
    @switch($modificationRequest->getModificationRequestType())
        @case(\Niyotail\Models\ModificationRequest::TYPE_TRANSACTION)
        Type: Transaction Deletion Request<br>
        ID: {{$modificationRequest->id}}<br>
        Transaction Id: {{$modificationRequest->transaction_id}}<br>
        @break
        @case(\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE)
        @switch($modificationRequest->action)
            @case(\Niyotail\Models\ModificationRequest::ACTION_DELETE)
            Type: Purchase Invoice Deletion Request<br>
            ID: {{$modificationRequest->id}}<br>
            Purchase Invoice Id: {{$modificationRequest->purchase_invoice_id}}<br>
            @break
            @case(\Niyotail\Models\ModificationRequest::ACTION_EDIT)
            Type: Purchase Invoice Modification Request<br>
            ID: {{$modificationRequest->id}}<br>
            Purchase Invoice Id: {{$modificationRequest->purchase_invoice_id}}<br>
            @break
        @endswitch
        @break
        @case(\Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM)
        @switch($modificationRequest->action)
            @case(\Niyotail\Models\ModificationRequest::ACTION_DELETE)
            Type: Purchase Invoice Item Deletion Request<br>
            ID: {{$modificationRequest->id}}<br>
            Purchase Invoice Item Id: {{$modificationRequest->purchase_invoice_item_id}}<br>
            @break
            @case(\Niyotail\Models\ModificationRequest::ACTION_EDIT)
            Type: Purchase Invoice Item Modification Request<br>
            ID: {{$modificationRequest->id}}<br>
            Purchase Invoice Item Id: {{$modificationRequest->purchase_invoice_item_id}}<br>
            @break
        @endswitch
        @break
    @endswitch
</div>
</body>

</html>