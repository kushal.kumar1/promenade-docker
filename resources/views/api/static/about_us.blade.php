<p>We are building a distribution platform to connect manufacturers and retailers directly and efficiently.<p>
<b>Online Presence</b>
<p>
  We provide retailers with our own application for convenient and easy order placement and tracking.
</p>
<b>Offline Presence</b>
<p>We also provide manual sales support through field agents for order collection, push sales, returns and collections.</p>
<b>Our Founders</b>
<p>Kumar Sangeetesh</p>
<p>Sangeetesh was part of IIM Ahmedabad's Golden Jubilee class of 2012. He worked at Accenture Consulting for 4 years managing projects on channel sales and distribution and then led the FMCG vertical for Blackbuck for 2 years.</p>
<p>Sachin Sharma<p>
<p>Sachin graduated from NITIE in 2014. He worked as an Area Sales Manager for DHL for 1 year and then in multiple roles in supply, operations and headed the metals and infra business for Blackbuck for 3 years.</p>
<p>Abhishek Halder<p>
<p>
Abhishek  was part of IIM Ahmedabad's Golden Jubilee class of 2012. He worked with HPCL – Mittal Energy Limited for 6 years in multiple roles ranging from Strategy, Planning and Optimization, Budgeting and Control, Process Design and Automation.
</p>
