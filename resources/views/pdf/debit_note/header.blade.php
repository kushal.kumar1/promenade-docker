<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">

  <head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
      * {
        margin: 0;
        padding: 0;
      }

      body {
        font-family: "sans-serif";
      }

      .main {
        border: 1px solid black;
      }

      .mt-10 {
        margin-top: 10px;
      }

      .pb-10
      {
        padding-bottom: 10px;
      }

      #heading {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
      }

      .user-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        text-transform: none;
      }

      .user-info>div {
        padding: 8px;
        font-size: 12px;
        width: 80%;
      }

      .user-info .billing {
        margin-top: 10px;
      }

      .user-info table {
        width: 100%;
      }

      .user-info table td{
        vertical-align: top;
        padding:4px;
      }

      .user-info .download-app {
        text-align:center;
        padding: 0px;
        margin:0px;
        margin-top: 10px;
      }

      .logo {
        display: block;
        margin: 8px auto;
        width: 20%;
      }

      .flex {
        display: flex;
        display: -webkit-box;
      }

      .flex-between {
        display: flex;
        display: -webkit-box;
        justify-content: space-between;
      }

      .mt-5{
        margin-top: 5px;
      }
      .bold{
        font-weight: bold
      }
    </style>
  </head>
</head>

<body>

@php
  $purchaseOrder = $invoice->purchaseOrder;
@endphp

  <div class="main">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <div id="heading">
      <b>DEBIT NOTE</b>
    </div>
    <div class="user-info">
      <div>
        <div class="billing" style="width: 50%; float:left;">
          @php $warehouse = $invoice->warehouse; @endphp
          <table cellspacing="0" cellpadding="4" style="">
            <tr>
              <td>
                <b>Ref ID</b>: {{$note->ref_id}}<br>
                <b>Date</b>: {{\Carbon\Carbon::parse($note->generated_at)->format('jS F Y')}}<br>
                <b>Financial Year</b>: {{$note->financial_year}}<br>
                {{$warehouse->legal_name}}
                <br>
                {{$warehouse->address}}
                 -
                <br>
                <strong>GSTIN</strong> {{$warehouse->gstin}}
              </td>
            </tr>
          </table>
        </div>
        <div class="billing" style="width: 50%; float:left;">
          @php
            $vendor = $invoice->vendor;
            $vendorAddress = $invoice->vendorAddress;
          @endphp
          <table cellspacing="0" cellpadding="4">
            <tr>
              <td><b>Vendor</b></td>
              <td>{{$vendor->trade_name}}</td>
            </tr>
            <tr>
              <td><b>Address</b></td>
              <td>
                @isset($vendorAddress->name){{$vendorAddress->name}} - @endisset{{$vendorAddress->address}}
                <br>
                <Strong>GSTIN</Strong> {{$vendorAddress->gstin}}
              </td>
            </tr>
            <tr>
              <td><b>Ref No.</b></td>
              <td>{{$invoice->vendor_ref_id}}</td>
            </tr>
            <tr>
              <td><b>Invoice Date</b></td>
              <td>{{\Carbon\Carbon::parse($invoice->created_at)->format('d-M-Y')}}</td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>
<html>
