<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: "sans-serif";
            max-width: 1200px;
            margin: 0 auto;
        }

        table {
            width: 100%;
            height: 100%;
            border-collapse: collapse;
        }

        table tr td {
            font-size: 12px;
            text-align: center;
        }

        table#main {
            border: 1px solid black;
            border-top: none;
            page-break-inside: auto;
        }

        table#main thead {
            background: #EEE;
            font-weight: bolder;
            text-align: center;
            border-bottom: 1px solid black;
            font-size: 16px;
            /* vertical-align: top; */
        }

        table#main tr {
            border: none;
            page-break-inside: avoid;
            page-break-after: auto;
        }

        table#main tr td {
            border-right: solid 1px black;
            border-left: solid 1px black;
            padding: 2px 3px;
        }

        table#main tr td:first-child {
            /* vertical-align: top; */
        }


        table#main tr td:first-child,
        table#main tr td:last-child {
            border: none;
        }

        table#main .tax tr td:first-child {
            vertical-align: middle;
        }

        table#main .tax tr {
            border: none;
        }

        .m-t-10 {
            margin-top: 10px;
        }

        .m-t-20 {
            margin-top: 20px;
        }

        .m-t-70 {
            margin-top: 70px;
        }

        .pd-r-25 {
            padding-right: 25px;
        }

        .flex {
            display: flex;
            display: -webkit-box;
            display: -ms-flexbox;
        }

        .w-70 {
            width: 70%;
        }

        .w-30 {
            width: 30%;
        }

        .w-50 {
            width: 50%;
        }

        .text-right {
            text-align: right;
        }

        .border-top-bottom {
            border-top: 1px solid;
            border-bottom: 1px solid;
        }

        .pd-20 {
            padding: 10px;
        }

        .pd-5 {
            padding-top: 5px;
        }

        .bold {
            font-weight: bolder;
        }

        .summary {
            page-break-inside: avoid;
            font-size: 14px;
        }

        .page-break {
            page-break-after: always;
            page-break-inside: avoid;
        }

        .checkbox {
            border: 1px solid #000;
            width: 10px;
            height: 10px;
        }

        .logo {
            width: 100px;
        }

        .title {
            padding: 5px;
        }

        .order {
            border: 1px solid black;
            margin-bottom: 35px;
            margin-top: 20px;
        }

        .center {
            text-align: center;
        }

        .bold {
            font-weight: bold;
        }

        .heading {

            font-size: 17px;
            text-transform: uppercase;
        }

        .main {
            border: 1px solid black;
        }

        .mt-10 {
            margin-top: 10px;
        }

        #heading {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
            text-align: center;
            font-size: 20px;
            padding-top: 5px;
        }

        .user-info {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            text-transform: uppercase;
        }

        .user-info > div {
            padding: 8px;
            font-size: 12px;
            width: 32%;
        }

        .user-info .billing {
            margin-top: 10px;
        }

        .user-info .download-app {
            text-align: center;
            padding: 0px;
            margin: 0px;
            margin-top: 10px;
        }

        .logo {
            display: block;
            margin: 8px auto;
            width: 20%;
        }

        .flex {
            display: flex;
            display: -webkit-box;
        }

        .flex-between {
            display: flex;
            display: -webkit-box;
            justify-content: space-between;
        }

        .mt-5 {
            margin-top: 5px;
        }

        .bold {
            font-weight: bold
        }

        .tnc {
            display: inline-block;
        }

    </style>
</head>

<body>
<div class="center">
    @if(\Illuminate\Support\Facades\App::environment() == 'local')
        <img src="{{asset('niyotail.png')}}" class="logo">
    @endif
    <h1 class="heading">Shipping Manifest #{{$manifest->id}}</h1>
    <div>
        Agent: {{$manifest->agent->name}} ({{$manifest->agent->code}}) / Vehicle : {{$manifest->vehicle_number}}
    </div>
    <div>
        Generated On: {{\Carbon\Carbon::parse($manifest->created_at)->format("d-m-Y h:i a")}}
    </div>
</div>
<div class="order">
    <table>
        <tr class="bold">
            <th>#</th>
            <th>Store</th>
            <th>Order ID</th>
            <th>Shipment ID</th>
            <th>Boxes</th>
            <th>Invoice No.</th>
            <th>Amount</th>
            <th>Returns</th>
            <th>Collection</th>
        </tr>
        @foreach($manifest->shipments as $shipment)
            <tr>
                <td>{{$loop->index+1}}</td>
                <td>{{$shipment->order->store->name}}</td>
                <td>{{$shipment->order->reference_id}}</td>
                <td>{{$shipment->id}}</td>
                <td>{{$shipment->boxes}}</td>
                <td>{{$shipment->invoice->reference_id}}</td>
                <td>Rs. {{number_format($shipment->invoice->amount, 2)}}</td>
                <td class="center">
                    <div class="checkbox" style="margin: 0 auto;">
                    </div>
                </td>
                <td class="center">
                    <div class="checkbox" style="margin: 0 auto;">
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
</div>
<div class="page-break"></div>

@foreach($manifest->shipments as $shipment)
    @if($shipment->status != \Niyotail\Models\Shipment::STATUS_CANCELLED && $shipment->status != \Niyotail\Models\Shipment::STATUS_READY_TO_SHIP && $shipment->status != \Niyotail\Models\Shipment::STATUS_GENERATED)
        @php
            $balance = empty($shipment->invoice->order->store->totalBalance) ? "0" : $shipment->invoice->order->store->totalBalance->balance_amount;
        @endphp
        <div class="main">
            <img src="{{asset('niyotail.png')}}" class="logo">
            <div id="heading">
                <b>GST INVOICE</b>
            </div>
            <div class="user-info">
                <div>
                    <div>
                        <b>Order ID : </b> {{$shipment->invoice->order->reference_id}}
                    </div>
                    <div class="billing">
                        <p>
                            Billing Address:
                        </p>
                        <b>{{$shipment->invoice->order->store->name}}</b> <br>
                        {!!nl2br(e($shipment->invoice->order->billingAddress->address))!!}<br>
                        {{$shipment->invoice->order->billingAddress->city->name}}
                        - {{$shipment->invoice->order->billingAddress->pincode}}<br/>
                        Contact: {{$shipment->invoice->order->billingAddress->mobile}}<br/>
                        @if(!empty($shipment->invoice->order->store->gstin))
                            GSTIN: {{$shipment->invoice->order->store->gstin}}
                        @endif
                    </div>
                    {{--        <div class="mt-5">--}}
                    {{--          {!!DNS1D::getBarcodeSvg("$invoice->reference_id", "C128",1.2, 40)!!}--}}
                    {{--        </div>--}}
                    <div class="mt-5">
                        <p>
                            <b>Total Outstanding: </b>&#x20b9; {{number_format($balance)}}*
                        </p>
                        <p>
                            <b>Total Invoices: </b>{{$shipment->invoice->order->store->invoices->count()}}*
                        </p>
                        <p style="font-size:12px;text-transform:lowercase">
                            * including current invoice
                        </p>
                    </div>
                </div>

                {{--      <div class="download-app">--}}
                {{--        <img src="{{public_path() . '/download-app.jpeg'}}" style="width: 120px;">--}}
                {{--        <p><b>(Download App & order above &#x20b9; 6000 to get additional 1% off)</b></p>--}}
                {{--      </div>--}}

                <div>
                    <p>
                        <b>Invoice No:</b> {{$shipment->invoice->reference_id}} <br>
                        <b>Invoice Date:</b> {{(new DateTime($shipment->invoice->created_at))->format("d-M-Y")}}<br>
                    </p>
                    <p class="mt-10">
                        <b>AREA / STORE ID: </b>{{$shipment->invoice->order->store->beat->name}}
                        / {{$shipment->invoice->order->store->id}}
                    </p>

                    <p class="mt-10">
                        Shipping Address:
                    </p>
                    <b>{{$shipment->invoice->order->store->name}}</b> <br>
                    {!!nl2br(e($shipment->invoice->order->shippingAddress->address))!!}<br>
                    {{$shipment->invoice->order->shippingAddress->city->name}}
                    - {{$shipment->invoice->order->shippingAddress->pincode}}<br/>
                    Contact: {{$shipment->invoice->order->shippingAddress->mobile}}
                </div>
            </div>
        </div>
        <div>
            @php
                $totalQuantity=$subtotal=$discount=$total=0;
                $orderItems=$shipment->orderItems;

                $allTaxes=$orderItems->pluck('taxes')->flatten();
                if($allTaxes->isNotEmpty())
                  $taxes=$allTaxes->groupBy('name')->keys()->all();
                $totalTaxes = 0;
                $groupedItems=$orderItems->groupBy('sku');
                $total=$orderItems->sum('total');
            @endphp
            <table id="main">
                <thead>
                <td><b>#</b></td>
                <td style="text-align:left;">Product</td>
                <td>Type</td>
                <td>Qty</td>
                <td>HSN</td>
                <td style="text-align:right;">MRP <br/>(&#x20b9;)</td>
                <td style="text-align:right;">Rate <br/>(&#x20b9;)</td>
                <td style="text-align:right;">Subtotal <br/>(&#x20b9;)</td>
                <td style="text-align:right;">Discount <br/>(&#x20b9;)</td>
                @if(!empty($taxes))
                    @foreach ($taxes as $tax)
                        <td>{{$tax}}
                            <table class="tax">
                                <tr>
                                    <th style="text-align:left">(%)</th>
                                    <th style="text-align:right">(&#x20b9;)</th>
                                </tr>
                            </table>
                        </td>
                    @endforeach
                @endif
                <td style="text-align:right;">Amount <br/>(&#x20b9;)</td>
                </thead>
                @php $counter=0;
                @endphp
                @foreach ($groupedItems as $sku => $items)

                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td style="text-align:left;">{{$items->first()->product->name}}</td>
                        <td>{{$items->first()->productVariant->value}}</td>
                        <td>{{$items->sum('quantity')}}</td>
                        <td>{{$items->first()->product->hsn_sac_code}}</td>
                        <td style="text-align:right;">{{number_format($items->first()->mrp)}}</td>
                        <td style="text-align:right;">{{number_format($items->first()->price - $items->first()->tax,2)}}</td>
                        <td style="text-align:right;">{{number_format($items->sum('subtotal'),2)}}</td>
                        <td style="text-align:right;">{{number_format($items->sum('discount'),2)}}</td>
                        @if(!empty($taxes))
                            @foreach ($taxes as $tax)
                                <?php
                                $taxAmount = $items->sum(function ($item) use ($tax) {
                                    $taxes = $item->taxes->where('name', $tax);
                                    if ($taxes->isNotEmpty())
                                        return $taxes->sum('amount');
                                    return 0.00;
                                });

                                $itemTax = $items->first()->taxes()->where('name', $tax)->first();

                                if (empty($itemTax)) {
                                    $taxPercentage = 0.00;
                                } else $taxPercentage = $itemTax->percentage;
                                $totalTaxes += $taxAmount;
                                ?>
                                <td>
                                    <table class="tax">
                                        <tr>
                                            <td style="text-align:left">{{number_format($taxPercentage,1)}}</td>
                                            <td style="text-align:right">
                                                {{number_format($taxAmount,2)}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            @endforeach
                        @endif
                        <td style="text-align:right;">{{number_format($items->sum('total'),2)}}</td>
                    </tr>
                @endforeach
            </table>

            <div class='summary'>
                <div class="flex m-t-10 pd-5">
                    <div class="w-70">
                    </div>
                    <div class="w-30">
                        <div class="flex">
                            <div class="w-50">
                                <b> SUBTOTAL</b>
                            </div>
                            <div class="w-50 text-right pd-r-25">
                                <b>&#x20b9; {{number_format($orderItems->sum('subtotal'),2)}}</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex pd-5">
                    <div class="w-70">
                    </div>
                    <div class="w-30">
                        <div class="flex">
                            <div class="w-50">
                                <b>DISCOUNT</b>
                            </div>
                            <div class="w-50 text-right pd-r-25">
                                <b>&#x20b9; {{number_format($orderItems->sum('discount'),2)}}</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex pd-5">
                    <div class="w-70">
                    </div>
                    <div class="w-30">
                        <div class="flex">
                            <div class="w-50">
                                <b>TAX</b>
                            </div>
                            <div class="w-50 text-right pd-r-25">
                                <b>&#x20b9; {{number_format($orderItems->sum('tax'),2)}}</b>
                            </div>
                        </div>
                    </div>
                </div>

                @if($total != $shipment->invoice->amount)
                    <div class="flex pd-5">
                        <div class="w-70">
                        </div>
                        <div class="w-30">
                            <div class="flex">
                                <div class="w-50">
                                    <b>ROUND OFF (+/-)</b>
                                </div>
                                <div class="w-50 text-right pd-r-25">
                                    <b>&#x20b9; {{number_format(($shipment->invoice->amount-$total),2)}}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="flex border-top-bottom pd-5" style="margin-top:5px">
                    <div class="w-70">
                        &nbsp;Rs {{Niyotail\Helpers\Utils::getAmountInWords($shipment->invoice->amount)}} Only
                    </div>

                    <div class="w-30">
                        <div class="flex">
                            <div class="w-50">
                                <b>GRAND TOTAL</b>
                            </div>
                            <div class="w-50 text-right pd-r-25">
                                <b>&#x20b9; {{number_format($shipment->invoice->amount)}}</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main">
            <div class="flex">
                <div class="w-40">
                    PAYMENTS:<br>
                    By Bank Transfer:<br>
                    <b>Odicea Distribution Technologies Pvt. Ltd.</b><br>
                    <b>Account:</b> 017105009556<br>
                    <b>IFSC:</b> ICIC0003890<br>
                </div>

                <div class="w-60">
                    <div class="flex">


                        <div class="w-70">
                            <div style="margin-left:60px;">
                                <div style="padding-top:30px"><b>Authorised Signatory</b></div>
                                <div style="margin-top:5px"><b>ODICEA DISTRIBUTION TECHNOLOGIES PVT. LTD.</b></div>
                                <p style="text-align:left;margin-top:5px">
                                    KHASRA NO. 213, MUSTIL NO. 31, KILA NO. 8,<br>
                                    SIHI VILLAGE, GURGAON 122004 HARYANA<br>
                                    Phone: +91 8368776202 | GSTIN: 06AACCO8394A1ZH<br>
                                    Website:www.niyotail.com | Email: support
                                    @niyotail.com
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div style="padding-top:10px;">
                <p class="bold">Terms & Conditions:</p>
                <div class="tnc">
                    <span>&bull;</span>
                    <span class="mr-5">Goods once sold will not be taken back or exchanged.</span>
                    <span> &bull;</span>
                    <span class="mr-5">Bills not paid by the due date will attract 24% Interest.</span>
                    <span> &bull; </span>
                    <span class="mr-5">All disputes subject to New Delhi Jurisdiction only.</span>
                    <span> &bull;</span>
                    <span class="mr-5">Cheques dishonoured/bounced on submission on or before the end of cheque validity will attract penalities of Rs. 500 per dishonour incident exclusive of applicable GST.</span>
                </div>

            </div>
        </div>
        @if(!$loop->last)
            <div class="page-break"></div>
        @endif
    @endif
@endforeach
</body>

</html>
