<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <head>
        <meta charset="utf-8">
        <style type="text/css" media="all">
            * {
                margin: 0;
                padding: 0;
            }

            body {
                font-family: "sans-serif";
            }

            .main {
                border: 1px solid black;
            }

            .mt-10 {
                margin-top: 10px;
            }

            #heading {
                border-top: 1px solid black;
                border-bottom: 1px solid black;
                text-align: center;
                font-size: 20px;
                padding-top: 5px;
            }

            .user-info {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-pack: justify;
                -ms-flex-pack: justify;
                justify-content: space-between;
                text-transform: uppercase;
            }

            .user-info>div {
                padding: 8px;
                font-size: 12px;
                width: 32%;
            }

            .user-info .billing {
                margin-top: 10px;
            }

            .user-info .download-app {
                text-align:center;
                padding: 0px;
                margin:0px;
                margin-top: 10px;
            }

            .logo {
                display: block;
                margin: 8px auto;
                width: 20%;
            }

            .flex {
                display: flex;
                display: -webkit-box;
            }

            .flex-between {
                display: flex;
                display: -webkit-box;
                justify-content: space-between;
            }

            .mt-5{
                margin-top: 5px;
            }
            .bold{
                font-weight: bold
            }
        </style>
    </head>
</head>

<body>
<div class="main">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <div id="heading">
        <b>Delivery Challan</b>
    </div>
    <div class="user-info">
        <div>
            <div>
                <b>Challan ID : </b> {{$stockTransferNote->reference_id}}
            </div>
            @if(!empty($stockTransferNote->stockTransfer->eway_bill_no))
                <div>
                    <b>Eway Bill No : </b> {{$stockTransferNote->stockTransfer->eway_bill_no}}
                </div>
            @endif

            @if(!empty($stockTransferNote->stockTransfer->vehicle_number))
                <div>
                    <b>Vehicle Number : </b> {{$stockTransferNote->stockTransfer->vehicle_number}}
                </div>
            @endif
            <div class="billing">
                <p>
                    From :
                </p>
                <b>{{$stockTransferNote->stockTransfer->fromWarehouse->legal_name}}</b> <br>
                {!!nl2br(e($stockTransferNote->stockTransfer->fromWarehouse->address))!!}<br>
                {{$stockTransferNote->stockTransfer->fromWarehouse->city->name}} - {{$stockTransferNote->stockTransfer->fromWarehouse->pincode}}<br />
            </div>
        </div>

        <div>
            <p>
                <b>Date:</b> {{(new DateTime($stockTransferNote->created_at))->format("d-M-Y")}}<br>
            </p>

            <p class="mt-10">
                To :
            </p>
            <b>{{$stockTransferNote->stockTransfer->toWarehouse->legal_name}}</b> <br>
            {!!nl2br(e($stockTransferNote->stockTransfer->toWarehouse->address))!!}<br>
            {{$stockTransferNote->stockTransfer->toWarehouse->city->name}} - {{$stockTransferNote->stockTransfer->toWarehouse->pincode}}<br />
        </div>
    </div>
</div>
</body>
<html>
