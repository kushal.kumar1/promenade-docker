<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: "sans-serif";
            max-width: 1200px;
            margin: 0 auto;
        }

        table {
            width: 100%;
            height: 100%;
            border-collapse: collapse;
        }

        table tr td {
            font-size: 12px;
            text-align: center;
        }

        table#main {
            border: 1px solid black;
            border-top: none;
            page-break-inside: auto;
        }

        table#main thead {
            background: #EEE;
            font-weight: bolder;
            text-align: center;
            border-bottom: 1px solid black;
            font-size: 16px;
            /* vertical-align: top; */
        }

        table#main tr {
            border: none;
            page-break-inside: avoid;
            page-break-after: auto;
        }

        table#main tr td {
            border-right: solid 1px black;
            border-left: solid 1px black;
            padding: 2px 3px;
        }

        table#main tr td:first-child {
            /* vertical-align: top; */
        }


        table#main tr td:first-child,
        table#main tr td:last-child {
            border: none;
        }

        table#main .tax tr td:first-child {
            vertical-align: middle;
        }

        table#main .tax tr {
            border: none;
        }

        .m-t-10 {
            margin-top: 10px;
        }

        .m-t-20 {
            margin-top: 20px;
        }

        .m-t-70 {
            margin-top: 70px;
        }

        .pd-r-25 {
            padding-right: 25px;
        }

        .flex {
            display: flex;
            display: -webkit-box;
            display: -ms-flexbox;
        }

        .w-70 {
            width: 70%;
        }

        .w-30 {
            width: 30%;
        }

        .w-50 {
            width: 50%;
        }

        .text-right {
            text-align: right;
        }

        .border-top-bottom {
            border-top: 1px solid;
            border-bottom: 1px solid;
        }

        .pd-20 {
            padding: 10px;
        }

        .pd-5 {
            padding-top: 5px;
        }

        .bold {
            font-weight: bolder;
        }

        .summary {
            page-break-inside: avoid;
            font-size: 14px;
        }
    </style>
</head>

<body>
<div>
    <table id="main">
        <thead>
        <td><b>#</b></td>
        <td style="text-align:left;">Product</td>
        <td>Requested Qty</td>
        <td>Confirmed Qty</td>
        <td>Sent Qty</td>
        <td>MRP</td>
        <td>HSN</td>
        <td style="text-align:right;">Amount <br />(&#x20b9;)</td>
        </thead>
        @foreach ($stockTransferNote->stockTransfer->items as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td style="text-align:left;">{{$item->product->name}}</td>
                <td>{{$item->requested_quantity}}</td>
                <td>{{$item->confirmed_quantity}}</td>
                <td>{{$item->sent_quantity}}</td>
                <td>{{$item->product->unitVariant->mrp}}</td>
                <td>{{$item->product->hsn_sac_code}}</td>
                <td style="text-align:right;">{{number_format($item->product->unitVariant->mrp * $item->sent_quantity)}}</td>
            </tr>
        @endforeach
    </table>

    <div class='summary'>
            <div class="flex pd-5">
                <div class="w-70">
                </div>
            </div>

        <div class="flex border-top-bottom pd-5" style="margin-top:5px">
            <div class="w-70">
                &nbsp;Rs {{Niyotail\Helpers\Utils::getAmountInWords($stockTransferNote->total)}} Only
            </div>

            <div class="w-30">
                <div class="flex">
                    <div class="w-50">
                        <b>GRAND TOTAL</b>
                    </div>
                    <div class="w-50 text-right pd-r-25">
                        <b>&#x20b9; {{number_format($stockTransferNote->total)}}</b>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>

</html>
