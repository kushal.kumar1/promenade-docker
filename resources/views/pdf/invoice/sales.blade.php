<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: "sans-serif";
      max-width: 1200px;
      margin: 0 auto;
    }

    table {
      width: 100%;
      height: 100%;
      border-collapse: collapse;
    }

    table tr td {
      font-size: 12px;
      text-align: center;
    }

    table#main {
      border: 1px solid black;
      border-top: none;
      page-break-inside: auto;
    }

    table#main thead {
      background: #EEE;
      font-weight: bolder;
      text-align: center;
      border-bottom: 1px solid black;
      font-size: 16px;
      /* vertical-align: top; */
    }

    table#main tr {
      border: none;
      page-break-inside: avoid;
      page-break-after: auto;
    }

    table#main tr td {
      border-right: solid 1px black;
      border-left: solid 1px black;
      padding: 2px 3px;
    }

    table#main tr td:first-child {
      /* vertical-align: top; */
    }


    table#main tr td:first-child,
    table#main tr td:last-child {
      border: none;
    }

    table#main .tax tr td:first-child {
      vertical-align: middle;
    }

    table#main .tax tr {
      border: none;
    }

    .center {
      text-align: center;
    }

    .m-t-10 {
      margin-top: 10px;
    }

    .m-t-20 {
      margin-top: 20px;
    }

    .m-t-70 {
      margin-top: 70px;
    }

    .pd-r-25 {
      padding-right: 25px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
      display: -ms-flexbox;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-50 {
      width: 50%;
    }

    .text-right {
      text-align: right;
    }

    .border-top-bottom {
      border-top: 1px solid;
      border-bottom: 1px solid;
    }

    .pd-20 {
      padding: 10px;
    }

    .pd-5 {
      padding-top: 5px;
    }

    .bold {
      font-weight: bolder;
    }

    .summary {
        page-break-inside: avoid;
        font-size: 14px;
    }

    .table-border, .table-border th, .table-border tr {
      border: solid 1px;
      padding: 1px;
    }

    .table-border td {
      padding: 1px;
    }

    .additional-info {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      text-transform: uppercase;
    }

    .additional-info>div {
      padding: 8px;
      font-size: 12px;
      width: 50%;
    }

  </style>
</head>

<body>
  <div>
    @php
    $totalQuantity=$subtotal=$discount=$total=0;
    $orderItems=$invoice->shipment->orderItems;
    $orderAdditionalInfo = $invoice->order->additionalInfo;
    $customerInfo = isset($orderAdditionalInfo->customer_info) ? json_decode($orderAdditionalInfo->customer_info, true) : array();

    $taxSummary = [];
    $allTaxes=$orderItems->pluck('taxes')->flatten();
    if($allTaxes->isNotEmpty())
      $taxes=$allTaxes->groupBy('name')->keys()->all();
    $totalTaxes = 0;
    $groupedItems=$orderItems->groupBy('sku');
    $total=$orderItems->sum('total');
    @endphp
    <table id="main">
      <thead>
        <td><b>#</b></td>
        <td style="text-align:left;">Product</td>
        <td>Barcode</td>
        <td>Type</td>
        <td>Qty</td>
        <td>HSN</td>
        <td style="text-align:right;">MRP <br />(&#x20b9;)</td>
        <td style="text-align:right;">Rate <br />(&#x20b9;)</td>
        <td style="text-align:right;">Subtotal <br />(&#x20b9;)</td>
        <td style="text-align:right;">Discount <br />(&#x20b9;)</td>
        @if(!empty($taxes))
        @foreach ($taxes as $tax)
        <td>{{$tax}}
          <table class="tax">
            <tr>
              <th style="text-align:left">(%)</th>
              <th style="text-align:right">(&#x20b9;)</th>
            </tr>
          </table>
        </td>
        @endforeach
        @endif
        <td style="text-align:right;">Amount <br />(&#x20b9;)</td>
      </thead>
      @php $counter=0;
      @endphp
      @foreach ($groupedItems as $sku => $items)
      <?php
        $subTotal = $items->sum('subtotal');
        $hsn = $items->first()->product->hsn_sac_code;
      ?>
      <tr>
        <td>{{$loop->iteration}}</td>
        <td style="text-align:left;">{{$items->first()->product->name}} (total {{$items->sum('quantity')*$items->first()->productVariant->quantity}} units)</td>
        <td>{{$items->first()->product->barcode}}</td>
        <td>{{$items->first()->productVariant->value}}</td>
        <td>{{$items->sum('quantity')}}</td>
        <td>{{$items->first()->product->hsn_sac_code}}</td>
        <td style="text-align:right;">{{number_format($items->first()->mrp)}}</td>
        <td style="text-align:right;">{{number_format($items->first()->price - $items->first()->tax,2)}}</td>
        <td style="text-align:right;">{{number_format($items->sum('subtotal'),2)}}</td>
        <td style="text-align:right;">{{number_format($items->sum('discount'),2)}}</td>
        @if(!empty($taxes))
        <?php
            $itemTaxInfo = $items->first()->taxes()->first();
            $taxClassPercentage = 0.00;
            if(!empty($itemTaxInfo) &&  ($itemTaxInfo->name == 'CGST' || $itemTaxInfo->name == 'SGST')){
                $taxClassPercentage = $itemTaxInfo->percentage*2;
            } else if(!empty($itemTaxInfo) &&  $itemTaxInfo->name == 'IGST'){
                $taxClassPercentage = $itemTaxInfo->percentage;
            }
            $taxClassName = 'GST'.$taxClassPercentage;
        ?>
        @foreach ($taxes as $tax)
        <?php
            $taxAmount = $items->sum(function ($item) use ($tax) {
            $taxes = $item->taxes->where('name', $tax);
            if ($taxes->isNotEmpty())
              return $taxes->sum('amount');
            return 0.00;
            });

            $itemTax = $items->first()->taxes()->where('name', $tax)->first();
            if(empty($itemTax)) {
              $taxPercentage  = 0.00;
            }
            else $taxPercentage = $itemTax->percentage;
            $totalTaxes += $taxAmount;

            if(isset($taxSummary[$hsn][$taxClassName][$tax])){
                $taxSummary[$hsn][$taxClassName][$tax] += $taxAmount;
            }else{
                $taxSummary[$hsn][$taxClassName][$tax] = $taxAmount;
            }
        ?>
        <td>
          <table class="tax">
            <tr>
              <td style="text-align:left">{{number_format($taxPercentage,1)}}</td>
              <td style="text-align:right">
                {{number_format($taxAmount,2)}}
              </td>
            </tr>
          </table>
        </td>
        @endforeach
        <?php
            if(isset($taxSummary[$hsn][$taxClassName]['taxable_value'])){
                $taxSummary[$hsn][$taxClassName]['taxable_value'] += $subTotal;
            }else{
                $taxSummary[$hsn][$taxClassName]['taxable_value'] = $subTotal;
            }
        ?>
        @endif
        <td style="text-align:right;">{{number_format($items->sum('total'),2)}}</td>
      </tr>
      @endforeach
    </table>
<div class='summary'>
  <div class="flex m-t-10 pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b> SUBTOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($orderItems->sum('subtotal'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>DISCOUNT</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($orderItems->sum('discount'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>TAX</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($orderItems->sum('tax'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  @if($total != $invoice->amount)
  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>ROUND OFF (+/-)</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format(($invoice->amount-$total),2)}}</b>
        </div>
      </div>
    </div>
  </div>
  @endif

  <div class="flex border-top-bottom pd-5" style="margin-top:5px">
    <div class="w-70">
      &nbsp;Rs {{Niyotail\Helpers\Utils::getAmountInWords($invoice->amount)}} Only
    </div>

    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>GRAND TOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($invoice->amount)}}</b>
        </div>
      </div>
    </div>
  </div>



  <div class="additional-info border-top">
    @if(!empty($taxes))
    <div>
      <h3 class="center pd-20">TAX SUMMARY</h3>

      <table class="table-border">
        <tr>
          <th>HSN/SAC</th>
          <th>TAX CLASS</th>
          <th>TAXABLE VALUE</th>
          <th>
            <table style="border:none">
              <tr style="border:none">
                <td colspan="3">TAX AMOUNT</td>
              </tr>
              <tr style="border:none">
                @foreach ($taxes as $tax)
                  <td>{{$tax}}</td>
                @endforeach
              </tr>
            </table>
          </th>
        </tr>
        @foreach ($taxSummary as $hsn => $taxValue)
          @foreach($taxValue as $taxClass => $taxData)
          <tr>
            <td>{{$hsn}}</td>
            <td>{{$taxClass}} %</td>
            <td>&#x20b9; {{number_format($taxData['taxable_value'],2)}}</td>
            <td>
              <table style="border:none">
                <tr style="border:none">
                  @foreach ($taxes as $tax)
                    <td>&#x20b9; {{number_format($taxData[$tax],2)}}</td>
                  @endforeach
                </tr>
              </table>
            </td>
          </tr>
          @endforeach
        @endforeach
      </table>
    </div>
    @endif
    <div>
      @if(!empty($orderAdditionalInfo->pos_order_id))
        <h3 class="center pd-20">ADDITIONAL INFORMATION</h3>
        <div>
          <div>
            <b>1K MALL ORDER ID : </b> {{$orderAdditionalInfo->pos_order_id}}
          </div>

          @isset($customerInfo['name'])
          <div class="pd-5">
            <b>CUSTOMER NAME : </b> {{$customerInfo['name']}}
          </div>
          @endisset

          @isset($customerInfo['mobile'])
          <div class="pd-5">
            <b>CUSTOMER CONTACT NUMBER : </b> {{$customerInfo['mobile']}}
          </div>
          @endisset

          @isset($customerInfo['due_amount'])
          <div class="pd-5">
            <b>AMOUNT TO BE COLLECTED FROM CUSTOMER : </b>&#x20b9; {{$customerInfo['due_amount']}}
          </div>
          @endisset

          <div class="pd-5">{!!DNS1D::getBarcodeSvg("$orderAdditionalInfo->pos_order_id", "C128",1.6, 50)!!}</div>
        </div>
      @endif
    </div>
  </div>
</div>

</div>
</body>

</html>
