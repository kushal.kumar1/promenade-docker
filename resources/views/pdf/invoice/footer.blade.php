<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: 'sans-serif';
      font-size: 11px;
    }

    .main {
      border: 1px solid black;
      padding: 5px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
    }

    .flex-between {
      display: flex;
      display: -webkit-box;
      justify-content: space-between;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-40 {
      width: 40%;
    }

    .w-60 {
      width: 60%;
    }

    .bold {
      font-weight: bolder;
    }

    .mr-5 {
      margin-right: 5px;
    }

    .mr-10 {
      margin-right: 10px;
    }

    .mt-5 {
      margin-top: 5px;
    }

    .tnc {
      display: inline-block;

    }
  </style>
  <script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);
          }
      }
      var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
      for (var css_class in css_selector_classes) {
          if (css_selector_classes.hasOwnProperty(css_class)) {
              var element = document.getElementsByClassName(css_selector_classes[css_class]);
              for (var j = 0; j < element.length; ++j) {
                  element[j].textContent = vars[css_selector_classes[css_class]];
              }
          }
      }
  }
  </script>
</head>

<body onload="subst()">
    <div style="padding-top:10px;">
      <p class="bold">Terms & Conditions:</p>
      <div class="tnc">
          <span>&bull;</span>
          <span class="mr-5">Goods once sold will not be taken back or exchanged.</span>
          <span> &bull;</span>
          <span class="mr-5">Bills not paid by the due date will attract 24% Interest.</span>
          <span> &bull; </span>
          <span class="mr-5">Goods need to be validated by retailers at the point of delivery.</span>
          <span> &bull; </span>
          <span class="mr-5">Any later claim of the short item received, free item not given, or wrong product received will not be entertained.</span>
          <span> &bull; </span>
          <span class="mr-5">Any discrepancy in Goods Quantity/Quality should be reported on the invoice at the time of delivery.</span>
          <span> &bull; </span>
          <span class="mr-5">All disputes subject to New Delhi Jurisdiction only.</span>
          <span> &bull;</span>
          <span class="mr-5">Cheques dishonoured/bounced on submission on or before the end of cheque validity will attract penalities of Rs. 500 per dishonour incident exclusive of applicable GST.</span>
      </div>

    </div>
  </div>
  <div style="text-align:center;font-size:14px; margin-top: 5px">
     Page <span class="page"></span> of <span class="topage"></span>
  </div>
</body>

</html>
