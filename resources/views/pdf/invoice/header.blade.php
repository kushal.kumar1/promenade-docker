<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">

  <head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
      * {
        margin: 0;
        padding: 0;
      }

      body {
        font-family: "sans-serif";
      }

      .main {
        border: 1px solid black;
      }

      .mt-10 {
        margin-top: 10px;
      }

      #heading {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
      }

      .text-center {
        text-align:center;
      }

      .user-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        text-transform: uppercase;
      }

      .user-info>div {
        padding: 8px;
        font-size: 12px;
        width: 32%;
      }

      .user-info .billing {
        margin-top: 10px;
      }

      .user-info .download-app {
        text-align:center;
        padding: 0px;
        margin:0px;
        margin-top: 10px;
      }

      .company-info {
        padding:5px;
      }

      .company-info>div {
        font-size: 14px;
        display: inline-block;
      }

      .company-info div:nth-child(1) {
        width: 15%;
      }

      .company-info div:nth-child(2) {
        width: 70%;
      }

      .logo {
        display: block;
        width: 70px;
      }

      .flex {
        display: flex;
      }

      .text-right {
        text-align: right;
      }

      .text-right>div{
        display: inline-block;
        margin-right: 10px;
      }

      .flex-between {
        display: flex !important;
        justify-content: space-between;
      }

      .mt-5{
        margin-top: 5px;
      }
      .bold{
        font-weight: bold
      }
    </style>
  </head>
</head>

<body>
  <div class="main">
    <div class="company-info">
      <div><img src="{{secure_asset('1k-kirana.jpeg')}}" class="logo"></div>
      <div class="text-center"><h3>ODICEA DISTRIBUTION TECHNOLOGIES PVT. LTD.</h3>
        <p class="mt-5"><b>Address:</b> {{$invoice->order->warehouse->address}} {{$invoice->order->warehouse->pincode}}</p>
        <p><b>GSTIN:</b> {{$invoice->order->warehouse->gstin}}</p>
        <p class="mt-5"><b>Website: </b>www.1knetworks.com | <b>Email: </b>suppport@1knetworks.com</p>
      </div>
      <div></div>
    </div>
    <div id="heading">
      <b>TAX INVOICE</b>
    </div>
    <div class="user-info">
      <div>
        <div>
          <?php $order_reference_id = $invoice->order->reference_id; ?>
          <b>Order ID : </b> {{$order_reference_id}}
        </div>
        <div class="billing">
          <p>
            Billing Name and Address:
          </p>
          <b>{{$invoice->order->store->name}}</b> <br>
          {!!nl2br(e($invoice->order->billingAddress->address))!!}<br>
          {{$invoice->order->billingAddress->city->name}} - {{$invoice->order->billingAddress->pincode}}<br />
          Contact: {{$invoice->order->billingAddress->mobile}}<br />
          @if(!empty($invoice->order->store->gstin))
            GSTIN: {{$invoice->order->store->gstin}}
            @endif

          <br><b>PLACE OF SUPPLY:</b> {{$invoice->order->billingAddress->city->state->name}}-{{$invoice->order->billingAddress->city->state->code}}
        </div>
{{--        <div class="mt-5">--}}
{{--          {!!DNS1D::getBarcodeSvg("$invoice->reference_id", "C128",1.2, 40)!!}--}}
{{--        </div>--}}
      </div>

{{--      <div class="download-app">--}}
{{--        <img src="{{public_path() . '/download-app.jpeg'}}" style="width: 120px;">--}}
{{--        <p><b>(Download App & order above &#x20b9; 6000 to get additional 1% off)</b></p>--}}
{{--      </div>--}}

      <div>
          <p>
            <b>Invoice No:</b> {{$invoice->reference_id}} <br>
            <b>Invoice Date:</b> {{(new DateTime($invoice->created_at))->format("d-M-Y")}}<br>
            @if(!empty($invoice->eway_bill_number))
            <b>E-way Bill Number:</b> {{$invoice->eway_bill_number}}<br>
            @endif
          </p>
          <p class="mt-10">
            <b>AREA / STORE ID: </b>{{$invoice->order->store->beat->name}} / {{$invoice->order->store->id}}
          </p>

          <p class="mt-10">
            Shipping Address:
          </p>
          <b>{{$invoice->order->store->name}}</b> <br>
          {!!nl2br(e($invoice->order->shippingAddress->address))!!}<br>
          {{$invoice->order->shippingAddress->city->name}} - {{$invoice->order->shippingAddress->pincode}}<br />
          Contact: {{$invoice->order->shippingAddress->mobile}}
      </div>
      <div class="text-right">{!!DNS2D::getBarcodeHTML("$order_reference_id", 'QRCODE', 7, 7)!!}</div>
    </div>
  </div>
</body>
<html>
