<html>

<head>
    <style>
        .page-break {
            clear: left;
            display: block;
            page-break-after: always;
        }

        body,
        html {
            height: 100%;
            margin: 0;
            color: #000;
            padding: 0;
            font-family: "arial";
        }

        td {
            text-align: center;
            padding: 3px;
        }

        table {
            width: 100%;
            height: 100%;
            border: 1px solid;
        }
    </style>
</head>

<body style="margin:0; padding:0;">
    @for ($i = 1; $i <= $qty/2; $i++)
        <div style="width: 100%">
        <div style="display:inline-block; margin-left: 10px; margin-top: 4px; width: 46%; ">
            <div style="margin-top:5px;font-size:12px">
                <span>{{substr($product->name, 0, 32)}}</span>
            </div>
            <div style="margin-top:2px; font-size: 10px;">
                <span>Packed: {{\Carbon\Carbon::now('Asia/KolKata')->format('d-M-y')}}</span>
                <span style="float:right">Expiry: {{\Carbon\Carbon::now('Asia/KolKata')->addMonths(6)->subDay(1)->format('d-M-y')}}</span>
            </div>
            <div style="margin-top:2px;font-size:10px">
                <span>FSSAI: 10819005000097</span>
                <span style="float:right">PID:&nbsp;{{$product->id}}</span>
            </div>
            <div style="text-align:center; margin-top: 5px">
                <div>
                  {!!DNS1D::getBarcodeSvg($product->barcode, "C128",1.7,40)!!}
                  <div style="font-size: 10px; font-weight:bold">{{$product->barcode}}</div>
                </div>
            </div>
        </div>
        <div style="display:inline-block; margin-right: 15px; margin-top: 4px; width: 46%; margin-left: 5px; float:right">
            <div style="margin-top:5px;font-size:12px">
                <span>{{substr($product->name, 0, 32)}}</span>
            </div>
            <div style="margin-top:2px; font-size: 10px;">
                <span>Packed: {{\Carbon\Carbon::now('Asia/KolKata')->format('d-M-y')}}</span>
                <span style="float:right">Expiry: {{\Carbon\Carbon::now('Asia/KolKata')->addMonths(6)->subDay(1)->format('d-M-y')}}</span>
            </div>
            <div style="margin-top:2px;font-size:10px">
                <span>FSSAI: 10819005000097</span>
                <span style="float:right">PID:&nbsp;{{$product->id}}</span>
            </div>
            <div style="text-align:center; margin-top: 5px">
                <div>
                  {!!DNS1D::getBarcodeSvg($product->barcode, "C128",1.7,40)!!}
                  <div style="font-size: 10px; font-weight:bold">{{$product->barcode}}</div>
                </div>
            </div>
        </div>
        </div>
        <span class="page-break"></span>
        @endfor
</body>

</html>
