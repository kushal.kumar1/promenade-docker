<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: 'sans-serif';
      font-size: 11px;
    }

    .main {
      border: 1px solid black;
      padding: 20px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
    }

    .flex-between {
      display: flex;
      display: -webkit-box;
      justify-content: space-between;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-40 {
      width: 40%;
    }

    .w-60 {
      width: 60%;
    }

    .bold {
      font-weight: bolder;
    }

    .mr-5 {
      margin-right: 5px;
    }

    .mr-10 {
      margin-right: 10px;
    }

    .mt-5 {
      margin-top: 5px;
    }

    .tnc {
      display: inline-block;

    }
  </style>
  <script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);
          }
      }
      var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
      for (var css_class in css_selector_classes) {
          if (css_selector_classes.hasOwnProperty(css_class)) {
              var element = document.getElementsByClassName(css_selector_classes[css_class]);
              for (var j = 0; j < element.length; ++j) {
                  element[j].textContent = vars[css_selector_classes[css_class]];
              }
          }
      }
  }
  </script>
</head>

<body onload="subst()">
  <div class="main">
    <div class="flex">
      <div class="w-40">
        PAYMENTS:<br>
        By Bank Transfer:<br>
        <b>Odicea Distribution Technologies Pvt. Ltd.</b><br>
        <b>Account:</b> 017105009556<br>
        <b>IFSC:</b> ICIC0003890<br>
      </div>

      <div class="w-60">
        <div class="flex">

          <div class="w-70">
            <div style="margin-left:60px;">
              <div style="padding-top:30px"><b>Authorised Signatory</b></div>
              <div style="margin-top:5px"><b>ODICEA DISTRIBUTION TECHNOLOGIES PVT. LTD.</b></div>
              <p style="text-align:left;margin-top:5px">
                KHASRA NO. 213, MUSTIL NO. 31, KILA NO. 8,<br>
                SIHI VILLAGE, GURGAON 122004 HARYANA<br>
                Phone: +91 8368776202 | GSTIN: 06AACCO8394A1ZH<br>
                Website:www.niyotail.com | Email: support@niyotail.com
              </p>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div style="text-align:center;font-size:14px; margin-top: 5px">
     Page <span class="page"></span> of <span class="topage"></span>
  </div>
</body>

</html>
