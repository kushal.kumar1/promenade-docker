<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: "sans-serif";
      max-width: 1200px;
      margin: 0 auto;
    }

    table {
      width: 100%;
      height: 100%;
      border-collapse: collapse;
    }

    table tr td {
      font-size: 12px;
      text-align: center;
    }

    table#main {
      border: 1px solid black;
      border-top: none;
      page-break-inside: auto;
    }

    table#main thead {
      background: #EEE;
      font-weight: bolder;
      text-align: center;
      border-bottom: 1px solid black;
      font-size: 16px;
      /* vertical-align: top; */
    }

    table#main tr {
      border: none;
      page-break-inside: avoid;
      page-break-after: auto;
    }

    table#main tr td {
      border-right: solid 1px black;
      border-left: solid 1px black;
      padding: 2px 3px;
    }

    table#main tr td:first-child {
      /* vertical-align: top; */
    }


    table#main tr td:first-child,
    table#main tr td:last-child {
      border: none;
    }

    table#main .tax tr td:first-child {
      vertical-align: middle;
    }

    table#main .tax tr {
      border: none;
    }

    .m-t-10 {
      margin-top: 10px;
    }

    .m-t-20 {
      margin-top: 20px;
    }

    .m-t-70 {
      margin-top: 70px;
    }

    .pd-r-25 {
      padding-right: 25px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
      display: -ms-flexbox;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-50 {
      width: 50%;
    }

    .text-right {
      text-align: right;
    }

    .border-top-bottom {
      border-top: 1px solid;
      border-bottom: 1px solid;
    }

    .pd-20 {
      padding: 10px;
    }

    .pd-5 {
      padding-top: 5px;
    }

    .bold {
      font-weight: bolder;
    }

    .summary {
        page-break-inside: avoid;
        font-size: 14px;
    }
  </style>
</head>

<body>
  <div>
    <table id="main">
      <thead>
        <td><b>#</b></td>
        <td style="text-align:left;">Product</td>
        <td>Type</td>
        <td>Qty</td>
        <td>HSN</td>
        <td style="text-align:right;">MRP <br />(&#x20b9;)</td>
        <td style="text-align:right;">Rate <br />(&#x20b9;)</td>
        <td style="text-align:right;">Subtotal <br />(&#x20b9;)</td>
        <td style="text-align:right;">Discount <br />(&#x20b9;)</td>
        @if(!empty($taxes))
          @foreach ($taxes as $tax)
          <td>{{$tax}}
            <table class="tax">
              <tr>
                <th style="text-align:left">(%)</th>
                <th style="text-align:right">(&#x20b9;)</th>
              </tr>
            </table>
          </td>
          @endforeach
        @endif
        <td style="text-align:right;">Amount <br />(&#x20b9;)</td>
      </thead>
      @foreach ($rows as $item)
      <tr>
        <td>{{$loop->index + 1}}</td>
        <td style="text-align:left;">{{$item['name']}}</td>
        <td>@if($item['is_partial_return']) {{$item['uom']}} @else {{$item['type']}} @endif</td>
        <td>{{$item['quantity']}}</td>
        <td>{{$item['hsn_sac_code']}}</td>
        <td style="text-align:right;">{{number_format($item['mrp'])}}</td>
        <td style="text-align:right;">{{number_format($item['price'],2)}}</td>
        <td style="text-align:right;">{{number_format($item['subtotal'],2)}}</td>
        <td style="text-align:right;">{{number_format($item['discount'],2)}}</td>
        @if(!empty($taxes))
        @foreach ($taxes as $tax)
        <td>
          <table class="tax">
            <tr>
              <td style="text-align:left">{{$item[$tax]['percentage']}}</td>
              <td style="text-align:right">
                {{number_format($item[$tax]['amount'],2)}}
              </td>
            </tr>
          </table>
        </td>
        @endforeach
        @endif
        <td style="text-align:right;">{{number_format($item['total'],2)}}</td>
      </tr>
      @endforeach
    </table>

<div class='summary'>
  <div class="flex m-t-10 pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b> SUBTOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($rows->sum('subtotal'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>DISCOUNT</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($rows->sum('discount'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>TAX</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($rows->sum('tax'),2)}}</b>
        </div>
      </div>
    </div>
  </div>

  @if($rows->sum('total') != $creditNote->amount)
  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>ROUND OFF (+/-)</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format(($creditNote->amount-$rows->sum('total')),2)}}</b>
        </div>
      </div>
    </div>
  </div>
  @endif

  <div class="flex border-top-bottom pd-5" style="margin-top:5px">
    <div class="w-70">
      &nbsp;Rs {{Niyotail\Helpers\Utils::getAmountInWords($creditNote->amount)}} Only
    </div>

    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>GRAND TOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($creditNote->amount)}}</b>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</body>

</html>
