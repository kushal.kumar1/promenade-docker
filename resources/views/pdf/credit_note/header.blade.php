<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">

  <head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
      * {
        margin: 0;
        padding: 0;
      }

      body {
        font-family: "sans-serif";
      }

      .main {
        border: 1px solid black;
      }

      .mt-10 {
        margin-top: 10px;
      }

      #heading {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
      }

      .user-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        text-transform: uppercase;
      }

      .user-info>div {
        padding: 15px;
        font-size: 14px;
        width: 50%;
      }

      .user-info .billing {
        margin-top: 10px;
      }

      .logo {
        display: block;
        margin: 8px auto;
        width: 20%;
      }

      .flex {
        display: flex;
        display: -webkit-box;
      }

      .flex-between {
        display: flex;
        display: -webkit-box;
        justify-content: space-between;
      }
    </style>
  </head>
</head>

<body>
  <div class="main">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <div id="heading">
      <b>CREDIT NOTE</b>
    </div>
    <div class="user-info">
      <div>
        <div>
          <b>Order ID : </b> {{$creditNote->order->reference_id}}
        </div>
        <div class="billing">
          <b>{{$creditNote->order->billingAddress->name}}</b> <br>
          {!!nl2br(e($creditNote->order->billingAddress->address))!!}<br>
          {{$creditNote->order->billingAddress->city->name}} - {{$creditNote->order->billingAddress->pincode}}<br />
          Contact: {{$creditNote->order->billingAddress->mobile}}<br />
          @if(!empty($creditNote->order->store))
            GSTIN: {{$creditNote->order->store->gstin}}
            @endif
        </div>
        <div class="mt-10">
        </div>
      </div>
      <div>
          <p>
            <b>Credit Note:</b> {{$creditNote->reference_id}} <br>
            <b>Issue Date:</b> {{(new DateTime($creditNote->created_at))->format("d-M-Y")}}<br>
          </p>
          <p class="mt-10">
            <b>AREA / STORE ID: </b>{{$creditNote->order->store->beat->name}} / {{$creditNote->order->store->id}}<br>
            <b>Invoice: </b>{{$creditNote->returnOrder->shipment->invoice->reference_id}}<br />
            <b>Invoice Date: </b>{{(new DateTime($creditNote->returnOrder->shipment->invoice->created_at))->format("d-M-Y")}}
          </p>
      </div>
    </div>
  </div>
</body>
<html>
