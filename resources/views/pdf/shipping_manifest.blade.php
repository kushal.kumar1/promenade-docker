<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style>
    body {
      margin: 0 auto;
      max-width: 1200px;
      font-size: 14px;
      line-height: 22px;
      width: 100%;
    }

    body * {
      margin: 0;
      font-size: inherit;
      line-height: inherit;
    }

    table {
      width: 100%;
      height: 100%;
      border-collapse: collapse;
      table-layout: auto;
    }

    tr {
      border-top: 1px solid #000;
      border-bottom: 1px solid #000;
    }

    th,
    td {
      text-align: left;
      padding: 5px;
    }

    tr:last-child {
      border: none;
    }


    .center {
      text-align: center;
    }

    .bold {
      font-weight: bold;
    }

    .heading {

      font-size: 17px;
      text-transform: uppercase;
    }

    .title {
      padding: 5px;
    }

    .order {
      border: 1px solid black;
      margin-bottom: 35px;
      margin-top: 20px;
    }

    .pd-30 {
      padding: 30px;
    }

    .page-break {
      page-break-after: always;
      page-break-inside: avoid;
    }

    .checkbox {
      border: 1px solid #000;
      width: 10px;
      height: 10px;
    }

    .logo{
      width: 100px;
    }
  </style>
</head>

<body>
  <div class="center">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <h1 class="heading">Shipping Manifest #{{$manifest->id}}</h1>
    <div>
      Agent: {{$manifest->agent->name}} ({{$manifest->agent->code}}) / Vehicle : {{$manifest->vehicle_number}}
    </div>
    <div>
      Generated On: {{\Carbon\Carbon::parse($manifest->created_at)->format("d-m-Y h:i a")}}
    </div>
  </div>
  <div class="order">
    <table>
      <tr class="bold">
        <th>#</th>
        <th>Store</th>
        <th>Order ID</th>
        <th>Shipment ID</th>
        <th>Boxes</th>
        <th>Invoice No.</th>
        <th>Amount</th>
        <th>Returns</th>
        <th>Collection</th>
      </tr>
      @foreach($manifest->shipments as $shipment)
        <tr>
          <td>{{$loop->index+1}}</td>
          <td>{{$shipment->order->store->name}}</td>
          <td>{{$shipment->order->reference_id}}</td>
          <td>{{$shipment->id}}</td>
          <td>{{$shipment->boxes}}</td>
          <td>{{$shipment->invoice->reference_id}}</td>
          <td>Rs. {{number_format($shipment->invoice->amount, 2)}}</td>
          <td class="center">
            <div class="checkbox">
            </div>
          </td>
          <td><div class="checkbox">
          </div></td>
        </tr>
        @endforeach
    </table>
  </div>
</body>

</html>
