<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        body {
            margin: 0 auto;
            max-width: 1200px;
            font-size: 14px;
            line-height: 22px;
            width: 100%;
        }

        body * {
            margin: 0;
            font-size: inherit;
            line-height: inherit;
        }

        table {
            width: 100%;
            height: 100%;
            border-collapse: collapse;
            table-layout: auto;
        }

        tr {
            border-top: 1px solid #000;
            border-bottom: none;
        }

        tr.group {
            border-top: 1px dashed #d3d3d3;
            border-bottom: none;
        }

        th, td {
            text-align: left;
            padding: 5px;
        }

        .center {
            text-align: center;
        }

        .bold {
            font-weight: bold;
        }

        .heading {
            font-size: 17px;
            text-transform: uppercase;
        }

        .title {
            padding: 5px;
        }

        .order {
            border: 1px solid black;
            margin-bottom: 35px;
        }

        .pd-30 {
            padding: 30px;
        }

        .page-break {
            page-break-after: always;
            page-break-inside: avoid;
        }

        .small {
            font-size: 12px;
        }
    </style>
</head>
<body>
<div class="pd-30 center">
    <h1 class="heading">Picklist (#{{$picklist->id}})</h1>
    <div>
        Generated on : {{\Carbon\Carbon::now()->setTimezone('Asia/Kolkata')->format("d-m-Y h:i a")}}
    </div>
    <div>
        <div class="bold"> Picker: @if(!empty($picklist->picker)){{ucwords($picklist->picker->name)}}@else{{ucwords($picklist->agent->name)}}@endif</div>
    </div>
</div>
<?php
$orders = $picklist->orderItems->pluck('order')->unique();
?>
<h1 class="center heading title">Orders</h1>
<div class="order">
    <table>
        <tr class="bold">
            <th>#</th>
            <th>Order Id</th>
            <th>Store</th>
            <th>Address</th>
            <th>Created</th>
        </tr>
        @foreach($orders as $order)
            <tr>
                <td>{{$loop->index+1}}.</td>
                <td>{{$order->reference_id}}</td>
                <td>{{$order->store->name}}</td>
                <td>{{$order->store->address}}, {{$order->store->pincode}}</td>
                <td>{{$order->created_at}}</td>
            </tr>
        @endforeach
    </table>
</div>

<h1 class="center heading title">Items</h1>
<div class="order">
    <table>
        <tr class="bold">
            <th>#</th>
            <th>Product</th>
            <th>Location</th>
            <th>Quantity</th>
            <th>Checked</th>
        </tr>
        @foreach($items as  $item)
            @php
                $variantQuantityArr = \Niyotail\Helpers\Utils::convertToVariantQuantity($item->quantity, $item->product_id);
                $variantQuantity = !empty($variantQuantityArr) ? implode(', ', $variantQuantityArr) : '';
            @endphp
            <tr>
                <td style="vertical-align:top">{{$loop->index+1}}.</td>
                <td>
                    <span class="bold">{{$item->product}}</span><br/>
                    <span class="small">Pid: {{$item->product_id}}</span><br/>
                    <span class="small">Barcode: {{$item->barcode}}</span>
                </td>
                <td>
                    @if(($picklist->warehouse_id == 2 && $item->storage_id == 6497) || empty($item->storage_id))
                        {{$item->location_old}}
                    @else
                        {{$item->location}}
                    @endif
                </td>
                <td>
                    <span>@if(!empty($variantQuantity)) {{$variantQuantity}} ({{$item->quantity}}
                        pcs) @else {{$item->quantity}} pcs @endif</span>
                </td>
                <td><input type="checkbox"></td>
            </tr>
        @endforeach
    </table>
</div>

</body>
</html>
