<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: "sans-serif";
      max-width: 1200px;
      margin: 0 auto;
    }

    table {
      width: 100%;
      height: 100%;
      border-collapse: collapse;
    }

    table tr td {
      font-size: 12px;
      text-align: center;
    }

    table#main {
      border: 1px solid black;
      border-top: none;
      page-break-inside: auto;
    }

    table#main tr.footer
    {
      padding:5px 0px;
      border-top: 1px solid black;
    }

    table#main thead, table#main tr.footer {
      background: #EEE;
      font-weight: bolder;
      text-align: center;
      border-bottom: 1px solid black;
      font-size: 16px;
      /* vertical-align: top; */
    }

    table#main tr {
      border: none;
      page-break-inside: avoid;
      page-break-after: auto;
    }

    table#main tr td {
      border-right: solid 1px black;
      border-left: solid 1px black;
      padding: 2px 3px;
    }

    table#main tr td:first-child {
      /* vertical-align: top; */
    }


    table#main tr td:first-child,
    table#main tr td:last-child {
      border: none;
    }

    table#main .tax tr td:first-child {
      vertical-align: middle;
    }

    table#main .tax tr {
      border: none;
    }

    .m-t-10 {
      margin-top: 10px;
    }

    .m-t-20 {
      margin-top: 20px;
    }

    .m-t-70 {
      margin-top: 70px;
    }

    .pd-r-25 {
      padding-right: 25px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
      display: -ms-flexbox;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-50 {
      width: 50%;
    }

    .text-right {
      text-align: right;
    }

    .border-top-bottom {
      border-top: 1px solid;
      border-bottom: 1px solid;
    }

    .pd-20 {
      padding: 10px;
    }

    .pd-5 {
      padding-top: 5px;
    }

    .bold {
      font-weight: bolder;
    }

    .summary {
        page-break-inside: avoid;
        font-size: 14px;
    }
  </style>
</head>
@php
  $invoiceTotal = 0;
@endphp
<body>
  <div>
    <table id="main">
      <thead>
      <td>#</td>
      <td>Product Name</td>
      <td>Barcode</td>
      <td>Unit MRP</td>
      <td>Total Units</td>
      <td>SKU</td>
      <td>Mfg. Date</td>
      <td>Expiry Date</td>
      <td>Vendor Batch No.</td>
      <td>Qty</td>
      <td>Invoiced Cost</td>
      <td>Total Invoiced Cost</td>
      <td>Total Effective Cost</td>
      </thead>
      <tbody>
      @foreach($items as $i=>$item)
        @php
            $invoiceTotal += $item['total_effective_cost'];
        @endphp
      <tr>
        <td>{{$i+1}}</td>
        <td style="text-align: left">{{$item['name']}}</td>
        <td>{{$item['barcode']}}</td>
        <td>{{$item['unit_mrp']}}</td>
        <td>{{$item['total_units']}}</td>
        <td>{{$item['sku']}}</td>
        <td>@if(!empty($item['manufacturing_date'])){{\Carbon\Carbon::parse($item['manufacturing_date'])->format('d/m/Y')}}@endif</td>
        <td>@if(!empty($item['expiry_date'])){{\Carbon\Carbon::parse($item['expiry_date'])->format('d/m/Y')}}@endif</td>
        <td>{{$item['vendor_batch_number']}}</td>
        <td>{{$item['quantity']}}</td>
        <td style="text-align: right">{{number_format($item['unit_invoiced_cost'], 2)}}</td>
        <td style="text-align: right">{{number_format($item['total_invoiced_cost'], 2)}}</td>
        <td style="text-align: right">{{number_format($item['total_effective_cost'], 2)}}</td>
      </tr>
      @endforeach
      </tbody>
    </table>

    <div class='summary'>

      <div class="flex m-t-10 pd-5">
        <div class="w-70">
        </div>
        <div class="w-30">
          <div class="flex">
            <div class="w-50">
              <b> Total</b>
            </div>
            <div class="w-50 text-right pd-r-25">
              <b>&#x20b9; {{number_format($invoiceTotal,2)}}</b>
            </div>
          </div>
        </div>
      </div>

      @if($grn->createdBy)
      <div class="flex m-t-10 pd-5">
        <div class="w-70">
        </div>
        <div class="w-30">
          <div class="flex">
            <div class="w-50">

            </div>
            <div class="w-50 text-right pd-r-25">
              <br>
              <b>Goods received by</b>
              <br>
              <br>
              <br>
              <br>
              <br>
              <b>{{$grn->createdBy->name}}</b>
              <br>
              {{$grn->createdBy->email}}
            </div>
          </div>
        </div>
      </div>
      @endif

    </div>


</div>
</body>

</html>
