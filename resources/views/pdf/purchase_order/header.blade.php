<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">

  <head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
      * {
        margin: 0;
        padding: 0;
      }

      body {
        font-family: "sans-serif";
      }

      .main {
        border: 1px solid black;
      }

      .mt-10 {
        margin-top: 10px;
      }

      #heading {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
      }

      .user-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        text-transform: uppercase;
      }

      .user-info>div {
        padding: 15px;
        font-size: 14px;
        width: 50%;
      }

      .user-info .billing {
        margin-top: 10px;
      }

      .logo {
        display: block;
        margin: 8px auto;
        width: 20%;
      }

      .flex {
        display: flex;
        display: -webkit-box;
      }

      .flex-between {
        display: flex;
        display: -webkit-box;
        justify-content: space-between;
      }

      .mt-5{
        margin-top: 5px;
      }
      .bold{
        font-weight: bold
      }
    </style>
  </head>
</head>

<body>
  <div class="main">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <div id="heading">
      <b>Purchase Order</b>
    </div>
    <div class="user-info">
      <div>
        <div>
          <b>Purchase Order ID : </b> {{$purchaseOrder->id}}
        </div>
        <div class="billing">
          <p>
            Vendor:
          </p>
          <b>{{$purchaseOrder->vendor->name}}</b> <br>
          @if(!empty($purchaseOrder->vendorAddress->gstin))
            Address: {{$purchaseOrder->vendorAddress->address}}<br>
            GSTIN: {{$purchaseOrder->vendorAddress->gstin}}
            @endif
        </div>
      </div>
      <div>
          <p>
            <b>Purchase Order Date:</b> {{(new DateTime($purchaseOrder->created_at))->format("d-M-Y")}}<br>
          </p>
      </div>
    </div>
  </div>
</body>
<html>
