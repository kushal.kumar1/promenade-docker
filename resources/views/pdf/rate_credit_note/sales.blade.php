<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style type="text/css" media="all">
    * {
      margin: 0;
      padding: 0;
    }

    body {
      font-family: "sans-serif";
      max-width: 1200px;
      margin: 0 auto;
    }

    table {
      width: 100%;
      height: 100%;
      border-collapse: collapse;
    }

    table tr td {
      font-size: 12px;
    }

    table#main {
      border: 1px solid black;
      border-top: none;
      page-break-inside: auto;
    }

    table#main thead {
      background: #EEE;
      font-weight: bolder;
      border-bottom: 1px solid black;
      font-size: 16px;
      /* vertical-align: top; */
    }

    table#main tr {
      border: none;
      page-break-inside: avoid;
      page-break-after: auto;
    }

    table#main tr td {
      border-right: solid 1px black;
      border-left: solid 1px black;
      padding: 2px 3px;
    }

    table#main tr td:first-child {
      /* vertical-align: top; */
    }


    table#main tr td:first-child,
    table#main tr td:last-child {
      border: none;
    }

    table#main .tax tr td:first-child {
      vertical-align: middle;
    }

    table#main .tax tr {
      border: none;
    }

    .m-t-10 {
      margin-top: 10px;
    }

    .m-t-20 {
      margin-top: 20px;
    }

    .m-t-70 {
      margin-top: 70px;
    }

    .pd-r-25 {
      padding-right: 25px;
    }

    .flex {
      display: flex;
      display: -webkit-box;
      display: -ms-flexbox;
    }

    .w-70 {
      width: 70%;
    }

    .w-30 {
      width: 30%;
    }

    .w-50 {
      width: 50%;
    }

    .text-right {
      text-align: right;
    }

    .border-top-bottom {
      border-top: 1px solid;
      border-bottom: 1px solid;
    }

    .pd-20 {
      padding: 10px;
    }

    .pd-5 {
      padding-top: 5px;
    }

    .bold {
      font-weight: bolder;
    }

    .summary {
        page-break-inside: avoid;
        font-size: 14px;
    }
  </style>
</head>

<body>
  <div>
    <table id="main">
      <thead>
        <td style="text-align:left;">Invoice Ref</td>
        <td style="text-align:left;">Product</td>
        <td style="text-align:right;">Qty</td>
        <td style="text-align:right;">Billing Value</td>
        <td style="text-align:right;">Selling price</td>
        <td style="text-align:right;">Subtotal</td>
        <td style="text-align:right;">CGST</td>
        <td style="text-align:right;">SGST</td>
        <td style="text-align:right;">IGST</td>
        <td style="text-align:right;">CESS</td>
        <td style="text-align:right;">SPL CESS</td>
        <td style="text-align:right;">Total</td>
      </thead>
      @php
        $subTotal = 0;
        $tax = 0;
        $total = 0;
      @endphp
      @foreach ($items->groupBy('invoice_ref_id') as $invRef=>$orderItems)
        @foreach($orderItems as $orderItem)
          @php
            $subTotal += $orderItem['subtotal'];
            $tax += $orderItem['cgst'] + $orderItem['sgst'] + $orderItem['igst'] + $orderItem['cess'] + $orderItem['spl_cess'];
            $total += $orderItem['total'];
          @endphp
          <tr style="border-bottom: 1px solid #000;">
            <td>{{$invRef}}</td>
            <td>{{$orderItem['name']}}</td>
            <td style="text-align:right;">{{$orderItem['quantity']}}</td>
            <td style="text-align:right;">{{$orderItem['total_sp']}}</td>
            <td style="text-align:right;">{{$orderItem['total_billed']}}</td>
            <td style="text-align:right;">{{$orderItem['subtotal']}}</td>
            <td style="text-align:right;">{{$orderItem['cgst']}}</td>
            <td style="text-align:right;">{{$orderItem['sgst']}}</td>
            <td style="text-align:right;">{{$orderItem['igst']}}</td>
            <td style="text-align:right;">{{$orderItem['cess']}}</td>
            <td style="text-align:right;">{{$orderItem['spl_cess']}}</td>
            <td style="text-align:right;">{{$orderItem['total']}}</td>
          </tr>
        @endforeach
      @endforeach
    </table>

<div class='summary'>
  <div class="flex m-t-10 pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>SUBTOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($subTotal,2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex pd-5">
    <div class="w-70">
    </div>
    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>TAX</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($tax,2)}}</b>
        </div>
      </div>
    </div>
  </div>

  <div class="flex border-top-bottom pd-5" style="margin-top:5px">
    <div class="w-70">
      @if($total > 0) &nbsp;Rs {{Niyotail\Helpers\Utils::getAmountInWords($total)}} Only @endif
    </div>

    <div class="w-30">
      <div class="flex">
        <div class="w-50">
          <b>GRAND TOTAL</b>
        </div>
        <div class="w-50 text-right pd-r-25">
          <b>&#x20b9; {{number_format($total, 2)}}</b>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</body>

</html>
