<!DOCTYPE HTML>
<html>

<head>
  <meta charset="utf-8">

  <head>
    <meta charset="utf-8">
    <style type="text/css" media="all">
      * {
        margin: 0;
        padding: 0;
      }

      body {
        font-family: "sans-serif";
      }

      .main {
        border: 1px solid black;
      }

      .mt-10 {
        margin-top: 10px;
      }

      #heading {
        border-top: 1px solid black;
        border-bottom: 1px solid black;
        text-align: center;
        font-size: 20px;
        padding-top: 5px;
      }

      .user-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        text-transform: uppercase;
      }

      .user-info>div {
        padding: 15px;
        font-size: 14px;
        width: 50%;
      }

      .user-info .billing {
        margin-top: 10px;
      }

      .logo {
        display: block;
        margin: 8px auto;
        width: 20%;
      }

      .flex {
        display: flex;
        display: -webkit-box;
      }

      .flex-between {
        display: flex;
        display: -webkit-box;
        justify-content: space-between;
      }
    </style>
  </head>
</head>

<body>
  <div class="main">
    <img src="{{asset('niyotail.png')}}" class="logo">
    <div id="heading">
      <b>DISCOUNT CREDIT NOTE</b>
    </div>
    <div class="user-info">
      <div>
        <div class="billing">
          <b>{{$store->name}}</b> <br>
          {!!nl2br(e($store->address))!!}<br>
          {{$store->pincodeRel->city->name}}, {{$store->pincodeRel->city->state->name}} - {{$store->pincode}}<br />
          Contact: {{$store->mobile}}<br />
          @if(!empty($store->gstin))
            GSTIN: {{$store->gstin}}
            @endif
        </div>
        <div class="mt-10">
{{--          {!!DNS1D::getBarcodeSvg($creditNote->reference_id, "C128",1.5, 40)!!}--}}
        </div>
      </div>
      <div>
          <p>
            <b>Credit Note:</b> {{$creditNote->reference_id}} ({{substr($creditNote->type, 0, 1)}})<br>
            <b>Date From:</b> {{(new DateTime($creditNote->start_date))->format("d-M-Y")}}<br>
            <b>Date To:</b> {{(new DateTime($creditNote->end_date))->format("d-M-Y")}}<br>
            <b>Issue Date:</b> {{(new DateTime($creditNote->generated_on))->format("d-M-Y")}}<br>
          </p>
      </div>
    </div>
  </div>
</body>
<html>
