<#> {{$otp}} is the OTP for logging in to your Niyotail app. Ab kirana par bachat karein har din!
@if (App::environment('production'))
{{config('sms.otp_key.production')}}
@else
{{config('sms.otp_key.local')}}
@endif
