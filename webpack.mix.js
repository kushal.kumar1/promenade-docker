let mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .sass("resources/assets/admin/sass/app.scss", 'public/admin/css')

    // Common JS
    .babel([
        "resources/assets/admin/js/app.js",
        "resources/assets/admin/js/grid.js",
        "resources/assets/admin/js/core/form.js",
        "resources/assets/admin/js/core/modal.js",
        "resources/assets/admin/js/core/dialog.js",
        "resources/assets/admin/js/core/template.js",
        "resources/assets/admin/js/core/toast.js",
    ],
        'public/admin/js/app.js')

    //Collection
    .babel([
        "public/plugins/jstree/jstree.min.js",
        "resources/assets/admin/js/collection/collection.js"
    ], "public/admin/js/collection.js")
    //Brand
    .babel([
        "public/plugins/jstree/jstree.min.js",
        "resources/assets/admin/js/brand/brand.js"
    ], "public/admin/js/brand.js")
    .babel("resources/assets/admin/js/user/profile.js", "public/admin/js/user.js")
    .js("resources/assets/admin/js/Store.js", "public/admin/js/store.js")

    // Order Detail
    .babel([
        "resources/assets/admin/js/order/order.js",
        "resources/assets/admin/js/order/shipment.js"
    ], "public/admin/js/order.js")

    //Product
    .js("resources/assets/admin/js/product/Product.js", "public/admin/js/product.js")

    .js("resources/assets/admin/js/Agent.js", "public/admin/js/agent.js")
    .js("resources/assets/admin/js/rule/Rule.js", "public/admin/js/rule.js")
    .js('resources/assets/admin/js/Picklist.js', 'public/admin/js/picklist.js')
    .js("resources/assets/admin/js/Locator.js", "public/admin/js/locator.js")
    .js("resources/assets/admin/js/Manifest.js", "public/admin/js/manifest.js")
    .js('resources/assets/admin/js/Search.js','public/admin/js/search.js')
    .js('resources/assets/admin/js/PurchaseInvoice.js', 'public/admin/js/purchase-invoice.js')
    .js('resources/assets/admin/js/Putaway.js', 'public/admin/js/putaway.js')

    .version();
