-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: niyoweb-prod.crvi1ow7nyif.ap-south-1.rds.amazonaws.com    Database: niyoweb
-- ------------------------------------------------------
-- Server version	8.0.25
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additional_order_infos`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `additional_order_infos` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `additional_order_infos_order_id_foreign` (`order_id`),
  KEY `additional_order_infos_pos_order_id_index` (`pos_order_id`),
  CONSTRAINT `additional_order_infos_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12220 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_beat_day_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_beat_day_map` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` int unsigned DEFAULT NULL,
  `beat_id` int unsigned DEFAULT NULL,
  `day_id` int unsigned DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`agent_id`),
  KEY `Index 3` (`beat_id`),
  CONSTRAINT `FK__agents` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK__beats` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent_beat_mktr_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_beat_mktr_map` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` int unsigned DEFAULT NULL,
  `beat_id` int unsigned DEFAULT NULL,
  `marketer_id` int unsigned DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`agent_id`),
  KEY `Index 3` (`beat_id`),
  KEY `Index 4` (`marketer_id`),
  CONSTRAINT `agent_id` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `beat_id` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `marketer_id` FOREIGN KEY (`marketer_id`) REFERENCES `marketers` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12174 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agents`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agents` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('sales','delivery','collection') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'sales',
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `otp` int unsigned DEFAULT NULL,
  `otp_expiry` datetime DEFAULT NULL,
  `otp_sent_count` int unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agents_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `api_clients`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `allowed_ips` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_banners`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_banners` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid_from` datetime DEFAULT NULL,
  `valid_to` datetime DEFAULT NULL,
  `link` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_section_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_section_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `app_section_id` int unsigned NOT NULL,
  `image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_section_items_app_section_id_sort_status_unique` (`app_section_id`,`sort`,`status`),
  CONSTRAINT `app_section_items_app_section_id_foreign` FOREIGN KEY (`app_section_id`) REFERENCES `app_sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_sections`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_sections` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `medium` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'retail',
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `layout` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `grid_columns` tinyint DEFAULT NULL,
  `item_count` int unsigned DEFAULT NULL,
  `sort` int NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attributes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attributes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_filter` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `attributes_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_item_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_item_logs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `audit_item_id` bigint unsigned NOT NULL,
  `description` json NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `audit_item_logs_audit_item_id_foreign` (`audit_item_id`),
  CONSTRAINT `audit_item_logs_audit_item_id_foreign` FOREIGN KEY (`audit_item_id`) REFERENCES `audit_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2001 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audit_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `audit_id` bigint unsigned NOT NULL,
  `associated_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `associated_type_id` int unsigned DEFAULT NULL,
  `quantity` int NOT NULL,
  `condition` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_settled` tinyint(1) NOT NULL DEFAULT '0',
  `rack_id` int unsigned DEFAULT NULL,
  `employee_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `audit_items_audit_id_foreign` (`audit_id`),
  KEY `audit_items_employee_id_foreign` (`employee_id`),
  KEY `audit_items_rack_id_foreign` (`rack_id`),
  CONSTRAINT `audit_items_audit_id_foreign` FOREIGN KEY (`audit_id`) REFERENCES `audits` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `audit_items_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `audit_items_rack_id_foreign` FOREIGN KEY (`rack_id`) REFERENCES `warehouse_racks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2206 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audits`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audits` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `audits_warehouse_id_foreign` (`warehouse_id`),
  KEY `audits_employee_id_foreign` (`employee_id`),
  CONSTRAINT `audits_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `audits_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auto_replenish_demand`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auto_replenish_demand` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int unsigned DEFAULT NULL,
  `product_id` int unsigned DEFAULT NULL,
  `reorder_qty` decimal(8,2) unsigned NOT NULL,
  `pending_qty` decimal(8,2) unsigned DEFAULT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1648 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auto_replenish_parameters`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auto_replenish_parameters` (
  `type` enum('store','warehouse') DEFAULT NULL,
  `class` varchar(10) DEFAULT NULL,
  `cum_qty_limit` decimal(8,4) DEFAULT NULL,
  `inventory_days` decimal(8,4) DEFAULT NULL,
  `reorder_level` decimal(8,4) DEFAULT NULL,
  `reorder_quantity` decimal(8,4) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `badge_user`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `badge_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `badge_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `badge_user_user_id_badge_id_unique` (`user_id`,`badge_id`),
  KEY `badge_user_badge_id_foreign` (`badge_id`),
  CONSTRAINT `badge_user_badge_id_foreign` FOREIGN KEY (`badge_id`) REFERENCES `badges` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `badge_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `badges`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `badges` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `beat_exceptions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beat_exceptions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `beat_id` int unsigned NOT NULL,
  `brand_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `beat_exceptions_beat_id_brand_id_unique` (`beat_id`,`brand_id`),
  KEY `beat_exceptions_brand_id_foreign` (`brand_id`),
  CONSTRAINT `beat_exceptions_beat_id_foreign` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beat_exceptions_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `beat_marketers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beat_marketers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` int unsigned NOT NULL,
  `beat_id` int unsigned NOT NULL,
  `marketer_id` int unsigned NOT NULL,
  `valid_from` date NOT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `agent_beat_mktr_validity_unique` (`agent_id`,`beat_id`,`marketer_id`,`valid_from`,`valid_to`),
  KEY `beat_marketers_beat_id_foreign` (`beat_id`),
  KEY `beat_marketers_marketer_id_foreign` (`marketer_id`),
  CONSTRAINT `beat_marketers_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beat_marketers_beat_id_foreign` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beat_marketers_marketer_id_foreign` FOREIGN KEY (`marketer_id`) REFERENCES `marketers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11674 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `beat_warehouse`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beat_warehouse` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `beat_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `beat_warehouse_beat_id_foreign` (`beat_id`),
  KEY `beat_warehouse_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `beat_warehouse_beat_id_foreign` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `beat_warehouse_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `beats`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beats` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `brands`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `marketer_id` int unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_name_unique` (`name`),
  KEY `brands_marketer_id_foreign` (`marketer_id`),
  CONSTRAINT `brands_marketer_id_foreign` FOREIGN KEY (`marketer_id`) REFERENCES `marketers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1724 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cart_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cart_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mrp` decimal(8,2) NOT NULL,
  `price` decimal(13,6) NOT NULL,
  `quantity` decimal(8,3) unsigned NOT NULL,
  `subtotal` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `discount` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `tax` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `total` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `cart_items_product_id_foreign` (`product_id`),
  KEY `cart_items_cart_id_foreign` (`cart_id`),
  CONSTRAINT `cart_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cart_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=483444 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `carts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `billing_address_id` int unsigned DEFAULT NULL,
  `shipping_address_id` int unsigned DEFAULT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtotal` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `discount` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `shipping_charges` decimal(8,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `total` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `store_order_id` bigint unsigned DEFAULT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'system',
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `carts_store_order_id_unique` (`store_order_id`),
  KEY `carts_billing_address_id_foreign` (`billing_address_id`),
  KEY `carts_shipping_address_id_foreign` (`shipping_address_id`),
  KEY `carts_store_id_foreign` (`store_id`),
  KEY `carts_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `carts_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `carts_billing_address_id_foreign` FOREIGN KEY (`billing_address_id`) REFERENCES `cart_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `carts_shipping_address_id_foreign` FOREIGN KEY (`shipping_address_id`) REFERENCES `cart_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `carts_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `carts_store_order_id_foreign` FOREIGN KEY (`store_order_id`) REFERENCES `store_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `carts_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45795 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `categories`
--

SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `categories` AS SELECT 
 1 AS `cl1_id`,
 1 AS `cl1_name`,
 1 AS `cl2_id`,
 1 AS `cl2_name`,
 1 AS `cl3_id`,
 1 AS `cl3_name`,
 1 AS `cl4_id`,
 1 AS `cl4_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `category_l1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_l1` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `collection_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`collection_id`),
  CONSTRAINT `FK_category_l1_collections` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_l2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_l2` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `category_l1_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index 2` (`category_l1_id`),
  CONSTRAINT `FK_category_l2_category_l1` FOREIGN KEY (`category_l1_id`) REFERENCES `category_l1` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=578 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_l2_cost_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_l2_cost_map` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category_l2_id` int unsigned NOT NULL,
  `marketer_level_id` int unsigned NOT NULL,
  `cost_basis` enum('mrp','cost') DEFAULT NULL,
  `cost_sp` decimal(16,6) DEFAULT NULL,
  `cost_mrp` decimal(16,6) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`category_l2_id`),
  KEY `Index 3` (`marketer_level_id`) USING BTREE,
  CONSTRAINT `FK_category_l2_cost_map_category_l2` FOREIGN KEY (`category_l2_id`) REFERENCES `category_l2` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_category_l2_cost_map_marketer_levels` FOREIGN KEY (`marketer_level_id`) REFERENCES `marketer_levels` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6098 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_l4_cost_map_rt`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_l4_cost_map_rt` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category_l4_id` int unsigned NOT NULL,
  `marketer_level_id` int unsigned NOT NULL,
  `commission_tag_id` int unsigned NOT NULL,
  `price_basis` enum('cost','mrp') DEFAULT NULL,
  `latest_cost_basis` enum('group','product') DEFAULT NULL,
  `mrp_basis` enum('fixed','dynamic') DEFAULT NULL,
  `std_vcm` decimal(9,6) DEFAULT NULL,
  `cost_sp` decimal(9,6) DEFAULT NULL,
  `cost_mrp` decimal(9,6) DEFAULT NULL,
  `mrp_slab` int unsigned DEFAULT NULL,
  `store_a` decimal(9,6) DEFAULT NULL,
  `store_b` decimal(9,6) DEFAULT NULL,
  `store_c` decimal(9,6) DEFAULT NULL,
  `min_oq` int unsigned DEFAULT NULL,
  `max_oq` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_l4_marketer_level` (`category_l4_id`,`marketer_level_id`),
  KEY `FK_cl4pm_marketer_levels` (`marketer_level_id`),
  KEY `FK_cl4pm_commission_tags` (`commission_tag_id`),
  CONSTRAINT `FK_cl4cm_newcat_l4` FOREIGN KEY (`category_l4_id`) REFERENCES `newcat_l4` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cl4pm_commission_tags` FOREIGN KEY (`commission_tag_id`) REFERENCES `commission_tags` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_cl4pm_marketer_levels` FOREIGN KEY (`marketer_level_id`) REFERENCES `marketer_levels` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5472 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `category_l4_product_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category_l4_product_map` (
  `product_id` int unsigned NOT NULL,
  `category_l4_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `FK_ncl4pm_newcat_l4` (`category_l4_id`),
  CONSTRAINT `FK_ncl4pm_newcat_l4` FOREIGN KEY (`category_l4_id`) REFERENCES `newcat_l4` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ncl4pm_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cities`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_name_unique` (`name`),
  KEY `cities_state_id_foreign` (`state_id`),
  CONSTRAINT `cities_state_id_foreign` FOREIGN KEY (`state_id`) REFERENCES `states` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `club_commissions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `club_commissions` (
  `club_level` varchar(50) DEFAULT NULL,
  `commission_tag_id` int unsigned DEFAULT NULL,
  `value` decimal(10,4) DEFAULT NULL,
  `status` int DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  KEY `Index 1` (`commission_tag_id`),
  CONSTRAINT `FK_club_commissions_niyoweb.commission_tags` FOREIGN KEY (`commission_tag_id`) REFERENCES `commission_tags` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `club_prices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `club_prices` (
  `product_variant_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mrp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `margin` decimal(10,6) DEFAULT NULL,
  `price` decimal(11,6) NOT NULL,
  `moq` decimal(6,3) NOT NULL,
  `latest_cost` decimal(11,6) DEFAULT NULL,
  `last_run` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `club_prices_product_variant_id_unique` (`product_variant_id`),
  KEY `club_prices_product_id_foreign` (`product_id`),
  CONSTRAINT `club_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `club_prices_product_variant_id_foreign` FOREIGN KEY (`product_variant_id`) REFERENCES `product_variants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `collection_product`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `collection_product` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `collection_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `collection_product_product_id_collection_id_unique` (`product_id`,`collection_id`),
  KEY `collection_product_collection_id_foreign` (`collection_id`),
  CONSTRAINT `collection_product_collection_id_foreign` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `collection_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30745 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `collections`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `collections` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commission_tag_product`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commission_tag_product` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `commission_tag_id` int unsigned NOT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`product_id`),
  KEY `Index 3` (`commission_tag_id`),
  CONSTRAINT `FK__products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_commission_tag_product_commission_tags` FOREIGN KEY (`commission_tag_id`) REFERENCES `commission_tags` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22627 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commission_tag_stores`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commission_tag_stores` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `commission_tag_id` int unsigned NOT NULL,
  `value` double(8,6) NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` date NOT NULL DEFAULT '2021-04-08',
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `commission_tag_stores_store_id_foreign` (`store_id`),
  KEY `commission_tag_stores_commission_tag_id_foreign` (`commission_tag_id`),
  CONSTRAINT `commission_tag_stores_commission_tag_id_foreign` FOREIGN KEY (`commission_tag_id`) REFERENCES `commission_tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `commission_tag_stores_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `commission_tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commission_tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `base_commission` decimal(10,4) DEFAULT NULL,
  `ws_commission` decimal(10,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conditions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conditions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `conditions_rule_id_foreign` (`rule_id`),
  CONSTRAINT `conditions_rule_id_foreign` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=859 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `consumer_pricing`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `consumer_pricing` (
  `product_id` int unsigned NOT NULL,
  `mrp` decimal(16,6) unsigned DEFAULT '0.000000',
  `old_mrp` decimal(16,6) unsigned DEFAULT '0.000000',
  `price` decimal(16,6) unsigned DEFAULT '0.000000',
  `old_price` decimal(16,6) unsigned DEFAULT '0.000000',
  `last_pricing_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `Index 1` (`product_id`),
  CONSTRAINT `FK_consumer_pricing_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `countries`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonecode` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `credit_limits`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `credit_limits` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `invoice_count` int unsigned DEFAULT NULL,
  `sale` int unsigned DEFAULT NULL,
  `value` int unsigned NOT NULL,
  `type` enum('manual','automatic') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'automatic',
  `partner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'niyotail',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `credit_limits_store_id_foreign` (`store_id`),
  CONSTRAINT `credit_limits_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1468 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `credit_notes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `credit_notes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int unsigned NOT NULL,
  `return_order_id` int unsigned DEFAULT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int NOT NULL,
  `amount` decimal(8,2) NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `credit_notes_reference_id_unique` (`reference_id`),
  KEY `credit_notes_return_order_id_foreign` (`return_order_id`),
  KEY `credit_notes_order_id_foreign` (`order_id`),
  CONSTRAINT `credit_notes_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `credit_notes_return_order_id_foreign` FOREIGN KEY (`return_order_id`) REFERENCES `return_orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `credit_requests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `credit_requests` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `store_id` int unsigned NOT NULL,
  `partner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'niyotail',
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_barcodes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_barcodes` (
  `id` int DEFAULT NULL,
  `nt_barcode` varchar(13) DEFAULT NULL,
  `product_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `custom_barcodes_short`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `custom_barcodes_short` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nt_barcode` varchar(13) DEFAULT NULL,
  `product_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`product_id`),
  CONSTRAINT `FK_custom_barcodes_short_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3718 DEFAULT CHARSET=latin1 AVG_ROW_LENGTH=47;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dashboard_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_images` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `valid_from` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `debit_note_swp`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `debit_note_swp` (
  `prev_debit_note` varchar(255) DEFAULT NULL,
  `new_debit_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_parameters`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_parameters` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `device_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `build_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2023 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `discount_rules`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `discount_rules` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conditions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `actions` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_usage_limit` int DEFAULT NULL,
  `per_customer_limit` int DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `discount_rules_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee_role`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_role` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_role_employee_id_role_id_unique` (`employee_id`,`role_id`),
  KEY `employee_role_role_id_foreign` (`role_id`),
  CONSTRAINT `employee_role_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1405 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee_warehouse`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employee_warehouse` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `employee_warehouse_employee_id_foreign` (`employee_id`),
  KEY `employee_warehouse_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `employee_warehouse_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `employee_warehouse_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=661 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employees`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employees` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` int unsigned DEFAULT NULL,
  `otp_expiry` datetime DEFAULT NULL,
  `otp_sent_count` int unsigned NOT NULL DEFAULT '0',
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employees_email_unique` (`email`),
  UNIQUE KEY `employees_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `expressions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `expressions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `condition_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `expressions_name_index` (`name`),
  KEY `expressions_value_index` (`value`),
  KEY `expressions_max_value_index` (`max_value`),
  KEY `expressions_condition_id_foreign` (`condition_id`),
  CONSTRAINT `expressions_condition_id_foreign` FOREIGN KEY (`condition_id`) REFERENCES `conditions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `failed_jobs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1623 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flat_inventories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flat_inventories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `storage_id` bigint unsigned DEFAULT NULL,
  `source_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_id` bigint unsigned DEFAULT NULL,
  `product_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `flat_inventories_storage_id_foreign` (`storage_id`),
  KEY `flat_inventories_product_id_foreign` (`product_id`),
  KEY `flat_inventories_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `flat_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `flat_inventories_storage_id_foreign` FOREIGN KEY (`storage_id`) REFERENCES `storages` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `flat_inventories_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=229801 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `focussed_products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `focussed_products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `sales` double(15,6) NOT NULL,
  `cost` double(15,6) NOT NULL,
  `margin` decimal(9,6) NOT NULL,
  `valid_on` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `focussed_products_product_id_foreign` (`product_id`),
  KEY `focussed_products_valid_on_IDX` (`valid_on`) USING BTREE,
  CONSTRAINT `focussed_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=993853 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups_products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups_products` (
  `id` int NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned DEFAULT NULL,
  `group_id` int unsigned DEFAULT NULL,
  `valid_from` timestamp NULL DEFAULT NULL,
  `valid_to` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_groups_products_products` (`product_id`),
  CONSTRAINT `FK_groups_products_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18604 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `importer_actions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `importer_actions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `output` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resource_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employee_id` int unsigned NOT NULL,
  `status` enum('pending','completed','failed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `importer_actions_employee_id_foreign` (`employee_id`),
  CONSTRAINT `importer_actions_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17519 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `initial_inventory`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `initial_inventory` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nt_store_id` int unsigned DEFAULT NULL,
  `nt_product_id` int unsigned DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `barcode_scanned` varchar(50) DEFAULT NULL,
  `quantity` decimal(14,4) unsigned DEFAULT NULL,
  `mrp` decimal(14,4) unsigned DEFAULT NULL,
  `mrp_value` decimal(14,4) unsigned DEFAULT NULL,
  `cost` decimal(14,4) unsigned DEFAULT NULL,
  `sale_per_week` decimal(14,4) unsigned DEFAULT NULL,
  `outer_units` decimal(14,4) unsigned DEFAULT NULL,
  `case_units` decimal(14,4) unsigned DEFAULT NULL,
  `counting_date` date DEFAULT NULL,
  `counting_timestamp` timestamp NULL DEFAULT NULL,
  `brand_name` varchar(255) DEFAULT NULL,
  `marketer_name` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_l2` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `Index 1` (`id`),
  KEY `Index 2` (`nt_product_id`),
  KEY `Index 3` (`nt_store_id`),
  CONSTRAINT `FK_initial_inventory_niyoweb.stores` FOREIGN KEY (`nt_store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92398 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned DEFAULT NULL,
  `grn_item_id` int DEFAULT NULL,
  `is_direct_store_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `product_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `quantity` decimal(12,3) NOT NULL,
  `original_stock` int NOT NULL DEFAULT '0',
  `vendor_batch_number` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_cost_price` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `effective_unit_cost_price` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `manufacturing_date` date DEFAULT NULL,
  `expiry_date` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `inactive_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `inventories_product_id_warehouse_id_batch_number_unique` (`product_id`,`warehouse_id`,`batch_number`),
  KEY `inventories_warehouse_id_foreign` (`warehouse_id`),
  KEY `inventories_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  CONSTRAINT `inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `inventories_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `inventories_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105494 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventory_transactions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `previous_quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_inventory_transactions` (`inventory_id`,`type`,`source_type`,`source_id`),
  KEY `inventory_transactions_source_type_source_id_index` (`source_type`,`source_id`),
  KEY `inventory_transactions_inventory_id_foreign` (`inventory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14259498 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventory_transactions_bk`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory_transactions_bk` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `previous_quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `inventory_transactions_source_type_source_id_index` (`source_type`,`source_id`),
  KEY `inventory_transactions_inventory_id_foreign` (`inventory_id`),
  CONSTRAINT `inventory_transactions_inventory_id_foreign` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7606779 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `inventory_transactions_old`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventory_transactions_old` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `inventory_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `previous_quantity` int NOT NULL,
  `source_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_id` int DEFAULT NULL,
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_info` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3695856 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `invoices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int unsigned NOT NULL,
  `shipment_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `amount` decimal(8,2) NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int NOT NULL,
  `pdf` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancelled_on` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoices_reference_id_unique` (`reference_id`),
  KEY `invoices_shipment_id_foreign` (`shipment_id`),
  KEY `invoices_order_id_foreign` (`order_id`),
  KEY `invoices_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `invoices_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `invoices_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90906 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `invoices_transactions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `invoices_transactions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int unsigned NOT NULL,
  `transaction_id` bigint unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `invoices_transactions_invoice_id_foreign` (`invoice_id`),
  KEY `invoices_transactions_transaction_id_foreign` (`transaction_id`),
  CONSTRAINT `invoices_transactions_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `invoices_transactions_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17419 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `k1_transactions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `k1_transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(50) DEFAULT NULL,
  `store_id` int unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `group` varchar(50) DEFAULT NULL,
  `head` varchar(50) DEFAULT NULL,
  `accrual_type` varchar(50) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `amount` decimal(16,2) unsigned NOT NULL,
  `comments` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`store_id`),
  CONSTRAINT `FK_k1_transactions_stores` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `local_purchases_reference`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `local_purchases_reference` (
  `purchase_invoice_id` int DEFAULT NULL,
  `new_vendor_ref_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manifest_shipment`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manifest_shipment` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `manifest_id` int unsigned NOT NULL,
  `shipment_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manifest_shipment_manifest_id_shipment_id_unique` (`manifest_id`,`shipment_id`),
  KEY `manifest_shipment_shipment_id_foreign` (`shipment_id`),
  CONSTRAINT `manifest_shipment_manifest_id_foreign` FOREIGN KEY (`manifest_id`) REFERENCES `manifests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `manifest_shipment_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81438 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manifest_warehouse_rack`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manifest_warehouse_rack` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `manifest_id` int unsigned NOT NULL,
  `rack_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manifest_warehouse_rack_manifest_id_rack_id_unique` (`manifest_id`,`rack_id`),
  KEY `manifest_warehouse_rack_rack_id_foreign` (`rack_id`),
  CONSTRAINT `manifest_warehouse_rack_manifest_id_foreign` FOREIGN KEY (`manifest_id`) REFERENCES `manifests` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `manifest_warehouse_rack_rack_id_foreign` FOREIGN KEY (`rack_id`) REFERENCES `warehouse_racks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3945 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manifests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manifests` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned NOT NULL DEFAULT '2',
  `created_by` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `manifests_created_by_foreign` (`created_by`),
  KEY `manifests_agent_id_foreign` (`agent_id`),
  KEY `manifests_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `manifests_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `manifests_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `manifests_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9767 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manual_pi_final`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `manual_pi_final` (
  `vendor_id` int DEFAULT NULL,
  `vendor_ref_id` varchar(255) DEFAULT NULL,
  `invoice_date` text,
  `pid` varchar(255) DEFAULT NULL,
  `sku` text,
  `units_in_sku` int DEFAULT NULL,
  `invoiced_qty_units` int DEFAULT NULL,
  `grn_qty_units` int DEFAULT NULL,
  `base_price_per_unit` float DEFAULT NULL,
  `discount_per_unit` float DEFAULT NULL,
  `taxable_total` float DEFAULT NULL,
  `Invoiced_cost_total` float DEFAULT NULL,
  `post_tax_discount_total` float DEFAULT NULL,
  `effective_cost_total` float DEFAULT NULL,
  `gst_total` float DEFAULT NULL,
  `cess_total` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `marg_products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marg_products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `marg_products_name_unique` (`name`),
  KEY `marg_products_product_id_foreign` (`product_id`),
  CONSTRAINT `marg_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `marketer_levels`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketer_levels` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `marketers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `marketers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_id` int unsigned DEFAULT '3',
  `pricing` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`level_id`),
  CONSTRAINT `FK_marketers_marketer_levels` FOREIGN KEY (`level_id`) REFERENCES `marketer_levels` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1131 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `micro_beats`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `micro_beats` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `beat_id` int unsigned DEFAULT NULL,
  `centroid_lat` decimal(11,8) unsigned DEFAULT NULL,
  `centrod_long` decimal(11,8) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`beat_id`),
  CONSTRAINT `FK__micro_beats_beats` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mis_monthly_marketers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mis_monthly_marketers` (
  `marketer_id` int DEFAULT NULL,
  `marketer_code` varchar(4) DEFAULT NULL,
  `marketer_alias` varchar(255) DEFAULT NULL,
  `marketer_id_name` varchar(255) DEFAULT NULL,
  `discount` decimal(16,6) DEFAULT NULL,
  `sale` decimal(16,6) DEFAULT NULL,
  `cost` decimal(16,6) DEFAULT NULL,
  `margin` decimal(16,6) DEFAULT NULL,
  `margin_pc` decimal(9,6) DEFAULT NULL,
  `count_beats` int DEFAULT NULL,
  `count_stores` int DEFAULT NULL,
  `count_products` int DEFAULT NULL,
  `count_orders` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moq_final`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `moq_final` (
  `sku` varchar(255) NOT NULL,
  `moq` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mrp_pricing_slabs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mrp_pricing_slabs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cl2cm_cost_basis` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `vcm_from` decimal(16,6) NOT NULL,
  `vcm_to` decimal(16,6) NOT NULL,
  `discount_pc` decimal(16,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `net_promoter_score`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `net_promoter_score` (
  `id` int unsigned NOT NULL,
  `store_id` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `score` int unsigned NOT NULL,
  `feedback` varchar(250) NOT NULL DEFAULT '',
  `rating_timestamp` timestamp NULL DEFAULT NULL,
  `feedback_caller` varchar(100) NOT NULL DEFAULT '',
  `feedback_date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users` (`user_id`),
  KEY `FK_stores` (`store_id`),
  CONSTRAINT `FK__users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newcat_l1`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newcat_l1` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newcat_l2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newcat_l2` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `newcat_l1_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_newcat_l2_newcat_l1` (`newcat_l1_id`),
  CONSTRAINT `FK_newcat_l2_newcat_l1` FOREIGN KEY (`newcat_l1_id`) REFERENCES `newcat_l1` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newcat_l3`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newcat_l3` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `newcat_l2_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_newcat_l3_newcat_l2` (`newcat_l2_id`),
  CONSTRAINT `FK_newcat_l3_newcat_l2` FOREIGN KEY (`newcat_l2_id`) REFERENCES `newcat_l2` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=332 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newcat_l4`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newcat_l4` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `newcat_l3_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_newcat_l4_newcat_l3` (`newcat_l3_id`),
  CONSTRAINT `FK_newcat_l4_newcat_l3` FOREIGN KEY (`newcat_l3_id`) REFERENCES `newcat_l3` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1080 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nid_qid_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nid_qid_map` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nid` int unsigned DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `qid` int unsigned DEFAULT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `nid_count` int DEFAULT NULL,
  `qid_count` int DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4878 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `old_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `old_orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `store_id` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `sku` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `mrp` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `variant` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `subtotal` varchar(255) NOT NULL,
  `discount` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `item_taxes` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_by_type` varchar(255) NOT NULL,
  `created_by_id` int NOT NULL,
  `processed` tinyint NOT NULL DEFAULT '0',
  `errors` text,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12825 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_addresses`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_addresses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `landmark` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_addresses_city_id_foreign` (`city_id`),
  CONSTRAINT `order_addresses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=92028 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_item_inventories`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item_inventories` (
  `order_item_id` int unsigned NOT NULL,
  `batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` decimal(8,3) unsigned NOT NULL,
  `unit_cost_price` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `order_item_inventories_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `order_item_inventories_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_item_metas`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item_metas` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` int unsigned NOT NULL,
  `is_focussed` tinyint(1) NOT NULL DEFAULT '0',
  `product_id` int unsigned NOT NULL,
  `store_id` int unsigned NOT NULL,
  `margin` decimal(11,6) NOT NULL,
  `agent_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_item_metas_order_item_id_unique` (`order_item_id`),
  KEY `order_item_metas_product_id_foreign` (`product_id`),
  KEY `order_item_metas_store_id_foreign` (`store_id`),
  KEY `order_item_metas_agent_id_foreign` (`agent_id`),
  CONSTRAINT `order_item_metas_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `order_item_metas_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_item_metas_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_item_metas_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4162092 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_item_rule`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item_rule` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` int unsigned NOT NULL,
  `rule_id` int unsigned NOT NULL,
  `discount` decimal(13,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_item_rule_rule_id_foreign` (`rule_id`),
  KEY `order_item_rule_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `order_item_rule_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_item_rule_rule_id_foreign` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_item_taxes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_item_taxes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_item_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` decimal(8,2) NOT NULL,
  `amount` decimal(13,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_item_taxes_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `order_item_taxes_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33806122 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mrp` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_price` decimal(11,6) DEFAULT NULL,
  `price` decimal(13,6) NOT NULL,
  `quantity` decimal(8,3) unsigned NOT NULL,
  `subtotal` decimal(13,6) NOT NULL,
  `discount` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `tax` decimal(13,6) NOT NULL,
  `total` decimal(13,6) NOT NULL,
  `source` enum('manual','offer','group_buy') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'manual',
  `parent_sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancellation_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_items_order_id_foreign` (`order_id`),
  KEY `order_items_product_id_foreign` (`product_id`),
  CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15961877 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int unsigned NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `order_logs_order_id_foreign` (`order_id`),
  KEY `order_logs_employee_id_foreign` (`employee_id`),
  CONSTRAINT `order_logs_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `order_logs_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=738093 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `order_ratings`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_ratings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int unsigned DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_ratings_order_id_type_unique` (`order_id`,`type`),
  CONSTRAINT `order_ratings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL DEFAULT '1',
  `billing_address_id` int unsigned DEFAULT NULL,
  `shipping_address_id` int unsigned DEFAULT NULL,
  `discount_rule_id` int unsigned DEFAULT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_charges` decimal(8,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(13,6) NOT NULL,
  `discount` decimal(13,6) NOT NULL DEFAULT '0.000000',
  `tax` decimal(13,6) NOT NULL,
  `total` decimal(13,6) NOT NULL,
  `payment_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancellation_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` int unsigned DEFAULT NULL,
  `required_delivery_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_reference_id_unique` (`reference_id`),
  KEY `orders_store_id_foreign` (`store_id`),
  KEY `orders_billing_address_id_foreign` (`billing_address_id`),
  KEY `orders_shipping_address_id_foreign` (`shipping_address_id`),
  KEY `orders_discount_rule_id_foreign` (`discount_rule_id`),
  KEY `orders_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `orders_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `orders_billing_address_id_foreign` FOREIGN KEY (`billing_address_id`) REFERENCES `order_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `orders_discount_rule_id_foreign` FOREIGN KEY (`discount_rule_id`) REFERENCES `discount_rules` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `orders_shipping_address_id_foreign` FOREIGN KEY (`shipping_address_id`) REFERENCES `order_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `orders_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `orders_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=93675 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment_gateway_logs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_gateway_logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int unsigned NOT NULL,
  `gateway_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `request` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `response` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `payment_gateway_logs_payment_id_foreign` (`payment_id`),
  CONSTRAINT `payment_gateway_logs_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payments`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` int unsigned NOT NULL,
  `return_order_id` int unsigned DEFAULT NULL,
  `method` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INR',
  `amount` decimal(8,2) NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('credit','debit') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'credit',
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payments_transaction_id_unique` (`transaction_id`),
  KEY `payments_order_id_foreign` (`order_id`),
  KEY `payments_return_order_id_foreign` (`return_order_id`),
  CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `payments_return_order_id_foreign` FOREIGN KEY (`return_order_id`) REFERENCES `return_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permissions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pi_20_21`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pi_20_21` (
  `Voucher_No` text,
  `Voucher_Date` text,
  `Supplier_Invoice_Date` text,
  `Supplier_Invoice_Number` text,
  `Vendor_Id` int DEFAULT NULL,
  `Vendor_Name` text,
  `Add1` text,
  `Add2` text,
  `Add3` text,
  `State` text,
  `GSTIN_No` text,
  `GSTIN_Type` text,
  `PID` varchar(255) DEFAULT NULL,
  `Mobile` text,
  `Email` text,
  `Item_Description` text,
  `HSNCode` varchar(255) DEFAULT NULL,
  `SKU` text,
  `Invoice_Quantity` int DEFAULT NULL,
  `Units_In_SKU` int DEFAULT NULL,
  `Total_Units` int DEFAULT NULL,
  `Unit_Rate` varchar(255) DEFAULT NULL,
  `Gross_Amount` varchar(255) DEFAULT NULL,
  `Freight` int DEFAULT NULL,
  `Pre_Tax_Discount` int DEFAULT NULL,
  `Taxable` varchar(255) DEFAULT NULL,
  `GST_Perc` int DEFAULT NULL,
  `CESS_Perc` int DEFAULT NULL,
  `SPL_CESS_Per_Unit` int DEFAULT NULL,
  `SGST` varchar(255) DEFAULT NULL,
  `CGST` varchar(255) DEFAULT NULL,
  `IGST` varchar(255) DEFAULT NULL,
  `CESS` varchar(255) DEFAULT NULL,
  `SPL_CESS` varchar(255) DEFAULT NULL,
  `TCS` varchar(255) DEFAULT NULL,
  `Total_Amount` varchar(255) DEFAULT NULL,
  `Post_Tax_Discount` varchar(255) DEFAULT NULL,
  `Round_Off` varchar(255) DEFAULT NULL,
  `Invoice_Total` varchar(255) DEFAULT NULL,
  `Narration` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Vehicle_No` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `Ewaybill_No` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `GRN_Qty_In_Units` varchar(255) DEFAULT NULL,
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pi_lcl_migrate`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pi_lcl_migrate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `last_vendor_ref` varchar(255) DEFAULT NULL,
  `new_vendor_ref` varchar(255) DEFAULT NULL,
  `pi_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `picklist_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `picklist_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `picklist_id` int unsigned NOT NULL,
  `order_item_id` int unsigned NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `skip_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `picklist_items_picklist_id_order_item_id_unique` (`picklist_id`,`order_item_id`),
  KEY `picklist_items_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `picklist_items_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `picklist_items_picklist_id_foreign` FOREIGN KEY (`picklist_id`) REFERENCES `picklists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10342226 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `picklists`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `picklists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `created_by` int unsigned NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `picklists_agent_id_foreign` (`agent_id`),
  KEY `picklists_created_by_foreign` (`created_by`),
  KEY `picklists_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `picklists_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `agents` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `picklists_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `picklists_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25735 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pincode_warehouse`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pincode_warehouse` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pincode_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pincode_warehouse_warehouse_id_pincode_id_unique` (`warehouse_id`,`pincode_id`),
  KEY `pincode_warehouse_pincode_id_foreign` (`pincode_id`),
  CONSTRAINT `pincode_warehouse_pincode_id_foreign` FOREIGN KEY (`pincode_id`) REFERENCES `pincodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pincode_warehouse_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pincodes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pincodes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `pincode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pincodes_pincode_unique` (`pincode`),
  KEY `pincodes_city_id_foreign` (`city_id`),
  CONSTRAINT `pincodes_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_offer_prices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_offer_prices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned DEFAULT NULL,
  `on_offer` smallint DEFAULT '1',
  `offer_price` decimal(16,6) unsigned NOT NULL,
  `reason` enum('monthly_offer','store_new_product','product_nature','1k_mall') DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`product_id`),
  CONSTRAINT `FK_pos_offer_prices_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3532 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_order_item_metas`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_order_item_metas` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_item_id` bigint unsigned NOT NULL,
  `discount` decimal(15,6) DEFAULT '0.000000',
  `promotion` decimal(15,6) DEFAULT '0.000000',
  `commission` decimal(15,6) DEFAULT '0.000000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pos_order_item_metas_order_item_id_foreign` (`pos_order_item_id`),
  CONSTRAINT `pos_order_item_metas_order_item_id_foreign` FOREIGN KEY (`pos_order_item_id`) REFERENCES `pos_order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3268860 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_order_item_shipments`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_order_item_shipments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_item_id` bigint unsigned NOT NULL,
  `quantity` decimal(8,3) unsigned NOT NULL,
  `unit_cost` decimal(15,6) DEFAULT NULL,
  `selling_price` decimal(15,6) NOT NULL,
  `shipment_id` int unsigned DEFAULT NULL,
  `batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pos_order_item_shipments_order_item_id_foreign` (`pos_order_item_id`),
  KEY `pos_order_item_shipments_shipment_id_foreign` (`shipment_id`),
  CONSTRAINT `pos_order_item_shipments_order_item_id_foreign` FOREIGN KEY (`pos_order_item_id`) REFERENCES `pos_order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pos_order_item_shipments_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3346488 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_order_item_taxes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_order_item_taxes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_item_id` bigint unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` double(8,2) NOT NULL,
  `value` double(8,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pos_order_item_taxes_order_item_id_foreign` (`pos_order_item_id`),
  CONSTRAINT `pos_order_item_taxes_order_item_id_foreign` FOREIGN KEY (`pos_order_item_id`) REFERENCES `pos_order_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5626198 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_order_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_id` bigint unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `pos_product_id` int unsigned NOT NULL,
  `uom` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` decimal(8,3) NOT NULL,
  `price` double NOT NULL,
  `original_price` double NOT NULL,
  `mrp` double(8,2) DEFAULT NULL,
  `subtotal` double NOT NULL,
  `tax` double NOT NULL,
  `discount` double NOT NULL,
  `total` double NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pos_order_items_order_id_foreign` (`pos_order_id`),
  KEY `pos_order_items_product_id_foreign` (`product_id`),
  CONSTRAINT `pos_order_items_order_id_foreign` FOREIGN KEY (`pos_order_id`) REFERENCES `pos_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pos_order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3269871 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pos_customer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int unsigned NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordered_at` timestamp NOT NULL,
  `invoiced_at` timestamp NOT NULL,
  `platform` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_id` (`order_id`),
  UNIQUE KEY `pos_orders_order_id_unique` (`order_id`),
  KEY `pos_orders_store_id_foreign` (`store_id`),
  CONSTRAINT `pos_orders_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1345319 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_sp_fixed`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_sp_fixed` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nt_pid` int unsigned DEFAULT NULL,
  `valid` int unsigned DEFAULT '1',
  `fixed_sp` decimal(16,6) unsigned NOT NULL,
  `fixed_reason` enum('offer','store_own_product','product_nature') DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `Index 1` (`id`),
  KEY `Index 2` (`nt_pid`) USING BTREE,
  CONSTRAINT `FK_pos_sp_overrule_products` FOREIGN KEY (`nt_pid`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pos_ws_prices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pos_ws_prices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned DEFAULT NULL,
  `on_offer` smallint DEFAULT '1',
  `offer_price` decimal(16,6) unsigned NOT NULL,
  `reason` enum('market_rates','liquidation','near_expiry','sales_offer') DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index 2` (`product_id`) USING BTREE,
  CONSTRAINT `pos_ws_prices_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_category_l2_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_category_l2_map` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `category_l2_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`product_id`),
  KEY `Index 3` (`category_l2_id`) USING BTREE,
  CONSTRAINT `FK_category_l1_product_map_category_l2` FOREIGN KEY (`category_l2_id`) REFERENCES `category_l2` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_category_l1_product_map_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23937 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_demand_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_demand_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_demand_id` int unsigned NOT NULL,
  `vendor_id` int unsigned DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `days_for_average_sales` int unsigned NOT NULL,
  `average_sale` int unsigned NOT NULL,
  `lead_time` int unsigned DEFAULT NULL,
  `days_of_demand` int unsigned DEFAULT NULL,
  `demand_booster` int unsigned DEFAULT NULL,
  `safety_stock_days` int unsigned DEFAULT NULL,
  `quantity` int unsigned DEFAULT NULL,
  `cost` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_demand_items_product_demand_id_foreign` (`product_demand_id`),
  KEY `product_demand_items_vendor_id_foreign` (`vendor_id`),
  KEY `product_demand_items_sku_foreign` (`sku`),
  CONSTRAINT `product_demand_items_product_demand_id_foreign` FOREIGN KEY (`product_demand_id`) REFERENCES `product_demands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_demand_items_sku_foreign` FOREIGN KEY (`sku`) REFERENCES `product_variants` (`sku`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `product_demand_items_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_demands`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_demands` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `marketer_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_threshold_days` int unsigned NOT NULL,
  `days_for_average_sales` int unsigned NOT NULL,
  `created_by` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_demands_created_by_foreign` (`created_by`),
  KEY `product_demands_marketer_id_foreign` (`marketer_id`),
  KEY `product_demands_vendor_id_foreign` (`vendor_id`),
  CONSTRAINT `product_demands_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `product_demands_marketer_id_foreign` FOREIGN KEY (`marketer_id`) REFERENCES `marketers` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `product_demands_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_groups`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_groups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `brand_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_groups_key_brand_id` (`brand_id`),
  CONSTRAINT `FK_product_groups_brands_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41464 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_images`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_images` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_images_name_unique` (`name`),
  KEY `product_images_product_id_foreign` (`product_id`),
  CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21645 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_requests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int DEFAULT NULL,
  `cl4_id` int unsigned DEFAULT NULL,
  `is_requested_group` tinyint(1) DEFAULT '0',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `barcode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auto_assign_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` int NOT NULL,
  `collection_id` int NOT NULL,
  `uom` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_mrp` double(8,2) NOT NULL,
  `outer_units` double(8,2) DEFAULT NULL,
  `case_units` double(8,2) DEFAULT NULL,
  `tax_class_id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `hsn_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `source_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_requests_cl4_id_foreign` (`cl4_id`),
  CONSTRAINT `product_requests_cl4_id_foreign` FOREIGN KEY (`cl4_id`) REFERENCES `newcat_l4` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9038 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_status_manager`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_status_manager` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47789 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_tag`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_tag` (
  `product_id` int unsigned NOT NULL,
  `tag_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `product_tag_product_id_tag_id_unique` (`product_id`,`tag_id`),
  KEY `product_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `product_tag_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `product_variants`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_variants` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `uom` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mrp` double(8,2) NOT NULL,
  `min_price` decimal(11,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(11,6) NOT NULL,
  `max_price` decimal(11,6) NOT NULL DEFAULT '0.000000',
  `moq` int NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_variants_product_id_value_unique` (`product_id`,`value`),
  UNIQUE KEY `product_variants_sku_unique` (`sku`),
  CONSTRAINT `product_variants_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79579 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` int unsigned DEFAULT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `barcode` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `marketer_barcode` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `hsn_sac_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uom` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pc',
  `meta_title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `published_at` datetime DEFAULT NULL,
  `tax_class_id` int unsigned NOT NULL,
  `cl4_id` int unsigned DEFAULT NULL,
  `allow_back_orders` tinyint unsigned NOT NULL DEFAULT '0' COMMENT '0=Deduct on order placed\\n1=Deduct on invoice genrated',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_slug_unique` (`slug`),
  KEY `products_brand_id_foreign` (`brand_id`),
  KEY `products_tax_class_id_foreign` (`tax_class_id`),
  KEY `products_group_id_foreign` (`group_id`),
  KEY `products_cl4_id_foreign` (`cl4_id`),
  CONSTRAINT `products_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `products_cl4_id_foreign` FOREIGN KEY (`cl4_id`) REFERENCES `newcat_l4` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `product_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `products_attributes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products_attributes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `attribute_id` int unsigned NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_attributes_product_id_attribute_id_unique` (`product_id`,`attribute_id`),
  KEY `products_attributes_attribute_id_foreign` (`attribute_id`),
  CONSTRAINT `products_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `products_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14342 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile_question_options`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile_question_options` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int unsigned NOT NULL,
  `option` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` tinyint NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `profile_question_options_question_id_foreign` (`question_id`),
  CONSTRAINT `profile_question_options_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `profile_questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profile_questions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile_questions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint NOT NULL DEFAULT '0',
  `is_mandatory` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_cart`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_cart` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned NOT NULL,
  `platform_id` bigint unsigned DEFAULT NULL,
  `discount_platform_id` bigint unsigned DEFAULT NULL,
  `vendor_address_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_ref_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_direct_store_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `is_local_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `store_id` int unsigned DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `eway_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` double(8,2) NOT NULL DEFAULT '0.00',
  `round` double(8,2) NOT NULL DEFAULT '0.00',
  `tax` double(8,2) NOT NULL DEFAULT '0.00',
  `tcs` double(8,2) DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `scanned_file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'system',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_cart_purchase_order_id_foreign` (`purchase_order_id`),
  KEY `purchase_invoice_cart_vendor_id_foreign` (`vendor_id`),
  KEY `purchase_invoice_cart_vendor_address_id_foreign` (`vendor_address_id`),
  KEY `purchase_invoice_cart_warehouse_id_foreign` (`warehouse_id`),
  KEY `purchase_invoice_cart_platform_id_foreign` (`platform_id`),
  KEY `purchase_invoice_cart_store_id_foreign` (`store_id`),
  CONSTRAINT `purchase_invoice_cart_platform_id_foreign` FOREIGN KEY (`platform_id`) REFERENCES `purchase_platforms` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `purchase_invoice_cart_purchase_order_id_foreign` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_cart_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `purchase_invoice_cart_vendor_address_id_foreign` FOREIGN KEY (`vendor_address_id`) REFERENCES `vendor_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_cart_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_cart_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13394 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_cart_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_cart_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_cart_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `rack_id` int DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int unsigned NOT NULL,
  `received_quantity` int DEFAULT NULL,
  `manufacturing_date` date DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `vendor_batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` decimal(15,6) NOT NULL,
  `discount` decimal(15,6) DEFAULT NULL,
  `cash_discount` decimal(15,6) DEFAULT NULL,
  `post_tax_discount` decimal(15,6) DEFAULT NULL,
  `value_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_tax_discount_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86818 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_cart_short_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_cart_short_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `hsn` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_class` int unsigned NOT NULL,
  `mrp` double(8,2) DEFAULT NULL,
  `discount` double(8,2) NOT NULL DEFAULT '0.00',
  `invoice_cost` double(8,2) NOT NULL,
  `post_tax_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `effective_cost` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_cart_short_items_cart_id_foreign` (`cart_id`),
  KEY `purchase_invoice_cart_short_items_tax_class_foreign` (`tax_class`),
  CONSTRAINT `purchase_invoice_cart_short_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `purchase_invoice_cart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_cart_short_items_tax_class_foreign` FOREIGN KEY (`tax_class`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_debit_note_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_debit_note_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `debit_note_id` int unsigned NOT NULL,
  `source_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` int NOT NULL,
  `item_id` int unsigned NOT NULL,
  `quantity` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_debit_note_items_debit_note_id_foreign` (`debit_note_id`),
  KEY `purchase_invoice_debit_note_items_item_id_foreign` (`item_id`),
  CONSTRAINT `purchase_invoice_debit_note_items_debit_note_id_foreign` FOREIGN KEY (`debit_note_id`) REFERENCES `purchase_invoice_debit_notes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1419 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_debit_notes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_debit_notes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_invoice_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `created_by` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'short_inventory',
  `generated_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_debit_notes_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `purchase_invoice_debit_notes_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `purchase_invoice_debit_notes_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_debit_notes_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=876 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_grn`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_grn` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `grn_path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'created',
  `inventory_imported` tinyint(1) NOT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_grn_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `purchase_invoice_grn_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `purchase_invoice_grn_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_grn_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19140 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_grn_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_grn_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `grn_id` int unsigned NOT NULL,
  `item_id` int unsigned NOT NULL,
  `quantity` int NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int unsigned DEFAULT NULL,
  `is_requested` tinyint(1) NOT NULL DEFAULT '0',
  `requested_product_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_grn_items_grn_id_foreign` (`grn_id`),
  KEY `purchase_invoice_grn_items_item_id_foreign` (`item_id`),
  CONSTRAINT `purchase_invoice_grn_items_grn_id_foreign` FOREIGN KEY (`grn_id`) REFERENCES `purchase_invoice_grn` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_grn_items_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `purchase_invoice_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=153601 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_item_modification_requests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_item_modification_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_item_id` int unsigned NOT NULL,
  `product_id` int unsigned DEFAULT NULL,
  `quantity` int unsigned DEFAULT NULL,
  `base_cost` decimal(15,6) DEFAULT NULL,
  `scheme_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_tax_discount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delete_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` tinyint NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `status` smallint NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pii_modification_requests_purchase_invoice_item_id_foreign` (`purchase_invoice_item_id`),
  KEY `pii_modification_requests_product_id_foreign` (`product_id`),
  KEY `pii_modification_requests_employee_id_foreign` (`employee_id`),
  CONSTRAINT `pii_modification_requests_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pii_modification_requests_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pii_modification_requests_purchase_invoice_item_id_foreign` FOREIGN KEY (`purchase_invoice_item_id`) REFERENCES `purchase_invoice_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_item_taxes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_item_taxes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_item_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `percentage` decimal(8,2) NOT NULL,
  `amount` decimal(15,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_item_taxes_purchase_invoice_item_id_foreign` (`purchase_invoice_item_id`),
  CONSTRAINT `purchase_invoice_item_taxes_purchase_invoice_item_id_foreign` FOREIGN KEY (`purchase_invoice_item_id`) REFERENCES `purchase_invoice_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned NOT NULL,
  `purchase_order_item_id` int DEFAULT NULL,
  `grn_id` int DEFAULT NULL,
  `product_id` int unsigned NOT NULL,
  `rack_id` int DEFAULT NULL,
  `is_requested` tinyint(1) NOT NULL DEFAULT '0',
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_cost` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `trade_discount` decimal(15,6) DEFAULT '0.000000',
  `scheme_discount` decimal(15,6) DEFAULT '0.000000',
  `qps_discount` decimal(15,6) DEFAULT '0.000000',
  `cash_discount` decimal(15,6) DEFAULT '0.000000',
  `program_schemes` decimal(15,6) DEFAULT '0.000000',
  `other_discount` decimal(15,6) DEFAULT '0.000000',
  `total_taxable` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `post_tax_discount` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `reward_points_cashback` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `taxes` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` decimal(15,6) DEFAULT NULL,
  `sgst` decimal(15,6) DEFAULT NULL,
  `igst` decimal(15,6) DEFAULT NULL,
  `cess` decimal(15,6) DEFAULT NULL,
  `spl_cess` decimal(15,6) DEFAULT NULL,
  `additional_charges` decimal(15,6) DEFAULT NULL,
  `tax` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `total_invoiced_cost` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `post_invoice_schemes` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `post_invoice_qps` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `post_invoice_claim` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `post_invoice_incentive` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `post_invoice_other` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `post_invoice_tot` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `total_effective_cost` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `quantity` int unsigned NOT NULL,
  `received_quantity` int unsigned DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `manufacturing_date` date DEFAULT NULL,
  `vendor_batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_items_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `purchase_invoice_items_product_id_foreign` (`product_id`),
  CONSTRAINT `purchase_invoice_items_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=154459 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_modification_requests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_modification_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned NOT NULL,
  `vendor_id` int unsigned DEFAULT NULL,
  `vendor_address_id` int unsigned DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform_id` bigint unsigned DEFAULT NULL,
  `eway_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forecast_payment_date` date DEFAULT NULL,
  `delete_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` tinyint NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `status` smallint NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pi_modification_requests_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `pi_modification_requests_vendor_id_foreign` (`vendor_id`),
  KEY `pi_modification_requests_vendor_address_id_foreign` (`vendor_address_id`),
  KEY `pi_modification_requests_platform_id_foreign` (`platform_id`),
  KEY `pi_modification_requests_employee_id_foreign` (`employee_id`),
  CONSTRAINT `pi_modification_requests_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pi_modification_requests_platform_id_foreign` FOREIGN KEY (`platform_id`) REFERENCES `purchase_platforms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pi_modification_requests_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pi_modification_requests_vendor_address_id_foreign` FOREIGN KEY (`vendor_address_id`) REFERENCES `vendor_addresses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pi_modification_requests_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoice_short_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoice_short_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int NOT NULL,
  `hsn` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_class` int unsigned NOT NULL,
  `mrp` double(8,2) DEFAULT NULL,
  `discount` double(8,2) NOT NULL DEFAULT '0.00',
  `invoice_cost` double(8,2) NOT NULL,
  `post_tax_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `effective_cost` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_invoice_short_items_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `purchase_invoice_short_items_tax_class_foreign` (`tax_class`),
  CONSTRAINT `purchase_invoice_short_items_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoice_short_items_tax_class_foreign` FOREIGN KEY (`tax_class`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_invoices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_invoices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `cart_id` int unsigned DEFAULT NULL,
  `vendor_id` int unsigned NOT NULL,
  `platform_id` bigint unsigned DEFAULT NULL,
  `discount_platform_id` bigint unsigned DEFAULT NULL,
  `purchase_order_id` int unsigned DEFAULT NULL,
  `vendor_address_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_ref_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_direct_store_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `is_local_purchase` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_date` date NOT NULL,
  `forecast_payment_date` date DEFAULT NULL,
  `eway_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `tcs` double(8,2) DEFAULT NULL,
  `round` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `total` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'generated',
  `scanned_file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `purchase_invoices_warehouse_id_foreign` (`warehouse_id`),
  KEY `purchase_invoices_purchase_order_id_foreign` (`purchase_order_id`),
  KEY `purchase_invoices_vendor_address_id_foreign` (`vendor_address_id`),
  KEY `purchase_invoices_vendor_id_foreign` (`vendor_id`),
  KEY `purchase_invoices_platform_id_foreign` (`platform_id`),
  CONSTRAINT `purchase_invoices_platform_id_foreign` FOREIGN KEY (`platform_id`) REFERENCES `purchase_platforms` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `purchase_invoices_purchase_order_id_foreign` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoices_vendor_address_id_foreign` FOREIGN KEY (`vendor_address_id`) REFERENCES `vendor_addresses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoices_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_invoices_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19153 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_order_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int unsigned NOT NULL,
  `demand_item_id` int unsigned DEFAULT NULL,
  `product_id` int unsigned DEFAULT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int NOT NULL DEFAULT '0',
  `price_calculation_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_calculation_factor` double(8,2) DEFAULT NULL,
  `cost_per_unit` double(8,2) NOT NULL DEFAULT '0.00',
  `total_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_order_items_purchase_order_id_foreign` (`purchase_order_id`),
  KEY `purchase_order_items_demand_item_id_foreign` (`demand_item_id`),
  CONSTRAINT `purchase_order_items_demand_item_id_foreign` FOREIGN KEY (`demand_item_id`) REFERENCES `auto_replenish_demand` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `purchase_order_items_purchase_order_id_foreign` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=137879 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned NOT NULL,
  `vendor_address_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `warehouse_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimated_delivery_date` date DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `purchase_orders_created_by_foreign` (`created_by`),
  KEY `purchase_orders_vendor_id_foreign` (`vendor_id`),
  CONSTRAINT `purchase_orders_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `purchase_orders_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16924 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `purchase_platforms`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `purchase_platforms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `platform` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'purchase',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `putaway_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `putaway_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `putaway_id` bigint unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `storage_id` bigint unsigned DEFAULT NULL,
  `quantity` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `putaway_items_putaway_id_foreign` (`putaway_id`),
  KEY `putaway_items_product_id_foreign` (`product_id`),
  KEY `putaway_items_storage_id_foreign` (`storage_id`),
  CONSTRAINT `putaway_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `putaway_items_putaway_id_foreign` FOREIGN KEY (`putaway_id`) REFERENCES `putaways` (`id`),
  CONSTRAINT `putaway_items_storage_id_foreign` FOREIGN KEY (`storage_id`) REFERENCES `storages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=703 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `putaways`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `putaways` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `putaways_employee_id_foreign` (`employee_id`),
  KEY `putaways_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `putaways_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `putaways_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=734 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rack_product`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rack_product` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rack_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `status` int unsigned NOT NULL DEFAULT '1',
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`rack_id`),
  KEY `Index 3` (`product_id`),
  CONSTRAINT `FK_rack_product_niyoweb.products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_rack_product_warehouse_racks` FOREIGN KEY (`rack_id`) REFERENCES `warehouse_racks` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33361 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rate_credit_note_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rate_credit_note_orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `rate_credit_note_id` bigint unsigned NOT NULL,
  `pos_order_id` bigint unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rate_credit_note_orders_rate_credit_note_id_foreign` (`rate_credit_note_id`),
  KEY `rate_credit_note_orders_pos_order_id_foreign` (`pos_order_id`),
  CONSTRAINT `rate_credit_note_orders_pos_order_id_foreign` FOREIGN KEY (`pos_order_id`) REFERENCES `pos_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rate_credit_note_orders_rate_credit_note_id_foreign` FOREIGN KEY (`rate_credit_note_id`) REFERENCES `rate_credit_notes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3002001 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rate_credit_notes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rate_credit_notes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `financial_year` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int unsigned NOT NULL,
  `subtotal` decimal(8,2) NOT NULL,
  `tax` decimal(8,2) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `generated_on` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rate_credit_notes_number_unique` (`number`),
  KEY `rate_credit_notes_store_id_foreign` (`store_id`),
  CONSTRAINT `rate_credit_notes_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=49650 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `return_cart_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `return_cart_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `return_cart_id` int unsigned NOT NULL,
  `shipment_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `units` double(8,2) DEFAULT NULL,
  `reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_order_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `return_cart_items_return_cart_id_foreign` (`return_cart_id`),
  KEY `return_cart_items_shipment_id_foreign` (`shipment_id`),
  KEY `return_cart_items_return_order_id_foreign` (`return_order_id`),
  CONSTRAINT `return_cart_items_return_cart_id_foreign` FOREIGN KEY (`return_cart_id`) REFERENCES `return_carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `return_cart_items_return_order_id_foreign` FOREIGN KEY (`return_order_id`) REFERENCES `return_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `return_cart_items_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43959 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `return_carts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `return_carts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_by_id` int unsigned DEFAULT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `return_carts_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `return_carts_store_id_foreign` (`store_id`),
  KEY `return_carts_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `return_carts_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `return_carts_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2253 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `return_order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `return_order_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `return_order_id` int unsigned NOT NULL,
  `order_item_id` int unsigned NOT NULL,
  `units` double(8,2) DEFAULT NULL,
  `reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `return_order_items_return_order_id_order_item_id_unique` (`return_order_id`,`order_item_id`),
  KEY `return_order_items_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `return_order_items_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `return_order_items_return_order_id_foreign` FOREIGN KEY (`return_order_id`) REFERENCES `return_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=886540 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `return_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `return_orders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int unsigned DEFAULT NULL,
  `order_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` int unsigned DEFAULT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `return_orders_order_id_foreign` (`order_id`),
  KEY `return_orders_shipment_id_foreign` (`shipment_id`),
  KEY `return_orders_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `return_orders_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `return_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `return_orders_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `return_orders_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36665 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_permission`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_permission` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int unsigned NOT NULL,
  `role_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_permission_permission_id_role_id_unique` (`permission_id`,`role_id`),
  KEY `role_permission_role_id_foreign` (`role_id`),
  CONSTRAINT `role_permission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rule_targets`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rule_targets` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `rule_id` int unsigned NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `rule_targets_rule_id_foreign` (`rule_id`),
  CONSTRAINT `rule_targets_rule_id_foreign` FOREIGN KEY (`rule_id`) REFERENCES `rules` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rules`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rules` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `should_stop` tinyint(1) NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime DEFAULT NULL,
  `offer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `source_quantity` int unsigned NOT NULL,
  `target_quantity` int unsigned NOT NULL,
  `max_offer` int unsigned DEFAULT NULL,
  `user_limit` int unsigned DEFAULT NULL,
  `total_limit` int unsigned DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shipment_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipment_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int unsigned NOT NULL,
  `order_item_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shipment_items_shipment_id_order_item_id_unique` (`shipment_id`,`order_item_id`),
  KEY `shipment_items_order_item_id_foreign` (`order_item_id`),
  CONSTRAINT `shipment_items_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `shipment_items_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12239238 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shipments`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shipments` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int unsigned NOT NULL,
  `picklist_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `boxes` int unsigned DEFAULT NULL,
  `otp` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `shipments_order_id_foreign` (`order_id`),
  KEY `shipments_picklist_id_foreign` (`picklist_id`),
  KEY `shipments_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `shipments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `shipments_picklist_id_foreign` FOREIGN KEY (`picklist_id`) REFERENCES `picklists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shipments_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=90472 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `special_order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `special_order_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `mrp` float DEFAULT NULL,
  `quantity` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `special_return`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `special_return` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12979 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spl_cn_2021`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spl_cn_2021` (
  `month` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `store_id` int DEFAULT NULL,
  `mrp` float DEFAULT NULL,
  `billing_price` float DEFAULT NULL,
  `total_quantity` float DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `tax` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `total_commission` float DEFAULT NULL,
  `pos_pid` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `tax_class` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `promotion_total` float DEFAULT NULL,
  `discount_total` float DEFAULT NULL,
  `commission_total` float DEFAULT NULL,
  `promo_subtotal` float DEFAULT NULL,
  `discount_subtotal` float DEFAULT NULL,
  `comm_subtotal` float DEFAULT NULL,
  `promo_tax` float DEFAULT NULL,
  `promo_cess` float DEFAULT NULL,
  `discount_tax` float DEFAULT NULL,
  `discount_cess` float DEFAULT NULL,
  `comm_tax` float DEFAULT NULL,
  `comm_cess` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `states`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `states` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `states_country_id_foreign` (`country_id`),
  CONSTRAINT `states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_transfer_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_transfer_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `stock_transfer_id` bigint unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `requested_quantity` int NOT NULL DEFAULT '0',
  `confirmed_quantity` int NOT NULL DEFAULT '0',
  `sent_quantity` int NOT NULL DEFAULT '0',
  `deficiency_reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `received_quantity` int DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stock_transfer_items_product_id_foreign` (`product_id`),
  KEY `stock_transfer_items_stock_transfer_id_foreign` (`stock_transfer_id`),
  CONSTRAINT `stock_transfer_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stock_transfer_items_stock_transfer_id_foreign` FOREIGN KEY (`stock_transfer_id`) REFERENCES `stock_transfers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_transfer_notes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_transfer_notes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `stock_transfer_id` bigint unsigned NOT NULL,
  `reference_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `financial_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int NOT NULL,
  `pdf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stock_transfer_notes_stock_transfer_id_foreign` (`stock_transfer_id`),
  CONSTRAINT `stock_transfer_notes_stock_transfer_id_foreign` FOREIGN KEY (`stock_transfer_id`) REFERENCES `stock_transfers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_transfers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_transfers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `from_warehouse_id` int unsigned NOT NULL,
  `to_warehouse_id` int unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eway_bill_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stock_transfers_from_warehouse_id_foreign` (`from_warehouse_id`),
  KEY `stock_transfers_to_warehouse_id_foreign` (`to_warehouse_id`),
  CONSTRAINT `stock_transfers_from_warehouse_id_foreign` FOREIGN KEY (`from_warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `stock_transfers_to_warehouse_id_foreign` FOREIGN KEY (`to_warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stock_write_off_details`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stock_write_off_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `write_off_note_id` int unsigned DEFAULT NULL,
  `write_off_reason` enum('expired','damaged','lost','stolen','found','returned') DEFAULT NULL,
  `write_off_date` date DEFAULT NULL,
  `product_id` int unsigned DEFAULT NULL,
  `batch` varchar(191) DEFAULT NULL,
  `quantity` bigint unsigned DEFAULT NULL,
  `unit_cost_price` decimal(16,6) unsigned DEFAULT NULL,
  `total_written_off` decimal(16,6) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`product_id`),
  CONSTRAINT `FK_stock_write_off_details_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `storages`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `storages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int unsigned NOT NULL,
  `zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `storages_warehouse_id_foreign` (`warehouse_id`),
  CONSTRAINT `storages_warehouse_id_foreign` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1731 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_answers`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_answers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `question_id` int unsigned NOT NULL,
  `answer_id` int unsigned DEFAULT NULL,
  `answer` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by_agent` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `store_answers_store_id_foreign` (`store_id`),
  KEY `store_answers_answer_id_foreign` (`answer_id`),
  KEY `store_answers_question_id_foreign` (`question_id`),
  KEY `store_answers_updated_by_agent_foreign` (`updated_by_agent`),
  CONSTRAINT `store_answers_answer_id_foreign` FOREIGN KEY (`answer_id`) REFERENCES `profile_question_options` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `store_answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `profile_questions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `store_answers_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `store_answers_updated_by_agent_foreign` FOREIGN KEY (`updated_by_agent`) REFERENCES `agents` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_credits`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_credits` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `order_id` int unsigned DEFAULT NULL,
  `amount` decimal(8,2) NOT NULL,
  `type` enum('credit','debit') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `store_credits_order_id_foreign` (`order_id`),
  KEY `store_credits_user_id_foreign` (`user_id`),
  CONSTRAINT `store_credits_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `store_credits_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_order_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_order_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_order_id` bigint unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `sku` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mrp` decimal(11,6) NOT NULL,
  `price` decimal(11,6) NOT NULL,
  `quantity` int NOT NULL,
  `subtotal` decimal(13,6) NOT NULL,
  `discount` decimal(13,6) NOT NULL,
  `tax` decimal(13,6) NOT NULL,
  `total` decimal(13,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `store_order_items_store_order_id_foreign` (`store_order_id`),
  KEY `store_order_items_product_id_foreign` (`product_id`),
  CONSTRAINT `store_order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `store_order_items_store_order_id_foreign` FOREIGN KEY (`store_order_id`) REFERENCES `store_orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=473169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_orders`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `store_id` int unsigned NOT NULL,
  `order_id` int unsigned DEFAULT NULL,
  `warehouse_id` int unsigned DEFAULT NULL,
  `subtotal` decimal(13,6) NOT NULL,
  `discount` decimal(13,6) NOT NULL,
  `shipping_charges` decimal(8,2) NOT NULL,
  `tax` decimal(13,6) NOT NULL,
  `total` decimal(13,6) NOT NULL,
  `source` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancellation_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by_id` bigint unsigned NOT NULL,
  `additional_info` json DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `store_orders_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  KEY `store_orders_store_id_foreign` (`store_id`),
  KEY `store_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `store_orders_ibfk_1` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `store_orders_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44948 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_os_age`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_os_age` (
  `store_id` int unsigned DEFAULT NULL,
  `net_os` decimal(24,6) DEFAULT NULL,
  `run_os` decimal(24,6) DEFAULT NULL,
  `osdays` decimal(34,6) DEFAULT NULL,
  `osday` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_purchase_invoices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_purchase_invoices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `purchase_invoice_id` int unsigned NOT NULL,
  `store_id` int unsigned NOT NULL,
  `store_order_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `store_purchase_invoices_store_order_id_foreign` (`store_order_id`),
  KEY `store_purchase_invoices_purchase_invoice_id_foreign` (`purchase_invoice_id`),
  KEY `store_purchase_invoices_store_id_foreign` (`store_id`),
  CONSTRAINT `store_purchase_invoices_purchase_invoice_id_foreign` FOREIGN KEY (`purchase_invoice_id`) REFERENCES `purchase_invoices` (`id`),
  CONSTRAINT `store_purchase_invoices_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`),
  CONSTRAINT `store_purchase_invoices_store_order_id_foreign` FOREIGN KEY (`store_order_id`) REFERENCES `store_orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7904 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_tag`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_tag` (
  `store_id` int unsigned NOT NULL,
  `tag_id` int unsigned NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `store_tags_store_id_tag_id_unique` (`store_id`,`tag_id`),
  KEY `store_tags_tag_id_foreign` (`tag_id`),
  CONSTRAINT `store_tags_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `store_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_user`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `store_id` int unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `store_user_user_id_store_id_unique` (`user_id`,`store_id`),
  KEY `store_user_store_id_foreign` (`store_id`),
  CONSTRAINT `store_user_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `store_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3833 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `store_wise_os`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_wise_os` (
  `store_id` int DEFAULT NULL,
  `store_name` char(255) DEFAULT NULL,
  `os_date` date DEFAULT NULL,
  `os_amt` decimal(16,6) DEFAULT NULL,
  `os_age` decimal(10,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stores`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stores` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `beat_id` int unsigned DEFAULT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `legal_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gstin` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` decimal(11,8) DEFAULT NULL,
  `lng` decimal(11,8) DEFAULT NULL,
  `location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_person` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `verified` tinyint unsigned NOT NULL DEFAULT '0',
  `status` tinyint unsigned NOT NULL DEFAULT '0',
  `disable_reason` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `stores_beat_id_foreign` (`beat_id`),
  CONSTRAINT `stores_beat_id_foreign` FOREIGN KEY (`beat_id`) REFERENCES `beats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4694 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `syncs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `syncs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `synced_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tax_classes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tax_classes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tax_classes_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `taxes`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `taxes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_class_id` int unsigned DEFAULT NULL,
  `source_state_id` int unsigned NOT NULL,
  `supply_state_id` int unsigned DEFAULT NULL,
  `supply_country_id` int unsigned DEFAULT NULL,
  `min_price` decimal(8,2) NOT NULL DEFAULT '0.00',
  `max_price` decimal(8,2) DEFAULT NULL,
  `percentage` decimal(8,2) NOT NULL,
  `additional_charges_per_unit` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `taxes_tax_class_id_foreign` (`tax_class_id`),
  KEY `taxes_source_state_id_foreign` (`source_state_id`),
  KEY `taxes_supply_state_id_foreign` (`supply_state_id`),
  KEY `taxes_supply_country_id_foreign` (`supply_country_id`),
  CONSTRAINT `taxes_source_state_id_foreign` FOREIGN KEY (`source_state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `taxes_supply_country_id_foreign` FOREIGN KEY (`supply_country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `taxes_supply_state_id_foreign` FOREIGN KEY (`supply_state_id`) REFERENCES `states` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `taxes_tax_class_id_foreign` FOREIGN KEY (`tax_class_id`) REFERENCES `tax_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp_barcode_correction`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `temp_barcode_correction` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pid` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `barcode` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14252 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp_cl4_ct_correction`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `temp_cl4_ct_correction` (
  `cl4_id` int DEFAULT NULL,
  `marketer_level_id` int DEFAULT NULL,
  `commission_tag_id` int DEFAULT NULL,
  `new_commission_tag_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp_duplicate_pos_orders_delete`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `temp_duplicate_pos_orders_delete` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `pos_order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp_rcn_correction`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `temp_rcn_correction` (
  `order_id` int DEFAULT NULL,
  `item_id` int DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `system_quantity` float DEFAULT NULL,
  `name` text COLLATE utf8mb4_general_ci,
  `total` float DEFAULT NULL,
  `mrp` float DEFAULT NULL,
  `total_mrp` float DEFAULT NULL,
  `actual_promotion` float DEFAULT NULL,
  `current_promotion` float DEFAULT NULL,
  `promotion` float DEFAULT NULL,
  `commission` float DEFAULT NULL,
  `discount` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction_deletion_requests`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_deletion_requests` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delete_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employee_id` int unsigned NOT NULL,
  `status` smallint NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_modification_requests_transaction_id_foreign` (`transaction_id`),
  KEY `t_modification_requests_employee_id_foreign` (`employee_id`),
  CONSTRAINT `t_modification_requests_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `t_modification_requests_transaction_id_foreign` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `store_id` int unsigned NOT NULL,
  `source_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source_id` int unsigned DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `payment_method` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `type` enum('credit','debit') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(16,2) NOT NULL,
  `balance` decimal(16,2) NOT NULL DEFAULT '0.00',
  `status` enum('pending','failed','cancelled','completed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by_id` int unsigned DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `opp_acc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opp_acc_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transactions_transaction_id_unique` (`transaction_id`),
  KEY `transactions_store_id_foreign` (`store_id`),
  KEY `transactions_source_type_source_id_index` (`source_type`,`source_id`),
  KEY `transactions_created_by_type_created_by_id_index` (`created_by_type`,`created_by_id`),
  CONSTRAINT `transactions_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=221829 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transactions_settlement`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions_settlement` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `debit_transaction_id` bigint unsigned NOT NULL,
  `credit_transaction_id` bigint unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `transactions_settlement_debit_transaction_id_foreign` (`debit_transaction_id`),
  KEY `transactions_settlement_credit_transaction_id_foreign` (`credit_transaction_id`),
  CONSTRAINT `transactions_settlement_credit_transaction_id_foreign` FOREIGN KEY (`credit_transaction_id`) REFERENCES `transactions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `transactions_settlement_debit_transaction_id_foreign` FOREIGN KEY (`debit_transaction_id`) REFERENCES `transactions` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=214577 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_feedback`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_feedback` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `type` enum('feedback','product_request') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_feedback_user_id_foreign` (`user_id`),
  CONSTRAINT `user_feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `otp` int unsigned DEFAULT NULL,
  `otp_expiry` datetime DEFAULT NULL,
  `otp_sent_count` int unsigned NOT NULL DEFAULT '0',
  `notes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=3762 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_addresses`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_addresses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned NOT NULL,
  `gstin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` decimal(12,8) DEFAULT NULL,
  `longitude` decimal(12,8) DEFAULT NULL,
  `location` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int unsigned DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'India',
  `country_code` int unsigned DEFAULT NULL,
  `status` tinyint NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendors_addresses_vendor_id_foreign` (`vendor_id`),
  KEY `vendors_addresses_city_id_foreign` (`city_id`),
  CONSTRAINT `vendors_addresses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `vendors_addresses_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1023 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_bank_accounts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_bank_accounts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int DEFAULT '1',
  `bank` varchar(255) DEFAULT NULL,
  `ifsc` varchar(11) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `type` enum('savings','current') DEFAULT NULL,
  `icici_beneficiary_id` varchar(255) DEFAULT NULL,
  `icici_beneficiary_nickname` varchar(255) DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index 2` (`vendor_id`),
  CONSTRAINT `FK__vendors_2` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=700 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_credit`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_credit` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned DEFAULT NULL,
  `credit_days` int unsigned DEFAULT NULL,
  `credit_limit` decimal(12,2) unsigned DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `valid_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `index_vendor_credit_vendor_id` (`vendor_id`),
  CONSTRAINT `FK_vendor_credit_vendor_id` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_invoice_items`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_invoice_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_invoice_id` int unsigned NOT NULL,
  `product_id` int unsigned NOT NULL,
  `product_sku` varchar(191) NOT NULL,
  `total_quantity` int unsigned NOT NULL DEFAULT '0',
  `mktr_batch_code` varchar(191) DEFAULT '0',
  `mfg_date` date DEFAULT NULL,
  `exp_date` date DEFAULT NULL,
  `total_base_cost` decimal(15,6) unsigned NOT NULL DEFAULT '0.000000',
  `total_pre-tax_discount` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_taxable` decimal(15,6) unsigned NOT NULL DEFAULT '0.000000',
  `total_cgst` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_sgst` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_igst` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_cess` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_post-tax_discount` decimal(15,6) unsigned DEFAULT '0.000000',
  `total_cost` decimal(15,6) unsigned NOT NULL DEFAULT '0.000000',
  `batch_id` varchar(191) NOT NULL DEFAULT '0',
  `unit_cost` decimal(15,6) NOT NULL DEFAULT '0.000000',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`vendor_invoice_id`),
  KEY `Index 3` (`product_id`),
  CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `vendor_invoice_id` FOREIGN KEY (`vendor_invoice_id`) REFERENCES `vendor_invoices` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2014 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_invoices`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_invoices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `vendor_id` int unsigned DEFAULT NULL,
  `reference` varchar(191) DEFAULT NULL,
  `amount_before_taxes` decimal(15,6) NOT NULL,
  `CGST` decimal(15,6) NOT NULL,
  `SGST` decimal(15,6) NOT NULL,
  `IGST` decimal(15,6) NOT NULL,
  `Cess` decimal(15,6) NOT NULL,
  `post_tax_discount` decimal(15,6) NOT NULL,
  `rounding` decimal(15,6) NOT NULL,
  `total_incl_tax` decimal(15,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `foreign_vendor_id` (`vendor_id`),
  CONSTRAINT `FK1_vendors_vendor_id` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor_marketer_map`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendor_marketer_map` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned NOT NULL,
  `marketer_id` int unsigned NOT NULL,
  `relation` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mov` int NOT NULL,
  `lead_time` int NOT NULL,
  `credit_term` int NOT NULL,
  `tot_signed` tinyint(1) NOT NULL DEFAULT '0',
  `tot_file` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_discount` int NOT NULL DEFAULT '0',
  `comments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `priority` int NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_from` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_marketer_map_vendor_id_foreign` (`vendor_id`),
  KEY `vendor_marketer_map_marketer_id_foreign` (`marketer_id`),
  CONSTRAINT `vendor_marketer_map_marketer_id_foreign` FOREIGN KEY (`marketer_id`) REFERENCES `marketers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `vendor_marketer_map_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendors` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `trade_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint unsigned NOT NULL DEFAULT '0',
  `legal_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `incorporation_type` enum('private limited','partnership','llp','proprietorship','individual','public limited','government','society','association','non-profit','international','huf') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('trading','non-trading') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `GSTIN` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PAN` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_days` int unsigned DEFAULT NULL,
  `credit_limit` int unsigned DEFAULT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1055 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors_contacts`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendors_contacts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `vendor_id` int unsigned NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `vendors_contacts_vendor_id_foreign` (`vendor_id`),
  CONSTRAINT `vendors_contacts_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors_non_trading_2`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vendors_non_trading_2` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint unsigned NOT NULL DEFAULT '0',
  `legal_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `GSTIN` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PAN` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_days` int unsigned DEFAULT NULL,
  `credit_limit` int unsigned DEFAULT NULL,
  `bank_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `warehouse_rack_types`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse_rack_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `warehouse_rack_types_position_unique` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `warehouse_racks`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse_racks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_id` int unsigned DEFAULT NULL,
  `reference` varchar(50) DEFAULT NULL,
  `type_id` bigint unsigned DEFAULT NULL,
  `sub-type` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `Index 2` (`warehouse_id`),
  KEY `warehouse_racks_type_id_foreign` (`type_id`),
  CONSTRAINT `FK_warehouse_racks_warehouses` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `warehouse_racks_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `warehouse_rack_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3977 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `warehouse_stock_audit`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouse_stock_audit` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int unsigned NOT NULL,
  `warehouse_id` int unsigned NOT NULL,
  `batch_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `audit_quantity` decimal(11,3) NOT NULL,
  `updated_quantity` decimal(11,3) NOT NULL,
  `previous_quantity` decimal(11,3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `warehouse_stock_audit_product_id_foreign` (`product_id`),
  CONSTRAINT `warehouse_stock_audit_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2198 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `warehouses`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `warehouses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `legal_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pan` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gstin` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `city_id` int unsigned NOT NULL,
  `parent_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `warehouses_name_unique` (`name`),
  KEY `warehouses_city_id_foreign` (`city_id`),
  KEY `warehouses_parent_id_foreign` (`parent_id`),
  CONSTRAINT `warehouses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `warehouses_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `warehouses` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wh_group_abc`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wh_group_abc` (
  `group_id` int unsigned NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `sale` decimal(16,6) DEFAULT NULL,
  `qty_sold` decimal(16,6) DEFAULT NULL,
  `qty_per_day` decimal(16,6) DEFAULT NULL,
  `cumulative_qty` decimal(16,6) DEFAULT NULL,
  `total_qty_sold` decimal(16,6) DEFAULT NULL,
  `cum_qty_share` decimal(16,6) DEFAULT NULL,
  `class_abc` varchar(50) DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `period_days` int unsigned DEFAULT '0',
  `outer_quantity` int unsigned DEFAULT NULL,
  `case_quantity` int unsigned DEFAULT NULL,
  `unit_mrp` decimal(16,6) unsigned DEFAULT NULL,
  `reorder_level_days` decimal(5,2) unsigned DEFAULT NULL,
  `reorder_level_units` decimal(16,6) unsigned DEFAULT NULL,
  `reorder_quantity_days` decimal(5,2) unsigned DEFAULT NULL,
  `reorder_quantity_units` decimal(16,6) unsigned DEFAULT NULL,
  `reorder_sku_value` varchar(50) DEFAULT NULL,
  `reorder_sku_quantity` decimal(16,6) DEFAULT NULL,
  `stock` decimal(16,6) DEFAULT NULL,
  `demand_units_final` decimal(16,6) DEFAULT NULL,
  `demand_sku_quantity_final` decimal(16,6) DEFAULT NULL,
  `demand_generated` int DEFAULT NULL,
  `last_run` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'niyoweb'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai1kmisfeb`(IN date_from VARCHAR(50),IN date_to VARCHAR(50))
BEGIN

SELECT 

res_fin.store_id, res_fin.store_name, 



res_fin.marketer_id, res_fin.marketer_alias, res_fin.marketer_id_code_alias,

res_fin.product_id, res_fin.product_name, 








res_fin.order_item_units AS fin_units,
res_fin.order_item_mrp AS fin_mrp,
res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,
res_fin.order_item_margin_vc AS fin_margin_vc,
res_fin.order_item_margin_vc/res_fin.order_item_mrp AS fin_margin_vc_pc







FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0) AS s1k_count_flag,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*oi.mrp) AS order_item_mrp,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*(oi.total-res_oii.cost)) AS order_item_margin,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*(oi.mrp-res_oii.cost)) AS order_item_margin_vc,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0)
*(oi.quantity*pv.quantity)) AS order_item_units,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

s1k.start_date,
IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")
LEFT JOIN `1kstores`.stores s1k ON s1k.niyo_id = s.id 

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)

AND bt.name LIKE "%1K%"
AND IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30"))>=s1k.start_date,1,0) != 0
AND IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) != 0


GROUP BY p.id, s.id 
ORDER BY p.id, s.id 

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly`(IN date_from VARCHAR(50),IN date_to VARCHAR(50))
BEGIN

SELECT * ,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_quantity AS fin_sku_quantity,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_units AS fin_units,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_mrp AS fin_mrp,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_min_price AS fin_min_price,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_tax AS fin_tax,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_discount AS fin_discount,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_total AS fin_sale,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_cost AS fin_cost,
CAST(res_fin.sale_flag AS DECIMAL(6))*res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,

SUM(oi.quantity) AS order_item_quantity, SUM(oi.quantity*pv.quantity) AS order_item_units,
SUM(oi.mrp) AS order_item_mrp, SUM(oi.min_price) AS order_item_min_price, SUM(oi.tax) AS order_item_tax, SUM(oi.discount) AS order_item_discount,


SUM(oi.total) AS order_item_total, SUM(res_oii.cost) AS order_item_cost, SUM(oi.total-res_oii.cost) AS order_item_margin,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to

GROUP BY o.id, oi.sku, oi.status, sh.id, i.id, cn.id


) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly_agent_app`(IN date_from VARCHAR(50),IN date_to VARCHAR(50))
BEGIN

SELECT 
res_fin.agent_id, res_fin.agent_name,


res_fin.marketer_id, res_fin.marketer_code, res_fin.marketer_alias, res_fin.marketer_id_code_alias,









res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,

res_fin.count_beats,
res_fin.count_stores,

res_fin.count_products,
res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)
AND bt.name NOT LIKE "%1K%"
AND a.id IS NOT NULL


GROUP BY a.id, m.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly_beats`(IN date_from VARCHAR(50),IN date_to VARCHAR(50))
BEGIN

DECLARE beat_list VARCHAR(50);
IF 1k_include = "include" 
	THEN SET beat_list = "";
	ELSE SET beat_list = "%1K%";
END IF ;
	
SELECT 
res_fin.beat_id, res_fin.beat_name, res_fin.beat_id_name,











res_fin.order_item_mrp AS fin_mrp,
res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,
res_fin.order_item_margin_vc AS fin_margin_vc,
res_fin.order_item_margin_vc/res_fin.order_item_mrp AS fin_margin_vc_pc,


res_fin.count_stores,
res_fin.count_marketers,
res_fin.count_products,
res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.mrp) AS order_item_mrp,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.mrp-res_oii.cost)) AS order_item_margin_vc,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)

AND bt.name NOT LIKE beat_list



GROUP BY bt.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly_marketers`(IN date_from VARCHAR(50),IN date_to VARCHAR(50), IN 1k_include VARCHAR(50))
BEGIN

DECLARE beat_list VARCHAR(50);
IF 1k_include = "include" 
	THEN SET beat_list = "";
	ELSE SET beat_list = "%1K%";
END IF ;

	
SELECT 


res_fin.marketer_id, res_fin.marketer_code, res_fin.marketer_alias, res_fin.marketer_id_code_alias,









res_fin.order_item_mrp AS fin_mrp,
res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,
res_fin.order_item_margin_vc AS fin_margin_vc,
res_fin.order_item_margin_vc/res_fin.order_item_mrp AS fin_margin_vc_pc,

res_fin.count_beats,
res_fin.count_stores,

res_fin.count_products,
res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.mrp) AS order_item_mrp,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.mrp-res_oii.cost)) AS order_item_margin_vc,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)

AND bt.name NOT LIKE beat_list



GROUP BY m.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly_products`(IN date_from VARCHAR(50),IN date_to VARCHAR(50), IN 1k_include VARCHAR(50))
BEGIN

DECLARE beat_list VARCHAR(50);
IF 1k_include = "include" 
	THEN SET beat_list = "";
	ELSE SET beat_list = "%1K%";
END IF ;	
	
SELECT 


res_fin.marketer_id, res_fin.marketer_alias, res_fin.marketer_id_code_alias,
res_fin.brand_id, res_fin.brand_name, res_fin.brand_id_name,
res_fin.product_id, res_fin.product_name, res_fin.product_id_name, res_fin.marketer_id_code_alias_product_id_name,







res_fin.order_item_units AS fin_units,
res_fin.order_item_mrp AS fin_mrp,
res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,
res_fin.order_item_margin_vc AS fin_margin_vc,
res_fin.order_item_margin_vc/res_fin.order_item_mrp AS fin_margin_vc_pc,

res_fin.count_beats,
res_fin.count_stores,


res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.mrp) AS order_item_mrp,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.mrp-res_oii.cost)) AS order_item_margin_vc,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.quantity*pv.quantity)) AS order_item_units,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)

AND bt.name NOT LIKE beat_list



GROUP BY p.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_mis_monthly_stores`(IN date_from VARCHAR(50),IN date_to VARCHAR(50), IN 1k_include VARCHAR(50))
BEGIN

DECLARE beat_list VARCHAR(50);
IF 1k_include = "include" 
	THEN SET beat_list = "";
	ELSE SET beat_list = "%1K%";
END IF ;
	
SELECT 
res_fin.beat_id, res_fin.beat_name, res_fin.beat_id_name,
res_fin.store_id, res_fin.store_name, res_fin.store_id_name, res_fin.store_indicator, res_fin.beat_id_name_store_id_name,










res_fin.order_item_mrp AS fin_mrp,
res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,
res_fin.order_item_margin_vc AS fin_margin_vc,
res_fin.order_item_margin_vc/res_fin.order_item_mrp AS fin_margin_vc_pc,



res_fin.count_marketers,
res_fin.count_products,
res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.mrp) AS order_item_mrp,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.mrp-res_oii.cost)) AS order_item_margin_vc,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE (DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to)

AND bt.name NOT LIKE beat_list



GROUP BY s.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ai_store_sale_margin`()
BEGIN

SELECT s.id AS store_id, s.name AS store_name, b.id AS beat_id, b.name AS beat_name,
SUM(oi.total) AS sale, SUM(res_oii.cost) AS cogs, SUM(oi.total)-SUM(res_oii.cost) AS margin,
1-SUM(res_oii.cost)/SUM(oi.total) AS margin_pc,

SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,oi.total,0)) AS sale_14d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,res_oii.cost,0)) AS cogs_14d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,oi.total,0))-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,res_oii.cost,0)) AS margin_14d,
1-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,res_oii.cost,0))/SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=14,oi.total,0)) AS margin_pc_14d,

SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,oi.total,0)) AS sale_30d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,res_oii.cost,0)) AS cogs_30d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,oi.total,0))-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,res_oii.cost,0)) AS margin_28d,
1-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,res_oii.cost,0))/SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=28,oi.total,0)) AS margin_pc_28d,

SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,oi.total,0)) AS sale_49d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,res_oii.cost,0)) AS cogs_49d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,oi.total,0))-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,res_oii.cost,0)) AS margin_49d,
1-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,res_oii.cost,0))/SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=49,oi.total,0)) AS margin_pc_49d,

SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,oi.total,0)) AS sale_63d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,res_oii.cost,0)) AS cogs_63d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,oi.total,0))-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,res_oii.cost,0)) AS margin_63d,
1-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,res_oii.cost,0))/SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=63,oi.total,0)) AS margin_pc_63d,

SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,oi.total,0)) AS sale_91d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,res_oii.cost,0)) AS cogs_91d,
SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,oi.total,0))-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,res_oii.cost,0)) AS margin_91d,
1-SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,res_oii.cost,0))/SUM(IF(DATEDIFF(CONVERT_TZ(CURDATE(),"+00:00","+05:30"),CONVERT_TZ(oi.created_at,"+00:00","+05:30"))<=91,oi.total,0)) AS margin_pc_91d

FROM niyoweb.order_items oi
JOIN niyoweb.orders o ON o.id=oi.order_id
JOIN niyoweb.stores s ON s.id=o.store_id
JOIN niyoweb.beats b ON b.id=s.beat_id
LEFT JOIN 
	(SELECT oii.order_item_id, SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
	FROM niyoweb.order_item_inventories oii GROUP BY oii.order_item_id) AS res_oii ON res_oii.order_item_id=oi.id
WHERE oi.status NOT IN ("cancelled","processing","returned","rto")
AND res_oii.cost IS NOT NULL
GROUP BY s.id
ORDER BY s.id

;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `auto_price_update`(IN passchar VARCHAR(255))
BEGIN

if passchar = "view_now"
then 
  
select nt_id, pos_id, name, res.category2_name, res.marketer_level, old_mrp, use_mrp, p.price, use_sp from
pos.product_store ps
left join pos.products p on p.id = ps.product_id
left join
(select np.id as nt_id, p.id as pos_id,
p.name as product_name, p.mrp as unit_mrp, br.name as brand_name, m.name as marketer_name, c.name as category_name,
ml.name as marketer_level, cl2.name as category2_name, cl1.name as category1_name, cl2cm.cost_basis,
pi2.latest_unit_cost, 1-pi2.latest_unit_cost/p.mrp as vcm, mps.discount_pc,
ceiling(if(ml.id = 1 and p.mrp < (if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc)))), p.mrp,if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc))))*100)/100  as use_sp,
ceiling(if(ml.id = 1, p.mrp, if(pi2.latest_unit_cost is null,p.mrp,if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_mrp),p.mrp)))) as use_mrp,
min(ps.price) as current_min_sp, max(ps.price) as current_max_sp, avg(ps.price) as current_avg_sp
from niyoweb.products np
left join niyoweb.brands br on br.id = np.brand_id
left join niyoweb.marketers m on m.id = br.marketer_id
left join niyoweb.collection_product cp on cp.product_id = np.id
left join niyoweb.collections c on c.id = cp.collection_id
left join pos.product_vendor pvd on pvd.vendor_product_identifier = np.id
left join pos.products p on p.id = pvd.product_id
left join niyoweb.product_category_l2_map pcl2m on pcl2m.product_id = np.id
left join niyoweb.category_l2 cl2 on cl2.id = pcl2m.category_l2_id
left join niyoweb.category_l1 cl1 on cl1.id = cl2.category_l1_id
left join niyoweb.marketer_levels ml on ml.id = m.level_id
left join niyoweb.Prod_Inventory pi2 on pi2.product_id = np.id
left join niyoweb.Prod_Master pm on pm.product_id = np.id
left join niyoweb.category_l2_cost_map cl2cm on cl2cm.category_l2_id = cl2.id and cl2cm.marketer_level_id = ml.id
left join niyoweb.mrp_pricing_slabs mps on mps.cl2cm_cost_basis = cl2cm.cost_basis and (1-pi2.latest_unit_cost/p.mrp) >= mps.vcm_from and (1-pi2.latest_unit_cost/p.mrp) < mps.vcm_to
left join pos.product_store ps on ps.product_id = p.id
left join pos.stores s on ps.store_id = s.id
where s.status = 1 and s.name not like "%zabardast%" and s.name not like "%test%" and s.name not like "%inventory%"
group by np.id
order by np.id asc ) as res on res.pos_id = p.id
where (datediff(date(convert_tz(now(),"+00:00","+05:30")),date(convert_tz(ps.auto_pricing_at,"+00:00","+05:30"))) >= 0
or ps.auto_pricing_at is null or ps.auto_pricing_at = "")
and res.pos_id is not null;

elseif passchar = "update_now"
then 
update
pos.product_store ps
left join pos.products p on p.id = ps.product_id
left join
(select np.id as nt_id, p.id as pos_id,
p.name as product_name, p.mrp as unit_mrp, br.name as brand_name, m.name as marketer_name, c.name as category_name,
ml.name as marketer_level, cl2.name as category2_name, cl1.name as category1_name, cl2cm.cost_basis,
pi2.latest_unit_cost, 1-pi2.latest_unit_cost/p.mrp as vcm, mps.discount_pc,
ceiling(if(ml.id = 1 and p.mrp < (if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc)))), p.mrp,if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc))))*100)/100  as use_sp,
ceiling(if(ml.id = 1, p.mrp, if(pi2.latest_unit_cost is null,p.mrp,if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_mrp),p.mrp)))) as use_mrp,
min(ps.price) as current_min_sp, max(ps.price) as current_max_sp, avg(ps.price) as current_avg_sp
from niyoweb.products np
left join niyoweb.brands br on br.id = np.brand_id
left join niyoweb.marketers m on m.id = br.marketer_id
left join niyoweb.collection_product cp on cp.product_id = np.id
left join niyoweb.collections c on c.id = cp.collection_id
left join pos.product_vendor pvd on pvd.vendor_product_identifier = np.id
left join pos.products p on p.id = pvd.product_id
left join niyoweb.product_category_l2_map pcl2m on pcl2m.product_id = np.id
left join niyoweb.category_l2 cl2 on cl2.id = pcl2m.category_l2_id
left join niyoweb.category_l1 cl1 on cl1.id = cl2.category_l1_id
left join niyoweb.marketer_levels ml on ml.id = m.level_id
left join niyoweb.Prod_Inventory pi2 on pi2.product_id = np.id
left join niyoweb.Prod_Master pm on pm.product_id = np.id
left join niyoweb.category_l2_cost_map cl2cm on cl2cm.category_l2_id = cl2.id and cl2cm.marketer_level_id = ml.id
left join niyoweb.mrp_pricing_slabs mps on mps.cl2cm_cost_basis = cl2cm.cost_basis and (1-pi2.latest_unit_cost/p.mrp) >= mps.vcm_from and (1-pi2.latest_unit_cost/p.mrp) < mps.vcm_to
left join pos.product_store ps on ps.product_id = p.id
left join pos.stores s on ps.store_id = s.id
where s.status = 1 and s.name not like "%zabardast%" and s.name not like "%test%" and s.name not like "%inventory%"
group by np.id
order by np.id asc ) as res on res.pos_id = p.id
set p.old_price = p.price,
p.old_mrp = p.mrp,
ps.old_price = ps.price,
p.mrp = if(res.use_mrp is null, if(p.mrp is null,p.old_mrp,p.mrp), res.use_mrp),
p.price = if(res.use_sp is null, if(p.price is null,p.old_price,p.price), res.use_sp),
ps.price = p.price,
ps.auto_pricing_at = now()
where (datediff(date(convert_tz(now(),"+00:00","+05:30")),date(convert_tz(ps.auto_pricing_at,"+00:00","+05:30"))) >= 0
or ps.auto_pricing_at is null or ps.auto_pricing_at = "")
and res.pos_id is not null;

else
select nt_id, pos_id, name, res.category2_name, res.marketer_level, old_mrp, use_mrp, p.price, use_sp from
pos.product_store ps
left join pos.products p on p.id = ps.product_id
left join
(select np.id as nt_id, p.id as pos_id,
p.name as product_name, p.mrp as unit_mrp, br.name as brand_name, m.name as marketer_name, c.name as category_name,
ml.name as marketer_level, cl2.name as category2_name, cl1.name as category1_name, cl2cm.cost_basis,
pi2.latest_unit_cost, 1-pi2.latest_unit_cost/p.mrp as vcm, mps.discount_pc,
ceiling(if(ml.id = 1 and p.mrp < (if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc)))), p.mrp,if(pi2.latest_unit_cost is null,min(ps.price),if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_sp),p.mrp*(1-mps.discount_pc))))*100)/100  as use_sp,
ceiling(if(ml.id = 1, p.mrp, if(pi2.latest_unit_cost is null,p.mrp,if(cl2cm.cost_basis="cost",pi2.latest_unit_cost*(1+cl2cm.cost_mrp),p.mrp)))) as use_mrp,
min(ps.price) as current_min_sp, max(ps.price) as current_max_sp, avg(ps.price) as current_avg_sp
from niyoweb.products np
left join niyoweb.brands br on br.id = np.brand_id
left join niyoweb.marketers m on m.id = br.marketer_id
left join niyoweb.collection_product cp on cp.product_id = np.id
left join niyoweb.collections c on c.id = cp.collection_id
left join pos.product_vendor pvd on pvd.vendor_product_identifier = np.id
left join pos.products p on p.id = pvd.product_id
left join niyoweb.product_category_l2_map pcl2m on pcl2m.product_id = np.id
left join niyoweb.category_l2 cl2 on cl2.id = pcl2m.category_l2_id
left join niyoweb.category_l1 cl1 on cl1.id = cl2.category_l1_id
left join niyoweb.marketer_levels ml on ml.id = m.level_id
left join niyoweb.Prod_Inventory pi2 on pi2.product_id = np.id
left join niyoweb.Prod_Master pm on pm.product_id = np.id
left join niyoweb.category_l2_cost_map cl2cm on cl2cm.category_l2_id = cl2.id and cl2cm.marketer_level_id = ml.id
left join niyoweb.mrp_pricing_slabs mps on mps.cl2cm_cost_basis = cl2cm.cost_basis and (1-pi2.latest_unit_cost/p.mrp) >= mps.vcm_from and (1-pi2.latest_unit_cost/p.mrp) < mps.vcm_to
left join pos.product_store ps on ps.product_id = p.id
left join pos.stores s on ps.store_id = s.id
where s.status = 1 and s.name not like "%zabardast%" and s.name not like "%test%" and s.name not like "%inventory%"
group by np.id
order by np.id asc ) as res on res.pos_id = p.id
where (datediff(date(convert_tz(now(),"+00:00","+05:30")),date(convert_tz(ps.auto_pricing_at,"+00:00","+05:30"))) >= 0
or ps.auto_pricing_at is null or ps.auto_pricing_at = "")
and res.pos_id is not null;

end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `beat_wise_dispatch_data`()
BEGIN
CREATE TEMPORARY TABLE beat_wise_dispatch_data(
beat_id int,
beat_name varchar(100),
box_count_d int,inv_count_d int,inv_amt_d int,box_count_m int,inv_count_m int,inv_amt_m int);
call beat_wise_dispatch_data_today();
call beat_wise_dispatch_data_month();

select beat_name as Beat,box_count_d as Today_Box_count,inv_count_d as Today_Invoice_count,
inv_amt_d as Today_invoice_Amount, box_count_m as Month_Box_count, inv_count_m as Month_Invoice_Count,
inv_amt_m as Month_Invoice_Amount, box_count_m/day(NOW()) as Avg_Box_count,
inv_count_m/day(NOW()) as Avg_Invoice_count, inv_amt_m/day(NOW()) as Avg_Invoice_Amount
from beat_wise_dispatch_data;

DROP TEMPORARY TABLE beat_wise_dispatch_data;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `beat_wise_dispatch_data_month`()
BEGIN
DECLARE done1 int;
DECLARE b_id1 int;
DECLARE b_name1 varchar(100);
DECLARE box_count1 int;
DECLARE inv_count1 int;
DECLARE inv_amount1 FLOAT;

DECLARE MONTH_CUR CURSOR for
select b.id as b_id1,b.name as Beat_name,sum(sh.boxes)as Box_counts,count(i.id)as Invoice_counts,sum(i.amount)as Invoice_amount
from manifests mf,manifest_shipment mfs,shipments sh,orders o,invoices i,stores s,beats b
where mf.id=mfs.manifest_id
and mfs.shipment_id=sh.id
and sh.order_id=o.id
and o.id=i.order_id
and o.store_id=s.id
and s.beat_id=b.id
and month(convert_tz(mf.created_at,"+00:00","+05:30"))=month(curdate())
group by b.name,b.id
order by b.name;
declare continue handler for not found set done1=1;
set done1 = 0;
open MONTH_CUR;
igmLoop: loop
fetch MONTH_CUR into b_id1, b_name1, box_count1, inv_count1, inv_amount1;
if done1 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT beat_id FROM  beat_wise_dispatch_data WHERE beat_id = b_id1)
    THEN
update beat_wise_dispatch_data set box_count_m=box_count1, inv_count_m=inv_count1, inv_amt_m=inv_amount1
        where beat_id=b_id1;
ELSE
INSERT INTO beat_wise_dispatch_data(beat_id, beat_name, box_count_m, inv_count_m, inv_amt_m)
              VALUES(b_id1, b_name1, box_count1, inv_count1, inv_amount1);
END IF;
    end loop igmLoop;
    close MONTH_CUR;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `beat_wise_dispatch_data_today`()
BEGIN
DECLARE done int;
DECLARE b_id int;
DECLARE b_name varchar(100);
DECLARE box_count int;
DECLARE inv_count int;
DECLARE inv_amount FLOAT;

DECLARE DAILY_CUR CURSOR for
select b.id as b_id, b.name as Beat_name,sum(sh.boxes)as Box_counts,count(i.id)as Invoice_counts,sum(i.amount)as Invoice_amount
from manifests mf,manifest_shipment mfs,shipments sh,orders o,invoices i,stores s,beats b
where mf.id=mfs.manifest_id
and mfs.shipment_id=sh.id
and sh.order_id=o.id
and o.id=i.order_id
and o.store_id=s.id
and s.beat_id=b.id
and date(convert_tz(mf.created_at,"+00:00","+05:30"))=date(CURDATE())
group by b.name,b.id
order by b.name;
declare continue handler for not found set done=1;
set done = 0;
open DAILY_CUR;
igmLoop: loop
fetch DAILY_CUR into b_id, b_name, box_count, inv_count, inv_amount;
if done = 1 then leave igmLoop; end if;
IF EXISTS(SELECT beat_id FROM  beat_wise_dispatch_data WHERE beat_id = b_id)
    THEN
update beat_wise_dispatch_data set box_count_d=box_count, inv_count_d=inv_count, inv_amt_d=inv_amount
        where beat_id=b_id;
ELSE
INSERT INTO beat_wise_dispatch_data(beat_id, beat_name, box_count_d, inv_count_d, inv_amt_d)
              VALUES(b_id, b_name, box_count, inv_count, inv_amount);
END IF;
    end loop igmLoop;
    close DAILY_CUR;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `commission_tag_product_update`()
BEGIN
  

insert pos.commission_tag_product
select p.id as product_id, nctp.commission_tag_id, now() as created_at, now() as updated_at
  from pos.products p
  left join pos.product_vendor pvd on pvd.product_id = p.id
  left join pos.commission_tag_product ctp on ctp.product_id = p.id
  left join niyoweb.products np on np.id = pvd.vendor_product_identifier
  left join niyoweb.commission_tag_product nctp on nctp.product_id = np.id
  where ctp.product_id is null and nctp.product_id is not null;


  update pos.products p
  left join pos.product_vendor pvd on pvd.product_id = p.id
  left join pos.commission_tag_product ctp on ctp.product_id = p.id
  left join niyoweb.products np on np.id = pvd.vendor_product_identifier
  left join niyoweb.commission_tag_product nctp on nctp.product_id = np.id
  set ctp.commission_tag_id = nctp.commission_tag_id
  where ctp.product_id is not null and ctp.commission_tag_id != nctp.commission_tag_id
  and nctp.product_id is not null;


  select np.id as product_id, np.name
  from niyoweb.products np 
  where np.id not in 
    (select ctp.product_id from niyoweb.commission_tag_product ctp);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `dec_plan`(in in_store_id nvarchar(10000))
BEGIN
select res.Revenue
from
(select s.id,sum(oi.total)as Revenue
from orders o,order_items oi,invoices i, shipments sh, shipment_items shi, stores s

where o.id=oi.order_id 
and o.store_id=s.id 
and o.id=i.order_id 
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id







and i.status != "cancelled" 
and oi.status!="cancelled" 
and oi.status!="processing" 
and oi.status!="returned" 
and FIND_IN_SET(s.id,in_store_id)
and month(convert_tz(i.created_at,"+00:00","+05:30"))=12
group by s.id) res
right join stores 
on stores.id = res.id
where find_in_set(stores.id,in_store_id)
order by find_in_set(stores.id,in_store_id);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `GST_Negative_Sale`(IN saletype varchar(255))
BEGIN
	if saletype = "rto"
	then 
		select 
			date(convert_tz(i.created_at, '+00:00', '+05:30')) as Invoice_Date,
			i.reference_id as Invoice_Number,
			date(i.cancelled_on) as RTO_Date, '' as Credit_Note_Number,
			s.name as Party_Name, s.gstin as GSTIN, 
			p.hsn_sac_code as HSN, m.code as Company, p.name as Item_Name, oi.sku as SKU,
			pv.quantity as quantity_in_units, sum(oi.quantity) as Quantity,
			oi.price as Sale_Price, 
			IFNULL((select sum(t.percentage) from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name ='IGST'),0) as GST_Slab,
			IFNULL((select t.percentage from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name = 'CESS' limit 1),0) as CESS_Slab,
			sum(oi.subtotal) as Sale_Amount, sum(oi.discount) as Discount,
			IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%GST%'), 0) as GST_Amount,
			IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%CESS%'),0) as CESS_Amount,
			sum(oi.tax) as Total_Tax_Amount,
			sum(oi.total) as Total_Amount
			from order_items oi, orders o, shipments sh, shipment_items shi, invoices i, stores s,
			products p, product_variants pv, brands b, marketers m 
			where oi.order_id=o.id and sh.order_id = o.id and shi.shipment_id=sh.id and shi.order_item_id=oi.id 
			and o.store_id = s.id and oi.product_id = p.id and oi.sku = pv.sku 
			and p.brand_id = b.id and b.marketer_id = m.id
			and o.status != 'cancelled' and i.shipment_id=sh.id and oi.status != 'cancelled'
			and i.order_id=o.id 
			and convert_tz(i.created_at,'+00:00', '+05:30') < '2020-08-01 00:00:00' 
			and (oi.status ='rto' and i.status ='cancelled' 
			and convert_tz(i.cancelled_on ,'+00:00', '+05:30') >= '2020-08-01 00:00:00'
			and convert_tz(i.cancelled_on ,'+00:00', '+05:30') < '2020-09-01 00:00:00'
			)
			group by i.id, oi.sku;
	else
		select 
			date(convert_tz(i.created_at, '+00:00', '+05:30')) as Invoice_Date,
			i.reference_id as Invoice_Number,
			date(convert_tz(cn.created_at, '+00:00', '+05:30')) as Return_Date, 
			cn.reference_id as Credit_Note_Number,
			s.name as Party_Name, s.gstin as GSTIN, 
			p.hsn_sac_code as HSN, m.code as Company, p.name as Item_Name, oi.sku as SKU,
			pv.quantity as quantity_in_units, sum(oi.quantity) as Quantity,
			oi.price as Sale_Price, 
			IFNULL((select sum(t.percentage) from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name ='IGST'),0) as GST_Slab,
			IFNULL((select t.percentage from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name = 'CESS' limit 1),0) as CESS_Slab,
			sum(oi.subtotal) as Sale_Amount, sum(oi.discount) as Discount,
			IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%GST%'), 0) as GST_Amount,
			IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%CESS%'),0) as CESS_Amount,
			sum(oi.tax) as Total_Tax_Amount,
			sum(oi.total) as Total_Amount
			from order_items oi, orders o, shipments sh, shipment_items shi, invoices i, stores s,
			products p, product_variants pv, brands b, marketers m, return_orders ro , return_order_items roi, credit_notes cn 
			where oi.order_id=o.id and sh.order_id = o.id and shi.shipment_id=sh.id and shi.order_item_id=oi.id 
			and o.store_id = s.id and oi.product_id = p.id and oi.sku = pv.sku 
			and p.brand_id = b.id and b.marketer_id = m.id
			and o.status != 'cancelled' and i.shipment_id=sh.id and oi.status != 'cancelled'
			and i.order_id=o.id 
			and ro.order_id = o.id and ro.shipment_id = sh.id and roi.return_order_id = ro.id and roi.order_item_id = oi.id 
			and ro.status = 'completed' and oi.status = 'returned' 
			and cn.order_id = o.id and cn.return_order_id = ro.id 
			and convert_tz(i.created_at,'+00:00', '+05:30') < '2020-08-01 00:00:00' 
			and convert_tz(cn.created_at,'+00:00', '+05:30') between '2020-08-01 00:00:00' and '2020-09-01 00:00:00'
			group by i.id, oi.sku
			order by cn.id;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `GST_Sale_Data`()
BEGIN
	select 
date(convert_tz(i.created_at, '+00:00', '+05:30')) as Invoice_Date,
i.reference_id as Invoice_Number,
s.name as Party_Name, s.gstin as GSTIN, 
p.hsn_sac_code as HSN, m.code as Company, p.name as Item_Name, oi.sku as SKU,
pv.quantity as quantity_in_units, sum(oi.quantity) as Quantity,
oi.price as Sale_Price, 
IFNULL((select sum(t.percentage) from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name ='IGST'),0) as GST_Slab,
IFNULL((select t.percentage from tax_classes tc, taxes t where tc.id = t.tax_class_id and tc.id=p.tax_class_id and t.name = 'CESS' limit 1),0) as CESS_Slab,
sum(oi.subtotal) as Sale_Amount, sum(oi.discount) as Discount,
IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%GST%'), 0) as GST_Amount,
IFNULL((select sum(amount) from order_item_taxes oit where oit.order_item_id=oi.id and name like '%CESS%'),0) as CESS_Amount,
sum(oi.tax) as Total_Tax_Amount,
sum(oi.total) as Total_Amount
from order_items oi, orders o, shipments sh, shipment_items shi, invoices i, stores s,
products p, product_variants pv, brands b, marketers m 
where oi.order_id=o.id and sh.order_id = o.id and shi.shipment_id=sh.id and shi.order_item_id=oi.id 
and o.store_id = s.id and oi.product_id = p.id and oi.sku = pv.sku 
and p.brand_id = b.id and b.marketer_id = m.id
and o.status != 'cancelled' and i.shipment_id=sh.id and oi.status != 'cancelled'
and sh.status!='cancelled'
and i.order_id=o.id 
and convert_tz(i.created_at,'+00:00', '+05:30') >= '2020-08-01 00:00:00' 
and convert_tz(i.created_at,'+00:00', '+05:30') < '2020-09-01 00:00:00'
and (
		(oi.status = 'delivered') 
		or
		(oi.status ='rto' and i.status ='cancelled' and convert_tz(i.cancelled_on ,'+00:00', '+05:30') >= '2020-09-01 00:00:00')
		or (oi.status = 'returned' and oi.id in
				(select roi.order_item_id from return_order_items roi, return_orders ro 
					where roi.return_order_id = ro.id
					and ro.status = 'completed'
					and convert_tz(ro.created_at,'+00:00', '+05:30') >= '2020-09-01 00:00:00'
				)
			)
	)
group by i.id, oi.sku;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `os_ageing`(in date_os date)
BEGIN

declare store, max_store int;
if date_os is null or date_os = ""
then
set date_os = "2020-12-31";
end if;
set store = 1;
set max_store = (select max(id) from niyoweb.stores);
drop table if exists niyoweb.store_wise_os;
create table niyoweb.store_wise_os
(store_id int(10), store_name char(255), os_date date, os_amt decimal(16,6), os_age decimal(10,4));

while store <= max_store
do
insert niyoweb.store_wise_os
select res2.store_id, res2.store_name, date_os as os_date, ifnull(sum(res2.unset_amt),0) as os_amt,
ifnull(sum(res2.unset_amt*res2.os_age)/sum(res2.unset_amt),0) as os_age
from
(select res1.*, if(@cum_amt > res1.os_amount, 0, if(res1.os_amount - @cum_amt > res1.amount, res1.amount, res1.os_amount - @cum_amt)) as unset_amt,
@cum_amt := @cum_amt + res1.amount
from 
(select t.id, t.store_id, s.name as store_name, t.amount, datediff(date_os,date(convert_tz(t.transaction_date,"+00:00","+05:30"))) as os_age,
res_os.os_amount
from niyoweb.transactions t
left join niyoweb.invoices i on i.id = t.source_id 
left join niyoweb.shipments sh on sh.id = i.shipment_id 
left join niyoweb.stores s on s.id = t.store_id 
left join 
(select t.store_id, sum(if(t.type="credit",-t.amount,+t.amount)) as os_amount
from niyoweb.transactions t 
where date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= date_os
and t.deleted_at is null 
and t.status in ("completed")
group by t.store_id 
order by t.store_id) as res_os on res_os.store_id = t.store_id 
where date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= date_os
and (t.deleted_at is null or date(convert_tz(t.deleted_at,"+00:00","+05:30")) > date_os)
and t.status not in ("pending")
and t.type in ("debit")
and t.source_id is not null 
and sh.status not in ("cancelled")
and t.store_id = store 
order by t.transaction_date desc) as res1
cross join (select @cum_amt := 0) as dum1) as res2
group by res2.store_id;
set store = store + 1;
end while

	
	
;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `pos_abc_rop`()
BEGIN
	
select * from 
(select res.*, (@abc_class := if(@frac < 0.7, "A", if(@frac < 0.9, "B","C"))) as abc_class,
(@shelf_qty := ceiling(res.qty_sold*21)) as shelf_qty, (@rop := ceiling(res.qty_sold*14)) as rop,
if(res.store_inv <= @rop, ceiling(@shelf_qty - greatest(res.store_inv,0)), 0) as ord_qty,
round(@cum_qty := if(@flag = res.store_id,@cum_qty,0) + res.qty_sold,6) as cumulative_qty,
(@frac := round(@cum_qty/res.tot_qty_sold,6)) as frac,
(@flag := res.store_id) as flag
from 
	(select o.store_id, res_tot.tot_qty_sold, gp.group_id, pg.name,
	cl0.id as cat_id, cl0.name as cat_name, cl1.id as cl1_id, cl1.name as cl1_name, cl2.id as cl2_id, cl2.name as cl2_name,
	m.id as mktr_id, m.name as mktr_name, br.id as brand_id, br.name as brand_name,	
	round(sum(oi.quantity)/28,6) as qty_sold, res_inv.store_inv
	from pos.order_items oi
	left join pos.orders o on o.id = oi.order_id 
	left join pos.product_vendor pvd on pvd.product_id = oi.product_id 
	left join niyoweb.products np on np.id = 1*pvd.vendor_product_identifier 
	left join niyoweb.groups_products gp on gp.product_id = np.id 
	left join niyoweb.product_groups pg on pg.id = gp.group_id 
	left join niyoweb.brands br on br.id = np.brand_id 
	left join niyoweb.marketers m on m.id = br.marketer_id 
	left join niyoweb.product_category_l2_map pcl2m on pcl2m.product_id = np.id 
	left join niyoweb.category_l2 cl2 on cl2.id = pcl2m.category_l2_id 
	left join niyoweb.category_l1 cl1 on cl1.id = cl2.category_l1_id 
	left join niyoweb.collections cl0 on cl0.id = cl1.collection_id 
	left join pos.stores s on s.id = o.store_id 
	left join 
		(select o.store_id, round(sum(oi.quantity)/28,6) as tot_qty_sold
		from pos.order_items oi
		left join pos.orders o on o.id = oi.order_id 
		left join pos.stores s on s.id = o.store_id 
		where oi.status not in ("cancelled")
		and s.name not like "%zabardast%" and s.name not like "%inventory%"
		and datediff(date(convert_tz(now(),"+00:00","+05:30")),date(convert_tz(oi.created_at,"+00:00","+05:30"))) between 1 and 28
		group by o.store_id 
		order by o.store_id asc) as res_tot on res_tot.store_id = o.store_id 
	left join 
		(select i.store_id, gp.group_id, sum(i.quantity) as store_inv 
		from pos.inventories i 
		left join pos.product_vendor pvd on pvd.product_id = i.product_id 
		left join niyoweb.groups_products gp on gp.product_id = 1*pvd.vendor_product_identifier 
		where i.status = 1 
		group by i.store_id, gp.group_id
		order by i.store_id asc, gp.group_id asc) as res_inv on res_inv.store_id = o.store_id and gp.group_id = res_inv.group_id
	where oi.status not in ("cancelled")
	and s.name not like "%zabardast%" and s.name not like "%inventory%"
	and datediff(date(convert_tz(now(),"+00:00","+05:30")),date(convert_tz(oi.created_at,"+00:00","+05:30"))) between 1 and 28
	group by o.store_id, gp.group_id 
	order by o.store_id asc, qty_sold desc) as res
cross join (select @cum_qty := 0) as dummy1
cross join (select @flag := 0) as dummy2
cross join (select @frac := 0) as dummy3
cross join (select @abc_class := 0) as dummy4
cross join (select @shelf_qty := 0) as dummy5
cross join (select @rop := 0) as dummy6
order by res.store_id asc, res.qty_sold desc) as res_fin
order by res_fin.store_id asc, res_fin.cat_id asc, res_fin.cl1_id asc, res_fin.cl2_id asc, res_fin.mktr_id asc, res_fin.brand_id asc, res_fin.group_id asc
;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `product_monthly_sale_margin`()
BEGIN

DECLARE mon_var VARCHAR(7);
DECLARE mi, yi INT;
DECLARE ymi DECIMAL(4,2);

SET mi=12;
SET yi=2019;
SET ymi=yi+0.01*mi;
SET mon_var=CONCAT(yi,"_",LPAD(mi,2,0));


SELECT p.id AS product_id, p.name AS product_name, m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,





SUM(IF(CONCAT(YEAR(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),"_",LPAD(MONTH(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),2,0))="2020_01",oi.quantity*pv.quantity,0)) AS units_2020_01,
SUM(IF(CONCAT(YEAR(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),"_",LPAD(MONTH(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),2,0))="2020_01",oi.total,0)) AS sale_2020_01,
SUM(IF(CONCAT(YEAR(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),"_",LPAD(MONTH(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),2,0))="2020_01",res_oii.cost,0)) AS cogs_2020_01,
SUM(IF(CONCAT(YEAR(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),"_",LPAD(MONTH(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),2,0))="2020_01",oi.total,0))-SUM(IF(CONCAT(YEAR(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),"_",LPAD(MONTH(CONVERT_TZ(oi.created_at,"+00:00","+05:30")),2,0))="2020_01",res_oii.cost,0)) AS margin_2020_01



FROM niyoweb.order_items oi
JOIN niyoweb.orders o ON o.id=oi.order_id
JOIN niyoweb.product_variants pv ON pv.sku=oi.sku
JOIN niyoweb.products p ON p.id=pv.product_id
JOIN niyoweb.brands b ON b.id=p.brand_id
JOIN niyoweb.marketers m ON m.id=b.marketer_id
LEFT JOIN 
	(SELECT oii.order_item_id, SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
	FROM niyoweb.order_item_inventories oii GROUP BY oii.order_item_id) AS res_oii ON res_oii.order_item_id=oi.id
WHERE oi.status NOT IN ("cancelled","processing","returned","rto")

GROUP BY p.id
ORDER BY p.id



;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ptr_both`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS ptr_b;
create TEMPORARY table ptr_b(Marketer_Name varchar(100),Marketer_Code varchar(100),Brand varchar(100),Product_Id int,
Product_Name varchar(200),SKU varchar(100), MRP float,Selling_Price float,Cost_Price float,Margin float,
Margin_Class varchar(100),Aeging_Class varchar(100),Status varchar(100),Week1_Sale float,Week2_Sale float,
Week3_Sale float,Week4_Sale float,Week5_Sale float);
call ptr_product();
call ptr_sale();
select * from ptr_b order by Product_Id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ptr_product`()
BEGIN
DECLARE done int;
DECLARE m_n varchar(100);
DECLARE m_c varchar(100);
DECLARE br varchar(100);
DECLARE p_id int;
DECLARE p_n varchar(200);
DECLARE sk varchar(100);
DECLARE mr float;
declare sp float;
declare cp float;
DECLARE marg float;
declare marg_c varchar(100);
declare aeg_c varchar(100);
declare s varchar(100);

DECLARE CUR CURSOR for

select res4.MarketerName,res4.MarketerCode,res4.Brand,res4.Prdoduct_id,res4.ProductName,res4.sku,res4.mrp,res4.Selling_Price,
res4.cost,res4.Margin,res4.Margin_Class,res2.Aeging_Class,res2.status
from
(select product_id,sum(total_cost)as Sum_Total_Cost,sum(wtd_amt)as Sum_Wtd_Amt,(sum(wtd_amt)/sum(total_cost))as inv_day,
if((sum(wtd_amt)/sum(total_cost))<5,"SuperFast",(if((sum(wtd_amt)/sum(total_cost))<10,"Fast",
(if((sum(wtd_amt)/sum(total_cost))<15,"Medium","Slow")))))as Aeging_Class,status
from
(select i.product_id,i.quantity,i.batch_number,i.unit_cost_price,
(i.quantity*i.unit_cost_price)as total_cost,datediff(curdate(),substring(i.batch_number,1,10)) AS inv_aeging,
(datediff(curdate(),substring(i.batch_number,1,10))*quantity)as wtd_amt,
p.name as Item_name,br.name as Brand,if(p.status=1,"Active","Inactive")as status,DATEDIFF(CURDATE(),date(p.created_at)) as Inv_Age
from inventories i,products p,brands br
where i.product_id=p.id  
and p.brand_id=br.id)res1
group by product_id)res2
join
(select *,if(margin<5,"Lowest_Margin",(if(margin<10,"Average_Margin",(if(margin<15,"High_Margin","Highest_Margin")))))as Margin_Class
from
(select m.name as MarketerName,m.code as MarketerCode,br.name as Brand,p.id as Prdoduct_id,p.name as ProductName,
pv.sku,pv.mrp,pv.price as Selling_Price,
(select unit_cost_price from inventories where product_id=p.id order by id desc limit 1) as cost,
((1-price/mrp)*100)as Margin,if(p.status=1,"Active","Inactive")as Status
from marketers m,brands br,products p,product_variants pv
where br.marketer_id=m.id
and p.brand_id=br.id
and p.id=pv.product_id
and pv.sku like "%unit")res3)res4
on res2.product_id=res4.Prdoduct_id;

declare continue handler for not found set done=1;
set done = 0;
open CUR;
igmLoop: loop
fetch CUR into m_n,m_c,br,p_id,p_n,sk,mr,sp,cp,marg,marg_c,aeg_c,s;
if done = 1 then leave igmLoop; end if;
IF EXISTS(SELECT product_id FROM  ptr_b WHERE product_id = p_id)
    THEN
update ptr_b set Marketer_Name=m_n,Marketer_Code=m_c,Brand=br,Product_Id=p_id,Product_Name=p_n,SKU=sk,MRP=mr,
Selling_Price=sp,Cost_Price=cp,Margin=marg,Margin_Class=marg_c,Aeging_Class=aeg_c,Status=s
        where product_id=p_id;
ELSE
INSERT INTO ptr_b(Marketer_Name,Marketer_Code,Brand,Product_Id,Product_Name,SKU,MRP,Selling_Price,Cost_Price,Margin,
Margin_Class,Aeging_Class,Status)
              VALUES(m_n,m_c,br,p_id,p_n,sk,mr,sp,cp,marg,marg_c,aeg_c,s);
END IF;
    end loop igmLoop;
    close CUR;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `ptr_sale`()
BEGIN

DECLARE done1 float;
declare product1_id int;
DECLARE w1 float;
DECLARE w2 float;
DECLARE w3 float;
DECLARE w4 float;
declare w5 float;

declare CUR1 cursor for
select oi.product_id,
sum(if((FLOOR(day(convert_tz(oi.created_at, '+00:00', '+05:30'))/7)+1)=1, oi.total, 0))as week1_sale,
sum(if((FLOOR(day(convert_tz(oi.created_at, '+00:00', '+05:30'))/7)+1)=2, oi.total, 0))as week2_sale,
sum(if((FLOOR(day(convert_tz(oi.created_at, '+00:00', '+05:30'))/7)+1)=3, oi.total, 0))as week3_sale,
sum(if((FLOOR(day(convert_tz(oi.created_at, '+00:00', '+05:30'))/7)+1)=4, oi.total, 0))as week4_sale,
sum(if((FLOOR(day(convert_tz(oi.created_at, '+00:00', '+05:30'))/7)+1)=5, oi.total, 0))as week5_sale
from orders o,order_items oi,invoices i
where o.id=oi.order_id
and o.id=i.order_id
and oi.status not in ("cancelled","processing","returned","pending_returned")
and i.status != 'cancelled'
and MONTH(convert_tz(oi.created_at, '+00:00', '+05:30'))=MONTH(CURDATE())
group by oi.product_id;

declare continue handler for not found set done1=1;
set done1 = 0;
open CUR1;
igmLoop: loop
fetch CUR1 into  product1_id,w1,w2,w3,w4,w5;
if done1 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Product_Id FROM  ptr_b WHERE Product_Id = product1_id)
    THEN
update ptr_b set Week1_Sale=w1,Week2_Sale=w2,Week3_Sale=w3,Week4_Sale=w4,Week5_Sale=w5
where Product_Id = product1_id;
ELSE
INSERT INTO ptr_b(Product_Id,Week1_Sale,Week2_Sale,Week3_Sale,Week4_Sale,Week5_Sale)
              VALUES(product1_id,w1,w2,w3,w4,w5);
END IF;
    end loop igmLoop;
    close CUR1;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `run_os`()
begin

declare stid, max_stid, run_os, max_os, max_tid, run_tid, run_osdays int;
set stid = 1;
set run_os = 0;
set max_stid = (select max(s.id) from niyoweb.stores s);
set max_os = 0;
set run_osdays = 0;
set max_tid = (select max(t.id) from niyoweb.transactions t 
				where t.status in ("completed") and t.deleted_at is null 
				and date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= "2021-03-31");
set run_tid = (select max(t.id) from niyoweb.transactions t 
				where t.status in ("completed") and t.deleted_at is null 
				and date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= "2021-03-31");

while(stid <= max_stid)
	do 
	set max_os = 
				(
				select sum(if(t.type="debit",+1,-1)*t.amount) 
				from niyoweb.transactions t 
				where t.status in ("completed")
				and t.deleted_at is null 
				and date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= "2021-03-31"
				and t.store_id = stid 
				);
	set max_tid = 
				(
				select max(t.id) 
				from niyoweb.transactions t 
				where t.status in ("completed") and t.deleted_at is null 
				and date(convert_tz(t.transaction_date,"+00:00","+05:30")) <= "2021-03-31"
				and t.store_id = stid 
				and t.source_type = "invoice"
				);
	set run_tid = max_tid;
	while(run_os < max_os)
		do
		set run_os = run_os + 
					(
					select if(t.source_type="invoice" and t.store_id = stid,if(t.type="debit",+1,-1)*t.amount,0)
					from niyoweb.transactions t 
					where t.id = run_tid
					);
		set run_osdays = run_osdays + 
					(
					select if(t.source_type="invoice" and t.store_id = stid,if(t.type="debit",+1,-1)*t.amount*
						datediff("2021-03-31",date(convert_tz(t.transaction_date,"+00:00","+05:30"))),0)
					from niyoweb.transactions t 
					where t.id = run_tid
					);
		set run_tid = run_tid - 1;
	end while;
	update niyoweb.store_os_age soa 
		set soa.net_os = max_os, soa.run_os = run_os, soa.osdays = run_osdays/run_os, osday = "2021-03-31"
		where soa.store_id = stid;
	set run_os = 0;
	set run_osdays = 0;
	set stid = stid + 1;
end while;
	
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `script_notes`()
BEGIN
	


	


	

	
	

	



	

	



	

	
	

	


	
	

		

	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_agent_product`()
BEGIN

DROP TEMPORARY TABLE if exists agent_product;
CREATE TEMPORARY TABLE agent_product(AgentId int,ProductId int,O_Date date);

insert into agent_product(AgentId, ProductId, O_Date)
select distinct a.id as agent_id,p.id as product_id,date(o.created_at)as created_at 
from orders o,order_items oi,products p,invoices i,stores s,agents a,agent_beat_mktr_map abm_map,beats b, 
marketers m,brands br 
where o.id=oi.order_id 
and oi.product_id=p.id 
and o.id=i.order_id 
and o.store_id=s.id 
and s.beat_id=b.id 
and a.id=abm_map.agent_id 
and b.id=abm_map.beat_id 
and m.id=abm_map.marketer_id 
and br.marketer_id=m.id 
and (date(convert_tz(o.created_at,"+00:00","+05:30")) 
between abm_map.valid_from and abm_map.valid_to)
and p.brand_id=br.id 
and o.status!="cancelled" 
and i.status!="cancelled" 
and oi.status!="cancelled"
and oi.status!="processing"
and oi.status!="returned"
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
order by a.id,date(o.created_at);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_agent_store`()
BEGIN

DROP TEMPORARY TABLE if EXISTS agent_store;
CREATE TEMPORARY table agent_store(AgentId int,StoreId int,O_Date date);

insert into agent_store(AgentId, StoreId, O_Date)
select distinct a.id as agent_id,s.id as store_id,date(o.created_at)as created_at 
from orders o,order_items oi,products p,invoices i,stores s,agents a,agent_beat_mktr_map abm_map,beats b, 
marketers m,brands br 
where o.id=oi.order_id 
and oi.product_id=p.id 
and o.id=i.order_id 
and o.store_id=s.id 
and s.beat_id=b.id 
and a.id=abm_map.agent_id 
and b.id=abm_map.beat_id 
and m.id=abm_map.marketer_id 
and br.marketer_id=m.id 
and (date(convert_tz(o.created_at,"+00:00","+05:30")) 
between abm_map.valid_from and abm_map.valid_to)
and p.brand_id=br.id 
and o.status!="cancelled" 
and i.status!="cancelled" 
and oi.status!="cancelled"
and oi.status!="processing"
and oi.status!="returned"
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
order by a.id,date(o.created_at);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_agent_store_product`()
BEGIN
	
DECLARE First_Date date;
DECLARE END_Day date;
DECLARE startDate date;
DECLARE myAgentID INT;
DECLARE dailyStoreCount INT;
DECLARE dailyProductCount INT;
DECLARE cumStoreCount INT;
DECLARE cumProductCount INT;
DECLARE done1 INT;

DECLARE CUR1 CURSOR FOR SELECT id FROM agents WHERE TYPE='sales' AND STATUS=1;
declare continue handler for not found set done1=1;
set done1 = 0;

DROP TEMPORARY TABLE IF EXISTS finale;
CREATE TEMPORARY TABLE finale
(Agent_ID INT,Stores_count_daily INT,Cum_Stores INT,Product_count_daily INT,Cum_Product INT,Created_At date);





SET First_Date='2019-12-01';
SET startDate = First_Date;
SET END_Day='2019-12-31';

CALL S_agent_store();
CALL S_agent_product();

open CUR1;
igmLoop: loop
fetch CUR1 into myAgentID;
if done1 = 1 then leave igmLoop; end if;
SET startDate = First_Date;
WHILE startDate<=END_Day DO
	set @dailyStoreCount = (Select count(distinct StoreId) from agent_store where AgentId=myAgentID and O_Date=startDate);
	set @cumStoreCount =  (Select count(distinct StoreId) from agent_store where AgentId=myAgentID and O_Date<=startDate);
	set @dailyProductCount = (Select count(distinct ProductId) from agent_product where AgentId=myAgentID and O_Date=startDate);
    set @cumProductCount = (Select count(distinct ProductId) from agent_product where AgentId=myAgentID and O_Date<=startDate);
	
   	IF EXISTS (SELECT Agent_Id FROM finale where Agent_Id=myAgentID AND Created_At=startDate)
	THEN 	
		UPDATE finale SET Stores_count_daily=@dailyStoreCount, Cum_Stores=@cumStoreCount,
		Product_count_daily=@dailyProductCount, Cum_Product=@dailyProductCount
		WHERE Agent_ID=myAgentID AND Created_At=startDate;
	ELSE
		INSERT INTO finale(Agent_ID,Stores_count_daily,Cum_Stores,Product_count_daily,Cum_Product,Created_At) 
		VALUES(myAgentID,@dailyStoreCount,@cumStoreCount,@dailyProductCount,@cumProductCount,startDate);
	END IF;
	SET startDate=date_add(startDate,INTERVAL 1 day);
END WHILE;

end loop igmLoop;
close CUR1;

SELECT * FROM finale; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_App`()
BEGIN

DECLARE done5 int;
DECLARE a5 int;
DECLARE T_app float;
DECLARE c5 date;

declare cur_app cursor for
select res.agent_id,count(distinct(res.order_id))as Total_app_order,
res.created_at
from
(select oi.order_id,
oi.total as total,a.id as agent_id,
date(convert_tz(o.created_at, '+00:00', '+05:30')) as created_at
from orders o,order_items oi,products p, invoices i,
shipments sh, shipment_items shi,
agents a,beats b,marketers m,agent_beat_mktr_map abm_map,stores s,brands br
where o.id=oi.order_id
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id
and o.store_id=s.id
and oi.product_id=p.id
and o.id=i.order_id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at,"+00:00","+05:30")) between abm_map.valid_from and abm_map.valid_to)
and s.beat_id=b.id
and br.marketer_id=m.id
and p.brand_id=br.id
and o.source="app"
and o.status!="cancelled"
and oi.status!="cancelled"
and i.status!="cancelled"
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12)as res
group by res.agent_id,res.created_at
order by res.agent_id,res.created_at;

declare continue handler for not found set done5=1;
set done5 = 0;
open cur_app;
igmLoop: loop
fetch cur_app into a5,T_app,c5;
if done5 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a5 and Created_At=c5)
    THEN
update results set Agent_id=a5, App_Orders=T_app,Created_At=c5
        where Agent_id=a5
       and Created_At=c5;
ELSE
INSERT INTO results(Agent_id,App_Orders,Created_At)
              VALUES(a5,T_app,c5);
END IF;
    end loop igmLoop;
    close cur_app;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_bill`()
BEGIN

DECLARE done11 int;
DECLARE a11 int;
DECLARE bill float;
DECLARE attendence int;
DECLARE c11 date;

declare cur_bill cursor for
select a.id as agent_id,
count(distinct o.id) as Bill_cut,
if(count(distinct o.id)>12,"1","0")as Attendence,
date(convert_tz(o.created_at,'+00:00', '+05:30')) as orderdate
from orders o,order_items oi,invoices i,
products p, brands br, stores s,
agents a,beats b,marketers m,agent_beat_mktr_map abm_map
where o.id=oi.order_id
and i.order_id=o.id
and oi.product_id=p.id
and p.brand_id=br.id
and o.store_id=s.id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at,"+00:00","+05:30")) between abm_map.valid_from and abm_map.valid_to)
and s.beat_id=b.id
and m.id=br.marketer_id
and o.status!="cancelled"
and i.status!="cancelled"
and oi.status not in ("processing", "cancelled", "returned")
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
group by a.id ,date(orderdate)
order by a.id;

declare continue handler for not found set done11=1;
set done11 = 0;
open cur_bill;
igmLoop: loop
fetch cur_bill into a11,bill,attendence,c11;
if done11 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a11 and Created_At=c11)
    THEN
update results set Agent_id=a11, Bill_Cut=bill,Attendence=attendence,Created_At=c11
        where Agent_id=a11
       and Created_At=c11;
ELSE
INSERT INTO results(Agent_id,Bill_Cut,Attendenc,Created_At)
              VALUES(a11,bill,attendence,c11);
END IF;
    end loop igmLoop;
    close cur_bill;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_Cp`()
BEGIN

DECLARE done10 int;
DECLARE a10 int;
DECLARE cp10 float;
DECLARE sp10 float;
DECLARE m10 float;
DECLARE c10 date;

declare cur_cp cursor for
select res.agent_id,sum(res1.Cost_Price) as cp, sum(res.Selling_price) as sp,
((1 - (sum(res1.Cost_Price)/sum(res.Selling_price)))*100) as Margin ,res.order_created_at
from
(select a.id as agent_id,oi.id as oi_id, oi.total as Selling_price ,
date(convert_tz(o.created_at, '+00:00', '+05:30'))as order_created_at
from orders o,order_items oi,products p,invoices i,
shipments sh, shipment_items shi,
stores s,agents a,agent_beat_mktr_map abm_map,beats b,
marketers m,brands br
where o.id=oi.order_id
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id
and oi.product_id=p.id
and o.id=i.order_id
and o.store_id=s.id
and s.beat_id=b.id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at,"+00:00","+05:30")) between abm_map.valid_from and abm_map.valid_to)
and br.marketer_id=m.id
and p.brand_id=br.id
and o.status!="cancelled"
and i.status != "cancelled"
and oi.status not in ("cancelled", "processing", "returned")
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12)as res
left join
(select order_item_id,
sum(quantity*unit_cost_price) as Cost_Price
from order_item_inventories
group by order_item_id)as res1
on res.oi_id=res1.order_item_id
group by res.agent_id ,res.order_created_at
order by res.agent_id;


declare continue handler for not found set done10=1;
set done10 = 0;
open cur_cp;
igmLoop: loop
fetch cur_cp into a10,cp10,sp10,m10,c10;
if done10 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a10 and Created_At=c10)
    THEN
update results set Agent_id=a10, Cost_Price=cp10,Selling_Price=sp10,Margin=m10,Created_At=c10
        where Agent_id=a10
       and Created_At=c10;
ELSE
INSERT INTO results(Agent_id,Cost_Price,Selling_Price,Margin,Created_At)
              VALUES(a10,cp10,sp10,m10,c10);
END IF;
    end loop igmLoop;
    close cur_cp;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_Fp`()
BEGIN

DECLARE done5 int;
DECLARE a5 int;
DECLARE fp float;
DECLARE c5 date;

declare cur_fp cursor for
select a.id as agent_id,
sum(oi.total)as Revenue,
date(convert_tz(o.created_at, '+00:00', '+05:30'))as order_created_at
from orders o,order_items oi,invoices i, shipments sh, shipment_items shi, stores s,products p, focussed_products fp,
agents a,beats b,marketers m,brands br,agent_beat_mktr_map abm_map
where o.id=oi.order_id and o.store_id=s.id and o.id=i.order_id
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id
and oi.product_id=p.id
and p.brand_id=br.id
and fp.product_id=p.id
and s.beat_id=b.id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at, '+00:00', '+05:30')) between abm_map.valid_from and abm_map.valid_to)
and br.marketer_id=m.id
and i.status != "cancelled"
and oi.status not in ("cancelled", "processing", "returned")
and p.id=fp.product_id and date(convert_tz(o.created_at,"+00:00","+05:30"))=fp.valid_on
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
group by a.id,date(order_created_at)
order by a.id;

declare continue handler for not found set done5=1;
set done5 = 0;
open cur_fp;
igmLoop: loop
fetch cur_fp into a5,fp,c5;
if done5 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a5 and Created_At=c5)
    THEN
update results set Agent_id=a5,Focus_Products=fp,Created_At=c5
        where Agent_id=a5
       and Created_At=c5;
ELSE
INSERT INTO results(Agent_id,Focus_Products,Created_At)
              VALUES(a5,fp,c5);
END IF;
    end loop igmLoop;
    close cur_fp;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_Fp_App`()
BEGIN

DECLARE done6 int;
DECLARE a6 int;
DECLARE fp_app float;
DECLARE c6 date;

declare cur_fp_app cursor for
select a.id as agent_id,
count(distinct(oi.order_id))as fp_app_orders,
date(convert_tz(o.created_at, '+00:00', '+05:30'))as order_created_at
from orders o,order_items oi,invoices i, shipments sh, shipment_items shi, stores s,products p, focussed_products fp,
agents a,beats b,marketers m,brands br,agent_beat_mktr_map abm_map
where o.id=oi.order_id and o.store_id=s.id and o.id=i.order_id
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id
and oi.product_id=p.id
and p.brand_id=br.id
and s.beat_id=b.id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at, '+00:00', '+05:30')) between abm_map.valid_from and abm_map.valid_to)
and p.id=fp.product_id and date(convert_tz(o.created_at,"+00:00","+05:30"))=fp.valid_on
and br.marketer_id=m.id
and i.status != "cancelled"
and oi.status not in ("cancelled", "processing", "returned")
and o.source="app"
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
group by a.id,order_created_at
order by a.id;

declare continue handler for not found set done6=1;
set done6 = 0;
open cur_fp_app;
igmLoop: loop
fetch cur_fp_app into a6,fp_app,c6;
if done6 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a6 and Created_At=c6)
    THEN
update results set Agent_id=a6, Fp_App_Orders=fp_app,Created_At=c6
        where Agent_id=a6
       and Created_At=c6;
ELSE
INSERT INTO results(Agent_id,Fp_App_Orders,Created_At)
              VALUES(a6,fp_app,c6);
END IF;
    end loop igmLoop;
    close cur_fp_app;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_res`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS results;
CREATE TEMPORARY TABLE results
(Agent_id int,
Revenue float,Cummulative_Revenue float,Bill_Cut int,Cummulative_Bill_Cut float,
Focus_Products float,Cummulative_Focus_Products float,
App_Orders Float,Cummulative_App_Orders float,
Fp_App_Orders Float,Cummulative_Fp_App_Orders Float,
Attendence float,
Cost_Price float,Cummulative_Cost_Price float,Selling_Price float,Margin float,Overall_Margin float,
Sku float,Invoice float,Sku_Invoice float,
Unique_Stores int,
          Total_Stores int,
          Created_At date);
call S_revenue();
call S_bill();
call S_App();
call S_Fp();
call S_Fp_App();
call S_Cp();





select *
from results;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `S_revenue`()
BEGIN
DECLARE done1 int;
DECLARE a1 int;
DECLARE rev float;
DECLARE c1 date;

declare cur_rev cursor for
select a.id as agent_id,
sum(oi.total)as Revenue,
date(convert_tz(o.created_at, '+00:00', '+05:30'))as order_created_at
from orders o,order_items oi,invoices i, shipments sh, shipment_items shi, stores s,products p,
agents a,beats b,marketers m,brands br,agent_beat_mktr_map abm_map
where o.id=oi.order_id and o.store_id=s.id and o.id=i.order_id
and o.id = sh.order_id and sh.id=shi.shipment_id and oi.id=shi.order_item_id
and i.shipment_id=sh.id
and oi.product_id=p.id
and p.brand_id=br.id
and s.beat_id=b.id
and a.id=abm_map.agent_id
and b.id=abm_map.beat_id
and m.id=abm_map.marketer_id
and (date(convert_tz(o.created_at, '+00:00', '+05:30')) between abm_map.valid_from and abm_map.valid_to)
and br.marketer_id=m.id
and i.status != "cancelled"
and oi.status not in ("cancelled", "processing", "returned")
and month(convert_tz(o.created_at,"+00:00","+05:30"))=12
group by a.id,order_created_at
order by a.id;

declare continue handler for not found set done1=1;
set done1 = 0;
open cur_rev;
igmLoop: loop
fetch cur_rev into a1,rev,c1;
if done1 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT Agent_id,Created_At FROM results WHERE Agent_id= a1 and Created_At=c1)
    THEN
update results set Agent_id=a1, Revenue=rev,Created_At=c1
        where Agent_id=a1
       and Created_At=c1;
ELSE
INSERT INTO results(Agent_id,Revenue,Created_At)
              VALUES(a1,rev,c1);
END IF;
    end loop igmLoop;
    close cur_rev;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `temp_mis_monthly_marketers`(IN date_from VARCHAR(50),IN date_to VARCHAR(50))
BEGIN

DROP TABLE IF EXISTS mis_monthly_marketers ;
CREATE TABLE mis_monthly_marketers (marketer_id INT, marketer_code VARCHAR(4), marketer_alias VARCHAR (255),
marketer_id_name VARCHAR (255) , discount DECIMAL (16,6), sale DECIMAL (16,6) , cost DECIMAL (16,6) , margin DECIMAL (16,6) ,
margin_pc DECIMAL (9,6) , count_beats INT , count_stores INT , count_products INT , count_orders INT );
	
INSERT INTO mis_monthly_marketers 

SELECT 


res_fin.marketer_id, res_fin.marketer_code, res_fin.marketer_alias, res_fin.marketer_id_code_alias,









res_fin.order_item_discount AS fin_discount,

res_fin.order_item_total AS fin_sale,

res_fin.order_item_cost AS fin_cost,

res_fin.order_item_margin AS fin_margin,
res_fin.order_item_margin/res_fin.order_item_total AS fin_margin_pc,

res_fin.count_beats,
res_fin.count_stores,

res_fin.count_products,
res_fin.count_orders

FROM

(SELECT 
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	"none",IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,"sale","return")
	) AS sale_identifier,

IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	) AS sale_flag,

DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS sale_date,
DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS rto_date, 
DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS return_date,

oi.id AS order_item_id, oi.status AS order_item_status, DATE(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) AS order_item_date,
o.id AS order_id, o.status AS order_status, o.reference_id AS order_reference, DATE(CONVERT_TZ(o.created_at,"+00:00","+05:30")) AS order_date,
shi.id AS shipment_item_id, DATE(CONVERT_TZ(shi.created_at,"+00:00","+05:30")) AS shipment_item_date,
sh.id AS shipment_id, sh.status AS shipment_status, DATE(CONVERT_TZ(sh.created_at,"+00:00","+05:30")) AS shipment_date, DATE(CONVERT_TZ(sh.updated_at,"+00:00","+05:30")) AS shipment_update_date,
i.id AS invoice_id, i.status AS invoice_status, i.reference_id AS invoice_reference, DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) AS invoice_date, DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) AS invoice_cancelled_date,
roi.id AS return_order_item_id, DATE(CONVERT_TZ(roi.created_at,"+00:00","+05:30")) AS return_order_item_date,
ro.id AS return_order_id, DATE(CONVERT_TZ(ro.created_at,"+00:00","+05:30")) AS return_order_date,
cn.id AS credit_note_id, cn.status AS credit_note_status, cn.reference_id AS credit_note_reference, DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) AS credit_note_date,

o.source AS order_source, o.created_by_type, o.created_by_id, a.id AS agent_id, a.name AS agent_name,
s.id AS store_id, s.name AS store_name, s.gstin AS store_gstin,
bt.id AS beat_id, bt.name AS beat_name,
m.id AS marketer_id, m.code AS marketer_code, m.alias AS marketer_alias,
br.id AS brand_id, br.name AS brand_name,
p.id AS product_id, p.name AS product_name,
pv.sku AS sku, pv.quantity AS conversion, pv.mrp AS sku_mrp,




SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.discount) AS order_item_discount,

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*oi.total) AS order_item_total, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*res_oii.cost) AS order_item_cost, 

SUM(
IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
	AND (DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN date_from AND date_to OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to),
	0,IF(DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to,+1,-1)
	)
*(oi.total-res_oii.cost)) AS order_item_margin,

COUNT(DISTINCT(s.id)) AS count_stores,
COUNT(DISTINCT(bt.id)) AS count_beats,
COUNT(DISTINCT(m.id)) AS count_marketers,
COUNT(DISTINCT(p.id)) AS count_products,
COUNT(DISTINCT(o.id)) AS count_orders,

CONCAT(LPAD(bt.id,3,0),"-",bt.name) AS beat_id_name,
CONCAT(LPAD(s.id,4,0),"-",s.name) AS store_id_name,
CONCAT(LPAD(bt.id,3,0),"-",bt.name,"-",LPAD(s.id,4,0),"-",s.name) AS beat_id_name_store_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias) AS marketer_id_code_alias,
CONCAT(LPAD(br.id,4,0),"-",br.name) AS brand_id_name,
CONCAT(LPAD(p.id,4,0),"-",p.name) AS product_id_name,
CONCAT(LPAD(m.id,3,0),"-",m.code,"-",m.alias,"-",LPAD(p.id,4,0),"-",p.name) AS marketer_id_code_alias_product_id_name,

IF(bt.name LIKE "%1K%","1K","") AS store_indicator

FROM 
niyoweb.order_items oi 
LEFT JOIN niyoweb.orders o ON oi.order_id =o.id 
LEFT JOIN niyoweb.shipment_items shi ON shi.order_item_id =oi.id 
LEFT JOIN niyoweb.shipments sh ON shi.shipment_id = sh.id AND sh.status NOT IN ("cancelled")
LEFT JOIN niyoweb.invoices i ON i.shipment_id =sh.id
LEFT JOIN niyoweb.return_order_items roi ON roi.order_item_id =oi.id
LEFT JOIN niyoweb.return_orders ro ON roi.return_order_id =ro.id AND ro.status NOT IN ("cancelled")
LEFT JOIN niyoweb.credit_notes cn ON cn.return_order_id =ro.id AND cn.status NOT IN ("cancelled")
LEFT JOIN (SELECT oii.order_item_id , SUM(oii.quantity) AS quantity, SUM(oii.quantity*oii.unit_cost_price) AS cost
			FROM niyoweb.order_items oi
			LEFT JOIN niyoweb.order_item_inventories oii ON oii.order_item_id =oi.id 
			GROUP BY oi.id
			) AS res_oii ON res_oii.order_item_id=oi.id
LEFT JOIN niyoweb.stores s ON s.id=o.store_id 
LEFT JOIN niyoweb.beats bt ON bt.id=s.beat_id 
LEFT JOIN niyoweb.product_variants pv ON pv.sku=oi.sku 
LEFT JOIN niyoweb.products p ON p.id=oi.product_id 
LEFT JOIN niyoweb.brands br ON br.id=p.brand_id 
LEFT JOIN niyoweb.marketers m ON m.id=br.marketer_id 
LEFT JOIN niyoweb.agents a ON a.id=o.created_by_id AND o.created_by_type IN ("agent")

WHERE DATE(CONVERT_TZ(i.created_at,"+00:00","+05:30")) BETWEEN date_from AND date_to
OR DATE(CONVERT_TZ(i.cancelled_on,"+00:00","+05:30")) BETWEEN  date_from AND date_to
OR DATE(CONVERT_TZ(cn.created_at,"+00:00","+05:30")) BETWEEN  date_from AND date_to



GROUP BY m.id

) AS res_fin

;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `transaction_acc_update`()
BEGIN
update niyoweb.transactions t 
set t.opp_acc =
(case
	when t.source_id is not null then "gst_sale"
	when t.source_id is null then 
		case 
			when t.payment_method in ("upi","net_banking","cheque","cheque_bounce") then "icici bank"
			when t.payment_method like "%cash_with%" then "cash with agent"
			when t.payment_method like "%cash%" then "cash in hand"
			when t.payment_method like "%paytm%" then "paytm" 
			when t.payment_method like "%1k_%" then replace(t.payment_method,"1k_","")
			else t.payment_method 
			end
	else null
	end),
t.opp_acc_no =
(case
	when t.source_id is not null then null
	when t.source_id is null then 
		case 
			when t.payment_method in ("upi","net_banking","cheque","cheque_bounce") then "017105009556"
			when t.payment_method like "%cash_with%" then replace(t.payment_method,"cash_with_","")
			when t.payment_method like "%cash%" then null
			when t.payment_method like "%paytm%" then replace(t.payment_method,"paytm_","") 
			when t.payment_method like "%1k_%" then null
			else null
			end
	else null
	end)
where t.id is null 

;	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `update_club_price`()
BEGIN


INSERT niyoweb.club_prices
SELECT pv.id as product_variant_id, pv.product_id AS product_id, pv.sku AS sku, pv.mrp AS mrp, cc.value AS margin,

ROUND(IFNULL(pii.latest_unit_cost*pv.quantity/ifnull((1 - cc.value),0.97),pv.price),2) AS price, 
IF(pv.value="unit",CEILING(100/pv.mrp),1) AS moq, round(pii.latest_unit_cost*pv.quantity,6) AS latest_cost,
NOW() AS last_run, NOW() AS created_at, NOW() AS updated_at
FROM niyoweb.product_variants pv
LEFT JOIN niyoweb.products p ON p.id = pv.product_id
LEFT JOIN niyoweb.product_variants pvu ON pvu.product_id = p.id AND pvu.value = "unit"
LEFT JOIN niyoweb.Prod_Inventory pii ON pii.product_id = p.id
left join niyoweb.commission_tag_product ctp on ctp.product_id = p.id 
left join niyoweb.commission_tags ct on ct.id = ctp.commission_tag_id
left join niyoweb.club_commissions cc on cc.commission_tag_id = ct.id
WHERE pv.id NOT IN (SELECT cp.product_variant_id FROM niyoweb.club_prices cp)
GROUP BY pv.id
ORDER BY p.id ASC, pv.quantity ASC;



UPDATE niyoweb.club_prices cp
LEFT JOIN niyoweb.product_variants pv ON pv.id = cp.product_variant_id
LEFT JOIN niyoweb.products p ON p.id = pv.product_id
LEFT JOIN niyoweb.product_variants pvu ON pvu.product_id = p.id AND pvu.value = "unit"
LEFT JOIN niyoweb.Prod_Inventory pii ON pii.product_id = p.id
left join niyoweb.commission_tag_product ctp on ctp.product_id = p.id 
left join niyoweb.commission_tags ct on ct.id = ctp.commission_tag_id
left join niyoweb.club_commissions cc on cc.commission_tag_id = ct.id
SET cp.price = ROUND(IFNULL(pii.latest_unit_cost*pv.quantity/ifnull((1 - cc.value),0.97),pv.price),2),
cp.mrp = pv.mrp, cp.margin = cc.value,
cp.moq = IF(pv.value="unit",CEILING(100/pv.mrp),1),
cp.latest_cost = ROUND(pii.latest_unit_cost*pv.quantity,6),
cp.last_run = NOW()
WHERE cp.product_variant_id IS NOT NULL;


UPDATE niyoweb.product_variants pv
LEFT JOIN niyoweb.club_prices cp ON cp.product_variant_id = pv.id
SET pv.min_price = cp.price
WHERE pv.min_price > cp.price;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `update_pos_consumer_prices`(
	IN `run_what` VARCHAR(255)
)
BEGIN

if run_what = "update" then

 




update pos.products p
  left join niyoweb.new_pos_mrp_price_rt_ws npmpr on npmpr.pos_id = p.id
  set p.old_price = ifnull(p.price,p.mrp),
  p.price = if(ifnull(npmpr.new_rt_sp,p.mrp)=0,p.mrp,ifnull(npmpr.new_rt_sp,p.mrp)),
  p.old_mrp = p.mrp, p.mrp = npmpr.new_mrp 
  where p.id >= 1; 
 
update 
pos.product_store ps
left join pos.products p on p.id = ps.product_id 
set ps.old_price = ps.price,
ps.price = ifnull(p.price,ifnull(ps.old_price,p.mrp)),
ps.auto_pricing_at = now()
where ps.price != p.price
and ps.product_id >= 1;

update pos.product_store ps
left join pos.products p on p.id = ps.product_id
set ps.price = p.mrp
where ps.price is null;



ELSE 



select 
  res.*, 
  round(
    (
      case when res.mrp_basis = 'dynamic' then res.prod_cost / (
        1 - ifnull(
          res.cost_mrp, 
          ifnull(res.cost_sp + 0.15, 0.25)
        )
      ) else res.old_mrp end
    ), 
    6
  ) as new_mrp, 
  round(
    (
      1 * case when res.offer_price is not null then res.offer_price when res.price_basis = 'cost' then least(
        res.old_mrp, 
        res.prod_cost / (
          1 - ifnull(res.cost_sp, 0.15)
        )
      ) when res.price_basis = 'mrp' then least(
        res.old_mrp, 
        res.old_mrp * (
          1 - ifnull(mps.discount_pc, 0)
        )
      ) else least(res.old_mrp, res.old_sp) end
    ), 
    6
  ) as new_rt_sp, 
  0.000001 as rt_min_oq, 
  999999 as rt_max_oq, 
  round(
    least(1, res.ws_price_ratio)* (
      case when res.offer_price is not null then res.offer_price when res.price_basis = 'cost' then least(
        res.old_mrp, 
        res.prod_cost / (
          1 - ifnull(res.cost_sp, 0.15)
        )
      ) when res.price_basis = 'mrp' then least(
        res.old_mrp, 
        res.old_mrp * (
          1 - ifnull(mps.discount_pc, 0)
        )
      ) else least(res.old_mrp, res.old_sp) end
    ), 
    6
  ) as new_ws_sp, 
  greatest(
    1, 
    ceiling(200 / res.old_mrp)
  ) as ws_min_oq, 
  999999 as ws_max_oq 
from 
  (
    select 
      np.id as nt_id, 
      p.id as pos_id, 
      p.name as product_name, 
      lc.group_id, 
      np.cl4_id as category_l4_id, 
      m.level_id as marketer_level_id, 
      cl4cmr.commission_tag_id, 
      ct.base_commission as rt_commission, 
      ct.ws_commission, 
      (1 - base_commission)/(1 - ws_commission) as ws_price_ratio, 
      cl4cmr.price_basis, 
      cl4cmr.latest_cost_basis, 
      cl4cmr.mrp_basis, 
      round(
        ifnull(res_vcm.std_vcm, cl4cmr.std_vcm), 
        6
      ) as std_vcm, 
      cl4cmr.cost_sp, 
      cl4cmr.cost_mrp, 
      cl4cmr.mrp_slab, 
      ifnull(
        if(
          if(
            cl4cmr.latest_cost_basis = 'group', 
            lc.grp_lat_cost, lc.prd_lat_cost
          ) = 0, 
          (
            1 - ifnull(res_vcm.std_vcm, cl4cmr.std_vcm)
          ) * p.mrp, 
          if(
            cl4cmr.latest_cost_basis = 'group', 
            lc.grp_lat_cost, lc.prd_lat_cost
          )
        ), 
        (
          1 - ifnull(res_vcm.std_vcm, cl4cmr.std_vcm)
        ) * p.mrp
      ) as prod_cost, 
      p.mrp as old_mrp, 
      p.price as old_sp, 
      pop.offer_price,
      p.last_price_change 
    from 
      pos.products p 
      left join pos.product_vendor pvd on pvd.product_id = p.id 
      left join niyoweb.products np on np.id = 1 * pvd.vendor_product_identifier 
      left join niyoweb.brands br on br.id = np.brand_id 
      left join niyoweb.marketers m on m.id = br.marketer_id 
      left join niyoweb.pos_offer_prices pop on pop.product_id = np.id 
      and convert_tz(now(), '+00:00', '+05:30') between pop.date_from 
      and pop.date_to 
      and pop.on_offer = 1 
      and pop.remarks like '%1K%' 
      left join niyoweb.category_l4_cost_map_rt cl4cmr on cl4cmr.category_l4_id = np.cl4_id 
      and cl4cmr.marketer_level_id = m.level_id 
      left join niyoweb.commission_tags ct on ct.id = cl4cmr.commission_tag_id 
      left join niyoweb.latest_costs lc on lc.product_id = np.id 
      left join (
        select 
          p.cl4_id as category_l4_id, 
          m.level_id as marketer_level_id, 
          1 - sum(
            ifnull(i.unit_cost_price, 0)
          )/ sum(
            if(
              i.unit_cost_price is null, 0, pv.mrp
            )
          ) as std_vcm 
        from 
          niyoweb.inventories i 
          left join niyoweb.products p on p.id = i.product_id 
          left join niyoweb.product_variants pv on pv.product_id = p.id 
          and pv.value = 'unit' 
          left join niyoweb.brands br on br.id = p.brand_id 
          left join niyoweb.marketers m on m.id = br.marketer_id 
        where 
          date(
            convert_tz(i.created_at, '+00:00', '+05:30')
          ) between (
            date(
              convert_tz(now(), '+00:00', '+05:30')
            ) - interval 91 day
          ) 
          and date(
            convert_tz(now(), '+00:00', '+05:30')
          ) 
        group by 
          p.cl4_id, 
          m.level_id
      ) as res_vcm on res_vcm.category_l4_id = cl4cmr.category_l4_id 
      and res_vcm.marketer_level_id = cl4cmr.marketer_level_id
  ) as res 
  left join niyoweb.mrp_pricing_slabs mps on mps.cl2cm_cost_basis = res.price_basis 
  and 1 - res.prod_cost / res.old_mrp between mps.vcm_from 
  and mps.vcm_to 
group by 
  res.pos_id







;

end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `update_wh_abc_groups`()
BEGIN
declare date_start, date_end date;


set date_start = date(convert_tz(now(),"+00:00","+05:30")) - interval (dayofweek(date(convert_tz(now(),"+00:00","+05:30")))+27) day;
set date_end = date(convert_tz(now(),"+00:00","+05:30")) - interval (dayofweek(date(convert_tz(now(),"+00:00","+05:30")))+0) day;

   

delete from niyoweb.wh_group_abc where group_id >= 0;
insert niyoweb.wh_group_abc
select *, 
round(res1.cumulative_qty/res2.total_qty_sold,6) as cum_qty_share, 
if((res1.cumulative_qty-res1.qty_sold)/res2.total_qty_sold < 0.7, "A",
	if((res1.cumulative_qty-res1.qty_sold)/res2.total_qty_sold < 0.9, "B", "C") 
) as class_abc,

date_start as date_from,

date_end as date_to, (date_end - date_start + 1) as period_days,
now() as last_run, 
null as outer_quantity, null as case_quantity, null as unit_mrp, null as reorder_level_days, null as reorder_level_units, 
null as reorder_quantity_days, null as reorder_quantity_units, null as reorder_sku_value, null as reorder_sku_quantity, null as stock,
  null as demand_units_final, null as demand_sku_quantity_final, null as demand_generated
from 
(select res.*, round(@cum_qty := @cum_qty + res.qty_sold,6) as cumulative_qty 
from 
(select pg.id as group_id, pg.name as group_name, round(sum(oi.total),6) as sale, round(sum(oi.quantity),6) as qty_sold,

round(sum(oi.quantity)/(date_end - date_start + 1),6) as qty_per_day
from pos.order_items oi 
left join pos.products p on p.id = oi.product_id 
left join pos.orders o on o.id = oi.order_id 
left join pos.stores s on s.id = o.store_id 
left join pos.product_vendor pv on pv.product_id = if(p.id in (select npm.product_id from pos.null_product_map npm),(select npm.new_product_id from pos.null_product_map npm where npm.product_id = p.id),p.id) 
left join niyoweb.products np on np.id = pv.vendor_product_identifier 
left join niyoweb.groups_products gp on gp.product_id = np.id
left join niyoweb.product_groups pg on pg.id = gp.group_id
where o.status in ("fulfilled") 
and s.status = 1 and s.name not like "%zabardast%" 
and s.name not like "%test%" and s.name not like "%inventory%"
and s.name not like "%count%" 
and date(convert_tz(o.created_at,"+00:00","+05:30")) between date_start and date_end
and gp.product_id is not null and gp.product_id != 0 and gp.product_id != ""
  
group by pg.id 
order by pg.id asc) as res 
cross join (select @cum_qty := 0) as dummy1
order by qty_sold desc ) as res1 
cross join 
(select sum(oi.quantity) as total_qty_sold 
from pos.order_items oi 
left join pos.products p on p.id = oi.product_id 
left join pos.orders o on o.id = oi.order_id 
left join pos.stores s on s.id = o.store_id 
where o.status in ("fulfilled") 
and s.status = 1 and s.name not like "%zabardast%" 
and s.name not like "%test%" and s.name not like "%inventory%"
and s.name not like "%count%" 
and date(convert_tz(o.created_at,"+00:00","+05:30")) between date_start and date_end
  
) as res2
where res1.group_id is not null;


update niyoweb.wh_group_abc wga 
left join 
(select wga.date_from as period_start, wga.date_to as period_end, datediff(wga.date_to,wga.date_from)+1 as period_days,
wga.group_id, res3.outer_qty, res3.case_qty, res3.unit_mrp, 
wga.qty_sold as period_sale_units, wga.qty_sold/(datediff(wga.date_to,wga.date_from)+1) as day_sale_units,
wga.class_abc as abc_class, arp.reorder_level as reorder_level_days, ceiling(arp.reorder_level*wga.qty_sold/(datediff(wga.date_to,wga.date_from)+1)) as reorder_level_units,
arp.reorder_quantity as reorder_quantity_days, ceiling(arp.reorder_quantity*wga.qty_sold/(datediff(wga.date_to,wga.date_from)+1)) as reorder_quantity_units,
res.stock as group_stock,
if(res.stock > ceiling(arp.reorder_level*wga.qty_sold/(datediff(wga.date_to,wga.date_from)+1)), 0, ceiling(arp.reorder_quantity*wga.qty_sold/(datediff(wga.date_to,wga.date_from)+1))) as demand_generated_units,
if(res3.outer_qty>0,"outer",if(res3.case_qty>0,"case","unit")) as order_uom,
if(res3.outer_qty>0,res3.outer_qty,if(res3.case_qty>0,res3.case_qty,1)) as uom_qty
from niyoweb.wh_group_abc wga 
left join niyoweb.auto_replenish_parameters arp on arp.class = wga.class_abc and arp.type = "warehouse"
left join 
(
select gp.group_id, ifnull(sum(i.quantity),0) as stock
from niyoweb.groups_products gp
left join niyoweb.inventories i on i.product_id = gp.product_id
group by gp.group_id
) as res on res.group_id = wga.group_id
left join
(
select res2.group_id, max(res2.outer_qty) as outer_qty, max(case_qty) as case_qty, max(unit_mrp) as unit_mrp
from
(select gp.group_id, pv.product_id,
sum(if(pv.value="outer",pv.quantity,0)) as outer_qty, sum(if(pv.value="case",pv.quantity,0)) as case_qty, sum(if(pv.value="unit",pv.mrp,0)) as unit_mrp
from niyoweb.groups_products gp
left join niyoweb.product_variants pv on pv.product_id = gp.product_id
group by pv.product_id) as res2
group by res2.group_id) as res3 on res3.group_id = wga.group_id
order by wga.group_id) as res4 on res4.group_id = wga.group_id
  set wga.outer_quantity = res4.outer_qty,
  wga.case_quantity = res4.case_qty,
  wga.unit_mrp = res4.unit_mrp,
  wga.reorder_level_days = res4.reorder_level_days,
  wga.reorder_level_units = res4.reorder_level_units,
  wga.reorder_quantity_days = res4.reorder_quantity_days,
  wga.reorder_quantity_units = res4.reorder_quantity_units,
  wga.reorder_sku_value = res4.order_uom,
  wga.reorder_sku_quantity = res4.uom_qty,
  wga.stock = res4.group_stock,
  wga.demand_units_final = if(res4.group_stock > res4.reorder_quantity_units,0,
    greatest(ceiling(res4.demand_generated_units/res4.uom_qty),ceiling(100/res4.unit_mrp/res4.uom_qty))*res4.uom_qty),
  wga.demand_sku_quantity_final = if(res4.group_stock > res4.reorder_quantity_units,0,
    greatest(ceiling(res4.demand_generated_units/res4.uom_qty),ceiling(100/res4.unit_mrp/res4.uom_qty))),
  wga.demand_generated = if(res4.group_stock > res4.reorder_quantity_units,0,1),
  wga.last_run = now()
where wga.group_id != 0 and wga.group_id is not null and wga.group_id != "";


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `vehicle_wise_dispatch_data`()
BEGIN
CREATE TEMPORARY TABLE vehicle_wise_dispatch_data(
vehicle_name varchar(100),
box_count_d int,inv_count_d int,inv_amt_d int,box_count_m int,inv_count_m int,inv_amt_m int);
call vehicle_wise_dispatch_data_today();
call vehicle_wise_dispatch_data_month();

select vehicle_name as Vehicle,box_count_d as Today_Box_count,inv_count_d as Today_Invoice_count,
inv_amt_d as Today_invoice_Amount, box_count_m as Month_Box_count, inv_count_m as Month_Invoice_Count,
inv_amt_m as Month_Invoice_Amount, box_count_m/day(NOW()) as Avg_Box_count,
inv_count_m/day(NOW()) as Avg_Invoice_count, inv_amt_m/day(NOW()) as Avg_Invoice_Amount
from vehicle_wise_dispatch_data;

DROP TEMPORARY TABLE vehicle_wise_dispatch_data;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `vehicle_wise_dispatch_data_month`()
BEGIN
declare done3 int;
DECLARE v_name3 varchar(100);
DECLARE box_count3 int;
DECLARE inv_count3 int;
DECLARE inv_amount3 FLOAT;

DECLARE MONTH_CUR_V CURSOR for
select mf.vehicle_number,sum(sh.boxes)as Box_counts,count(i.id)as Invoice_counts,sum(i.amount)as Invoice_amount
from manifests mf,manifest_shipment mfs,shipments sh,orders o,invoices i,stores s,beats b
where mf.id=mfs.manifest_id
and mfs.shipment_id=sh.id
and sh.order_id=o.id
and o.id=i.order_id
and o.store_id=s.id
and s.beat_id=b.id
and month(convert_tz(mf.created_at,"+00:00","+05:30"))=month(CURDATE())
and mf.status!='cancelled'
group by mf.vehicle_number
order by mf.vehicle_number;
declare continue handler for not found set done3=1;
set done3 = 0;
open MONTH_CUR_V;
igmLoop: loop
fetch MONTH_CUR_V into v_name3, box_count3, inv_count3, inv_amount3;
if done3 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT vehicle_name FROM  vehicle_wise_dispatch_data WHERE vehicle_name = v_name3)
    THEN
update vehicle_wise_dispatch_data set box_count_m=box_count3, inv_count_m=inv_count3, inv_amt_m=inv_amount3
        where vehicle_name=v_name3;
ELSE
INSERT INTO vehicle_wise_dispatch_data(vehicle_name, box_count_m, inv_count_m, inv_amt_m)
              VALUES( v_name3,box_count3, inv_count3, inv_amount3);
END IF;
    end loop igmLoop;
    close MONTH_CUR_V;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`niyoweb`@`%` PROCEDURE `vehicle_wise_dispatch_data_today`()
BEGIN
DECLARE done2 int;
DECLARE v_name varchar(100);
DECLARE box_count2 int;
DECLARE inv_count2 int;
DECLARE inv_amount2 FLOAT;

DECLARE DAILY_CUR_V CURSOR for
select mf.vehicle_number,sum(sh.boxes)as Box_counts,count(i.id)as Invoice_counts,sum(i.amount)as Invoice_amount
from manifests mf,manifest_shipment mfs,shipments sh,orders o,invoices i,stores s,beats b
where mf.id=mfs.manifest_id
and mfs.shipment_id=sh.id
and sh.order_id=o.id
and o.id=i.order_id
and o.store_id=s.id
and s.beat_id=b.id
and date(convert_tz(mf.created_at,"+00:00","+05:30"))=date(CURDATE())
and mf.status='dispatched' and mf.status!='cancelled'
group by mf.vehicle_number
order by mf.vehicle_number;
declare continue handler for not found set done2=1;
set done2 = 0;
open DAILY_CUR_V;
igmLoop: loop
fetch DAILY_CUR_V into  v_name,box_count2, inv_count2, inv_amount2;
if done2 = 1 then leave igmLoop; end if;
IF EXISTS(SELECT vehicle_name FROM  vehicle_wise_dispatch_data WHERE vehicle_name = v_name)
    THEN
	update vehicle_wise_dispatch_data set box_count_d=box_count2, inv_count_d=inv_count2, inv_amt_d=inv_amount2
        where vehicle_name=v_name;
ELSE
	INSERT INTO vehicle_wise_dispatch_data(vehicle_name, box_count_d, inv_count_d, inv_amt_d)
              VALUES(v_name, box_count2, inv_count2, inv_amount2);
END IF;
    end loop igmLoop;
    close DAILY_CUR_V;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `categories`
--

/*!50001 DROP VIEW IF EXISTS `categories`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`niyoweb`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `categories` AS select `cl1`.`id` AS `cl1_id`,`cl1`.`name` AS `cl1_name`,`cl2`.`id` AS `cl2_id`,`cl2`.`name` AS `cl2_name`,`cl3`.`id` AS `cl3_id`,`cl3`.`name` AS `cl3_name`,`cl4`.`id` AS `cl4_id`,`cl4`.`name` AS `cl4_name` from (((`newcat_l1` `cl1` join `newcat_l2` `cl2`) join `newcat_l3` `cl3`) join `newcat_l4` `cl4`) where ((`cl2`.`newcat_l1_id` = `cl1`.`id`) and (`cl3`.`newcat_l2_id` = `cl2`.`id`) and (`cl4`.`newcat_l3_id` = `cl3`.`id`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-07 12:14:24
