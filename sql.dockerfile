FROM mysql:8.0.26
COPY ./db-data/schema.sql /docker-entrypoint-initdb.d
COPY ./db-data/v-actualdata.sql /docker-entrypoint-initdb.d
