<?php

namespace Niyotail\Exceptions;

use Exception;

class ServiceException extends Exception
{
    public function report()
    {
    }

    public function render($request)
    {
        return response(['message' => $this->getMessage(),'error' => $this->getMessage()], 422);
    }
}
