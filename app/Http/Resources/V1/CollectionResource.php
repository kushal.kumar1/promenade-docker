<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class CollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'=>$this->name,
            'slug'=>$this->slug,
            //'product_count' => $this->when(!empty($this->product_count), $this->product_count),
            'icon'=> Image::getSrc($this->resource, 100, 100)
        ];
    }
}
