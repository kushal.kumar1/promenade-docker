<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
        ];
        if (!empty($this->state))
            $data['state'] = new StateResource($this->state);
        return $data;
    }
}
