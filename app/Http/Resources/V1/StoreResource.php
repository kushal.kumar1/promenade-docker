<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\Users\UserResource;
use Niyotail\Models\Pincode;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'legal_name' => $this->legal_name,
            'gstin' => $this->gstin,
            'pan' => $this->pan,
            'lat' => (float)$this->lat,
            'lng' => (float)$this->lng,
            'address' => $this->address,
            'landmark' => $this->landmark,
            'pincode' => $this->pincode,
            'contact_person' => $this->contact_person,
            'contact_mobile' => $this->contact_mobile,
            'beat_id' => !empty($this->beat_id) ? $this->beat_id : null,
            'beat_name' => !empty($this->beat_id) ? $this->beat->name : null,
            'beat_long_name' => !empty($this->beat_id) ? $this->beat->long_name : null,
            'verified'=>(boolean)$this->verified,
            'outstanding_amount' => !empty($this->totalBalance) ? $this->totalBalance->balance_amount : 0.00,
            'pending_approval_amount' => !empty($this->totalPendingBalance) ? $this->totalPendingBalance->balance_amount : 0.00,
            'credit_applied' => (!empty($this->creditRequest) && $this->creditRequest->isNotEmpty()) ? $this->creditRequest->first()->status : null,
            //Todo: Eager load method to be implemented
            'serviceable'=>(boolean)Pincode::checkServiceability($this->pincode)
        ];
    }
}
