<?php

namespace Niyotail\Http\Resources\V1\Order;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\StoreResource;
use Niyotail\Http\Resources\V1\Users\UserResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $orderStatus = $this->getStatus();
        $itemsGroupedByStatus = $this->getItemsWithStatus();
        return [
            'id' => $this->id,
            'reference_id' => $this->reference_id,
            // 'user'=> new UserResource($this->user),
            'store'=> new StoreResource($this->store),
            'subtotal' => $this->subtotal,
            'discount' => $this->discount,
            'tax' => $this->tax,
            'total' => $this->total,
            'status' => $orderStatus['status'],
            'status_tracking' => $orderStatus['tracking'],
            'items'=> GroupedOrderItemResource::collection($this->itemsBySku->values()),
            $this->mergeWhen(!empty($itemsGroupedByStatus), [
               'items_with_status' => $itemsGroupedByStatus,
            ]),
            'shipping_address' => new OrderAddressResource($this->shippingAddress),
            'billing_address' => new OrderAddressResource($this->billingAddress),
            'created_at' => $this->created_at,
            'payment_method' => $this->getPaymentMethod(),
            'delivery_date' => !empty($this->required_delivery_date) ? $this->required_delivery_date : null,

        ];
    }

    private function getStatus()
    {
        $status = $this->status;
        $statusTrack = ['confirmed' => 0, 'billed' => 0, 'dispatched' => 0, 'delivered' => 0];
        switch ($this->status)
        {
          case 'confirmed':
          case 'manifested':
            $status = 'confirmed';
            $statusTrack['confirmed'] = 1;
            break;
          case 'billed':
            $status = 'billed';
            $statusTrack['confirmed'] = 1;
            $statusTrack['billed'] = 1;
            break;
          case 'transit':
            $status = 'dispatched';
            $statusTrack['confirmed'] = 1;
            $statusTrack['billed'] = 1;
            $statusTrack['dispatched'] = 1;
            $statusTrack['feedback'] = $this->getRatingParameters();
            break;
          case 'completed':
            $status = 'delivered';
            $statusTrack['confirmed'] = 1;
            $statusTrack['billed'] = 1;
            $statusTrack['dispatched'] = 1;
            $statusTrack['delivered'] = 1;
            $statusTrack['feedback'] = $this->getRatingParameters();
            break;
        }
        $orderStatus['status'] = $status;
        $orderStatus['tracking'] = $statusTrack;
        return $orderStatus;
    }

    private function getItemsWithStatus()
    {
        $itemsGroupedByStatus = [];
        foreach($this->ItemsByStatusAndSku as $status => $itemsBySku) {
            $itemsGroupedByStatus[$status] =  GroupedOrderItemResource::collection($itemsBySku->values());
        }
        return $itemsGroupedByStatus;
    }

    private function getRatingParameters()
    {
        $statusTrack['isRequired'] = true;
        $statusTrack['text'] = 'Please rate your order experience.';
        $statusTrack['type'] = 'delivery';
        if (!empty($this->rating) && $this->rating->isNotEmpty()) {
            $statusTrack['isRequired'] = false;
            $rating = $this->rating->where('type', 'delivery')->sortByDesc('id')->first();
            $statusTrack['rating'] = $rating;
        }
        return $statusTrack;
    }

    private function getPaymentMethod() {
        $methods = config('payments.online_methods');
        return $methods[$this->payment_method]['label'];
    }
}
