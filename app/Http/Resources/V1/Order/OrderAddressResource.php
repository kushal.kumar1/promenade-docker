<?php

namespace Niyotail\Http\Resources\V1\Order;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\CityResource;

class OrderAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address,
            'pincode' => $this->pincode,
            'city' => new CityResource($this->city),
            // 'incomplete'=>empty($this->address),
        ];
    }
}
