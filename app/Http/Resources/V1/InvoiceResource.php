<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $settledAmount=!empty($this->settledAmount)?$this->settledAmount->amount:0;
        $unSettledAmount=$this->amount-$settledAmount;
        return [
            'id' => $this->id,
            'reference_id' => $this->reference_id ,
            'amount' => $this->amount,
            'settled_amount' => (string)$settledAmount,
            'unsettled_amount' => (string)$unSettledAmount,
            'pdf_url' => '/v1/stores/'.$request->store_id.'/invoices/'.$this->id
        ];
    }
}
