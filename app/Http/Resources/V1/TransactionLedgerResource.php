<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionLedgerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $details = $this->getTransactionDetails();
        return [
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'payment_method' => $this->payment_method,
            'image' => $this->image,
            'type' => $this->type,
            'amount' => $this->amount,
            'balance' => $this->balance,
            'status' => $this->status,
            'remarks' => $this->remarks,
            'created_at' => $this->transaction_date,
            $this->mergeWhen(!empty($details), [
                'reference_id' => $details['reference_id'] ?? null,
                'reference_type' => $details['reference_type'] ?? null,
            ]),
            'invoices' => InvoiceResource::collection($this->invoices)
        ];
    }

    public function getTransactionDetails()
    {
        $details = null;
        if (!empty($this->source)) {
            $details['reference_type'] = $this->source_type;
            $details['reference_id'] = $this->source->reference_id;
        }
        switch ($this->payment_method) {
            case 'cheque':
            case 'cheque_bounce':
                $details['reference_type'] = $this->payment_method;
                $paymentDetail = !empty($this->payment_details) ? json_decode($this->payment_details) : null;
                $details['reference_id'] = !empty($paymentDetail) ? $paymentDetail->cheque_number : null;
                break;
            case 'net_banking':
            case 'UPI':
            case 'paytm':
            case 'paytm_abhishek':
            case 'paytm_sachin':
            case 'paytm_sangeetesh':
            case 'store_card_swipe':
                $details['reference_type'] = $this->payment_method;
                $paymentDetail = !empty($this->payment_details) ? json_decode($this->payment_details) : null;
                $details['reference_id'] = !empty($paymentDetail) ? $paymentDetail->reference_id : null;
                break;
            case 'cash':
                $details['reference_type'] = $this->payment_method;
                $details['reference_id'] = null;
                break;
            default:
                $details['reference_type'] = $this->payment_method;
                $details['reference_id'] = null;
        }
        return $details;
    }
}
