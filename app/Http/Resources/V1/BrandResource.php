<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            //'product_count' => $this->when(!empty($this->product_count), $this->product_count),
            //'sub_brands' => $this->when(!empty($this->sub_brands), BrandResource::collection(collect($this->sub_brands))),
            'logo'=> Image::getSrc($this->resource, 100)
        ];
    }
}
