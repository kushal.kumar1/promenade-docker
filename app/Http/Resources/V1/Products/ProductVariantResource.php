<?php

namespace Niyotail\Http\Resources\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\Cart\ProductVariantCartResource;
use Niyotail\Models\Store;

class ProductVariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //TODO: Optimize this by creating resource collection and passing the values from resource collection to resource
        $product = $this->resource->product;
        $product->load('activeInventories');
        // $stock = (int)($this->product->regularInventoryForWarehouse($this->warehouse_id)/$this->quantity);
        $store = Store::find($request->store_id);

        $salePrice = round($this->getFinalPrice($store) - $this->discount, 2);
        $appliedOffers = $this->transformRules($this->appliedRules);
        $offers = $this->transformRules($this->rules);
        $ntCost = $this->getNTCost($store);

        return [
            'id' => $this->id,
            'sku' => $this->sku,
            'value' => $this->value,
            'uom' => (!empty($this->uom) && ($this->value == 'unit')) ? $product->uom : 'pc',
            'quantity'=> $this->quantity,
            'mrp' => $this->mrp,
            'price' => $salePrice,
            'original_price' => $this->getFinalPrice($store),
            'margin' => calculateMargin($salePrice, $this->mrp),
            'moq' => $this->moq,
            'stock' => $this->getStock($product),
            'applied_offers' => empty($appliedOffers) ? $offers : $appliedOffers,
            'offers' => $offers,
            'cart' => new ProductVariantCartResource($this->cartItem, true),
            $this->mergeWhen($store->isClub1K() && !empty($ntCost), [
                'nt_cost' => $ntCost,
                'nt_margin' => calculateMargin($ntCost, $salePrice),
            ]),
            'show_nt_cost' => true,
        ];
    }

    private function transformRules($rules)
    {
        $offers = null;
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                $offers[] =  ['id' => $rule->id, 'name' => $rule->name];
            }
        }

        return $offers;
    }

    private function getStock($product)
    {
//        $stock = (int)($product->regularInventoryForWarehouse($this->warehouse_id)/$this->quantity);
        return 999;
    }

    private function getNTCost($store)
    {
        if ($store->isClub1K()) {
          if (!empty($this->clubPrice->latest_cost))
            return $this->clubPrice->latest_cost;
        }
        // $latestInventory = $product->activeInventories->sortByDesc('created_at')->first();
        // if(!empty($latestInventory))
        //   return $latestInventory->unit_cost_price * $this->quantity;

        return null;
    }
}
