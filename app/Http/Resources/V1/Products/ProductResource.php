<?php

namespace Niyotail\Http\Resources\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\BrandResource;

class ProductResource extends JsonResource
{
    //TODO: refactor to make one resource to include all conditions for different api, tetsing this out with the cart and introduced type = "detail" only for product page
    private $type;

    public function __construct($resource, $type = null)
    {
        $this->type = $type;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $warehouseIds = $this->warehouse_id;
        $variants = $this->variants;
        if (!empty($warehouseIds)) {
          $variants = $variants->map(function ($variant) use ($warehouseIds) {
              $variant['warehouse_id']=$warehouseIds;
              return $variant;
          });
        }
        return [
            'id' => $this->id,
            'name' => ucwords($this->name),
            'uom'=>$this->uom,
            'description' => $this->description,
             // $this->mergeWhen($this->type == "detail", [
            'brand' => !empty($this->brand) ? (new BrandResource($this->brand)) : null,
            'variants' => ProductVariantResource::collection($variants),
            //'variants' => ProductVariantResource::collection($this->variants),
            'salable' => $this->isSalable($this->beatId),
            // ]),
            'images' => ProductImageResource::collection($this->images),
            'group_buy' => null
        ];
    }
}
