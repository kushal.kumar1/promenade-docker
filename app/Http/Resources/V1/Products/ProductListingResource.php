<?php

namespace Niyotail\Http\Resources\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\BrandResource;

class ProductListingResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->variants->isEmpty()) {
            return [];
        }
        $cheapestMrp=$this->variants->first()->mrp;
        $cheapestPrice=$this->variants->first()->price;
        foreach ($this->variants as $variant) {
            if ($variant->price < $cheapestPrice) {
                $cheapestMrp = $variant->mrp;
                $cheapestPrice = $variant->price;
            }
        }

        $variantMargin = calculateMargin($cheapestPrice, $cheapestMrp);
        $warehouseIds = $this->warehouse_id;
        $variants = $this->variants;
        if (!empty($warehouseIds)) {
          $variants = $variants->map(function ($variant) use ($warehouseIds) {
              $variant['warehouse_id']=$warehouseIds;
              return $variant;
          });
        }

        return [
            'id'=> $this->id,
            'name'=> ucwords($this->name),
            'brand'=> !empty($this->brand)?(new BrandResource($this->brand)):null,
            'uom'=>$this->uom,
            'mrp'=>$cheapestMrp,
            'price'=>$cheapestPrice,
            'margin'=>$variantMargin,
            'images'=>ProductImageResource::collection($this->images),
            'salable'=>$this->isSalable($this->beatId),
            'variants' => ProductVariantResource::collection($variants),
            'group_buy' => null
        ];
    }
}
