<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'image'=>Image::getSrc($this->resource, 800, 400),
          'actionUrl'=>!empty($this->link) ? $this->link : '',
        ];
    }
}
