<?php

namespace Niyotail\Http\Resources\V1\Users;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\StoreResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data= [
            'id'=> $this->id,
            'first_name'=> $this->first_name,
            'last_name'=> $this->last_name,
            'email'=> $this->email,
            'mobile'=>$this->mobile,
            'verified'=>boolval($this->verified),
            'new_user'=> $this->stores->isEmpty(),
        ];
        $data['stores'] = StoreResource::collection($this->stores);

        return $data;
    }
}
