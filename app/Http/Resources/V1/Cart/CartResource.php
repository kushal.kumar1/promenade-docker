<?php

namespace Niyotail\Http\Resources\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\StoreResource;
use Niyotail\Http\Resources\V1\Users\UserResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $cartErrors =  $this->resource->validate();
        return [
            'items'=> CartItemResource::collection($this->modified_items),
            'free_items' =>  $this->when($this->free_items->isNotEmpty(), function () {
                return CartItemResource::collection($this->free_items);
            }),
            'subtotal' => $this->subtotal,
            'discount' => $this->discount,
            'tax' => $this->tax,
            'total' => $this->total,
            'shipping_address' => new CartAddressResource($this->shippingAddress),
            'billing_address' => new CartAddressResource($this->billingAddress),
            'cart_errors' => collect($cartErrors)
        ];
    }
}
