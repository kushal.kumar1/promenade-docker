<?php

namespace Niyotail\Http\Resources\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\Products\ProductResource;
use Niyotail\Http\Resources\V1\RuleResource;
use Niyotail\Models\CartItem;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => new ProductResource($this->product),
            'sku' => $this->sku,
            'variant' => $this->variant,
            'moq' => $this->productVariant->moq,
            'mrp' => $this->mrp,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'subtotal' => $this->subtotal,
            'tax' => $this->tax,
            'discount' => $this->discount,
            'total' => $this->total,
            // 'free_items' =>  $this->when($this->source == CartItem::SOURCE_MANUAL && $this->free_items->isNotEmpty(), function () {
            //     return self::collection($this->free_items);
            // }),
            // 'rules' => $this->when($this->source == CartItem::SOURCE_OFFER, function () {
            //     return RuleResource::collection($this->rules);
            // }),
            'free_items' => ($this->source == CartItem::SOURCE_MANUAL && $this->free_items->isNotEmpty()) ? self::collection($this->free_items) : [],
            'rules' => ($this->source == CartItem::SOURCE_OFFER) ? RuleResource::collection($this->rules) : [],
            'source' => $this->source,
        ];
    }
}
