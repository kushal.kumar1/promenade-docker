<?php

namespace Niyotail\Http\Resources\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\Products\ProductVariantResource;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;

class ProductVariantCartResource extends JsonResource
{
    protected $only_cart;

    public function __construct($resource, $only_cart = false)
    {
        $this->only_cart = $only_cart;
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $output = [
            'cart' => null,
            'items_count' => 0,
        ];


        if($this->resource instanceof CartItem) {
            $output = [
                'cart' => [
                    'variant_id' => $this->productVariant->id,
                    //'product_id' => $this->product->id,
                    'sku' => $this->sku,
                    'price' => $this->price,
                    'quantity' => $this->quantity,
                    'total' => $this->total,
                    //'tax' => round(abs($this->tax), 2),
                    //'mrp' => $this->mrp,
                    //'subtotal' => round(abs($this->subtotal), 2),
                    'discount' => $this->discount,
                    'margin' => calculateMargin($this->price, $this->mrp),
                    //'source' => $this->source,
                    'applied_offers' => $this->transformRules($this->rules)
                ],
                'items_count' => $this->cart->items()->count(),
            ];
        }

        // Deletion case
        if($this->resource instanceof Cart)
            $output['items_count'] =  $this->items()->count();

        // While products listing
        if($this->only_cart)
            $output = $output['cart'];

        return $output;
    }

    private function transformRules($rules)
    {
        $offers = null;
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                $offers[] =  ['id' => $rule->id, 'name' => $rule->name];
            }
        }

        return $offers;
    }
}
