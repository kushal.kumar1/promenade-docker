<?php

namespace Niyotail\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'latest_version'=>config('settings.latest_version'),
          'force_update_app_version'=>config('settings.force_update_app_version'),
        ];
    }
}
