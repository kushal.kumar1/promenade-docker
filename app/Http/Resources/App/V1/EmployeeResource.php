<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Models\Warehouse;

class EmployeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            $this->mergeWhen(!empty($this->email), [
                'email' => $this->email,
            ]),
            $this->mergeWhen(!empty($this->mobile), [
                'mobile' => $this->mobile,
            ]),
            'warehouse' => WarehouseResource::collection($this->warehouses)
        ];
    }
}
