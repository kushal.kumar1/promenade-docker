<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        $product = $this;
        return [
            'id' => $this->id,
            'name' => $this->name,
            'barcode' => $this->barcode,
            $this->mergeWhen(!empty($this->qty), [
                'quantity' => $this->qty
            ]),
            $this->mergeWhen(!is_null($this->featuredImage), [
                'featuredImage' => Image::getSrc($this->featuredImage,null,null)
            ]),
            $this->mergeWhen($this->relationLoaded('variants'), [
                'variants' => VariantResource::collection($this->variants)
            ])
        ];
    }
}
