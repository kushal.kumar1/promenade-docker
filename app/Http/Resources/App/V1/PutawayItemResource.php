<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class PutawayItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'placed_quantity' => $this->placed_quantity,
            'product' => new ProductResource($this->product),
            'storage' => new StorageResource($this->storage)
        ];
    }
}
