<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class StorageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->zone,
            'warehouse' => new WarehouseResource($this->warehouse),
            'category' => $this->category,
            'label' => $this->label
        ];
    }
}
