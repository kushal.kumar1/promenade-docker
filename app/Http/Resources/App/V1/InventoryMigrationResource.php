<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Models\Putaway\Putaway;

class InventoryMigrationResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => strtotime($this->created_at),
            'putaway_id' => $this->getPutawayId(),
            'items' => InventoryMigrationItemResource::collection($this->items),
            'from_remarks' => config('settings.inventory_migration_from_remarks')
        ];
    }

    private function getPutawayId()
    {
        $putaway = $this->putaways->where('status', '!=', Putaway::STATUS_CLOSED)->first();
        return !empty($putaway) ? $putaway->id : null;
    }
}
