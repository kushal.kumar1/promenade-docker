<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class ReasonResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sourceType' => $this->source_type,
            'reasonType' => $this->reason_type,
            'text' => $this->reason_text
        ];
    }
}