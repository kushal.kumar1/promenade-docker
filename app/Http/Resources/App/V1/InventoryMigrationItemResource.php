<?php

namespace Niyotail\Http\Resources\App\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class InventoryMigrationItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'quantity' => $this->quantity,
            'product' => new ProductResource($this->product),
            'source_storage' => new StorageResource($this->fromStorage),
            'placed_quantity' => $this->putawayItems->sum('placed_quantity') ?? null,
            'cart_quantity' => $this->putawayItems->where('placed_quantity', null)->sum('quantity') ?? null,
            'destination_storage' => new StorageResource($this->toStorage),
            'status' => $this->status
        ];
    }
}
