<?php

namespace Niyotail\Http\Resources\App\V1\Picklist;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;

class PicklistResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'items_count' => $this->orderItems->unique('sku')->count(),
            'quantity_to_be_picked' => $this->items->where('status', PicklistItem::STATUS_PENDING)->count(),
            'status' => $this->status,
            'created_at' => Carbon::parse($this->created_at)->setTimezone('Asia/Kolkata')->toDateTimeLocalString(),

            $this->mergeWhen(in_array($this->status, [Picklist::STATUS_PICKED, Picklist::STATUS_CLOSING, Picklist::STATUS_CLOSED]), [
                'time_taken' => Carbon::parse($this->picking_started_at)->diffInMinutes(Carbon::parse($this->picking_ended_at)) . " mins"
            ]),
        ];
    }
}
