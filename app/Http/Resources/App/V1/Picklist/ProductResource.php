<?php

namespace Niyotail\Http\Resources\App\V1\Picklist;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product_id' => $this->product_id,
            'barcode' => $this->barcode,
            'pending_quantity' => $this->picklist_items_count
        ];
    }
}
