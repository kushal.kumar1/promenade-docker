<?php

namespace Niyotail\Http\Resources\App\V1\Picklist;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;
use Niyotail\Models\ProductImage;

class PicklistItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $productImage = ProductImage::where('product_id', $this->product_id)->first();
        $imageUrl = Image::getSrc($productImage);
        return [
            'product_image' => $imageUrl,
            'product_id' => $this->product_id,
            'storage_id' => $this->storage_id,
            'name' => $this->product,
            'mrp' => $this->mrp,
            'barcode' => $this->barcode,
            'pending_quantity' => $this->pending_quantity,
            'picked_quantity' => $this->picked_quantity,
            'skipped_quantity' => $this->skipped_quantity,
            'location' => $this->location
        ];
    }
}
