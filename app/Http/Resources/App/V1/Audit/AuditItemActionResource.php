<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\StorageResource;

class AuditItemActionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'action' => $this->action,
            'delta_quantity' => $this->delta_quantity,
            $this->mergeWhen(!empty($this->destination_storage), [
                'destination_storage' => new StorageResource($this->destination_storage)
            ]),
            $this->mergeWhen(!empty($this->reason), [
                'reason' => new ReasonResource($this->reason)
            ]),
        ];
    }
}