<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\EmployeeResource;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\PutawayItemResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\StorageResource;

class AuditItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'auditId' => $this->audit_id,
            'product' => new ProductResource($this->product),
            'storage' => new StorageResource($this->storage),
            'systemQuantity' => $this->system_quantity,
            'status' => $this->status,
            $this->mergeWhen(!empty($this->assignedTo), [
                'assignedTo' => new EmployeeResource($this->assignedTo)
            ]),
            $this->mergeWhen(!empty($this->remarks), [
                'reason' => $this->remarks
            ]),
            $this->mergeWhen(!empty($this->auditItemMetas), [
                'actualQuantity' => AuditItemMetaResource::collection($this->auditItemMetas)
            ]),
            $this->mergeWhen(!empty($this->auditItemActions), [
                'actions' => AuditItemActionResource::collection($this->auditItemActions)
            ])
        ];
    }
}