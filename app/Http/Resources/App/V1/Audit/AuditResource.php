<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\EmployeeResource;
use Niyotail\Http\Resources\App\V1\PutawayItemResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\VariantResource;
use Niyotail\Http\Resources\App\V1\WarehouseResource;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;

class AuditResource extends JsonResource
{
    public function toArray($request)
    {
        $audit = Audit::find($this->id);

        $distinctProducts = AuditProductResource::collection(
            collect(AuditService::distinctProductIds($audit))
                ->map(function ($productId, $key) use ($audit) {
                    return [
                        'product' => Product::find($productId),
                        'audit' => $audit
                    ];
                })
        );
        $distinctStorages = AuditStorageResource::collection(
            collect(AuditService::distinctStorageIds($audit))
                ->map(function ($storageId, $key) use ($audit) {
                    return [
                        'storage' => Storage::find($storageId),
                        'audit' => $audit
                    ];
                })
        );

        return [
            'id' => $this->id,
            'name' => $this->name,
            'type' => $this->type,
            'status' => $this->status,
            $this->mergeWhen(!empty($this->reason), [
                'reason' => new ReasonResource($this->reason)
            ]),
            'warehouse' => new WarehouseResource($this->warehouse),
            $this->mergeWhen($this->type == Audit::TYPE_PRODUCT, [
                'distinctProducts' => $distinctProducts
            ]),
            $this->mergeWhen($this->type == Audit::TYPE_STORAGE, [
                'distinctStorages' => $distinctStorages
            ]),
            'createdBy' => new EmployeeResource($this->createdBy)
        ];
    }
}