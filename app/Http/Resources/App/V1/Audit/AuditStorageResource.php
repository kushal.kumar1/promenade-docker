<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\EmployeeResource;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\StorageResource;
use Niyotail\Http\Resources\App\V1\WarehouseResource;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Product;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;

class AuditStorageResource extends JsonResource
{
    public function toArray($request)
    {
        $audit = $this->resource['audit'];
        $storage = $this->resource['storage'];

        $auditItems = $audit->auditItems()
            ->notCancelled()
            ->whereStorage($storage->id)
            ->get();

        $totalQuantity = $auditItems
            ->reduce(function ($totalSystemQuantity, $auditItem) {
                return $totalSystemQuantity + AuditItemService::actualQuantity($auditItem);
            });

        $numberOfProducts = $auditItems->count();

        return [
            'product' => new StorageResource($storage),
            'totalQuantity' => $totalQuantity,
            'numberOfProducts' =>$numberOfProducts
        ];
    }
}