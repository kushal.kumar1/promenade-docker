<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\EmployeeResource;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\WarehouseResource;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Product;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;

class AuditProductResource extends JsonResource
{
    public function toArray($request)
    {
        $audit = $this->resource['audit'];
        $product = $this->resource['product'];

        $auditItems = $audit->auditItems()
            ->notCancelled()
            ->whereProduct($product->id)
            ->get();

        $totalQuantity = $auditItems
            ->reduce(function ($totalSystemQuantity, $auditItem) {
                return $totalSystemQuantity + AuditItemService::actualQuantity($auditItem);
            });

        $numberOfStorages = $auditItems->count();

        return [
            'product' => new ProductResource($product),
            'totalQuantity' => $totalQuantity,
            'numberOfStorages' =>$numberOfStorages
        ];
    }
}