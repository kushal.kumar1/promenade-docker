<?php

namespace Niyotail\Http\Resources\App\V1\Audit;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;

class AuditItemMetaResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'condition' => $this->condition,
            'quantity' => $this->quantity,
            $this->mergeWhen(!empty($this->reason), [
                'reason' => new ReasonResource($this->reason)
            ]),
            $this->mergeWhen(!empty($this->remarks), [
                'remarks' => new ReasonResource($this->remarks)
            ])
        ];
    }
}