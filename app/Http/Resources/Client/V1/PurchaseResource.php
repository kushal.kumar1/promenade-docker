<?php


namespace Niyotail\Http\Resources\Client\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseResource extends JsonResource
{
    public function toArray($request)
    {
        $groupedItems = $this->orderItems->groupBy('sku');
        return [
            'id' => $this->id,
            'status' => $this->status,
            'document_number' => $this->invoice->reference_id,
            'invoice_id' => $this->invoice->id,
            'total' => $this->invoice->amount,
            'otp' => $this->otp,
            'items' => PurchaseItemsResource::collection($groupedItems->values()),
            'created_at' => strtotime($this->resource->getOriginal('created_at'))
        ];
    }
}