<?php


namespace Niyotail\Http\Resources\Client\V1;


use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseItemsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'product' => $this->first()->product->name,
            'variant' => $this->first()->variant,
            'mrp' => $this->first()->mrp,
            'price' => $this->first()->price,
            'quantity' => $this->sum('quantity'),
            'subtotal' => $this->sum('subtotal'),
            'tax' => $this->sum('tax'),
            'total' => $this->sum('total')
        ];
    }
}