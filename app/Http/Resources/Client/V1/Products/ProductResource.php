<?php

namespace Niyotail\Http\Resources\Client\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Client\V1\BrandResource;
use Niyotail\Http\Resources\Client\V1\CollectionResource;
use Niyotail\Http\Resources\Client\V1\GroupResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $variant = $this->variants->where('value', 'unit')->first();
        return [
            'id' => $this->id,
            'name' => ucwords($this->name),
            'description' => $this->description,
            'brand' => !empty($this->brand) ? (new BrandResource($this->brand)) : null,
            'collections' => !empty($this->collections) ? CollectionResource::collection($this->collections) : null,
            'group' => !empty($this->group) ? (new GroupResource($this->group)) : null,
            'barcode' => $this->barcode,
            'uom' => $this->uom,
            'mrp' => $variant->mrp,
            'hsn_sac_code' => $this->hsn_sac_code,
            'tax_group_id' => $this->tax_class_id,
            'commission_tags' => !empty($this->commissionTags->first()) ? $this->commissionTags->first()->id : null
        ];
    }
}
