<?php


namespace Niyotail\Http\Resources\Client\V1\ReturnOrder;


use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Models\ReturnOrder;

class ReturnOrderItemResource extends JsonResource
{
    public function toArray($request)
    {
        $product = $this->first()->product;
        $variant = $this->first()->productVariant;
        $variantQty = $variant->quantity;
        $mrp = $this->first()->mrp / $variantQty;
        $price = $this->first()->price / $variantQty;

        $orderedQty = $this->sum('quantity') * $variantQty;
//        $returnedQty = $this->sum(function ($item) {
//            return $item->returnOrderItems->sum('units');
//        });

        $returnedQty = $this->sum(function ($item) {
            $units = 0;
            foreach($item->returnOrderItems as $returnOrderItem) {
                if ($returnOrderItem->returnOrder->status != ReturnOrder::STATUS_CANCELLED) {
                    $units += $returnOrderItem->units;
                }
            }
            return $units;
        });

        $returnRatio = round($returnedQty / $orderedQty, 6);

        return [
            'product' => $product->name,
            'variant' => 'unit',
            'mrp' => $mrp,
            'price' => $price,
            'quantity' => $returnedQty,
            'subtotal' => round($this->sum('subtotal') * $returnRatio, 2),
            'tax' => round($this->sum('tax') * $returnRatio, 2),
            'total' => round($this->sum('total') * $returnRatio, 2)
        ];
    }
}
