<?php


namespace Niyotail\Http\Resources\Client\V1\ReturnOrder;


use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Models\ReturnOrder;

class GroupedReturnOrderResource extends JsonResource
{
    public function toArray($request)
    {
        $returns = $this->getReturnParamters($this->returns);
        return [
            'ref_id' => $this->ref_id,
            'date' => $this->return_date,
            'status' => $returns['status'],
            'total_items' => $this->item_count,
            'total_amount' => $returns['total_amount'],
            'credit_amount' => $returns['credit_amount'],
            'returns' => ReturnOrderResource::collection($this->returns),
            // 'created_at' => strtotime($this->resource->getOriginal('created_at'))
        ];
    }

    private function getReturnParamters($returns) {
        $status = 'partially_credited';
        $count = count($returns);
        if (count($returns->where('status', ReturnOrder::STATUS_INITIATED)) == $count) {
            $status = 'initiated';
        } else if (count($returns->where('status', ReturnOrder::STATUS_INITIATED)) > 1) {
            $status = 'pending';
        } else if (count($returns->where('status', ReturnOrder::STATUS_COMPLETED)) == $count) {
            $status = 'fully_credited';
        } else if (count($returns->where('status', ReturnOrder::STATUS_CANCELLED)) == $count) {
            $status = 'cancelled';
        }

        $totalAmount = 0;
        $creditNoteAmount = 0;
        foreach($returns as $return) {
            $creditNoteAmount += $return->creditNote->amount ?? 0;
            $totalAmount += $return->orderItems->sum('total');
        }

        return ['status' => $status,
                'credit_amount' => $creditNoteAmount,
                'total_amount' => $totalAmount];
    }
}
