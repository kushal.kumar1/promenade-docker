<?php


namespace Niyotail\Http\Resources\Client\V1\ReturnOrder;


use Illuminate\Http\Resources\Json\JsonResource;

class ReturnOrderResource extends JsonResource
{
    public function toArray($request)
    {
        $groupedItems = $this->orderItems->groupBy('sku');
        return [
            'id' => $this->id,
            'status' => $this->status,
            'document_number' => $this->creditNote ? $this->creditNote->reference_id : null,
            'total' => $this->creditNote ? $this->creditNote->amount : null,
            'items' => ReturnOrderItemResource::collection($groupedItems->values()),
            'created_at' => strtotime($this->resource->getOriginal('created_at'))
        ];
    }
}
