<?php

namespace Niyotail\Http\Resources\Client\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Models\RateCreditNote;

class LedgerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $details = $this->getTransactionDetails();
        return [
            'id' => $this->id,
            'transaction_id' => $this->transaction_id,
            'payment_method' => $this->payment_method,
            'description' => $this->description,
            'type' => $this->type,
            'amount' => $this->amount,
            'balance' => $this->balance == 0 ? $this->balance : -($this->balance),
            'remarks' => $this->remarks,
            'transaction_date' => $this->transaction_date,
            'reference_id' => $details['reference_id'] ?? null,
            'reference_type' => $details['reference_type'] ?? null,

        ];
    }

    private function getTransactionDetails(): array
    {
        $details = [
            'reference_id' => null,
            'reference_type' => null
        ];
        if (!empty($this->source)) {
            $details['reference_type'] = ($this->source instanceof RateCreditNote) ? "$this->source_type-{$this->source->type}" : $this->source_type;
            $details['reference_id'] = $this->source->reference_id;
        }
        switch ($this->payment_method) {
            case 'cheque':
            case 'cheque_bounce':
                $details['reference_type'] = $this->payment_method;
                $paymentDetail = !empty($this->payment_details) ? json_decode($this->payment_details) : null;
                $details['reference_id'] = !empty($paymentDetail) ? $paymentDetail->cheque_number : null;
                break;
            case 'net_banking':
            case 'UPI':
            case 'paytm':
            case 'paytm_abhishek':
            case 'paytm_sachin':
            case 'paytm_sangeetesh':
                $details['reference_type'] = $this->payment_method;
                $paymentDetail = !empty($this->payment_details) ? json_decode($this->payment_details) : null;
                $details['reference_id'] = !empty($paymentDetail) ? $paymentDetail->reference_id : null;
                break;
            case 'cash':
                $details['reference_type'] = $this->payment_method;
                $details['reference_id'] = null;
                break;
        }
        return $details;
    }
}
