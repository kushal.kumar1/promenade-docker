<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;

class KpiResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            [
                'name' => 'Total Inventory',
                'value' => $this['kpiMetrics']['kpiStats']['total_inventory'],
                'decimals' => 0,
                'type' => 'money'
            ],
            // [
            //     'name' => 'JIT Inventory',
            //     'value' => $this['kpiMetrics']['kpiStats']['jit_inventory'],
            //     'decimals' => 0,
            //     'type' => 'money'
            // ],
            [
                'name' => 'Account Receivables',
                'value' => $this['kpiMetrics']['kpiStats']['account_receivables'],
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Average Revenue Per Order',
                'value' => $this['miscMetrics']['averageRevenuePerOrder'],
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Average Order per Store per Month',
                'value' => $this['miscMetrics']['averageOrderSize'] * $this['miscMetrics']['orderFrequencyPerMonth'],
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Total Brands',
                'value' => $this['kpiMetrics']['kpiStats']['brandsCount'],
                'decimals' => 0,
            ],
            [
                'name' => 'Average SKU per Order',
                'value' => $this['kpiMetrics']['averageSkuPerOrder'],
                'decimals' => 0,
            ],
            [
                'name' => 'Average Fulfilled SKU per Order',
                'value' => $this['kpiMetrics']['averageSkuPerOrderWithoutCancellation'],
                'decimals' => 0,
            ],
            [
                'name' => 'Average SKU per Retailer',
                'value' => $this['kpiMetrics']['averageSKUPerRetailer'],
                'decimals' => 0,
            ],
            [
                'name' => 'Order Frequency per Month',
                'value' => $this['storeMetrics']['storesThatOrdered'] === 0 ? null : $this['miscMetrics']['orderFrequencyPerMonth'],
                'decimals' => 0,
            ],
            [
                'name' => 'Average Order Size',
                'value' => $this['orderMetrics']['orderStats']->sum('count') === 0 ? null : $this['miscMetrics']['averageOrderSize'],
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Credit Days',
                'value' => $this['miscMetrics']['averageRevenuePerDay'] === 0 ? null : $this['kpiMetrics']['kpiStats']['account_receivables']/($this['miscMetrics']['averageRevenuePerDay']),
                'decimals' => 0,
            ],
            [
                'name' => 'Inventory Days',
                'value' => $this['miscMetrics']['averageRevenuePerDay'] === 0 ? null : $this['kpiMetrics']['kpiStats']['total_inventory']/$this['miscMetrics']['averageRevenuePerDay'],
                'decimals' => 0,
            ],
            [
                'name' => 'Gross Margin',
                'value' => $this['miscMetrics']['grossMarginAmount'],
                'decimals' => 0,
                'type' => 'money',
                'percent' => [
                    'name' => 'Gross Margin Percent',
                    'value' => $this['miscMetrics']['grossMarginPercentage'],
                ]
            ],
            [
                'name' => 'Discount',
                'value' => $this['miscMetrics']['discountAmount'],
                'decimals' => 0,
                'type' => 'money',
                'percent' => [
                    'name' => 'Discount Percent',
                    'value' => $this['miscMetrics']['discountAmountPercentage'],
                ]
            ],
        ];

        return $this->transformData($data);
    }

    private function transformData(array $rows): array {
        foreach ($rows as $index => $row) {
            if(isset($row['decimals']))
                $rows[$index]['value'] = number_format($row['value'], (int) $row['decimals']);
            if(isset($row['type']) && $row['type'] === 'money')
                $rows[$index]['value'] = '₹'.$rows[$index]['value'];
            if(isset($row['type']) && $row['type'] === 'percent')
                $rows[$index]['value'] = $rows[$index]['value'].'%';
            if(isset($row['percent']) && isset($row['percent']['value']))
                $rows[$index]['percent']['value'] = number_format($row['percent']['value'], 2).'%';
            unset($rows[$index]['type'], $rows[$index]['decimals']);
        }
        return $rows;
    }
}
