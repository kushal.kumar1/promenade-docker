<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class TopProductsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'quantity' => $this->quantity,
            'variant' => $this->variant,
            'order_item_price' => number_format($this->order_item_price, 0),
            'sku' => $this->sku,
            'image_url' => Image::getSrc($this->images->first(), 50, 50),
            'url' => route('admin::products.edit', $this->id)
        ];
    }
}
