<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\Graphs;

use Illuminate\Http\Resources\Json\JsonResource;

class GraphSalesOrdersResource extends JsonResource
{
    public function toArray($request)
    {
        $dates = [];
        $data = [];

        foreach ($this['orders'] as $index => $order) {
            $dates[] = $order->date;
            $rtoForNetRevenue = optional($this['rtoForNetRevenue']->where('date', $order->date)->first())->rto_amount ?? 0;
            $returns = optional($this['returns']->where('date', $order->date)->first())->rto_total ?? 0;
            $gross_revenue = optional($this['revenues']->where('date', $order->date)->first())->revenue ?? 0;

            $data['order_count'][$index] = $order->count;
            $data['order_total'][$index] = $order->sales;
            $data['gross_revenue'][$index] = $gross_revenue;
            $data['net_revenue'][$index] = $gross_revenue - ($returns + $rtoForNetRevenue);
        }

        return [
            'labels' => $dates,
            'orders' => $data['order_count'],
            'sales' => $data['order_total'],
            'revenues' => $data['net_revenue'],
        ];
    }
}
