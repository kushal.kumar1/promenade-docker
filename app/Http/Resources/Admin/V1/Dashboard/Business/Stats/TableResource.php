<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;

class TableResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [];
        foreach ($this['orderMetrics']['orderStats']->sortByDesc('date') as $orderStat) {
            $date = $orderStat->date;
            $net_revenue = (optional($this['revenueMetrics']['invoices']->where('date', $date)->first())->revenue ?? 0) - ((optional($this['returnMetrics']['returns']->where('date', $date)->first())->return_total ?? 0) + (optional($this['returnMetrics']['rtoForNetRevenue']->where('date', $date)->first())->rto_amount) ?? 0);
            $gross_margin = (optional($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())->sales_amount ?? 0) - (optional($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())->cost_of_sold_goods ?? 0);

            $gross_margin_percent = 0;
            if(!empty($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())) {
                if(!empty(optional($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())->sales_amount)) {
                    $gross_margin_percent = (1 - (float)(optional($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())->cost_of_sold_goods ?? 0) / optional($this['kpiMetrics']['kpiStats']['shipmentInventoryData']->where('date', $date)->first())->sales_amount ?? 0) * 100;
                }
            }

            $data[] = [
                'date' => $orderStat->date,
                'orders' => $orderStat->count,
                'stores' => $orderStat->store_count,
                'new_stores' => $this['storeMetrics']['storesCount'],
                'order_value' => 'Rs'.$orderStat->sales,
                // 'jit_order_value' => 'Rs'.(optional($this['orderMetrics']['jitOrderStats']->where('date', $date)->first())->sales ?? 0),
                'net_revenue' => 'Rs'.$net_revenue,
                'returns' => 'Rs'.(optional($this['returnMetrics']['returns']->where('date', $date)->first())->return_total ?? 0),
                'collections' => 'Rs'.(optional($this['collectionMetrics']['transaction']->where('date', $date)->first())->collection ?? 0),
                'gross_margin' => 'Rs'.$gross_margin,
                'gross_margin_percent' => number_format($gross_margin_percent, 2).'%',
            ];
        }

        return $this->transformData($data);
    }

    private function transformData(array $rows): array {
        foreach ($rows as $row_index => $columns) {
            foreach ($columns as $column_index => $column) {
                if(substr($column, 0, 2) === 'Rs') {
                    $rows[$row_index][$column_index] = '₹'.number_format(substr($column, 2, strlen($column)), 0);
                }
            }
        }
        return $rows;
    }
}
