<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;

class StatsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'kpi' => new KpiResource($this),
            'table' => new TableResource($this)
        ];
    }
}
