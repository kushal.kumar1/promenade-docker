<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;

class TopRetailersResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'beat_name' => $this->beat_name,
            'order_item_price' => number_format($this->order_item_price, 0),
            'address' => $this->address,
            'url' => route('admin::stores.profile', $this->id)
        ];
    }
}
