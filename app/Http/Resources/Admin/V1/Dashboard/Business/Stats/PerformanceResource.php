<?php

namespace Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats;

use Illuminate\Http\Resources\Json\JsonResource;

class PerformanceResource extends JsonResource
{
    public function toArray($request)
    {
        $data = [
            [
                'name' => 'Orders',
                'value' => $this['orderMetrics']['orderStats']->sum('count')
            ],
            [
                'name' => 'Order Value',
                'value' => $this['orderMetrics']['orderStats']->sum('sales'),
                'decimals' => 0,
                'type' => 'money'
            ],
            // [
            //     'name' => 'JIT Order Value',
            //     'value' => $this['orderMetrics']['jitOrderStats']->sum('sales'),
            //     'decimals' => 0,
            //     'type' => 'money'
            // ],
            [
                'name' => 'Net Revenue',
                'value' => $this['revenueMetrics']['invoices']->sum('revenue') - ($this['returnMetrics']['returns']->sum('return_total') + $this['returnMetrics']['rtoForNetRevenue']->sum('rto_total')),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Collections',
                'value' => $this['collectionMetrics']['transaction']->sum('collection'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Unfulfilled In Invoiced Orders',
                'value' => $this['orderMetrics']['unfulfilledInvoicedOrders']->sum('sales'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Order Pending Invoices',
                'value' => $this['orderMetrics']['orderPendingInvoices']->sum('sales'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Effective Coverage',
                'value' => (($this['storeMetrics']['storesThatOrdered'] / $this['storeMetrics']['totalActiveVerifiedStores']) * 100),
                'decimals' => 0,
                'type' => 'percent'
            ],
            [
                'name' => 'Stores Added',
                'value' => $this['storeMetrics']['storesCount']
            ],
            [
                'name' => 'Gross Revenue',
                'value' => $this['revenueMetrics']['invoices']->sum('revenue'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Cash Discount',
                'value' => $this['collectionMetrics']['cashDiscount']->sum('collection'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Bounced Cheques',
                'value' => $this['collectionMetrics']['chequeBounce']->sum('collection'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Pending Transactions',
                'value' => $this['collectionMetrics']['pending']->sum('collection'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Cancelled Items',
                'value' => $this['orderMetrics']['unfulfilledInvoicedOrders']->where('order_item_status', 'cancelled')->sum('sales'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'RTO',
                'value' => $this['returnMetrics']['rtoStats']->sum('rto_total'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'RTO Past',
                'value' => $this['returnMetrics']['rtoForNetRevenue']->sum('rto_total'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Returns',
                'value' => $this['returnMetrics']['returns']->sum('return_total'),
                'decimals' => 0,
                'type' => 'money'
            ],
            [
                'name' => 'Stores',
                'value' => $this['storeMetrics']['storesThatOrdered'],
            ],
        ];

        return $this->transformData($data);
    }

    private function transformData(array $rows): array {
        foreach ($rows as $index => $row) {
            if(isset($row['decimals']))
                $rows[$index]['value'] = number_format($row['value'], (int) $row['decimals']);
            if(isset($row['type']) && $row['type'] === 'money')
                $rows[$index]['value'] = '₹'.$rows[$index]['value'];
            if(isset($row['type']) && $row['type'] === 'percent')
                $rows[$index]['value'] = $rows[$index]['value'].'%';
            if(isset($row['percent']) && isset($row['percent']['value']))
                $rows[$index]['percent']['value'] = number_format($row['percent']['value'], 2).'%';
            unset($rows[$index]['type'], $rows[$index]['decimals']);
        }
        return $rows;
    }
}
