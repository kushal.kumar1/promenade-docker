<?php

namespace Niyotail\Http\Resources\Agent\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class DeliveryResource extends JsonResource
{
    /**
     * Optimized algorithm after looking at various StackOverflow answers and blog posts.
     * Main sources:
     * <ul>
     *     <li><a href="https://geodatasource.com/developers/php">GeoDataSource PHP</a></li>
     *     <li><a href="https://en.wikipedia.org/wiki/Great-circle_distance#Computational_formulas">Wikipedia</a></li>
     * </ul>
     *
     * The first source is based on the haversine formula, which is only accurate for small distances.
     * To guarantee accuracy across all distances, this function is based on the Vincenty formula
     * (read through the above Wikipedia link to understand the difference).
     *
     * @param float $latitudeFrom
     * @param float $longitudeFrom
     * @param float $latitudeTo
     * @param float $longitudeTo
     *
     * @return float [km]
     *
     * @author Adhiraj Singh Chauhan
     */
    function calculateDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
        if (($latitudeFrom == $latitudeTo) && ($longitudeFrom == $longitudeTo)) {
            return 0;
        } else {
            // convert from degrees to radians
            $latFrom = deg2rad($latitudeFrom);
            $lonFrom = deg2rad($longitudeFrom);
            $latTo = deg2rad($latitudeTo);
            $lonTo = deg2rad($longitudeTo);

            $lonDelta = $lonTo - $lonFrom;

            $a = pow(cos($latTo) * sin($lonDelta), 2) +
                pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
            $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

            $angle = atan2(sqrt($a), $b);

            // return distance in km (multiply angle by Earth's mean radius in km)
            return $angle * 6371;
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $shipments = [];
        // trim down the fat for shipments
        foreach ($this->shipments as $shipment) {
            if ($shipment->canDeliver() || $shipment->canReturn()) {
                $order = $shipment->order;
                $shippingAddress = $order->shippingAddress;
                $store = $order->store;
                $storeUsers = $store->users;

                $users = [];
                foreach ($storeUsers as $user) {
                    $users[] = [
                        'name' => trim($user->first_name . ' ' . $user->last_name),
                        'mobile' => $user->mobile
                    ];
                }

                $shipmentDetails = [];
                foreach ($order->items as $item) {
                    $product = $item->product;
                    $productId = $product->id;

                    // db stores individual rows of order items if more than 1 item is added to cart
                    // merge all these items and increment count
                    if (!isset($shipmentDetails[$productId])) {
                        $shipmentDetails[$productId] = [
                            'productId' => $productId,
                            'productName' => $product->name,
                            'productImageUrl' => Image::getSrc($product->images->first()),
                            'sku' => $item->sku,
                            'variant' => $item->variant,
                            'quantity' => $item->quantity
                        ];
                    } else {
                        $shipmentDetails[$productId]['quantity'] += $item->quantity;
                    }
                }

                $shipments[] = [
                    'id' => $shipment->id,
                    'status' => $shipment->status,
                    'boxes' => $shipment->boxes,
                    'invoiceRef' => $shipment->invoice->reference_id,
                    'shippingAddress' => [
                        'name' => $shippingAddress->name,
                        'mobile' => $shippingAddress->mobile,
                        'lat' => $shippingAddress->lat,
                        'lng' => $shippingAddress->lng,
                        'address' => $shippingAddress->address,
                        'distance' => $request->lat && $request->lng
                            ? (float)number_format($this->calculateDistance(
                                $shippingAddress->lat, $shippingAddress->lng, $request->lat, $request->lng),
                                2
                            )
                            : 0
                    ],
                    'shipmentDetails' => $shipmentDetails,
                    'users' => $users,
                    'beat' => $store->beat->name
                ];
            }
        }

        return $shipments;
    }
}
