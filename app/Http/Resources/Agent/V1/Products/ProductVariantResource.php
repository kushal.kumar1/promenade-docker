<?php

namespace Niyotail\Http\Resources\Agent\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\Cart\ProductVariantCartResource;
use Niyotail\Models\Store;

class ProductVariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sku' => $this->sku,
            'value' => $this->value,
            'uom' => (!empty($this->uom) && ($this->value == 'unit')) ? $this->uom : 'pc',
            'quantity' => intval(!empty($this->quantity) ? $this->quantity : 1),
            'mrp' => floatval($this->mrp),
            'min_price' => floatval($this->min_price),
            'price' => floatval($this->price),
            'max_price' => floatval($this->max_price),
            'margin' => calculateMargin($this->price, $this->mrp),
            'moq' => (intval($this->moq) == 0) ? 1 : intval($this->moq),
            'stock' => (float) 999,
            'offers' => null, //$this->transformRules($this->rules),
            'allow_negotiation' => false,
            'cart' => new ProductVariantCartResource($this->cartItem, true)
        ];
    }

    private function transformRules($rules)
    {
        $offers = null;
        if (!empty($rules)) {
            foreach ($rules as $rule) {
                $offers[] = ['id' => $rule->id, 'name' => $rule->name];
            }
        }

        return $offers;
    }
}
