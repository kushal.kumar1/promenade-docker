<?php

namespace Niyotail\Http\Resources\Agent\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\V1\BrandResource;
use Niyotail\Http\Resources\Agent\V1\TagResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $warehouseIds = $this->warehouse_id;
        $variants = $this->variants;
        if (!empty($warehouseIds)) {
            $variants = $variants->map(function ($variant) use ($warehouseIds) {
                $variant['warehouse_id'] = $warehouseIds;
                return $variant;
            });
        }
        return [
            'id' => $this->id,
            'name' => ucwords($this->name),
            'slug' => $this->slug,
            'description' => $this->description,
            'uom' => $this->uom ?? 'pc',
            'agent_description' => $this->agent_product_description,
            'brand' => !empty($this->brand) ? (new BrandResource($this->brand)) : null,
            'stock' => $this->total_inventory,
            'variants' => ProductVariantResource::collection($variants),
            'jit' => !empty($this->allow_back_orders),
            'images' => ProductImageResource::collection($this->images),
            'salable' => !empty($this->beatId) ? $this->isSalable($this->beatId) : true,
            'tags' => !empty($this->tags) ? TagResource::collection($this->tags) : null,
            'group_buy' => null
        ];
    }
}
