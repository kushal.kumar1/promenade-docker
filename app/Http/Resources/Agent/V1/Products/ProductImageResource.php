<?php

namespace Niyotail\Http\Resources\Agent\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class ProductImageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'position'=>$this->position,
            'image'=>Image::getSrc($this->resource,null,400)
        ];
    }
}
