<?php

namespace Niyotail\Http\Resources\Agent\V1\Products;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\BrandResource;
use Niyotail\Http\Resources\Agent\V1\TagResource;

class ProductListingResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $cheapestMrp=$this->variants->first()->mrp;
        $cheapestPrice=$this->variants->first()->price;
        foreach ($this->variants as $variant) {
            if ($variant->price < $cheapestPrice) {
                $cheapestMrp=$variant->mrp;
                $cheapestPrice=$variant->price;
            }
        }

        return [
            'id'=> $this->id,
            'name'=> ucwords($this->name),
            'slug'=> $this->slug,
            'brand'=> !empty($this->brand)?(new BrandResource($this->brand)):null,
            'mrp'=> floatval($cheapestMrp),
            'price'=> floatval($cheapestPrice),
            'uom' => $this->uom ?? 'pc',
            'images' => ProductImageResource::collection($this->images),
            'salable' => true, //$this->isSalable($this->beatId),
            'variants' => ProductVariantResource::collection($this->variants),
            'stock' => $this->relationLoaded('totalInventory') ? $this->total_inventory : null,
            'focussed' => false, //!empty($this->focussed),
            'jit' => false, //!empty($this->allow_back_orders),
            'tags' => null, //!empty($this->tags) ? TagResource::collection($this->tags) : null,
            'group_buy' => null
        ];
    }
}
