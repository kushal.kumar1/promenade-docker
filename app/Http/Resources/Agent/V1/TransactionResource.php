<?php

namespace Niyotail\Http\Resources\Agent\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $settledAmount = !empty($this->settledAmount) ? $this->settledAmount->amount:0;
        $unSettledAmount = $this->amount-$settledAmount;
        return [
            'id' => $this->id,
            'reference_id' => $this->transaction_id ,
            'amount' => $this->amount,
            'settled_amount' => (string)$settledAmount,
            'unsettled_amount' => (string)$unSettledAmount,
            'type' => $this->getType(),
            'invoice' => new InvoiceResource($this->source)
        ];
    }

    private function getType() {
      if (!empty($this->source_type)) {
          return $this->source_type;
      }
      switch($this->payment_method) {
        case 'cheque_bounce':
          return 'Cheque Bounce';
        case 'reverse_cash_discount':
          return 'REVERSE CD';
        case 'balance_transferred_winomkar':
          return 'Winomkar Carry over';
        case 'balance_transferred_marg':
          return 'Marg Carry over';
        case 'cheque_bounce_charges':
          return 'Cheque Bounce Charges';
        default:
          return null;
      }
    }
}
