<?php

namespace Niyotail\Http\Resources\Agent\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;


class ProductVariantCartResource extends JsonResource
{
    protected $for_listing;

    public function __construct($resource, $for_listing = false)
    {
        $this->for_listing = $for_listing;
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $output = [
            'cart' => null,
            'items_count' => 0,
        ];

        if($this->resource instanceof CartItem) {
            $output = [
                'cart' => [
                    'variant_id' => $this->productVariant->id,
                    'price' => round(abs($this->price), 2),
                    'quantity' => $this->quantity,
                    'total' => round(abs($this->total), 2),
                    'discount' => round(abs($this->discount), 2),
                    'margin' => calculateMargin($this->price, $this->mrp)
                ],
                'items_count' => $this->cart->items()->count(),
            ];
        }

        // Deletion case
        if($this->resource instanceof Cart)
            $output['items_count'] =  $this->items()->count();

        // While products listing
        if($this->for_listing)
            $output = $output['cart'];

        return $output;
    }
}
