<?php

namespace Niyotail\Http\Resources\Agent\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;


class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product' => new ProductListingResource($this->product),
            'sku' => $this->sku,
            'variant' => $this->variant,
            'moq' => $this->productVariant->moq,
            'mrp' => $this->mrp,
            'price' => $this->price,
            'quantity' => $this->quantity,
            'subtotal' => $this->subtotal,
            'tax' => $this->tax,
            'total' => $this->total,
        ];
    }
}
