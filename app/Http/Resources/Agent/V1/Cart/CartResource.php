<?php

namespace Niyotail\Http\Resources\Agent\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;


class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $cartErrors =  [];

        return [
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'discount' => $this->discount,
            'tax' => $this->tax,
            'total' => $this->total,
            'items'=> CartItemResource::collection($this->items),
            'shipping_address' => null,
            'billing_address' => null,
            'cart_errors' => collect($cartErrors),
            'profiling_required' => false
        ];
    }
}
