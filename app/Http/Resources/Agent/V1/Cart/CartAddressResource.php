<?php

namespace Niyotail\Http\Resources\Agent\V1\Cart;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\CityResource;

class CartAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mobile' => $this->mobile,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'location' => $this->location,
            'address' => $this->address,
            'landmark' => $this->landmark,
            'pincode' => $this->pincode,
            'city' => new CityResource($this->city),
            'incomplete' => empty($this->address),
        ];
    }
}
