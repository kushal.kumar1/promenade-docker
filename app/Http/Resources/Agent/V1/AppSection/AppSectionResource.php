<?php

namespace Niyotail\Http\Resources\Agent\V1\AppSection;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;
use Niyotail\Http\Resources\Agent\V1\BannerResource;
use Niyotail\Http\Resources\Agent\V1\CollectionResource;
use Niyotail\Http\Resources\Agent\V1\BrandResource;

class AppSectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            $this->mergeWhen(isset($this->title),['title' => $this->title]),
            'layout' => $this->layout,
            'grid_columns' => $this->grid_columns,
            'type' => $this->source,
            'code' => $this->getKeyWithCode(),
            'items' => $this->getSectionItems() ?: [],
        ];
    }

    protected function getKeyWithCode()
    {
        $source = $this->source;
        switch ($source) {
            case 'products':
                switch ($this->code) {
                    case 'new':
                        return 'new=1';
                    case 'focus':
                        return 'focussed=1';
                    case 'ufml':
                        return 'ufml=1';
                    case 'jit':
                        return 'jit=1';
                }
                break;
            case 'categories':
                return 'categories=all';
            case 'brands':
                return 'brands=all';
        }
        return '';
    }

    protected function getSectionItems()
    {
        switch ($this->source)
        {
          case 'banner':
            if ($this->layout == 'slider')
            {
              return array_filter(BannerResource::collection($this->items)->jsonSerialize());
            }
            break;
          case 'brands':
            return array_filter(BrandResource::collection($this->items)->jsonSerialize());
          case 'categories':
            return array_filter(CollectionResource::collection($this->items)->jsonSerialize());
          case 'products':
            return array_filter(ProductListingResource::collection($this->items)->jsonSerialize());
        }
        return array_filter(AppSectionItemResource::collection($this->items)->jsonSerialize());
    }
}
