<?php

namespace Niyotail\Http\Resources\Agent\V1\AppSection;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class AppSectionItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen($this->image, [
                'image' => $this->getImageUrl(),
                //'image' => $this->image,
            ]),
            'type' => $this->type,
            'actionUrl' => $this->code,
            'id' => $this->id
        ];
    }

    private function getImageUrl()
    {
        if (preg_match("/^(http)/", $this->image)) {
            return $this->image;
        }
        $image = Image::getSrc($this->resource);
        return $image;
    }
}
