<?php

namespace Niyotail\Http\Resources\Agent\V1\StoreProfiling;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileQuestionOptionResource extends JsonResource
{
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'option' => $this->option,
            'priority' => $this->priority,
            //'store_answer' => (!empty($this->storeAnswers) && $this->storeAnswers->isNotEmpty()) ? 'yes' : 'no',
            'store_answer' => $this->checkAnswer(),
        ];
    }

    private function checkAnswer() {
        $answer = 'no';
        if(!empty($this->storeAnswers) && $this->storeAnswers->isNotEmpty()) {
          $answer = empty($this->option) ? $this->storeAnswers->first()->answer : 'yes';
        }
        return $answer;
    }
}
