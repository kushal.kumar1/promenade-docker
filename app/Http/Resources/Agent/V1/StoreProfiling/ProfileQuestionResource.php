<?php

namespace Niyotail\Http\Resources\Agent\V1\StoreProfiling;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileQuestionResource extends JsonResource
{
    public function toArray($request)
    {
        //$storeProfileAnswer = !empty($this->storeProfile) ? $this->storeProfile->where('store_id', $request->store_id)->first() : null;
        $storeAnswer = $this->answerOptions->pluck('storeAnswers')->collapse();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'type'=> $this->type,
            'priority' => $this->priority,
            'is_mandatory' => $this->is_mandatory,
            'answer' => !empty($storeAnswer) ? ($storeAnswer->isNotEmpty() ? true : false) : false,
            'options'=> ProfileQuestionOptionResource::collection($this->answerOptions->values()),
        ];
    }
}
