<?php

namespace Niyotail\Http\Resources\Agent\V1\Order;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;

class GroupedOrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product' => new ProductListingResource($this->first()->product),
            'sku' => $this->first()->sku,
            'variant' => $this->first()->variant,
            'mrp' => $this->first()->mrp,
            'price' => $this->first()->price,
            'quantity' => $this->sum('quantity'),
            'subtotal' => $this->sum('subtotal'),
            'tax' => $this->sum('tax'),
            'total' => $this->sum('total')
        ];
    }
}
