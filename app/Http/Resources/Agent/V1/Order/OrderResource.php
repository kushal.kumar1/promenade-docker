<?php

namespace Niyotail\Http\Resources\Agent\V1\Order;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;
use Carbon\Carbon;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference_id' => $this->id,
            'store'=> new StoreResource($this->store),
            'subtotal' => $this->subtotal,
            'discount' => $this->discount,
            'tax' => $this->tax,
            'total' => $this->total,
            'status' => $this->status,
            'items' => OrderItemResource::collection($this->items),
            'cancellable' => ($this->status == 'confirmed') ? true : false,
            'shipping_address' => null,
            'billing_address' => null,
            'delivery_date' => null,
            'created_at' => $this->created_at

        ];
    }
}
