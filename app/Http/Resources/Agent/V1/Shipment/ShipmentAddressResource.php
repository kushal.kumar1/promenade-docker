<?php

namespace Niyotail\Http\Resources\Agent\V1\Shipment;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\CityResource;

class ShipmentAddressResource extends JsonResource
{
    private $distance;

    public function __construct($resource, $distance = null)
    {
        $this->distance = $distance;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'address' => $this->address,
            'pincode' => $this->pincode,
            'city' => new CityResource($this->city),
            'distance' => $this->distance
            // 'incomplete'=>empty($this->address),
        ];
    }
}
