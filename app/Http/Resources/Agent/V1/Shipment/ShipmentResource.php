<?php

namespace Niyotail\Http\Resources\Agent\V1\Shipment;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;
use Niyotail\Http\Resources\Agent\V1\UserResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;

class ShipmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $settledAmount = !empty($this->invoice->settledAmount) ? $this->invoice->settledAmount->amount : 0;
        $unSettledAmount = $this->invoice->amount - $settledAmount;
        $transactionID = $this->invoice->transactions->first()->id;
        return [
            'id' => $this->id,
            'status' => $this->status,
            'boxes' => $this->boxes,
            'invoice_reference' => $this->invoice->reference_id,
            'transaction_id' => $transactionID,
            'invoice_id' => $this->invoice->id,
            'invoice_unsettled_amount' => $unSettledAmount,
            'shipping_address' => new ShipmentAddressResource($this->order->shippingAddress, $this->distance),
            'beat' => $this->order->store->beat->name,
            'shipment_items' => GroupedShipmentItemResource::collection($this->orderItemsBySku->values()),
            'users' => UserResource::collection($this->order->store->users),
            'store' => new StoreResource($this->order->store),
            'pod_url' => !empty($this->pod_name) ? Image::getSrc($this->resource) : "",
        ];
    }
}
