<?php

namespace Niyotail\Http\Resources\Agent\V1\AgentTargetStats;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;


class AgentStoreStatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'email'=> $this->email,
            'mobile' => $this->mobile,
            'type' => $this->type,
            'stores' => $this->targetBeats->isNotEmpty() ? $this->getAllStoresWithTarget() : null
        ];
    }

    private function getAllStoresWithTarget() {
        $targetBeats = $this->targetBeats;
        $storesData = [];
        foreach($targetBeats as $targetBeat) {
            $stores = $targetBeat->beat->stores;
            foreach ($stores as $store) {
              $storesData[] = new StoreResource($store);
            }
        }
        return $storesData;
    }
}
