<?php

namespace Niyotail\Http\Resources\Agent\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Helpers\Image;

class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'color'=>$this->getColor($this->id),
        ];
    }

    private function getColor($id)
    {
      switch($id) {
        case -2:
          return '#2874bb';
        case -1:
          return '#d95a41';
        case 1:
          return '#013220';
        case 2:
          return '#013220';
        case 3:
          return '#013220';
        case 4:
          return '#7f00ff';
        case 5:
          return '#ff0000';
        default:
          return '#ff0000';
      }
    }
}
