<?php

namespace Niyotail\Http\Resources\Agent\V1;

use Illuminate\Http\Resources\Json\JsonResource;
use Niyotail\Http\Resources\Agent\V1\UserResource;
use Niyotail\Models\Pincode;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'legal_name' => $this->legal_name,
            'gstin' => $this->gstin,
            'pan' => $this->pan,
            'lat' => (float)$this->lat,
            'lng' => (float)$this->lng,
            'location' => $this->location,
            'address' => $this->address,
            'landmark' => $this->landmark,
            'pincode' => $this->pincode,
            'verified' => (bool)$this->verified,
            'contact_person' => $this->contact_person,
            'contact_mobile' => $this->contact_mobile,
            'beat_id' => !empty($this->beat_id) ? $this->beat_id : null,
            'beat_name' => !empty($this->beat_id) ? $this->beat->name : null,
            'beat_long_name' => !empty($this->beat_id) ? $this->beat->long_name : null,
            'outstanding_amount' => !empty($this->totalBalance) ? $this->totalBalance->balance_amount : 0.00,
            'pending_approval_amount' => !empty($this->totalPendingBalance) ? $this->totalPendingBalance->balance_amount : 0.00,
            'credit_limit' => $this->credit_limit_value,
            'status' => $this->status,
            'disable_reason' => $this->disable_reason,
            'users' => UserResource::collection($this->users)
        ];
    }
}
