<?php

namespace Niyotail\Http\Resources\Agent\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class BeatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'long_name' => $this->long_name,
        ];
    }
}
