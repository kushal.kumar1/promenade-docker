<?php


namespace Niyotail\Http\Resources\Brand;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class DashboardStatsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'store_id' => $this->id,
            'name' => $this->name,
            'geo' => array(
                "lat" => $this->lat,
                "lng" => $this->lng,
            ),
            "beat_id" => $this->beat_id,
            'location' => array(
                "location" => $this->location,
                "address"=> $this->address,
            ),
            "order_stats" => array(
                "total_orders" => $this->total_orders,
                "total_brands" => $this->total_brands,
                "total_items" => (integer) $this->total_products,
                "total_value" => array(
                    "value" => round($this->total_amount, 2),
                    "formatted_value" => "INR ".number_format($this->total_amount, 2)
                ),
            )
        ];
    }
}