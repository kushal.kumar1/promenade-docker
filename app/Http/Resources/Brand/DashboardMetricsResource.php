<?php


namespace Niyotail\Http\Resources\Brand;


use Illuminate\Http\Resources\Json\JsonResource;

class DashboardMetricsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->date,
            'total_amount' => $this->total_amount,
            'total_amount_formatted' => '&#8377; '.number_format($this->total_amount),
            'total_orders' => $this->total_orders,
            'total_products' => $this->total_products,
            'total_stores' => $this->total_stores,
            'total_beats' => $this->total_beats,
            'total_brands' => $this->total_brands
        ];
    }
}