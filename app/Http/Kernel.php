<?php

namespace Niyotail\Http;

use Niyotail\Http\Middleware\Api\V1\AuthMiddleware as ApiAuthMiddleware;
use Niyotail\Http\Middleware\Api\V1\ClientAuthMiddleware as ApiClientAuthMiddleware;
use Niyotail\Http\Middleware\Api\V1\CheckStoreInRequest;
use Niyotail\Http\Middleware\Api\V1\CheckClub1KStoreInRequest;
use Niyotail\Http\Middleware\Api\V1\UserOnBoarded;
use Niyotail\Http\Middleware\AssignGuard;
use Niyotail\Http\Middleware\AuthenticateMiddleware;
use Niyotail\Http\Middleware\Admin\AuthenticateMiddleware as AdminAuthenticateMiddleware;
use Niyotail\Http\Middleware\GuestMiddleware;
use Niyotail\Http\Middleware\TrustProxies;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Niyotail\Http\Middleware\Admin\BasicAuthMiddleware;
use Niyotail\Http\Middleware\VerifySelectedWarehouse;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Fruitcake\Cors\HandleCors::class,
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Niyotail\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        TrustProxies::class
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Niyotail\Http\Middleware\ViewMiddleware::class,
            \Niyotail\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Niyotail\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:100,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \Niyotail\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'chopper.guest' => GuestMiddleware::class,
        'chopper.auth' => AuthenticateMiddleware::class,
        'chopper.admin.auth' => AdminAuthenticateMiddleware::class,
        'chopper.admin.auth.basic' => BasicAuthMiddleware::class,
        'api.auth' => ApiAuthMiddleware::class,
        'api.client.auth' => ApiClientAuthMiddleware::class,
        // 'user.onBoarding' => UserOnBoarded::class,
        'store.check' => CheckStoreInRequest::class,
        'club1k.check' => CheckClub1KStoreInRequest::class,
        'assign.guard' => AssignGuard::class,
        'auth.jwt'  =>  \Tymon\JWTAuth\Http\Middleware\Authenticate::class,
        'warehouse.check' => VerifySelectedWarehouse::class,
        'api.warehouse.check' => \Niyotail\Http\Middleware\Api\V1\VerifySelectedWarehouse::class
    ];
}
