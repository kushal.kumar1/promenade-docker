<?php

namespace Niyotail\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

class ViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $viewFinder = View::getFinder();
        $viewFinder->addNamespace('chopper', resource_path('views/chopper'));
        $viewFinder->prependLocation(resource_path('views/chopper'));
        View::setFinder($viewFinder);
        return $next($request);
    }
}
