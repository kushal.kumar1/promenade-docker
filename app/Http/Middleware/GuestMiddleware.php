<?php

namespace Niyotail\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class GuestMiddleware
 *
 * @package Niyotail\Http\Middleware
 */
class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param string                    $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = 'admin')
    {
        if (Auth::guard($guard)->check()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/');
            }
        }
        return $next($request);
    }
}
