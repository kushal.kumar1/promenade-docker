<?php

namespace Niyotail\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Niyotail\Helpers\MultiWarehouseHelper;

class VerifySelectedWarehouse
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty(session()->get('selected-warehouse'))) {
            return redirect()->route('admin::choose-warehouse');
        }

        $warehouses = auth()->user()->warehouses;
        $warehouseHelper = app(MultiWarehouseHelper::class);
        $childWarehouses = auth()->user()->childWarehouses->where('parent_id', session()->get('selected-warehouse'));
        if (!in_array(session()->get('selected-warehouse'), $warehouses->pluck('id')->toArray())) {
            return redirect()->route('admin::choose-warehouse')
                ->with('error', "Access denied! Please choose valid warehouse!");
        }

        $currentWarehouses = $childWarehouses->pluck('id')->push(session()->get('selected-warehouse'))->toArray();
        $warehouseHelper->setWarehouses($currentWarehouses, session()->get('selected-warehouse'));
        return $next($request);
    }
}
