<?php

namespace Niyotail\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

/**
 * Class AuthenticateMiddleware
 *
 * @package Niyotail\Http\Middleware\Admin
 */
class AuthenticateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param null                      $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/login');
            }
        }
        if (!Auth::guard($guard)->user()->status) {
            Auth::guard($guard)->logout();
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('/login');
            }
        }

        return $next($request);
    }
}
