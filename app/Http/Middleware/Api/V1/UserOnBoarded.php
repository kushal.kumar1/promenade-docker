<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserOnBoarded
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard)
    {
        $user=Auth::guard($guard)->user();
        if($user->isNew())
            return response()->json(['error' => 'User on boarding incomplete','message' => 'User on boarding incomplete !'],422);

        return $next($request);
    }
}
