<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AuthMiddleware extends BaseMiddleware
{
    public function handle($request, Closure $next, $guard)
    {
        try {
            if (! $this->auth->parser()->setRequest($request)->hasToken()) {
                return response()->json(['error' => 'Token Missing','message' => 'Token missing'], 401);
            }
            $user=Auth::guard($guard)->user();
            if (empty($user)) {
                return response()->json(['error' => 'Token Invalid','message' => 'Token Invalid'], 401);
            }
            if (!$user->status) {
                return response()->json(['error' => 'User Blocked','message' => 'you are blocked !!'], 401);
            }
        } catch (TokenExpiredException | TokenInvalidException | TokenBlacklistedException $exception) {
            return response()->json(['error' => 'Token Invalid','message' => 'Token Invalid'], 401);
        }
        return $next($request);
    }
}
