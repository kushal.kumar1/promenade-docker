<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckClub1KStoreInRequest
{
    public function handle($request, Closure $next, $guard)
    {
        $user = Auth::guard($guard)->user();

        if (!$user->hasClub1KStoreAccess($request->store_id)) {
            auth()->logout();
            return response()->json([
                'error' => 'You have no 1K store!',
                'message' => 'You have no 1K store!'
            ], 401);
        }

        return $next($request);
    }
}
