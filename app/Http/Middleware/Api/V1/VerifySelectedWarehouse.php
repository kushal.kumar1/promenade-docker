<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;

class VerifySelectedWarehouse
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $warehouseId = $request->header('warehouseId');
        if (empty($warehouseId)) {
            return response()->json(['error' => 'Warehouse ID is required'], 422);
        }

        //TODO:: check for employee -> warehouse relation
        $request->request->add(['warehouse_id' => $warehouseId]);
        return $next($request);
    }
}
