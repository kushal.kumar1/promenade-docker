<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Store;

class CheckStoreInRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard)
    {
        $user=Auth::guard($guard)->user();
        if (! $request->has('store_id')) {
            //hack for old versions
            //Todo: remove once force update api is in released
            $user->load('stores');
            $stores = $user->stores;
            if (!empty($stores)) {
                $request->request->add(['store_id' => $user->stores->first()->id]);
                return $next($request);
            }

            return response()->json(['error' => 'Store Id Missing','message' => 'Store id missing'], 422);
        }

        $store=Store::find($request->store_id);
        if (empty($store)) {
            return response()->json(['error' => 'Store id does not exist.','message' => 'Incorrect Store id given'], 422);
        }
        if (!$user->hasStoreAccess($store->id)) {
            return response()->json(['error' => 'Unauthorised access','message' => 'You are not authorised to access this store.'], 403);
        }
        return $next($request);
    }
}
