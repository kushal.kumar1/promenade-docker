<?php

namespace Niyotail\Http\Middleware\Api\V1;

use Closure;
use Niyotail\Models\ApiClient;

class ClientAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param bool $clientName
     * @return mixed
     */
    public function handle($request, Closure $next, $clientName = false)
    {
        $apiClientRepo = null;
        if ($clientName)
            $apiClient = ApiClient::where(['name' => $clientName, 'secret' => $request->secret])->first();
        else
            $apiClient = ApiClient::where('secret', $request->secret)->first();
        if (!empty($apiClient) && ($apiClient->allowed_ips == '*' || in_array($request->ip(), explode(',', $apiClient->allowed_ips))))
            return $next($request);
        return response(['error' => 'Unauthorised access - Invalid secret or ip not allowed'], 401);
    }
}
