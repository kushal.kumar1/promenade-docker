<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\App\V1\Putaway\AddItemRequest;
use Niyotail\Http\Requests\Api\App\V1\Putaway\CloseRequest;
use Niyotail\Http\Requests\Api\App\V1\Putaway\InitRequest;
use Niyotail\Http\Requests\Api\App\V1\Putaway\SetStorageRequest;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\PutawayItemResource;
use Niyotail\Http\Resources\App\V1\PutawayResource;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Product;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;
use Niyotail\Services\Putaway\PutawayService;

class PutawayController extends Controller
{
    public function index(Request $request)
    {
        $employee = Auth::guard('employee_api')->user();
        $list = Putaway::with('items.storage')->where(['employee_id' => $employee->id, 'warehouse_id' => $request->warehouse_id])->orderBy('id', 'desc')->get();
        return PutawayResource::collection($list);
    }

    public function detail($id, Request $request)
    {
        $employee = Auth::guard('employee_api')->user();
        $employeeId = $employee->id;
        $putaway = Putaway::with('items.storage')->where(['employee_id' => $employeeId, 'warehouse_id' => $request->warehouse_id])->find($id);
        if(empty($putaway)){
            return response()->json(['message'=>"Putaway not found!", 'error'=>'Putaway not found!'],422);
        }
        return new PutawayResource($putaway);
    }

    public function init(InitRequest $request, PutawayService $service): PutawayResource
    {
        $employee = Auth::guard('employee_api')->user();
        $putaway = $service->create($request->warehouse_id, $employee);
        return new PutawayResource($putaway);
    }

    public function close($id, CloseRequest $request, PutawayService $service)
    {
        $employee = Auth::guard('employee_api')->user();
        $putaway = Putaway::byWarehouseEmployee($request->warehouse_id, $employee->id)->find($id);
        if (empty($putaway)) {
            return response()->json(['error' => 'Putaway not found!'], 422);
        }
        if ($putaway->status != Putaway::STATUS_STARTED) {
            return response()->json(['error' => 'No putaways found for finish operation!'], 422);
        }
        $putaway = $service->close($putaway);
        return new PutawayResource($putaway);
    }


    public function start($id, Request $request, PutawayService $service)
    {
        $employee = Auth::guard('employee_api')->user();
        $putaway = Putaway::byWarehouseEmployee($request->warehouse_id, $employee->id)->find($id);
        if (empty($putaway)) {
            return response()->json(['error' => 'Putaway not found!'], 422);
        }
        if ($putaway->status != Putaway::STATUS_OPEN) {
            return response()->json(['error' => 'No open putaways found!'], 422);
        }
        $putaway = $service->start($putaway);
        return new PutawayResource($putaway);
    }

    public function setItemStorage(SetStorageRequest $request, PutawayService $service)
    {
        $item = PutawayItem::whereHas('putaway', function ($query) use ($request) {
            $query->where('warehouse_id', $request->warehouse_id);
        })
            ->findOrFail($request->id);
        $item = $service->assignStorageToItem($item, $request->storage_id);

        return new PutawayItemResource($item);
    }

    public function putItem(AddItemRequest $request, PutawayService $putawayService)
    {
        $putaway = Putaway::where('warehouse_id', $request->warehouse_id)->findOrFail($request->id);
        $putawayItem = $putawayService->putItem($putaway, $request->product_id, $request->quantity);
        return new PutawayItemResource($putawayItem);
    }

    //TODO:: add qty check for products already in putaway list
    // withCount can be removed
    public function searchProduct(Request $request)
    {
        $products = Product::where('barcode', 'like', "%$request->barcode%")
            ->whereHas('inventoriesV2', function ($inventoryQuery) use ($request) {
                $inventoryQuery->where('status', FlatInventory::STATUS_PUTAWAY)
                    ->where('warehouse_id', $request->warehouse_id);
            })
//            ->withCount(['inventoriesV2 as qty' => function ($inventoryQuery) {
//                $inventoryQuery->where('status', FlatInventory::STATUS_PUTAWAY);
//            }])
            ->with('variants')
            ->get();

        return ProductResource::collection($products);
    }

    //TODO:: add request class
    public function removeItem($id, Request $request, PutawayService $service)
    {
        $item = PutawayItem::whereHas('putaway', function ($query) use ($request) {
            $query->where('warehouse_id', $request->warehouse_id);
        })
            ->with('putaway')->findOrFail($id);
        $putaway = $item->putaway;
        $service->removeItem($item);
        return new PutawayResource($putaway);
    }
}
