<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\Utils;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\App\V1\Picklist\PicklistItemResource;
use Niyotail\Http\Resources\App\V1\Picklist\PicklistResource;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Services\PicklistService;

class PicklistController extends Controller
{
    public function index(Request $request)
    {
        $picker = auth()->user();
        $picklistQuery = Picklist::where('warehouse_id', $request->warehouse_id)->with('orderItems', 'items')
            ->where('picker_id', $picker->id);
        if ($request->has('status')) {
            $picklistQuery->where('status', $request->status);
        }
        $picklists = $picklistQuery->paginate(15);
        return PicklistResource::collection($picklists);
    }

    public function items($id, Request $request)
    {
        $picklist = Picklist::with('orderItems.order.store')->where('warehouse_id', $request->warehouse_id)->find($id);
        if (empty($picklist)) {
            return response()->json(['message' => 'Invalid Picklist', 'error' => 'Invalid picklist!'], 422);
        }
        if ($picklist->status == Picklist::STATUS_CLOSED) {
            return response()->json(['message' => 'Picklist Closed', 'error' => 'Picklist already closed!'], 422);
        }
        $meta = [];
        foreach ($picklist->orderItems->pluck('order')->unique() as $order) {
            $meta[] = ['order_id' => $order->reference_id, 'store_name' => $order->store->name];
        }
        $picklistItemQuery = PicklistItem::getLocationWiseItemsForApi($id);

        if ($request->has('status')) {
            $picklistItemQuery->where('picklist_items.status', $request->status);
        }

        $items = $picklistItemQuery
            ->groupBy('products.id', 'storages.id')
            ->get();

        $picklistItems = Utils::paginate($items);
        return PicklistItemResource::collection($picklistItems)->additional(['meta' => $meta]);
    }

    public function pickItem($id, Request $request, PicklistService $service)
    {
        $picker = auth()->user();
        if ($picker->picklists->whereNotIn('id', $id)->where('status', Picklist::STATUS_PICKING)->count() > 0) {
            return response()->json([
                'message' => 'Please complete the previous picklist first!',
                'error' => 'Please complete the previous picklist first!'
            ], 422);
        }
        $picklist = Picklist::with('items')->find($id);
        if (empty($picklist)) {
            return response()->json(['message' => 'Invalid Picklist', 'error' => 'Invalid picklist!'], 422);
        }
        $skipData = $request->has('skip') ? $request->skip : [];
        try {
            $service->processItems($picklist, $request->product_id, $request->quantity, $skipData, $request->storage_id);
        } catch (ServiceException $exception) {
            return response()->json(['message' => $exception->getMessage(), 'error' => $exception->getMessage()], 422);
        }
        $picklist->refresh();
        $isComplete = $picklist->items->where('status', PicklistItem::STATUS_PENDING)->count() == 0;
        return response()->json(['message' => 'Picked the product', 'is_complete' => $isComplete], 200);
    }


    public function removeItem($id, Request $request, PicklistService $service)
    {
        $picklist = Picklist::find($id);
        if (empty($picklist)) {
            return response()->json(['message' => 'Invalid Picklist', 'error' => 'Invalid picklist!'], 422);
        }
        try {
            $service->removeItems($picklist, $request->product_id,$request->storage_id);
        } catch (ServiceException $exception) {
            return response()->json(['message' => $exception->getMessage(), 'error' => $exception->getMessage()], 422);
        }
        $picklistItem = PicklistItem::join('order_items', 'order_items.id', '=', 'picklist_items.order_item_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('storages', 'picklist_items.storage_id', '=', 'storages.id')
            ->leftJoin('product_variants', function ($join) {
                $join->on('product_variants.product_id', '=', 'products.id')
                    ->where('product_variants.value', 'unit');
            })
            ->where('picklist_items.picklist_id', $picklist->id)
            ->where('products.id', $request->product_id)
            ->where('picklist_items.storage_id', $request->storage_id)
            ->selectRaw('count(distinct(Case when picklist_items.status = "pending" then picklist_items.id End)) as pending_quantity, count(distinct(Case when picklist_items.status = "picked" then picklist_items.id End)) as picked_quantity, count(distinct(Case when picklist_items.status = "skipped" then picklist_items.id End)) as skipped_quantity, products.id as product_id, product_variants.mrp as mrp, products.barcode as barcode, products.name as product, storages.label as location, storages.id as storage_id')
            ->first();

        return (new PicklistItemResource($picklistItem));
    }

    public function search($id, Request $request)
    {
        $barcode = $request->query('barcode');

        $picklistItems = PicklistItem::join('order_items', 'picklist_items.order_item_id', '=', 'order_items.id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->leftJoin('rack_product', function ($join) {
                $join->on('rack_product.product_id', '=', 'products.id')
                    ->where('rack_product.status', 1);
            })
            ->leftJoin('product_variants', function ($join) {
                $join->on('product_variants.product_id', '=', 'products.id')
                    ->where('product_variants.value', 'unit');
            })
            ->leftJoin('warehouse_racks', 'rack_product.rack_id', '=', 'warehouse_racks.id')
            ->selectRaw('count(distinct(Case when picklist_items.status = "pending" then picklist_items.id End)) as pending_quantity, count(distinct(Case when picklist_items.status = "picked" then picklist_items.id End)) as picked_quantity, count(distinct(Case when picklist_items.status = "skipped" then picklist_items.id End)) as skipped_quantity, products.id as product_id, product_variants.mrp as mrp, products.barcode as barcode, products.name as product, GROUP_CONCAT(distinct(warehouse_racks.reference)) as location')
            ->where('picklist_items.picklist_id', $id)
            ->where('products.barcode', $barcode)
            ->groupBy('order_items.sku')
            ->paginate();

        return PicklistItemResource::collection($picklistItems);
    }

    public function markPicked($id, Request $request, PicklistService $service)
    {
        $picklist = Picklist::with('items')->find($id);
        if (empty($picklist)) {
            return response()->json(['message' => 'Invalid Picklist', 'error' => 'Invalid picklist!'], 422);
        }
        if ($picklist->status == Picklist::STATUS_PICKED) {
            return response()->json(['message' => 'Picklist already marked as Picked!', 'error' => 'Picklist already marked as Picked!'], 422);
        }
        if (!$picklist->canMarkPicked()) {
            return response()->json([
                'message' => 'Picklist cannot be marked as picked! Please process all items in picklist first!',
                'error' => 'Picklist cannot be marked as picked! Please process all items in picklist first!'
            ], 422);
        }
        try {
            $picklist = $service->updatePicklistStatus($picklist, Picklist::STATUS_PICKED);
        } catch (ServiceException $exception) {
            return response()->json(['message' => $exception->getMessage(), 'error' => $exception->getMessage()], 422);
        }
        return (new PicklistResource($picklist));
    }

    public function skipReasons(Request $request)
    {
        $data = PicklistItem::getConstants('SKIP');
        return response()->json(['data' => $data]);
    }
}