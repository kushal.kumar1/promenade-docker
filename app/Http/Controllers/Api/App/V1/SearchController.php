<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\Request;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Http\Resources\App\V1\StorageResource;
use Niyotail\Models\Product;
use Niyotail\Models\Reason;
use Niyotail\Models\Storage;

class SearchController extends Controller
{
    public function storage(Request $request)
    {
        $storages = Storage::where('warehouse_id', $request->warehouse_id)
            ->where('label', 'like', "%$request->term%")
            ->paginate(10);
        return StorageResource::collection($storages);
    }

    public function reasons(Request $request)
    {
        $reasonSource = $request->reasonSource;
        $reasonType = $request->reasonType;

        $reasons = Reason::where('reason_text', 'like', "%$request->term%");

        if(!empty($reasonSource))
            $reasons->where('source_type', $reasonSource);
        if(!empty($reasonType))
            $reasons->where('reason_type', $reasonType);

        return ReasonResource::collection($reasons->paginate(10));
    }

    public function product(Request $request)
    {
        if ($request->filled('productId')) {
            return new ProductResource(Product::findOrFail($request->productId));
        }
        if ($request->filled('barcode')) {
            return new ProductResource(Product::where('barcode', 'like', "%$request->barcode")->first());
        }
        return null;
    }
}
