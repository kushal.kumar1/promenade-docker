<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Niyotail\Contracts\Authentication\EmployeeAuthContract;
use Niyotail\Helpers\Authentication\EmployeeAuthHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\App\V1\Auth\LoginRequest;
use Niyotail\Http\Requests\Api\App\V1\Auth\SendOtpRequest;
use Niyotail\Http\Resources\App\V1\EmployeeResource;
use Niyotail\Models\Employee;

class AuthController extends Controller implements EmployeeAuthContract
{
    public function sendOtp(SendOtpRequest $request, EmployeeAuthHelper $authHelper)
    {
        return $authHelper->sendOtp($request, $this);
    }

    public function login(LoginRequest $request, EmployeeAuthHelper $authHelper)
    {
        return $authHelper->execute($request, $this);
    }

    public function employeeLogInFailed($message): JsonResponse
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function employeeIsBlocked(): JsonResponse
    {
        return response()->json(['message' => "You are blocked", 'error' => "Employee is Blocked !!"], 422);
    }

    public function employeeHasLoggedOut()
    {
        // TODO: Implement employeeHasLoggedOut() method.
    }

    public function employeeLoggedIn(Employee $employee, $token)
    {
        $employee->loadMissing('warehouses');
        $data = new EmployeeResource($employee);
        return response()->json(['data' => $data, 'token' => $token]);
    }

    public function otpFailed($message): JsonResponse
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function otpSuccessfullySent()
    {
        return response()->json(['message' => "Otp Sent Successfully"]);
    }

    public function userHasLoggedOut()
    {
        // TODO: Implement userHasLoggedOut() method.
    }
}
