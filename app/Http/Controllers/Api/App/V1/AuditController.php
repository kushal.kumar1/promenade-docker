<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\App\V1\Audit\AuditItemResource;
use Niyotail\Http\Resources\App\V1\Audit\AuditResource;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\ReasonResource;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemMeta;
use Niyotail\Models\Product;
use Niyotail\Models\Reason;
use Niyotail\Models\Storage;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;

class AuditController extends Controller
{
    public function listAudits(Request $request)
    {
        $employee = Auth::guard('employee_api')->user();

        $audits = Audit::all()->filter(function ($audit) use ($employee) {
            return AuditService::canViewAudit($audit, $employee);
        });

        if (empty($audits))
            return response()->json($audits);

        return AuditResource::collection($audits);
    }

    public function listAuditItems(Audit $audit, Request $request)
    {
        $employee = Auth::guard('employee_api')->user();

        $query = $audit->auditItems();
        if ($audit->type == Audit::TYPE_PRODUCT && !is_null(Product::find($request->productId))) {
            $query->whereProduct($request->productId);
        }
        if ($audit->type == Audit::TYPE_STORAGE && Storage::find($request->storageId)->exists()) {
            $query->whereStorage($request->storageId);
        }

        $auditItems = $query->with('product', 'storage', 'assignedTo', 'auditItemMetas', 'auditItemActions')->get()->filter(function ($auditItem) use ($employee) {
            return AuditItemService::canViewAuditItem($auditItem, $employee);
        });

        if (empty($auditItems))
            return response()->json($auditItems);

        return AuditItemResource::collection($auditItems);
    }

    public function getAudit(Audit $audit, Request $request)
    {
        $employee = Auth::guard('employee_api')->user();

        if (!AuditService::canViewAudit($audit, $employee))
            return response()->json(['error' => 'Not authorized.'], 403);

        if (empty($audit))
            return response()->json($audit);

        return new AuditResource($audit);
    }

    public function getAuditItem(Audit $audit, AuditItem $auditItem, Request $request)
    {
        $employee = Auth::guard('employee_api')->user();

        if (!AuditItemService::canViewAuditItem($auditItem, $employee))
            return response()->json(['error' => 'Not authorized.'], 403);

        if (empty($auditItem))
            return response()->json($auditItem);

        return new AuditItemResource($auditItem);
    }

    public function extraInventory(Audit $audit, Request $request)
    {
        $employee = Auth::guard('employee_api')->user();

        $inputs = $request->json()->get('inputs');
        $productId = $request->json()->get('productId');
        $productBarcode = $request->json()->get('productBarcode');
        if ($audit->type == Audit::TYPE_STORAGE)
            $storageId = $request->json()->get('storageId');
        if ($audit->type == Audit::TYPE_PRODUCT) {
            $storageLabel = $request->json()->get('storageId');
            $storageId = Storage::where('warehouse_id', $audit->warehouse_id)
                ->where('label', $storageLabel)
                ->firstOrFail()
                ->id;
        }

        if ($audit->auditItems()
                ->notCancelled()
                ->whereStorage($storageId)
                ->whereProduct($productId)
                ->count() > 0)
            throw new ServiceException();

        DB::transaction(function () use ($employee, $storageId, $productId, $audit, $inputs) {
            $auditItem = AuditItemService::createAuditItem($audit->id, $productId, $storageId, 'EXTRA INVENTORY FOUND');
            $auditItem->saveOrFail();
            $goodInput = 0;

            $auditItem->assigned_to = $employee->id;
            $auditItem->status = AuditItem::STATUS_PENDING_APPROVAL;
            $auditItem->saveOrFail();

            foreach ($inputs as $input) {
                switch ($input['condition']) {
                    case AuditItemMeta::CONDITION_GOOD:
                        $goodInput++;
                        if ($goodInput > 1)
                            throw new ServiceException();

                        $auditItemMeta = new AuditItemMeta();
                        $auditItemMeta->audit_item_id = $auditItem->id;
                        $auditItemMeta->condition = AuditItemMeta::CONDITION_GOOD;
                        $auditItemMeta->quantity = $input['quantity'];
                        $auditItemMeta->reason_id = $input['reasonId'] ?? null;
                        $auditItemMeta->remarks = $input['remarks'] ?? null;

                        $auditItemMeta->saveOrFail();
                        break;
                    case AuditItemMeta::CONDITION_BAD:
                        $auditItemMeta = new AuditItemMeta();
                        $auditItemMeta->audit_item_id = $auditItem->id;
                        $auditItemMeta->condition = AuditItemMeta::CONDITION_BAD;
                        $auditItemMeta->quantity = $input['quantity'];
                        $auditItemMeta->reason_id = $input['reasonId'];
                        $auditItemMeta->remarks = $input['remarks'] ?? null;

                        $auditItemMeta->saveOrFail();
                        break;
                    default:
                        throw new ServiceException();
                }
            }
        });

        return response()->json();
    }

    public function input(Audit $audit, AuditItem $auditItem, Request $request)
    {
        if ($auditItem->status != AuditItem::STATUS_ASSIGNED || !AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_PENDING_APPROVAL))
            throw new ServiceException();

        $auditItem->auditItemMetas()->delete();

        $inputs = $request->json()->get('inputs');
        DB::transaction(function () use ($audit, $request, $auditItem, $inputs) {
            if (is_null($auditItem->storage_id)) {
                $storageLabel = $request->json()->get('storageLabel');
                if (empty($storageLabel))
                    throw new ServiceException("Required Parameter 'storageLabel' not provided");

                $storage = Storage::where('warehouse_id', $audit->warehouse_id)->where('label', $storageLabel)->first();
                if (empty($storage))
                    throw new ServiceException("Invalid Parameter 'storageLabel' provided");

                $newAuditItem = new AuditItem();
                $newAuditItem->audit_id = $auditItem->audit_id;
                $newAuditItem->product_id = $auditItem->product_id;
                $newAuditItem->storage_id = $storage->id;
                $newAuditItem->assigned_to = $auditItem->assigned_to;
                $newAuditItem->system_quantity = $auditItem->system_quantity;

                $newAuditItem->save();
                $auditItem = $newAuditItem;
            }

            $goodInput = 0;
            foreach ($inputs as $input) {
                switch ($input['condition']) {
                    case AuditItemMeta::CONDITION_GOOD:
                        $goodInput++;
                        if ($goodInput > 1)
                            throw new ServiceException();

                        $auditItemMeta = new AuditItemMeta();
                        $auditItemMeta->audit_item_id = $auditItem->id;
                        $auditItemMeta->condition = AuditItemMeta::CONDITION_GOOD;
                        $auditItemMeta->quantity = $input['quantity'];
                        $auditItemMeta->reason_id = $input['reasonId'] ?? null;
                        $auditItemMeta->remarks = $input['remarks'] ?? null;

                        $auditItemMeta->saveOrFail();
                        break;
                    case AuditItemMeta::CONDITION_BAD:
                        $auditItemMeta = new AuditItemMeta();
                        $auditItemMeta->audit_item_id = $auditItem->id;
                        $auditItemMeta->condition = AuditItemMeta::CONDITION_BAD;
                        $auditItemMeta->quantity = $input['quantity'];
                        $auditItemMeta->reason_id = $input['reasonId'];
                        $auditItemMeta->remarks = $input['remarks'] ?? null;

                        $auditItemMeta->saveOrFail();
                        break;
                    default:
                        throw new ServiceException();
                }
            }

            if ($auditItem->auditItemMetas()->whereCondition(AuditItemMeta::CONDITION_GOOD)->count() == 0)
                throw new ServiceException();

            $auditItem->status = AuditItem::STATUS_PENDING_APPROVAL;
            $auditItem->saveOrFail();
        });
    }

    public function submit(Audit $audit, AuditItem $auditItem, Request $request)
    {
    }

    public function getProduct(Request $request)
    {
        return new ProductResource(Product::find($request->productId));
    }

    public function searchProducts(Request $request)
    {
        return new ProductResource(Product::where('barcode', 'like', "%$request->barcode")->first());
    }
}
