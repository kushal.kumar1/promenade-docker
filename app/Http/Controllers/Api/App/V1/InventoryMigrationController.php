<?php

namespace Niyotail\Http\Controllers\Api\App\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\App\V1\InventoryMigration\MigrationRequest;
use Niyotail\Http\Requests\Api\App\V1\InventoryMigration\AddItemRequest;
use Niyotail\Http\Requests\Api\App\V1\InventoryMigration\UpdateItemRequest;
use Niyotail\Http\Requests\Api\App\V1\InventoryMigration\PlaceItemRequest;
use Niyotail\Http\Resources\App\V1\InventoryMigrationResource;
use Niyotail\Http\Resources\App\V1\ProductResource;
use Niyotail\Http\Resources\App\V1\PutawayResource;
use Niyotail\Http\Resources\App\V1\PutawayItemResource;
use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Models\Product;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;
use Niyotail\Services\InventoryMigrationService;
use Niyotail\Services\Putaway\PutawayService;

class InventoryMigrationController extends Controller
{
    public function index(MigrationRequest $request)
    {
        $employee = Auth::guard('employee_api')->user();
        $employeeId = $employee->id;
        $migrations = InventoryMigration::getMigrations($request->warehouse_id, $employeeId)->get();
        return InventoryMigrationResource::collection($migrations);
    }

    public function detail($id, MigrationRequest $request)
    {
        $employee = Auth::guard('employee_api')->user();
        $employeeId = $employee->id;
        $migration = InventoryMigration::getMigrations($request->warehouse_id, $employeeId, $id)->first();
        return !empty($migration) ? new InventoryMigrationResource($migration) : ['data' => null];
    }

    public function searchProduct($id, Request $request)
    {
        $term = $request->term;
        $products = Product::where(function ($q) use ($term) {
            $q->where('name', 'like', "%$term%")->orWhere('id', $term)->orWhere('barcode', 'like', "%$term%");
        })->whereHas('migrationItems', function ($migrationQuery) use ($id, $request) {
            $migrationQuery->where('inventory_migration_id', $id);
            if (!empty($request->type) && $request->type == 'cart') {
                $migrationQuery->whereIn('status', [InventoryMigrationItem::STATUS_PROCESSING, InventoryMigrationItem::STATUS_PARTIAL]);
            } else {
                $migrationQuery->where('status', InventoryMigrationItem::STATUS_PENDING);
            }
        })->get();

        return ProductResource::collection($products);
    }

    public function updateCartItem($id, UpdateItemRequest $request, PutawayService $putawayService)
    {
        $employee = Auth::guard('employee_api')->user();
        $employeeId = $employee->id;

        $putawayItem = $putawayService->updatePutawayItem($employeeId, $request->putaway_item_id, $request->quantity);
        $putawayItem->loadMissing('product', 'storage');
        return (new PutawayItemResource($putawayItem));
    }

    public function placeItem($id, PlaceItemRequest $request, PutawayService $putawayService)
    {
        $employee = Auth::guard('employee_api')->user();
        $employeeId = $employee->id;

        $putawayItem = PutawayItem::with('putaway')->where('putaway_id', $id)->find($request->putaway_item_id);
        if (empty($putawayItem)) {
            return response()->json(['error'=>'Invalid item. This item is not assigned!']);
        }

        if ($putawayItem->putaway->employee_id != $employeeId) {
            return response()->json(['error' => 'Unauthorized place item request!'], 422);
        }

        if (!empty($putawayItem->storage_id) && $putawayItem->storage_id != $request->destination_storage_id) {
            return response()->json(['error'=>'Destination Storage is not valid!']);
        }

        $excessAreaPutawayItem = $putawayService->placeItem($putawayItem, $request->placed_quantity, $request->destination_storage_id);
        $putawayItem->refresh();
        $putawayItem->loadMissing('product', 'storage');

        $additional = ['excess_area_item' => null];
        if (!empty($excessAreaPutawayItem)) {
            $excessAreaPutawayItem->loadMissing('product', 'storage');
            $additional['excess_area_item'] = (new PutawayItemResource($excessAreaPutawayItem));
        }
        return (new PutawayItemResource($putawayItem))->additional($additional);
    }

    public function addItemToCart(AddItemRequest $request, InventoryMigrationService $service)
    {
        $employee = Auth::guard('employee_api')->user();
        $putaway = $service->addItemToCart($request, $employee);
        $putaway->load('items.product', 'items.storage');
        return new PutawayResource($putaway);
    }
}
