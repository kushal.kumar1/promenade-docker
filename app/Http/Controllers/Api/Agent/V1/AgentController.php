<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Agent;
use Niyotail\Models\DashboardImage;
use Niyotail\Helpers\Image;
use Niyotail\Models\Product;
use Niyotail\Http\Requests\Api\Agent\V1\DeviceParameterRequest;
use Niyotail\Services\AgentService;

class AgentController extends Controller
{
    public function storeDeviceParameters(DeviceParameterRequest $request, AgentService $agentService) {
        $agent = Auth::guard('agent_api')->user();
        abort_if(empty($agent), 404);
        $agentService->storeDeviceParameters($request, $agent);
        return response()->json(['success' => true]);
    }

    public function dashboard(Request $request) {
        $agent = Auth::guard('agent_api')->user();
        abort_if(empty($agent), 404);
        $agent_id = $agent->id;
        //$agent_id = 1;
        $todayAgentData = Agent::getTodaySalesData($agent_id);
        $monthWiseAgentData = Agent::getMonthwiseSalesData($agent_id, $request);
        $startDate = date('Y-m-01 00:00:00');
        $startDateMargin = date('Y-m-08 00:00:00');
        $endDate = (new DateTime())->setTimestamp(strtotime('+1 days'))
                ->setTimezone(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d 00:00:00');
        $currentMonthSKUData = Product::getCurrentMonthTopSKUData($agent_id, $startDate, $endDate);
        $currentMonthSKUDataForMargin = Product::getCurrentMonthTopSKUDataForMargin($agent_id, $startDateMargin, $endDate);

        $todayData = $this->todayAgentData($todayAgentData);
        $thisMonthData = $this->thisMonthAgentData($monthWiseAgentData, $currentMonthSKUData, $currentMonthSKUDataForMargin, $startDate, $endDate);
        $monthWiseData = $this->monthwiseAgentData($monthWiseAgentData);
        $dashboardImage = DashboardImage::active()->whereRaw('month(valid_from) = month(now())')->orderBy('created_at', 'desc')->first();
        $dashboardImage = !empty($dashboardImage) ? Image::getSrc($dashboardImage) : null;
        $data = ['today' => $todayData, 'current_month' => $thisMonthData, 'monthwise' => $monthWiseData, 'dashboard_image' => $dashboardImage];
        return response()->json(['data' => $data]);
    }

    private function todayAgentData($todayAgentData) {
      $todayData['sales'] = empty($todayAgentData) ? 0 : $todayAgentData->orders->sum('total');
      $todayData['orders'] = empty($todayAgentData) ? 0 : $todayAgentData->orders->count();
      $todayData['stores'] = empty($todayAgentData) ? 0 : count(array_unique($todayAgentData->orders->pluck('store_id')->all()));
        return $todayData;
    }

    private function monthwiseAgentData($monthWiseAgentData) {
        if ($monthWiseAgentData->isEmpty()) {
            return null;
        }
        $monthWiseData = [];
        foreach($monthWiseAgentData as $monthWise) {
            $monthWiseData[$monthWise->date]['sales'] = $monthWise->total;
            $monthWiseData[$monthWise->date]['orders'] = $monthWise->order_count;
            $monthWiseData[$monthWise->date]['stores'] = $monthWise->store_count;
        }
        return $monthWiseData;
    }

    private function thisMonthAgentData($monthWiseAgentData, $currentMonthSKUData, $currentMonthSKUDataForMargin, $startDate, $endDate) {
        $thisMonth = date('Y-m');
        $thisMonthData = $monthWiseAgentData->where('date', $thisMonth)->first();
        if (empty($thisMonthData)) {
            return null;
        }
        $dateDiff = strtotime($endDate) - strtotime($startDate);
        $dateDiff = abs(round($dateDiff / 86400));

        $monthData['sales'] = $thisMonthData->total;
        $monthData['orders'] = $thisMonthData->order_count;
        $monthData['stores'] = $thisMonthData->store_count;
        $monthData['average_orders_per_day'] = ($dateDiff > 0) ? round($thisMonthData->order_count/$dateDiff) : $thisMonthData->order_count;
        $monthData['top_skus'] = [];
        if ($currentMonthSKUData->isNotEmpty()) {
            $monthData['top_skus'] = $currentMonthSKUData->toArray();
        }

        $monthData['top_margin_skus'] = [];
        if ($currentMonthSKUDataForMargin->isNotEmpty()) {
            $monthData['top_margin_skus'] = $currentMonthSKUDataForMargin->toArray();
        }
        return $monthData;
    }
}
