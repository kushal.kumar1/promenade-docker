<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Queue;
use Niyotail\Contracts\Authentication\UserAuthContract;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Niyotail\Http\Resources\Agent\V1\InvoiceResource;
use Niyotail\Http\Resources\V1\Users\UserResource;
use Niyotail\Jobs\SendOtpToCustomer;
use Niyotail\Models\Invoice;
use Niyotail\Models\Store;
use Niyotail\Models\User;
use Niyotail\Services\UserService;

class InvoiceController extends Controller
{
    public function getPdf($id)
    {
        $invoice = Invoice::find($id);
        abort_if(empty($invoice), 404);
        return response()->file($invoice->invoice_url);
    }

    public function getPdfUrl($invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        abort_if(empty($invoice), 404);
        return response()->json(['data' => $invoice->invoice_url]);
    }
}
