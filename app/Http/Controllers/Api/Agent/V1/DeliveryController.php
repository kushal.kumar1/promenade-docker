<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Delivery\MarkDeliveredRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Delivery\ReturnRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Delivery\RetryRTORequest;
use Niyotail\Http\Requests\Api\Agent\V1\Delivery\GetInvoiceRequest;
use Niyotail\Http\Resources\Agent\V1\Shipment\ShipmentResource;
use Niyotail\Models\Invoice;
use Niyotail\Models\Manifest;
use Niyotail\Models\Shipment;
use Niyotail\Models\Agent;
use Niyotail\Services\Order\ShipmentService;
use Niyotail\Services\ReturnOrder\ReturnOrderService;
use Illuminate\Support\Facades\Auth;


class DeliveryController extends Controller
{
    public function getShipments(Request $request)
    {
        $agent = Auth::guard('agent_api')->user();
        if (!empty($agent) && $agent->type === 'delivery') {
                $shipments = Shipment::with(['order' => function ($q) use ($request) {
                    $q->with('shippingAddress');
                    $q->with(['store' => function ($query) use ($request) {
                        $query->with('users', 'beat', 'creditLimit');
                    }]);
                }])
                ->with('invoice')
                ->with(['orderItems' => function ($query) {
                    $query->canReturn();
                }])
                ->whereHas('manifests', function ($query) use ($agent) {
                    $query->where('agent_id', $agent->id)
                        ->where('status', Manifest::STATUS_DISPATCHED);
                })
                ->whereIn('shipments.status', [Shipment::STATUS_DISPATCHED, Shipment::STATUS_DELIVERED])
                ->orderByRaw('FIELD(shipments.status, "dispatched", "delivered")')
                ->orderByDistance($request->lat, $request->lng)
                ->paginate(30);

            return ShipmentResource::collection($shipments)->additional([
                'return_reasons' => config('settings.return_reasons'),
                'rto_reasons' => config('settings.return_reasons'),
                'retry_reasons' => config('settings.return_reasons')
            ]);
        } else {
            return [];
        }
    }

    public function getInvoice(GetInvoiceRequest $request)
    {
        $shipment = Shipment::with(['order' => function ($q) use ($request) {
            $q->with('shippingAddress');
            $q->with(['store' => function ($query) use ($request) {
                $query->with('users', 'beat', 'creditLimit');
            }]);
        }])
        ->with('orderItems', 'invoice')
        ->whereHas('invoice', function ($query) use ($request) {
            $query->where('reference_id', $request->reference_id);
        })->first();

        return (new ShipmentResource($shipment))->additional([
            'return_reasons' => config('settings.return_reasons'),
            'rto_reasons' => config('settings.return_reasons'),
            'retry_reasons' => config('settings.return_reasons')
        ]);
    }

    public function getPendingDeliveriesCount(Request $request)
    {
        $agent = Auth::guard('agent_api')->user();
        if (!empty($agent) && $agent->type === 'delivery') {
            $count = Shipment::whereHas('manifests', function ($query) use ($agent) {
                $query->where('agent_id', $agent->id)
                    ->where('status', Manifest::STATUS_DISPATCHED);
            })
            ->whereIn('status', [Shipment::STATUS_DISPATCHED, Shipment::STATUS_DELIVERED])
            ->count();
            return $count;
        } else {
            return 0;
        }
    }

    public function deliver($id, MarkDeliveredRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($id);
        if (!empty($shipment)) {
            try{
                $shipmentService->verifyOtp($shipment, $request->otp);
                $shipmentService->updateShipment($shipment, Shipment::STATUS_DELIVERED);
                $shipment->loadMissing('order.shippingAddress', 'order.store.users', 'order.store.beat', 'order.store.creditLimit', 'invoice');
            }catch (ServiceException $exception){
                return response()->json(['message' => $exception->getMessage(), 'error' => $exception->getMessage()],422);
            }
            return new ShipmentResource($shipment);
        } else {
            return response()->json(['message' => 'Shipment does not exist', 'error' => 'Shipment does not exist']);
        }
    }

    public function return($id, ReturnRequest $request, ReturnOrderService $returnOrderService)
    {
        $agent = Auth::guard('agent_api')->user();
        $newItems = [];
        foreach($request->items as $item) {
          $variant = explode('-', $item['sku']);
          $skuType = $variant[1];
          $item[$skuType] = $item['quantity'];
          $newItems [] = $item;
        }

        $request->merge(['shipment_id' => $id, 'items' => $newItems]);
        $order = $returnOrderService->setAuthority(ReturnOrderService::AUTHORITY_AGENT)->create($request, $agent);
        //return response()->json($order);
        return response()->json(['success' => 'true']);
    }

    public function rto($id, RetryRTORequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($id);
        if (!empty($shipment)) {
            $shipmentService->updateShipment($shipment, Shipment::STATUS_PENDING_RTO, $request->reason);
            $shipment->loadMissing('order.shippingAddress', 'order.store.users', 'order.store.beat', 'order.store.creditLimit', 'invoice');
            return new ShipmentResource($shipment);
        } else {
            return response()->json(['message' => 'Shipment does not exist', 'error' => 'Shipment does not exist']);
        }
    }

    public function retry($id, RetryRTORequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($id);
        if (!empty($shipment)) {
            $shipmentService->updateShipment($shipment, Shipment::STATUS_PENDING_RETRY, $request->reason);
            $shipment->loadMissing('order.shippingAddress', 'order.store.users', 'order.store.beat', 'order.store.creditLimit', 'invoice');
            return new ShipmentResource($shipment);
        } else {
            return response()->json(['message' => 'Shipment does not exist', 'error' => 'Shipment does not exist']);
        }
    }

    public function sendOTP($id, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($id);
        if (!empty($shipment)) {
            $shipmentService->resendOTP($shipment);
            $shipment->loadMissing('order.shippingAddress', 'order.store.users', 'order.store.beat', 'order.store.creditLimit', 'invoice');
            return new ShipmentResource($shipment);
        } else {
            return response()->json(['message' => 'Shipment does not exist', 'error' => 'Shipment does not exist']);
        }
    }

    public function uploadPod($id, Request $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($id);
        if(!empty($shipment)) {
            $file = $request->file('pod');
            $shipmentService->savePOD($file, $shipment);
            $shipment->loadMissing('order.shippingAddress', 'order.store.users', 'order.store.beat', 'order.store.creditLimit', 'invoice');
            return new ShipmentResource($shipment);
        }
        else {
            return response()->json(['message' => 'Shipment does not exist', 'error' => 'Shipment does not exist']);
        }
    }
}
