<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Niyotail\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Requests\Api\Agent\V1\Order\CreateRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Order\UpdateRequest;
use Niyotail\Http\Resources\Agent\V1\Order\OrderResource;
use Niyotail\Models\Cart;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\StoreOrder\StoreOrderService;

class OrderController extends Controller
{
    public function store(CreateRequest $request, StoreOrderService $service)
    {
        die;
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->where('store_id', $request->store_id)->with('items.product')->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart', 'error' => 'Empty Cart'], 422);
        }

//        $cart->merge();
        $order = $service->create($cart);
        $order = StoreOrder::with('items','store.creditLimit')->find($order->id);
        //TODO:: create new store order resource and need app updates basis that
        return new OrderResource($order);
    }

    public function update($id, UpdateRequest $request, StoreOrderService $service)
    {
        $storeOrder = StoreOrder::findOrFail($request->id);
        $storeOrder = $service->cancel($request);
        return new OrderResource($storeOrder);
    }
}
