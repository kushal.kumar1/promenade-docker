<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Niyotail\Http\Resources\Agent\V1\BannerResource;
use Niyotail\Http\Resources\Agent\V1\BrandResource;
use Niyotail\Http\Resources\Agent\V1\CollectionResource;
use Niyotail\Http\Resources\Agent\V1\TopProductsResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;
use Niyotail\Http\Resources\Agent\V1\AgentTargetStats\AgentStoreStatResource;
use Niyotail\Http\Resources\Agent\V1\AgentTargetStats\StoreStatResource;
use Niyotail\Http\Resources\Agent\V1\AppSection\AppSectionResource;
use Niyotail\Models\Brand;
use Niyotail\Models\AppBanner;
use Niyotail\Models\AppSection;
use Illuminate\Support\Carbon;
use Niyotail\Models\Collection;
use Niyotail\Models\Agent;
use Niyotail\Models\Store;
use Illuminate\Support\Facades\Auth;
use Niyotail\Helpers\AppHomeHelper;

class StaticController extends Controller
{
    public function home()
    {
        //$brands=Brand::whereHas('products')->orderBy('id', 'desc')->get();
        $brands =  Brand::getAllVisible();
        $collections=Collection::getAllVisibleRoots();
        $appBanners = AppBanner::active()
        ->where(function ($q) {
            $q->where('valid_from', '<=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
              ->where('valid_to', '>=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
              ->orWhere('valid_from', null);
        })
        ->orderBy('position')
        ->get();
        $brandResources=BrandResource::collection($brands);
        $collectionResources=CollectionResource::collection($collections);
        $bannerResources=BannerResource::collection($appBanners);
        return response()->json(['data'=>['brands'=>$brandResources,'collections'=>$collectionResources,'banners'=>$bannerResources]]);
    }

    public function stats($fromDate, $toDate = null, $agent=null)
    {
        if(empty($agent))
            $agent = Auth::guard('agent_api')->user();
        $agentId = $agent->id;
        $agent = Agent::where('id', $agentId)
                      // ->with(['targets' => function ($query) use($fromDate) {
                      //   $query->whereNull('store_id')->where('valid_from', '<=', $fromDate)
                      //           ->where(function ($subQuery) use ($fromDate) {
                      //               $subQuery->whereNull('valid_to')->orWhere('valid_to', '>=', $fromDate);
                      //           });
                      // }])
                      ->first();
        $dayFactor = Carbon::parse($fromDate)->daysInMonth; //Considering 4 holidays/sundays in month
        if (empty($toDate)) {
            $toDate = $fromDate;
            $dayFactor = 1;
        }

        $targets = ['sale' => null, 'unique_stores' => null, 'unique_products' => null, 'points' => null];
        // if ($agent->targets->isNotEmpty()) {
        //     $agentTarget = $agent->targets->first();
        //     $targets['sale'] = !empty($agentTarget->values['sale']) ? ($agentTarget->values['sale'] * $dayFactor) : null;
        //     $targets['unique_stores'] = !empty($agentTarget->values['unique_stores']) ? ($agentTarget->values['unique_stores'] * $dayFactor) : null;
        //     $targets['unique_products'] = !empty($agentTarget->values['unique_products']) ? ($agentTarget->values['unique_products'] * $dayFactor) : null;
        //     $targets['points'] = !empty($agentTarget->values['points']) ? ($agentTarget->values['points'] * $dayFactor) : null;
        // }
        $sale['title'] = "Sales";
        $ordered = 0;//(new Sale($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_ORDERED)->output();
        $invoiced = 0;//(new Sale($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_INVOICED)->output();
        $delivered = 0;//(new Sale($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_DELIVERED)->output();
        $sale['value']['ordered'] = '&#8377; '. number_format($ordered);
        $sale['value']['invoiced'] = '&#8377; '. number_format($invoiced);
        $sale['value']['delivered'] = '&#8377; '. number_format($delivered);
        $sale['target'] = !empty($targets['sale']) ?  '&#8377; '. number_format($targets['sale']) : null;
        if ($fromDate != $toDate) {
          $sale['target_achieved'] = !empty($targets['sale']) ?  ($delivered > $targets['sale']) : null;
        } else {
          $sale['target_achieved'] = !empty($targets['sale']) ?  ($ordered > $targets['sale']) : null;
        }


        $uSAPOrdered = ['unique_stores' => 0, 'unique_products' => 0];//(new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_ORDERED)->output();
        $uSAPOrderedInvoiced = ['unique_stores' => 0, 'unique_products' => 0];//(new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_INVOICED)->output();
        $uSAPOrderedDelivered = ['unique_stores' => 0, 'unique_products' => 0];//(new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_DELIVERED)->output();

        $stores['title'] = "Unique Stores";
        $stores['value']['ordered'] = $uSAPOrdered['unique_stores'];
        $stores['value']['invoiced'] = $uSAPOrderedInvoiced['unique_stores'];
        $stores['value']['delivered'] = $uSAPOrderedDelivered['unique_stores'];
        $stores['target'] = $targets['unique_stores'];

        $products['title'] = "Unique Products";
        $products['value']['ordered'] =  $uSAPOrdered['unique_products'];
        $products['value']['invoiced'] = $uSAPOrderedInvoiced['unique_products'];
        $products['value']['delivered'] =  $uSAPOrderedDelivered['unique_products'];
        $products['target'] = $targets['unique_products'];

        $point['title'] = "Points";
        $point['value']['ordered'] = 0;//(new Point($fromDate, $toDate, $agent))->setType(Point::TYPE_ORDERED)->output();
        $point['value']['invoiced'] = 0;//(new Point($fromDate, $toDate, $agent))->setType(Point::TYPE_INVOICED)->output();
        $point['value']['delivered'] = 0;//(new Point($fromDate, $toDate, $agent))->setType(Point::TYPE_DELIVERED)->output();
        $point['target'] = $targets['points'];

        $stats = collect([$sale, $stores, $products, $point]);

        $topProducts = collect();//(new TopProductList($fromDate, $toDate))->setAgent($agent)->setType(Sale::TYPE_DELIVERED)->output();

        return response()->json(['data' => ['stats' => $stats, 'topProducts' => TopProductsResource::collection($topProducts)]]);
    }

    public function storeStats(Request $request)
    {
        return StoreStatResource::collection(collect());
        $agent = Auth::guard('agent_api')->user();
        $type = 'month';
        if (empty($request->from_date)) {
          $request->merge(['from_date' => date('Y-m-01')]);
          $request->merge(['to_date' => date('Y-m-d')]);
        }
        else if (empty($request->to_date)) {
            $request->merge(['to_date' => $request->from_date]);
            $type = 'day';
        }
        $request->merge(['per_page' => empty($request->per_page) ? 5 : $request->per_page]);
        if ($type == 'day') {
          $storeStats = Store::active()->verified()
                              ->with(['beat' => function($query) use ($request, $agent) {
                                  $query->with(['agentBeatDays' => function($subQuery) use ($request, $agent) {
                                      $subQuery->where('agent_id', $agent->id)->agentBeat($request->from_date);
                                  }]);
                              }])
                              ->with(['targets' =>function ($query) use($request, $agent) {
                                  $query->where('agent_id', $agent->id)->where('valid_from', '<=', $request->from_date)
                                        ->where(function ($subQuery) use ($request) {
                                            $subQuery->whereNull('valid_to')->orWhere('valid_to', '>=', $request->from_date);
                                        });
                              }])
                              ->whereHas('beat', function($query) use ($request, $agent) {
                                  $query->whereHas('agentBeatDays', function($subQuery) use ($request, $agent) {
                                    $subQuery->where('agent_id', $agent->id)->agentBeat($request->from_date);
                                  });
                              })
                              ->with('creditLimit', 'users')->paginate($request->per_page);
        } else {
            $storeStats = Store::active()->verified()
                                // ->whereHas('beat', function($query) use ($request) {
                                //     $query->whereHas('agentBeatDays', function($subQuery) use ($request) {
                                //       $subQuery->where('agent_id', 2)->agentAllBeats($request->from_date, $request->to_date);
                                //     });
                                // })
                                ->whereHas('orderItemMetas', function($query) use ($request, $agent) {
                                    $query->where('agent_id', $agent->id)->whereHas('orderItem', function($subQuery) use ($request) {
                                      $subQuery->whereRaw("date(convert_tz(created_at, '+00:00', '+05:30')) between '$request->from_date' and '$request->to_date'");
                                    });
                                })
                                ->with(['targets' =>function ($query) use($request, $agent) {
                                    $query->where('agent_id', $agent->id)->where(function($query) use($request) {
                                        $query->where(function($subQuery) use($request) {
                                          $subQuery->where('valid_from', '<=', $request->from_date)
                                                  ->where(function ($subQuery) use ($request) {
                                                     $subQuery->whereNull('valid_to')->orWhere('valid_to', '>=', $request->from_date);
                                                   });
                                        })
                                        ->orWhere(function($subQuery) use($request) {
                                          $subQuery->where('valid_from', '<=', $request->to_date)
                                                  ->where(function ($subQuery) use ($request) {
                                                     $subQuery->whereNull('valid_to')->orWhere('valid_to', '>=', $request->to_date);
                                                   });
                                        });
                                    });
                                }])
                                ->with('creditLimit', 'users')->paginate($request->per_page);
        }

        $storeStats->map(function ($storeStat) use ($agent, $request) {
            $storeStat['stats'] = $this->getStoreStatsWithTarget($storeStat, $agent, $request->from_date, $request->to_date);
            return $storeStat;
        });
        return StoreStatResource::collection($storeStats);
    }

    private function getStoreStatsWithTarget($store, $agent, $fromDate, $toDate)
    {
        $targets = ['sale' => null];
        // if ($store->targets->isNotEmpty()) {
        //     $dayFactor = 1;
        //     if ($toDate != $fromDate) {
        //         $dayFactor = Carbon::parse($fromDate)->daysInMonth; //Considering 4 holidays/sundays in month
        //     }
        //     $agentStoreTargets = $store->targets;
        //     foreach($agentStoreTargets as $agentStoreTarget) {
        //       $targets['sale'] += $agentStoreTarget->values['sale'];
        //     }
        //     $targets['sale'] = $targets['sale'] * $dayFactor;
        // }

        $saleData['ordered'] = (new Sale($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_ORDERED)->output();
        $saleData['invoiced'] = (new Sale($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_INVOICED)->output();
        $saleData['delivered'] = (new Sale($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_DELIVERED)->output();
        $saleData['returned'] = (new Sale($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_RETURNED)->output();

        $storeTargetStats = ['target_achieved' => null, 'is_order_placed' => null];
        if ($fromDate != $toDate) {
          if(!empty($targets['sale'])) {
            $storeTargetStats['target_achieved'] = ($targets['sale'] > $saleData['delivered']['total']);
          }
        } else {
          $storeTargetStats['is_order_placed'] = ($saleData['ordered']['total'] > 0);
        }

        $cardStat['title'] = "Order Value";
        $cardStat['value'] = '&#8377; '. number_format($saleData['ordered']['total']);
        $cardStats[] = $cardStat;
        if (!empty($targets['sale'])) {
          $cardStat['title'] = "Store Reveue Target";
          $cardStat['value'] = '&#8377; '. number_format($targets['sale']);
          $cardStats[] = $cardStat;
          $cardStat['title'] = "Achieved Target";
          $cardStat['value'] = round(($saleData['ordered']['total'] / $targets['sale']) * 100). ' %';
          $cardStats[] = $cardStat;
        }

        $detailStats = [];
        $sale['title'] = "Revenue Amount";
        $sale['value'] = '&#8377; '. number_format($saleData['invoiced']['total'] - $saleData['returned']['total']);
        $detailStats[] = $sale;

        $return['title'] = "Return Amount";
        $return['value'] = '&#8377; '. number_format($saleData['returned']['total']);
        $detailStats[] = $return;

        $orderCount['title'] = "Order Count";
        $orderCount['value'] = $saleData['delivered']['order_count'];
        $detailStats[] = $orderCount;

        // $sale['title'] = "Sales";
        // $sale['value']['ordered'] = $saleData['ordered']['total'];
        // $sale['value']['invoiced'] = $saleData['invoiced']['total'];
        // $sale['value']['delivered'] = $saleData['delivered']['total'];
        // $sale['value']['returned'] = $saleData['returned']['total'];
        // $sale['target'] = $targets['sale'];

        // $orderCount['title'] = "Order Count";
        // $orderCount['value']['ordered'] = $saleData['ordered']['order_count'];;
        // $orderCount['value']['invoiced'] = $saleData['invoiced']['order_count'];
        // $orderCount['value']['delivered'] = $saleData['delivered']['order_count'];
        // $orderCount['target'] = null;



        $uSAPOrdered = (new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_ORDERED)->output();
        $uSAPOrderedInvoiced = (new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_INVOICED)->output();
        $uSAPOrderedDelivered = (new UniqueStoreAndProduct($fromDate, $toDate))->setAgent($agent)->setStore($store)->setType(Sale::TYPE_DELIVERED)->output();

        $products['title'] = "SKUs Purchased";
        if ($fromDate != $toDate) {
          $products['value'] =  $uSAPOrderedInvoiced['unique_products'];
        } else {
          $products['value'] =  $uSAPOrdered['unique_products'];
        }
        $detailStats[] = $products;

        // $products['title'] = "Unique Products";
        // $products['value']['ordered'] =  $uSAPOrdered['unique_products'];
        // $products['value']['invoiced'] = $uSAPOrderedInvoiced['unique_products'];
        // $products['value']['delivered'] =  $uSAPOrderedDelivered['unique_products'];
        // $products['target'] = null;

        $brands['title'] = "Brands Purchased";
        if ($fromDate != $toDate) {
          $brands['value'] =  $uSAPOrderedInvoiced['unique_brands'];
        } else {
          $brands['value'] =  $uSAPOrdered['unique_brands'];
        }
        $detailStats[] = $brands;

        // $brands['title'] = "Unique Brands";
        // $brands['value']['ordered'] =  $uSAPOrdered['unique_brands'];
        // $brands['value']['invoiced'] = $uSAPOrderedInvoiced['unique_brands'];
        // $brands['value']['delivered'] =  $uSAPOrderedDelivered['unique_brands'];
        // $brands['target'] = null;
        //
        // $stats = collect([$sale, $orderCount, $products, $brands]);
        $stats = collect(['target_achieved' => $storeTargetStats['target_achieved'], 'is_order_placed' => $storeTargetStats['is_order_placed'], 'dashboard' => $cardStats, 'detail' => $detailStats]);
        return $stats;
    }

    public function dashboard(Request $request, AppHomeHelper $appHomeHelper)
    {
        $explore = $appHomeHelper->getAppSections($request, AppSection::MEDIUM_AGENT);
        return AppSectionResource::collection($explore);
    }

    public function brands(Request $request, AppHomeHelper $appHomeHelper)
    {
        $brands = Brand::getAllVisible();
        return BrandResource::collection($brands);
    }

    public function categories(Request $request, AppHomeHelper $appHomeHelper)
    {
        $collections = Collection::getAllVisible();
        return CollectionResource::collection($collections);
    }
}
