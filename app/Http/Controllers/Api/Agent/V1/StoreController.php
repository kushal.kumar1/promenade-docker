<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Store\CreateRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Store\IndexRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Store\UpdateRequest;
use Illuminate\Http\Request;
use Niyotail\Http\Resources\Agent\V1\Order\OrderResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;
use Niyotail\Models\Order;
use Niyotail\Models\Store;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\User;
use Niyotail\Services\StoreService;
use Niyotail\Services\UserService;

class StoreController extends Controller
{
    public function index(IndexRequest $request)
    {
        $stores = Store::verified()->active()->with('users', 'totalBalance', 'beat', 'creditLimit', 'totalPendingBalance')
            ->orderByDistance($request->latitude, $request->longitude)->paginate($request->per_page);
        return StoreResource::collection($stores);
    }

    public function store(CreateRequest $request, StoreService $service, UserService $userService)
    {
        $store = $service->setAuthority(StoreService::AUTHORITY_AGENT)->create($request);
        // Add user from the contact info of store
        $user = User::where('mobile', $request->contact_mobile)->first();
        if (empty($user)) {
            $userRequest = new Request();
            $userRequest->merge(['mobile' => $request->contact_mobile]);
            $userRequest->merge([
                'first_name' => split_name($request->contact_person)[0],
                'last_name' => split_name($request->contact_person)[1]
            ]);
            $user = $userService->create($userRequest);
        }
        $store->users()->attach($user->id);
        return new StoreResource($store);
    }

    public function update($id, UpdateRequest $request, StoreService $service)
    {
        $store = Store::find($id);
        abort_if(empty($store), 404);
        $store = $service->setAuthority(StoreService::AUTHORITY_AGENT)->update($request, $store);
        $store->loadMissing('users', 'creditLimit');
        return new StoreResource($store);
    }

    public function show(Request $request, $id)
    {
        $store = Store::find($id);
        abort_if(empty($store), 404);
        $store->load('beat', 'creditLimit', 'users');
        $store->append('credit_limit_value');
        return new StoreResource($store);
    }

    public function orders(Request $request, $id)
    {
        $agent = Auth::guard('agent_api')->user();
        $orders = StoreOrder::with(['items.product' => function ($query) {
            $query->with('images', 'brand');
        }])
            ->with('store.beat', 'store.creditLimit')
            ->createdByUser($agent)
            ->where('store_id', $id)
            ->orderBy('id', 'desc')
            ->paginate($request->get('per_page', 10));
        return OrderResource::collection($orders);
    }
}
