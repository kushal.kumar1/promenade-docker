<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Niyotail\Http\Requests\Api\Agent\V1\Transaction\CreateRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Transaction\LedgerRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Transaction\SettingsRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Transaction\TransactionCreateRequest;
use Niyotail\Http\Resources\Agent\V1\BankResource;
use Niyotail\Http\Resources\Agent\V1\TransactionResource;
use Niyotail\Http\Resources\Agent\V1\TransactionLedgerResource;
use Niyotail\Http\Resources\Agent\V1\UserResource;
use Carbon\Carbon;
use Niyotail\Models\Bank;
use Niyotail\Models\Store;
use Niyotail\Models\Transaction;
use Niyotail\Models\WarehouseScope;
use Niyotail\Services\TransactionService;

class TransactionController extends Controller
{
    public function storeTransaction(TransactionCreateRequest $request,TransactionService $transactionService)
    {
        if (!empty($request->cheque_issuer) && !ctype_digit($request->cheque_issuer)) {
            $bank = Bank::where('name', $request->cheque_issuer)->first();
            if (empty($bank)) {
                throw new \Exception('Error Issuer bank');
            }
            $chequeIssuer = $bank->id;
            $request->merge(['cheque_issuer' => $chequeIssuer]);
        }
        $transaction = $transactionService->setAuthority(TransactionService::AUTHORITY_AGENT)->storeTransaction($request, Auth::guard('agent_api')->user());
        return response()->json(['success' => !empty($transaction) ? true : false]);
    }

    public function getLedgerForStore(LedgerRequest $request, $storeId)
    {
        $transaction = Transaction::where('store_id', '=', $storeId)
            ->with(['invoices'=>function($invoiceQuery){
                $invoiceQuery->withoutGlobalScope(WarehouseScope::class);
            }]);

        $format = "d M, Y";
        if ($request->has('start_date')) {
          $startDate = Carbon::createFromFormat($format, $request->start_date)->toDateString();
          $transaction = $transaction->where('created_at', '>=', $startDate);
        }

        if ($request->has('end_date')) {
            $endDate = Carbon::createFromFormat($format, $request->end_date)->toDateString();
            $transaction = $transaction->where('created_at', '<=', $endDate);
        }

        $endingBalance = 0;

        $endingBalanceTransaction = $transaction->orderBy('created_at', 'DESC')->first();
        if (!empty($endingBalanceTransaction)) {
            $endingBalance = $endingBalanceTransaction->balance;
        }

        $transactions = $transaction->paginate($request->per_page);

        return TransactionLedgerResource::collection($transactions)
            ->additional(['endingBalance' => $endingBalance]);
    }

    public function settings(SettingsRequest $request)
    {
        $store = Store::where('id', $request->store_id)->with(['users' => function($subQuery) {
                      $subQuery->where('status', 1);
                  }])->first();

        $paymentMethods = Transaction::MAP_AGENT_PAYMENT_METHODS;
        // $paymentMethods[Transaction::TYPE_CREDIT] = array_filter($paymentMethods[Transaction::TYPE_CREDIT], function ($elem) {
        //    return strpos($elem, '1k_') === false;
        // });
        if (empty($request->header('X-version'))) {
            $bankResource = config('payments.bank_names');
        } else {
            $banks = Bank::active()->get();
            $bankResource = BankResource::collection($banks);
        }

        return UserResource::collection($store->users)
                  ->additional(['payment_methods' => $paymentMethods, 'bank_names' => $bankResource]);
    }

    public function getPendingTransactions($id, Request $request)
    {
        //\DB::enableQueryLog();
        $transactions = Transaction::toBeSettled()->where('store_id',$id)
                              ->with('settledAmount')->orderBy('id', 'desc')->paginate($request->per_page);
        //dd($transactions);
        //dd(\DB::getQueryLog());
        return TransactionResource::collection($transactions);
    }
}
