<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Niyotail\Helpers\Filters\FilterHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Product\ListingRequest;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;
use Niyotail\Http\Resources\Agent\V1\Products\ProductResource;
use Niyotail\Models\Cart;
use Niyotail\Models\Product;
use Niyotail\Models\Brand;
use Niyotail\Models\Store;
use Niyotail\Models\Tag;
use Niyotail\Helpers\RuleEngine;
use Niyotail\Models\Expression;
use Niyotail\Models\Warehouse;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\ProductService;

class ProductController extends Controller
{
    private function getAllBrands($brands)
    {
        $brandIds = [];
        foreach ($brands as $id) {
            $children = Brand::getChildren($id)->pluck('id')->toArray();
            $brandIds = array_merge($brandIds, $children);
        }

        return $brandIds;
    }

    public function index(ListingRequest $request)
    {
        if ($request->has('brands')) {
            $brandIds = $this->getAllBrands($request->brands);
            $request->merge(['brandIds' => $brandIds]);
        }

        if ($request->has('store_id')) {
            $store = Store::find($request->store_id);
            $request->merge(['beat_id' => $store->beat_id]);

            $warehouses = Warehouse::whereHas('beats', function ($q) use ($request) {
                            $q->where('beat_id', $request->beat_id);
                          })->get();
            $warehouseIds = $warehouses->pluck('id')->toArray();
            if (!empty($warehouseIds)) {
                $request->merge(['warehouse_id' => $warehouseIds]);
            }
        } else {
            $store = null;
            $warehouseIds = null;
        }

        $filterHelper = new FilterHelper();
        $filteredProducts = $filterHelper->getFilteredProducts($request->merge(['forAgentApi' => 1]));

        $productQuery = Product::whereIn('products.id', $filteredProducts['productIds'])
            //->apiFilters($request)
            ->with('brand', 'images', 'variants')
            ->with(['tags' => function($query) {
                $query->whereNotIn('name', ['UFML', 'Group Buying']);
            }])
            ->withStockByWarehouse(2)
            ->selectRaw('products.*')
            ->join('inventories', 'inventories.product_id', '=', 'products.id');

        /*if (!empty($warehouseIds)) {
            $productQuery = $productQuery->whereIn('inventories.warehouse_id', $warehouseIds);
        } else {
            $productQuery = $productQuery->whereNotIn('inventories.warehouse_id', ['2']);
        }*/

        $orderByPid = implode(',', $filteredProducts['productIds']);
        if (!empty($orderByPid)) {
          $productQuery = $productQuery->orderByRaw("FIELD(products.id, $orderByPid)");
        }
        $products = $productQuery
            ->groupBy('product_id')
            ->get();

        if (!empty($store)) {
            $beatId = $store->beat_id;
            $products->map(function ($product) use ($beatId, $warehouseIds) {
                $product['beatId'] = $beatId;
                $product['warehouse_id'] = $warehouseIds;
                $this->addTags($product);
                return $product;
            });
        }

        // Include cart status with all variants of products
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->with('items')->where('store_id', $request->store_id)->first();
        $products = CartService::processProductsForCartItem($products, $cart);

//        if(!empty($products) && $products->isNotEmpty()) {
//            RuleEngine::processProductsForRules($products, $store, Expression::MEDIUM_RETAILER);
//        }

        $products = ProductService::processProductToHideVariantsForUom($products);

        $productsPaginator = new LengthAwarePaginator($products, $filteredProducts['numFound'], ($filteredProducts['rows'] === 0 ? 1 : $filteredProducts['rows']), $filteredProducts['currentPage'], ['path' => urldecode($request->url())]);

        return ProductListingResource::collection($productsPaginator)->additional([
            'meta' => ['filters' => $filteredProducts['filters']]
        ]);
    }

    private function addTags($product)
    {
        if ($product->allow_back_orders)
        {
            $jitTag = new Tag();
            $jitTag->id = -1;
            $jitTag->name = 'JIT';
            $product->tags->push($jitTag);
        }

        if ($product->focussed)
        {
            $focussedTag = new Tag();
            $focussedTag->id = -2;
            $focussedTag->name = 'FOCUSSED';
            $product->tags->push($focussedTag);
        }
    }

    public function show(Request $request, $id)
    {
        $product = Product::with('brand', 'variants', 'images')
                    ->withStockByWarehouse(2)
                    ->has('variants')
                    ->find($id);

        abort_if(empty($product), 404, "Invalid Product Id");

        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->with('items')->where('store_id', $request->store_id)->first();
        $product = CartService::processProductsForCartItem(collect()->add($product), $cart)->first();
        $product = ProductService::processProductToHideVariantsForUom(collect()->add($product))->first();
//        RuleEngine::processProductForRules($product, $store, Expression::MEDIUM_RETAILER);
        return new ProductResource($product);
    }
}
