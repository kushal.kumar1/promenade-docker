<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Niyotail\Helpers\PaymentMethodHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\AddItemRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\PaymentMethodRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\ProductCartRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\RemoveItemRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\UpdateAddressRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\UpdateItemRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Cart\ViewRequest;
use Niyotail\Http\Resources\Agent\V1\Cart\CartResource;
use Niyotail\Http\Resources\Agent\V1\Cart\ProductVariantCartResource;
use Niyotail\Models\CartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\ProfileQuestion;
use Niyotail\Services\Cart\CartService;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Cart;

class CartController extends Controller
{
    public function addToCart(AddItemRequest $request, CartService $service)
    {
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->where('store_id', $request->store_id)->first();
        if (!empty($cart)) {
            $service = $service->setCart($cart);
        }
        $request->merge(['source' => Cart::SOURCE_AGENT_APP]);
        $cart = $service->addToCart($request, $agent);
        $cart = Cart::withApiData()->find($cart->id);
        return new CartResource($cart);
    }

    public function updateItem($id, UpdateItemRequest $request, CartService $service)
    {
        $cartItem = CartItem::with('cart')->find($id);
        abort_if(empty($cartItem), 404);
        if (!empty($cartItem->cart->order_id)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $request->request->add(['id' => $id]);
        $cart = $service->updateQuantity($request);
        $cart = Cart::withApiData()->find($cart->id);
        return new CartResource($cart);
    }

    public function productVariantCartUpdate(ProductCartRequest $request, CartService $service)
    {
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->where('store_id', $request->store_id)->first();
        $productVariant = ProductVariant::findOrFail($request->variant_id);
        $request->merge(['id' => $request->variant_id]);
        $request->merge(['source' => Cart::SOURCE_AGENT_APP]);
        if (!empty($cart)) {
            $service = $service->setCart($cart);
        }
        // Delete item from cart
        if($request->quantity == 0) {
            $cartItem = $cart->items->where('sku', $productVariant->sku)->first();
            if(!empty($cartItem)) {
                $request->request->add(['id' => $cartItem->id]);
                $cart = $service->removeItem($request);
                return new ProductVariantCartResource($cart);
            }
        }
        // Update item quantity in cart
        else {
            $cart = $service->addToCart($request, $agent);
            $cart = Cart::withApiData()->find($cart->id);
            $cartItem = $cart->items->where('sku', $productVariant->sku)->first();
            return new ProductVariantCartResource($cartItem);
        }
    }

    public function removeItem($id, RemoveItemRequest $request, CartService $service)
    {
        $cartItem = CartItem::with('cart')->find($id);
        abort_if(empty($cartItem), 404);
        if (!empty($cartItem->cart->order_id)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }

        $request->request->add(['id' => $id]);
        $cart = $service->removeItem($request);
        $cart = Cart::withApiData()->find($cart->id);
        if (empty($cart)) {
            return (json_encode(new \stdClass));
        }
        return new CartResource($cart);
    }

    public function updateAddress(UpdateAddressRequest $request, CartService $service)
    {
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->where('store_id', $request->store_id)->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $cart = $service->createOrUpdateAddress($cart);
        $cart = Cart::withApiData()->find($cart->id);
        return new CartResource($cart);
    }

    public function getCart(ViewRequest $request)
    {
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->withApiData()->where('store_id', $request->store_id)->first();
        if (empty($cart)) {
            return (json_encode(new \stdClass));
        }
//        $storeID = $request->store_id;
//        $questions = ProfileQuestion::active()
//                    ->with(['storeAnswers' => function($subQuery) use ($storeID){
//                      $subQuery->where('store_id', $storeID);
//                    }])->get();
//        $questionCount = $questions->count();
//        $storeAnswerCount = $questions->pluck('storeAnswers')->collapse()->count();
//        if ($storeAnswerCount < $questionCount) {
//          $cart['profiling_required'] = true;
//        } else {
//          $cart['profiling_required'] = false;
//        }

        $cart['profiling_required'] = false;
        return new CartResource($cart);
    }

    public function getPaymentMethods(PaymentMethodRequest $request)
    {
        $agent = Auth::guard('agent_api')->user();
        $cart = Cart::byAgent($agent)->withApiData()->where('store_id', $request->store_id)->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $data=[];
        $paymentMethods=PaymentMethodHelper::getPaymentMethods($cart);
        foreach ($paymentMethods as $name=>$paymentMethod) {
            $data[]=['type'=>$name,'label'=>$paymentMethod['label']];
        }
        return response()->json(['data' => $data]);
    }
}
