<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\StoreProfiling\CreateRequest;
use Niyotail\Http\Requests\Api\Agent\V1\StoreProfiling\UpdateRequest;
use Niyotail\Http\Resources\Agent\V1\StoreProfiling\ProfileQuestionResource;
use Niyotail\Models\ProfileQuestion;
use Niyotail\Models\StoreAnswer;
use Niyotail\Services\StoreProfilingService;

class StoreProfilingController extends Controller
{
    public function index(Request $request, $id) {
        $questions = ProfileQuestion::getQuestions($id);
        $answeredCount = StoreAnswer::where('store_id', $id)->groupBy('question_id')->get();
        return ProfileQuestionResource::collection($questions)->additional(['meta' => [
                    'answeredCount' => $answeredCount->count(),
                ]]);
    }

    public function store(CreateRequest $request, $id, StoreProfilingService $service)
    {
        $agent = Auth::guard('agent_api')->user();
        $optionIDs = $request->has('answer_id') ? $request->answer_id : null;
        $storeID = $id;
        $profileQuestion = ProfileQuestion::getQuestion($request->question_id, $storeID, $optionIDs);
        abort_if(empty($profileQuestion), 404);
        $request->merge(['store_id' => $storeID]);
        $service->create($request, $profileQuestion, $agent);
        return response(['success'=>'true'], 200);
    }

    public function update(UpdateRequest $request, $id, $question_id, StoreProfilingService $service)
    {
        $agent = Auth::guard('agent_api')->user();
        $optionIDs = $request->answer_id;
        $storeID = $id;
        $request->merge(['question_id' => $question_id,
                         'store_id' => $storeID]);
        $service->update($request, $agent);
        return response(['success'=>'true'], 200);
    }

    public function show(Request $request, $id, $question_id) {
        $profileQuestion = ProfileQuestion::getQuestion($question_id, $id);
        abort_if(empty($profileQuestion),404);
        return new ProfileQuestionResource($profileQuestion);
    }
}
