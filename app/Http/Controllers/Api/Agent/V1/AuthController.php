<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Illuminate\Http\Request;
use Niyotail\Contracts\Authentication\AgentAuthContract;
use Niyotail\Contracts\Authentication\EmployeeAuthContract;
use Niyotail\Helpers\Authentication\AgentAuthHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Auth\LoginRequest;
use Niyotail\Http\Requests\Api\Agent\V1\Auth\SendOtpRequest;
use Niyotail\Http\Resources\Agent\V1\AgentResource;
use Niyotail\Models\Agent;

class AuthController extends Controller implements AgentAuthContract
{
    public function login(LoginRequest $request, AgentAuthHelper $customerAuthHelper)
    {
        return $customerAuthHelper->execute($request, $this);
    }

    public function sendOtp(SendOtpRequest $request, AgentAuthHelper $customerAuthHelper)
    {
        return $customerAuthHelper->sendOtp($request, $this);
    }

    public function agentLogInFailed($message)
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function agentIsBlocked()
    {
        return response()->json(['message' => "You are blocked", 'error' => "Agent is Blocked !!"], 422);
    }

    public function agentHasLoggedOut()
    {
        // TODO: Implement agentHasLoggedOut() method.
    }

    public function otpFailed($message)
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function otpSuccessfullySent()
    {
        return response()->json(['message' => "Otp Sent Successfully"]);
    }

    public function agentLoggedIn(Agent $agent, $token)
    {
        $data = new AgentResource($agent);
        return response()->json(['data' => $data, 'token' => $token]);
    }
}
