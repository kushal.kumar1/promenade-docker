<?php

namespace Niyotail\Http\Controllers\Api\Agent\V1;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Agent\V1\Search\SearchRequest;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Requests\Api\Agent\V1\Search\StoreSearchRequest;
use Niyotail\Http\Resources\Agent\V1\BeatResource;
use Niyotail\Http\Resources\Agent\V1\Products\ProductListingResource;
use Niyotail\Http\Resources\Agent\V1\StoreResource;
use Niyotail\Models\Beat;
use Niyotail\Models\Store;
use Niyotail\Models\Warehouse;
use Niyotail\Services\ProductService;

class SearchController extends Controller
{
    public function products(SearchRequest $request, SearchHelper $searchHelper)
    {
//        $store = Store::find($request->store_id);
//        $beatId = $store->beat_id;
//        $request->merge(['beat_id' => $store->beat_id]);
//
//        $warehouses = Warehouse::whereHas('beats', function ($q) use ($request) {
//                        $q->where('beat_id', $request->beat_id);
//                      })->get();
//        $warehouseIds = $warehouses->pluck('id')->toArray();
//        if (!empty($warehouseIds)) {
//            $request->merge(['warehouse_id' => $warehouseIds]);
//        }

        $products = $searchHelper->searchProduct($request->keyword, $request);

//        $products->map(function ($product) use ($beatId, $warehouseIds) {
//            $product['beatId'] = $beatId;
//            $product['warehouse_id'] = $warehouseIds;
//            return $product;
//        });

        $products = ProductService::processProductToHideVariantsForUom($products);

        return ProductListingResource::collection($products);
    }

    public function beats(SearchRequest $request)
    {
        $term = $request->keyword;
        $beats = Beat::where("name", "like", "%$term%")->limit(10)->get();
        return BeatResource::collection($beats);
    }

    public function stores(StoreSearchRequest $request, SearchHelper $searchHelper)
    {
         $storeQuery = Store::verified()->with('beat', 'creditLimit')->active();
         if ($request->has('keyword')) {
             $term = $request->keyword;
             $storeQuery->where("name", "like", "%$term%")->orWhere('id', $term);
             if (is_numeric($term) && strlen($term) >= 4) {
                 $storeQuery->orWhereHas('users', function($query) use ($term) {
                   $query->where('mobile', 'like', "%$term%");
                 });
             }
         }
         if ($request->has('latitude') && $request->has('longitude')) {
             $storeQuery->where(function($subQuery) use($request) {
               $subQuery->whereDistance($request->latitude, $request->longitude, 5)
                     ->orWhereNull('lat')->orWhereNull('lng');
             });
         }
         $stores = $storeQuery->orderByRaw('ISNULL(lat), ISNULL(lng)')->limit(10)->get();
         /* Avoid using solr for store search */
//        $stores = $searchHelper->searchStores($request);
        return StoreResource::collection($stores);
    }
}
