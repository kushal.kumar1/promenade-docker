<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\Agent\V1\AgentResource;
use Niyotail\Models\Agent;

class AgentController extends Controller
{
    public function index(Request $request)
    {
        //$agents = Agent::active()->where('type', 'sales')->get();
        $agents = collect();
        return AgentResource::collection($agents);
    }
}
