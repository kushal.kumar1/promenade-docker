<?php
/**
 * Created by PhpStorm.
 * User: harisshah
 * Date: 20/05/19
 * Time: 7:27 PM
 */

namespace Niyotail\Http\Controllers\Api\V1;


use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\V1\PincodeResource;
use Niyotail\Models\Pincode;

class PincodeController extends Controller
{

    public function show()
    {
        $pincodes = Pincode::all();
        return PincodeResource::collection($pincodes);
    }

}