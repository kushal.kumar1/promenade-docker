<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Niyotail\Helpers\PaymentMethodHelper;
use Niyotail\Helpers\Payments\PaymentException;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Niyotail\Http\Requests\Api\V1\Order\CreateRequest;
use Niyotail\Http\Requests\Api\V1\Order\UpdateRequest;
use Niyotail\Http\Resources\V1\Order\OrderResource;
use Niyotail\Models\Cart;
use Niyotail\Models\Order;
use Niyotail\Services\Order\OrderRatingService;
use Niyotail\Services\Order\OrderService;

class OrderController extends Controller
{
    public function store(CreateRequest $request, OrderService $orderService)
    {
        throw new \Exception('User role not allowed to place order.');
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->where('store_id', $request->store_id)->with('items.product')->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart', 'error' => 'Empty Cart'], 422);
        }
        // if ($cart->total <= 10000) {
        //     return response()->json(['message' => 'Can not place order as order value is less than Rs 10,000.','error' => 'Can not place order as order value is less than Rs 10,000'], 422);
        // }
        // Validate Cart before creating order
        $cartErrors = $cart->validate();
        if (!empty($cartErrors)) {
            return response()->json(['message' => "Order cannot be created! Please check cart for issues",
                        'error' => 'Order cannot be created'], 422);
        }

        try {
            $request->merge(['source' => Order::SOURCE_APP]);
            $order = $orderService->setAuthority(OrderService::AUTHORITY_CUSTOMER)->create($request, $cart);
            if (strpos($order->store->beat->name, '1K') !== false) {
                $posStoreStatus = DB::select(DB::raw("select status from pos.stores where branding=$order->store_id"));
                $orderID = $order->id;
                if (!empty($posStoreStatus) && $posStoreStatus[0]->status == 1) {
                  Artisan::call("order:mrp_update $orderID");
                }
            }
        } catch (\Exception $exception) {
            return response()->json(['message' => "Order cannot be created!".$exception->getMessage(), 'error' => 'Order cannot be created'], 422);
        }

        $order = Order::withApiData()->find($order->id);
        return new OrderResource($order);
    }

    public function update($id, UpdateRequest $request, OrderService $orderService)
    {
        $request->merge(['id' => $id]);
        $order = $orderService->cancel($request->id, $request->reason);
        return new OrderResource($order);
    }

    public function saveRating($id, Request $request, OrderRatingService $orderRatingService)
    {
        $request->merge(['order_id' => $id]);
        $orderRatingService->create($request);
        return response()->json(['success' => 'true']);
    }
}
