<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Requests\Api\V1\Store\CreateRequest;
use Niyotail\Http\Requests\Api\V1\Store\UpdateRequest;
use Niyotail\Http\Requests\Api\V1\Store\ViewOrderRequest;
use Niyotail\Http\Requests\Api\V1\Store\ViewTransactionRequest;
use Niyotail\Http\Resources\V1\Order\OrderResource;
use Niyotail\Http\Resources\V1\StoreResource;
use Niyotail\Http\Resources\V1\TransactionLedgerResource;
use Niyotail\Models\Invoice;
use Niyotail\Models\Order;
use Niyotail\Models\Store;
use Niyotail\Models\Transaction;
use Niyotail\Services\StoreService;
use Niyotail\Services\StoreCreditService;
use Niyotail\Http\Controllers\Controller;
use Carbon\Carbon;

class StoreController extends Controller
{
    public function store(CreateRequest $request, StoreService $service)
    {
        $user = Auth::guard('api')->user();
        if (!$request->filled('contact_person')) {
            $request->merge(['contact_person' => $user->name]);
        }
        if (!$request->filled('contact_mobile')) {
            $request->merge(['contact_mobile' => $user->mobile]);
        }
        $store = $service->create($request);
        $store = $service->addDefaultUser($store, $user);
        return new StoreResource($store);
    }

    public function update($id, UpdateRequest $request, StoreService $service)
    {
        $store = Store::find($id);
        abort_if(empty($store), 404);
        $store = $service->update($request, $store);
        return new StoreResource($store);
    }

    public function orders($id, ViewOrderRequest $request)
    {
        $store = Store::find($id);
        abort_if(empty($store), 404);
        $orders = Order::withApiData()->where('store_id', $id)->orderBy('id', 'desc')->paginate($request->per_page);
        return OrderResource::collection($orders);
    }

    public function transactions($id, ViewTransactionRequest $request)
    {
        $transaction = Transaction::where('store_id', $id)->with('invoices');

        $format = "d M, Y";
        if ($request->has('start_date')) {
          $startDate = Carbon::createFromFormat($format, $request->start_date)->toDateString();
          $transaction = $transaction->where('created_at', '>=', $startDate);
        }

        if ($request->has('end_date')) {
            $endDate = Carbon::createFromFormat($format, $request->end_date)->toDateString();
            $transaction = $transaction->where('created_at', '<=', $endDate);
        }

        $endingBalance = 0;

        $endingBalanceTransaction = $transaction->orderBy('created_at', 'DESC')->first();
        if (!empty($endingBalanceTransaction)) {
            $endingBalance = $endingBalanceTransaction->balance;
        }

        $transactions = $transaction->paginate($request->per_page);

        return TransactionLedgerResource::collection($transactions)
            ->additional(['endingBalance' => $endingBalance]);
    }

    public function getInvoicePdf($id, $invoiceID)
    {
        $invoice = Invoice::find($invoiceID);
        abort_if(empty($invoice), 404);
        return response()->file($invoice->invoice_url);
    }

    public function requestCredit($id, Request $request, StoreCreditService $service)
    {
        $store = Store::find($id);
        abort_if(empty($store), 404);
        $user = Auth::guard('api')->user();
        try {
            $service->createCreditRequest($id, $user, $request);
        } catch (\Exception $exception) {
            return response()->json(['success' => 'false']);
        }
        return response()->json(['success' => 'true']);
    }
}
