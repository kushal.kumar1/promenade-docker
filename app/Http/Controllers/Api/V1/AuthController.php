<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Niyotail\Contracts\Authentication\UserAuthContract;
use Niyotail\Helpers\Authentication\UserAuthHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\V1\Auth\LoginRequest;
use Niyotail\Http\Requests\Api\V1\Auth\SendOtpRequest;
use Niyotail\Http\Resources\V1\Users\UserResource;
use Niyotail\Models\User;

class AuthController extends Controller implements UserAuthContract
{
    public function login(LoginRequest $request, UserAuthHelper $customerAuthHelper)
    {
        return $customerAuthHelper->execute($request, $this);
    }

    public function sendOtp(SendOtpRequest $request, UserAuthHelper $customerAuthHelper)
    {
        return $customerAuthHelper->sendOtp($request, $this);
    }

    public function userLogInFailed($message)
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function userIsBlocked()
    {
        return response()->json(['message' => "You are blocked", 'error' => "User is Blocked !!"], 422);
    }

    public function userHasLoggedOut()
    {
        // TODO: Implement userHasLoggedOut() method.
    }

    public function otpFailed($message)
    {
        return response()->json(['message' => $message, 'error' => $message], 422);
    }

    public function otpSuccessfullySent()
    {
        return response()->json(['message' => "Otp Sent Successfully"]);
    }

    public function userLoggedIn(User $customer, $token)
    {
        $stores = $customer->stores;
        foreach($stores as $store) {
          $store->load('totalBalance','totalPendingBalance');
        }

        $data = new UserResource($customer);
        return response()->json(['data' => $data, 'token' => $token]);
    }
}
