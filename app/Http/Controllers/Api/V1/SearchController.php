<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\V1\SearchRequest;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Resources\V1\Products\ProductListingResource;
use Niyotail\Helpers\RuleEngine;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Cart;
use Niyotail\Models\Expression;
use Niyotail\Models\Store;
use Niyotail\Models\Warehouse;
use Niyotail\Services\Cart\CartService;

class SearchController extends Controller
{
    public function search(SearchRequest $request, SearchHelper $searchHelper)
    {
        $store = Store::find($request->store_id);
        $beatId = $store->beat_id;
        $request->merge(['beat_id' => $store->beat_id]);

        // $warehouses = Warehouse::whereHas('beats', function ($q) use ($request) {
        //     $q->where('beat_id', $request->beat_id);
        // })->get();
        // $warehouseIds = $warehouses->pluck('id')->toArray();
        $warehouseIds = Utils::getAvailableWarehouseForStore($store);

        if (!empty($warehouseIds)) {
            $request->merge(['warehouse_id' => $warehouseIds]);
        }

        $products = $searchHelper->searchProduct($request->keyword, $request);
        $products->map(function ($product) use ($beatId, $warehouseIds) {
            $product['beatId'] = $beatId;
            $product['warehouse_id'] = $warehouseIds;
            return $product;
        });

        if(!empty($products) && $products->isNotEmpty()) {
            // Include cart status with all variants of products
            $user = Auth::guard('api')->user();
            $cart = Cart::byUser($user)->with('items')->where('store_id', $request->store_id)->first();
            $products = CartService::processProductsForCartItem($products, $cart);

            RuleEngine::processProducts($products, $store, Expression::MEDIUM_RETAILER);
            RuleEngine::processProductsForRules($products, $store, Expression::MEDIUM_RETAILER);
        }
        return ProductListingResource::collection($products);
    }
}
