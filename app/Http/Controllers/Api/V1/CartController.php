<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\PaymentMethodHelper;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Niyotail\Http\Requests\Api\V1\Cart\ProductCartRequest;
use Niyotail\Http\Requests\Api\V1\Cart\AddItemRequest;
use Niyotail\Http\Requests\Api\V1\Cart\PaymentMethodRequest;
use Niyotail\Http\Requests\Api\V1\Cart\RemoveItemRequest;
use Niyotail\Http\Requests\Api\V1\Cart\UpdateAddressRequest;
use Niyotail\Http\Requests\Api\V1\Cart\UpdateItemRequest;
use Niyotail\Http\Requests\Api\V1\Cart\ViewRequest;
use Niyotail\Http\Resources\V1\Cart\ProductVariantCartResource;
use Niyotail\Http\Resources\V1\Cart\CartResource;
use Niyotail\Models\CartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Services\Cart\CartService;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Cart;
use Niyotail\Helpers\RuleEngine;

class CartController extends Controller
{
    public function addToCart(AddItemRequest $request, CartService $service)
    {
        throw new \Exception('User role not allowed to place order.');
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->where('store_id', $request->store_id)->first();
        if (!empty($cart)) {
            $service = $service->setCart($cart);
        }
        $cart = $service->addToCart($request, $user);
        $cart = Cart::withApiData()->find($cart->id);
        RuleEngine::processCart($cart);
        $cart['items'] = $cart->items->map(function ($item) use ($cart) {
            $item['product'] = CartService::processProductsForCartItem(collect()->add($item->product), $cart)->first();
            return $item;
        });
        return new CartResource($cart);
    }

    public function updateItem($id, UpdateItemRequest $request, CartService $service)
    {
        $cartItem = CartItem::with('cart')->find($id);
        abort_if(empty($cartItem), 404);
        if (!empty($cartItem->cart->order_id)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $request->request->add(['id' => $id]);
        $cart = $service->updateQuantity($request);
        $cart = Cart::withApiData()->find($cart->id);
        RuleEngine::processCart($cart);
        $cart['items'] = $cart->items->map(function ($item) use ($cart) {
            $item['product'] = CartService::processProductsForCartItem(collect()->add($item->product), $cart)->first();
            return $item;
        });
        return new CartResource($cart);
    }

    public function removeItem($id, RemoveItemRequest $request, CartService $service)
    {
        $cartItem = CartItem::with('cart')->find($id);
        abort_if(empty($cartItem), 404);
        if (!empty($cartItem->cart->order_id)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }

        $request->request->add(['id' => $id]);
        $cart = $service->removeItem($request);
        $cart = Cart::withApiData()->find($cart->id);
        if (empty($cart) || $cart->items->isEmpty()) {
            return (json_encode(new \stdClass));
        }
        RuleEngine::processCart($cart);
        $cart['items'] = $cart->items->map(function ($item) use ($cart) {
            $item['product'] = CartService::processProductsForCartItem(collect()->add($item->product), $cart)->first();
            return $item;
        });
        return new CartResource($cart);
    }

    public function updateAddress(UpdateAddressRequest $request, CartService $service)
    {
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->where('store_id', $request->store_id)->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $cart = $service->createOrUpdateAddress($cart);
        $cart = Cart::withApiData()->find($cart->id);
        $cart['items'] = $cart->items->map(function ($item) use ($cart) {
            $item['product'] = CartService::processProductsForCartItem(collect()->add($item->product), $cart)->first();
            return $item;
        });
        return new CartResource($cart);
    }

    public function getCart(ViewRequest $request)
    {
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->withApiData()->where('store_id', $request->store_id)->first();
        if (empty($cart) || $cart->items->isEmpty()) {
            return (json_encode(new \stdClass));
        }
        RuleEngine::processCart($cart);
        $cart['items'] = $cart->items->map(function ($item) use ($cart) {
            $item['product'] = CartService::processProductsForCartItem(collect()->add($item->product), $cart)->first();
            return $item;
        });
        return new CartResource($cart);
    }

    public function getPaymentMethods(PaymentMethodRequest $request)
    {
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->withApiData()->where('store_id', $request->store_id)->first();
        if (empty($cart)) {
            return response()->json(['message' => 'Empty Cart','error' => 'Empty Cart'], 422);
        }
        $data=[];
        $paymentMethods=PaymentMethodHelper::getPaymentMethods($cart);
        foreach ($paymentMethods as $name=>$paymentMethod) {
            $data[]=['type'=>$name,'label'=>$paymentMethod['label'],'description'=>$paymentMethod['description']];
        }
        return response()->json(['data' => $data]);
    }

    public function productVariantCartUpdate(ProductCartRequest $request, CartService $service)
    {
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->where('store_id', $request->store_id)->first();
        $productVariant = ProductVariant::findOrFail($request->variant_id);
        $request->merge(['id' => $request->variant_id]);
        if (!empty($cart)) {
            $service = $service->setCart($cart);
        }
        // Delete item from cart
        if($request->quantity == 0) {
            $cartItem = $cart->items->where('sku', $productVariant->sku)->first();
            $request->request->add(['id' => $cartItem->id]);
            $cart = $service->removeItem($request);
            return new ProductVariantCartResource($cart);
        }
        // Update item quantity in cart
        else {
            $cart = $service->addToCart($request, $user);
            $cart = Cart::withApiData()->find($cart->id);
            RuleEngine::processCart($cart);
            $cartItem = $cart->items->where('sku', $productVariant->sku)->first();
            return new ProductVariantCartResource($cartItem);
        }
    }
}
