<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\V1\User\OnBoardingRequest;
use Niyotail\Http\Requests\Api\V1\User\UpdateRequest;
use Niyotail\Http\Requests\Api\V1\User\ViewOrderRequest;
use Niyotail\Http\Requests\Api\V1\User\FeedbackRequest;
use Niyotail\Http\Resources\V1\Order\OrderResource;
use Niyotail\Http\Resources\V1\Users\UserResource;
use Niyotail\Http\Resources\V1\Products\ProductListingResource;
use Niyotail\Http\Resources\V1\Products\ProductResource;
use Niyotail\Models\Order;
use Niyotail\Models\Product;
use Niyotail\Services\UserService;
use Niyotail\Services\UserFeedbackService;
use Niyotail\Models\User;
use Niyotail\Http\Requests\Api\V1\User\DeviceParameterRequest;

class UserController extends Controller
{
    public function profile(Request $request)
    {
        $user = Auth::guard('api')->user();
        $user->load('stores');
        return new UserResource($user);
    }

    public function update($id, UpdateRequest $request, UserService $service)
    {
        $user = User::find($id);
        $user = $service->update($request, $user);
        $user->load('stores');
        return new UserResource($user);
    }

    public function onBoarding($id, OnBoardingRequest $request, UserService $service)
    {
        $user = User::find($id);
        $user = $service->onBoarding($request, $user);
        $user->load('stores');
        return new UserResource($user);
    }

    public function feedback($id, FeedbackRequest $request, UserFeedbackService $service)
    {
        $userFeedback = $service->create($request);
        return response()->json(['data'=> ['success' => true]]);
    }

    public function storeDeviceParameters(DeviceParameterRequest $request, UserService $userService) {
        $user = Auth::guard('api')->user();
        abort_if(empty($user), 404);
        $userService->storeDeviceParameters($request, $user);
        return response()->json(['success' => true]);
    }
}
