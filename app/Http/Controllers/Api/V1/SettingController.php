<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\V1\SettingResource;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        return new SettingResource(collect());
    }
}
