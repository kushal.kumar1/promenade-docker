<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Support\Facades\View;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\V1\BannerResource;
use Niyotail\Http\Resources\V1\CollectionResource;
use Niyotail\Http\Resources\V1\BrandResource;
use Niyotail\Http\Resources\V1\AppSection\AppSectionResource;

use Niyotail\Models\Brand;
use Niyotail\Models\Collection;
use Niyotail\Models\AppBanner;
use Niyotail\Models\AppSection;
use Niyotail\Helpers\AppHomeHelper;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class StaticController extends Controller
{
    public function home(Request $request)
    {
        $brands =  Brand::getAllVisible();
        $collections = Collection::getAllVisibleRoots();


        $appBanners = AppBanner::active()
        ->where(function ($q) {
            $q->where('valid_from', '<=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
              ->where('valid_to', '>=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
              ->orWhere('valid_from', null);
        })
        ->orderBy('position')
        ->get();

        $brandResources=BrandResource::collection($brands);
        $collectionResources=CollectionResource::collection($collections);
        $bannerResources=BannerResource::collection($appBanners);
        return response()->json(['data'=>['brands'=>$brandResources,'collections'=>$collectionResources,'banners'=>$bannerResources]]);
    }

    public function dashboard(Request $request, AppHomeHelper $appHomeHelper)
    {
        $explore = $appHomeHelper->getAppSections($request, AppSection::MEDIUM_RETAILER);
        return AppSectionResource::collection($explore);
    }

    public function brands(Request $request, AppHomeHelper $appHomeHelper)
    {
        $brands = Brand::getAllVisible();
        return BrandResource::collection($brands);
    }

    public function categories(Request $request, AppHomeHelper $appHomeHelper)
    {
        $collections = Collection::getAllVisible();
        return CollectionResource::collection($collections);
    }

    public function aboutUs()
    {
        $data=view('api.static.about_us')->render();
        return response()->json(['data'=>$data]);
    }
}
