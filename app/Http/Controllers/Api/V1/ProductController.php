<?php

namespace Niyotail\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Niyotail\Helpers\Filters\FilterHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\V1\Product\ListingRequest;
use Niyotail\Http\Resources\V1\Products\ProductListingResource;
use Niyotail\Http\Resources\V1\Products\ProductResource;
use Niyotail\Models\Cart;
use Niyotail\Models\Product;
use Niyotail\Models\Inventory;
use Niyotail\Models\Brand;
use Niyotail\Models\Store;
use Niyotail\Models\Warehouse;
use Niyotail\Helpers\RuleEngine;
use Niyotail\Models\Expression;
use Niyotail\Services\Cart\CartService;
use Niyotail\Helpers\Utils;

class ProductController extends Controller
{
    private function getAllBrands($brands)
    {
        $brandIds = [];
        foreach ($brands as $id) {
            $children = Brand::getChildren($id)->pluck('id')->toArray();
            $brandIds = array_merge($brandIds, $children);
        }

        $brandIds = array_unique($brandIds);

        return $brandIds;
    }

    public function index(ListingRequest $request)
    {
        if ($request->has('brands')) {
            $brandIds = $this->getAllBrands($request->brands);
            $request->merge(['brandIds' => $brandIds]);
        }

        $store = Store::with('tags')->find($request->store_id);
        $beatId = $store->beat_id;
        $request->merge(['beat_id' => $store->beat_id]);

        // $warehouses = Warehouse::whereHas('beats', function ($q) use ($request) {
        //                 $q->where('beat_id', $request->beat_id);
        //               })->get();
        // $warehouseIds = $warehouses->pluck('id')->toArray();
        $warehouseIds = Utils::getAvailableWarehouseForStore($store);

        if (!empty($warehouseIds)) {
            $request->merge(['warehouse_id' => $warehouseIds]);
        }

        $filterHelper = new FilterHelper();
        $filteredProducts = $filterHelper->getFilteredProducts($request);

        $productQuery = Product::whereIn('products.id', $filteredProducts['productIds'])
            //->apiFilters($request)
            ->with('brand', 'images', 'variants', 'activeInventories')
            ->selectRaw('products.*')
            ->join('inventories', 'inventories.product_id', '=', 'products.id');

        $products = $productQuery
            ->groupBy('product_id')
            // ->orderBy('published_at', 'desc')
            ->get();

          $products->map(function ($product) use ($beatId, $warehouseIds) {
              $product['beatId'] = $beatId;
              $product['warehouse_id']=$warehouseIds;
              return $product;
          });

        // Include cart status with all variants of products
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->with('items')->where('store_id', $request->store_id)->first();
        $products = CartService::processProductsForCartItem($products, $cart);

        RuleEngine::processProducts($products, $store, Expression::MEDIUM_RETAILER);
        RuleEngine::processProductsForRules($products, $store, Expression::MEDIUM_RETAILER);

        $productsPaginator = new LengthAwarePaginator($products, $filteredProducts['numFound'], 15, $filteredProducts['currentPage'], ['path' => urldecode($request->url())]);

        return ProductListingResource::collection($productsPaginator)->additional([
            'meta' => [
                'counts' => $filteredProducts['rows'],
                'filters' => $filteredProducts['filters']
            ]
        ]);
    }

    public function show(Request $request, $id)
    {
        $product = Product::with('brand', 'variants', 'images', 'activeInventories')->has('variants')->find($id);
        abort_if(empty($product), 404, "Invalid Product Id");
        $store = Store::with('tags')->find($request->store_id);
        $product['beatId'] = $store->beat_id;
        $request->merge(['beat_id' => $store->beat_id]);

        // $warehouses = Warehouse::whereHas('beats', function ($q) use ($request) {
        //                 $q->where('beat_id', $request->beat_id);
        //               })->get();
        // $warehouseIds = $warehouses->pluck('id')->toArray();
        $warehouseIds = Utils::getAvailableWarehouseForStore($store);

        if (!empty($warehouseIds)) {
            $request->merge(['warehouse_id' => $warehouseIds]);
        }
        $product['warehouse_id'] = $warehouseIds;

        // Include cart status with all variants of products
        $user = Auth::guard('api')->user();
        $cart = Cart::byUser($user)->with('items')->where('store_id', $request->store_id)->first();
        $product = CartService::processProductsForCartItem(collect()->add($product), $cart)->first();

        RuleEngine::processProduct($product, $store, Expression::MEDIUM_RETAILER);
        RuleEngine::processProductForRules($product, $store, Expression::MEDIUM_RETAILER);

        return new ProductResource($product, "detail");
    }
}
