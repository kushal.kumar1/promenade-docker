<?php


namespace Niyotail\Http\Controllers\Api\Client\V1;

use Niyotail\Helpers\InvoiceHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\Client\V1\PurchaseResource;
use Niyotail\Models\Invoice;
use Niyotail\Models\Shipment;
use Carbon\Carbon;
use Niyotail\Http\Requests\Api\Client\V1\FilterRequest;

class ShipmentController extends Controller
{
    public function purchase($storeId, FilterRequest $filter)
    {
        $format = "Y-m-d";
        $query = Shipment::ShipmentByStore($storeId);
        if ($filter->has('start_date')) {
            $startDate = Carbon::createFromFormat($format, $filter->start_date)->toDateString();
            $endDate = Carbon::createFromFormat($format, $filter->end_date)->addDay()->toDateString();
            $query = $query->whereBetween("created_at", [$startDate, $endDate]);
        }else {
            $query = $query->whereRaw("date(convert_tz(created_at, '+00:00','+05:30')) >= '2021-04-01'");
        }
        $shipments = $query->orderBy('created_at', 'desc')->paginate();
        return PurchaseResource::collection($shipments);
    }

    public function downloadInvoice($invoiceId)
    {
        $invoice = Invoice::find($invoiceId);
        if (empty($invoice)) {
            return response()->json(['error' => 'Invoice not found!'], 422);
        }
        return InvoiceHelper::downloadPdf($invoice);
    }
}