<?php

namespace Niyotail\Http\Controllers\Api\Client\V1;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Niyotail\Helpers\CreditNoteHelper;
use Niyotail\Helpers\Filters\TransactionFilterHelper;
use Niyotail\Helpers\InvoiceHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Client\V1\LedgerRequest;
use Niyotail\Http\Resources\Client\V1\LedgerResource;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Invoice;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Store;
use Niyotail\Models\Transaction;
use Niyotail\Models\WarehouseScope;

class TransactionController extends Controller
{
    /**
     * @param $storeId
     * @param LedgerRequest $request
     * @return AnonymousResourceCollection
     */
    public function ledger($storeId, LedgerRequest $request): AnonymousResourceCollection
    {
        $transactionQuery = Transaction::where(['store_id' => $storeId, 'status' => Transaction::STATUS_COMPLETED])
            ->with(['source'=>function($query) {
                $query->withOutGlobalScope(WarehouseScope::class);
            }]);

        $format = "Y-m-d";
        if ($request->has('start_date')) {
            $startDate = Carbon::createFromFormat($format, $request->start_date)->toDateString();
            $endDate = Carbon::createFromFormat($format, $request->end_date)->addDay()->toDateString();
            $transactionQuery = $transactionQuery->whereBetween('transaction_date', [$startDate, $endDate]);
        }

        $endingBalance = 0;

        $startingBalance = 0;

        $endingBalanceQuery = clone $transactionQuery;
        $endingBalanceTransaction = $endingBalanceQuery->orderBy('transaction_date', 'DESC')->first();
        if (!empty($endingBalanceTransaction)) {
            $endingBalance = $endingBalanceTransaction->balance;
        }

        $startingBalanceQuery = clone $transactionQuery;
        $startBalanceTransaction = $startingBalanceQuery->orderBy('transaction_date', 'ASC')->first();
        if (!empty($startBalanceTransaction)) {
            //Fetch balance before this entry or calculate based on current balance, type and amount
            $startingBalance = $startBalanceTransaction->balance + $startBalanceTransaction->amount * (($startBalanceTransaction->type == 'debit') ? 1 : -1);
        }

        $sortType = 'desc';

        if ($request->has('sort_type')) {
            $sortType = $request->sort_type;
        }

        $debitSumQuery = clone $transactionQuery;
        $creditQuery = clone $transactionQuery;
        $debitSum = $debitSumQuery->where('type', 'debit')->sum('amount');
        $creditSum = $creditQuery->where('type', 'credit')->sum('amount');

        if (!empty($request->filters)) {
            $filterHelper = new TransactionFilterHelper();
            $transactionQuery = $filterHelper->getFilterQuery($request->filters, $transactionQuery);
        }
        $transactions = $transactionQuery->orderBy('transaction_date', $sortType)->paginate();
        $clientVersion = !empty($request->header('Version')) ? $request->header('Version') : null;

        $additionalData = $this->additionalInfo($clientVersion, $endingBalance, $startingBalance, $debitSum, $creditSum);
        $additionalData['commission_earned'] = Store::getCommissionEarned($storeId, $request->start_date, $request->end_date)->sum('commission');
        $additionalData['total_pending_return_value'] = OrderItem::getStorePendingReturnVal($storeId);
        $additionalData['filters'] = config('settings.pos_transaction_filters');

        return LedgerResource::collection($transactions)
            ->additional($additionalData);
    }

    private function additionalInfo($clientVersion, $endingBalance, $startingBalance, $debitSum, $creditSum)
    {
        if (empty($clientVersion) || $clientVersion <= '1.35') {
            return [
                'ending_balance' => $endingBalance == 0 ? $endingBalance : -($endingBalance),
                'starting_balance' => $startingBalance == 0 ? $startingBalance : -($startingBalance),
                'debit_sum' => $debitSum,
                'credit_sum' => $creditSum
            ];
        }
        return [
            'ending_balance' => [
                'amount' => $endingBalance == 0 ? $endingBalance : -($endingBalance),
                'label' => $endingBalance >= 0 ? "You receive" : "You pay"
            ],
            'starting_balance' => [
                'amount' => $startingBalance == 0 ? $startingBalance : -($startingBalance),
                'label' => $startingBalance >= 0 ? "You receive" : "You pay"
            ],
            'debit_sum' => $debitSum,
            'credit_sum' => $creditSum
        ];
    }

    public function transactionDocument(Transaction $transaction)
    {
        if ($transaction->source instanceof Invoice) {
            return InvoiceHelper::downloadPdf($transaction->source);
        }

        if ($transaction->source instanceof CreditNote) {
            return CreditNoteHelper::downloadCreditNote($transaction->source);
        }
        return true;
    }
}
