<?php

namespace Niyotail\Http\Controllers\Api\Client\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;

use Niyotail\Http\Resources\Client\V1\Products\ProductResource;
use Niyotail\Models\Product;
use Niyotail\Models\Employee;
use Niyotail\Models\Cart;
use Niyotail\Models\Order;
use Niyotail\Models\Brand;
use Niyotail\Models\Marketer;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\Order\OrderService;

use Niyotail\Services\BrandService;
use Niyotail\Services\CollectionService as CategoryService;
use Niyotail\Services\MarketerService;

class ClientController extends Controller
{
    public function syncProducts(Request $request)
    {
        $productQuery = Product::active()->with('brand', 'collections', 'commissionTags');

        if(!empty(config('settings.niyoos_products_sync'))) {
          $productQuery = $productQuery->orWhereIn('id', config('settings.niyoos_products_sync'));
        }

        if ($request->has('last_sync_time')) {
            $productQuery = $productQuery->where('updated_at', '>=', "$request->last_sync_time");
        }

        $productQuery = $productQuery->with(['variants' => function($query) {
                          $query->where('value', 'unit')->withTrashed();
                        }])->whereHas('variants', function($q) {
                          $q->where('value', 'unit')->withTrashed();
                        });

        $products = $productQuery->orderBy('id')->paginate($request->per_page);
        return ProductResource::collection($products);
    }

    public function addToCart(Request $request)
    {
        ini_set('max_execution_time', 300);
        // $cartResult = $cartHelper->createCartFromGroup($request);
        $cartResult = null;
        try {
          $cart = $cartResult['cart'];
          if (!empty($cart)) {
              return response()->json(['data' => $this->getAPIResponse($request, null, $cartResult['result_quantity'])]);
          }
          return response()->json(['data' => $this->getAPIResponse($request, null, $cartResult['result_quantity'])]);

        } catch (\Exception $e) {
          return response()->json(['data' => null]);
        }
    }

    private function getAPIResponse($request, $order, $result)
    {
        if (!empty($order)) {
          $data['order_id'] = $order->id;
          $data['status'] = $order->status;
        }
        if (!empty($result)) {
            $products = null;
            foreach($request->product as $product) {
              $productResponse['group_id'] = $product['group_id'];
              $productResponse['quantity'] = $product['quantity'];
              $productResponse['ordered_quantity'] = $result[$product['group_id']]['ordered_quantity'];
              $productResponse['in_process_quantity'] = $result[$product['group_id']]['in_process_quantity'];
              $products[] = $productResponse;
            }
            $data['items'] = $products;
        }

        if (env('APP_ENV') == 'local') {
          \Log::info(json_encode($data));
        }
        return $data;
    }

    public function createOrder(Request $request, OrderService $orderService)
    {
        ini_set('max_execution_time', 300);
        try {
            if($request->has('1kmall_order') && $request->get('1kmall_order')) {
                $employee = Employee::where('email', '1kmall@niyotail.com')->first();
            }
            else $employee = Employee::where('email', 'replenish@niyotail.com')->first();

          $cart = Cart::withoutOrder()->byEmployee($employee)->where('store_id', $request->store_id)->with('items.product')->first();
          if (!empty($cart)) {
              $order = $orderService->create($request, $cart);
              $order->load('items.productVariant');
              return response()->json(['data' => $this->getAPIResponse($request, $order, null)]);
          }
          return response()->json(['data' => $this->getAPIResponse($request, null, null)]);

        } catch (\Exception $e) {
          if (!empty($cart)) {
            $cartService = new CartService();
            $cartService->setAuthority(CartService::AUTHORITY_SYSTEM)->deleteCart($cart);
          }
          return response()->json(['data' => null]);
        }
	  }

    public function createBrand(Request $request,  BrandService $service)
    {
        ini_set('max_execution_time', 300);
        try
        {
            $brand = Brand::where("name", $request->name)->first();
            if (empty($brand)) {
                $brand = $service->create($request);
                return response()->json(['data' => $brand->toJson()]);
            }
            return response()->json(['data' => $brand->toJson()]);
        } catch (\Exception $e) {
            return response()->json(['data' => null]);
        }
    }

    public function createCategory(Request $request, CategoryService $service)
    {
        ini_set('max_execution_time', 300);
        try {
            $category = Category::where("name", $request->name)->first();
            if (empty($category))
            {
            $category = $service->create($request);
            return response()->json(['data' => $category->toJson()]);
            }
            return response()->json(['data' => $category->toJson()]);
        } catch (\Exception $e) {
            return response()->json(['data' => null]);
        }
    }

    public function createMarketer(Request $request, MarketerService $service)
    {
        ini_set('max_execution_time', 300);
        try {
            $marketer = Marketer::where("name", $request->name)->first();
            if (empty($marketer)) {
                $marketer = $service->create($request);
                return response()->json(['data' => $marketer->toJson()]);
            }
            return response()->json(['data' => $marketer->toJson()]);
        }
        catch (\Exception $e) {
            return response()->json(['data' => null]);
        }
    }
}
