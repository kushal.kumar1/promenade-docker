<?php

namespace Niyotail\Http\Controllers\Api\Client\V1;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Api\Client\V1\Order\ViewRequest;
use Niyotail\Http\Resources\Client\V1\OrderResource;
use Niyotail\Models\Order;

class OrderController extends Controller
{
    public function orders(ViewRequest $request)
    {
        $itemQuery = function ($itemQuery) {
            $itemQuery->selectRaw('order_id, product_id, count(id) as qty, status')
                ->groupBy('product_id', 'status');
        };

        $orders = Order::with(array('items'=>$itemQuery))
            ->whereIn('orders.id', $request->order_ids)
            ->select('orders.id', 'orders.status')
            ->get();

        return OrderResource::collection($orders);
    }
}
