<?php


namespace Niyotail\Http\Controllers\Api\Client\V1;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Resources\Client\V1\ReturnOrder\GroupedReturnOrderResource;
use Niyotail\Http\Resources\Client\V1\ReturnOrder\ReturnOrderResource;
use Niyotail\Models\ReturnOrder;
use Carbon\Carbon;
use Niyotail\Http\Requests\Api\Client\V1\FilterRequest;

class ReturnsController extends Controller
{
    public function returns($storeId, FilterRequest $filter) {
        
        $format = "Y-m-d";
        $query = ReturnOrder::ReturnsByStore($storeId);
        if ($filter->has('start_date')) {
            $startDate = Carbon::createFromFormat($format, $filter->start_date)->toDateString();
            $endDate = Carbon::createFromFormat($format, $filter->end_date)->addDay()->toDateString();
            $query = $query->whereBetween("created_at", [$startDate, $endDate]);
        }else {
            $query = $query->whereRaw("date(convert_tz(created_at, '+00:00','+05:30')) >= '2021-04-01'");
        }
  
        $returns = $query->orderBy('created_at', 'desc')->paginate();//ReturnOrder::getReturnsByStore($storeId);
        return ReturnOrderResource::collection($returns);
    }

    public function allReturns($storeId, Request $request) {

        $groupedReturns = ReturnOrder::getReturnsByStoreOrDate($storeId, $request->start_date, $request->end_date);

        $groupedReturns->map(function ($groupedReturn) {
            $returns = ReturnOrder::getReturnsByReferenceId($groupedReturn->ref_id);
            $groupedReturn['returns']=$returns;
            return $groupedReturn;
        });
        return GroupedReturnOrderResource::collection($groupedReturns);
    }
}
