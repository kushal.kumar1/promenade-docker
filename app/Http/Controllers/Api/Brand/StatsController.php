<?php

namespace Niyotail\Http\Controllers\Api\Brand;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\BrandService;


class StatsController extends Controller
{
    protected $brandService;

    public function __construct()
    {
        $this->brandService = new BrandService();
    }

    public function dashboard(Request $request)
    {
        return $this->brandService->getFormattedBrandStatsForMap($request);
    }

    public function metrics(Request $request)
    {
        return $this->brandService->getFormattedBrandMetrics($request);
    }
}