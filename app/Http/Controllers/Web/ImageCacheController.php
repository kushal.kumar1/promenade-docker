<?php

namespace Niyotail\Http\Controllers\Web;

use Niyotail\Http\Controllers\Controller;
use Storage;
use \Imagick;

class ImageCacheController extends Controller
{
    private $width;
    private $height;

    /* Example:
        "/media/cache/400X258/attraction/1/abc.jpg"
    */
    public function index($path)
    {
        $validUrl = $this->checkAndProcessDimensions($path);
        if ($validUrl) {
            $image = $this->processImage($path);

            //response
            $mime = $image->getImageMimeType();
            $image->destroy();
            $file = public_path("media/cache/$path");
            return response()->file($file, [
                    'Content-Type' => $mime
            ]);
        }
    }

    private function checkAndProcessDimensions($path)
    {
        $size = strstr($path, '/', true);
        $size = explode('X', $size);
        $this->width = !empty($size[0]) && is_numeric($size[0]) ? $size[0] : null;
        $this->height = !empty($size[1]) && is_numeric($size[1]) ? $size[1] : null;
        if (empty($this->width) && empty($this->height)) {
            return false;
        }
        return true;
    }

    private function processImage($path)
    {
        $filePath = "media/".substr(strstr($path, '/'), 1);
        try {
            $imagick = new Imagick();
            $image = Storage::disk('public')->get($filePath);
            $imagick->readImageBlob($image);
        } catch (\Exception $e) {
            abort('500');
        }

        //resize
        $this->resizeImage($imagick);
        //compress
        $imagick->setInterlaceScheme(Imagick::COMPRESSION_LOSSLESSJPEG);
        $imagick->setImageCompression(Imagick::COMPRESSION_JPEG);
        $imagick->setImageCompressionQuality(100);
        $imagick->stripImage();

        //save
        $resizedImage = $imagick->getImageBlob();
        Storage::disk('public')->put("media/cache/".$path, $resizedImage);
        return $imagick;
    }

    private function resizeImage($imagick)
    {
        if ($this->height == null) {
            $imagick->scaleImage($this->width, 0);
        } elseif ($this->width == null) {
            $imagick->scaleImage(0, $this->height);
        } else {
            $imagick->cropThumbnailImage($this->width, $this->height);
        }
    }
}
