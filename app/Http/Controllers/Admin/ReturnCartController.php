<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Models\Invoice;
use Niyotail\Services\ReturnCart\ReturnCartService;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Niyotail\Http\Requests\Admin\ReturnCart\AddItemRequest;
use Niyotail\Http\Requests\Admin\ReturnCart\UpdateItemRequest;
use Niyotail\Http\Requests\Admin\ReturnCart\RemoveItemRequest;
use Niyotail\Models\ReturnCart;
use Niyotail\Models\ReturnCartItem;
use Niyotail\Models\Shipment;

class ReturnCartController extends Controller
{
    public function suggestions(Request $request)
    {
        $term = $request->term;
        $storeID = $request->store_id;

        if ($request->has('search_type') && $request->search_type == 'invoice') {
            $invoice = Invoice::where('reference_id', 'LIKE', "%$term%")
                ->with('shipment')
                ->first();
            $shipments = collect([]);
            if(!empty($invoice)){
                $shipment = $invoice->shipment;
                $canReturn = $shipment->canReturn();
                if(!$canReturn) return null;
                $shipment->loadMissing('items');
                $shipments->push($shipment);
            }
        }
        else {
          $shipments = Shipment::with(['orderItems' => function ($query) use($term, $storeID) {
                          $query->canReturn()
                                ->whereHas('order', function($q) use($storeID) {
                                  $q->where('store_id', $storeID);
                                })
                                ->whereHas('product', function($q) use($term) {
                                  $q->where('name', 'like', "%$term%")->orWhere('id', $term)->orWhere('barcode', 'like', "%$term%");
                                })
                                ->with(['product' => function($q) use($term) {
                                  $q->with('images')->where('name', 'like', "%$term%")->orWhere('id', $term)->orWhere('barcode', 'like', "%$term%");
                                }]);
                        }])
                        ->with('order')
                        ->whereHas('orderItems', function ($query) use($term, $storeID) {
                          $query->canReturn()
                                ->whereHas('order', function($q) use($storeID) {
                                  $q->where('store_id', $storeID);
                                })
                                ->whereHas('product', function($q) use($term) {
                                  $q->where('name', 'like', "%$term%")->orWhere('id', $term)->orWhere('barcode', 'like', "%$term%");
                                });
                        })
                        ->where('status', '!=', Shipment::STATUS_CANCELLED)
                        ->whereRaw('created_at >= DATE_SUB(NOW(), INTERVAL 16 MONTH)')->orderBy('created_at', 'desc')->limit(25)->get();
        }
        return view('admin.return.order._shipments', compact('shipments'));
    }

    public function addItem(AddItemRequest $request, ReturnCartService $returnCartService)
    {
        $employee = Auth::guard('admin')->user();
        $cart = ReturnCart::byEmployee($employee)
            ->with('items.productVariant.product.images')
            ->where('store_id', $request->store_id)->first();

        $returnCartService->setCart($cart);
        $cart = $returnCartService->addToCart($request, $employee);
        $cart->loadMissing('items.productVariant.product.images');

        return view('admin.return.order._items', compact('cart'));
    }

    public function updateItem(UpdateItemRequest $request, ReturnCartService $returnCartService)
    {
        $employee = Auth::guard('admin')->user();
        $cartItem = ReturnCartItem::whereHas('returnCart', function ($cartQuery) use ($employee) {
            $cartQuery->byEmployee($employee);
        })->find($request->id);
        abort_if(empty($cartItem), 404);

        $cart = $returnCartService->updateQuantity($request);
        $cart->loadMissing('items.productVariant.product.images');

        return view('admin.return.order._items', compact('cart'));
    }

    public function removeItem(RemoveItemRequest $request, ReturnCartService $returnCartService)
    {
        $employee = Auth::guard('admin')->user();
        $cartItem = ReturnCartItem::whereHas('returnCart', function ($cartQuery) use ($employee) {
            $cartQuery->byEmployee($employee);
        })->find($request->id);
        abort_if(empty($cartItem), 404);

        $cart = $returnCartService->removeCartItem($request);

        return view('admin.return.order._items', compact('cart'));
    }
}
