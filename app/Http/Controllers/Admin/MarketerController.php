<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Marketer\ViewRequest;
use Niyotail\Http\Requests\Admin\Marketer\CreateRequest;
use Niyotail\Http\Requests\Admin\Marketer\UpdateRequest;
use Niyotail\Models\Marketer;
use Niyotail\Models\MarketerLevel;
use Niyotail\Services\MarketerService;
use Niyotail\Grids\MarketerGrid;

class MarketerController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = MarketerGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.marketer.index', compact('grid'));
    }

    public function create(Request $request)
    {
        $levels = MarketerLevel::all();
        return view('admin.marketer.create', compact('levels'));
    }

    public function edit($id)
    {
        $marketer = Marketer::with(['brands', 'level'])->find($id);
        $levels = MarketerLevel::all();
        abort_if(empty($marketer), 404);
        return view('admin.marketer.edit', compact('marketer', 'levels'));
    }

    public function store(CreateRequest $request, MarketerService $marketerService)
    {
        $marketer = $marketerService->create($request);
        return response()->json($marketer);
    }

    public function update(UpdateRequest $request, MarketerService $marketerService)
    {
        $marketer = $marketerService->update($request);
        return response()->json($marketer);
    }
}
