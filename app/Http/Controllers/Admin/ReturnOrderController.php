<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\CreditNoteHelper;
use Niyotail\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Niyotail\Http\Requests\Admin\ReturnOrder\CancelRequest;
use Niyotail\Http\Requests\Admin\ReturnOrder\CompleteRequest;
use Niyotail\Http\Requests\Admin\ReturnOrder\CreateReturnOrderRequest;
use Niyotail\Http\Requests\Admin\ReturnOrder\CreateRequest;
use Niyotail\Http\Requests\Admin\ReturnOrder\ViewRequest;

use Niyotail\Models\CreditNote;
use Niyotail\Models\Inventory;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\ReturnOrderItem;
use Niyotail\Models\Shipment;
use Niyotail\Models\ReturnCart;
use Niyotail\Grids\ReturnGrid;
use Niyotail\Services\ReturnOrder\ReturnOrderService;

class ReturnOrderController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ReturnGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.return.index', [
        'grid' => $grid
    ]);
    }

    public function create($shipmentId)
    {
        $shipment = Shipment::with(['orderItems' => function ($query) {
            $query->with('product.images')->canReturn();
        }])->find($shipmentId);

        abort_if(empty($shipment), 404);
        return view('admin.return.create', compact('shipment'));
    }

    public function getCreditNote($id)
    {
        $creditNote = CreditNote::find($id);
        abort_if(empty($creditNote), 404);
        $pdfUrl = CreditNoteHelper::streamPdf($creditNote);
        return response()->redirectTo($pdfUrl);
    }

    public function store(CreateRequest $request, ReturnOrderService $returnOrderService)
    {
        $createdBy = Auth::user();
        $shipment = $returnOrderService->setAuthority(ReturnOrderService::AUTHORITY_ADMIN)->create($request, $createdBy);
        return response()->json($shipment);
    }

    public function cancel(CancelRequest $request, ReturnOrderService $returnOrderService)
    {
        $shipment = $returnOrderService->setAuthority(ReturnOrderService::AUTHORITY_ADMIN)->cancel($request);
        return response()->json($shipment);
    }

    public function confirmComplete($returnId) {
        $returnOrder = ReturnOrder::with('items.orderItem')->find($returnId);
        $groupedItems = $returnOrder->items->groupBy('orderItem.sku');
        return view('admin.return.confirm', compact('returnOrder', 'groupedItems'));
    }

    public function complete($returnOrderId, Request $request, ReturnOrderService $returnOrderService)
    {
        $returnOrder = ReturnOrder::findOrFail($returnOrderId);
        $response = $returnOrderService->setAuthority(ReturnOrderService::AUTHORITY_ADMIN)->confirmReturn($returnOrder, $request);
        return response()->json($response);
    }

    public function completeSpecialReturn(ReturnOrderService $returnOrderService) {
        $orders = DB::table('special_return')->get();
        foreach ($orders as $order) {
            $returnOrder = ReturnOrder::findOrFail($order->id);
            $response = $returnOrderService->setAuthority(ReturnOrderService::AUTHORITY_ADMIN)
                ->confirmSpecialReturn($returnOrder);
        }
    }

    public function createFromCart(CreateReturnOrderRequest $request, ReturnOrderService $returnOrderService)
    {
        $employee = Auth::guard('admin')->user();
        $cart = ReturnCart::byEmployee($employee)->where('store_id', $request->store_id)
            ->with('items.productVariant')->orderBy('created_at', 'desc')->first();
        abort_if(empty($cart), 404);

        $returnOrderService->createMultiple($request, $cart, $employee);

        // return redirect(route('admin::returns.index'))->with("message", $message);
        return response()->json(["success" => true]);
    }

    // this function is used in rare scenario where inventory needs
    // to be decreased for expiry, damaged or near expiry items
    // post completing return
    public function decreaseInventoryForExpiryReturns() {
        $data = collect(array ());
        $pendingItems = $data->where('Status', '!=', 'Done')
            ->where('PID', '!=', 'New');

        $itemsByPid = $pendingItems->groupBy('PID');

        $itemWiseQty = [];
        foreach ($itemsByPid as $pid=>$items) {
            $itemWiseQty[$pid] = $items->sum('Qty');
        }

        DB::transaction(function() use ($itemWiseQty) {

            foreach ($itemWiseQty as $item=>$qty) {
                $inventories = Inventory::where('product_id', $item)
                    ->where('quantity', '>', 0)
                    ->get();

                // sort by batch date
                $inventories = $inventories->sortBy(function($inventory) {
                    return $inventory->getPrettyBatchDate();
                });

                if($inventories->count()) {
                    $totalInv = $inventories->sum('quantity');
                    if($totalInv <= $qty) {
                        // set all inv values to 0
                        $inventories->map(function($i){
                            $i->quantity = 0;
                            $i->save();
                        });
                    }
                    else {
                        // deduct qty from batches one by one
                        foreach ($inventories as $inventory) {
                            if($qty == 0) break;

                            $invQty = $inventory->quantity;
                            if($invQty < $qty) {
                                $inventory->quantity = 0;
                            }
                            else {
                                $inventory->quantity = $invQty - $qty;
                            }

                            $qty -= $invQty;
                            $inventory->save();
                        }
                    }
                }
            }
        });
    }
}
