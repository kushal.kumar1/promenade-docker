<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Niyotail\Grids\VendorMarketerMapGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Vendor\VendorMarketerMapRequest;
use Niyotail\Models\Marketer;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorMarketerMap;
use Niyotail\Services\VendorService;

class VendorMarketerMapController extends Controller
{
    protected $vendorService;

    public function __construct()
    {
        $this->vendorService = new VendorService();
    }

    public function index($id, Request $request) {
        $vendor = Vendor::findOrFail($id);
        $grid = VendorMarketerMapGrid::get(['vendor_id'=>$id]);

        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.vendor.marketer.index', [
            'grid' => $grid,
            'vendor' => $vendor
        ]);
    }

    public function addMarketer($vendorId) {
        $vendor = Vendor::findOrFail($vendorId);
        $relations = ['CFA', 'Brand', 'Super Stockist', 'Sub Stockist', 'Distributor', 'Wholesaler'];
        $channels = ['MT', 'GT', 'E-comm'];

        return view('admin.vendor.marketer.form')
            ->with(compact('relations', 'channels', 'vendor'));
    }

    public function editMarketer($vendorId, $marketerId) {
        $vendor = Vendor::findOrFail($vendorId);
        $marketer = Marketer::findOrFail($marketerId);
        $relations = ['CFA', 'Brand', 'Super Stockist', 'Sub Stockist', 'Distributor', 'Wholesaler'];
        $channels = ['MT', 'GT', 'E-comm'];

        $map = VendorMarketerMap::VendorId($vendorId)->MarketerId($marketerId)->first();

        return view('admin.vendor.marketer.form')
            ->with(compact('relations', 'channels', 'vendor', 'marketer', 'map'));
    }

    public function save(VendorMarketerMapRequest $request) {
        return $this->vendorService->saveVendorMarketerMap($request);
    }

    public function downloadTotFile($mapId) {
        $map = VendorMarketerMap::findOrFail($mapId);
        $file = $map->tot_file;
        abort_if(empty($file), 404);

        return Storage::disk('s3')->download('signed_tot_file/'.$file);
    }

     public function delete() {

     }
}