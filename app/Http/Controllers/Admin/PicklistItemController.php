<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Helpers\PicklistHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Picklist\AddItemRequest;
use Niyotail\Http\Requests\Admin\Picklist\RemoveItemRequest;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Models\Product;
use Niyotail\Services\PicklistService;

class PicklistItemController extends Controller
{
    public function search(Request $request)
    {
        $picklistId = $request->picklist_id;
        $barcode = $request->barcode;

        $products = Product::with('featuredImage')
            ->where('barcode', $barcode)
            ->whereHas('picklistItems', function ($query) use ($picklistId) {
                $query->where('picklist_id', $picklistId)->where('picklist_items.status', PicklistItem::STATUS_PENDING);
            })
            ->withCount(['picklistItems' => function ($query) use ($picklistId) {
                $query->where('picklist_id', $picklistId)->where('picklist_items.status', PicklistItem::STATUS_PENDING);
            }])
            ->get();

        return view('admin.picklist.scanner.products', compact('products', 'picklistId'));
    }

    public function addItem(AddItemRequest $request, PicklistService $service)
    {
        $picklist = Picklist::find($request->picklist_id);
        $pendingPicklistCount = Picklist::where('picker_id', $picklist->picker_id)
            ->whereIn('status', [Picklist::STATUS_PICKING, Picklist::STATUS_PICKED])->where('id', '!=', $picklist->id)->count();
        if ($pendingPicklistCount > 0) {
            return response(['custom_error' => "Please complete the previous picklist of the picker", 'message' => "Please complete the previous picklist of the picker"], 422);
        }
        $skipData = $request->has('skip') ? $request->skip : [];
        $service->setAuthority($service::AUTHORITY_ADMIN)->processItems($picklist, $request->product_id, $request->quantity, $skipData);
        $pickedProducts = PicklistHelper::getProcessedProducts($picklist);
        return view('admin.picklist.scanner.picked_items', compact('pickedProducts'));
    }

    public function removeItem(RemoveItemRequest $request, PicklistService $service)
    {
        $picklist = Picklist::find($request->picklist_id);
        $service->removeItems($picklist, $request->product_id);
        $pickedProducts = PicklistHelper::getProcessedProducts($picklist);
        return view('admin.picklist.scanner.picked_items', compact('pickedProducts'));
    }
}
