<?php

namespace Niyotail\Http\Controllers\Admin;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Niyotail\Grids\Picklist\PicklistGrid;
use Niyotail\Grids\Picklist\PicklistItemsGrid;
use Niyotail\Grids\ShipmentGrid;
use Niyotail\Helpers\PicklistHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Picklist\CreateRequest;
use Niyotail\Http\Requests\Admin\Picklist\DetailsRequest;
use Niyotail\Http\Requests\Admin\Picklist\UpdateRequest;
use Niyotail\Http\Requests\Admin\Picklist\ViewRequest;
use Niyotail\Models\Employee;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Services\PicklistService;

class PicklistController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = PicklistGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.picklist.index', [
            'grid' => $grid
        ]);
    }

    public function items(DetailsRequest $request, $id)
    {
        $picklist = Picklist::with('agent')->find($id);
        abort_if(empty($picklist), 404);
        $grid = PicklistItemsGrid::get(['id' => $id]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.picklist.grid', [
            'grid' => $grid,
            'picklist' => $picklist
        ]);
    }

    public function scanner($id, Request $request)
    {
        $picklist = Picklist::with('agent')->find($id);
        abort_if(empty($picklist) || !$picklist->allowScan(), 404);
        $pickedProducts = PicklistHelper::getProcessedProducts($picklist);
        return view('admin.picklist.scanner', compact('picklist', 'pickedProducts'));
    }

    public function shipments($id, Request $request)
    {
        $picklist = Picklist::with('agent', 'shipments')->find($id);
        abort_if(empty($picklist), 404);

        $grid = ShipmentGrid::get(['picklist_id' => $id]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.picklist.grid', [
            'grid' => $grid,
            'picklist' => $picklist
        ]);

        return view('admin.picklist.shipments', compact('picklist', 'grid'));
    }

    public function getPdf($id, PicklistService $service)
    {
        $picklist = Picklist::find($id);
        abort_if(empty($picklist), 404);
        //TODO: Check to be removed
        if($picklist->warehouse_id == 3){
            $items = PicklistItem::getLocationWiseItems($picklist->id);
        }else{
            $items = PicklistItem::getItemsWithLocations($picklist);
        }
        // Stream pdf on local environment
        if (App::environment('local')) {
            $picklist->loadMissing('agent', 'orderItems.order');
            $pdf = SnappyPdf::loadView('pdf.picklist', ['picklist' => $picklist, 'items' => $items])
                ->setOption('footer-center', utf8_decode('Page [page] of [topage] (Picklist ID: ' . $picklist->id . ')'))
                ->setPaper('A4')
                ->setOption('image-quality', 100)->output();

            return response()->make($pdf, 200, [
                'Content-Type' => 'application/pdf'
            ]);
        }
        return PicklistHelper::getPdf($picklist, $items);

    }

    public function updateAgent(Request $request, PicklistService $picklistService)
    {
        $picklist = Picklist::find($request->id);
        abort_if(empty($picklist), 404);
        $picklist = $picklistService->updateAgent($request);
        return response()->json($picklist);
    }

    public function assignPicker(Request $request, PicklistService $picklistService)
    {
        $picklist = Picklist::find($request->id);
        abort_if(empty($picklist), 404);
        $employee = Employee::findOrFail($request->employee_id);
        $picklist = $picklistService->assignPicker($picklist, $employee);
        return response()->json($picklist);
    }


    public function store(CreateRequest $request, PicklistService $picklistService)
    {
        $picklist = $picklistService->create($request, Auth::guard('admin')->user());
    }

    public function close(UpdateRequest $request, PicklistService $picklistService)
    {
        $picklist = Picklist::findOrFail($request->id);
        $picklist = $picklistService->close($picklist);
        return response()->json($picklist);
    }
}
