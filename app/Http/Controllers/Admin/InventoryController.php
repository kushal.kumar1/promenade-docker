<?php

namespace Niyotail\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Niyotail\Grids\InventoryGrid;
use Niyotail\Grids\InventoryTransactionGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Inventory\UpdateFlatInventoryStatusRequest;
use Niyotail\Http\Requests\Admin\Inventory\UpdateStatusRequest;
use Niyotail\Http\Requests\Admin\Inventory\UpdateTypeRequest;
use Niyotail\Http\Requests\Admin\Inventory\UpdateWarehouseRequest;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Inventory;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;
use Niyotail\Models\Warehouse;
use Niyotail\Services\InventoryService;

class InventoryController extends Controller
{
    public function index($id, Request $request)
    {
        $product = Product::find($id);
        abort_if(empty($product), 404);

        $grid = InventoryGrid::get(['productId' => $id]);
        if ($request->ajax()) {
            return $grid;
        }
        $inactiveReasons = config('settings.inventory_inactive_reasons');
        $warehouses = Warehouse::where('status', 1)->get();
        return view('admin.product.inventory.index', compact('grid', 'product', 'warehouses', 'inactiveReasons'));
    }

    public function updateStatus(UpdateStatusRequest $request, InventoryService $service)
    {
        $inventory = $service->updateStatus($request);
        return response()->json($inventory);
    }

    public function updateType(UpdateTypeRequest $request, InventoryService $service)
    {
        $service->updateType($request);
    }

    public function updateWarehouse(UpdateWarehouseRequest $request, InventoryService $service)
    {
        $service->updateWarehouse($request);
    }

//    public function updateQuantity($id, UpdateQuantityRequest $request, InventoryService $service)
//    {
//        $product = Product::with('inventories', 'activeInventories', 'totalInventory')->find($id);
//        abort_if(empty($product), 404);
//
//        $employee = Auth::guard('admin')->user();
//        $request->merge(['user_id' => $employee->id]);
//        $service->updateQuantity($product, $request);
//
//        return redirect()->route('admin::products.edit', ['id' => $product->id]);
//    }

    public function getInventoryTransactions($inventoryId, Request $request)
    {
        $inventory = Inventory::find($inventoryId);
        $grid = InventoryTransactionGrid::get(['id' => $inventoryId]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.product.inventory.detail', compact('grid', 'inventory'));
    }

    public function updateInventoryStatusUi(Request $request)
    {
        $flatInventoryStatus = FlatInventory::getConstants("STATUS");
        $warehouses = Warehouse::physical()->get();
        return view('admin.product.inventory.update_status', compact('flatInventoryStatus', 'warehouses'));
    }

    public function updateInventoryStatus(UpdateFlatInventoryStatusRequest $request)
    {
        $flatInventoryQuery = FlatInventory::where(['warehouse_id' => $request->warehouse_id, 'product_id' => $request->product_id,
            'status' => $request->old_status]);

        if ($request->filled('mfd')) {
            $mfd = Carbon::parse($request->mfd)->toDateString();
            $flatInventories = $flatInventoryQuery->where('manufacturing_date', $mfd)->limit($request->quantity)
                ->get();
        }
        if (empty($flatInventories)) {
            $flatInventories = $flatInventoryQuery->limit($request->quantity)->get();
        }

        if ($flatInventories->isEmpty()) {
            return response(['message' => "No inventories found!"], 422);
        }
        if ($flatInventories->count() < $request->quantity) {
            return response(['message' => "Only {$flatInventories->count()} found in $request->old_status"], 422);
        }

        $updatedData = ['status' => $request->new_status];
        if(!empty($request->storage_id)) {
            $storage = Storage::find($request->storage_id);
            $updatedData['storage_id'] = $storage->id;
            $updatedData['warehouse_id'] = $storage->warehouse_id;
        }

        FlatInventory::whereIn('id', $flatInventories->pluck('id')->toArray())->update($updatedData);

        return response(['message' => "Inventory status updated!"]);
    }


    public function searchProducts(Request $request)
    {
        $term = $request->term;
        return Product::orWhere("id", "like", "%$term%")
            ->orWhere("name", "like", "%$term%")
            ->limit(20)
            ->get();
    }

    public function searchStorages(Request $request)
    {
        $term = $request->term;
        return Storage::orWhere("zone", "like", "%$term%")
            ->orWhere("label", "like", "%$term%")
            ->limit(20)
            ->get();
    }

    public function searchStatusWiseInventoryCount(Request $request)
    {
        $result = FlatInventory::getStatusWiseCount($request->product_id, $request->warehouse_id)->toArray();
        return view('admin.product.inventory.status_wise_count', compact('result'))->render();
    }
}
