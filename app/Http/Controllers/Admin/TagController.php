<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Grids\TagGrid;
use Niyotail\Http\Controllers\Controller;

class TagController extends Controller
{
    public function index(Request $request)
    {
        $grid = TagGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.tag.index', compact('grid'));
    }
}
