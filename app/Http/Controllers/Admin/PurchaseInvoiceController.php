<?php

namespace Niyotail\Http\Controllers\Admin;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Niyotail\Grids\PurchaseInvoice\ReturnItemGrid;
use Niyotail\Grids\PurchaseInvoiceGrid;
use Niyotail\Helpers\File;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\admin\PurchaseInvoice\AddForecastPaymentDateRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\EditRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\GenerateSaleOrderRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\ViewRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\UpdateRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\AddPaymentDateRequest;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\LableRequest;
use Niyotail\Models\Inventory;
use Niyotail\Models\Product;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceCartItems;
use Niyotail\Models\PurchaseInvoiceGrn;
use Niyotail\Models\PurchaseInvoiceShortItem;
use Niyotail\Models\PurchaseOrder;
use Niyotail\Models\PurchasePlatform;
use Niyotail\Models\StorePurchaseInvoice;
use Niyotail\Models\TaxClass;
use Niyotail\Models\Warehouse;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Services\DebitNoteService;
use Niyotail\Services\InventoryService;
use Niyotail\Services\PurchaseInvoiceGrnService;
use Niyotail\Services\PurchaseInvoiceService;

class PurchaseInvoiceController extends Controller
{
    protected $purchaseInvoiceService;
    protected $purchaseInvoiceGrnService;
    protected $debitNoteService;

    public function __construct()
    {
        $this->purchaseInvoiceService = new PurchaseInvoiceService();
        $this->purchaseInvoiceGrnService = new PurchaseInvoiceGrnService();
        $this->debitNoteService = new DebitNoteService();
    }

    public function index(ViewRequest $request)
    {
        $grid = PurchaseInvoiceGrid::get($request->has('po_id') ? ["po_id" => $request->po_id] : []);
        if ($request->ajax()) {
            return $grid;
        }
        $storePurchaseInvoicesCount = StorePurchaseInvoice::whereHas('purchaseInvoice', function ($piQuery) {
            $piQuery->where('status', PurchaseInvoice::STATUS_COMPLETED);
        })->whereNull('assigned_id')->count();
        return view('admin.purchase_invoice.index', [
            'grid' => $grid,
            'storePurchaseInvoicesCount' => $storePurchaseInvoicesCount
        ]);
    }

    public function create($purchaseOrderId, Request $request)
    {
        $response = array();

        $purchaseOrder = PurchaseOrder::with('vendor', 'items', 'createdBy')->find($purchaseOrderId);
        abort_if(empty($purchaseOrder), 404);

        $warehouses = Warehouse::active()->get();

        $response['po'] = $purchaseOrder;
        $response['vendor_addresses'] = $purchaseOrder->vendor->addresses;
        $response['form_destination'] = $form_destination = null;
        $response['warehouses'] = $warehouses;

        return view('admin.purchase_invoice.edit', $response);
    }

    public function editInvoice($invoiceId, EditRequest $request)
    {
        if (!Gate::allows('view-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $invoice = PurchaseInvoice::with('items.productVariant')->find($invoiceId);
        abort_if(empty($invoice), 404);

        $purchaseOrder = PurchaseOrder::with('vendor', 'items', 'createdBy')
            ->find($invoice->purchase_order_id);

        $response['invoice'] = $invoice;
        if ($purchaseOrder) {
            $response['po'] = $purchaseOrder;
        }

        // allowing to generate GRN before completing invoice
        $items = $this->purchaseInvoiceGrnService->getInvoiceItemsForGrn($invoice);
        $isDebitNoteAllowed = $this->purchaseInvoiceGrnService->ifEligibleForDebitNote($items);
        $cgstTotal = $igstTotal = $sgstTotal = $cessTotal = $splCessTotal = 0;
        foreach ($invoice->items as $item) {
            $cgstTotal += round($item->cgst * $item->quantity * $item->productVariant->quantity, 2);
            $sgstTotal += round($item->sgst * $item->quantity * $item->productVariant->quantity, 2);
            $igstTotal += round($item->igst * $item->quantity * $item->productVariant->quantity, 2);
            $cessTotal += round($item->cess * $item->quantity * $item->productVariant->quantity, 2);
            $splCessTotal += round($item->spl_cess * $item->quantity * $item->productVariant->quantity, 2);
        }
        $taxBreakups = [
            "<b>CGST</b> : $cgstTotal ",
            "<b>SGST</b> : $sgstTotal ",
            "<b>IGST</b> : $igstTotal ",
            "<b>CESS </b>: $cessTotal ",
            "<b>SPL_CESS</b> : $splCessTotal "
        ];

        $taxClasses = TaxClass::active()->get();
        $measureUnits = ProductVariant::getConstants('uom');
        $response['invoice_items'] = $items;
        $response['taxClasses'] = $taxClasses;
        $response['measureUnits'] = $measureUnits;
        $response['is_debit_note_allowed'] = $isDebitNoteAllowed;
        $response['taxBreakups'] = array_filter($taxBreakups);
        $response['discount_platforms'] = PurchasePlatform::discount()->get();

        return view('admin.purchase_invoice.grn.generate', $response);
    }

    public function store(Request $request)
    {
        if (!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        // TODO - Implement request validations here

        $invoiceParams = $request->input('invoice');
        $items = $request->input('items');

        $invoiceReq = new Request();
        $invoiceReq->replace($invoiceParams);

        if (!empty($invoiceParams['id'])) {
            $invoice = $this->purchaseInvoiceService->updatePurchaseInvoice($invoiceReq);
        } else {
            $invoice = $this->purchaseInvoiceService->createPurchaseInvoice($invoiceReq);
        }

        $this->purchaseInvoiceService->addInvoiceItems($invoice, $items);
        return $invoice;
    }

    public function markComplete($invoiceId, Request $request)
    {
        if (!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoiceId), 404);

        $grns = $invoice->all_grn;
        foreach ($grns as $grn) {
            if (!$grn->inventory_imported) {
                throw new \Exception('Please import total order inventory in GRN before completing the invoice.');
            }
        }

        $generatedDate = $request->has('debit_note_date') &&
        !empty($request->input('debit_note_date')) ? $request->debit_note_date : Carbon::now()->format('Y-m-d H:i:s');
        $this->debitNoteService->generateDebitNote($invoice, $generatedDate);

        /* Commenting in order to allow to generate debit note */
//        $grnItems = $this->purchaseInvoiceGrnService->getInvoiceItemsForGrn($invoice);
//
//        foreach ($grnItems as $item) {
//            if(!$item['is_complete']) {
//                throw new \Exception('Please import total order inventory before completing the invoice.');
//            }
//        }

        return $this->purchaseInvoiceService->markComplete($invoice);
    }

    public function itemHtml(Request $request)
    {
        $itemProps = array(
            "product_id" => $request->get('product_id'),
            "name" => $request->get('name'),
            'barcode' => $request->get('barcode'),
            'sku' => $request->get('sku'),
            'quantity' => $request->get('quantity'),
            'is_requested' => $request->get('is_requested', null)
        );

        $response['itemDetails'] = $itemProps;
        $itemFields = (new PurchaseInvoiceCartItems())->getSortedFields();
        $response['itemFields'] = $itemFields;

        if (!$itemProps['is_requested']) {
            $product = Product::find($itemProps['product_id']);
            $productVariant = ProductVariant::sku($request->get('sku'))->first();
            $response['mrp'] = $productVariant->mrp;
            $response['taxes'] = $product->taxes;
            $response['product'] = $product;
            $response['variant_qty'] = $productVariant->quantity;
        } else {
            $product = ProductRequest::find($itemProps['product_id']);
            $response['product'] = $product;
            $response['taxes'] = $product->taxes;
            $response['mrp'] = $product->mrp;
        }

        return view('admin.purchase_invoice.item-row', $response);
    }


    public function getGrnPdf($grnId)
    {
        try {
            $grn = PurchaseInvoiceGrn::find($grnId);
            abort_if(empty($grn), 404);
            return $this->purchaseInvoiceGrnService->generateGrnPdf($grn);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function importPurchaseInvoice($invoiceId, Request $request)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);

        $this->purchaseInvoiceService->importInvoiceItemsToInventory($invoice);
    }

    public function getCalculatedFields(Request $request)
    {
        $values = $this->purchaseInvoiceService->getInvoiceItemPreview($request);
        return view('admin.purchase_invoice.cart._itemValues', compact('values'));
    }

    public function recalculateInvoice($invoiceId)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);
        $this->purchaseInvoiceService->recalculatePurchaseInvoice($invoice);
        return redirect(route('admin::purchase-invoices.editInvoice', $invoiceId));
    }

    public function updateItemPrice($invoiceId, Request $request)
    {
        if (!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);
        return $this->purchaseInvoiceService->updateItemCostAndQuantity($request, $invoice);
    }

    public function fixIrregularInventory()
    {
        $irregularItems = [];
        $inventories = Inventory::with('grnItem.item')->whereNotNull('grn_item_id')->get();
        $chunks = $inventories->chunk(100);
        foreach ($chunks as $chunk) {
            foreach ($chunk as $inventory) {
                $invoiceItem = $inventory->grnItem->item;
                $invoiceCost = $invoiceItem->total_effective_cost;
                $inventoryUnitCost = $inventory->unit_cost_price;
                if ($inventoryUnitCost != $invoiceCost) {
                    $irregularItems[] = [
                        'inventory_id' => $inventory->id,
                        'inventory_cost' => $inventoryUnitCost,
                        'pi_cost' => $invoiceCost
                    ];
                    $inventory->unit_cost_price = $invoiceCost;
                    $inventory->save();
                }
            }
        }
    }

    public function fixIrregularIBatchId()
    {
        $inventoryService = new InventoryService();
        $grn = PurchaseInvoiceGrn::with('invoice', 'items', 'items.inventoryItem')->get();
        foreach ($grn as $g) {
            $items = $g->items->where('batch_number', 'not like', '2020-%');
            foreach ($items as $item) {
                $productId = $item->product_id;
                $inventoryItem = $item->inventoryItem;
                if ($inventoryItem) {
                    $batchNo = $inventoryService->getBatchNo(
                        $g, $productId, 1
                    );
                    if ($batchNo) {
                        $inventoryItem->batch_number = $batchNo;
                        $inventoryItem->save();
                    }
                }
            }
        }
    }

    public function returnInvoiceItems($invoiceId, Request $request)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);

        $grid = ReturnItemGrid::get(['invoice_id' => $invoiceId]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.purchase_invoice.return.index', [
            'grid' => $grid
        ]);
    }

    public function createReturnDebit(Request $request)
    {
        if (!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }
        return $this->purchaseInvoiceService->createReturnDebitNote($request);
    }

    public function resetItemImportQty(Request $request)
    {
        $itemId = $request->item_id;
        return $this->purchaseInvoiceService->resetItemImportQty($itemId);
    }

    public function deleteInvoice($invoiceId, UpdateRequest $request)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);

        return $this->purchaseInvoiceService->deletePurchaseInvoice($invoice);
    }

    public function updateForecastPaymentDate($invoiceId, AddForecastPaymentDateRequest $addForecastPaymentDateRequest)
    {
        $purchaseInvoice = PurchaseInvoice::find($invoiceId);
        $purchaseInvoice->forecast_payment_date = $addForecastPaymentDateRequest->forecast_payment_date;
        $purchaseInvoice->save();
        return $purchaseInvoice;
    }

    public function updatePaymentDate($invoiceId, AddPaymentDateRequest $addPaymentDateRequest)
    {
        $purchaseInvoice = PurchaseInvoice::find($invoiceId);
        $purchaseInvoice->payment_date = $addPaymentDateRequest->payment_date;
        $purchaseInvoice->save();
        return $purchaseInvoice;
    }

    public function updateDiscountPlatform($invoiceId, Request $request) {
        if (!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);

        $this->validate($request, [
            'discount_platform_id' => 'nullable|int|exists:purchase_platforms,id'
        ]);

        $invoice->discount_platform_id = $request->discount_platform_id;
        $invoice->save();
        return $invoice;
    }

    public function uploadCopy($id, Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file'
        ]);

        $purchaseInvoice = PurchaseInvoice::find($id);
        abort_if(empty($purchaseInvoice), 404);

        $image = $request->file('file');
        $targetDirectory = storage_path('app/purchased_invoice/invoice/' . $id);
        $file = new File($image);
        $file->setName(uniqid() . "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $purchaseInvoice->scanned_file = $file->getName();
        $purchaseInvoice->save();
        return $purchaseInvoice;
    }

    public function getScannedCopy($id)
    {
        try {
            $purchaseInvoice = PurchaseInvoice::find($id);
            abort_if(empty($purchaseInvoice), 404);

            $file = $purchaseInvoice->scanned_file;
            $exists = Storage::has('purchased_invoice/invoice/' . $purchaseInvoice->id . '/' . $file);
            if ($exists) {
                return Storage::download('purchased_invoice/invoice/' . $purchaseInvoice->id . '/' . $file);
            } else {
                $cart = $purchaseInvoice->purchaseCart;
                $file = $cart->scanned_file;
                abort_if(empty($file), 404);
                return Storage::download('purchased_invoice/' . $cart->id . '/' . $file);
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function updateShortProduct($invoiceId, $itemId, Request $request)
    {
        $purchaseInvoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($purchaseInvoice), 404);
        $product = PurchaseInvoiceShortItem::findOrFail($itemId);
        return $this->purchaseInvoiceService->updateShortProduct($product, $request);
    }

    public function generateSaleOrder(GenerateSaleOrderRequest $request)
    {
        $purchaseInvoiceId = $request->purchase_invoice_id;
        Artisan::call("generate:direct-purchase-order $purchaseInvoiceId");
        return response(['message' => "Sale Orders Generated!"]);
    }

    public function generateLabels(LableRequest $request)
    {
        $product = Product::with('unitVariant')->find($request->productId);
        $purchaseInvoiceItem = PurchaseInvoiceItem::find($request->id);
        $expiryDate = $purchaseInvoiceItem->expiry_date;
        $manufacturingDate = $purchaseInvoiceItem->manufacturing_date;
        $invoiceId = $purchaseInvoiceItem->purchase_invoice_id;
        $logoPath = env('LOGO_ABSOLUTE_PATH');
        $qty = $request->qty;
        if($qty % 2 != 0){
            $qty = $qty + 1;
        }
        $pdf = SnappyPdf
            ::setOrientation('portrait')
            ->setOption('page-width', '108')
            ->setOption('page-height', '25')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ->loadView('pdf.invoice_item_labels', compact('product','qty','logoPath','expiryDate','manufacturingDate', 'invoiceId'));
        return $pdf->stream();
    }
}
