<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Requests\Admin\Tax\EditRequest;
use Niyotail\Http\Requests\Admin\Tax\StoreRequest;
use Niyotail\Http\Requests\Admin\Tax\UpdateRequest;
use Niyotail\Models\Country;
use Niyotail\Models\State;
use Niyotail\Models\Tax;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Warehouse;
use Niyotail\Services\TaxService;
use Niyotail\Exceptions\ServiceException;

class TaxController extends Controller
{
    public function edit(EditRequest $request)
    {
        $tax = Tax::find($request->id);
        $countries = Country::get();
        $states = State::where('country_id', 101)->get();
        return view('admin.tax.form', compact('tax', 'countries', 'states'));
    }
    public function store(StoreRequest $request, TaxService $taxService)
    {
        $tax = $taxService->create($request);
        return response()->json($tax);
    }

    public function update(UpdateRequest $request, TaxService $taxService)
    {
        $tax = $taxService->update($request);
        return response()->json($tax);
    }
}
