<?php


namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\PurchaseOrder\CreatePurchaseOrderRequest;
use Niyotail\Services\AutoReplenishDemandService;
use Niyotail\Grids\DemandGenerationGrid;
use Niyotail\Services\PurchaseOrderService;

class DemandGenerationController extends Controller
{
    protected $autoReplenishDemandService;
    protected $purchaseOrderService;

    public function __construct()
    {
        $this->autoReplenishDemandService = new AutoReplenishDemandService();
        $this->purchaseOrderService = new PurchaseOrderService();
    }

    public function index (Request $request) {
        $grid = DemandGenerationGrid::get();
        if($request->ajax())
        {
            return $grid;
        }

        return view('admin.demand.index', compact('grid'));
    }

    public function submit (CreatePurchaseOrderRequest $request) {
        // create purchase order
        $vendorId = $request->input('vendor_id');
        $demandItemIds = $request->input('demand_item_ids');
        return $this->purchaseOrderService->createPurchaseOrderWithDemand($vendorId, $demandItemIds);
    }

    public function generateDemand() {
        $this->autoReplenishDemandService->generateDemandByProcedure();
        return redirect(route('admin::auto-replenish-demand.index'));
    }
}