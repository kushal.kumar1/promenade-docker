<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Niyotail\Helpers\InvoiceHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Invoice\RegenerateRequest;
use Niyotail\Models\Invoice;
use Niyotail\Models\Shipment;
use Niyotail\Services\InvoiceService;

class InvoiceController extends Controller
{
    public function index($id)
    {
        $invoice = Invoice::find($id);
        abort_if(empty($invoice), 404);
        return InvoiceHelper::downloadPdf($invoice);
    }

    public function regenerate(RegenerateRequest $request)
    {
        $invoice = Invoice::find($request->id);
        abort_if(empty($invoice), 404);

        $financialYear = $invoice->financial_year;
        $number = $invoice->number;
        return Artisan::call("invoice:regenerate $financialYear $number");
    }

    public function addEWayBillNumber(Request $request, InvoiceService $service)
    {
        $shipment = Shipment::with('invoice')->findOrFail($request->shipment_id);
        $service->addElectronicWayBillNumber($shipment->invoice, $request->eway_number);
    }
}
