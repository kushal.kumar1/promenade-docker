<?php

namespace Niyotail\Http\Controllers\Admin;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\ModificationRequests\ModificationRequestGrid;
use Niyotail\Http\Requests\Admin\Agent\ViewRequest;
use Niyotail\Http\Requests\Admin\ModificationRequests\ModificationRequestCreate;
use Niyotail\Http\Requests\Admin\ModificationRequests\ModificationRequestIndex;
use Niyotail\Models\ModificationRequest;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceItemModificationRequest;
use Niyotail\Models\PurchaseInvoiceModificationRequest;
use Niyotail\Models\TransactionDeletionRequest;
use Niyotail\Services\EmployeeService;
use Niyotail\Services\ModificationRequestService;
use Throwable;

class ModificationRequestController extends Controller
{
    private $modificationRequestService;
    private $employeeService;
    private $modificationRequest;

    public function __construct()
    {
        $this->modificationRequestService = new ModificationRequestService();
        $this->employeeService = new EmployeeService();
    }

    /**
     * @throws ServiceException
     */
    public function index(Request $request)
    {
        try {
            $grid = ModificationRequestGrid::get($request->modificationRequestType);
            if ($request->ajax()) {
                return $grid;
            }
            $params = [
                "grid" => $grid,
                "modificationRequestType" => $request->modificationRequestType,
                "deeplink" => false
            ];

            if ($request->hasHeader('referer') &&
                strpos($request->headers->get('referer'), "modification-requests") === false) {
//                if ($request->modificationRequestType == ModificationRequest::TYPE_TRANSACTION
//                    && !empty($request->deeplink)
//                    && !empty($request->transactionId)) {}

                if ($request->modificationRequestType == ModificationRequest::TYPE_PURCHASE_INVOICE
                    && !empty($request->deeplink)
                    && !empty($request->purchaseInvoiceId)) {
                    $purchaseInvoice = PurchaseInvoice::with(['vendor', 'vendorAddress', 'platform'])
                        ->find($request->purchaseInvoiceId);

                    if (!empty($purchaseInvoice)) {
                        $params["deeplink"] = true;
                        $params["purchaseInvoice"] = $purchaseInvoice;
                    }
                }

//                if ($request->modificationRequestType == ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM
//                    && !empty($request->deeplink)
//                    && !empty($request->purchaseInvoiceItemId)) {
//                }
            }
            return view('admin.modification_request.index', $params);
        } catch (Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * @throws ServiceException
     */
    public function create(Request $request)
    {
        foreach (ModificationRequest::getTypes() as $type) {
            if (in_array($type, str_replace("-", "_", explode("/", $request->getRequestUri())),
            )) {
                $request->modificationRequestType = $type;
            }
        }
        $modificationRequestType = $request->modificationRequestType;
        if (!ModificationRequest::isValidType($modificationRequestType)) {
            throw new ServiceException("Invalid parameter value passed for modificationRequestType:{$modificationRequestType}");
        }

        try {
            $modificationRequest = $this->modificationRequestService
                ->createModificationRequest($request)
                ->saveOrFail();

            return redirect()
                ->route(
                    "admin::modification-requests.index", [
                    "modificationRequestType" => $modificationRequestType
                ]);
        } catch (Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * @throws ServiceException
     */
    public function uploadPurchaseInvoiceImage(Request $request)
    {
        $modificationRequestType = $request->modificationRequestType;
        if ($request->modificationRequestType != ModificationRequest::TYPE_PURCHASE_INVOICE) {
            throw new ServiceException("Invalid parameter value passed for modificationRequestType:{$modificationRequestType}");
        }
        try {
            $uploadedImage = PurchaseInvoiceModificationRequest::uploadPurchaseInvoiceImage($request->file('upload_image'));
            return $uploadedImage;
        } catch (Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * @throws ServiceException
     * @throws Exception
     */
    public function downloadPurchaseInvoiceImage(Request $request)
    {
        $modificationRequestType = $request->modificationRequestType;
        if ($request->modificationRequestType != ModificationRequest::TYPE_PURCHASE_INVOICE) {
            throw new Exception("Invalid parameter value passed for modificationRequestType:{$modificationRequestType}");
        }
        try {
            $modificationRequestId = $request->modificationRequestId;
            $purchaseInvoiceModificationRequest = PurchaseInvoiceModificationRequest::findorfail($modificationRequestId);
            return response()->download($purchaseInvoiceModificationRequest->getPurchaseInvoiceImagePath());
//            return Storage::download($purchaseInvoiceModificationRequest->getPurchaseInvoiceImagePath());
        } catch (Exception $e) {
//            abort(404);
            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * @throws ServiceException
     */
    public function delete(Request $request)
    {
        foreach (ModificationRequest::getTypes() as $type) {
            if (in_array($type, str_replace("-", "_", explode("/", $request->getRequestUri())),
            )) {
                $request->modificationRequestType = $type;
            }
        }
        $modificationRequestType = $request->modificationRequestType;
        if (!ModificationRequest::isValidType($modificationRequestType)) {
            throw new ServiceException("Invalid parameter value passed for modificationRequestType:{$modificationRequestType}");
        }

        try {
            if (!$this->modificationRequestService->deleteModificationRequest($request)) {
                throw new Exception("Failed to delete Modification Request");
            }
            return redirect()->route(
                "admin::modification-requests.index", [
                "modificationRequestType" => $request->modificationRequestType
            ]);
        } catch (Exception $e) {
            throw new ServiceException($e->getMessage());
        }
    }

}