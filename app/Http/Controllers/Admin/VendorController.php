<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\VendorGrid;
use Niyotail\Grids\VendorBankAccountGrid;
use Niyotail\Grids\VendorAddressesGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Vendor\ViewRequest;
use Niyotail\Http\Requests\Admin\Vendor\UpdateRequest;
use Niyotail\Http\Requests\Admin\Vendor\Address\CreateAddressRequest;
use Niyotail\Http\Requests\Admin\Vendor\VendorAccountRequest;

// use Niyotail\Services\UserService;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;
use Niyotail\Models\Pincode;
use Niyotail\Models\Vendor;
use Niyotail\Services\VendorService;
use Niyotail\Models\VendorBankAccount;

/**
 * Class VendorController
 *
 * @package Niyotail\Http\Controllers\Admin
 *
 */
class VendorController extends Controller
{
    public function index (ViewRequest $request)
    {
        $grid = VendorGrid::get();
        
        if ($request->ajax()) {
            return $grid;
        }
        
        return view('admin.vendor.index', [
            'grid' => $grid
        ]);
    }

    public function add ()
    {
        $labelSelect = ['trading', 'non-trading'];
        $incorporationTypeSelect = ['private limited', 'partnership', 'llp', 'proprietorship', 'individual', 'public limited', 'government', 'society', 'association', 'non-profit', 'international', 'huf'];
        
        return view('admin.vendor.form')->with(compact('labelSelect', 'incorporationTypeSelect'));
    }

    public function create (UpdateRequest $request, VendorService $vendorService)
    {
        return $vendorService->create($request);
    }
    
    public function edit ($id)
    {
        $vendor = Vendor::find($id);
        abort_if(empty($vendor), 404);
       
        $labelSelect = ['trading', 'non-trading'];
        $incorporationTypeSelect = ['private limited', 'partnership', 'llp', 'proprietorship', 'individual', 'public limited', 'government', 'society', 'association', 'non-profit', 'international', 'huf'];
      
        return view('admin.vendor.form', compact('vendor', 'labelSelect', 'incorporationTypeSelect'));
    }

    public function update (UpdateRequest $request, VendorService $vendorService, $id)
    {
        $vendorService->update($request, $id);
      
        return redirect()->back()->withSuccess("Vendor information has been updated.");
    }

    public function addresses (ViewRequest $request)
    {
        $grid = VendorAddressesGrid::get();
        
        if ($request->ajax()) {
            return $grid;
        }
        
        return view('admin.vendor.address.index', [
            'grid' => $grid
        ]);
    }

    public function editAddress ($id)
    {
        $vendor = Vendor::find($id);
        abort_if (empty($vendor), 404);
       
        $addresses = $vendor->addresses;
        
        return view('admin.vendor.address.edit', compact('vendor', 'addresses'));
    }

    public function createAddress (CreateAddressRequest $request, VendorService $vendorService, $id)
    {
        $vendorService->createAddress($request, $id);
        
        return redirect()->back()->withSuccess("Vendor Address has been added.");
    }

    public function updateAddress (CreateAddressRequest $request, VendorService $vendorService, $id, $addressId)
    {
        $vendorService->updateAddress($request, $id, $addressId);
        return redirect()->back()->withSuccess("Vendor Address has been updated.");
    }
    
    public function deleteAddress (VendorService $vendorService, $id, $addressId)
    {
        $vendorService->deleteAddress($id, $addressId);
      
        return redirect()->back()->withSuccess("Vendor Address has been deleted.");
    }

    public function bankAccounts (ViewRequest $request)
    {
        $grid = VendorBankAccountGrid::get();
        
        if ($request->ajax()) {
            return $grid;
        }
        
        return view('admin.vendor.bank_account.index', [
            'grid' => $grid
        ]);
    }

    public function createBankAccount (VendorAccountRequest $request, VendorService $vendorService, $id)
    {
        $vendorService->createBankAccount($request, $id);

        return redirect()->back()->withSuccess("Vendor Bank Account has been added.");

    }

    public function editBankAccount ($id)
    {
        $vendor = Vendor::find($id);
        abort_if (empty($vendor), 404);

        $accounts = VendorBankAccount::where('vendor_id', $id)->get();

        return view('admin.vendor.bank_account.edit', compact('vendor', 'accounts'));
    }

    public function updateBankAccount ($id, $accountId, Request $request, VendorService $vendorService)
    {
        $vendorService->updateBankAccount($id, $accountId, $request);
        
        return redirect()->back()->withSuccess("Vendor Bank Account has been updated.");
    }

    public function deleteBankAccount (VendorService $vendorService, $id, $accountId)
    {
        $vendorService->deleteBankAccount($id, $accountId);

        return redirect()->back()->withSuccess("Vendor Bank Account has been deleted.");
    }
}
