<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Models\AppBanner;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\AppBannerService;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\AppBannerGrid;
use Niyotail\Http\Requests\Admin\AppBanner\CreateRequest;
use Niyotail\Http\Requests\Admin\AppBanner\UpdateRequest;
use Niyotail\Http\Requests\Admin\AppBanner\ViewRequest;

class AppBannerController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = AppBannerGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.app_banner.index', compact('grid'));
    }

    public function edit(ViewRequest $request, $id)
    {
        $appBanner = AppBanner::find($id);
        abort_if(empty($appBanner), 404);
        return view('admin.app_banner.edit', compact('appBanner'));
    }

    public function create()
    {
        return view('admin.app_banner.create');
    }
    public function store(CreateRequest $request, AppBannerService $appBannerService)
    {
        $appBanner = $appBannerService->create($request);
        return response()->json($appBanner);
    }

    public function update(UpdateRequest $request, AppBannerService $appBannerService)
    {
        $appBanner = $appBannerService->update($request);
        return response()->json($appBanner);
    }
}
