<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Niyotail\Grids\ProfileQuestionOptionsGrid;
use Niyotail\Grids\ProfileQuestionsGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\ProfileQuestions\OptionRequest;
use Niyotail\Http\Requests\Admin\ProfileQuestions\QuestionRequest;
use Niyotail\Models\ProfileQuestion;
use Niyotail\Models\ProfileQuestionOption;


class ProfileQuestionsController extends Controller
{
    public function index(Request $request)
    {
        $grid = ProfileQuestionsGrid::get();
        if($request->ajax())
        {
            return $grid;
        }

        return view('admin.profile_questions.index', compact('grid'));
    }

    public function edit($id = null, Request $request)
    {
        $response = array();

        if(!empty($id))
        {
            $response['question'] = ProfileQuestion::find($id)->toArray();
        }
        return view('admin.profile_questions.form', $response);
    }

    public function manageQuestion(QuestionRequest $request)
    {
        $data = [
            "name" => $request->name,
            "type" => $request->type,
            "status" => $request->status,
            "is_mandatory" => $request->is_mandatory,
            "priority" => $request->priority
        ];

        $operation = "created";

        if($request->has('id'))
        {
            $data['id'] = $request->id;
            $question = ProfileQuestion::updateQuestion($data);
            $operation = "updated";
        }
        else
        {
            $question = ProfileQuestion::createQuestion($data);
        }

        $message = "Profile question ".$operation." successfully";

        return redirect(route("admin::profile-questions.index"))->with("message", $message);
    }

    public function options($qId, Request $request)
    {
        $grid = ProfileQuestionOptionsGrid::get(["q_id" => $qId]);
        if($request->ajax())
        {
            return $grid;
        }

        return view('admin.profile_questions.options.index', compact('grid'));
    }

    public function editOption($qId, $optionId = null, Request $request)
    {
        $response = array();
        $response['question'] = ProfileQuestion::find($qId)->toArray();
        if(!empty($optionId))
        {
            $response['option'] = ProfileQuestionOption::find($optionId)->toArray();
        }
        return view('admin.profile_questions.options.form', $response);
    }

    public function manageOption(OptionRequest $request)
    {
        $data = [
            "question_id" => $request->question_id,
            "option" => $request->option,
            "priority" => $request->priority,
            "status" => $request->status
        ];

        $operation = "created";

        if($request->has('id'))
        {
            $data['id'] = $request->id;
            $option = ProfileQuestionOption::updateOption($data);
            $operation = "updated";
        }
        else
        {
            $option = ProfileQuestionOption::createOption($data);
        }

        $message = "Profile question option ".$operation." successfully";

        return redirect(route("admin::profile-questions.options", $request->question_id))->with("message", $message);
    }
}