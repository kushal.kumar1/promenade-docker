<?php

namespace Niyotail\Http\Controllers\admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\ChequeMetaService;
use Niyotail\Http\Requests\Admin\ChequeMeta\CreateRequest;
use Niyotail\Http\Requests\Admin\ChequeMeta\UpdateRequest;
use Niyotail\Http\Requests\Admin\ChequeMeta\DeleteRequest;

class ChequeMetaController extends Controller
{
    public function store(CreateRequest $request, ChequeMetaService $chequeMetaService)
    {
        return $chequeMetaService->create($request);
    }

    public function update(UpdateRequest $request, ChequeMetaService $chequeMetaService)
    {
        $chequeMetaService->update($request);
        return response(['message' => 'Cheque info has been updated']);
    }

    public function delete(DeleteRequest $request, ChequeMetaService $chequeMetaService)
    {
        $chequeMetaService->delete($request);
        return true;
    }

}
