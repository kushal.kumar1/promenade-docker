<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Grids\Orders\OrderGrid;
use Niyotail\Helpers\Utils;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Orders\AssignAgentRequest;
use Niyotail\Http\Requests\Admin\Orders\CancelOrderItemRequest;
use Niyotail\Http\Requests\Admin\Orders\CancelRequest;
use Niyotail\Http\Requests\Admin\Orders\CreateRemarkRequest;
use Niyotail\Http\Requests\Admin\Orders\DetailsRequest;
use Niyotail\Http\Requests\Admin\Orders\ViewRequest;
use Niyotail\Models\Agent;
use Niyotail\Models\Order;
use Niyotail\Models\Product;
use Niyotail\Models\Shipment;
use Niyotail\Models\User;
use Niyotail\Services\Order\OrderService;

class OrderController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = OrderGrid::get(['picklist' => false]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.order.index', [
            'grid' => $grid,
            'title' => 'Orders'
        ]);
    }

    public function createPicklistOrders(ViewRequest $request)
    {
        $grid = OrderGrid::get(['picklist' => true]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.order.index', [
            'grid' => $grid,
            'title' => 'Ready to Pick Orders'
        ]);
    }


    public function assignAgent($id, AssignAgentRequest $request, OrderService $orderService)
    {
        $order = Order::find($id);
        $agent = Agent::active()->find($request->agent_id);
        abort_if(empty($order) && empty($agent), 422);

        $order = $orderService->assignAgent($order, $agent);
        return response()->json($order);
    }

    //Disabled because of a bug
//    public function updateItems($id, UpdateOrderRequest $request, OrderService $orderService)
//    {
//        $order = Order::find($id);
//        abort_if(empty($order), 404);
//
//        $orderService->setAuthority(OrderService::AUTHORITY_ADMIN)->updateItems($order, $request);
//    }


    public function cancel(CancelRequest $request, OrderService $orderService)
    {
        $order = $orderService->cancel($request->id, $request->reason);
        return response()->json($order);
    }

    public function detail(DetailsRequest $request, $id)
    {
        $order = Order::with('shippingAddress', 'billingAddress', 'store', 'user', 'warehouse', 'additionalInfo')
            ->with('items.product.images', 'shipments.orderItems', 'shipments.invoice', 'returns.orderItems.product', 'returns.creditNote')
            ->with(['logs' => function ($logsQuery) {
                $logsQuery->orderBy('created_at', 'desc')->orderBy('id', 'desc')->with('employee');
            }])
            ->with(['invoices' => function ($query) {
                $query->doesntHave('shipment')
                    ->orWhereHas('shipment', function ($query) {
                        $query->whereIn('status', [Shipment::STATUS_DELIVERED, Shipment::STATUS_DISPATCHED]);
                    });
            }])
            ->where('id', $id)
            ->first();
        abort_if(empty($order), 404);

        $order->loadMissing('items.productVariant');
        return view('admin.order.detail', compact('order'));
    }

    public function products(Request $request)
    {
        $products = Product::searchWithInventoryCount($request->term, (Utils::getStore()->id));
        return view('admin.order.products', compact('products'));
    }

    public function customers(Request $request)
    {
        $term = $request->term;
        $customers = User::where("first_name", "like", "%$term%")
            ->orWhere("last_name", "like", "%$term%")
            ->orWhere("mobile", "like", "%$term%")
            ->orWhere("email", "like", "%$term%")
            ->limit(10)->get();

        return view('admin.order.customers', ['customers' => $customers]);
    }

    public function storeRemarks($orderId, CreateRemarkRequest $request, OrderService $orderService)
    {
        $order = Order::find($orderId);
        $orderService->setAuthority(OrderService::AUTHORITY_ADMIN)->createRemarks($order, $request);
        return response()->json($order);
    }

    public function cancelOrderItem(CancelOrderItemRequest $request, $orderId, OrderService $orderService)
    {
        $order = Order::find($orderId);
        abort_if(empty($order), 422);
        $order = $orderService->cancelItems($order, $request);
        return response()->json($order);
    }
}
