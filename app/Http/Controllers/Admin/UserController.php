<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\UserGrid;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\User\ViewRequest;
use Niyotail\Models\User;
use Niyotail\Models\City;
use Niyotail\Models\Order;
use Niyotail\Services\UserService;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;

/**
 * Class UserController
 *
 * @package Niyotail\Http\Controllers\Admin
 *
 */
class UserController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = UserGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.user.index', [
            'grid' => $grid
        ]);
    }

    public function profile(ViewRequest $request, $id)
    {
        $user = User::with('stores')
        ->with(['orders' =>  function ($query) {
            $query->with('items')->orderBy('created_at', 'desc')->limit(5);
        }])
        ->withCount(['orders' => function ($query) {
            $query->whereNotIn('status', [Order::STATUS_CANCELLED]);
        }])
        ->with('badges')
        ->find($id);

        abort_if(empty($user), 404);
        $cities = City::get();
        return view('admin.user.profile', compact('user', 'cities'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        abort_if(empty($user), 404);

        return view('admin.user.form', compact('user'));
    }

    public function store(Requests\Admin\User\CreateRequest $request, UserService $userService)
    {
        $user = $userService->create($request);
        return response()->json($user);
    }

    public function update(Requests\Admin\User\UpdateRequest $request, UserService $userService)
    {
        $user = User::find($request->id);
        $user = $userService->setAuthority(UserService::AUTHORITY_ADMIN)->update($request, $user);
        return response()->json($user);
    }

    public function updateStatus(Request $request, UserService $userService)
    {
        $user = User::find($request->id);
        $user = $userService->setAuthority(UserService::AUTHORITY_ADMIN)->saveStatus($request, $user);
        return response()->json($user);
    }

    public function storeNotes(Request $request, UserService $userService)
    {
        $user = User::find($request->id);
        $user = $userService->setAuthority(UserService::AUTHORITY_ADMIN)->saveNotes($request, $user);
        return response()->json($user);
    }

    public function updateBadges(Requests\Admin\User\UpdateBadgeRequest $request, $id, UserService $userService)
    {
        $user = User::find($request->id);
        $user = $userService->setAuthority(UserService::AUTHORITY_ADMIN)->updateBadges($request, $user);
        return response()->json($user);
    }
}
