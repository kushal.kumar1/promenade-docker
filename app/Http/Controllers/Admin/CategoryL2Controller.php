<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\CategoryL2Grid;
use Niyotail\Models\CategoryL1;
use Niyotail\Http\Requests\Admin\CategoryL2\ViewRequest;
use Niyotail\Http\Requests\Admin\CategoryL2\CreateRequest;
use Niyotail\Http\Requests\Admin\CategoryL2\UpdateRequest;
use Niyotail\Models\CategoryL2;
use Niyotail\Services\CategoryL2Service;

class CategoryL2Controller extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = CategoryL2Grid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.category-l2.index', [
            'grid' => $grid
        ]);
    }

    public function create()
    {
        return view('admin.category-l2.form');
    }

    public function store(CreateRequest $request, CategoryL2Service $service)
    {
        return $service->store($request);
    }

    public function edit($id)
    {
        $category = CategoryL2::with('collection')->find($id);
        abort_if(empty($category), 404);
        return view('admin.category-l2.form')->with(compact('category'));
    }

    public function update($id, UpdateRequest $request, CategoryL2Service $service)
    {
        return $service->update($request, $id);
    }
}
