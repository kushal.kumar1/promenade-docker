<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\ShipmentGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Shipments\CancelRequest;
use Niyotail\Http\Requests\Admin\Shipments\DeliveredRequest;
use Niyotail\Http\Requests\Admin\Shipments\RetryRequest;
use Niyotail\Http\Requests\Admin\Shipments\RtoRequest;
use Niyotail\Http\Requests\Admin\Shipments\UpdateBoxRequest;
use Niyotail\Http\Requests\Admin\Shipments\ViewRequest;
use Niyotail\Models\Shipment;
use Niyotail\Services\Order\ShipmentService;

class ShipmentController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ShipmentGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.shipment.index', [
            'grid' => $grid
        ]);
    }

    public function updateBoxQuantity(UpdateBoxRequest $request, ShipmentService $shipmentService)
    {
        $shipment = $shipmentService->setAuthority(ShipmentService::AUTHORITY_ADMIN)->updateBoxes($request);
        return response()->json($shipment);
    }

    public function cancel(CancelRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($request->id);
        $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_CANCELLED);
        return response()->json($shipment);
    }

    public function retry(RetryRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($request->id);
        if ($request->has('type') && $request->type == 'reject') {
            $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_DISPATCHED);
        } else {
            $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_RETRY);
        }
        return response()->json($shipment);
    }

    public function rto(RtoRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($request->id);
        if ($request->has('type') && $request->type == 'reject') {
            $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_DISPATCHED);
        } else {
            $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_RTO);
        }
        return response()->json($shipment);
    }

    public function deliver(DeliveredRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($request->id);
        abort_if(empty($shipment), 404);
        try{
            if(!empty($shipment->otp)){
                $shipmentService->verifyOtp($shipment, $request->otp);
            }
            $shipment = $shipmentService->updateShipment($shipment, Shipment::STATUS_DELIVERED);
        }catch (ServiceException $exception){
            return response(['error'=>$exception->getMessage(),'message'=>$exception->getMessage()],422);
        }
        return response()->json($shipment);
    }

    public function resendOTP(ViewRequest $request, ShipmentService $shipmentService)
    {
        $shipment = Shipment::find($request->id);
        abort_if(empty($shipment), 404);

        $shipmentService->resendOTP($shipment);
        return response()->json($shipment);
    }
}
