<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Niyotail\Helpers\RawQueryHelper;
use Niyotail\Http\Controllers\Controller;


class DailyReportDownloadController extends Controller
{
    public function takeDate() {
        return view('admin.purchase_invoice.report.reportDownload');
    }

    public function dailyPurchaseReportDownload(Request $request) {
        $this->validate($request, [
            'start_at'    => 'required|date',
            'end_at'      => 'required|date|after_or_equal:start_at',
        ]);

        $query = RawQueryHelper::getPurchaseReportQuery($request->start_at, $request->end_at, $request->report_as);
        $delimiter = ",";
        $filename = "DPR" . date('Y-m-d') . ".csv";
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('Voucher_No', 'Voucher_Date', 'Supplier_Invoice_Date', 'Supplier_Invoice_Number', 'Vendor_Id', 'Vendor_Name', 'Add1', 'Add2', 'Add3', 'State', 'GSTIN_No', 
        'GSTIN_Type', 'PID', 'Mobile', 'Email', 'Item_Description', 'HSNCode', 'SKU', 'Invoice_Quantity', 'Units_In_SKU', 'Total_Units', 'Unit_Rate', 'Gross_Amount', 'Freight', 'Pre_Tax_Discount'
        ,'Taxable', 'GST_Perc', 'CESS_Perc', 'SPL_CESS_Per_Unit', 'SGST', 'CGST', 'IGST', 'CESS', 'SPL_CESS', 'TCS', 'Total_Amount', 'Post_Tax_Discount', 'Round_Off', 'Invoice_Total', 'Narration', 'Vehicle_No', 'Ewaybill_No', 'GRN_Qty_In_Units'  
    );
        fputcsv($f, $fields, $delimiter);
        foreach($query as $row){
            // DD($row);
            $rowData = array($row->Voucher_No, $row->Voucher_Date, $row->Supplier_Invoice_Date, $row->Supplier_Invoice_Number, $row->Vendor_Id, $row->Vendor_Name, $row->Add1, $row->Add2, $row->Add3, $row->State, $row->GSTIN_No, $row->GSTIN_Type, $row->PID, $row->Mobile, $row->Email, $row->Item_Description, $row->HSNCode, $row->SKU,
            $row->Invoice_Quantity, $row->Units_In_SKU, $row->Total_Units, $row->Unit_Rate, $row->Gross_Amount, $row->Freight, $row->Pre_Tax_Discount, $row->Taxable, $row->GST_Perc, 
            $row->CESS_Perc, $row->SPL_CESS_Per_Unit, $row->SGST, $row->CGST, $row->IGST, $row->CESS, $row->SPL_CESS, $row->TCS, $row->Total_Amount, $row->Post_Tax_Discount, 
            $row->Round_Off, $row->Invoice_Total, $row->Narration, $row->Vehicle_No, $row->Ewaybill_No, $row->GRN_Qty_In_Units
        );
            fputcsv($f, $rowData, $delimiter);
        }
        
        //move back to beginning of file
        fseek($f, 0);
        
        //set headers to download file rather than displayed
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        //output all remaining data on a file pointer
        fpassthru($f);
    }
}