<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Contracts\Authentication\AdminAuthContract;
use Niyotail\Helpers\Authentication\AdminAuthHelper;
use Niyotail\Helpers\Response\AdminResponse;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;
use Illuminate\Support\Facades\Redirect;

/**
 * Class AuthController
 *
 * @package Niyotail\Http\Controllers\Authentication\Admin
 */
class AuthController extends Controller implements AdminAuthContract
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.login');
    }


    /**
     * @param Requests\Admin\Auth\LoginRequest $request
     * @param AdminAuthHelper $employeeAuthHelper
     * @return mixed
     */
    public function login(Requests\Admin\Auth\LoginRequest $request, AdminAuthHelper $employeeAuthHelper)
    {
        return $employeeAuthHelper->execute($request, $this);
    }

    /**
     * @param \Illuminate\Http\Request                           $request
     * @param \Niyotail\Helpers\Authentication\AdminAuthHelper $employeeAuthHelper
     *
     * @return mixed
     */
    public function logout(Request $request, AdminAuthHelper $employeeAuthHelper)
    {
        return $employeeAuthHelper->logout($request, $this);
    }

    /**
     * @param $user
     *
     * @return mixed
     */
    public function userHasLoggedIn($user)
    {
        if($user->warehouses->count() > 1) {
            return redirect(route('admin::choose-warehouse'));
        }
        session()->put('selected-warehouse', $user->warehouses->first()->id);
        return redirect(route('admin::dashboard'));
    }

    /**
     * @param $message
     *
     * @return mixed
     */
    public function userLogInFailed($message)
    {
        return AdminResponse::getInstance()->error($message);
    }

    /**
     * @return mixed
     */
    public function userIsBlocked()
    {
        return AdminResponse::getInstance()->error('Your Account is inactive');
    }

    /**
     * @return mixed
     */
    public function userHasLoggedOut()
    {
        return redirect(route('admin::login'));
    }
}
