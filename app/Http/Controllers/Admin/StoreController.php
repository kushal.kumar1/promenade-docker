<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Store\CreateRequest;
use Niyotail\Http\Requests\Admin\Store\UpdateRequest;
use Niyotail\Http\Requests\Admin\StoreOrder\StoreOrderRequest;
use Niyotail\Models\City;
use Niyotail\Models\Order;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\StoreAnswer;
use Niyotail\Models\Tag;
use Niyotail\Models\Warehouse;
use Niyotail\Services\UserService;
use Niyotail\Services\StoreService;
use Niyotail\Services\CreditLimitService;
use Illuminate\Http\Request;
use Niyotail\Models\Store;
use Niyotail\Models\User;
use Niyotail\Grids\StoreGrid;
use Niyotail\Models\Cart;
use Niyotail\Models\ReturnCart;
use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Requests\Admin\Store\ViewRequest;
use Niyotail\Http\Requests\Admin\Store\StatusRequest;
use Niyotail\Http\Requests\Admin\Store\AddUserRequest;
use Niyotail\Http\Requests\Admin\Store\RemoveUserRequest;
use Niyotail\Http\Requests\Admin\Store\ViewStoreProfileRequest;
use Niyotail\Http\Requests\Admin\CreditLimit\UpdateRequest as CreditLimitUpdateRequest;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\RuleEngine;

class StoreController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = StoreGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.store.index', compact('grid'));
    }

    public function profile(ViewStoreProfileRequest $request, CreditLimitService $service)
    {
        $storeId = $request->id;

        $store = Store::with('users', 'beat', 'pincode', 'creditLimit')
            ->with(['transactions' => function ($query) {
                $query->where('status', 'completed')->orderBy('id', 'desc')->limit(5);
            }])
            ->with(['orders' => function ($query) {
                $query->orderBy('created_at', 'desc')->limit(5);
            }])
            ->find($storeId);

        abort_if(empty($store), 404);
        $autoCreditLimit = $service->getAutoCreditLimit($store);

        $storeAnswers = StoreAnswer::with(["question", "agent"])->StoreId($storeId)->get();

        $tags = Tag::store()->get();

        $selectedTagIds = $store->tags->pluck('id')->toArray();

        $projectedOutstanding = Order::where('store_id', $storeId)
            ->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_MANIFESTED])
            ->sum('total');


        return view('admin.store.profile', compact('store', 'autoCreditLimit',
            'storeAnswers', 'tags', 'selectedTagIds', 'projectedOutstanding'));
    }

    public function cart($id, StoreOrderRequest $request)
    {
        $warehouseId = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        $store = Store::with('totalBalance')->find($id);
        abort_if(empty($store) || !$store->canCreateOrder(), 404);

        $employee = Auth::guard('admin')->user();
        $warehouses = Warehouse::active()->get();

        $cart = Cart::byEmployee($employee)->where('store_id', $id)
            ->where('warehouse_id', $warehouseId)
            ->with('items.productVariant')
            ->orderBy('created_at', 'desc')
            ->latest()->first();

        if (!empty($cart)) {
            $cart->load('items.product.images');
        }

        $tags = Tag::order()->active()->get();

        return view('admin.cart.index', compact('store', 'cart','warehouses', 'warehouseId', 'tags'));
    }

    public function store(CreateRequest $request, StoreService $storeService, UserService $userService)
    {
        $store = DB::transaction(function () use ($storeService, $userService, $request) {
            $store = $storeService->setAuthority(StoreService::AUTHORITY_ADMIN)->create($request);

            // Add user from the contact info of store
            $user = User::where('mobile', $request->contact_mobile)->first();
            if (empty($user)) {
                $userRequest = new Request();
                $userRequest->merge(['mobile' => $request->contact_mobile]);
                $userRequest->merge([
                    'first_name' => split_name($request->contact_person)[0],
                    'last_name' => split_name($request->contact_person)[1]
                ]);
                $user = $userService->create($userRequest);
            }
            $store->users()->attach($user->id);
            return $store;
        });

        return response()->json($store);
    }

    public function update(UpdateRequest $request, StoreService $storeService)
    {
        $store = Store::find($request->id);
        abort_if(empty($store), 404);
        $store = $storeService->setAuthority(StoreService::AUTHORITY_ADMIN)->update($request, $store);
        return response()->json($store);
    }

    public function addUser(AddUserRequest $request, StoreService $storeService)
    {
        $store = $storeService->setAuthority(StoreService::AUTHORITY_ADMIN)->addUser($request);
        return response()->json($store);
    }

    public function removeUser(RemoveUserRequest $request, StoreService $storeService)
    {
        $store = $storeService->setAuthority(StoreService::AUTHORITY_ADMIN)->removeUser($request);
        return response()->json($store);
    }

    public function updateStatus(StatusRequest $request, StoreService $service)
    {
        $service->setAuthority(StoreService::AUTHORITY_ADMIN)->updateStatus($request);
    }

    public function updateCreditLimit(CreditLimitUpdateRequest $request, CreditLimitService $service)
    {
        $service->override($request);
    }

    public function returnCart($id)
    {
        $store = Store::with('beat', 'totalBalance', 'creditLimit')->find($id);
        abort_if(empty($store), 404);

        $employee = Auth::guard('admin')->user();

        $cart = ReturnCart::byEmployee($employee)->where('store_id', $id)
            ->with('items.productVariant.product.images')->orderBy('created_at', 'desc')->first();

        return view('admin.return.order.index', compact('store', 'cart'));
    }
}
