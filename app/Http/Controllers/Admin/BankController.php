<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\BankGrid;
use Niyotail\Http\Requests\Admin\Bank\CreateRequest;
use Niyotail\Http\Requests\Admin\Bank\UpdateRequest;
use Illuminate\Http\Request;
use Niyotail\Services\BankService;

class BankController extends Controller
{
    public function index(Request $request)
    {
        $grid = BankGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.bank.index', compact('grid'));
    }

    public function store(CreateRequest $request, BankService $bankService)
    {
        return $bankService->create($request);
    }

    public function update(UpdateRequest $request, BankService $bankService)
    {
        $bankService->update($request);
        return response(['message' => 'Bank name has been updated']);
    }
}