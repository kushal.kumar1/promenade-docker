<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Niyotail\Grids\WarehouseGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Warehouses\CreateRequest;
use Niyotail\Http\Requests\Admin\Warehouses\UpdateRequest;
use Niyotail\Http\Requests\Admin\Warehouses\ViewRequest;
use Niyotail\Models\City;
use Niyotail\Models\Warehouse;
use Niyotail\Services\WarehouseService;

class WarehouseController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = WarehouseGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.warehouse.index', [
            'grid' => $grid,
        ]);
    }

    public function create()
    {
        $cities = City::get();
        return view('admin.warehouse.create', compact('cities'));
    }

    public function edit($id, ViewRequest $request)
    {
        $warehouse = Warehouse::where('id', $id)->first();
        $cities = City::get();
        abort_if(empty($warehouse), 404);
        return view('admin.warehouse.edit', compact('warehouse', 'cities'));
    }

    public function store(CreateRequest $request, WarehouseService $warehouseService)
    {
        $warehouse = $warehouseService->create($request);
        return response()->json($warehouse);
    }

    public function update(UpdateRequest $request, WarehouseService $warehouseService)
    {
        $warehouse = $warehouseService->update($request);
        return response()->json($warehouse);
    }

    public function selectWarehouse(Request $request)
    {
        $request->session()->put('selected-warehouse', $request->warehouse_id);
        return redirect(route('admin::dashboard'));
    }

    public function chooseWarehouse(Request $request)
    {
        $warehouses = Auth::user()->warehouses;
        return view('admin.choose-warehouse', compact('warehouses'));
    }
}
