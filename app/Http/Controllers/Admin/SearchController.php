<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Agent;
use Niyotail\Models\Attribute;
use Niyotail\Models\Badge;
use Niyotail\Models\Beat;
use Niyotail\Models\BeatMarketer;
use Niyotail\Models\Brand;
use Niyotail\Models\CategoryL2;
use Niyotail\Models\City;
use Niyotail\Models\Collection;
use Niyotail\Models\Country;
use Niyotail\Models\Employee;
use Niyotail\Models\Marketer;
use Niyotail\Models\NewCatL4;
use Niyotail\Models\Product;
use Niyotail\Models\ProductGroup;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\PurchasePlatform;
use Niyotail\Models\Reason;
use Niyotail\Models\State;
use Niyotail\Models\Storage;
use Niyotail\Models\Store;
use Niyotail\Models\Tag;
use Niyotail\Models\TaxClass;
use Niyotail\Models\Transaction;
use Niyotail\Models\User;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Models\Warehouse;
use Niyotail\Models\WarehouseRack;
use Niyotail\Models\Picklist;

//use Niyotail\Models\VariantType;

class SearchController extends Controller
{
    public function attributes(Request $request)
    {
        $term = $request->term;
        $attributes = Attribute::where("name", "like", "%$term%")->limit(10)->get();

        return $attributes;
    }

    public function users(Request $request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        $users = User::where("first_name", "like", "%$term%")
            ->orWhere("last_name", "like", "%$term%")
            ->orWhere("mobile", "like", "%$term%")
            ->orWhere("email", "like", "%$term%")
            ->orWhereRaw("CONCAT(first_name, ' ', last_name) like '%$term%'")
            ->limit(10)->get();
        return $users;
    }

    public function agents(Request $request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        $type = $request->has('type') ? $request->type : null;
        $query = Agent::active()
            ->where(function ($q) use ($term) {
                $q->where("name", "like", "%$term%")
                    ->orWhere("code", "like", "%$term%");
            });
        if (!empty($type)) {
            $query->where('type', $type);
        }
        $agents = $query->limit(10)->get();
        return $agents;
    }

    public function beats(Request $request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        return Beat::searchWithKeyword($term);
    }

    public function badges(Request $request)
    {
        $term = $request->term;
        $badges = Badge::where("name", "like", "%$term%")->limit(10)->get();

        return $badges;
    }

    public function variantType(Request $request)
    {
//        return VariantType::where('name', 'like', "%$request->term%")->limit(10)->get();
    }

    public function states(Request $request)
    {
        $query = State::where('name', 'like', "%$request->term%");
        if ($request->countryId != '') {
            $query = $query->where('country_id', $request->countryId);
        }
        return $query->limit(10)->get();
    }

    public function countries(Request $request)
    {
        return Country::where('name', 'like', "%$request->term%")->limit(10)->get();
    }

    public function cities(Request $request)
    {
        return City::where('name', 'like', "%$request->term%")->limit(10)->get();
    }

    public function collections(Request $request)
    {
        return Collection::searchByKeyword($request->term, 10);
    }

    public function stores(Request $request)
    {
        $term = $request->term;
//        $term = str_replace(' ', '%', $term);
        return Store::searchWithKeyword($term, 10, ['beat']);
    }

    public function products(Request $request)
    {
        $term = str_replace(' ', '%', $request->term);
        if (!empty($request->marketer_id)) {
            $query = Product::where('name', 'like', "%$term%")
                ->with('variants');
            $query->whereHas('brand', function ($query) use ($request) {
                $query->where('marketer_id', $request->marketer_id);
            });
            return $query->limit(10)->get();
        }
        return Product::searchSimpleWithKeyword($term, 10, ['variants', 'taxClass']);
    }

    //TODO:: Project search shows that this function is not being, remove if not being used anywhere, commenting route and code
//    public function searchProductsByBarcode(Request $request)
//    {
//        $barcode = $request->input('barcode');
//        $products = Product::searchSimpleWithBarcode($barcode, 5, ['variants','taxClass']);
//
//        if(!$products->count() && $request->has('fallback') && $request->input('fallback') == "product_request")
//        {
//            // No result from product table
//            // fetch from product request
//            $products = ProductRequest::searchByBarcode($barcode);
//        }
//
//        if($request->has('is_html') && $request->input('is_html'))
//        {
//            $viewTemplate = $request->input('is_html');
//            return view($viewTemplate, compact('products'));
//        }
//    }

    public function variants(Request $request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        return ProductVariant::with('product')
            ->whereHas('product', function ($subQuery) use ($term) {
                $subQuery->where(function ($q) use ($term) {
                    $q->where("name", "like", "%$term%")
                        ->orWhere("id", $term);
                })
                    ->where('status', 1);
            })
            ->limit(10)->get();
    }

    public function brands(Request $request)
    {
        $term = $request->term;
        return Brand::searchWithKeyword($term);
    }

    public function tags(Request $request)
    {
        $term = $request->term;
        $tags = Tag::where("name", "like", "%$term%")->limit(10)->get();

        return $tags;
    }

    public function racks(Request $request)
    {
        $term = $request->term;
        $tags = WarehouseRack::where("reference", "like", "%$term%")->limit(10)->get();

        return $tags;
    }

    public function marketers(Request $request)
    {
        $term = $request->term;
        $marketers = Marketer::where(function ($q) use ($term) {
            $q->where("name", "like", "%$term%")
                ->orWhere("alias", "like", "%$term%")
                ->orWhere("code", "like", "%$term%");
        })
            ->limit(10)->get();

        return $marketers;
    }

    public function groups(Request $request)
    {
        $term = $request->term;
        return ProductGroup::where("name", "like", "%$term%")
            ->orWhere("id", "like", "%$term%")->limit(10)->get();
    }

    public function cl2(Request $request)
    {
        $term = $request->term;
        return CategoryL2::where("name", "like", "%$term%")
            ->orWhere("id", "like", "%$term%")->limit(10)->get();
    }

    public function availableMarketersByBeat(Request $request)
    {
        $excludeList = BeatMarketer::select('marketer_id')->where('beat_id', $request->beat_id)->get();
        $term = $request->term;
        $marketers = Marketer::
        where(function ($q) use ($term) {
            $q->where("name", "like", "%$term%")
                ->orWhere("alias", "like", "%$term%")
                ->orWhere("code", "like", "%$term%");
        })
            ->whereNotIn('id', $excludeList)
            ->limit(10)->get();

        return $marketers;
    }

    public function vendors(Request $request)
    {
        $term = $request->term;
        $vendors = Vendor::with('addresses')->where(function ($q) use ($term) {
            $q->where("name", "like", "%$term%");
        })->limit(10)->get();

        return $vendors;
    }

    public function taxClasses(Request $request)
    {
        return TaxClass::active()->where('name', 'like', "%$request->term%")->get();
    }

    public function cl4Id(Request $request)
    {
        $term = $request->term;
        return NewCatL4::where("name", "like", "%$term%")
            ->orWhere("id", "like", "%$term%")->limit(10)->get();
    }

    public function beatLocations(Request $request)
    {
        return WarehouseRack::where('type_id', 8)
            ->where('reference', 'like', "%$request->term%")
            ->limit(10)->get();
    }

    public function transactions(Request $request)
    {
        $term = $request->term;
        return Transaction::where("transaction_id", "like", "%$term%")
            ->where('type', 'credit')->limit(20)->get();
    }

    public function purchaseInvoices(Request $request)
    {
        $term = $request->term;
        return PurchaseInvoice::with(['vendor', 'vendorAddress', 'platform'])
            ->orWhere("id", "like", "%$term%")
            ->orWhere('vendor_ref_id', 'like', "%$term%")
            ->limit(20)->get();
    }

    public function vendorIds(Request $request)
    {
        $term = $request->term;
        return Vendor::orWhere("id", "like", "%$term%")
            ->orWhere("name", "like", "%$term%")
            ->limit(20)->get();
    }

    public function vendorAddresses(Request $request)
    {
        $term = $request->term;
        $vendorId = $request->get('vendor_id', null);
        if (empty($vendorId))
            return VendorsAddresses::orWhere("id", "like", "%$term%")
                ->orWhere("address", "like", "%$term%")
                ->limit(20)
                ->get();

        return Vendor::findOrFail($vendorId)
            ->hasMany(VendorsAddresses::class, 'vendor_id', 'id')
            ->get()
            ->reject(function ($vendorAddress, $key) use ($term) {
                if (empty($term)) {
                    return false;
                }
                return (strpos(strval($vendorAddress->id), strval($term)) === false) &&
                    (strpos(strval($vendorAddress->address), strval($term)) === false);
            })
            ->take(20);
    }

    public function purchaseInvoiceItems(Request $request)
    {
        $term = $request->term;
        $purchaseInvoiceId = $request->get('purchase_invoice_id', null);

        if (empty($purchaseInvoiceId))
            return PurchaseInvoiceItem::where("id", "like", "%$term%")
                ->limit(20)
                ->get();

        return PurchaseInvoice::findOrFail($purchaseInvoiceId)
            ->hasMany(PurchaseInvoiceItem::class, 'purchase_invoice_id', 'id')
            ->with(['product:id,name'])
            ->get()
            ->reject(function ($purchaseInvoiceItem, $key) use ($term) {
                if (empty($term)) {
                    return false;
                }
                return strpos(strval($purchaseInvoiceItem->id), strval($term)) === false;
            })
            ->take(20);
    }

    public function products_2(Request $request)
    {
        $term = $request->term;
        return Product::orWhere("id", "like", "%$term%")
            ->orWhere("name", "like", "%$term%")
            ->limit(20)
            ->get();
    }

    public function platforms(Request $request)
    {
        $term = $request->term;
        return PurchasePlatform::orWhere("id", "like", "%$term%")
            ->orWhere("platform", "like", "%$term%")
            ->limit(20)
            ->get();
    }

    public function storages(Request $request)
    {
        return Storage::where('label', 'like', "%$request->term%")
            ->limit(10)->get();
    }

    public function warehouses(Request $request)
    {
        return Warehouse::where('name', 'like', "%$request->term%")
            ->limit(10)->get();
    }

    public function employees(Request $request)
    {

        $query = Employee::whereHas('warehouses', function ($warehouseQuery) {
            $warehouseQuery->whereIn('warehouses.id', app(MultiWarehouseHelper::class)->getCurrentWarehouses());
        });
        $query->where(function ($q) use ($request) {
            $q->orWhere('name', 'like', "%$request->term%");
            $q->orWhere('mobile', 'like', "%$request->term%");
        });

        return $query->limit(10)->get();
    }

    public function reasons(Request $request)
    {
        $query = Reason::where('source_type', $request->source);
        if (!empty($request->type))
            $query->where('reason_type', $request->type);
        $query->where('reason_text', 'like', "%$request->term%");

        return $query->limit(10)->get();

    }

    public function pickers(Request $request)
    {
        return Employee::whereHas('roles', function ($roleQuery) {
            $roleQuery->where('roles.name', '=', 'Picker');
        })->whereHas('warehouses', function ($warehouseQuery) {
            $warehouseQuery->whereIn('warehouses.id', app(MultiWarehouseHelper::class)->getCurrentWarehouses());
        })->withCount(['picklists' => function ($picklistQuery) {
            $picklistQuery->where('status', Picklist::STATUS_GENERATED)->orWhere('status', Picklist::STATUS_PICKING);
        }])->where('name', 'like', "%$request->term%")->orderBy('picklists_count')
            ->limit(10)->get();
    }

    public function paymentTags(Request $request)
    {
        return Tag::paymentTag()
            ->where('name', 'like', "%$request->term%")
            ->limit(20)
            ->get();
    }
}
