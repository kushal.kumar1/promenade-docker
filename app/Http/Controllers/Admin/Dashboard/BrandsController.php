<?php


namespace Niyotail\Http\Controllers\Admin\Dashboard;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\BrandService;

class BrandsController extends Controller
{
    protected $brandService;

    public function __construct()
    {
        $this->brandService = new BrandService();
    }

    public function index(Request $request)
    {
        $response = array();

        /*
            To pass on filters and data to the view
        */
        $filtersData = SearchHelper::getDashboardFiltersFromRequest($request, true);
        $filters = $filtersData['filters'];
        $response['filters'] = $filters;
        $selectedFilters = $filtersData['data'];
        $response = array_merge($response, $selectedFilters);
        $response['chart_types'] = SearchHelper::getChartTypes();
        $response['view_type'] = $request->input('view-type', 'map-view');
        $response['selected_chart_type'] = $request->input('types', ['order_counts']);

        /*
            AJAX response for Grid is required here for
            CSV export purpose -
            Since it attempts to get data from the page URL
         */
        if($request->ajax())
        {
            return $this->brandService->getBrandsDashboardGrid($request);
        }

        return view('admin.dashboard.brands.analytics', $response);
    }


    public function grid(Request $request)
    {
        $types = $request->types;
        if(empty($types) && Cookie::get('table-types'))
        {
            $types = unserialize(Cookie::get('table-types'));
        }

        return response()
            ->json($this->brandService->getBrandsDashboardGrid($request))
            ->withCookie(Cookie::forever("table-types", serialize($types)));
    }

    public function heatmap(Request $request)
    {
        return $this->brandService->getFormattedBrandStatsForMap($request);
    }

    public function chart(Request $request)
    {
        $response = array();
        $filtersData = SearchHelper::getDashboardFiltersFromRequest($request);
        $response['filters'] = $filtersData['filters'];
    }
}