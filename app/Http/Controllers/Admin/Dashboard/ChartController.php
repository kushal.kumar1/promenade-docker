<?php

namespace Niyotail\Http\Controllers\Admin\Dashboard;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Requests\Admin\Dashboard\ChartRequest;
use Niyotail\Models\Brand;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Invoice;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Shipment;

/*
    USAGE:
    URL: /dashboard/chart?end_date=2020-01-30&start_date=2020-01-01&types[]=order_counts&types[]=gross_revenues&types[]=net_revenues
    Check Types in App/Http/Requests/Admin/Dashboard/ChartRequest.php
*/

class ChartController
{
    private $types;
    private $startDate;
    private $endDate;
    private $isFiltered;
    private $supported_types;

    // FILTERS
    private $store_type;
    private $filters;

    public function __construct(Request $request)
    {
        $this->types = request()->input('types', []);

        // DEFAULTS
        $this->store_type = 'all';
        $this->isFiltered = false;

        $this->supported_types = ['order_counts','order_totals','gross_revenues','return_totals','rto_totals','brand_counts','net_revenues','top_brands','top_marketers','top_stores','top_products', 'order_item_units'];

        $filters = SearchHelper::getDashboardFiltersFromRequest($request, false);
        $this->filters = $filters['filters'];

        $this->startDate = Carbon::parse($this->filters['start_date'])->toDateTimeString();
        $this->endDate = Carbon::parse($this->filters['end_date']." 23:59:59")->toDateTimeString();

        if(
            !empty($filters['store_ids']) ||
            !empty($filters['marketer_ids']) ||
            !empty($filters['collection_ids']) ||
            !empty($filters['product_ids']) ||
            !empty($filters['brand_ids']) ||
            !empty($filters['beat_ids']) ||
            !empty($filters['store_type'])
        )
        {
            $this->isFiltered = true;
        }
    }

    // TODO: Order Margin (Rs), Beats (#), Products (#), Categories
    public function getChart(ChartRequest $request) {
        $data['labels'] = $this->getDateRangeArray();
        $data['datasets'] = [];

        // Get chart types
        if(empty($request->types))
            return response()->json($this->supported_types);

        // Validate chart types
        foreach($this->types as $type) {
            abort_if(!in_array($type, $this->supported_types), 400, 'invalid type');
        }

        if(in_array('order_counts', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Orders (#)';
            $data['datasets'][$dataset_index]['yAxisID'] = 'y-axis-2';
            $orders = $this->orderQuery();
            foreach ($data['labels'] as $label) {
                $order = optional($orders->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $order->count ?? 0;
            }
        }


        if(in_array('order_totals', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Orders (Rs)';
            $orders = $orders ?? $this->orderQuery();
            foreach ($data['labels'] as $label) {
                $order = optional($orders->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $order->total ?? 0;
            }
        }


        if(in_array('gross_revenues', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Gross Revenue (Rs)';
            $revenues = $revenues ?? $this->revenueQuery();
            foreach ($data['labels'] as $label) {
                $revenue = optional($revenues->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $revenue->revenue ?? 0;
            }
        }


        if(in_array('return_totals', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Returns (Rs)';
            $returns = $returns ?? $this->returnQuery();
            foreach ($data['labels'] as $label) {
                $return = optional($returns->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $return->return_total ?? 0;
            }
        }


        if(in_array('rto_totals', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'RTO (Rs)';
            $rtos = $rtos ?? $this->rtoQuery();
            foreach ($data['labels'] as $label) {
                $rto = optional($rtos->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $rto->rto_total ?? 0;
            }
        }


        if(in_array('brand_counts', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Brands (#)';
            $data['datasets'][$dataset_index]['yAxisID'] = 'y-axis-2';
            $brands = $brands ?? $this->brandQuery();
            foreach ($data['labels'] as $label) {
                $brand = optional($brands->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $brand->count ?? 0;
            }
        }


        if(in_array('top_brands', $this->types)) {
            $brandsData = $this->TopBrandsQuery();
            $uniqueBrands = $brandsData['topBrands']->unique('brand_name')->pluck('brand_name');
            $dataset_index = count($data['datasets']);

            $data['datasets'][$dataset_index]['label'] = 'Other Brands';

            foreach ($data['labels'] as $label) {
                $data['datasets'][$dataset_index]['data'][] = optional($brandsData['otherBrands']->where('date', $label)->first())->order_total ?? 0;

                foreach ($uniqueBrands as $brand_index => $brand_name) {
                    $data['datasets'][$dataset_index+$brand_index+1]['label'] = $brand_name;
                    $data['datasets'][$dataset_index+$brand_index+1]['data'][] = optional($brandsData['topBrands']->where('date', $label)->where('brand_name', $brand_name)->first())->order_total ?? 0;
                }
            }
        }


        if(in_array('order_item_units', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Order Items Count (#)';
            $itemsCount = $this->OrderItemUnitsQuery();
            foreach ($data['labels'] as $label) {
                $rto = optional($itemsCount->where('date', $label)->first());
                $data['datasets'][$dataset_index]['data'][] = $rto->units_count ?? 0;
            }
        }


        if(in_array('top_marketers', $this->types)) {
            $marketersData = $this->TopMarketersQuery();
            $uniqueMarketers = $marketersData['topMarketers']->unique('marketer_name')->pluck('marketer_name');
            $dataset_index = count($data['datasets']);

            $data['datasets'][$dataset_index]['label'] = 'Other Marketers';

            foreach ($data['labels'] as $label) {
                $data['datasets'][$dataset_index]['data'][] = optional($marketersData['otherMarketers']->where('date', $label)->first())->order_total ?? 0;

                foreach ($uniqueMarketers as $marketer_index => $marketer_name) {
                    $data['datasets'][$dataset_index+$marketer_index+1]['label'] = $marketer_name;
                    $data['datasets'][$dataset_index+$marketer_index+1]['data'][] = optional($marketersData['topMarketers']->where('date', $label)->where('marketer_name', $marketer_name)->first())->order_total ?? 0;
                }
            }
        }


        if(in_array('top_stores', $this->types)) {
            $storesData = $this->TopStoresQuery();
            $uniqueStores = $storesData['topStores']->unique('store_name')->pluck('store_name');
            $dataset_index = count($data['datasets']);

            $data['datasets'][$dataset_index]['label'] = 'Other Stores';

            foreach ($data['labels'] as $label) {
                $data['datasets'][$dataset_index]['data'][] = optional($storesData['otherStores']->where('date', $label)->first())->order_total ?? 0;

                foreach ($uniqueStores as $store_index => $store_name) {
                    $data['datasets'][$dataset_index+$store_index+1]['label'] = $store_name;
                    $data['datasets'][$dataset_index+$store_index+1]['data'][] = optional($storesData['topStores']->where('date', $label)->where('store_name', $store_name)->first())->order_total ?? 0;
                }
            }
        }


        if(in_array('net_revenues', $this->types)) {
            $dataset_index = count($data['datasets']);
            $data['datasets'][$dataset_index]['label'] = 'Net Revenue (Rs)';
            $revenues = $revenues ?? $this->revenueQuery();
            $returns = $returns ?? $this->returnQuery();
            $rtoForNetIncome = $this->rtoForNetRevenueQuery();
            foreach ($data['labels'] as $label) {
                $gross_revenue = optional($revenues->where('date', $label)->first())->revenue ?? 0;
                $return_total = optional($returns->where('date', $label)->first())->return_total ?? 0;
                $rto_total = optional($rtoForNetIncome->where('date', $label)->first())->rto_total ?? 0;
                $data['datasets'][$dataset_index]['data'][] = $gross_revenue - ($return_total + $rto_total);
            }
        }

        // Common Dataset Options
        foreach ($data['datasets'] as $index => $dataset) {
            $data['datasets'][$index]['fill'] = false;
            $data['datasets'][$index]['lineTension'] = 0;
            $data['datasets'][$index]['borderColor'] = $this->getColors($index, $dataset['label']);
            if(!isset($data['datasets'][$index]['yAxisID']))
                $data['datasets'][$index]['yAxisID'] = 'y-axis-1';
        }

        return response()->json($data);
    }

    private function getDateRangeArray() {
        $dates = CarbonPeriod::between($this->startDate, Carbon::parse($this->endDate)->subDay()->toDateString())->toArray();
        return array_map(function($date) {
            return $date->format('Y-m-d');
        }, $dates);
    }

    private function getColors($index, $label): string {
        if(strpos($label, 'Other') !== false)
            return '#7f8c8d';

        switch ($index) {
            case 0:
                return '#c0392b';
            case 1:
                return '#8e44ad';
            case 2:
                return '#d35400';
            case 3:
                return '#2980b9';
            case 4:
                return '#16a085';
            case 5:
                return '#1B1464';
            case 6:
                return '#B53471';
            default:
                return '#000000';
        }
    }

    private function orderQuery()
    {
        return Order::selectRaw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, count(distinct orders.id) as count, ceiling(sum(order_items.total)) as total")
            ->where('orders.status', '!=', Order::STATUS_CANCELLED)
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function revenueQuery() {
        return OrderItem::selectRaw("DATE_FORMAT(date(convert_tz(invoices.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(order_items.total)) as revenue")
            ->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
            ->join('shipments', function($query) {
                $query->on('shipments.id', '=', 'shipment_items.shipment_id')->where('shipments.status', '!=', 'cancelled');
            })
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('invoices', function($query) {
                $query->on('invoices.shipment_id', '=', 'shipments.id')
                    ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                    ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'");
            })
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function returnQuery() {
        return OrderItem::selectRaw("DATE_FORMAT(date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(order_items.total)) as return_total")
                      ->join('return_order_items', 'return_order_items.order_item_id', '=', 'order_items.id')
                      ->join('return_orders', function($query) {
                          $query->on('return_orders.id', '=', 'return_order_items.return_order_id')->where('return_orders.status', '!=', 'cancelled');
                      })
                      ->join('orders', 'orders.id', '=', 'return_orders.order_id')
                      ->join('credit_notes', function($query) {
                          $query->on('credit_notes.return_order_id', '=', 'return_orders.id')
                              ->whereRaw("convert_tz(credit_notes.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'");
                      })
                      ->when($this->isFiltered, function ($query) {
                          $query = $query
                              ->join('products', 'products.id', '=', 'order_items.product_id')
                              ->join('brands', 'brands.id', '=', 'products.brand_id')
                              ->join('stores', 'stores.id', '=', 'orders.store_id');
                          return $this->applyFilterQueries($query);
                      })
                      ->groupBy('date')
                      ->orderBy('date')
                      ->get();
    }

    private function rtoQuery() {
        return Shipment::selectRaw('date(convert_tz(invoices.cancelled_on, "+00:00", "+05:30")) as date, sum(invoices.amount) as rto_total')
            ->join('invoices', 'shipments.id', '=', 'invoices.shipment_id')
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->where('shipments.status', 'rto')
            ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function brandQuery() {
        return Brand::active()->visible()
            ->selectRaw('date(convert_tz(brands.created_at, "+00:00", "+05:30")) as date, count(id) as count')
            ->whereRaw("convert_tz(created_at, '+00:00', '+05:30') between '$this->startDate' and  '$this->endDate'")
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function TopBrandsQuery() {
        $topBrands = OrderItem::selectRaw('brands.id as brand_id, ceiling(sum(order_items.total)) as order_total')
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and  '$this->endDate'")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->orderBy('order_total', 'desc')
            ->groupBy('brand_id')
            ->limit(6)
            ->get()->pluck('brand_id');

        $topBrandsData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total, brands.name as brand_name")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', function($query) use ($topBrands) {
                $query->on('brands.id', '=', 'products.brand_id')
                    ->whereIn('brands.id', $topBrands);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date', 'brand_name')
            ->get();

        $otherBrandsData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', function($query) use ($topBrands) {
                $query->on('brands.id', '=', 'products.brand_id')
                    ->whereNotIn('brands.id', $topBrands);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date')
            ->get();

        return [
            'topBrands' => $topBrandsData,
            'otherBrands' => $otherBrandsData,
        ];
    }

    private function OrderItemUnitsQuery() {
        return OrderItem::selectRaw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, sum(product_variants.quantity*order_items.quantity) as units_count")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('product_variants', 'product_variants.sku', '=', 'order_items.sku')
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function TopMarketersQuery() {
        $topMarketers = OrderItem::selectRaw('marketers.id as marketer_id, ceiling(sum(order_items.total)) as order_total')
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and  '$this->endDate'")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('marketers', 'marketers.id', '=', 'brands.marketer_id')
            ->orderBy('order_total', 'desc')
            ->groupBy('marketer_id')
            ->limit(6)
            ->get()->pluck('marketer_id');

        $topMarketersData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total, marketers.name as marketer_name")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('marketers', function($query) use ($topMarketers) {
                $query->on('marketers.id', '=', 'brands.marketer_id')
                    ->whereIn('marketers.id', $topMarketers);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date', 'marketer_name')
            ->get();

        $otherMarketersData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('marketers', function($query) use ($topMarketers) {
                $query->on('marketers.id', '=', 'brands.marketer_id')
                    ->whereNotIn('marketers.id', $topMarketers);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date')
            ->get();

        return [
            'topMarketers' => $topMarketersData,
            'otherMarketers' => $otherMarketersData,
        ];
    }

    private function TopStoresQuery() {
        $topStores = OrderItem::selectRaw('stores.id as store_id, ceiling(sum(order_items.total)) as order_total')
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and  '$this->endDate'")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->orderBy('order_total', 'desc')
            ->groupBy('store_id')
            ->limit(6)
            ->get()->pluck('store_id');

        $topStoresData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total, stores.name as store_name")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', function($query) use ($topStores) {
                $query->on('stores.id', '=', 'orders.store_id')
                    ->whereIn('stores.id', $topStores);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date', 'store_name')
            ->get();

        $otherStoresData = OrderItem::selectRaw("date(convert_tz(order_items.created_at, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as order_total")
            ->where('order_items.status', '!=', OrderItem::STATUS_CANCELLED)
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', function($query) use ($topStores) {
                $query->on('stores.id', '=', 'orders.store_id')
                    ->whereNotIn('stores.id', $topStores);
            })
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->orderBy('date')
            ->orderBy('order_total',  'desc')
            ->groupBy('date')
            ->get();

        return [
            'topStores' => $topStoresData,
            'otherStores' => $otherStoresData,
        ];
    }

    private function rtoForNetRevenueQuery() {
      return OrderItem::selectRaw("DATE_FORMAT(date(convert_tz(invoices.cancelled_on, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(order_items.total)) as rto_total")
          ->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
          ->join('shipments', function($query) {
              $query->on('shipments.id', '=', 'shipment_items.shipment_id')
                    ->where('shipments.status', Shipment::STATUS_RTO);
          })
          ->join('orders', 'orders.id', '=', 'shipments.order_id')
          ->join('invoices', function($query) {
              $query->on('invoices.shipment_id', '=', 'shipments.id')
                  ->where('invoices.status', Invoice::STATUS_CANCELLED);
                  // ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'");
          })
          ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') <= '$this->startDate'")
          ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
          ->when($this->isFiltered, function ($query) {
              $query = $query
                  ->join('products', 'products.id', '=', 'order_items.product_id')
                  ->join('brands', 'brands.id', '=', 'products.brand_id')
                  ->join('stores', 'stores.id', '=', 'orders.store_id');
              return $this->applyFilterQueries($query);
          })
          ->groupBy('date')
          ->orderBy('date')
          ->get();
    }

    private function applyFilterQueries(Builder $query): Builder {

        return $query->when( $this->filters['store_type'] === '1k', function ($query) {
                    return $query->whereIn('orders.store_id', $this->getoneKStores());
                })
                ->when($this->filters['store_type'] === 'retail', function ($query) {
                    return $query->whereNotIn('orders.store_id', $this->getoneKStores());
                })
                ->when(!empty($this->filters['beat_ids']), function ($query) {
                    return $query->whereIn('stores.beat_id', $this->filters['beat_ids']);
                })
                ->when(!empty($this->filters['brand_ids']), function ($query) {
                    return $query->whereIn('products.brand_id', $this->filters['brand_ids']);
                })
                ->when(!empty($this->filters['store_ids']), function ($query) {
                    return $query->whereIn('orders.store_id', $this->filters['store_ids']);
                })
                ->when(!empty($this->filters['product_ids']), function ($query) {
                    return $query->whereIn('order_items.product_id', $this->filters['product_ids']);
                })
                ->when(!empty($this->filters['collection_ids']), function ($query) {
                    return $query->join('collection_product', function($q) {
                        $q->on('collection_product.product_id', '=', 'products.id')->limit(1);
                    })->whereIn('collection_product.collection_id', $this->filters['collection_ids']);
                })
                ->when(!empty($this->filters['marketer_ids']), function ($query) {
                    return $query->whereIn('brands.marketer_id', $this->filters['marketer_ids']);
                });
    }

    private function getoneKBeats(): array {
        return cache()->remember('onek.beat.ids', 30, function() {
            return DB::table('beat_warehouse')->select('beat_id')->distinct()->get()->pluck('beat_id')->toArray();
        });
    }

    private function getoneKStores(): array {
        return cache()->remember('onek.store.ids', 30, function() {
            return DB::table('stores')->select('id')->whereIn('beat_id', $this->getoneKBeats())->get()->pluck('id')->toArray();
        });
    }
}
