<?php


namespace Niyotail\Http\Controllers\Admin\Dashboard;


use Niyotail\Http\Controllers\Controller;

class WelcomeController extends Controller
{
    public function index() {
        return view('admin.dashboard.welcome');
    }
}