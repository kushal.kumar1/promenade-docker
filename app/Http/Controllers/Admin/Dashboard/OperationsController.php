<?php

namespace Niyotail\Http\Controllers\Admin\Dashboard;

use Niyotail\Models\Order;
use Niyotail\Models\Shipment;
use Niyotail\Models\ReturnOrder;
use Niyotail\Http\Requests\Admin\Dashboard\ViewOperationsRequest;

class OperationsController
{
    public function index(ViewOperationsRequest $request)
    {
        $shipments = $this->getShipments();
        $orders = $this->getOrders();
        $pendingReturns = ReturnOrder::where('status', ReturnOrder::STATUS_INITIATED)->count();
        return view('admin.dashboard.operations', compact('shipments', 'orders', 'pendingReturns'));
    }

    private function getShipments()
    {
        $shipments['generated'] = Shipment::where('status', Shipment::STATUS_GENERATED)->count();
        $shipments['dispatched'] = Shipment::where('status', Shipment::STATUS_DISPATCHED)->count();
        $shipments['retry'] = Shipment::where('status', Shipment::STATUS_RETRY)->count();

        return $shipments;
    }

    private function getOrders()
    {
        $orders['confirmed'] = Order::where('status', Order::STATUS_CONFIRMED)->count();
        $orders['manifested'] = Order::where('status', Order::STATUS_MANIFESTED)->count();
        $orders['transit'] = Order::where('status', Order::STATUS_TRANSIT)->count();

        return $orders;
    }
}
