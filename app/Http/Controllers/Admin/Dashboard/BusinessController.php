<?php

namespace Niyotail\Http\Controllers\Admin\Dashboard;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\Graphs\GraphSalesOrdersResource;
use Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\PerformanceResource;
use Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\StatsResource;
use Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\TopProductsResource;
use Niyotail\Http\Resources\Admin\V1\Dashboard\Business\Stats\TopRetailersResource;
use Niyotail\Models\Marketer;
use DateTime;
use DateTimeZone;
use Niyotail\Models\Brand;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Store;
use Niyotail\Models\Product;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Niyotail\Models\Invoice;
use Niyotail\Models\Inventory;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Shipment;
use Niyotail\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Beat;
use Niyotail\Services\BrandService;

class BusinessController extends Controller
{
    private $isFiltered;

    // FILTERS
    private $store_type;
    private $beat_ids;
    private $brand_ids;
    private $marketer_ids;
    private $store_ids;
    private $product_ids;
    private $collection_ids;

    // DATES
    private $startDate;
    private $endDate;
    private $startDateUI;
    private $endDateUI;

    // Tag Ids
    private $oneKTagId = 8;
    private $clubOneKTagId = 9;

    public function __construct(Request $request)
    {
        // DEFAULTS
        $this->store_type = 'all';
        $this->beat_ids = null;
        $this->brand_ids = null;
        $this->marketer_ids = null;
        $this->store_ids = null;
        $this->product_ids = null;
        $this->collection_ids = null;

        $this->startDate = now()->format('Y-m-d 00:00:00');
        $this->endDate = now()->addDay()->format('Y-m-d 00:00:00');
        $this->startDateUI = $this->startDate;
        $this->endDateUI = now()->format('Y-m-d 00:00:00');
        $this->isFiltered = false;

        // REQUEST VALUES
        if($request->filled('store_type'))
            $this->store_type = $request->store_type;
        if($request->filled('brand_ids'))
            $this->brand_ids = $request->brand_ids;
        if($request->filled('beat_ids'))
            $this->beat_ids = $request->beat_ids;
        if($request->filled('marketer_ids'))
            $this->marketer_ids = $request->marketer_ids;
        if($request->filled('store_ids'))
            $this->store_ids = $request->store_ids;
        if($request->filled('product_ids'))
            $this->product_ids = $request->product_ids;
        if($request->filled('collection_ids'))
            $this->collection_ids = $request->collection_ids;

        if($request->filled('store_type') || $request->filled('brand_ids') || $request->filled('beat_ids') || $request->filled('marketer_ids') || $request->filled('store_ids') || $request->filled('product_ids') || $request->filled('collection_ids'))
            $this->isFiltered = true;

        if(!empty($request->start_date) && !empty($request->end_date)) {
            $this->startDateUI = Carbon::parse($request->start_date)->toDateTimeString();
            $this->endDateUI = Carbon::parse($request->end_date)->toDateTimeString();
            $this->startDate = $this->startDateUI;
            $this->endDate = Carbon::parse($request->end_date)->addDay()->toDateTimeString();
        }
    }

    public function index(Request $request)
    {
        $response = [
            'startDate' => Carbon::parse($this->startDate),
            'endDate' => Carbon::parse($this->endDate),
            'startDateUI' => Carbon::parse($this->startDateUI),
            'endDateUI' => Carbon::parse($this->endDateUI),
            'dateRangeUI' => Carbon::parse($this->startDateUI)->format('d-m-Y').' - '.Carbon::parse($this->endDateUI)->format('d-m-Y'),
            'store_type' => $request->input("store_type", "all"),
            'chart_types' => SearchHelper::getChartTypes(),
            'selected_chart_type' => $request->input('types', ['order_counts'])
        ];

        $filtersData = SearchHelper::getDashboardFiltersFromRequest($request, true);
        $response['filters'] = $filtersData['filters'];
        $response['filters']['start_date'] = now()->format("Y-m-d");
        $response = array_merge($response, $filtersData['data']);

        return view('admin.dashboard.business', $response);
    }

    public function getMetricsForPerformance(Request $request) {
        return [
            'orderMetrics' => $this->getOrderStats(),
            'revenueMetrics' => $this->getRevenueStats(),
            'returnMetrics' => $this->getReturnsStats(),
            'collectionMetrics' => $this->getCollectionStats(),
            'storeMetrics' => $this->getStoreData()
        ];
    }

    public function getMetrics(Request $request) {

        $orderMetrics = $this->getOrderStats(true);
        $revenueMetrics = $this->getRevenueStats();
        $returnMetrics = $this->getReturnsStats();
        $collectionMetrics = $this->getCollectionStats();
        $storeMetrics = $this->getStoreData();

        // KPI
        $kpiData = $this->getKPIData();
        $kpiMetrics = [
            'kpiStats' => $kpiData,
            'storesCount' => $storeMetrics['storesCount'],
            'orderPendingInvoices' => $orderMetrics['orderPendingInvoices']->isEmpty() ? 0 : $orderMetrics['orderPendingInvoices'],
            'unfulfilledInvoicedOrders' => $orderMetrics['unfulfilledInvoicedOrders']->isEmpty() ? 0 : $orderMetrics['unfulfilledInvoicedOrders'],
            'averageSKUPerRetailer' => ($orderMetrics['orderStoreDashboardStats']->isEmpty()) ? 0 :  (float) $orderMetrics['orderStoreDashboardStats']->sum('sku_quantity')/$orderMetrics['orderStoreDashboardStats']->count(),
            'averageSkuPerOrder' => $orderMetrics['orderDashboardStats']->isEmpty() ? 0 : (float) $orderMetrics['orderDashboardStats']->sum('sku_quantity')/$orderMetrics['orderDashboardStats']->count(),
            'averageSkuPerOrderWithoutCancellation' => $orderMetrics['orderDashboardStatsWithoutCancelled']->isEmpty() ? 0 : (float) $orderMetrics['orderDashboardStatsWithoutCancelled']->sum('sku_quantity')/$orderMetrics['orderDashboardStatsWithoutCancelled']->count(),
        ];
        $todayOrderCostP = $kpiData['shipmentInventoryData']->isEmpty() ? 0 : $kpiData['shipmentInventoryData']->sum('cost_of_sold_goods');
        $todaySalesP = $kpiData['shipmentInventoryData']->isEmpty() ? 0 : $kpiData['shipmentInventoryData']->sum('sales_amount');

        $miscMetrics['grossMargin'] = $todaySalesP - $todayOrderCostP;
        $miscMetrics['grossMarginPercentage'] = $todaySalesP === 0 ? 0 : (1 - ((float)$todayOrderCostP/ $todaySalesP)) * 100;

        $miscMetrics['discountAmount'] = $kpiData['shipmentInventoryData']->isEmpty() ? 0 : $kpiData['shipmentInventoryData']->sum('sales_discount');
        $miscMetrics['discountAmountPercentage'] = $todaySalesP === 0 ? 0 : ((float)$miscMetrics['discountAmount']/$todaySalesP) * 100;

        $miscMetrics['orderPendingInvoices'] = $orderMetrics['orderPendingInvoices']->isEmpty() ? 0 : $orderMetrics['orderPendingInvoices']->sum('sales');
        $miscMetrics['averageRevenuePerDay'] = ($revenueMetrics['invoices7days']->sum('revenue') - $returnMetrics['returns7days']->sum('return_total')) / (7);

        $miscMetrics['days'] = (new DateTime())->setTimezone(new DateTimeZone('Asia/Kolkata'))->format('d');
        $miscMetrics['hours'] = (new DateTime())->setTimezone(new DateTimeZone('Asia/Kolkata'))->format('H');
        $miscMetrics['monthHoursTillNow'] = (($miscMetrics['days']-1) * 24) + $miscMetrics['hours'];
        $miscMetrics['numDaysOfCurrentMonth'] = (new DateTime('last day of this month'))->format('d');
        $miscMetrics['totalHoursInMonth'] = $miscMetrics['numDaysOfCurrentMonth'] * 24;
        $miscMetrics['monthHoursTillNow'] = ($miscMetrics['monthHoursTillNow'] == 0) ? $miscMetrics['totalHoursInMonth'] : $miscMetrics['monthHoursTillNow'];

        $miscMetrics['orderFrequencyPerMonth'] = $storeMetrics['storesThatOrdered'] === 0 ? 0 : ($orderMetrics['orderStats']->sum('count') / $storeMetrics['storesThatOrdered']) * ($miscMetrics['totalHoursInMonth'] / $miscMetrics['monthHoursTillNow']);
        $miscMetrics['averageRevenuePerOrder'] = $orderMetrics['orderStats']->sum('count') === 0 ? 0 : ($revenueMetrics['invoices']->sum('revenue') - $returnMetrics['returns']->sum('return_total')) / $orderMetrics['orderStats']->sum('count');
        $miscMetrics['averageOrderSize'] = $orderMetrics['orderStats']->sum('count') === 0 ? 0 : $orderMetrics['orderStats']->sum('sales') / $orderMetrics['orderStats']->sum('count');

        $miscMetrics['grossMarginAmount'] = $kpiData['shipmentInventoryData']->sum('sales_amount') - $kpiData['shipmentInventoryData']->sum('cost_of_sold_goods');
        $miscMetrics['grossMarginPercentage'] = $kpiData['shipmentInventoryData']->sum('sales_amount') === 0 ? 0 : (1 - ((float)$kpiData['shipmentInventoryData']->sum('cost_of_sold_goods') / $kpiData['shipmentInventoryData']->sum('sales_amount'))) * 100;

        return [
            'orderMetrics' => $orderMetrics,
            'revenueMetrics' => $revenueMetrics,
            'returnMetrics' => $returnMetrics,
            'collectionMetrics' => $collectionMetrics,
            'storeMetrics' => $storeMetrics,
            'kpiMetrics' => $kpiMetrics,
            'miscMetrics' => $miscMetrics
        ];
    }

    public function statsAPI(Request $request) {
        ini_set("memory_limit", -1);
        if($request->graph_type === 'sales_orders') {
            return new GraphSalesOrdersResource($this->getGraphData($request));
        }

        if($request->top_list_type === 'most_selling_products') {
            return TopProductsResource::collection($this->getMostSellingProducts());
        }

        elseif($request->top_list_type === 'most_selling_retailers') {
            return TopRetailersResource::collection($this->getMostSellingRetailers());
        }

        elseif($request->stats_type === 'performance') {
            return new PerformanceResource($this->getMetricsForPerformance($request));
        }

        return new StatsResource($this->getMetrics($request));
    }

    private function getGraphData(Request $request) {
        if($request->graph_type === 'sales_orders') {
            $orders = Order::selectRaw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, count(distinct orders.id) as count, ceiling(sum(orders.total)) as sales")
                ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->groupBy('date')
                ->orderBy('date')
                ->get();

            $revenues = Invoice::selectRaw("DATE_FORMAT(date(convert_tz(invoices.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(invoices.amount)) as revenue")
                ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                ->whereIn(DB::raw('date(created_at)'), $orders->pluck('date')->toArray())
                ->groupBy('date')
                ->orderBy('date')
                ->get();

            $returns = CreditNote::selectRaw("DATE_FORMAT(date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(credit_notes.amount)) as rto_total")
                ->whereIn(DB::raw('date(credit_notes.created_at)'), $orders->pluck('date')->toArray())
                ->groupBy('date')
                ->orderBy('date')
                ->get();

            $rtoForNetRevenue = Shipment::selectRaw("date(convert_tz(invoices.cancelled_on, '+00:00', '+05:30')) as date, sum(invoices.amount) as rto_amount")
                ->join('invoices', function ($query) {
                    return $query->on('shipments.id', '=', 'invoices.shipment_id')
                        ->where('invoices.status', Invoice::STATUS_CANCELLED);
                })
                ->where('shipments.status', Shipment::STATUS_RTO)
                ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') <= '$this->startDate'")
                ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->groupBy('date')
                ->get();

            return compact('orders', 'revenues', 'returns', 'rtoForNetRevenue');
        }
        return abort(400, 'invalid graph_type');
    }

    private function getOrderStats($complete = false)
    {
        $orderStats = Order::selectRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date, count(distinct orders.id) as count, sum(order_items.total) as sales, count(distinct(orders.store_id)) as store_count")
                ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->when($this->isFiltered, function ($query) {
                    $query = $query
                        ->join('products', 'products.id', '=', 'order_items.product_id')
                        ->join('brands', 'brands.id', '=', 'products.brand_id')
                        ->join('stores', 'stores.id', '=', 'orders.store_id');
                    return $this->applyFilterQueries($query);
                })
                ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->groupBy('date')
                ->get();

        // $jitOrderStats = Order::selectRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date, count(distinct orders.id) as count, sum(order_items.total) as sales, count(distinct(store_id)) as store_count")
        //     ->where('orders.status', '!=', Order::STATUS_CANCELLED)
        //     ->join('order_items', 'order_items.order_id', '=', 'orders.id')
        //     ->when($this->isFiltered, function ($query) {
        //         $query = $query
        //             ->join('products', 'products.id', '=', 'order_items.product_id')
        //             ->join('brands', 'brands.id', '=', 'products.brand_id')
        //             ->join('stores', 'stores.id', '=', 'orders.store_id');
        //         return $this->applyFilterQueries($query);
        //     })
        //     ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
        //     ->groupBy('date')
        //     ->get();

            if($complete === true) {
                $orderDashboardStats = Order::selectRaw("count(distinct(order_items.sku)) as sku_quantity")
                    ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                    ->when($this->isFiltered, function ($query) {
                        $query = $query
                            ->join('products', 'products.id', '=', 'order_items.product_id')
                            ->join('brands', 'brands.id', '=', 'products.brand_id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id');
                        return $this->applyFilterQueries($query);
                    })
                    ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                    ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                    ->groupBy('order_items.order_id')
                    ->get();

                $twoDayAgo = Carbon::today()->subDays(2)->format('Y-m-d H:i:s');

                $orderDashboardStatsWithoutCancelled = Order::selectRaw("count(distinct(order_items.sku)) as sku_quantity")
                    ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                    ->when($this->isFiltered, function ($query) {
                        $query = $query
                            ->join('products', 'products.id', '=', 'order_items.product_id')
                            ->join('brands', 'brands.id', '=', 'products.brand_id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id');
                        return $this->applyFilterQueries($query);
                    })
                    ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                    ->whereNotIn('order_items.status', ['processing', 'manifested'])
                    ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between ' $this->startDate' and '$twoDayAgo'")
                    ->groupBy('order_items.order_id')
                    ->get();

                $orderStoreDashboardStats = Order::selectRaw("count(distinct(order_items.sku)) as sku_quantity")
                    ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                    ->when($this->isFiltered, function ($query) {
                        $query = $query
                            ->join('products', 'products.id', '=', 'order_items.product_id')
                            ->join('brands', 'brands.id', '=', 'products.brand_id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id');
                        return $this->applyFilterQueries($query);
                    })
                    ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                    ->groupBy('orders.store_id')
                    ->get();

                $orderInventoryStats = Order::selectRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date,
                      COALESCE(sum(order_item_inventories.quantity * order_item_inventories.unit_cost_price), sum(order_items.quantity * flat_inventories.unit_cost)) as cost_of_sold_goods")
                    ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                    ->join('order_item_inventories', 'order_item_inventories.order_item_id', '=', 'order_items.id')
                    ->leftJoin('order_item_flat_inventories', 'order_item_flat_inventories.order_item_id', '=', 'order_items.id')
                    ->leftJoin('flat_inventories', 'order_item_flat_inventories.flat_inventory_id', '=', 'flat_inventories.id')
                    ->when($this->isFiltered, function ($query) {
                        $query = $query
                            ->join('products', 'products.id', '=', 'order_items.product_id')
                            ->join('brands', 'brands.id', '=', 'products.brand_id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id');
                        return $this->applyFilterQueries($query);
                    })
                    ->where('orders.status', '!=', Order::STATUS_CANCELLED)
                    ->whereRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) between ' $this->startDate' and '$twoDayAgo'")
                    ->groupBy('date')->get();
            }

            $pendingOrdersForBillingStats = Order::selectRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date, sum(order_items.total) as sales")
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->when($this->isFiltered, function ($query) {
                    $query = $query
                        ->join('products', 'products.id', '=', 'order_items.product_id')
                        ->join('brands', 'brands.id', '=', 'products.brand_id')
                        ->join('stores', 'stores.id', '=', 'orders.store_id');
                    return $this->applyFilterQueries($query);
                })
                ->whereIn('orders.status', [Order::STATUS_CONFIRMED, Order::STATUS_MANIFESTED])
                ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->whereRaw('orders.id not in (select order_id from invoices where status != "cancelled")')
                ->groupBy('date')->get();

            $unfulfilledStats = Order::selectRaw("date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date, sum(order_items.total) as sales, order_items.status as order_item_status")
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->when($this->isFiltered, function ($query) {
                    $query = $query
                        ->join('products', 'products.id', '=', 'order_items.product_id')
                        ->join('brands', 'brands.id', '=', 'products.brand_id')
                        ->join('stores', 'stores.id', '=', 'orders.store_id');
                    return $this->applyFilterQueries($query);
                })
                ->whereHas('invoices', function($query) {
                    $query->where('status', '!=', 'cancelled');
                })
                ->whereRaw("orders.id in (select order_id from invoices where convert_tz(created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate' )")
                ->whereIn('order_items.status', ['processing', 'manifested', 'cancelled'])
                ->where('orders.status', '!=', 'cancelled')
                ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->groupBy('date')->groupBy('order_item_status')->get();

        return [
            'orderStats' => $orderStats,
            // 'jitOrderStats' => $jitOrderStats,
            'orderDashboardStats' => $orderDashboardStats ?? null,
            'orderDashboardStatsWithoutCancelled' => $orderDashboardStatsWithoutCancelled ?? null,
            'orderStoreDashboardStats' => $orderStoreDashboardStats ?? null,
            'orderInventoryStats' => $orderInventoryStats ?? null,
            'orderPendingInvoices' => $pendingOrdersForBillingStats,
            'unfulfilledInvoicedOrders' => $unfulfilledStats
        ];
    }

    private function getRevenueStats()
    {
        $invoices = OrderItem::selectRaw("date(convert_tz(invoices.created_at, '+00:00', '+05:30')) as date, sum(order_items.total) as revenue")
            ->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
            ->join('shipments', function($query) {
                $query->on('shipments.id', '=', 'shipment_items.shipment_id')->where('shipments.status', '!=', 'cancelled');
            })
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('invoices', function($query) {
                $query->on('invoices.shipment_id', '=', 'shipments.id')
                    // ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                    ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                    ->where(function($q) {
                      $q->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                        ->orWhere(function($innerQ) {
                          $innerQ->where('invoices.status', Invoice::STATUS_CANCELLED)
                                ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') > '$this->endDate'");
                        });
                    });
            })
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->groupBy('date')
            ->get();

        //$yesterday = \Carbon\Carbon::today()->subDay();
        $yesterday = \Carbon\Carbon::today();
        $weekBeforeYesterday = \Carbon\Carbon::today()->subDays(7);

        $invoices7days = OrderItem::selectRaw("date(convert_tz(invoices.created_at, '+00:00', '+05:30')) as date, invoices.id, sum(order_items.total) as revenue")
            ->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
            ->join('shipments', function($query) {
                $query->on('shipments.id', '=', 'shipment_items.shipment_id')->where('shipments.status', '!=', 'cancelled');
            })
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('invoices', function($query) use ($yesterday, $weekBeforeYesterday) {
                $query->on('invoices.shipment_id', '=', 'shipments.id')
                    ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                    ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$weekBeforeYesterday' and '$yesterday'");
            })
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->get();

        return [
            'invoices' => $invoices,
            'invoices7days' => $invoices7days
        ];
    }

    private function getReturnsStats()
    {
      $returnsData = OrderItem::selectRaw("DATE_FORMAT(date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date, ceiling(sum(order_items.total)) as return_total")
                      ->join('return_order_items', 'return_order_items.order_item_id', '=', 'order_items.id')
                      ->join('return_orders', function($query) {
                          $query->on('return_orders.id', '=', 'return_order_items.return_order_id')->where('return_orders.status', '!=', 'cancelled');
                      })
                      ->join('orders', 'orders.id', '=', 'return_orders.order_id')
                      ->join('credit_notes', function($query) {
                          $query->on('credit_notes.return_order_id', '=', 'return_orders.id')
                              ->whereRaw("convert_tz(credit_notes.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'");
                      })
                      ->when($this->isFiltered, function ($query) {
                          $query = $query
                              ->join('products', 'products.id', '=', 'order_items.product_id')
                              ->join('brands', 'brands.id', '=', 'products.brand_id')
                              ->join('stores', 'stores.id', '=', 'orders.store_id');
                          return $this->applyFilterQueries($query);
                      })
                      ->groupBy('date')
                      ->orderBy('date')
                      ->get();

        $yesterday = \Carbon\Carbon::today();
        $weekBeforeYesterday = \Carbon\Carbon::today()->subDays(7);

        $returns7days = CreditNote::selectRaw("date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')) as date, credit_notes.id, credit_notes.amount")
            ->join('orders', 'orders.id', '=', 'credit_notes.order_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
        ->whereRaw("convert_tz(credit_notes.created_at, '+00:00', '+05:30') between '$weekBeforeYesterday' and '$yesterday'")
        ->get();

        $returns7DaysData = collect();
        foreach ($returns7days->unique('date') as $return) {
            $returns7DaysData[] = (object) [
                'date' => $return->date,
                'return_total' => $returns7DaysData->where('date', $return->date)->unique('id')->sum('amount')
            ];
        }

        $rtoStats = Shipment::selectRaw('date(convert_tz(invoices.cancelled_on, "+00:00", "+05:30")) as date, sum(invoices.amount)')
                    ->join('invoices', 'shipments.id', '=', 'invoices.shipment_id')
                    ->join('orders', 'orders.id', '=', 'shipments.order_id')
                    ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                    ->when($this->isFiltered, function ($query) {
                        $query = $query
                            ->join('products', 'products.id', '=', 'order_items.product_id')
                            ->join('brands', 'brands.id', '=', 'products.brand_id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id');
                        return $this->applyFilterQueries($query);
                    })
                    ->where('shipments.status', 'rto')
                    // ->whereRaw("order_logs.description like '%to rto%'")
                    ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                    // ->whereRaw("convert_tz(order_logs.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                    ->get();

        $rtoStatsData = collect();
        foreach ($rtoStats->unique('date') as $rtoStat) {
            $rtoStatsData[] = (object) [
                'date' => $rtoStat->date,
                'rto_total' => $rtoStats->where('date', $rtoStat->date)->unique('id')->sum('amount')
            ];
        }

        $rtoForNetRevenue = OrderItem::selectRaw("date(convert_tz(invoices.cancelled_on, '+00:00', '+05:30')) as date, ceiling(sum(order_items.total)) as rto_total")
            ->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
            ->join('shipments', function($query) {
                $query->on('shipments.id', '=', 'shipment_items.shipment_id')
                    ->where('shipments.status', Shipment::STATUS_RTO);
            })
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('invoices', function($query) {
                $query->on('invoices.shipment_id', '=', 'shipments.id')
                    ->where('invoices.status', Invoice::STATUS_CANCELLED);
                    // ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') <= '$this->startDate'")
                    // ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'");
            })
            ->when($this->isFiltered, function ($query) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id')
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return $this->applyFilterQueries($query);
            })
            ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') <= '$this->startDate'")
            ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->groupBy('date')
            ->get();

        return [
            'returns' => $returnsData,
            'returns7days' => $returns7DaysData,
            'rtoStats' => $rtoStatsData,
            'rtoForNetRevenue' => $rtoForNetRevenue
        ];
    }

    private function getCollectionStats()
    {
        $transaction = Transaction::selectRaw("date(convert_tz(transaction_date, '+00:00', '+05:30')) as date, sum(amount) as collection")
                        ->join('stores', 'stores.id', '=', 'transactions.store_id')
                        ->whereRaw("convert_tz(transaction_date, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                        ->where('transactions.type', 'credit')
                        ->when($this->store_type === '1k', function ($query) {
                            return $query->whereIn('stores.id', $this->getOneKTagStores());
                        })
                        ->when($this->store_type === 'club_1k', function ($query) {
                            return $query->whereIn('stores.id', $this->getClubOneKTagStores());
                        })
                        ->when($this->store_type === 'retail', function ($query) {
                            return $query->whereIn('stores.id', $this->getRetailStores());
                        })
                        ->when(!empty($this->beat_ids), function ($query) {
                            return $query->whereIn('stores.beat_id', $this->beat_ids);
                        })
                        ->when(!empty($this->store_ids), function ($query) {
                            return $query->whereIn('stores.id', $this->store_ids);
                        })
                        ->where('transactions.status', Transaction::STATUS_COMPLETED)
                        ->whereNotIn('payment_method', ['cash_discount'])
                        /*->whereRaw("convert_tz(created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")*/
                        // ->where('created_by_type', '!=', null)
                        ->groupBy('date')->get();

            $cashDiscount = Transaction::selectRaw("date(convert_tz(transactions.created_at, '+00:00', '+05:30')) as date, sum(amount) as collection")
                ->join('stores', 'stores.id', '=', 'transactions.store_id')
                ->whereRaw("convert_tz(transactions.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->when($this->store_type === '1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getOneKTagStores());
                })
                ->when($this->store_type === 'club_1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getClubOneKTagStores());
                })
                ->when($this->store_type === 'retail', function ($query) {
                    return $query->whereIn('stores.id', $this->getRetailStores());
                })
                ->when(!empty($this->beat_ids), function ($query) {
                    return $query->whereIn('stores.beat_id', $this->beat_ids);
                })
                ->when(!empty($this->store_ids), function ($query) {
                    return $query->whereIn('stores.id', $this->store_ids);
                })
                ->where('payment_method', 'cash_discount')
                ->whereRaw("convert_tz(transactions.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->where('transactions.status', Transaction::STATUS_COMPLETED)
                ->groupBy('date')->get();

            $chequeBounce = Transaction::selectRaw("date(convert_tz(transactions.created_at, '+00:00', '+05:30')) as date, sum(amount) as collection")
                ->join('stores', 'stores.id', '=', 'transactions.store_id')
                ->whereRaw("convert_tz(transactions.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->when($this->store_type === '1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getOneKTagStores());
                })
                ->when($this->store_type === 'club_1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getClubOneKTagStores());
                })
                ->when($this->store_type === 'retail', function ($query) {
                    return $query->whereIn('stores.id', $this->getRetailStores());
                })
                ->when(!empty($this->beat_ids), function ($query) {
                    return $query->whereIn('stores.beat_id', $this->beat_ids);
                })
                ->when(!empty($this->store_ids), function ($query) {
                    return $query->whereIn('stores.id', $this->store_ids);
                })
                ->where('payment_method', 'cheque_bounce')
                ->whereRaw("convert_tz(transactions.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
                ->where('transactions.status', Transaction::STATUS_COMPLETED)
                ->groupBy('date')->get();

            $pendingTransactions = Transaction::selectRaw("date(convert_tz(transactions.created_at, '+00:00', '+05:30')) as date, sum(amount) as collection")
                ->join('stores', 'stores.id', '=', 'transactions.store_id')
                ->when($this->store_type === '1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getOneKTagStores());
                })
                ->when($this->store_type === 'club_1k', function ($query) {
                    return $query->whereIn('stores.id', $this->getClubOneKTagStores());
                })
                ->when($this->store_type === 'retail', function ($query) {
                    return $query->whereIn('stores.id', $this->getRetailStores());
                })
                ->when(!empty($this->beat_ids), function ($query) {
                    return $query->whereIn('stores.beat_id', $this->beat_ids);
                })
                ->when(!empty($this->store_ids), function ($query) {
                    return $query->whereIn('stores.id', $this->store_ids);
                })
                ->whereRaw("convert_tz(transactions.created_at, '+00:00', '+05:30') < '$this->endDate'")
                ->where('transactions.status', Transaction::STATUS_PENDING)
                ->groupBy('date')->get();

        return [
            'transaction' => $transaction,
            'cashDiscount' => $cashDiscount ?? null,
            'chequeBounce' => $chequeBounce ?? null,
            'pending' => $pendingTransactions ?? null
        ];
    }

    //TODO: discuss logic, commenting the list for now
    private function getMostSellingProducts()
    {
        return Product::with('images')
            // // ->selectRaw('products.*, sum(order_items.quantity * product_variants.quantity) as quantity')
            ->selectRaw('products.*, sum(order_items.quantity) as quantity, order_items.sku, order_items.variant, sum(order_items.total) as order_item_price')
            ->join('order_items', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('shipments', function($join) {
                $join->on('shipments.order_id', '=', 'orders.id')
                    ->where('shipments.status', '!=', 'cancelled');
            })
            ->join('shipment_items', function($join) {
                $join->on('shipment_items.shipment_id', '=', 'shipments.id')
                    ->on('shipment_items.order_item_id', '=', 'order_items.id');
            })
            ->join('invoices', function($join) {
                $join->on('invoices.order_id', '=', 'orders.id')
                    ->on('invoices.shipment_id', '=', 'shipments.id')
                    ->where('invoices.status', '!=', 'cancelled');
            })
            // ->join('product_variants', 'product_variants.sku', '=', 'order_items.sku')
            ->when($this->isFiltered, function ($query) {
                return $this->applyFilterQueries($query);
            })
            // ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
            ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->where('orders.status', '!=', Order::STATUS_CANCELLED)
            ->whereNotIn('order_items.status', ['processing', 'manifested', 'cancelled', 'returned', 'rto'])
            ->groupBy('order_items.sku')
            ->orderBy('order_item_price', 'desc')
            ->take(25)->get();
    }

    private function getMostSellingRetailers()
    {
        $orders = Product::with('images')
            ->selectRaw('stores.*, beats.name as beat_name, sum(order_items.total) as order_item_price')
            ->join('order_items', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('beats', 'stores.beat_id', '=', 'beats.id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->when($this->isFiltered, function ($query) {
                return $this->applyFilterQueries($query);
            })
            ->join('shipments', function($join) {
                $join->on('shipments.order_id', '=', 'orders.id')
                    ->where('shipments.status', '!=', 'cancelled');
            })
            ->join('shipment_items', function($join) {
                $join->on('shipment_items.shipment_id', '=', 'shipments.id')
                    ->on('shipment_items.order_item_id', '=', 'order_items.id');
            })
            ->join('invoices', function($join) {
                $join->on('invoices.order_id', '=', 'orders.id')
                    ->on('invoices.shipment_id', '=', 'shipments.id')
                    ->where('invoices.status', '!=', 'cancelled');
            })
            ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
            ->where('orders.status', '!=', Order::STATUS_CANCELLED)
            ->whereNotIn('order_items.status', ['processing', 'manifested', 'cancelled'])
            ->groupBy('orders.store_id')
            ->orderBy('order_item_price', 'desc')
            ->take(25)->get();
        return $orders;
    }

    private function getStoreData()
    {
        $storesCount =  Store::active()->verified()
            ->when($this->store_type === '1k', function ($query) {
                return $query->whereIn('id', $this->getOneKTagStores());
            })
            ->when($this->store_type === 'club_1k', function ($query) {
                return $query->whereIn('id', $this->getClubOneKTagStores());
            })
            ->when($this->store_type === 'retail', function ($query) {
                return $query->whereIn('id', $this->getRetailStores());
            })
            ->when(!empty($this->beat_ids), function ($query) {
                return $query->whereIn('beat_id', $this->beat_ids);
            })
            ->whereRaw("convert_tz(stores.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->count();

        $totalActiveVerifiedStoresCount = Store::active()->verified()
            ->when($this->store_type === '1k', function ($query) {
                return $query->whereIn('id', $this->getOneKTagStores());
            })
            ->when($this->store_type === 'club_1k', function ($query) {
                return $query->whereIn('id', $this->getClubOneKTagStores());
            })
            ->when($this->store_type === 'retail', function ($query) {
                return $query->whereIn('id', $this->getRetailStores());
            })
            ->when(!empty($this->beat_ids), function ($query) {
                return $query->whereIn('beat_id', $this->beat_ids);
            })
            ->count();

        $storesThatOrdered = Order::selectRaw("store_id, date(convert_tz(orders.created_at, '+00:00', '+05:30')) as date")
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->where('orders.status', '!=', Order::STATUS_CANCELLED)
            ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '$this->startDate' and '$this->endDate'")
            ->when($this->isFiltered, function ($query) {
                return $this->applyFilterQueries($query);
            })
            ->groupBy('store_id')->get();

        return collect([
            'storesThatOrdered' => $storesThatOrdered->count(),
            'storesThatOrderedValues' => $storesThatOrdered,
            'totalActiveVerifiedStores' => $totalActiveVerifiedStoresCount,
            'storesCount' => $storesCount
        ]);
    }

    private function getKPIData()
    {
        $totalInventory = FlatInventory::selectRaw('sum(unit_cost) as total_inventory_value')
            // Inventory::selectRaw('sum(quantity * unit_cost_price) as total_inventory_value')
            // ->join('products', 'products.id', '=', 'inventories.product_id')
            ->join('products', 'products.id', '=', 'flat_inventories.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->when($this->store_type === '1k', function ($query) {
                return $query->whereIn('warehouse_id', $this->getoneKWarehouses());
            })
            ->when($this->store_type === 'retail', function ($query) {
                return $query->whereNotIn('warehouse_id', $this->getoneKWarehouses());
            })
            ->when(!empty($this->brand_ids), function ($query) {
                return $query->whereIn('products.brand_id', $this->brand_ids);
            })
            ->when(!empty($this->product_ids), function ($query) {
                // return $query->whereIn('inventories.product_id', $this->product_ids);
                return $query->whereIn('flat_inventories.product_id', $this->product_ids);
            })
            ->when(!empty($this->marketer_ids), function ($query) {
                return $query->whereIn('brands.marketer_id', $this->marketer_ids);
            })->whereIn('flat_inventories.status', [FlatInventory::STATUS_READY_FOR_SALE, FlatInventory::STATUS_PUTAWAY, FlatInventory::STATUS_HOLD])
            ->first();

        // $jitInventory = Inventory::selectRaw('sum(quantity * unit_cost_price) as total_inventory_value')
        //     ->join('products', 'products.id', '=', 'inventories.product_id')
        //     ->join('brands', 'brands.id', '=', 'products.brand_id')
        //     ->when($this->store_type === '1k', function ($query) {
        //         return $query->whereIn('warehouse_id', $this->getoneKWarehouses());
        //     })
        //     ->when($this->store_type === 'retail', function ($query) {
        //         return $query->whereNotIn('warehouse_id', $this->getoneKWarehouses());
        //     })
        //     ->when(!empty($this->brand_ids), function ($query) {
        //         return $query->whereIn('products.brand_id', $this->brand_ids);
        //     })
        //     ->when(!empty($this->product_ids), function ($query) {
        //         return $query->whereIn('inventories.product_id', $this->product_ids);
        //     })
        //     ->when(!empty($this->marketer_ids), function ($query) {
        //         return $query->whereIn('brands.marketer_id', $this->marketer_ids);
        //     })
        //     ->whereHas('product', function($query) {
        //         return $query->where('allow_back_orders', 1);
        //     })->first();

        // $totalInventory = Product::selectRaw('sum(product_variants.price * inventories.quantity) as total_inventory_value')
        //     ->join('inventories', 'inventories.product_id', '=', 'products.id')
        //     ->join('product_variants', 'product_variants.product_id', 'products.id')
        //     ->where('product_variants.value', 'unit')
        //     ->active()
        //     ->first();

        $accountRecievables = Store::with('totalBalance')
            ->when($this->store_type === '1k', function ($query) {
                return $query->whereIn('id', $this->getOneKTagStores());
            })
            ->when($this->store_type === 'club_1k', function ($query) {
                return $query->whereIn('id', $this->getClubOneKTagStores());
            })
            ->when($this->store_type === 'retail', function ($query) {
                return $query->whereIn('id', $this->getRetailStores());
            })
            ->when(!empty($this->beat_ids), function ($query) {
                return $query->whereIn('stores.beat_id', $this->beat_ids);
            })
            ->get();

        $brandsCount = Brand::active()->visible()
              ->whereRaw("convert_tz(created_at, '+00:00', '+05:30') between '$this->startDate' and  '$this->endDate'")
                ->when(!empty($this->brand_ids), function ($query) {
                    return $query->whereIn('id', $this->brand_ids);
                })
                ->when(!empty($this->marketer_ids), function ($query) {
                    return $query->whereIn('marketer_id', $this->marketer_ids);
                })
                ->count();

        $allBrandsCount = Brand::active()->visible()->count();


        $storeFilters = [];

        if(!empty($this->store_ids)) {
            $storeFilters = $this->store_ids;
        } else {
            if($this->store_type === '1k')
            {
                $storeFilters = array_merge($storeFilters, $this->getOneKTagStores());
            }

            if($this->store_type === 'club_1k')
            {
                $storeFilters = array_merge($storeFilters, $this->getClubOneKTagStores());
            }

            if($this->store_type === 'retail')
            {
                $storeFilters = array_merge($storeFilters, $this->getRetailStores());
            }

            $storeFilters = array_unique($storeFilters);
        }

        $filterString = '';

        if(!empty($storeFilters)) {
            $filterString = ' and st.id in ("'.implode('"', $storeFilters).'") ';
        }
        if(!empty($this->brand_ids)) {
            $filterString = ' and p.brand_id in ("'.implode('"', $this->brand_ids).'") ';
        }
        if(!empty($this->product_ids)) {
            $filterString = ' and p.id in ("'.implode('"', $this->product_ids).'") ';
        }
        if(!empty($this->collection_ids)) {
            $filterString = ' and col.id in ("'.implode('"', $this->collection_ids).'") ';
        }
        if(!empty($this->marketer_ids)) {
            $filterString = ' and b.marketer_id in ("'.implode('"', $this->marketer_ids).'") ';
        }

        // TODO: Expensive query
        $shipmentData = DB::select("
            select
            date(CONVERT_TZ(i.created_at, '+00:00', '+05:30')) as date,
            COALESCE(round(sum(oii.unit_cost_price*pv.quantity*oi.quantity), 4), round(sum(fi.unit_cost*pv.quantity*oi.quantity), 4)) as cost_of_sold_goods,
            round(sum(oi.quantity*oi.subtotal), 4) as sales_subtotal,
            round(sum(oi.quantity*oi.tax), 4) as sales_tax,
            round(sum(oi.quantity*oi.discount), 4) as sales_discount,
            round(sum(oi.quantity*oi.total), 4) as sales_amount

            from shipments s

            left join invoices as i on s.id = i.shipment_id
            left join shipment_items si on s.id = si.shipment_id
            left join order_items oi on si.order_item_id = oi.id
            left join order_item_inventories oii on oi.id = oii.order_item_id
            left join order_item_flat_inventories oifi on oifi.order_item_id = oi.id
            left join flat_inventories fi on oifi.flat_inventory_id = fi.id
            left join products p on oi.product_id = p.id
            left join product_variants pv on oi.sku = pv.sku
            left join brands b on p.brand_id = b.id
            left join collection_product cp on p.id = cp.product_id
            left join collections col on cp.collection_id = col.id
            left join orders o on oi.order_id = o.id
            left join stores st on o.store_id = st.id
            where s.status not in ('cancelled', 'returned') and i.status not in ('cancelled') and oi.status not in ('returned', 'partial_return')
            and date(CONVERT_TZ(i.created_at, '+00:00', '+05:30')) BETWEEN '".$this->startDate."' and '".$this->endDate."'
            ".$filterString."
            GROUP by date(CONVERT_TZ(i.created_at, '+00:00', '+05:30'))
        ");

//        $shipmentInventoryData = Shipment::selectRaw("date(convert_tz(shipments.created_at, '+00:00', '+05:30')) as date, sum(oii.cost_price) as cost_of_sold_goods, sum(order_items.total) as sales_amount, sum(order_items.discount) as sales_discount")
//              ->join('orders', 'orders.id', '=', 'shipments.order_id')
//              ->join('invoices', 'invoices.shipment_id', '=', 'shipments.id')
//              ->join('shipment_items', 'shipment_items.shipment_id', '=', 'shipments.id')
//              ->join('order_items', 'shipment_items.order_item_id', '=', 'order_items.id')
//              ->join(\DB::raw('(select order_item_id, sum(order_item_inventories.quantity * order_item_inventories.unit_cost_price) as cost_price from order_item_inventories, order_items where order_item_inventories.order_item_id = order_items.id group by order_item_id) oii'), 'oii.order_item_id', '=', 'order_items.id')
//              ->where('shipments.status', '!=', Shipment::STATUS_CANCELLED)
//              ->whereRaw("date(convert_tz(shipments.created_at, '+00:00', '+05:30'))  between '$this->startDate' and  '$this->endDate'")
//              ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
//              ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
//                ->when($this->isFiltered, function ($query) {
//                    $query = $query
//                        ->join('products', 'products.id', '=', 'order_items.product_id')
//                        ->join('brands', 'brands.id', '=', 'products.brand_id')
//                        ->join('stores', 'stores.id', '=', 'orders.store_id');
//                    return $this->applyFilterQueries($query);
//                })
//              ->groupBy('date')
//              ->get();
        $shipmentInventoryData = collect(json_decode(json_encode($shipmentData), true));
        return [
              'total_inventory' => $totalInventory->total_inventory_value,
              // 'jit_inventory' => $jitInventory->total_inventory_value,
              'allBrandsCount' => $allBrandsCount,
              'brandsCount' => $brandsCount,
              'account_receivables' => $accountRecievables->sum('totalBalance.balance_amount'),
              'shipmentInventoryData' => $shipmentInventoryData
            ];
    }

    private function getoneKBeats(): array {
        return cache()->remember('onek.beat.ids', 30, function() {
            return DB::table('beat_warehouse')->select('beat_id')->distinct()->get()->pluck('beat_id')->toArray();
        });
    }

    private function getoneKStores(): array {
        return cache()->remember('onek.store.ids', 30, function() {
            return DB::table('stores')->select('id')->whereIn('beat_id', $this->getoneKBeats())->get()->pluck('id')->toArray();
        });
    }

    private function getOneKTagStores(): array{
        return cache()->remember('tagOnek.store.ids', 30, function() {
            return DB::table('store_tag')->select('store_id')
                ->where('tag_id', $this->oneKTagId)
                ->distinct()->get()->pluck('store_id')->toArray();
        });
    }

    private function getClubOneKTagStores(): array{
        return cache()->remember('tagClubOnek.store.ids', 30, function() {
            return DB::table('store_tag')->select('store_id')
                ->where('tag_id', $this->clubOneKTagId)
                ->distinct()->get()->pluck('store_id')->toArray();
        });
    }

    private function getRetailStores(): array{
        return cache()->remember('tagRetail.store.ids', 30, function() {
            return Store::whereDoesntHave('tags')->get()->pluck('id')->toArray();
        });
    }

    private function getoneKWarehouses(): array {
        return cache()->remember('onek.warehouse.ids', 30, function() {
            return DB::table('beat_warehouse')->select('warehouse_id')->whereIn('beat_id', $this->getoneKBeats())->where('warehouse_id', '!=', 1)->get()->pluck('warehouse_id')->toArray();
        });
    }

    private function applyFilterQueries(Builder $query): Builder {
            return $query->when($this->store_type === '1k', function ($query) {
                return $query->whereIn('stores.id', $this->getOneKTagStores());
            })
            ->when($this->store_type === 'club_1k', function ($query) {
                return $query->whereIn('stores.id', $this->getClubOneKTagStores());
            })
            ->when($this->store_type === 'retail', function ($query) {
                return $query->whereIn('stores.id', $this->getRetailStores());
            })
            ->when(!empty($this->beat_ids), function ($query) {
                return $query->whereIn('stores.beat_id', $this->beat_ids);
            })
            ->when(!empty($this->brand_ids), function ($query) {
                return $query->whereIn('products.brand_id', $this->brand_ids);
            })
            ->when(!empty($this->store_ids), function ($query) {
                return $query->whereIn('orders.store_id', $this->store_ids);
            })
            ->when(!empty($this->product_ids), function ($query) {
                return $query->whereIn('order_items.product_id', $this->product_ids);
            })
            ->when(!empty($this->collection_ids), function ($query) {
                return $query->join('collection_product', function($q) {
                    $q->on('collection_product.product_id', '=', 'products.id')->limit(1);
                })->whereIn('collection_product.collection_id', $this->collection_ids);
            })
            ->when(!empty($this->marketer_ids), function ($query) {
                return $query->whereIn('brands.marketer_id', $this->marketer_ids);
            });
    }
}
