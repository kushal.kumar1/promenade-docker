<?php

namespace Niyotail\Http\Controllers\Admin\Dashboard;

use Illuminate\Http\Request;
use Niyotail\Http\Requests\Admin\Dashboard\FiltersRequest;
use Niyotail\Models\Beat;
use Niyotail\Models\Brand;
use Niyotail\Models\Collection;
use Niyotail\Models\Marketer;
use Niyotail\Models\Product;
use Niyotail\Models\Store;

/*
    USAGE: /dashboard/filters?type=brands&marketer_ids[]=8
    Check app/Http/Requests/Admin/Dashboard/FiltersRequest.php
*/

class FiltersController
{
    private $filters;
    private $store_types;
    private $response_limit;

    public function __construct()
    {
        $this->store_types = ['all', '1k', 'club_1k', 'retail'];
        $this->response_limit = 20;

        if(request()->filled('store_type'))
            $this->filters['store_type'] = request()->store_type;

        if(request()->filled('beat_ids'))
            $this->filters['beat_ids'] = request()->beat_ids;

        if(request()->filled('brand_ids'))
            $this->filters['brand_ids'] = request()->brand_ids;

        if(request()->filled('marketer_ids'))
            $this->filters['marketer_ids'] = request()->marketer_ids;

        if(request()->filled('product_ids'))
            $this->filters['product_ids'] = request()->product_ids;

        if(request()->filled('store_ids'))
            $this->filters['store_ids'] = request()->store_ids;

        if(request()->filled('collection_ids'))
            $this->filters['collection_ids'] = request()->collection_ids;
    }

    public function getFilters(FiltersRequest $request) {
        if(method_exists($this, $request->type)) {
            return response()->json($this->{$request->type}($request));
        }
        return abort(400, 'type invalid');
    }

    private function store_types($request)
    {
        return $this->store_types;
    }

    private function beats($request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        return Beat::
            when(!empty($this->filters['store_ids']), function($query) {
                return $query->whereHas('stores', function($q) {
                    return $q->whereIn('id', $this->filters['store_ids']);
                });
            })
            ->when(!empty($this->filters['store_type']), function($query) {
                if($this->filters['store_type'] === '1k') {
                    return $query->whereHas('warehouses', function($q) {
                        return $q->where('warehouses.id', 2);
                    });
                }
                if($this->filters['store_type'] === 'retail') {
                    return $query->whereDoesntHave('warehouses');
                }
            })
            ->active()->where("name", "like", "%$term%")->limit($this->response_limit)->get();
    }

    private function brands($request)
    {
        $term = $request->term;
        $term = str_replace(' ', '%', $term);
        return Brand::
            when(!empty($this->filters['marketer_ids']), function($query) {
                return $query->whereIn('marketer_id', $this->filters['marketer_ids']);
            })
            ->where("name", "like", "%$term%")->limit($this->response_limit)->get();
    }

    private function marketers(Request $request)
    {
        $term = $request->term;

        return Marketer::
            where(function ($q) use ($term) {
                $q->where("name", "like", "%$term%")
                    ->orWhere("alias", "like", "%$term%")
                    ->orWhere("code", "like", "%$term%");
            })
            ->when(!empty($this->filters['brand_ids']), function($query) {
                return $query->whereHas('brands', function($q) {
                    $q->whereIn('brands.id', $this->filters['brand_ids']);
                });
            })
            ->limit($this->response_limit)->get();
    }

    private function products(Request $request)
    {
        $term = $request->term;

        return Product::
            when(!empty($this->filters['brand_ids']), function($query) {
                return $query->whereIn('brand_id', $this->filters['brand_ids']);
            })
            ->when(!empty($this->filters['marketer_ids']), function($query) {
                return $query->whereHas('brand', function($q) {
                    $q->whereIn('marketer_id', $this->filters['marketer_ids']);
                });
            })
            ->when(!empty($this->filters['collection_ids']), function($query) {
                return $query->whereHas('collections', function($q) {
                    $q->whereIn('collection_id', $this->filters['collection_ids']);
                });
            })
            ->where('name', 'like', "%$term%")->limit($this->response_limit)->get();
    }

    private function stores(Request $request)
    {
        $term = $request->term;

        return Store::active()->verified()
            ->where('name', 'like', "%$term%")
            ->when(!empty($this->filters['beat_ids']), function($query) {
                return $query->whereIn('beat_id', $this->filters['beat_ids']);
            })
            ->limit($this->response_limit)->get();
    }

    private function collections(Request $request)
    {
        $term = $request->term;

        return Collection::
            when(!empty($this->filters['product_ids']), function($query) {
                return $query->whereHas('products', function($q) {
                    $q->whereIn('product_id', $this->filters['product_ids']);
                });
            })
            ->where('name', 'like', "%$term%")->limit($this->response_limit)->get();
    }
}