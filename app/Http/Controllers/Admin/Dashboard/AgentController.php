<?php

namespace Niyotail\Http\Controllers\Admin\Dashboard;

use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Niyotail\Models\Agent;
use Niyotail\Helpers\Metric\{Sale, Point, TopProductList, UniqueStoreAndProduct};

class AgentController extends Controller
{
    private $startDate;
    private $endDate;
    private $startDateUI;
    private $endDateUI;
    private $agentId;
    private $agent;
    private $default;

    public function __construct(Request $request)
    {
        // DEFAULTS
        $this->default = true;
        $this->agentId = 0;
        $this->startDate = now()->format('Y-m-d 00:00:00');
        $this->endDate = now()->addDays(1)->format('Y-m-d 00:00:00');
        $this->startDateUI = now()->format('Y-m-d 00:00:00');
        $this->endDateUI = now()->format('Y-m-d 00:00:00');

        // REQUEST VALUES
        if($request->filled('agentId'))
            $this->agentId = $request->agentId;

        if($request->filled('daterange')) {
            list($startDate, $endDate) = explode(' - ', $request->daterange);
            $this->startDateUI = Carbon::parse($startDate)->toDateTimeString();
            $this->endDateUI = Carbon::parse($endDate)->toDateTimeString();
            $this->default = false;
        }

        $this->agent = Agent::where('id', $this->agentId)
            ->with(['targets' => function ($query) {
                $query->whereNull('store_id')->where('valid_from', '<=', $this->startDateUI)->where('valid_to', '>=', $this->endDateUI);
            }])->first();
    }

    public function index(Request $request)
    {
        return view('admin.dashboard.agents', [
            'agents' => Agent::whereType('sales')->active()->orderBy('name')->get(),
            'sales' => $this->getSales(),
            'unique_stores' => $this->getUniqueStoresAndProducts()['unique_stores'],
            'unique_products' => $this->getUniqueStoresAndProducts()['unique_products'],
            'points' => $this->getPoints(),
            'targets' => $this->getTargets(),
            'startDate' => $this->startDateUI,
            'endDate' => $this->endDateUI,
            'agent' => $this->agent,
            'agentId' => $this->agentId,
            'topSkus' => $this->getTopSkus(),
            'default' => $this->default
        ]);
    }

    private function getSales(): array {
        $sales = new Sale($this->startDateUI, $this->endDateUI);

        if(!empty($this->agent))
            $sales->setAgent($this->agent);

        return [
            'ordered' => $sales->setType(Sale::TYPE_ORDERED)->output(),
            'invoiced' => $sales->setType(Sale::TYPE_INVOICED)->output(),
            'delivered' => $sales->setType(Sale::TYPE_DELIVERED)->output()
        ];
    }

    private function getUniqueStoresAndProducts(): array {
        $unique_stores_products = new UniqueStoreAndProduct($this->startDateUI, $this->endDateUI);

        if(!empty($this->agent))
            $unique_stores_products->setAgent($this->agent);

        $ordered = $unique_stores_products->setType(Sale::TYPE_ORDERED)->output();
        $invoiced = $unique_stores_products->setType(Sale::TYPE_INVOICED)->output();
        $delivered = $unique_stores_products->setType(Sale::TYPE_DELIVERED)->output();

        return [
            'unique_stores' => [
                'ordered' => $ordered['unique_stores'],
                'invoiced' => $invoiced['unique_stores'],
                'delivered' => $delivered['unique_stores']
            ],
            'unique_products' => [
                'ordered' => $ordered['unique_products'],
                'invoiced' => $invoiced['unique_products'],
                'delivered' => $delivered['unique_products'],
            ]
        ];
    }

    private function getPoints(): array {
        if(!($this->agent instanceof Agent))
            return [];

        $points = new Point($this->startDateUI, $this->endDateUI, $this->agent);

        return [
            'ordered' => $points->setType(Point::TYPE_ORDERED)->output(),
            'invoiced' => $points->setType(Point::TYPE_INVOICED)->output(),
            'delivered' => $points->setType(Point::TYPE_DELIVERED)->output(),
        ];
    }

    private function getTargets(): array {
        $dayFactor = Carbon::parse($this->endDateUI)->diffInDays(Carbon::parse($this->startDateUI)) + 1;

        if($this->endDateUI == $this->startDateUI)
            $dayFactor = 1;

        if(!empty($this->agent) && $this->agent->targets->isNotEmpty()) {
            $agentTarget = $this->agent->targets->first();
            return [
                'sales' => !empty($agentTarget->values['sale']) ? ($agentTarget->values['sale'] * $dayFactor) : null,
                'unique_stores' => !empty($agentTarget->values['unique_stores']) ? ($agentTarget->values['unique_stores'] * $dayFactor) : null,
                'unique_products' => !empty($agentTarget->values['unique_products']) ? ($agentTarget->values['unique_products'] * $dayFactor) : null,
                'points' => !empty($agentTarget->values['points']) ? ($agentTarget->values['points'] * $dayFactor) : null,
            ];
        }

        return ['sales' => 0, 'unique_stores' => 0, 'unique_products' => 0, 'points' => 0];
    }

    private function getTopSkus() {
        $topProductList = new TopProductList($this->startDateUI, $this->endDateUI);

        if(!empty($this->agent))
            $topProductList->setAgent($this->agent);

        return $topProductList->setType(Sale::TYPE_DELIVERED)->output();
    }
}
