<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\StockTransfer\StockTransferInboundGrid;
use Niyotail\Grids\StockTransfer\StockTransferItemGrid;
use Niyotail\Grids\StockTransfer\StockTransferOutboundGrid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\StockTransfer;
use Niyotail\Models\StockTransferItem;
use Niyotail\Models\Warehouse;
use Niyotail\Services\StockTransferService;

class StockTransferController extends Controller
{
    public function inBound(Request $request)
    {
        $grid = StockTransferInboundGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.stock_transfer.inbound-index', compact('grid'));
    }


    public function outbound(Request $request)
    {
        $grid = StockTransferOutboundGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        $warehouses = Warehouse::where('id', '!=', app(MultiWarehouseHelper::class)->getSelectedWarehouse())
            ->get();
        return view('admin.stock_transfer.outbound-index', compact('grid', 'warehouses'));
    }

    public function start(Request $request, StockTransferService $service)
    {
        $service->create($request);
        return response(['message' => 'Stock transfer created']);
    }

    public function update(Request $request, StockTransferService $service)
    {
        $service->update($request);
        return response(['message'=>'Success']);
    }

    public function items($id, Request $request)
    {
        $stockTransfer = StockTransfer::findOrFail($id);
        $grid = StockTransferItemGrid::get(['id' => $id]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.stock_transfer.items', compact('grid', 'stockTransfer'));
    }

    public function markComplete(Request $request, StockTransferService $service)
    {
        $stockTransfer = StockTransfer::with('items')->findOrFail($request->id);
        $itemsWithOutReceivedQuantityCount = StockTransferItem::whereNull('received_quantity')
            ->where('stock_transfer_id', $request->id)->count();
        if($itemsWithOutReceivedQuantityCount > 0) {
            throw new ServiceException('Please add received of all items before marking it complete!');
        }
        $service->markCompleted($stockTransfer);
        return response(['message' => 'Stock transfer completed!']);
    }

    public function markConfirm(Request $request, StockTransferService $service)
    {
        $stockTransfer = StockTransfer::findOrFail($request->id);
        $service->markConfirm($stockTransfer);
        return response(['message' => 'Stock transfer Confirmed!']);
    }

    public function generatePdf(Request $request, StockTransferService $service)
    {
        $stockTransfer = StockTransfer::findOrFail($request->id);
        if (empty($stockTransfer->transferNote)) {
            $service->createStockTransferNote($stockTransfer);
        }
        $stockTransfer->refresh();
        $path = "transfer-notes/{$stockTransfer->transferNote->financial_year}/{$stockTransfer->transferNote->number}.pdf";
        if(!Storage::exists($path)){
            $service->createPdf($stockTransfer->transferNote);
        }
        return Storage::download($path);
    }
}
