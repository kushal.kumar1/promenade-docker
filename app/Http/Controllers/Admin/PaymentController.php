<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\PaymentGrid;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        $grid = PaymentGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.payment.index', [
            'grid' => $grid
        ]);
    }
}
