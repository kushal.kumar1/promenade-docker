<?php

namespace Niyotail\Http\Controllers\Admin;


use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Niyotail\Grids\Audit\AuditGrid;
use Niyotail\Grids\Audit\AuditItemGrid;
use Niyotail\Grids\Audit\HoldListGrid;
use Niyotail\Grids\Audit\StorageReportGrid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Employee;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;
use function view;

class AuditController extends Controller
{
    // Views
    public function index(Request $request)
    {
        $warehouseId = $request->getSession()->get('selected-warehouse');
        $grid = AuditGrid::get(['warehouseId' => $warehouseId]);
        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.audit.index', ['grid' => $grid]);
    }

    public function detail(Audit $audit, Request $request)
    {
        $grid = AuditItemGrid::get(['audit' => $audit]);
        if ($request->ajax()) {
            return $grid;
        }

        return view(
            !$audit->exists ? 'admin.audit.index' : 'admin.audit.detail', [
            'grid' => $grid,
            'audit' => $audit
        ]);
    }

    public function holdList(Request $request)
    {
        $warehouseId = $request->getSession()->get('selected-warehouse');
        $grid = HoldListGrid::get(['warehouseId' => $warehouseId]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.audit.hold_list', [
            'grid' => $grid
        ]);
    }

    public function storageReport(Request $request)
    {
        $warehouseId = $request->getSession()->get('selected-warehouse');
        $grid = StorageReportGrid::get(['warehouseId' => $warehouseId]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.audit.storage_report', [
            'grid' => $grid
        ]);
    }

    public function createAudit(Request $request)
    {
        try {
            $type = $request->type;
            $name = $request->name;
            $reasonId = $request->reasonId;
            $warehouseId = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
            $createdBy = Auth::user();

            $csvString = $request->file('file')->get();
            $itemIdGroups = [];
            preg_match_all('/[0-9]+/', $csvString, $itemIdGroups);
            $itemIds = $itemIdGroups[0] ?? [];

            if (count($itemIds) == 0)
                return Redirect::back()->with('messages', [['type' => 'error', 'message' => 'No Items provided in file.']]);

            DB::transaction(function () use ($warehouseId, $itemIds, $createdBy, $reasonId, $type, $name) {
                $audit = AuditService::createAudit($name, $type, $warehouseId, $createdBy->id, $reasonId);

                foreach ($itemIds as $itemId) {
                    if ($type == 0 and empty(Product::find($itemId)))
                        return Redirect::back()->with('messages', [['type' => 'error', 'message' => "Invalid Product Id ({$itemId}) provided"]]);
                    if ($type == 1 and empty(Storage::find($itemId)))
                        return Redirect::back()->with('messages', [['type' => 'error', 'message' => "Invalid Storage Id ({$itemId}) provided"]]);

                    AuditService::generateAuditItems($audit, $itemId);
                }
            });

            return Redirect::back()->with('messages', [
                ['type' => 'success', 'message' => 'Audit Created']
            ]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function confirmAudit(Audit $audit, Request $request)
    {
        try {
            AuditService::confirmAudit($audit, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Confirmed']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function startAudit(Audit $audit, Request $request)
    {
        try {
            AuditService::startAudit($audit, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Started']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function approveAudit(Audit $audit, Request $request)
    {
        try {
            AuditService::approveAudit($audit, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Approved']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function completeAudit(Audit $audit, Request $request)
    {
        try {
            AuditService::completeAudit($audit, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Completed']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function cancelAudit(Audit $audit, Request $request)
    {
        try {
            AuditService::cancelAudit($audit, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Cancelled']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function assign($auditId, $auditItemId, Request $request)
    {
        try {
            $audit = Audit::findOrFail($auditId);
            $auditItem = AuditItem::findOrFail($auditItemId);
            $assignedTo = Employee::findOrFail($request->assignedTo);

            AuditItemService::assignAuditItem($auditItem, Auth::user(), $assignedTo);

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Item Assigned']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function reject($auditId, $auditItemId, Request $request)
    {
        try {
            $audit = Audit::findOrFail($auditId);
            $auditItem = AuditItem::findOrFail($auditItemId);
            AuditItemService::rejectAuditItem($auditItem, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Item Rejected']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function action($auditId, $auditItemId, Request $request)
    {
        try {
            $audit = Audit::findOrFail($auditId);
            $auditItem = AuditItem::findOrFail($auditItemId);
            $action = $request->action;

            switch ($action) {
                case AuditItemAction::ACTION_HOLD:
                    AuditItemService::hold($auditItem, Auth::user(), $request->quantity, $request->destinationStorageId, $request->reasonId);
                    break;
                case AuditItemAction::ACTION_EXCESS:
                    AuditItemService::markExcess(
                        $auditItem,
                        Auth::user(),
                        $request->quantity,
                        $request->destinationStorageId,
                        $request->reasonId
                    );
                    break;
                case AuditItemAction::ACTION_LOSS:
                    AuditItemService::markLoss(
                        $auditItem,
                        Auth::user(),
                        $request->quantity,
                        $request->destinationStorageId,
                        $request->reasonId
                    );
                    break;
                case AuditItemAction::ACTION_RELEASE:
                {
                    Storage::findOrFail($request->destinationStorageId);
                    AuditItemService::release(
                        $auditItem,
                        Auth::user(),
                        $request->quantity,
                        $request->destinationStorageId,
                        $request->reasonId
                    );
                    break;
                }
                case 'dismiss':
                {
                    $auditItem = AuditItem::find($auditItemId);
                    $auditItem->status = AuditItem::STATUS_RESOLVED;
                    $auditItem->save();
                    break;
                }
            }

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => '']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function cancel($auditId, $auditItemId, Request $request)
    {
        try {
            $audit = Audit::findOrFail($auditId);
            $auditItem = AuditItem::findOrFail($auditItemId);
            AuditItemService::cancelAuditItem($auditItem, Auth::user());

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'Audit Item Cancelled']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function bulkAssign(Audit $audit, Request $request)
    {
        try {
            $selectedAuditItemIds = $request->selected;
            $assignedTo = Employee::findOrFail($request->assignedTo);
            $employee = Auth::user();
            if (empty($selectedAuditItemIds))
                return Redirect::back()->with('messages', [['type' => 'info', 'message' => 'No Audit Items selected!']]);

            DB::transaction(function () use ($assignedTo, $selectedAuditItemIds, $employee) {
                foreach ($selectedAuditItemIds as $auditItemId) {
                    $auditItem = AuditItem::findOrFail($auditItemId);
                    AuditItemService::assignAuditItem($auditItem, $employee, $assignedTo);
                }
            });

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'assigned selected audit items']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function bulkReject(Audit $audit, Request $request)
    {
        try {
            $selectedAuditItemIds = $request->selected;
            $employee = Auth::user();
            if (empty($selectedAuditItemIds))
                return Redirect::back()->with('messages', [['type' => 'info', 'message' => 'No Audit Items selected!']]);

            DB::transaction(function () use ($selectedAuditItemIds, $employee) {
                foreach ($selectedAuditItemIds as $auditItemId) {
                    $auditItem = AuditItem::findOrFail($auditItemId);
                    AuditItemService::rejectAuditItem($auditItem, $employee);
                }
            });

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'rejected selected audit items']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }

    public function bulkCancel(Audit $audit, Request $request)
    {
        try {
            $selectedAuditItemIds = $request->selected;
            $employee = Auth::user();
            if (empty($selectedAuditItemIds))
                return Redirect::back()->with('messages', [['type' => 'info', 'message' => 'No Audit Items selected!']]);

            DB::transaction(function () use ($selectedAuditItemIds, $employee) {
                foreach ($selectedAuditItemIds as $auditItemId) {
                    $auditItem = AuditItem::findOrFail($auditItemId);
                    AuditItemService::cancelAuditItem($auditItem, $employee);
                }
            });

            return Redirect::back()->with('messages', [['type' => 'success', 'message' => 'canceled selected audit items']]);
        } catch (Exception $exception) {
            return Redirect::back()->with('messages', [['type' => 'error', 'message' => $exception->getMessage()]]);
        }
    }
}