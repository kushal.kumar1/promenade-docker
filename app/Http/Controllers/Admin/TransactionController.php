<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Transaction\StoreRequest;
use Niyotail\Http\Requests\Admin\Transaction\UpdateRequest;
use Niyotail\Services\TransactionService;
use Niyotail\Models\Store;
use Niyotail\Models\Bank;
use Niyotail\Models\Transaction;
use Niyotail\Grids\TransactionGrid;
use Niyotail\Grids\TransactionsGrid;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function index($storeId, Request $request)
    {
        $store = Store::with('creditLimit')->find($storeId);
        abort_if(empty($store), 404);

        $grid = TransactionGrid::get(['store_id'=>$store->id]);
        if ($request->ajax()) {
            return $grid;
        }

        $payment_methods = Transaction::getConstants('PAYMENT_METHOD');
        // $types = Transaction::getConstants('TYPE');

        $mappedPaymentMethods = Transaction::MAP_PAYMENT_METHODS;

        return view('admin.transaction.index', compact('grid', 'store', 'payment_methods', 'mappedPaymentMethods'));
    }

    public function store(StoreRequest $request, TransactionService $transactionService)
    {
        $transaction = $transactionService->storeTransaction($request, Auth::guard('admin')->user());
        return response()->json($transaction);
    }

    public function create(Request $request)
    {
        $store = Store::getWithLatestTransactions($request->store_id);
        abort_if(empty($store), 404);
        $payment_methods = Transaction::getConstants('PAYMENT_METHOD');
        $chequeTransactions = $store->transactions->isNotEmpty() ? $store->transactions->where('type', 'credit') : null;
        $banks = Bank::active()->get();
        $mappedPaymentMethods = Transaction::MAP_PAYMENT_METHODS;
        return view('admin.transaction.create', compact('store', 'payment_methods', 'mappedPaymentMethods', 'chequeTransactions','banks'));
    }

    public function edit(Request $request, $id)
    {
        $transaction = Transaction::withTrashed()->with('store','settledDebitTransactions', 'settledAmount', 'settledByCreditTransaction', 'chequeMetas')->where('id', $id)->first();
        abort_if(empty($transaction), 404);
        $store = $transaction->store;
        return view('admin.transaction.edit', compact('store', 'transaction'));
    }

    public function delete(Request $request, TransactionService $transactionService)
    {
        $transaction = Transaction::find($request->transaction_id);
        abort_if(empty($transaction), 404);
        $storeID = $transaction->store_id;
        $transactionService->deleteTransaction($request, Auth::guard('admin')->user(), $transaction);
        //return redirect()->route('admin::transactions', ['id' => $storeID]);
    }

    public function update(UpdateRequest $request, TransactionService $transactionService)
    {
        $transaction = Transaction::find($request->transaction_id);
        abort_if(empty($transaction), 404);
        $storeID = $transaction->store_id;
        if ($request->action_type == 'accept') {
          $transactionService->approveTransaction($request, Auth::guard('admin')->user(), $transaction);
        }
        else {
          $transactionService->deleteTransaction($request, Auth::guard('admin')->user(), $transaction);
        }
    }

    public function getTransactions(Request $request)
    {
        $storeID = $request->store_id;
        $type = $request->type;
        $grid = TransactionsGrid::get(['type' => $type, 'store_id' => $storeID]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.transactions.index', compact('grid', 'type'));
    }
}
