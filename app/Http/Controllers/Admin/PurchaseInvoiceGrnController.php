<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Grids\PurchaseInvoiceGrnGrid;
use Niyotail\Helpers\Utils;
use Niyotail\Http\Controllers\Controller;

use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceGrn;
use Niyotail\Services\PurchaseInvoiceGrnService;

class PurchaseInvoiceGrnController extends Controller
{
    protected $grnService;

    public function __construct()
    {
        $this->grnService = new PurchaseInvoiceGrnService();
    }

    public function index(Request $request)
    {
        $grid = PurchaseInvoiceGrnGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.purchase_invoice.grn.index', [
            'grid' => $grid
        ]);
    }

    public function create($invoiceId, Request $request)
    {

    }

    public function store($invoiceId, Request $request)
    {
        if(!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $selectedItems = $request->get('items', []);
        if(empty($selectedItems))
        {
            throw new \Exception('Please select at least 1 item to create GRN');
        }

        /* @var PurchaseInvoice $invoice */
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);

        $grn = $this->grnService->createGrnWithItems($invoice, $request);
        return ['success' => true, 'message' => 'GRN generated successfully', 'payload' => $grn];
    }

    public function importInventoryByGrn($grnId) {
        if(!Gate::allows('update-purchase-invoice')) {
            throw new \Exception('You shall not pass.');
        }

        $grn = PurchaseInvoiceGrn::findOrFail($grnId);
        $this->grnService->importInventoryByGrn($grn);
        return redirect(route('admin::purchase-invoices.editInvoice', $grn->purchase_invoice_id));
    }

    public function update(Request $request)
    {

    }
}