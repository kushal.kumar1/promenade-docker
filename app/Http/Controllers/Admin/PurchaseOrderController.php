<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Niyotail\Grids\PurchaseOrderGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\PurchaseOrder\AddNewGroupRequest;
use Niyotail\Http\Requests\Admin\PurchaseOrder\CreateRequest;
use Niyotail\Http\Requests\Admin\PurchaseOrder\UpdatePurchaseOrderItem;
use Niyotail\Http\Requests\Admin\PurchaseOrder\UpdatePurchaseOrderRequest;
use Niyotail\Models\PurchaseOrder;
use Niyotail\Models\Warehouse;
use Niyotail\Services\PurchaseOrderService;

class PurchaseOrderController extends Controller
{
    protected $purchaseOrderService;

    public function __construct()
    {
        $this->purchaseOrderService = new PurchaseOrderService();
    }

    public function index(Request $request)
    {
        $grid = PurchaseOrderGrid::get();

        if ($request->ajax())
        {
            return $grid;
        }

        return view('admin.purchase_order.list', compact('grid'));
    }

    public function details ($id) {
        $purchaseOrder = $this->purchaseOrderService->getFullPurchaseOrderById($id);
        $warehouses = Warehouse::all();
        return view('admin.purchase_order.details', compact('purchaseOrder', 'warehouses'));
    }

    public function createPurchaseOrder(CreateRequest $request) {
        $vendorId = $request->input('vendor_id');
        $po = $this->purchaseOrderService->createPurchaseOrder($vendorId, 'pending');
        $poId = $po->id;
        return redirect(route('admin::purchase-order.single', $poId));
    }

    public function updatePurchaseOrder($poId, UpdatePurchaseOrderRequest $request)
    {
        $vendorAddressId = $request->vendor_address_id;
        $warehouseId = $request->warehouse_id;
        $deliveryDate = $request->delivery_date;
        return $this->purchaseOrderService->updatePurchaseOrder($poId, $vendorAddressId, $warehouseId, $deliveryDate);
    }

    public function updatePurchaseOrderItem($poId, UpdatePurchaseOrderItem $request) {
        $poItemId = $request->input('id');
        $productId = $request->input('product_id');
        $quantity = $request->input('quantity');
        $sku = $request->input('sku');
        $priceCalculationType = $request->input('price_type');
        $priceCalculationFactor = $request->input('pc_factor', 0);
        return $this->purchaseOrderService->updatePurchaseOrderItem($poId,
            $poItemId, $productId, $sku, $quantity, $priceCalculationType, $priceCalculationFactor);
    }

    public function addNewGroupToPurchaseOrder($po, AddNewGroupRequest $request)
    {
        $groupId = $request->input('group_id');
        $quantity = $request->input('quantity');
        $result = $this->purchaseOrderService->addNewGroupToPurchaseOrder($po, $groupId, $quantity);
        return response()->json($result);
    }

    public function placePurchaseOrder($poId)
    {
        $this->purchaseOrderService->placePurchaseOrder($poId);
    }

    public function generatePurchaseInvoice($id)
    {
        return $this->purchaseOrderService->generatePurchaseInvoice($id);
    }

    public function generatePurchaseOrderFile($poId)
    {
        $this->purchaseOrderService->generatePurchaseOrderFile($poId);
        return redirect()->route('admin::purchase-order.single', $poId);
    }

    public function getPurchaseOrderFile($poId)
    {
        try {
            $po = PurchaseOrder::find($poId);
            abort_if(empty($po), 404);

            $pdf = $po->file;
            abort_if(empty($pdf), 404);
            return Storage::download('purchase_order/'.$pdf);
        } catch (\Exception $ex)
        {
            throw new \Exception($ex->getMessage());
        }
    }
}
