<?php

namespace Niyotail\Http\Controllers\Admin;

use Clegginabox\PDFMerger\PDFMerger;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Niyotail\Grids\DispatchManifest\ManifestGrid;
use Niyotail\Grids\DispatchManifest\CreateManifestGrid;
use Niyotail\Http\Requests\Admin\Manifest\CancelRequest;
use Niyotail\Http\Requests\Admin\Manifest\CloseRequest;
use Niyotail\Http\Requests\Admin\Manifest\CreateRequest;
use Niyotail\Http\Requests\Admin\Manifest\DeliverRequest;
use Niyotail\Http\Requests\Admin\Manifest\DetailsRequest;
use Niyotail\Http\Requests\Admin\Manifest\DispatchRequest;
use Niyotail\Http\Requests\Admin\Manifest\RemoveShipmentRequest;
use Niyotail\Http\Requests\Admin\Manifest\ViewRequest;
use Niyotail\Models\Manifest;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Shipment;
use Niyotail\Services\ManifestService;
use Niyotail\Exceptions\ServiceException;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Storage;

class ManifestController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ManifestGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.manifest.index', [
            'grid' => $grid
        ]);
    }

    public function create(Request $request)
    {
        $grid = CreateManifestGrid::get();
        if ($request->ajax()) {
            return $grid;
        }

        abort_unless(Manifest::canCreate(), 403, "Close all manifests older than 35 days!");
        return view('admin.manifest.create', compact('grid'));
    }

    public function edit(DetailsRequest $request, $id)
    {
        $manifest = Manifest::with(['shipments'=>function ($query) {
            $query->withCount('items')->with(['invoice', 'orderItems']);
        },'locations'])->find($id);
        abort_if(empty($manifest), 404);
        return view('admin.manifest.edit', compact('manifest'));
    }

    public function updateDetails(Request $request, ManifestService $manifestService)
    {
        $manifest = Manifest::find($request->id);
        abort_if(empty($manifest), 404);
        $manifest = $manifestService->updateDetails($request);
        return response()->json($manifest);
    }

    public function store(CreateRequest $request, ManifestService $manifestService)
    {
        $manifest = $manifestService->create($request, Auth::guard('admin')->user());
        return response()->json($manifest);
    }

    public function cancel(CancelRequest $request, ManifestService $manifestService)
    {
        $manifest = $manifestService->cancel($request);
        return response()->json($manifest);
    }

    public function removeShipment(RemoveShipmentRequest $request, ManifestService $manifestService)
    {
        $manifest = $manifestService->removeShipment($request);
        return response()->json($manifest);
    }

    public function close(CloseRequest $request, ManifestService $manifestService)
    {
        $manifest = $manifestService->close($request);
        return response()->json($manifest);
    }

    public function dispatched(DispatchRequest $request, ManifestService $manifestService)
    {
        $manifest = Manifest::find($request->id);
        if (empty($manifest->vehicle_number) || empty($manifest->agent_id) || $manifest->locations->isEmpty()) {
            return response()->json(['message' => "Locations or Agent or Vehicle Missing. Kindly update the information"], 422);
        }
        $manifest = $manifestService->dispatch($request);
        return response()->json($manifest);
    }

    public function deliverShipments(DeliverRequest $request, ManifestService $manifestService)
    {
        $manifest = $manifestService->markShipmentsDelivered($request);
        return response()->json($manifest);
    }

    public function getPdf($id)
    {
        $manifest = Manifest::find($id);
        abort_if(empty($manifest), 404);
        if (App::environment() == 'local') {
            $service = new ManifestService();
            $pdf =  $service->getPdf($manifest);
            return $pdf->stream();
        }
        return response()->file($manifest->pdf_url);
    }

    public function getInvoices($id)
    {
        $manifest = Manifest::with('shipments.invoice')->find($id);
        $pdf = new PDFMerger();
        foreach ($manifest->shipments as $shipment){
            $invoicePdf = "invoices/".$shipment->invoice->financial_year."/".$shipment->invoice->number.".pdf";
            $pdf->addPDF(Storage::path($invoicePdf), 'all');
        }
        $pdf->merge('browser', '','P');
    }
}
