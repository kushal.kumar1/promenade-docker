<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\ProductRequestGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\ProductRequest\CreateRequest;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\TaxClass;
use Niyotail\Services\Audit\AuditService;
use Niyotail\Services\ProductRequestService;
use Niyotail\Services\ProductService;

class ProductRequestController extends Controller
{
    public function index(Request $request)
    {
        $grid = ProductRequestGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.product_request.index', [
            'grid' => $grid
        ]);
    }

    public function edit($id)
    {
        $product = ProductRequest::with('taxes', 'brand', 'collection', 'group', 'newCatL4')->find($id);
        abort_if(empty($product), 404);
        $taxClasses = TaxClass::active()->get();
        $measureUnits = ProductVariant::getConstants('uom');;
        // $newCat = NewCatL4::with('')

        return view('admin.product_request.edit')->with(compact('product', 'taxClasses', 'measureUnits'));
    }

    public function store(CreateRequest $request, ProductRequestService $service)
    {
        $productRequest = $service->create($request);
        if ($productRequest->source == 'audit') {
            $user = Auth::guard('admin')->user();
            $audit = Audit::findOrFail($request->source_id);
            (new AuditService())->addItem($request, $audit, $user, $productRequest);
        }
        $productRequest->loadMissing('taxes');
        return $productRequest;
    }

    public function getById(Request $request)
    {
        if (!$request->has('id')) {
            throw new ServiceException('Id is required');
        }

        $id = $request->get('id');
        return ProductRequest::with('taxes')->find($id);
    }

    public function createProductFromRequest($id)
    {
        $service = new ProductService();
        $productRequest = ProductRequest::findOrFail($id);
        $product = $service->createFromRequest($productRequest);
        return redirect(route('admin::products.edit', $product->id));
    }

    public function deleteProductRequest($id)
    {
        $product = ProductRequest::findOrFail($id);
        $product->delete();
        Session::flash('message', 'Product request deleted successfully.');
        Session::flash('alert-class', 'alert-success');
    }

    public function update(Request $request, ProductRequestService $service)
    {
        try {
            $productRequest = $service->update($request);
        } catch (ServiceException $exception) {
            return response(['error' => $exception->getMessage()], 422);
        }
        return $productRequest;
    }
}
