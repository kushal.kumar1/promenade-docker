<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\RuleGrid;
use Niyotail\Models\Rule;
use Niyotail\Services\RuleService;
use Niyotail\Models\ProductVariant;
use Niyotail\Http\Requests\Admin\Rule\CreateRuleRequest;
use Niyotail\Http\Requests\Admin\Rule\UpdateRuleRequest;
use Niyotail\Http\Requests\Admin\Rule\ViewRuleRequest;

class RuleController extends Controller
{
    public function index(ViewRuleRequest $request)
    {
        $grid = RuleGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.rule.index', [
            'grid' => $grid
        ]);
    }

    public function detail($id, ViewRuleRequest $request)
    {
        $rule = Rule::with(['conditions.expressions.attribute' => function ($morphTo) {
            $morphTo->morphWith([
                ProductVariant::class => ['product']
            ]);
        }])->with(['targets.attribute' => function ($morphTo) {
            $morphTo->morphWith([
                ProductVariant::class => ['product']
            ]);
        }])->find($id);
        abort_if(empty($rule), 404);
        $rule->conditions->map(function ($condition) {
            $condition->groupedExpressions = $condition->expressions->groupBy('name');
            return $condition;
        });
        return view('admin.rule.form', compact('rule'));
    }

    public function create(ViewRuleRequest $request)
    {
        return view('admin.rule.form');
    }

    public function store(CreateRuleRequest $request, RuleService $ruleService)
    {
        $rule = $ruleService->create($request);

        return $rule;
    }

    public function update($id, UpdateRuleRequest $request, RuleService $ruleService)
    {
        $rule = Rule::find($id);
        abort_if(empty($rule), 404);
        $rule = $ruleService->update($rule, $request);

        return $rule;
    }
}
