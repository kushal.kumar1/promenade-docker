<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Events\Order\OrderPlaced;
use Niyotail\Events\Shipment\ShipmentGenerated;
use Niyotail\Helpers\TaxHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Agent;
use Niyotail\Models\Cart;
use Niyotail\Models\CartAddress;
use Niyotail\Models\CartItem;
use Niyotail\Models\Order;
use Niyotail\Models\OrderAddress;
use Niyotail\Models\OrderItem;
use Niyotail\Models\OrderLog;
use Niyotail\Models\Pincode;
use Niyotail\Models\Shipment;
use Niyotail\Models\ShipmentItem;
use Niyotail\Services\Order\OrderItemService;

class SpecialCaseController extends Controller
{
    protected $orderItemService;

    public function __construct()
    {
        $this->orderItemService = new OrderItemService();
    }

    public function createShipment($orderId) {
        $order = Order::find($orderId);

        $shipment = DB::transaction(function () use($order) {
            $shipment = new Shipment();
            $shipment->status = Shipment::STATUS_GENERATED;
            $shipment->boxes = 0;
            $shipment->order()->associate($order);
            $shipment->save();

            $shipmentItems = [];
            $amount = 0;
            $items = $order->items->where('status', 'processing');
            $shipmentItems = [];
            $itemIds = [];
            foreach ($items as $item) {
                $total = $item->total;
                $amount += $total;

                if($amount >= 45000) {
                    break;
                }

                $itemIds[] = $item->id;

                $shipmentItems[] = [
                    'shipment_id' => $shipment->id,
                    'order_item_id' => $item->id
                ];
            }

            OrderItem::whereIn('id', $itemIds)->update(['status' => OrderItem::STATUS_BILLED]);
            ShipmentItem::insert($shipmentItems);
            event(new ShipmentGenerated($shipment));
            return $shipment;
        });

        return $shipment;
    }

    public function createOrderFromCart($cartId) {
        $employee = Auth::guard('admin')->user();
        $cart = Cart::find($cartId);
        abort_if(empty($cart), 422);

        $orderId = '40469';
        $order = Order::find($orderId);
        $orderItems = $order->items->groupBy('sku');

        $loadedItems = [];
        foreach ($orderItems as $sku=>$orderItem) {
            $loadedItems[] = $sku;
        }

        $cartItems = $cart->items->whereNotIn('sku', $loadedItems);

        foreach ($cartItems as $item) {
            $sku = $item->sku;
            $quantity = $item->quantity;

            $discount = splitAmount($item->discount, $item->quantity);
            foreach ($item->rules as $rule) {
                $rule->discounts = splitAmount($rule->pivot->discount, $item->quantity);
            }
            $itemQuantity = intVal($item->quantity);
            for ($i = 1; $i <= $itemQuantity; $i++) {
                $orderItem = $this->orderItemService->createSpecial($item, $order->id, $discount[$i], $i);
                //calculate & save taxes
                $this->createItemTaxes($orderItem, $order->store->pincode);
                $orderItem->load('taxes');
                $this->orderItemService->collectTotals($orderItem);
            }
        }

        $cart->order()->associate($order);
        $cart->save();
        $order->refresh();

        // niyo_logger()->on($order)->log("Order Created");
        $orderLog = new OrderLog();
        $log = 'Order Created by ' . $order->createdBy->name . ' (' . $order->created_by_type . ')';
        if(!is_array($log))
            $log=[$log];
        $orderLog->description = json_encode(['data' => $log]);
        $orderLog->order_id = $order->id;
        $orderLog->employee_id = 45;
        $orderLog->save();


        //Collect all totals
        $this->collectTotals($order);
        return $order;



        $order = DB::transaction(function () use ($cart) {

            $data = $cart->attributesToArray();
            unset($data['order_id']);
            $data['reference_id'] = time() . rand(0, 9);
            $data['payment_method'] = 'cash';
            $data['required_delivery_date'] = null;
            $data['status'] = Order::STATUS_CONFIRMED;
            $data['source'] = Order::SOURCE_SYSTEM;
            $order = Order::create($data);
            // add address
            $cart->load('shippingAddress', 'billingAddress');
            $cart->loadMissing('items.rules');
            if (!empty($cart->shippingAddress)) {
                $orderShippingAddresses = $this->processAddress((new OrderAddress()), $cart->shippingAddress);
                $orderShippingAddresses->save();
                if ($cart->shippingAddress->id == $cart->billingAddress->id) {
                    $orderBillingAddresses = $orderShippingAddresses;
                } else {
                    $orderBillingAddresses = $this->processAddress((new OrderAddress()), $cart->billingAddress);
                    $orderBillingAddresses->save();
                }
                $order->shippingAddress()->associate($orderShippingAddresses);
                $order->billingAddress()->associate($orderBillingAddresses);
                $order->save();
            }

            //add items
            foreach ($cart->items as $item) {

                dd(
                    $item
                );

                $discount = splitAmount($item->discount, $item->quantity);
                foreach ($item->rules as $rule) {
                    $rule->discounts = splitAmount($rule->pivot->discount, $item->quantity);
                }
                $itemQuantity = intVal($item->quantity);
                for ($i = 1; $i <= $itemQuantity; $i++) {
                    $orderItem = $this->orderItemService->createSpecial($item, $order->id, $discount[$i], $i);
                    //calculate & save taxes
                    $this->createItemTaxes($orderItem, $order->store->pincode);
                    $orderItem->load('taxes');
                    $this->orderItemService->collectTotals($orderItem);
                }
            }
            //associate order to cart
            $cart->order()->associate($order);
            $cart->save();
            $order->refresh();

            // niyo_logger()->on($order)->log("Order Created");
            $orderLog = new OrderLog();
            $log = 'Order Created by ' . $order->createdBy->name . ' (' . $order->created_by_type . ')';
            if(!is_array($log))
                $log=[$log];
            $orderLog->description = json_encode(['data' => $log]);
            $orderLog->order_id = $order->id;
            $orderLog->employee_id = 45;
            $orderLog->save();


            //Collect all totals
            $this->collectTotals($order);
            return $order;
        });


        return response()->json($order);
    }

    public function addAllCartItems($cartId) {
        $cart = Cart::find($cartId);

        $cartItems = DB::table('special_order_items as soi')
            ->selectRaw('soi.id as test, soi.quantity as final_qty, p.*, pv.*')
            ->leftJoin('products as p', 'p.id', 'soi.product_id')
            ->leftJoin('product_variants as pv', 'pv.product_id', 'p.id')
            ->where('pv.value', 'unit')
            ->get();
        $validCartItems = $cartItems->where('final_qty', '>', 0);

        foreach ($validCartItems as $item) {

            $cartItem = [
                'cart_id' => $cartId,
                'warehouse_id' => 1,
                'product_id' => $item->product_id,
                'sku' => $item->sku,
                'variant' => 'unit',
                'mrp' => $item->mrp,
                'min_price' => $item->min_price,
                'price' => $item->mrp,
                'quantity' => $item->final_qty
            ];

            $tax = TaxHelper::getTaxes($item->tax_class_id, $item->mrp, 13, 13, 101);
            $taxPerc = $tax->sum('percentage');
            $subTotal = round($cartItem['mrp']/(1 + $taxPerc/100), 4);

            $cartItem['subtotal'] = $subTotal*$item->final_qty;
            $cartItem['discount'] = 0;
            $cartItem['tax'] = ($cartItem['mrp'] - $subTotal)*$item->final_qty;
            $cartItem['total'] = $cartItem['mrp']*$item->final_qty;
            $cartItem['source'] = 'manual';
            $cartItem['parent_sku'] = '';
            CartItem::insert($cartItem);
        }

        dd($validCartItems);

    }

    private function processAddress(OrderAddress $orderAddress, CartAddress $cartAddress):OrderAddress
    {
        $orderAddress->name=$cartAddress->name;
        $orderAddress->mobile=$cartAddress->mobile;
        $orderAddress->address=$cartAddress->address;
        $orderAddress->landmark=$cartAddress->landmark;
        $orderAddress->pincode=$cartAddress->pincode;
        $orderAddress->lat=$cartAddress->lat;
        $orderAddress->lng=$cartAddress->lng;
        $orderAddress->city_id=$cartAddress->city_id;
        return $orderAddress;
    }

    public function createItemTaxes(OrderItem $item, $storePincode)
    {
        $pincode = Pincode::with('city.state')->where('pincode', $storePincode)->first();
        $sourceStateId = $item->warehouse->city->state_id;
        $supplyState = $pincode->city->state;
        $taxes = TaxHelper::getTaxes($item->product->tax_class_id, $item->mrp, $sourceStateId, $supplyState->id, $supplyState->country_id);

        $orderItemTaxes= [];
        if (!empty($taxes)) {
            $totalTaxRate = $taxes->sum('percentage');
            $additionalTax = $taxes->sum('additional_charges_per_unit') * $item->quantity * $item->productVariant->quantity;
            $taxAmount = inclusiveTaxAmount($item->price * $item->quantity - $item->discount, $totalTaxRate, $additionalTax);
            $priceExcludingTax =  $item->price * $item->quantity - $item->discount - $taxAmount;
            foreach ($taxes as $tax) {
                $additionalCharges = $tax->additional_charges_per_unit * $item->quantity * $item->productVariant->quantity;
                $orderItemTaxes[] = $this->getItemTax($tax, $priceExcludingTax, $additionalCharges);
            }
        }
        $item->taxes()->createMany($orderItemTaxes);
    }

    public function collectTotals(Order $order)
    {
        $order->loadMissing('items');
        $subTotal = $discount = $tax = 0;
        foreach ($order->items as $item) {
            $subTotal += $item->subtotal;
            $discount += $item->discount;
            $tax += $item->tax;
        }

        $order->subtotal = $subTotal;
        $order->discount = $discount;
        $order->tax = $tax;
        $order->total = $subTotal - $discount + $tax;
        $order->save();
    }

    private function getItemTax($tax, $priceExcludingTax, $additionalCharges)
    {
        if($tax->percentage) {
            return [
                'name' => $tax->name,
                'percentage' => $tax->percentage,
                'amount' => (($priceExcludingTax * $tax->percentage) / 100) +  $additionalCharges
            ];
        }

    }
}