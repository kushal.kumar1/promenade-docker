<?php

namespace Niyotail\Http\Controllers\Admin;

use Faker\Guesser\Name;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\PincodeGrid;
use Niyotail\Http\Requests\Admin\Pincode\ViewPincodeRequest;
use Niyotail\Http\Requests\Admin\Pincode\CreatePincodeRequest;
use Niyotail\Http\Requests\Admin\Pincode\UpdatePincodeRequest;
use Niyotail\Models\City;
use Niyotail\Models\Pincode;
use Niyotail\Services\PincodeService;

// use Niyotail\Http\Requests\Admin\;


class PincodeController extends Controller
{
    public function index(ViewPincodeRequest $request)
    {
        $grid = PincodeGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.pincode.index', compact('grid'));
    }

    public function create()
    {
        $cities = City::get();
        return view('admin.pincode.form')->with(compact('cities'));
    }

    public function store(CreatePincodeRequest $request, PincodeService $ruleService)
    {
        $pincode = $ruleService->create($request);
        return $pincode;
    }

    public function edit ($id)
    {
        $pincode = Pincode::find($id);
        $cityName = null;
        abort_if(empty($pincode), 404);
        $cities = City::get();
        return view('admin.pincode.form', compact('pincode', 'cities', 'id'));
    }

    public function update (UpdatePincodeRequest $request, PincodeService $PincodeService, $id)
    {
        $PincodeService->update($request, $id);
      
        return redirect()->back()->withSuccess("Pincode information has been updated.");
    }
}
