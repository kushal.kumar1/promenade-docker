<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\ProductGroupGrid;
use Niyotail\Services\ProductGroupService;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Niyotail\Models\ProductGroup;

class ProductGroupController extends Controller
{
    public function index(Request $request) {
        $grid = ProductGroupGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.product_group.index', [
            'grid' => $grid
        ]);
    }

    public function add (Request $request) {
        return view('admin.product_group.form');
    }

    public function store(Request $request, ProductGroupService $productGroupService)
    {
        $productGroup = $productGroupService->create($request, Auth::guard('admin')->user());
        return response()->json($productGroup);
    }

    public function edit ($id, Request $request) {
        $group = ProductGroup::findOrFail($id);
        return view('admin.product_group.form')->with(compact('group'));
    }

    public function update ($id, Request $request, ProductGroupService $productGroupService) {
        $productGroup = $productGroupService->update($id, $request, Auth::guard('admin')->user());
        return response()->json($productGroup);
    }

    public function delete ($id) {
        ProductGroup::findOrFail($id)->delete();
        return Redirect::back();
    }
}
