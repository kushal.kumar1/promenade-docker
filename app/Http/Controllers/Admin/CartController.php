<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Services\Cart\CartService;
use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Niyotail\Models\Product;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Tag;
use Niyotail\Http\Requests\Admin\Cart\AddItemRequest;
use Niyotail\Http\Requests\Admin\Cart\UpdateItemRequest;
use Niyotail\Http\Requests\Admin\Cart\RemoveItemRequest;

class CartController extends Controller
{
    public function suggestions(Request $request)
    {
        $term = $request->term;
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        $products = Product::active()
            ->inventoryCount([$selectedWarehouse])
            ->where("name", "like", "%$term%")
            ->orWhere("barcode", "like", "%$term%")
            ->with('variants', 'brand', 'images')->limit(50)->get();
        return view('admin.cart._products', compact('products'));
    }

    public function addItem(AddItemRequest $request, CartService $cartService)
    {
        $employee = Auth::guard('admin')->user();
        $cart = Cart::byEmployee($employee)
            ->with('items.product.images')
            ->where('store_id', $request->store_id)
            ->where('warehouse_id', $request->warehouse_id)
            ->latest()->first();
        $cartService->setCart($cart);
        $cart = $cartService->addToCart($request, $employee);
        $cart->load('items.product.images');
        $tags = Tag::order()->active()->get();
        return view('admin.cart._items', compact('cart', 'tags'));
    }

    public function updateItem(UpdateItemRequest $request, CartService $cartService)
    {
        $employee = Auth::guard('admin')->user();
        $cartItem = CartItem::whereHas('cart', function ($cartQuery) use ($employee) {
            $cartQuery->byEmployee($employee);
        })->find($request->id);
        abort_if(empty($cartItem), 404);
        $cart = $cartService->updateQuantity($request);
        $cart->load('items.product.images');
        $tags = Tag::order()->active()->get();
        return view('admin.cart._items', compact('cart', 'tags'));
    }

    public function removeItem(RemoveItemRequest $request, CartService $cartService)
    {
        $employee = Auth::guard('admin')->user();
        $cartItem = CartItem::whereHas('cart', function ($cartQuery) use ($employee) {
            $cartQuery->byEmployee($employee);
        })->find($request->id);
        abort_if(empty($cartItem), 404);
        $cart = $cartService->removeItem($request);
        $cart->load('items.product.images');
        $tags = Tag::order()->active()->get();
        return view('admin.cart._items', compact('cart', 'tags'));
    }
}
