<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Grids\FlatInventory\ProductWiseInventoryGrid;
use Niyotail\Grids\FlatInventory\ProductLocationsGrid;
use Niyotail\Grids\FlatInventory\StoragesGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Storage;
use Niyotail\Models\Product;

class FlatInventoryController extends Controller
{

    public function productWiseInventoryCount(Request $request)
    {
        $grid = ProductWiseInventoryGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.flat_inventory.product_wise_count', compact('grid'));
    }

    public function storages(Request $request)
    {
        $grid = StoragesGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.flat_inventory.storages', compact('grid'));
    }

    public function storageDetails($id, Request $request)
    {
        $selectString = ['products.name as product, products.id as product_id'];
        foreach (FlatInventory::getConstants('STATUS') as $status) {
            $selectString [] = "COUNT(case when flat_inventories.status='{$status}' then flat_inventories.id end) as {$status}";
        }
        $storage = Storage::select('id', 'label')->find($id);
        $storageData = FlatInventory::join('products', 'flat_inventories.product_id', '=', 'products.id')
            ->selectRaw(implode(', ', $selectString))
            ->where('flat_inventories.storage_id', $id)
            ->groupBy('products.id')
            ->get();
        return view('admin.flat_inventory.storage-detail', compact('storageData', 'storage'));
    }

    public function productLocations($id, Request $request)
    {
        $product = Product::find($id);
        $grid = ProductLocationsGrid::get(['product_id'=>$id]);
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.flat_inventory.product_location', compact('grid', 'product'));
    }
}
