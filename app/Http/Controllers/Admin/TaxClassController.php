<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Validation\Rules\RequiredIf;
use Niyotail\Grids\TaxClassGrid;
use Niyotail\Grids\TaxGrid;
use Niyotail\Http\Requests\Admin\TaxClass\EditRequest;
use Niyotail\Http\Requests\Admin\TaxClass\StoreRequest;
use Niyotail\Http\Requests\Admin\TaxClass\UpdateRequest;
use Niyotail\Http\Requests\Admin\TaxClass\ViewRequest;
use Niyotail\Models\Country;
use Niyotail\Models\State;
use Niyotail\Models\TaxClass;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Warehouse;
use Niyotail\Services\TaxClassService;
use Niyotail\Exceptions\ServiceException;

class TaxClassController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = TaxClassGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.tax_class.index', compact('grid'));
    }

    public function edit(EditRequest $request)
    {
        $grid = TaxGrid::get(['tax_class_id'=>$request->id]);
        if ($request->ajax()) {
            return $grid;
        }
        $taxClass = TaxClass::with('taxes')->find($request->id);
        $countries = Country::get();
        $states = State::where('country_id', 101)->get();
        return view('admin.tax_class.edit', compact('taxClass', 'grid', 'countries', 'states'));
    }

    public function store(StoreRequest $request, TaxClassService $taxClassService)
    {
        $taxClass = $taxClassService->create($request);
        return response()->json($taxClass);
    }

    public function update(UpdateRequest $request, TaxClassService $taxClassService)
    {
        $taxClass = $taxClassService->update($request);
        return response()->json($taxClass);
    }
}
