<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\RoleService;
use Illuminate\Http\Request;
use Niyotail\Grids\RoleGrid;
use Niyotail\Models\Role;
use Niyotail\Models\Permission;
use Niyotail\Http\Requests\Admin\Roles;

class RoleController extends Controller
{
    public function index(Roles\ViewRequest $request)
    {
        $grid = RoleGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.role.index', [
            'grid' => $grid
        ]);
    }

    public function edit($id)
    {
        $role = Role::with('permissions')->find($id);
        abort_if(empty($role), 404);
        $permissions = Permission::orderBy('type', 'asc')->get();
        return view('admin.role.edit', [
            'role' => $role,
            'permissions' => $permissions
        ]);
    }

    public function store(Roles\CreateRequest $request, RoleService $roleService)
    {
        $role = $roleService->create($request);
        return response()->json($role);
    }

    public function update(Roles\UpdateRequest $request, RoleService $roleService)
    {
        $role = $roleService->update($request);
        return response()->json($role);
    }

    public function delete(Request $request, RoleService $roleService)
    {
        $role = $roleService->delete($request);
        return response()->json($role);
    }
}
