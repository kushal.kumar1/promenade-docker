<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Brand\ViewRequest;
use Niyotail\Http\Requests\Admin\Brand\CreateRequest;
use Niyotail\Http\Requests\Admin\Brand\UpdateRequest;
use Niyotail\Models\Brand;
use Niyotail\Services\BrandService;
use Niyotail\Helpers\Image;

class BrandController extends Controller
{
    public function index(ViewRequest $request)
    {
        $brands = Brand::getAllWithParent();
        $brandTree = buildTree($brands->toArray());
        return view('admin.brand.index', compact('brandTree'));
    }

    public function detail($id)
    {
        $brand = Brand::with('marketer')->find($id);
        $brand->logo = Image::getSrc($brand, 100);
        abort_if(empty($brand), 404);
        return $brand;
    }

    public function store(CreateRequest $request, BrandService $brandService)
    {
        $brand = $brandService->create($request);
        return response()->json($brand);
    }

    public function update(UpdateRequest $request, BrandService $brandService)
    {
        $brand = $brandService->update($request);
        return response()->json($brand);
    }

    public function sort(Request $request, BrandService $brandService)
    {
        $brand = $brandService->sort($request);
        return response()->json($brand);
    }

    public function delete(Request $request, BrandService $brandService, $id)
    {
        $brand = $brandService->delete($id);
        return response()->json($brand);
    }
}
