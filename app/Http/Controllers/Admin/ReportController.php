<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\PurchaseInvoice\GrnItemsGrid;
use Niyotail\Grids\PurchaseInvoice\PurchaseInvoiceItemsGrid;
use Niyotail\Grids\Reports\ProductInventorySnapshotGrid;
use Niyotail\Grids\Reports\ReturnOrderItemGrid;
use Niyotail\Grids\Reports\InventoryReportGrid;
use Niyotail\Grids\Reports\OrderItemGrid;
use Niyotail\Grids\Reports\ProcessingOrderItemGrid;
use Niyotail\Grids\Reports\InvoiceGrid;
use Niyotail\Grids\Reports\InventorySnapshotGrid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Niyotail\Http\Requests\Admin\Reports\InventoryReportRequest;
use Niyotail\Http\Requests\Admin\Reports\OrderItemReportRequest;
use Niyotail\Http\Requests\Admin\Reports\ReturnOrderItemRequest;
use Niyotail\Services\ReportService;
use Niyotail\Http\Controllers\Controller;

class ReportController extends Controller
{
    public function inventory(InventoryReportRequest $request)
    {
        $grid = InventoryReportGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Inventory'
        ]);
    }

    public function orderItem(OrderItemReportRequest $request)
    {
        $grid = OrderItemGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Order Items'
        ]);
    }

    public function returnOrderItem(ReturnOrderItemRequest $request)
    {
        $grid = ReturnOrderItemGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Return Order Items'
        ]);
    }

    public function invoices(Request $request)
    {
        $grid = InvoiceGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Invoices'
        ]);
    }

    public function getInventorySnapshot(Request $request)
    {
        $grid = InventorySnapshotGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
              'grid' => $grid,
              'name' => 'Inventory Snapshots'
        ]);
    }

    public function getInventorySnapshotFile($file)
    {
        $fileName = 'inventory/'.$file;
        return Storage::download($fileName);
    }


    public function processingOrderItem(OrderItemReportRequest $request)
    {
        $grid = ProcessingOrderItemGrid::get();

        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Processing Items',
        ]);
    }

    public function purchaseInvoiceItem(OrderItemReportRequest $request)
    {
        $grid = PurchaseInvoiceItemsGrid::get();

        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Purchase Invoice Items',
        ]);
    }

    public function grnItems(Request $request) {
        $grid = GrnItemsGrid::get();

        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'GRN Items',
        ]);
    }

    public function getProductInventorySnapshot(Request $request) {
        $grid = ProductInventorySnapshotGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.report.index', [
            'grid' => $grid,
            'name' => 'Product Inventory Snapshots'
        ]);
    }

    public function getProductInventorySnapshotFile($file)
    {
        $fileName = 'product_inventory/'.$file;
        return Storage::download($fileName);
    }
}
