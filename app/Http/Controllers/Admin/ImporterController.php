<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Grids\ImporterActionGrid;
use Niyotail\Http\Requests\Admin\Importers\CreateRequest;
use Niyotail\Http\Requests\Admin\Importers\ViewRequest;
use Niyotail\Models\ImporterAction;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Services\ImporterActionService;
use Illuminate\Http\Request;

class ImporterController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ImporterActionGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.importer.index', [
            'grid' => $grid
        ]);
    }

    public function store(CreateRequest $request, ImporterActionService $importerActionService)
    {
        $importerAction = $importerActionService->create($request);
        return response()->json($importerAction);
    }
}
