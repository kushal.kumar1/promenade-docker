<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Grids\Putaway\PutawayGrid;
use Niyotail\Grids\Putaway\PutawayItemGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Product;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Services\Putaway\PutawayService;

class PutawayController extends Controller
{
    public function index(Request $request)
    {
        $grid = PutawayGrid::get();
        if ($request->ajax())
        {
            return $grid;
        }
        return view('admin.putaway.index', compact('grid'));
    }

    public function detail($id, Request $request)
    {
        $putaway = Putaway::findOrFail($id);
        $grid = PutawayItemGrid::get(['id' => $putaway->id]);
        if ($request->ajax())
        {
            return $grid;
        }
        return view('admin.putaway.detail', compact('grid','putaway'));
    }

    public function searchProduct(Request $request)
    {
        $products = Product::whereHas('inventories', function($query){
            $query->where('status', FlatInventory::STATUS_PUTAWAY);
        })
            ->active()
            ->where('barcode', $request->term)
            ->get();

        return $products;
    }

    public function create(Request $request, PutawayService $service)
    {
        $putaway = $service->create(2, Auth::guard('admin')->user());
        return $putaway;
    }

    public function putItem($id, Request $request, PutawayService $service)
    {
        $putaway = Putaway::findOrFail($id);
        $service->putItem($putaway, $request->product_id, $request->quantity, $request->storage_id);
    }

    public function close($id, Request $request, PutawayService $service)
    {
        $putaway = Putaway::findOrFail($id);
        $service->close($putaway);
    }

}
