<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Niyotail\Models\Product;

class BarcodeController extends Controller
{
    public function generateLabels($productId, $qty)
    {
        $product = Product::with('variants')->find($productId);
        $logoPath = env('LOGO_ABSOLUTE_PATH');
        $pdf = SnappyPdf
            ::setOrientation('portrait')
            ->setOption('page-width', '108')
            ->setOption('page-height', '25')
            ->setOption('margin-bottom', 0)
            ->setOption('margin-left', 0)
            ->setOption('margin-right', 0)
            ->setOption('margin-top', 0)
            ->loadView('pdf.labels', compact('product','qty','logoPath'));
        return $pdf->stream();
    }
}
