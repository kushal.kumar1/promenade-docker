<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Collection;
use Niyotail\Services\CollectionService;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;
use Niyotail\Helpers\Image;
use Niyotail\Http\Requests\Admin\Collection\ViewRequest;
use Niyotail\Http\Requests\Admin\Collection\CreateRequest;
use Niyotail\Http\Requests\Admin\Collection\UpdateRequest;
use Niyotail\Http\Requests\Admin\Collection\SortRequest;
use Niyotail\Http\Requests\Admin\Collection\DeleteRequest;

class CollectionController extends Controller
{
    public function index(ViewRequest $request)
    {
        $collections = Collection::getAllWithParent();
        $collectionTree=buildTree($collections->toArray());
        return view('admin.collection.index', compact('collectionTree'));
    }

    public function detail($id)
    {
        $collection = Collection::find($id);
        $collection->icon = Image::getSrc($collection, 100);
        abort_if(empty($collection), 404);
        return $collection;
    }


    public function store(CreateRequest $request, CollectionService $collectionService)
    {
        $collectionService->setAuthority(CollectionService::AUTHORITY_ADMIN);
        $collection = $collectionService->create($request);
        return response()->json($collection);
    }

    public function update(UpdateRequest $request, CollectionService $collectionService)
    {
        $collectionService->setAuthority(CollectionService::AUTHORITY_ADMIN);
        $collection = $collectionService->update($request);
        return response()->json($collection);
    }

    public function sort(SortRequest $request, CollectionService $collectionService)
    {
        $collectionService->setAuthority(CollectionService::AUTHORITY_ADMIN);
        $collection = $collectionService->sort($request);
        return response()->json($collection);
    }

    public function delete(DeleteRequest $request, CollectionService $collectionService, $id)
    {
        $collectionService->setAuthority(CollectionService::AUTHORITY_ADMIN);
        $collection = $collectionService->delete($id);
        return response()->json($collection);
    }
}
