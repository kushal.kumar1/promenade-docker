<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\AttributeGrid;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\AttributeService;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;

/**
 * Class AttributeController
 * @package Niyotail\Http\Controllers\Admin
 */
class AttributeController extends Controller
{

    /**
     * @param Requests\Admin\Attributes\ViewRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Requests\Admin\Attributes\ViewRequest $request)
    {
        $grid = AttributeGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.attribute.index', [
            'grid' => $grid
        ]);
    }

    /**
     * @param Requests\Admin\Attributes\CreateRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Requests\Admin\Attributes\CreateRequest $request, AttributeService $attributeService)
    {
        $attribute = $attributeService->create($request);
        return response()->json($attribute);
    }


    /**
     * @param Requests\Admin\Attributes\UpdateRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Requests\Admin\Attributes\UpdateRequest $request, AttributeService $attributeService)
    {
        $attribute = $attributeService->update($request);
        return response()->json($attribute);
    }

    /**
     * @param Requests\Admin\Attributes\DeleteRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Requests\Admin\Attributes\DeleteRequest $request, AttributeService $attributeService)
    {
        $attributeService->delete($request);
    }
}
