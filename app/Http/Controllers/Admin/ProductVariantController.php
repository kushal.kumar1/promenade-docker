<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\ProductGrid;
use Niyotail\Grids\ProductVariantGrid;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Product;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Storage;
use Niyotail\Models\Warehouse;
use Niyotail\Services\ProductVariantService;
use Niyotail\Http\Requests\Admin\ProductVariant\CreateRequest;
use Niyotail\Http\Requests\Admin\ProductVariant\UpdateRequest;
use Niyotail\Http\Requests\Admin\ProductVariant\DeleteRequest;
use Niyotail\Http\Requests\Admin\ProductVariant\RestoreRequest;
use Illuminate\Http\Request;
use Niyotail\Http\Requests\Admin\ProductVariant\ViewRequest;

class ProductVariantController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ProductVariantGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.variant.index', [
            'grid' => $grid
        ]);
    }

    public function create($productId)
    {
        $product = Product::with('images', 'variants')->where('id', $productId)->first();
        $types = ProductVariant::getConstants('type');
        $measureUnits = ProductVariant::getConstants('uom');
        return view('admin.variant.create', compact('product', 'types', 'measureUnits'));
    }

    public function edit($productId, $variantId)
    {
        $product = Product::where('id', $productId)->with('images', 'variants')->first();
        $types = ProductVariant::getConstants('type');
        $measureUnits = ProductVariant::getConstants('uom');
        $variant = ProductVariant::where('product_id', $productId)
            ->where('id', $variantId)
            ->first();

        return view('admin.variant.edit', compact('product', 'variant', 'types', 'measureUnits'));
    }

    public function store($productId, CreateRequest $request, ProductVariantService $variantService)
    {
        $variant = $variantService->create($productId, $request);
        return response()->json($variant);
    }

    public function update(UpdateRequest $request, ProductVariantService $variantService)
    {
        $variant = $variantService->update($request);
        return response()->json($variant);
    }

    public function delete(DeleteRequest $request, ProductVariantService $variantService)
    {
        $variant = $variantService->delete($request->id);
        return response()->json($variant);
    }

    public function restore(RestoreRequest $request, ProductVariantService $variantService)
    {
        $variant = $variantService->restore($request->id);
        return response()->json($variant);
    }

    public function getVariantDetails(Request $request)
    {
        $id = $request->get('id');
        return ProductVariant::with('product.taxes', 'product.allVariants')->find($id);
    }
}
