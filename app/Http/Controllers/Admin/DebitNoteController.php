<?php


namespace Niyotail\Http\Controllers\Admin;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Niyotail\Grids\DebitNoteGrid;
use Niyotail\Helpers\Utils;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceDebitNote;
use Niyotail\Services\DebitNoteService;

class DebitNoteController extends Controller
{
    protected $debitNoteService;

    public function __construct()
    {
        $this->debitNoteService = new DebitNoteService();
    }

    public function index(Request $request)
    {
        $grid = DebitNoteGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.purchase_invoice.debit_note.index', [
            'grid' => $grid
        ]);
    }

    public function createDebitNote($invoiceId, Request $request)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoice), 404);
        $generatedDate = $request->has('debit_note_date') ? $request->debit_note_date : Carbon::now()->format('Y-m-d H:i:s');
        return $this->debitNoteService->generateDebitNote($invoice, $generatedDate);
    }

    public function generatePdf($id)
    {
        $note = PurchaseInvoiceDebitNote::find($id);
        abort_if(empty($note), 404);
        $pdf = $this->debitNoteService->generatePdf($note);
        return $pdf->download('debit_note-' . $note->id . '.pdf');
    }
}