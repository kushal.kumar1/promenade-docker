<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Beat\CreateRequest;
use Niyotail\Http\Requests\Admin\Beat\UpdateRequest;
use Niyotail\Services\BeatService;
use Illuminate\Http\Request;
use Niyotail\Models\Beat;
use Niyotail\Grids\BeatGrid;
use Niyotail\Models\Brand;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Requests\Admin\Beat\ViewRequest;

class BeatController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = BeatGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.beat.index', compact('grid'));
    }

    public function create(ViewRequest $request)
    {
        return view('admin.beat.create');
    }

    public function edit($id)
    {
        $beat = Beat::find($id);
        return view('admin.beat.edit', compact('beat'));
    }

    public function store(CreateRequest $request, BeatService $service)
    {
        $beat = $service->create($request);
        return response()->json($beat);
    }

    public function update(UpdateRequest $request, BeatService $service)
    {
        $beat = $service->update($request);
        return response()->json($beat);
    }
}
