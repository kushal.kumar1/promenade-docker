<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Grids\ProductGrid;
use Niyotail\Helpers\Integration\ProductSync;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\Product\CreateRequest;
use Niyotail\Http\Requests\Admin\Product\Image\CreateRequest as ImageCreateRequest;
use Niyotail\Http\Requests\Admin\Product\UpdateRequest;
use Niyotail\Http\Requests\Admin\Product\ViewRequest;
use Niyotail\Models\Brand;
use Niyotail\Models\Collection;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Product;
use Niyotail\Models\TaxClass;
use Niyotail\Models\Warehouse;
use Niyotail\Services\ProductImageService;
use Niyotail\Services\ProductService;

class ProductController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = ProductGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.product.index', [
            'grid' => $grid
        ]);
    }

    public function genrateBarcode(ProductService $productService, $id)
    {
        $product = Product::find($id);
        /* @var Product $product */
        $product = $productService->assignNewBarcode($product);
        return $product;
    }

    public function create()
    {
        $collections = buildTree(Collection::getAllWithParent()->toArray());
        $collections = $this->makeCollectionJson($collections, []);
        $brands = buildTree(Brand::getAllWithParent()->toArray());
        $brands = $this->makeBrandJson($brands, null);
        $taxClasses = TaxClass::active()->get();
        $measureUnits = Product::getConstants('uom');
        return view('admin.product.create', compact('collections', 'taxClasses', 'brands', 'measureUnits'));
    }

    private function makeCollectionJson($collections, $selectedCollections, $parent = null)
    {
        $branch = array();
        foreach ($collections as $collection) {
            $element = [];
            $element['id'] = $collection['id'];
            if (!empty($parent)) {
                $element['parent'] = $collection['id'];
            }
            $element['text'] = $collection['name'];
            $element['state']['opened'] = true;
            $element['state']['selected'] = in_array($collection['id'], $selectedCollections);
            if (!empty($collection['children'])) {
                $element['children'] = $this->makeCollectionJson($collection['children'], $selectedCollections, $collection['id']);
            }
            $branch[] = $element;
        }
        return $branch;
    }

    private function makeBrandJson($brands, $selectedBrand, $parent = null)
    {
        $branch = array();
        foreach ($brands as $brand) {
            $element = [];
            $element['id'] = $brand['id'];
            if (!empty($parent)) {
                $element['parent'] = $brand['id'];
            }
            $element['text'] = $brand['name'];
            $element['state']['opened'] = true;
            $element['state']['selected'] = ($brand['id'] == $selectedBrand);
            if (!empty($brand['children'])) {
                $element['children'] = $this->makeBrandJson($brand['children'], $selectedBrand, $brand['id']);
            }
            $branch[] = $element;
        }
        return $branch;
    }

    public function edit($id)
    {
        $warehouseHelper = app(MultiWarehouseHelper::class);
        $warehouse = Warehouse::where('id', $warehouseHelper->getSelectedWarehouse())->first();

        $product = Product::with('collections', 'brand', 'inventories.warehouse', 'tags', 'warehouseRacks', 'group')
            ->with(['variants' => function ($query) {
                $query->withTrashed();
            }])
            ->with(['images' => function ($query) {
                $query->orderBy('position', 'asc');
            }])->with(['orderItems' => function($orderItemQuery){
                $orderItemQuery->whereIn('status', [OrderItem::STATUS_PROCESSING,OrderItem::STATUS_MANIFESTED])
                    ->with('order');
            }])->inventoryCount([$warehouseHelper->getSelectedWarehouse()])
            ->blockInventoryCount($warehouseHelper->getSelectedWarehouse())
            ->find($id);

        abort_if(empty($product), 404);

        $taxClasses = TaxClass::active()->get();
        $measureUnits = Product::getConstants('uom');

        return view('admin.product.edit', compact('product', 'taxClasses', 'warehouse', 'measureUnits'));
    }

    public function store(CreateRequest $request, ProductService $productService)
    {
        $product = $productService->create($request);
        return response()->json($product);
    }

    public function update(UpdateRequest $request, ProductService $productService)
    {
        $product = $productService->update($request);
        return response()->json($product);
    }

    public function storeImages(ImageCreateRequest $request, ProductImageService $productImageService)
    {
        $productImages = $productImageService->create($request);
        return response()->json($productImages);
    }

    public function updateImages(Request $request, ProductImageService $productImageService)
    {
        $productImageService->update($request);
    }

    public function deleteImage(Request $request, ProductImageService $productImageService)
    {
        $productImageService->delete($request);
    }

    public function syncProduct(Request $request)
    {
        $product = Product::find($request->id);
        abort_if(empty($product), 404, 'Invalid Product');
        if (empty($product->status)) {
            return response(['message' => 'In active product cannot be synced!'], 422);
        }
        ProductSync::push($product);
        return response(['message' => 'Product sync in progress']);
    }

    public function syncProductPrice(Request $request)
    {
        $product = Product::find($request->id);
        abort_if(empty($product), 404, 'Invalid Product');
        if ($product->is1KMall()) {
            return response(['message' => '1K-Mall product prices cannot be synced!'], 422);
        }
        if (empty($product->status)) {
            return response(['message' => 'Inactive product prices cannot be synced!'], 422);
        }
        ProductSync::pushPrice($product);
        return response(['message' => 'Product sync price in progress']);
    }
}
