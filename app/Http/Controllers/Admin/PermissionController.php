<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\PermissionService;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function store(Request $request, PermissionService $permissionService)
    {
        $permission = $permissionService->create($request);
        return response()->json($permission);
    }

    public function update(Request $request, PermissionService $permissionService)
    {
        $permission = $permissionService->update($request);
        return response()->json($permission);
    }

    public function delete(Request $request, PermissionService $permissionService)
    {
        $permission = $permissionService->delete($request);
        return response()->json($permission);
    }
}
