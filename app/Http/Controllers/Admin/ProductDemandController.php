<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Grids\ProductDemandGrid;
use Niyotail\Grids\ProductDemandItemsGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\ProductDemandItem;
use Niyotail\Models\Warehouse;
use Niyotail\Services\ProductDemandService;

class ProductDemandController extends Controller
{
    protected $productDemandService;

    public function __construct()
    {
        $this->productDemandService = new ProductDemandService();
    }

    public function index(Request $request)
    {
        $grid = ProductDemandGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.product_demand.index', [
            'grid' => $grid
        ]);
    }

    public function demandForm($id = null, Request $request)
    {
        $demand = null;
        $grid = null;
        if (!empty($id)) {
            $demand = ProductDemand::findOrFail($id);
            $grid = ProductDemandItemsGrid::get(['demand_id' => $demand->id]);
            if ($request->ajax()) {
                return $grid;
            }
        }
        $userWarehouses = auth()->user()->warehouses;
        $userWarehouses = $userWarehouses->whereNotIn('type', [Warehouse::TYPE_VIRTUAL]);
        return view('admin.product_demand.details', compact('demand', 'grid','userWarehouses'));
    }

    public function saveDemand(Request $request, $id = null)
    {
        $this->validate($request, [
            'channel' => 'required|string',
            'sale_days' => 'required|integer',
            'inventory_days' => 'required|integer|min:1',
            'reorder_days' => 'required|integer|min:1',
        ]);

        try {
            return $this->productDemandService->saveDemand($request, $id);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function saveDemandItems($id, Request $request)
    {
        $demand = ProductDemand::findOrFail($id);

        $this->validate($request, [
            'items.*' => 'required'
        ]);

        $finalizeDemand = $request->has('finalize_demand') && $request->input('finalize_demand');

        $this->productDemandService->updateDemandItems($demand, $request->input('items'), $finalizeDemand);
    }

    public function previewDemandOrderItems($id)
    {
        $demand = ProductDemand::findOrFail($id);
        $items = $demand->items;
        $items->loadMissing('group', 'group.brand', 'group.brand.marketer',
            'group.brand.marketer.vendorMarketerMaps', 'group.brand.marketer.vendorMarketerMaps.vendor');

        $formattedItems = $this->productDemandService->getFormattedItemsForPreview($items);

        return view('admin.product_demand.po_items_preview', compact('demand', 'formattedItems'));
    }

    public function updateVendorAssignment($id, Request $request) {
        $this->validate($request, [
            'item' => 'required|array'
        ]);

        $demand = ProductDemand::findOrFail($id);

        $items = $request->input('item');

        foreach ($items as $item) {
            ProductDemandItem::ID($item['id'])->update(['vendor_id' => $item['vendor_id']]);
        }

        if($request->has('finalize_demand') && $request->input('finalize_demand') == "1") {
            $demand->status = ProductDemand::VENDOR_ASSIGNED;
            $demand->save();
        }

        return redirect(route('admin::product-demands.preview.po-items', $id));
    }

    public function previewDemandOrders($id)
    {
        $demand = ProductDemand::findOrFail($id);
        $items = $demand->items->where('vendor_id', '!=', null);
        $items->loadMissing('group', 'group.brand', 'group.brand.marketer',
            'group.brand.marketer.vendorMarketerMaps', 'group.brand.marketer.vendorMarketerMaps.vendor');

        $formattedItems = $this->productDemandService->getFormattedItemsForPO($items);

        return view('admin.product_demand.po_preview', compact('demand', 'formattedItems'));
    }

    public function createPOFromDemand($id, Request $request) {
        $demand = ProductDemand::findOrFail($id);
        return $this->productDemandService->createPurchaseOrderFromDemand($demand);
    }
}
