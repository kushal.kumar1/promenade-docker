<?php


namespace Niyotail\Http\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Niyotail\Grids\RateCreditNoteGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\RateCreditNote;
use Niyotail\Services\RateCreditNoteService;

class RateCreditNoteController extends Controller
{
    public function index(Request $request) {
        $grid = RateCreditNoteGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.rate_credit_note.index', [
            'grid' => $grid,
            'title' => 'Rate Credit Notes'
        ]);
    }

    public function createTransaction($id) {
        $creditNote = RateCreditNote::findOrFail($id);
        $service = new RateCreditNoteService();
        $service->createTransaction($creditNote);
    }

    public function downloadCreditNote($id) {
        $creditNote = RateCreditNote::findOrFail($id);
        $name = 'dcn_'.$creditNote->id.".pdf";
        $path = "discount_credit_note/".$name;
        return Storage::disk('s3')->download($path);
    }
}