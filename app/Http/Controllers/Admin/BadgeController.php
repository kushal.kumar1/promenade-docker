<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\BadgeGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\BadgeService;
use Illuminate\Http\Request;
use Niyotail\Http\Requests;

/**
 * Class BadgeController
 * @package Niyotail\Http\Controllers\Admin
 */
class BadgeController extends Controller
{

    /**
     * @param Requests\Admin\Badges\ViewRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Requests\Admin\Badges\ViewRequest $request)
    {
        $grid = BadgeGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.badge.index', [
            'grid' => $grid
        ]);
    }

    /**
     * @param Requests\Admin\Badges\CreateRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Requests\Admin\Badges\CreateRequest $request, BadgeService $badgeService)
    {
        $badge = $badgeService->create($request);
        return response()->json($badge);
    }

    /**
     * @param Requests\Admin\Badges\UpdateRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Requests\Admin\Badges\UpdateRequest $request, BadgeService $badgeService)
    {
        $badge = $badgeService->update($request);
        return response()->json($badge);
    }

    /**
     * @param Requests\Admin\Badges\DeleteRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function delete(Requests\Admin\Badges\DeleteRequest $request, BadgeService $badgeService)
    {
        $badgeService = $badgeService->delete($request);
    }
}
