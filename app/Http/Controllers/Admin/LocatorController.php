<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Store;

class LocatorController extends Controller
{
    public function index()
    {
        return view('admin.locator.index');
    }

    public function stores(Request $request)
    {
        $term = $request->term;
        $query = Store::active()->where(function ($subQuery) use ($term) {
            $subQuery->where('name', 'like', "%$term%")
                ->orWhere('address', 'like', "%$term%");
        });

        if (!empty($request->latitude) && !empty($request->longitude)) {
            $distanceQuery = "111.111 * DEGREES(ACOS(LEAST(COS(RADIANS(lat)) * COS(RADIANS(?)) * COS(RADIANS(lng - ?)) + SIN(RADIANS(lat)) * SIN(RADIANS(?)), 1.0)))";
            $query->selectRaw("*, $distanceQuery as distance", [$request->latitude, $request->longitude, $request->latitude]);
            $query->orderByRaw('-distance desc');
        }

        $stores = $query->take(20)->get();

        return view('admin.locator._stores', compact('stores'));
    }
}
