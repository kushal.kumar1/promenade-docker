<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Models\DashboardImage;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\AppBannerService;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\DashboardImageGrid;
use Niyotail\Http\Requests\Admin\DashboardImage\CreateRequest;
use Niyotail\Http\Requests\Admin\DashboardImage\UpdateRequest;
use Niyotail\Http\Requests\Admin\DashboardImage\ViewRequest;

class DashboardImageController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = DashboardImageGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.dashboard_image.index', compact('grid'));
    }

    public function edit(ViewRequest $request, $id)
    {
        $dashboardImage = DashboardImage::find($id);
        abort_if(empty($dashboardImage), 404);
        return view('admin.dashboard_image.edit', compact('dashboardImage'));
    }

    public function create()
    {
        return view('admin.dashboard_image.create');
    }
    public function store(CreateRequest $request, AppBannerService $appBannerService)
    {
        $appBanner = $appBannerService->createAgentDashboardImage($request);
        return response()->json($appBanner);
    }

    public function update(UpdateRequest $request, AppBannerService $appBannerService)
    {
        $appBanner = $appBannerService->updateAgentDashboardImage($request);
        return response()->json($appBanner);
    }
}
