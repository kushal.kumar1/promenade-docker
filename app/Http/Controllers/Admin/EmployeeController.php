<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Grids\EmployeeGrid;
use Niyotail\Helpers\Response\AdminResponse;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Models\Warehouse;
use Niyotail\Services\EmployeeService;
use Illuminate\Http\Request;
use Niyotail\Models\Employee;
use Niyotail\Models\Role;
use Niyotail\Http\Requests\Admin\Employees;
use Illuminate\Support\Facades\Auth;

class EmployeeController extends Controller
{
    public function index(Employees\ViewRequest $request)
    {
        $grid = EmployeeGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.employee.index', [
            'grid' => $grid
        ]);
    }

    public function profile()
    {
        $employee=Auth::guard('admin')->user();
        return view('admin.employee.profile', compact('employee'));
    }

    public function edit($id)
    {
        $employee = Employee::with('roles', 'warehouses')->find($id);
        $roles = Role::get();
        abort_if(empty($employee), 404);
        return view('admin.employee.edit', [
            'employee' => $employee,
            'roles' => $roles
        ]);
    }

    public function store(Employees\CreateRequest $request, EmployeeService $employeeService)
    {
        $employeeService->setAuthority(EmployeeService::AUTHORITY_ADMIN);
        $employee = $employeeService->create($request);
        return response()->json($employee);
    }

    public function update(Employees\UpdateRequest $request, EmployeeService $employeeService)
    {
        $employeeService->setAuthority(EmployeeService::AUTHORITY_ADMIN);
        $employee = $employeeService->update($request);
        return response()->json($employee);
    }

    public function updateProfile(Employees\UpdateProfileRequest $request, EmployeeService $employeeService)
    {
        $employee=Auth::guard('admin')->user();
        $employeeService->setAuthority(EmployeeService::AUTHORITY_ADMIN);
        $employee = $employeeService->updateProfile($employee, $request);
        return response()->json($employee);
    }
}
