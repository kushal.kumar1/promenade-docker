<?php


namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Niyotail\Grids\PurchaseInvoiceCartGrid;
use Niyotail\Helpers\File;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\Cart\CreatePICartRequest;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\PurchaseInvoiceCartItems;
use Niyotail\Models\PurchasePlatform;
use Niyotail\Models\TaxClass;
use Niyotail\Models\Warehouse;
use Niyotail\Models\WarehouseRack;
use Niyotail\Services\PurchaseInvoiceCartItemService;
use Niyotail\Services\PurchaseInvoiceCartService;

class PurchaseInvoiceCartController extends Controller
{
    protected $purchaseInvoiceCartService;
    protected $purchaseInvoiceCartItemService;

    public function __construct()
    {
        $this->purchaseInvoiceCartService = new PurchaseInvoiceCartService();
        $this->purchaseInvoiceCartItemService = new PurchaseInvoiceCartItemService();
    }

    public function index(Request $request)
    {
        $grid = PurchaseInvoiceCartGrid::get();
        if ($request->ajax()) {
            return $grid;
        }

        return view('admin.purchase_invoice.cart.list', compact('grid'));
    }

    public function createCart(Request $request)
    {
        $vendorId = $request->input('vendor_id');
        return $this->purchaseInvoiceCartService->createVendorCart($vendorId);
    }

    public function manageCart(Request $request, $id)
    {
        $cart = PurchaseInvoiceCart::with('warehouse')->findOrFail($id);
        abort_if(empty($cart), 404);

        $cartId = $cart->id;
        $productRequest = ProductRequest::where('source', 'purchase_invoice_cart')
            ->where('source_id', $cartId)
            ->get();

        $warehouseRacks = null;
        if (!empty($cart->warehouse_id)) {
            $warehouseRacks = WarehouseRack::all();
        }

        $vendor_addresses = $cart->vendor->addresses;
        $currentWarehouses = app(MultiWarehouseHelper::class)->getCurrentWarehouses();
        $warehouses = Warehouse::active()->whereIn('id', $currentWarehouses)->get();

        $itemFields = (new PurchaseInvoiceCartItems())->getSortedFields();

        $taxClasses = TaxClass::active()->get();

        $measureUnits = ProductVariant::getConstants('uom');

        $purchasePlatforms = PurchasePlatform::purchase()->get();

        return view('admin.purchase_invoice.cart.index',
            compact('cart', 'itemFields', 'vendor_addresses',
                'warehouses', 'taxClasses', 'productRequest', 'measureUnits', 'warehouseRacks', 'purchasePlatforms'));
    }

    public function updateCart($cartId, CreatePICartRequest $request)
    {
        $isDirectPurchase = $request->is_direct_store_purchase;
        if ($isDirectPurchase) {
            if (empty($request->store_id)) {
                throw new \Exception('Store Id is required with direct purchase');
            }
        }
        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);

        return $this->purchaseInvoiceCartService->updatePurchaseInvoiceCart($request, $cart);
    }

    public function itemSuggestions($cartId, Request $request)
    {
        $cart = PurchaseInvoiceCart::findOrFail($cartId);
        if ($cart->type == PurchaseInvoiceCart::TYPE_STOCK_TRANSFER) {
            return (new SearchHelper())->searchFromSourceShipment($request->term, $cart);
        } else {
            return (new SearchHelper())->suggestItemsInPurchaseInvoiceCart($request->term, $cart);
        }
    }

    public function addItem($cartId, Request $request)
    {
        $this->validate($request, [
            'manufacturing_date' => 'required|date',
            'expiry_date' => 'required|date|after:manufacturing_date'
        ]);

        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);
        return $this->purchaseInvoiceCartItemService->addItem($request, $cart);
    }

    public function getCartItemsView($cartId)
    {
        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);
        return view('admin.purchase_invoice.cart._items', compact('cart'));
    }

    public function updateItem(Request $request)
    {
        $cartId = $request->cart_id;
        $itemId = $request->item_id;
        $quantity = $request->quantity;

        if (!$cartId || !$itemId || $quantity < 1) {
            throw new \Exception('Please provide valid cart item and quantity.');
        }

        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);
        $this->purchaseInvoiceCartItemService->updateItemQuantity($itemId, $quantity, $cart);

        return view('admin.purchase_invoice.cart._items', compact('cart'));
    }

    public function removeItem(Request $request)
    {
        $cartId = $request->cart_id;
        $itemId = $request->item_id;

        if (!$cartId || !$itemId) {
            throw new \Exception('Please provide valid cart and item id');
        }

        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);

        $this->purchaseInvoiceCartItemService->removeItem($itemId, $cart);

        return view('admin.purchase_invoice.cart._items', compact('cart'));
    }

    public function createInvoice($cartId, Request $request)
    {
        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);
        $this->validate($request, [
            'eway_number' => Rule::requiredIf($request->total >= 50000)
        ]);
        $ewayNumber = $request->input('eway_number', null);
        $deliveryDate = $request->input('delivery_date', null);
        $tcs = $request->input('tcs', null);
        if (!empty($tcs)) {
            $cart->tcs = $tcs;
            $cart->save();
        }
        return $this->purchaseInvoiceCartService->createInvoiceFromCart($cart, $deliveryDate, $ewayNumber, $tcs);
    }

    public function getCartItemById($cartId, $id, Request $request)
    {
        return PurchaseInvoiceCartItems::with(['productVariant', 'productRequest'])->find($id);
    }

    public function uploadCopy($cartId, Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file'
        ]);

        $cart = PurchaseInvoiceCart::find($cartId);
        abort_if(empty($cart), 404);

        $image = $request->file('file');
        $targetDirectory = storage_path('app/purchased_invoice/' . $cartId);
        $file = new File($image);
        $file->setName(uniqid() . "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $cart->scanned_file = $file->getName();
        $cart->save();
        return $cart;
    }

    public function getScannedCopy($cartId)
    {
        try {
            $cart = PurchaseInvoiceCart::find($cartId);
            abort_if(empty($cart), 404);

            $file = $cart->scanned_file;
            abort_if(empty($file), 404);
            return Storage::download('purchased_invoice/' . $cart->id . '/' . $file);
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function createShortProduct(Request $request)
    {
        $this->validate($request, [
            'source_id' => 'required',
            'name' => 'required',
            'quantity' => 'required|integer|min:1',
            'effective_cost' => 'required|min:1'
        ]);

        $cartId = $request->get('source_id');

        return $this->purchaseInvoiceCartService->createShortProduct($cartId, $request);
    }

    public function removeShortProduct(Request $request)
    {
        $this->validate($request, [
            'cart_id' => 'required',
            'item_id' => 'required',
        ]);
        $this->purchaseInvoiceCartService->removeShortProduct($request);
    }

    public function deleteCartById($cartId)
    {
        $this->purchaseInvoiceCartService->removeInvoiceCart($cartId);
    }
}