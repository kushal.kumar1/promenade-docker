<?php

namespace Niyotail\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Niyotail\Grids\Orders\StoreOrderGrid;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Http\Requests\Admin\StoreOrder\DetailsRequest;
use Niyotail\Models\Cart;
use Niyotail\Models\Order;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\StoreOrder\StoreOrderService;


class StoreOrderController extends Controller
{
    public function index(Request $request)
    {
        $grid = StoreOrderGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        return view('admin.store-order.index', [
            'grid' => $grid,
            'title' => 'Store Orders'
        ]);
    }

    public function detail($id, DetailsRequest $request)
    {
        $storeOrder = StoreOrder::with('store', 'items.product.images', 'createdBy')
            ->with('items.product.images')
            ->findOrFail($id);

        $posOrderId = null;
        $stockTransferType = null;
        $additionalInfo = json_decode($storeOrder->additional_info, true);
        if (!empty($additionalInfo)) {
            $posOrderId = array_key_exists('pos_order_id', $additionalInfo['data']) ? $additionalInfo['data']['pos_order_id'] : null;
            $stockTransferType = array_key_exists('st_type', $additionalInfo['data']) ? $additionalInfo['data']['st_type'] : null;
        }

        return view('admin.store-order.detail', compact('storeOrder', 'posOrderId', 'stockTransferType'));
    }

    public function store(Request $request, StoreOrderService $service)
    {
        $employee = Auth::guard('admin')->user();
        $cart = Cart::withoutStoreOrder()->byEmployee($employee)->where('store_id', $request->store_id)->with('items.product')->latest()->first();
        abort_if(empty($cart), 422);

        if (!empty($request->tag)) {
            $cart->tag = $request->tag;
        }

        $additionalInfo = $request->additional_info;
        if ($cart->tag == Order::TAG_1K_MALL && !empty($additionalInfo['pos_order_id'])) {
            $customerInfo = $service->getMallOrderCustomerInfo($additionalInfo['pos_order_id']);
            if (empty($customerInfo)) {
                throw new \Exception('Pos Order Reference ID is not valid.');
            }

            $additionalInfo = array_merge($additionalInfo, $customerInfo);
            $cart->additional_info = json_encode($additionalInfo);
        }
        $storeOrder = $service->create($cart);
        return response()->json($storeOrder);
    }

    /**
     *
     * This works only with StoreExternalPurchases failed due to Credit limit!
     * @return void
     * @throws \Niyotail\Exceptions\ServiceException
     */
    public function retryUnFullFilledOrder($storeOrderId, StoreOrderService $service)
    {
        $storeOrder = StoreOrder::where(['source' => StoreOrder::SOURCE_STORE_PURCHASE, 'status' => StoreOrder::STATUS_UNFULFILLED])
            ->findOrFail($storeOrderId);
        $service->retryUnFulfilledOrder($storeOrder);
    }
}
