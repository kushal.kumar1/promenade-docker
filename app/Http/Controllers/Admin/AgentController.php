<?php

namespace Niyotail\Http\Controllers\Admin;

use Niyotail\Models\Agent;
use Illuminate\Http\Request;
use Niyotail\Http\Controllers\Controller;
use Niyotail\Services\AgentService;
use Niyotail\Grids\AgentGrid;
use Niyotail\Http\Requests\Admin\Agent\ViewRequest;
use Niyotail\Http\Requests\Admin\Agent\CreateRequest;
use Niyotail\Http\Requests\Admin\Agent\UpdateRequest;
use Niyotail\Http\Requests\Admin\Agent\UpdateBeatMapRequest;
use Niyotail\Http\Requests\Admin\Agent\DeleteBeatMapRequest;

class AgentController extends Controller
{
    public function index(ViewRequest $request)
    {
        $grid = AgentGrid::get();
        if ($request->ajax()) {
            return $grid;
        }
        $agentTypes = Agent::getEnum('type');
        return view('admin.agent.index', [
            'grid' => $grid,
            'agentTypes' => $agentTypes
        ]);
    }

    public function profile(ViewRequest $request, $id)
    {
        $agent = Agent::with('beatMarketers')->find($id);
        abort_if(empty($agent), 404);
        $agentTypes = Agent::getEnum('type');
        return view('admin.agent.profile', compact('agent', 'agentTypes'));
    }

    public function edit($id)
    {
        $agent = Agent::find($id);
        abort_if(empty($agent), 404);

        return view('admin.agent.form', compact('agent'));
    }

    public function store(CreateRequest $request, AgentService $agentService)
    {
        $agent = $agentService->create($request);
        return response()->json($agent);
    }

    public function update(UpdateRequest $request, AgentService $agentService)
    {
        $agent = Agent::find($request->id);
        $agent = $agentService->setAuthority(AgentService::AUTHORITY_ADMIN)->update($request, $agent);
        return response()->json($agent);
    }

    public function updateBeatMap(UpdateBeatMapRequest $request, AgentService $agentService)
    {
        $agent = $agentService->setAuthority(AgentService::AUTHORITY_ADMIN)->updateBeatMap($request);
        return response()->json($agent);
    }

    public function deleteBeatMap(DeleteBeatMapRequest $request, AgentService $agentService)
    {
        $agent = $agentService->setAuthority(AgentService::AUTHORITY_ADMIN)->deleteBeatMap($request);
        return response()->json($agent);
    }


    public function updateStatus(Request $request, AgentService $agentService)
    {
        $agent = Agent::find($request->id);
        $agent = $agentService->setAuthority(AgentService::AUTHORITY_ADMIN)->saveStatus($request, $agent);
        return response()->json($agent);
    }
}
