<?php
use Niyotail\Helpers\Log\Logger;

if (!function_exists('getDomain')) {
    /**
     * Return App Domain.
     *
     * @return string
     *
     * @author Ankit
     */
    function getDomain()
    {
        return substr(strstr(request()->root(), '.', 0), 1);
    }
}

if (!function_exists('generateOtp')) {
    /**
     * @return int
     *
     * @author Ankit
     */
    function generateOtp()
    {
        return rand(100000, 999999);
    }
}


if (!function_exists('getPassword')) {
    /**
     * @param string $salt
     *
     * @return int
     * @author Ankit
     */
    function getPassword($salt = '')
    {
        return str_random(2) . $salt . str_random(3);
    }
}

if (!function_exists('mysqlDateTime')) {
    function mysqlDateTime($dateTime)
    {
        return date('Y-m-d H:i:s', strtotime($dateTime));
    }
}

if (!function_exists('inclusiveTaxAmount')) {
    function inclusiveTaxAmount($amount, $rate, $additionalTax)
    {
        $amountWithoutTax = ($amount - $additionalTax) / (1 + ($rate / 100));
        return $amount - $amountWithoutTax;
    }
}

if (!function_exists('exclusiveTaxAmount')) {
    function exclusiveTaxAmount($amount, $rate)
    {
        return ($amount * $rate) / 100;
    }
}


if (!function_exists('getProRataValue')) {
    function getProRataValue($totalValue, $value, $valueForProRata)
    {
        return (($value / $totalValue) * $valueForProRata);
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($salt)
    {
        return time() . rand(100, 999) . $salt;
    }
}

if (!function_exists('buildTree')) {
    function buildTree(array $elements, $parent = null, $parentPath = null)
    {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parent) {
                $path = empty($parentPath) ? $element['slug'] : $parentPath . "/" . $element['slug'];
                // $element['url'] = route('web::listing', ['collection' => $path]);
                $children = buildTree($elements, $element['id'], $path);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }
}

if (!function_exists('calculateMargin')) {
    function calculateMargin($price, $mrp)
    {
        if ($mrp == 0) {
            return 100.00;
        }
        return round(abs(($mrp-$price)/$mrp*100), 2);
    }
}

if (!function_exists('splitAmount')) {
    function splitAmount($total, $divisor)
    {
        $arrayCount = ceil($divisor);

        if ($total == 0) {
            return array_fill(1, $arrayCount, 0.00);
        }

        $newTotal = 0; //$splitTotals[1] * $divisor;
        $splitTotals = array_fill(1, $arrayCount, round($total / $divisor, 2));
        $splitTotals[$arrayCount] = round(($total * fmod($divisor, 1) / $divisor), 2);
        for ($i=1; $i <= $arrayCount; $i++) {
            $newTotal += $splitTotals[$i];
        }

        $difference = abs(round($newTotal - $total, 2));
        $index = $arrayCount;
        while ($difference > 0) {
            $index = $index > 0 ? $index : $arrayCount;
            $difference -= 0.01;
            $splitTotals[$index--] += $newTotal < $total ? 0.01 : -0.01;
        }
        return $splitTotals;
    }
}

if (!function_exists('split_name')) {
    function split_name($name)
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#'.$last_name.'#', '', $name));
        return array($first_name, $last_name);
    }
}

if (!function_exists('niyo_logger')) {
    function niyo_logger()
    {
        return app(Logger::class);
    }
}
