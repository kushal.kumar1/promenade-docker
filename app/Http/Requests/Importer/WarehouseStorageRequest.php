<?php

namespace Niyotail\Http\Requests\Importer;

use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Niyotail\Models\Storage;

class WarehouseStorageRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'warehouse_id' => 'required|exists:warehouses,id',
            'zone'=> ['required', $this->validateStorageAttribute()],
            'category'=> ['required', $this->validateStorageAttribute()],
            'label' =>  'required'
        ];
    }

    private function validateStorageAttribute(): Closure
    {
        return function ($attribute, $value, $fail) {
            $attributeValues = ($attribute == 'zone') ? Storage::getConstants('ZONE') : Storage::getConstants('CATEGORY');
            if(!in_array(strtolower($value), $attributeValues)){
                $fail('The selected '.$attribute.' is invalid.');
            }
        };
    }
}
