<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Cl4CostMapRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'cl4_id' => 'required|exists:newcat_l4,id',
            'marketer_level_id' => 'required|exists:marketer_levels,id',
            'commission_tag_id' => 'required|exists:commission_tags,id',
            'price_basis' => ['nullable', Rule::in(['cost', 'mrp'])],
            'latest_cost_basis' => ['nullable', Rule::in(['group', 'product'])],
            'mrp_basis' => ['nullable', Rule::in(['fixed', 'dynamic'])],
        ];
    }
}
