<?php

namespace Niyotail\Http\Requests\Importer\Cart;

use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Store;

class WholesaleCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('create-wholesale-order');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'exists:stores,id', $this->isStoreActive()],
            'quantity' => 'required|integer|min:1',
            'sku' => 'required|exists:product_variants,sku',
            'price' => 'required',
            'warehouse_id' => 'required',
            'tsm_number' => 'required'
        ];
    }

    private function isStoreActive(): Closure
    {
        return function ($attribute, $value, $fail) {
            $store = Store::with('tags')->find($value);
            if (!empty($store)) {
                if ($store->status != 1) {
                    $fail('Store is not active!');
                }
                if ($store->verified == 0) {
                    $fail('Store is not verified');
                }
                if($store->tags->isNotEmpty() && $store->tags->where('name', 'Wholesale')->isEmpty()){
                    $fail('Wholesale tag not assigned to the store');
                }
            }
        };
    }
}
