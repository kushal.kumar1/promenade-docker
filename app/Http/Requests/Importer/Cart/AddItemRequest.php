<?php

namespace Niyotail\Http\Requests\Importer\Cart;

use Closure;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Product;
use Niyotail\Models\Store;

class AddItemRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'store_id' => ['required', 'exists:stores,id', $this->isStoreActive()],
            'quantity' => 'required|integer|min:1',
            'sku' => 'required|exists:product_variants,sku',
            'warehouse_id' => 'required'
        ];
    }

    private function isStoreActive(): Closure
    {
        return function ($attribute, $value, $fail) {
            $store = Store::with('tags')->find($value);
            if (!empty($store)) {
                if ($store->status != 1) {
                    $fail('Store is not active!');
                }
                if ($store->verified == 0) {
                    $fail('Store is not verified');
                }
                if($store->type == Store::TYPE_RETAIL) {
                    if ($store->tags->isNotEmpty() && $store->tags->where('name', '1K')->isEmpty()) {
                        $fail('1K tag not assigned to the store');
                    }
                }
            }
        };
    }
}
