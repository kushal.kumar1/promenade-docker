<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;

class NewCatL4Request extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'cl3_id' => 'required|exists:newcat_l3,id'
        ];
    }
}
