<?php

namespace Niyotail\Http\Requests\Importer\PurchaseInvoiceCart;

use Carbon\Carbon;
use Niyotail\Http\Requests\Request;

class PurchaseInvoiceCartRequest extends Request
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'vendor_id' => 'required',
            'vendor_ref_id' => 'required_if:is_local_purchase,0',
            'vendor_address_id' => 'required',
            'invoice_date' => 'required|before_or_equal:' . Carbon::now()->toDateString(),
            'sku' => 'required|exists:product_variants,sku',
            'quantity' => 'required',
            'received_quantity' => 'required',
            'is_direct_store_purchase' => 'required',
            'cost' => 'required',
            'store_id' => 'required_if:is_direct_store_purchase,1'
        ];
    }

    public function messages()
    {
        return [
            'store_id.required_if' => 'Store Id is required when the cart is direct store purchase'
        ];
    }
}