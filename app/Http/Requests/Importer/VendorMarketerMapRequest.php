<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Niyotail\Models\VendorMarketerMap;

class VendorMarketerMapRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'vendor_id' => 'required|exists:vendors,id',
            'marketer_id' => 'required|exists:marketers,id',
            'relation' => ['required', Rule::in(VendorMarketerMap::VALID_RELATIONS)],
            'channel' => ['required', Rule::in(VendorMarketerMap::VALID_CHANNELS)],
            'mov' => 'numeric|min:0',
            'lead_time' => 'numeric|min:0',
            'credit_term' => 'numeric|min:0',
            'cash_discount' => 'numeric|min:0',
            'tot_signed' => 'boolean',
            'priority' => 'numeric|min:1|max:5',
            'status' => 'boolean',
            'date_from' => 'required',
        ];
    }

}
