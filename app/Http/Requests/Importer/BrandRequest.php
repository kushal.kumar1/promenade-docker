<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|unique:brands,name',
            'marketer_id' => 'required|exists:marketers,id',
            'visible' => 'nullable|boolean',
            'status' => 'nullable|boolean'
        ];
    }
}
