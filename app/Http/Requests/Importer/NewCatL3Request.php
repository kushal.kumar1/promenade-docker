<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;

class NewCatL3Request extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'cl2_id' => 'required|exists:newcat_l2,id'
        ];
    }
}
