<?php

namespace Niyotail\Http\Requests\Importer;

use Closure;
use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Models\Store;

class InventoryMigrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'warehouse_id' => 'required|exists:warehouses,id',
            'product_id' => 'required|exists:products,id',
            'from_storage_id' => 'required|exists:storages,id',
            'quantity' => 'required|integer|min:1',
            'employee_id' => 'required|exists:employees,id'
        ];
    }
}
