<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;

class NewCatL1Request extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            'name' => 'required'
        ];
    }
}
