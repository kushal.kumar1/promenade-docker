<?php

namespace Niyotail\Http\Requests\Importer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Niyotail\Models\ProductDemand;

class ProductDemandRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'group_id' => 'required',
            'warehouse_id' => 'required',
            'reorder_quantity' => 'required|numeric',
            'channel' => ['required', Rule::in(array_values(ProductDemand::getConstants('CHANNEL')))],
            'cost_price' => 'required_with:cost_price_by',
            'product_id' => 'required_with:cost_price_by'
        ];
    }
}
