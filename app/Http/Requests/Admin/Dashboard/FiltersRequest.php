<?php

namespace Niyotail\Http\Requests\Admin\Dashboard;

use Niyotail\Http\Requests\Request;

class FiltersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => 'required|in:store_types,beats,brands,marketers,products,stores,collections',
            'store_type' => 'string|in:all,1k,retail',
            'beat_ids.*' => 'numeric',
            'brand_ids.*' => 'numeric',
            'marketer_ids.*' => 'numeric',
            'product_ids.*' => 'numeric',
            'store_ids.*' => 'numeric',
            'collection_ids.*' => 'numeric'
        ];
    }
}
