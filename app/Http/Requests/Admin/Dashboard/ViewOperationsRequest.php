<?php

namespace Niyotail\Http\Requests\Admin\Dashboard;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewOperationsRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-operations-dashboard');
    }

    public function rules()
    {
        return [

        ];
    }
}
