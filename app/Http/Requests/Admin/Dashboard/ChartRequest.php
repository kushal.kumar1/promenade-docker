<?php

namespace Niyotail\Http\Requests\Admin\Dashboard;

use Niyotail\Http\Requests\Request;

class ChartRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'start_date' => 'string',
            'end_date' => 'string'
        ];
    }
}
