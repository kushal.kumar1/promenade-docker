<?php

namespace Niyotail\Http\Requests\Admin\Dashboard;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewBusinessRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-business-dashboard');
    }

    public function rules()
    {
        return [

        ];
    }
}
