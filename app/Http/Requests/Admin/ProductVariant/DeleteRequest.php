<?php

namespace Niyotail\Http\Requests\Admin\ProductVariant;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class DeleteRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('delete-variant');
    }

    public function rules()
    {
        return [
            'id' => 'required',
        ];
    }
}
