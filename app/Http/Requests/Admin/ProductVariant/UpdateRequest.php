<?php

namespace Niyotail\Http\Requests\Admin\ProductVariant;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-variant');
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'value' => 'required',
            'sku' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'min_price' => 'required',
            'max_price' => 'required',
            // 'uom' => 'required',
            'quantity' => 'required'
        ];
    }
}
