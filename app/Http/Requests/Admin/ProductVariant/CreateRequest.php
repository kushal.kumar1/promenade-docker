<?php

namespace Niyotail\Http\Requests\Admin\ProductVariant;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-variant');
    }

    public function rules()
    {
        return [
            'value' => 'required',
            'sku' => 'required',
            'mrp' => 'required',
            'price' => 'required',
            'min_price' => 'required|lte:price',
            'max_price' => 'required|lte:mrp,gte:price',
            // 'uom' => 'required',
            'quantity' => 'required'
        ];
    }
}
