<?php

namespace Niyotail\Http\Requests\Admin\Vendor\Address;

use Niyotail\Http\Requests\Request;
// use Illuminate\Support\Facades\Gate;

class CreateAddressRequest extends Request
{
    public function authorize()
    {
        return true;
        // return Gate::allows('create-vendor-request');
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'pincode' => 'required|exists:pincodes,pincode',
            'city' => 'required',
            'state' => 'required',
        ];
    }
}
