<?php

namespace Niyotail\Http\Requests\Admin\Vendor;

use Niyotail\Http\Requests\Request;

class VendorAccountRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'number' => 'required',
            'confirm_number' => 'required|same:number'
        ];
    }
}
