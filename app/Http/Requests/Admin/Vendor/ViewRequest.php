<?php

namespace Niyotail\Http\Requests\Admin\Vendor;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return true;
        // return Gate::allows('view-vendor');
    }

    public function rules()
    {
        return [

        ];
    }
}
