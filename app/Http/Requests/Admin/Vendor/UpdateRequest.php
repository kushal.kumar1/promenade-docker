<?php

namespace Niyotail\Http\Requests\Admin\Vendor;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-vendor');
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'trade_name' => 'required',
            'legal_name' => 'required',
            'status' => 'required',
        ];
    }
}
