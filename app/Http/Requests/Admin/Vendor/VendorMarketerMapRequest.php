<?php


namespace Niyotail\Http\Requests\Admin\Vendor;


use Niyotail\Http\Requests\Request;

class VendorMarketerMapRequest extends Request
{
    public function authorize()
    {
        return true;
        // return Gate::allows('update-vendor');
    }

    public function rules()
    {
        return [
            'vendor_id' => 'required|exists:vendors,id',
            'marketer_id' => 'required|exists:marketers,id',
            'relation' => 'required|string',
            'channel' => 'required|string',
            'priority' => 'required|integer|min:1',
            'lead_time' => 'required|integer|min:0',
            'mov' => 'required|integer|min:0',
            'credit_term' => 'required|integer|min:0',
            'tot_signed' => 'required|boolean',
            'tot_file' => 'required_if:tot_signed,1',
            'cash_discount' => 'required|integer|min:0',
            'cash_discount_terms' => 'nullable|string',
            'status' => 'required|in:0,1'
        ];
    }
}