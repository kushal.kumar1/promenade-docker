<?php

namespace Niyotail\Http\Requests\Admin\CreditLimit;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-credit_limit');
    }

    public function rules()
    {
        return [
          'id' => 'required|exists:stores,id',
          'type' => 'required',
          'value' => 'required_if:type,manual|numeric|nullable'
        ];
    }
}
