<?php

namespace Niyotail\Http\Requests\Admin\DashboardImage;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-app_banner');
    }

    public function rules()
    {
        return [
            'status'=>'required',
            'valid_from'=>'date',
            'file'=>'required|dimensions:ratio=2'
        ];
    }
}
