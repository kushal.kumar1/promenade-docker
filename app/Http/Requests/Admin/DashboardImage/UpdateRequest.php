<?php

namespace Niyotail\Http\Requests\Admin\DashboardImage;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-app_banner');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'valid_from'=>'nullable|date',
            'status'=>'required',
            'file'=>'dimensions:ratio=2'
        ];
    }
}
