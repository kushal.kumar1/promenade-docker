<?php

namespace Niyotail\Http\Requests\Admin\Warehouses;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\Warehouse;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-warehouse');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'name' => 'required',
            'email'=>'email|nullable',
            'address' => 'required',
            'pincode' => 'required',
            'invoice_code' => 'required',
            'status' => 'required | boolean',
            'city_id'=> 'integer|exists:cities,id'
        ];
    }
}
