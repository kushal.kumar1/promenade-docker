<?php

namespace Niyotail\Http\Requests\Admin\Warehouses;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-warehouse');
    }

    public function rules()
    {
        return [

        ];
    }
}
