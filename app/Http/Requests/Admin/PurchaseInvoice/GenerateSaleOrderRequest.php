<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;


use Illuminate\Foundation\Http\FormRequest;

class GenerateSaleOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
//        return Gate::allows('generate-sale-order');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
