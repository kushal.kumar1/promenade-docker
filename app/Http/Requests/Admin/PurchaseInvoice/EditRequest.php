<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\PurchaseInvoice;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return MultiWarehouseHelper::isValidWarehouse(PurchaseInvoice::class, $this->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
