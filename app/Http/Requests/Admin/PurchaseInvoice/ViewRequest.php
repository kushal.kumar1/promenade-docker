<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-purchase-invoice');
    }

    public function rules()
    {
        return [

        ];
    }
}
