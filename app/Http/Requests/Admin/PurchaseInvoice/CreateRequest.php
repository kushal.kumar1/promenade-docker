<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Vendor;
use Niyotail\Models\Warehouse;

class CreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'reference_id' => 'required|string',
            'vendor_id' => 'required| exists:' . (new Vendor())->getTable() . ',id',
            'invoice_date' => 'date|nullable',
            'quantity' => 'required|numeric',
            'warehouse_id' => 'required | exists:' . (new Warehouse())->getTable() . ',id',
            'sku' => 'required | exists:' . (new ProductVariant())->getTable() . ',sku',
            'base_cost' => 'required|numeric',
            'trade_discount' => 'required|numeric',
            'scheme_discount' => 'required|numeric',
            'qps_discount' => 'required|numeric',
            'cash_discount' => 'required|numeric',
            'program_schemes' => 'required|numeric',
            'other_discount' => 'required|numeric',
            'post_tax_discount' => 'required|numeric',
            'reward_points_cashback' => 'required|numeric',
            'post_invoice_schemes' => 'required|numeric',
            'post_invoice_qps' => 'required|numeric',
            'post_invoice_claim' => 'required|numeric',
            'post_invoice_incentive' => 'required|numeric',
            'post_invoice_other' => 'required|numeric',
            'post_invoice_tot' => 'required|numeric',
            'expiry_date' => 'date|nullable',
            'manufacturing_date' => 'date|nullable',
        ];
    }
}
