<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('revert-invoice');
    }

    public function rules()
    {
        return [

        ];
    }
}
