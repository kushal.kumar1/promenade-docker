<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice;

use Niyotail\Http\Requests\Request;

class LableRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'received_quantity' => 'required',
            'qty' => 'required|lte:received_quantity'
        ];
    }
}
