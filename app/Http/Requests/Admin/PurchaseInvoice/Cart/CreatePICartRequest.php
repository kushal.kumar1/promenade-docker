<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoice\Cart;

use Carbon\Carbon;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Models\Warehouse;

class CreatePICartRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'vendor_ref_id' => 'required_if:is_local_purchase,0|string',
            'vendor_id' => 'required| exists:' . (new Vendor())->getTable() . ',id',
            'vendor_address_id' => 'required| exists:' . (new VendorsAddresses())->getTable() . ',id',
            'invoice_date' => 'required|date|before_or_equal:'.(Carbon::today()->format('Y-m-d')),
            'store_id' => 'required_if:is_direct_store_purchase,1'
        ];
    }
}
