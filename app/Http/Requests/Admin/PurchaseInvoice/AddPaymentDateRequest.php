<?php

namespace Niyotail\Http\Requests\admin\PurchaseInvoice;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class AddPaymentDateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (!Gate::allows('update-purchase-invoice'))
        {
            throw new \Exception('You shall not pass.');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:purchase_invoices,id',
            'payment_date' => ['required', 'date_format:Y-m-d', 'before_or_equal:' . now()->format('Y-m-d')]
        ];
    }
}
