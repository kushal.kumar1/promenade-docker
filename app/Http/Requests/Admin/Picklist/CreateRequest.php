<?php

namespace Niyotail\Http\Requests\Admin\Picklist;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create-picklist');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'selected' => 'required|array|min:1',
            'selected.*' => 'numeric|exists:orders,id'
        ];
    }

    public function messages()
    {
        return [
            'selected.required' => "No orders selected"
        ];
    }
}
