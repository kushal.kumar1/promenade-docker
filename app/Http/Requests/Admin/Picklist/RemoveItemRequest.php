<?php

namespace Niyotail\Http\Requests\Admin\Picklist;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class RemoveItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('process-picklist-item');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picklist_id'=> 'required|exists:picklists,id',
            'product_id' => 'required',
        ];
    }
}
