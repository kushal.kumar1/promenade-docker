<?php

namespace Niyotail\Http\Requests\Admin\Picklist;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class AddItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('process-picklist-item');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picklist_id'=> 'required|exists:picklists,id',
            'product_id' => 'required',
            'quantity' => 'required|numeric|min:0',
            'skip.*.quantity' => 'required|numeric|min:0',
            'skip.*.reason' => 'required',
        ];
    }
}
