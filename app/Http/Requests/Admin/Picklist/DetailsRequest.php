<?php

namespace Niyotail\Http\Requests\Admin\Picklist;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Picklist;

class DetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Gate::allows('view-picklist')) {
            return MultiWarehouseHelper::isValidWarehouse(Picklist::class, $this->id);
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
