<?php

namespace Niyotail\Http\Requests\Admin\Invoice;

use Niyotail\Http\Requests\Request;

class RegenerateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:invoices,id'
        ];
    }
}
