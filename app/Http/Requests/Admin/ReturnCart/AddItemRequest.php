<?php

namespace Niyotail\Http\Requests\Admin\ReturnCart;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\ProductVariant;

class AddItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-return');
    }

    public function rules()
    {
        return [
            'store_id' => 'required',
//            'quantity' => 'required|numeric|min:1',
            'shipment_id' => 'required|numeric',
            'reason' => 'required',
            'sku' => 'required | exists:'.(new ProductVariant())->getTable().',sku',
        ];
    }
}
