<?php

namespace Niyotail\Http\Requests\Admin\ReturnCart;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class RemoveItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-order');
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
