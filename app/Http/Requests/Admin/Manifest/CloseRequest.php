<?php

namespace Niyotail\Http\Requests\Admin\Manifest;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CloseRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-manifest');
    }

    public function rules()
    {
        return [
            'id'=>'required|exists:manifests,id',
        ];
    }
}
