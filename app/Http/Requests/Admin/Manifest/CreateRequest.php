<?php

namespace Niyotail\Http\Requests\Admin\Manifest;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-manifest');
    }

    public function rules()
    {
        return [
            'selected' => 'required|array',
            'selected.*'=>'required|exists:shipments,id',
        ];
    }
}
