<?php

namespace Niyotail\Http\Requests\Admin\Manifest;

use Illuminate\Validation\Rule;
use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Order;
use Niyotail\Models\Shipment;

class DeliverRequest extends Request
{
    public function authorize(): bool
    {
        return Gate::allows('update-manifest');
    }

    public function rules(): array
    {
        return [
            'id'=>'required|exists:manifests,id',
            'otps.*' => Rule::requiredIf($this->otpValidation())
        ];
    }

    public function messages()
    {
        return [
            'otps.*.required' => 'Otp is required!'
        ];
    }

    private function otpValidation(): \Closure
    {
        return function () {
            $shipment = Shipment::with('order')->whereIn('id', array_keys($this->otps))->first();
            return $shipment->order->type != Order::TYPE_STOCK_TRANSFER;
        };
    }
}
