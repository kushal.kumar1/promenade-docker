<?php

namespace Niyotail\Http\Requests\Admin\Shipments;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class RetryRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-shipment');
    }

    public function rules()
    {
        return [
            'id'=>'required|exists:shipments,id',
            'type' => 'string'
        ];
    }
}
