<?php

namespace Niyotail\Http\Requests\Admin\Shipments;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-shipment');
    }

    public function rules()
    {
        return [

        ];
    }
}
