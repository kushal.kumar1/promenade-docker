<?php

namespace Niyotail\Http\Requests\Admin\Shipments;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Order;
use Niyotail\Models\Shipment;

class DeliveredRequest extends Request
{
    public function authorize(): bool
    {
        return Gate::allows('update-shipment');
    }

    public function rules(): array
    {
        return [
            'id' => 'required|exists:shipments,id',
            'otp' => Rule::requiredIf($this->otpValidation())
        ];
    }

    private function otpValidation(): \Closure
    {
        return function () {
            $shipment = Shipment::with('order')->find($this->id);
            return $shipment->order->type != Order::TYPE_STOCK_TRANSFER;
        };
    }
}
