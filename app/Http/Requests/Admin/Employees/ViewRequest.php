<?php

namespace Niyotail\Http\Requests\Admin\Employees;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-employee');
    }

    public function rules()
    {
        return [

        ];
    }
}
