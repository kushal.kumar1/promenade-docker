<?php

namespace Niyotail\Http\Requests\Admin\Employees;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-employee');
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required_without:mobile|nullable|email|unique:employees,email,',
            'mobile' => 'required_without:email|nullable|numeric|digits:10|unique:employees,mobile,',
            'password' => 'required|min:3'
        ];
    }
}
