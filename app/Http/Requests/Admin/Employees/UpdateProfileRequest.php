<?php

namespace Niyotail\Http\Requests\Admin\Employees;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateProfileRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' =>'required',
            'password'=>'required_with:change-password'.(!empty($this->get('change-password'))?'|min:3':''),
        ];
    }
}
