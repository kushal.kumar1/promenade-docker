<?php

namespace Niyotail\Http\Requests\Admin\Employees;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-employee');
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'email' => 'required_without:mobile|email|unique:employees,email,'.$this->id,
            'name' => 'required',
            'mobile' => 'required_without:email|nullable|numeric|digits:10',
            'password' => 'required_with:change-password' . (!empty($this->get('change-password')) ? '|min:3' : ''),
            'warehouses' => 'required|array',
            'roles' => 'array'
        ];
    }
}
