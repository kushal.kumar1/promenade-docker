<?php

namespace Niyotail\Http\Requests\Admin\Attributes;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-attribute');
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric',
            'name'=>'required|alpha_num_space',
            'is_filter'=>'boolean'
        ];
    }
}
