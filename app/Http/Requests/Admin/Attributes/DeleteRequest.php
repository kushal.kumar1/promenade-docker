<?php

namespace Niyotail\Http\Requests\Admin\Attributes;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class DeleteRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('delete-attribute');
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric'
        ];
    }
}
