<?php

namespace Niyotail\Http\Requests\Admin\Attributes;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-attribute');
    }

    public function rules()
    {
        return [

        ];
    }
}
