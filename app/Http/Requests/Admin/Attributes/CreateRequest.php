<?php

namespace Niyotail\Http\Requests\Admin\Attributes;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-attribute');
    }

    public function rules()
    {
        return [
            'name'=>'required|alpha_num_space',
            'is_filter'=>'boolean'
        ];
    }
}
