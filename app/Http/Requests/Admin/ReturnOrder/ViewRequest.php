<?php

namespace Niyotail\Http\Requests\Admin\ReturnOrder;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-return');
    }

    public function rules()
    {
        return [

        ];
    }
}
