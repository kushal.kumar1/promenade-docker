<?php

namespace Niyotail\Http\Requests\Admin\ReturnOrder;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class CancelRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-return');
    }

    public function rules()
    {
        return [
            'id' => 'required'
        ];
    }
}
