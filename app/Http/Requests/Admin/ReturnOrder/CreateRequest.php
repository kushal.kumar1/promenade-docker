<?php

namespace Niyotail\Http\Requests\Admin\ReturnOrder;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-return');
    }

    public function rules()
    {
        return [
            'shipment_id' => 'required',
            'items' => 'required|array',
            'reason' => 'required'
        ];
    }
}
