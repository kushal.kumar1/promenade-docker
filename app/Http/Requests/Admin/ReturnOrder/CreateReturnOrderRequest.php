<?php

namespace Niyotail\Http\Requests\Admin\ReturnOrder;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateReturnOrderRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-return');
    }

    public function rules()
    {
        return [
            'store_id' => 'required'
        ];
    }
}
