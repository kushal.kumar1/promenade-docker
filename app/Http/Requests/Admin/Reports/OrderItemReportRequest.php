<?php

namespace Niyotail\Http\Requests\Admin\Reports;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class OrderItemReportRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-report');
    }

    public function rules()
    {
        return [

        ];
    }
}
