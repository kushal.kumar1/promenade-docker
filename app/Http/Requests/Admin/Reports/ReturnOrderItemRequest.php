<?php

namespace Niyotail\Http\Requests\Admin\Reports;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class ReturnOrderItemRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('view-report');
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
