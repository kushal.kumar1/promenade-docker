<?php

namespace Niyotail\Http\Requests\Admin\Badges;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-badge');
    }

    public function rules()
    {
        return [

        ];
    }
}
