<?php

namespace Niyotail\Http\Requests\Admin\Badges;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class DeleteRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('delete-badge');
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric'
        ];
    }
}
