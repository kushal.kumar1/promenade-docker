<?php

namespace Niyotail\Http\Requests\Admin\Badges;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-badge');
    }

    public function rules()
    {
        return [
            'name'=>'required|alpha_num_space',
            // 'description'=>'required'
        ];
    }
}
