<?php

namespace Niyotail\Http\Requests\Admin\Brand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-brand');
    }

    public function rules()
    {
        return [

        ];
    }
}
