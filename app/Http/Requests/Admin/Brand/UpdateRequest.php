<?php

namespace Niyotail\Http\Requests\Admin\Brand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-brand');
    }

    public function rules()
    {
        return [
            'id' => 'required | numeric',
            'name' => 'required|string',
            'marketer_id' => 'required|exists:marketers,id'
        ];
    }
}
