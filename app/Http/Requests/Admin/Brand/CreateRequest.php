<?php

namespace Niyotail\Http\Requests\Admin\Brand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-brand');
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'marketer_id' => 'required|exists:marketers,id'
            // 'logo'=>'dimensions:max_width=500,max_height=500'
        ];
    }
}
