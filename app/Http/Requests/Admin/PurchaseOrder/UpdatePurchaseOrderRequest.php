<?php


namespace Niyotail\Http\Requests\Admin\PurchaseOrder;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Models\Warehouse;

class UpdatePurchaseOrderRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-purchase_order');
    }

    public function rules()
    {
        return [
            'vendor_address_id'=>'required|exists:'.(new VendorsAddresses())->getTable().',id',
            'warehouse_id'=>'required|exists:'.(new Warehouse())->getTable().',id',
            'delivery_date' => 'required'
        ];
    }
}