<?php


namespace Niyotail\Http\Requests\Admin\PurchaseOrder;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class AddNewGroupRequest extends Request
{
    public function authorize()
    {
        //return Gate::allows('create-purchase-order');
        return true;
    }

    public function rules()
    {
        return [
            'group_id' => 'required|int',
            'quantity' => 'required|int',
        ];
    }
}