<?php

namespace Niyotail\Http\Requests\Admin\PurchaseOrder;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-purchase_order');
    }

    public function rules()
    {
        return [

        ];
    }
}
