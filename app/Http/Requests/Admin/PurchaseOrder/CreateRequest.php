<?php

namespace Niyotail\Http\Requests\Admin\PurchaseOrder;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\Vendor;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-purchase_order');
    }

    public function rules()
    {
        return [
            'vendor_id'=>'required|exists:'.(new Vendor())->getTable().',id',
        ];
    }
}
