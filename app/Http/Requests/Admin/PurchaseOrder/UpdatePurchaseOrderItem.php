<?php


namespace Niyotail\Http\Requests\Admin\PurchaseOrder;


use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdatePurchaseOrderItem extends Request
{
    public function authorize()
    {
        //return Gate::allows('create-purchase-order');
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|int', // po item id
            'product_id' => 'required|int',
            'sku' => 'required|string',
            'quantity' => 'required|int',
            'price_type' => 'required|string',
            'pc_factor' => 'required_unless:price_type,latest_unit_cost'
        ];
    }
}