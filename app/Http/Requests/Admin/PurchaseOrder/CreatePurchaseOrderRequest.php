<?php


namespace Niyotail\Http\Requests\Admin\PurchaseOrder;


use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreatePurchaseOrderRequest extends Request
{
    public function authorize()
    {
        //return Gate::allows('create-purchase-order');
        return true;
    }

    public function rules()
    {
        return [
            'vendor_id' => 'required|int',
            'demand_item_ids' => 'required|array',
        ];
    }
}