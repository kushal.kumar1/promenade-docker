<?php

namespace Niyotail\Http\Requests\Admin\Product;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Product;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-product') || Gate::allows('update-product-racks');
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:products,id',
            'name' => 'required | string',
            // 'slug' => 'required | alpha_dash',
            'brand_id' => 'required|exists:brands,id',
//            'collections' => 'required',
            'hsn_sac_code' => 'required',
            'uom' => 'required',
            'tax_class_id' => 'required|exists:tax_classes,id',
            'attributes.*.value' => 'required_with:attributes',
        ];
    }

    public function messages()
    {
        return [
            'attributes.*.value.required_with' => 'Attribute Value is required'
        ];
    }
}
