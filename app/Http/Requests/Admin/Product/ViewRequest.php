<?php

namespace Niyotail\Http\Requests\Admin\Product;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-product');
    }

    public function rules()
    {
        return [

        ];
    }
}
