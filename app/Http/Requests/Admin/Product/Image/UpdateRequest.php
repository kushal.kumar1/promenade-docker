<?php

namespace Niyotail\Http\Requests\Admin\Product\Image;

use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-product');
    }

    public function rules()
    {
        return [

        ];
    }
}
