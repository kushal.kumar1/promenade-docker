<?php

namespace Niyotail\Http\Requests\Admin\Product\Image;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-product');
    }

    public function rules()
    {
        return [
            'images.*'=>'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ];
    }
}
