<?php

namespace Niyotail\Http\Requests\Admin\Product;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-product');
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'brand_id' => 'required|exists:brands,id',
            'group_id' => 'nullable|exists:product_groups,id',
//            'collections' => 'required',
            'hsn_sac_code'=>'required',
            'uom' => 'required',
            'cl4_id' => 'required|exists:newcat_l4,id',
            'tax_class_id' => 'required | exists:tax_classes,id',
            'attributes.*.value'=>'required_with:attributes',
            'tag_ids' => 'nullable|array',
            'tag_ids.*' => 'exists:tags,id'
        ];
    }

    public function messages()
    {
        return [
            'attributes.*.value.required_with' => 'Attribute Value is required'
        ];
    }
}
