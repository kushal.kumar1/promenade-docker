<?php

namespace Niyotail\Http\Requests\Admin\Beat;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-beat');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'name' => 'required|string',
            'long_name'=>'required|string',
            'brands'=>'nullable|array',
            'status'=>'required|boolean'
        ];
    }
}
