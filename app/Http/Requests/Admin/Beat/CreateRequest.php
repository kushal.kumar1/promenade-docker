<?php

namespace Niyotail\Http\Requests\Admin\Beat;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-beat');
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'long_name'=>'required|string',
            'brands'=>'nullable|array',
            'status'=>'required|boolean'
        ];
    }
}
