<?php

namespace Niyotail\Http\Requests\Admin\ChequeMeta;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create-cheque-meta');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id' => 'required|exists:transactions,id',
            'cheque_issue_date' => 'required|date',
            'image' => 'string',
            'cheque_number' => 'required|numeric',
            'bank_id' => 'required|numeric|exists:banks,id'
        ];
    }
}
