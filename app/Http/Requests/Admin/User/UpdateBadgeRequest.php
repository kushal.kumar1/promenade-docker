<?php

namespace Niyotail\Http\Requests\Admin\User;
use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class UpdateBadgeRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-user');
    }

    public function rules()
    {
        return [
            'badges'=>'array'
        ];
    }
}
