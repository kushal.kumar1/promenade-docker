<?php

namespace Niyotail\Http\Requests\Admin\User;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-user');
    }

    public function rules()
    {
        return [
            'first_name' => 'required|alpha_space',
            'last_name' => 'alpha_space',
            'mobile' => 'required|phone:mobile',
            'email' => 'email | nullable',
        ];
    }
}
