<?php

namespace Niyotail\Http\Requests\Admin\User;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-user');
    }

    public function rules()
    {
        return [

        ];
    }
}
