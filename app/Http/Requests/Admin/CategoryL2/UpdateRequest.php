<?php

namespace Niyotail\Http\Requests\Admin\CategoryL2;

use Niyotail\Http\Requests\Request;
// use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required | string',
            'category_l1_id' => 'required',
        ];
    }
}
