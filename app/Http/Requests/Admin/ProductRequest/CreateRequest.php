<?php

namespace Niyotail\Http\Requests\Admin\ProductRequest;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Brand;
use Niyotail\Models\Collection;
use Niyotail\Models\NewCatL4;
use Niyotail\Models\TaxClass;

class CreateRequest extends Request
{
    public function authorize()
    {
        return true;//Gate::allows('create-product_request');
    }

    public function rules()
    {
        return [
            'brand_id' => 'nullable|exists:'.(new Brand())->getTable().',id',
            'collection_id' => 'nullable|exists:'.(new Collection())->getTable().',id',
            'name' => 'required',
            // 'barcode' => 'required',
            'mrp' => 'required',
            'uom' => 'required',
            'tax_class_id' => 'required|exists:'.(new TaxClass())->getTable().',id',
            'cl4_id' => 'required|exists:'.(new NewCatL4())->getTable().',id'
        ];
    }
}
