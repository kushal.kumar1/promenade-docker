<?php

namespace Niyotail\Http\Requests\Admin\Pincode;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdatePincodeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'city_id' => 'required'
        ];
    }
}
