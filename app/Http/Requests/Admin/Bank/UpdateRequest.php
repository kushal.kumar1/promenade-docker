<?php

namespace Niyotail\Http\Requests\Admin\Bank;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-bank');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'name'=>'required|string',
            'is_active' => 'required'
        ];
    }
}
