<?php

namespace Niyotail\Http\Requests\Admin\Store;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-store');
    }

    public function rules()
    {
        return [
            'beat_id' => 'required | exists:beats,id',
            'id' => 'required',
            'name' => 'required',
            'gstin' => ['nullable', 'regex:/^([0][1-9]|[1-2][0-9]|[3][0-5])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/'],
            'address' => 'required',
            'landmark' => 'required',
            'pincode' => 'required|numeric|exists:pincodes,pincode',
            'lat' => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng' => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'status' => 'boolean',
            'verified' => 'boolean'
        ];
    }
}
