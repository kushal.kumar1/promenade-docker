<?php

namespace Niyotail\Http\Requests\Admin\Store;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewTransactionRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-transaction');
    }

    public function rules()
    {
    }
}
