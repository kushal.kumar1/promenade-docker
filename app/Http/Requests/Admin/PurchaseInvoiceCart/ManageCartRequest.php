<?php

namespace Niyotail\Http\Requests\Admin\PurchaseInvoiceCart;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\PurchaseInvoiceCart;

class ManageCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return MultiWarehouseHelper::isValidWarehouse(PurchaseInvoiceCart::class, $this->vendor_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
