<?php

namespace Niyotail\Http\Requests\Admin\Importers;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-importer');
    }

    public function rules()
    {
        return [
            'type' => 'required',
            'file' => 'required|mimes:csv,txt|max:5120'
        ];
    }
}
