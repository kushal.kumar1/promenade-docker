<?php

namespace Niyotail\Http\Requests\Admin\Importers;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-importer');
    }

    public function rules()
    {
        return [

        ];
    }
}
