<?php

namespace Niyotail\Http\Requests\Admin\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Niyotail\Models\Store;
use Niyotail\Models\Tag;
use Niyotail\Models\Transaction;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('create-transaction');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' => 'required|exists:' . (new Store())->getTable() . ',id',
            'type' => 'required|' . Rule::in(Transaction::getConstants('type')),
            'payment_method' => 'required|' . Rule::in(Transaction::getConstants('payment_method')),
//            'payment_tag' => 'required_if:type,credit|' . Rule::in(Tag::paymentTags()),
            'cheque_date' => 'required_if:payment_method,cheque,cheque_bounce|date_format:"d M, Y"',
            'cheque_number' => 'required_if:payment_method,cheque,cheque_bounce',
            'cheque_issuer' => 'required_if:payment_method,cheque,cheque_bounce',
            'amount' => 'required_if:auto_settle,1|numeric|min:1',
            'auto_settle' => 'required_if:type,credit|integer',
            'settle_transactions' => "required_if:auto_settle,0",
            'settle_transactions.*.amount' => "required_with:settle_transactions",
            'agent_id' => "required_if:type,credit",
        ];
    }
}
