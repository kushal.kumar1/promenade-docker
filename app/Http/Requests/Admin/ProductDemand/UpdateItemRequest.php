<?php

namespace Niyotail\Http\Requests\Admin\ProductDemand;

use Illuminate\Validation\Rule;
use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\ProductDemandItem;
use Niyotail\Models\ProductVariant;

class UpdateItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-purchase_order');
    }

    public function rules()
    {
        return [
            'id'=>['required',Rule::exists((new ProductDemand())->getTable())],
            'items.*.sku'=>'required',
            'items.*.cost'=>'nullable|numeric',
            'items.*.lead_time'=>'nullable|numeric',
            'items.*.days_of_demand'=>'nullable|numeric',
            'items.*.demand_booster'=>'nullable|numeric',
            'items.*.safety_stock'=>'nullable|numeric',
        ];
    }

    public function messages()
    {
        return [
//            'items.*.quantity.required' => 'Item quantity is required',
//            'items.*.quantity.min' => 'Item quantity should be minimum 1',
        ];
    }

}
