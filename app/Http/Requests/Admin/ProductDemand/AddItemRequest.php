<?php

namespace Niyotail\Http\Requests\Admin\ProductDemand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Product;
use Niyotail\Models\ProductDemandItem;

class AddItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-purchase_order');
    }

    public function rules()
    {
        return [
            'product_id'=>'required|exists:'.(new Product())->getTable().',id',
        ];
    }
}
