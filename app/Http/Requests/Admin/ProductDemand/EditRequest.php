<?php

namespace Niyotail\Http\Requests\Admin\ProductDemand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class EditRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-purchase_order');
    }

    public function rules()
    {
        return [

        ];
    }
}
