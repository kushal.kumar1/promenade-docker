<?php

namespace Niyotail\Http\Requests\Admin\ProductDemand;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Marketer;
use Niyotail\Models\Vendor;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-purchase_order');
    }

    public function rules()
    {
        return [
            'marketer_id' => 'nullable|exists:'.(new Marketer())->getTable().',id',
            'vendor_id' => 'nullable|exists:'.(new Vendor())->getTable().',id',
            'stock_threshold_days' => 'required|min:1',
            'days_for_average_sales' => 'required|min:1',
        ];
    }
}
