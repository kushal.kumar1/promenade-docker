<?php


namespace Niyotail\Http\Requests\Admin\ProfileQuestions;


use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class QuestionRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('edit-profile-questions');
    }

    public function rules()
    {
        return [
            'id' => 'required|sometimes',
            'name' => 'required',
            'type' => 'required',
            'status' => 'required',
            'priority' => 'required',
            'is_mandatory' => 'required'
        ];
    }
}