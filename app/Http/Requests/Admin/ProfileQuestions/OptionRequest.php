<?php


namespace Niyotail\Http\Requests\Admin\ProfileQuestions;


use Illuminate\Support\Facades\Gate;
use Niyotail\Http\Requests\Request;

class OptionRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('edit-profile-questions');
    }

    public function rules()
    {
        return [
            'id' => 'required|sometimes',
            'question_id' => 'required',
            'option' => 'required',
            'status' => 'required',
            'priority' => 'required'
        ];
    }
}