<?php

namespace Niyotail\Http\Requests\Admin\Roles;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-role');
    }

    public function rules()
    {
        return [

        ];
    }
}
