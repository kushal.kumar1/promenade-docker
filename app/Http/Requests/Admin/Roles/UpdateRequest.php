<?php

namespace Niyotail\Http\Requests\Admin\Roles;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-role');
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'permissions'=>'array'
        ];
    }
}
