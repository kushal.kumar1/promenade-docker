<?php

namespace Niyotail\Http\Requests\Admin\Roles;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-role');
    }

    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
