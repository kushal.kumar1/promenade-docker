<?php

namespace Niyotail\Http\Requests\Admin\Tax;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update-tax_class');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tax_class_id' => 'required',
            'supply_state_id' => 'nullable',
            'source_state_id' => 'required',
            'supply_country_id' => 'nullable',
            'name' => 'required|string',
            'percentage' => 'required|numeric',
            'min_price' => 'required|numeric',
            'max_price' => 'nullable|numeric',
            'start_date' => 'required|date',
            'end_date' => 'nullable|date'
        ];
    }

    public function messages()
    {
        return [
          'source_state_id.required'=>'Please select the Source State.'
        ];
    }
}
