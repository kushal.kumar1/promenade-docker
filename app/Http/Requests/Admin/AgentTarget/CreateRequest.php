<?php

namespace Niyotail\Http\Requests\Admin\AgentTarget;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Niyotail\Models\Agent;

class CreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id',
            'store_id' => 'nullable|exists:stores,id',
            'valid_from' => 'required|date_format:Y-m-d',
            'valid_to' => 'nullable|date_format:Y-m-d',
            'revenue' => 'required|numeric',
            'unique_stores' => 'required|numeric',
            'points' => 'required|numeric',
        ];
    }
}
