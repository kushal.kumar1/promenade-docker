<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CancelOrderItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-order');
    }

    public function rules()
    {
        return [
            'sku'=>'required',
            'reason'=>'required',
            'quantity'=>'required',
        ];
    }
}
