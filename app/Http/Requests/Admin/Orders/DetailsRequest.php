<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Order;

class DetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        if (Gate::allows('view-order')) {
            return MultiWarehouseHelper::isValidWarehouse(Order::class, $this->id);
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
