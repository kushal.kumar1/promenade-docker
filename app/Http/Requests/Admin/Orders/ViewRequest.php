<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-order');
    }

    public function rules()
    {
        return [

        ];
    }
}
