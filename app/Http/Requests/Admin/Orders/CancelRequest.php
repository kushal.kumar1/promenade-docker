<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CancelRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-order');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'reason'=>'required'
        ];
    }
}
