<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class AssignAgentRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('assign-agent');
    }

    public function rules()
    {
        return [
            'agent_id' => 'required|exists:agents,id'
        ];
    }
}
