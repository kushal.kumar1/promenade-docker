<?php

namespace Niyotail\Http\Requests\Admin\Orders;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateOrderRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-order');
    }

    public function rules()
    {
        return [
            'store_id' => 'required',
            'payment_method' => 'required',
            'agent_id' => 'nullable|exists:agents,id'
        ];
    }
}
