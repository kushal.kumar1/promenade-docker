<?php

namespace Niyotail\Http\Requests\Admin\Agent;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-agent');
    }

    public function rules()
    {
        return [

        ];
    }
}
