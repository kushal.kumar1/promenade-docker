<?php

namespace Niyotail\Http\Requests\Admin\Agent;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Niyotail\Models\Agent;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-agent');
    }

    public function rules()
    {
        return [
            'code' => 'required|string',
            'name' => 'required|alpha_space',
            'mobile' => 'required|phone:mobile',
            'email' => 'email | nullable',
            'type' => 'required|'.Rule::in(Agent::getEnum('type')),
        ];
    }
}
