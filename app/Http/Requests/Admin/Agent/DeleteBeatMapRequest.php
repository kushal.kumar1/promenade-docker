<?php

namespace Niyotail\Http\Requests\Admin\Agent;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class DeleteBeatMapRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-agent');
    }

    public function rules()
    {
        return [
            'id'=>'required|exists:beat_marketers,id',
            'agent_id' => 'required|exists:agents,id',
        ];
    }
}
