<?php

namespace Niyotail\Http\Requests\Admin\Agent;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateBeatMapRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-agent');
    }

    public function rules()
    {
        return [
            'beat_id'=>'required|exists:beats,id',
            'marketers'=>'required',
            'agent_id' => 'required|exists:agents,id',
        ];
    }
}
