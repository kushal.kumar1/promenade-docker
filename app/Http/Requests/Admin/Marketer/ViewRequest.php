<?php

namespace Niyotail\Http\Requests\Admin\Marketer;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class ViewRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('view-marketer');
    }

    public function rules()
    {
        return [

        ];
    }
}
