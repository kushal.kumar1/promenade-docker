<?php

namespace Niyotail\Http\Requests\Admin\Marketer;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-marketer');
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'alias' => 'required|string',
            'code' => 'required|string',
            'level_id' => 'required|integer',
        ];
    }
}
