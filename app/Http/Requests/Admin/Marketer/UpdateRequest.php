<?php

namespace Niyotail\Http\Requests\Admin\Marketer;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-marketer');
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:marketers,id',
            'name' => 'required|string',
            'alias' => 'required|string',
            'code' => 'required|string',
            'level_id' => 'required|integer',
        ];
    }
}
