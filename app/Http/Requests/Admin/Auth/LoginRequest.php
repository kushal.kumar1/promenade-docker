<?php

namespace Niyotail\Http\Requests\Admin\Auth;

use Niyotail\Http\Requests\Request;

class LoginRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'=>'required|email',
            'password'=>'required',
        ];
    }
}
