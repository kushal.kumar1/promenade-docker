<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;

class UpdateFlatInventoryStatusRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'warehouse_id' => 'required',
            'product_id' => 'required',
            'old_status' => 'required',
            'new_status' => 'required',
            'quantity' => 'required'
        ];
    }
}
