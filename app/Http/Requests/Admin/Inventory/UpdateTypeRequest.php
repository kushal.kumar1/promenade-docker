<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Warehouse;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class UpdateTypeRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-inventory');
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:inventories,id',
            'type' => 'required|'.Rule::in(['regular','group_buy']),
            'quantity' => 'required|numeric|min:1'
        ];
    }
}
