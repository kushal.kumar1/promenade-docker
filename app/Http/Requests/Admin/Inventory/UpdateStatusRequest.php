<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Warehouse;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class UpdateStatusRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-inventory');
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:inventories,id',
            'status' => 'required|boolean',
            'inactive_reason' => 'required_if:status,0|nullable|'.Rule::in(config('settings.inventory_inactive_reasons'))
        ];
    }
}
