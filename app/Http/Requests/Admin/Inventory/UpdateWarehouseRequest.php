<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Warehouse;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class UpdateWarehouseRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-inventory');
    }

    public function rules()
    {
        return [
            'id' => 'required|exists:inventories,id',
            'to_warehouse_id' => 'required|exists:warehouses,id',
            'quantity' => 'required|numeric|min:1'
        ];
    }
}
