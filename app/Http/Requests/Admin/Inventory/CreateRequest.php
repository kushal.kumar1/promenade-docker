<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\Warehouse;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'batch_number' => 'required',
            'purchase_invoice_id' => 'nullable | exists:'.(new PurchaseInvoice())->getTable().',id',
            'warehouse_id' => 'required | exists:'.(new Warehouse())->getTable().',id',
            'quantity' => 'required|numeric',
            'sku' => 'required | exists:'.(new ProductVariant())->getTable().',sku',
            'expiry_date' => 'date|nullable',
            'mfg_date' => 'date|nullable',
            'total_cost' => 'required|numeric',
        ];
    }
}
