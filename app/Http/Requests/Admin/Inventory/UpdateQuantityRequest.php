<?php

namespace Niyotail\Http\Requests\Admin\Inventory;

use Niyotail\Http\Requests\Request;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Warehouse;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Gate;

class UpdateQuantityRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-inventory');
    }

    public function rules()
    {
        return [
            'quantity' => 'required'
        ];
    }
}
