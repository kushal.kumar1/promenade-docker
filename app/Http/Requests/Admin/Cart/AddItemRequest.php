<?php

namespace Niyotail\Http\Requests\Admin\Cart;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class AddItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-order');
    }

    public function rules()
    {
        return [
            'store_id' => 'required',
            'warehouse_id' => 'required|exists:warehouses,id',
            'quantity' => 'required|regex:/^\d+(\.\d{1,3})?$/|gt:0',
            'id' => 'required|numeric'
        ];
    }
}
