<?php

namespace Niyotail\Http\Requests\Admin\Cart;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateItemRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-order');
    }

    public function rules()
    {
        return [
            'id' => 'required',
            'quantity' => 'required|regex:/^\d+(\.\d{1,3})?$/|gt:0'
        ];
    }
}
