<?php

namespace Niyotail\Http\Requests\Admin\Cart;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CloneRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-order');
    }

    public function rules()
    {
        return [

        ];
    }
}
