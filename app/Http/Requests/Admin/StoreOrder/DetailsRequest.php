<?php

namespace Niyotail\Http\Requests\Admin\StoreOrder;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\Utils;
use Niyotail\Models\StoreOrder;

class DetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //TODO: Add Gate Permissions;
         return MultiWarehouseHelper::isValidWarehouse(StoreOrder::class, $this->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
