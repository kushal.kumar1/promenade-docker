<?php

namespace Niyotail\Http\Requests\Admin\StoreOrder;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Niyotail\Models\Store;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        $store = Store::find($this->id);

        if ($store->type == Store::TYPE_WAREHOUSE) {
            return Gate::allows('create-stock-transfer');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
