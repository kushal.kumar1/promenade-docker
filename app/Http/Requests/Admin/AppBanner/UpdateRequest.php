<?php

namespace Niyotail\Http\Requests\Admin\AppBanner;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class UpdateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('update-app_banner');
    }

    public function rules()
    {
        return [
            'id'=>'required',
            'valid_from'=>'nullable|date',
            'valid_to'=>'nullable|date',
            'position'=>'required',
            'status'=>'required',
            'file'=>'dimensions:ratio=2'
        ];
    }
}
