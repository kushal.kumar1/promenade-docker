<?php

namespace Niyotail\Http\Requests\Admin\AppBanner;

use Niyotail\Http\Requests\Request;
use Illuminate\Support\Facades\Gate;

class CreateRequest extends Request
{
    public function authorize()
    {
        return Gate::allows('create-app_banner');
    }

    public function rules()
    {
        return [
            'position'=>'required',
            'status'=>'required',
            'valid_from'=>'nullable|date',
            'valid_to'=>'nullable|date',
            'file'=>'required|dimensions:ratio=2'
        ];
    }
}
