<?php

namespace Niyotail\Http\Requests\Admin\Rule;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update-rule');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'position' => 'required|integer',
            'should_stop' => 'required|in:0,1',

            'offer' => 'required',
            'source_quantity' => 'nullable|integer|min:1',
            'target_quantity' => 'nullable|integer|min:1',
            'offer_value' => 'nullable|between:0,99.99|min:1',

            'max_offer' => 'nullable|integer|min:1',
            'user_limit' => 'nullable|integer|min:1',
            'total_limit' => 'nullable|integer|min:1',
            'status' => 'required|in:0,1',

            'start_at' => 'required|date_format:"d M, Y h:i A"',
            'end_at' => 'nullable|date_format:"d M, Y h:i A"|after:start_at',

            'conditions' => 'required|array|min:1',
            'conditions.*.expressions' => 'required|array|min:1',
            'conditions.*.expressions.*.name' => 'required',
            'conditions.*.expressions.*.values' => 'required_without:conditions.*.expressions.*.min_value|array|min:1',
            'conditions.*.expressions.*.min_value' => 'required_without:conditions.*.expressions.*.values|numeric',
            'conditions.*.expressions.*.max_value' => 'nullable|numeric',

            'targets' => 'array',
            'target_type' => 'required_with:targets'
        ];
    }
}
