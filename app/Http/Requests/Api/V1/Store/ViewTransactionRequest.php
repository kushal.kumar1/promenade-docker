<?php

namespace Niyotail\Http\Requests\Api\V1\Store;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Store;

class ViewTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        $store = Store::find($id);
        if (!empty($store)) {
            $user = Auth::guard('api')->user();
            $store = Store::whereHas('users', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->find($id);
            if (empty($store))
                return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
