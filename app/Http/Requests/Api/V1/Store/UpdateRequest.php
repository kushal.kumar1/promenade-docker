<?php

namespace Niyotail\Http\Requests\Api\V1\Store;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Store;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $id = $this->route('id');
        $store = Store::find($id);
        if (!empty($store)) {
            $user = Auth::guard('api')->user();
            $store = Store::whereHas('users', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })->find($id);
            if (empty($store)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'gstin' => 'nullable|string',
            'address' => 'required|string',
            'landmark' => 'required|string',
            'pincode' => 'required|string',
            'lat' => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng' => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'contact_person'=>'required',
            'contact_mobile'=>'required',
        ];
    }
}
