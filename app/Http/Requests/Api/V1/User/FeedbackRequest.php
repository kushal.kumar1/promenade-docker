<?php

namespace Niyotail\Http\Requests\Api\V1\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Niyotail\Models\UserFeedback;
use Niyotail\Models\User;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $reqUser = User::find($this->id);
        $user = Auth::guard('api')->user();
        if (!empty($reqUser)) {
            if ($user->id != $reqUser->id) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'=>'required|'.Rule::in(UserFeedback::getConstants('type')),
            'feedback'=>'required|string'
        ];
    }
}
