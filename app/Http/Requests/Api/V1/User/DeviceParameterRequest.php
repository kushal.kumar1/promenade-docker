<?php

namespace Niyotail\Http\Requests\Api\V1\User;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Models\Agent;

class DeviceParameterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_id'=>'required'
        ];
    }
}
