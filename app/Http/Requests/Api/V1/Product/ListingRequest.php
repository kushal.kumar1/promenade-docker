<?php

namespace Niyotail\Http\Requests\Api\V1\Product;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Models\Store;

class ListingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' => 'required',
            'collections.*'=> 'sometimes|required|exists:collections,id',
            'brands.*'=> 'sometimes|required|exists:brands,id',
        ];
    }
}
