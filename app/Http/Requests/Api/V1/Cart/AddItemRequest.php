<?php

namespace Niyotail\Http\Requests\Api\V1\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Store;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:' . (new ProductVariant())->getTable().',id',
            'quantity' => 'required|numeric|min:0.1',
            'store_id' => 'required|exists:' . (new Store())->getTable().',id',
        ];
    }
}
