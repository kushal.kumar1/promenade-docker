<?php

namespace Niyotail\Http\Requests\Api\V1\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\CartItem;

class UpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $cartItem = CartItem::with('cart')->find($this->id);
        $user = Auth::guard('api')->user();
        if (!empty($cartItem)) {
            if ($user->id != $cartItem->cart->created_by_id) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quantity'=>'required|numeric|min:0.1'
        ];
    }
}
