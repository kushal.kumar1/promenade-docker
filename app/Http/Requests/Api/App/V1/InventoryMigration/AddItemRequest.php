<?php

namespace Niyotail\Http\Requests\Api\App\V1\InventoryMigration;

use Illuminate\Foundation\Http\FormRequest;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() :bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'warehouse_id' => 'required|exists:warehouses,id',
            'product_id' => 'required|exists:products,id',
            'source_storage_id' => 'required|exists:storages,id',
            'migration_item_id' => 'required|exists:inventory_migration_items,id',
            'quantity' => 'required|min:1'
        ];
    }
}
