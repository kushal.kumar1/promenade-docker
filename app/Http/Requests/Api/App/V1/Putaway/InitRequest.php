<?php

namespace Niyotail\Http\Requests\Api\App\V1\Putaway;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InitRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [];
    }

}
