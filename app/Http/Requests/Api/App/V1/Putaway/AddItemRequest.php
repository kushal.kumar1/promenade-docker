<?php

namespace Niyotail\Http\Requests\Api\App\V1\Putaway;

use Illuminate\Foundation\Http\FormRequest;

class AddItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() :bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'required|exists:putaways,id',
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|min:1'
        ];
    }
}
