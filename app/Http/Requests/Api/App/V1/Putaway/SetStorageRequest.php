<?php

namespace Niyotail\Http\Requests\Api\App\V1\Putaway;

use Illuminate\Foundation\Http\FormRequest;

class SetStorageRequest extends FormRequest
{
    public function authorize(): bool
    {
        //TODO: put check if user owns the putaway list
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'required|exists:putaway_items,id',
            'storage_id' => 'required|exists:storages,id',
        ];
    }
}
