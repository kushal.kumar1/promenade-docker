<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class LedgerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'date_format:"d M, Y"',
            'end_date' => 'date_format:"d M, Y"',
        ];
    }
}
