<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Transaction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Niyotail\Models\Invoice;
use Niyotail\Models\Store;
use Niyotail\Models\Transaction;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' => 'required|exists:'.(new Store())->getTable().',id',
            'amount'=>'required|min:1',
            'payment_method'=>['required',Rule::in([Transaction::PAYMENT_METHOD_CASH,Transaction::PAYMENT_METHOD_CHEQUE,
                Transaction::PAYMENT_METHOD_UPI,Transaction::PAYMENT_METHOD_NET_BANKING, Transaction::PAYMENT_METHOD_PAYTM_ABHISHEK,
                Transaction::PAYMENT_METHOD_PAYTM_SANGEETESH, Transaction::PAYMENT_METHOD_PAYTM_SACHIN])],
            'reference_id'=>[Rule::requiredIf(in_array($this->get('payment_method'),[Transaction::PAYMENT_METHOD_UPI,Transaction::PAYMENT_METHOD_NET_BANKING]))],
            'cheque_date'=>'required_if:payment_method,'.Transaction::PAYMENT_METHOD_CHEQUE.'|date_format:"d M, Y"',
            'cheque_number'=>'required_if:payment_method,'.Transaction::PAYMENT_METHOD_CHEQUE,
            'image'=>[Rule::requiredIf(in_array($this->get('payment_method'),[Transaction::PAYMENT_METHOD_CHEQUE,
                Transaction::PAYMENT_METHOD_UPI,Transaction::PAYMENT_METHOD_NET_BANKING])),'mimes:jpeg,png,jpg,gif,svg'],
            'invoices'=>'required_without:auto_settle|array',
            'invoices.*.id'=>'required_without:auto_settle|exists:'.(new Invoice())->getTable().',id',
            'invoices.*.amount_to_settle'=>'required_without:auto_settle|min:1',
        ];
    }
}
