<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\StoreProfiling;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer_id' => "required_without:answer|array|min:1",
            'answer.*'  => "required_without:answer|integer",
            'answer' => 'required_without:answer_id|string',
        ];
    }
}
