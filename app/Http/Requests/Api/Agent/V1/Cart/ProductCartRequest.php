<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Cart;

use Illuminate\Foundation\Http\FormRequest;

class ProductCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variant_id' => 'required|exists:product_variants,id',
            'store_id' => 'required|exists:stores,id',
            'price' =>  'required|numeric|between:0,999999.99',
            'quantity' =>  'required|numeric|between:0,999999',
        ];
    }
}
