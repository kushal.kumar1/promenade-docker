<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Niyotail\Models\Agent;
use Niyotail\Models\CartItem;

class RemoveItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $cartItem = CartItem::with('cart')->find($this->id);
        $user = Auth::guard('agent_api')->user();
        if (!empty($cartItem)) {
            if ($user->id != $cartItem->cart->created_by_id || !$cartItem->cart->isCreatedBy(Agent::class)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
