<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Delivery;

use Niyotail\Http\Requests\Request;

class RetryRTORequest extends Request
{
    public function rules()
    {
        return [
            'reason' => 'required',
        ];
    }
}
