<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Delivery;

use Niyotail\Http\Requests\Request;

class ReturnRequest extends Request
{
    public function rules()
    {
        return [
            'items' => 'required|array',
            'items.*.sku' => 'required',
            'items.*.quantity' => 'required',
            'items.*.reason' => 'required',
        ];
    }
}
