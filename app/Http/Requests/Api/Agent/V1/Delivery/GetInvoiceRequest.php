<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Delivery;

use Niyotail\Http\Requests\Request;

class GetInvoiceRequest extends Request
{
    public function rules()
    {
        return [
            'reference_id' => 'required',
        ];
    }
}
