<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Store;

use Illuminate\Foundation\Http\FormRequest;
use Niyotail\Models\Agent;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'latitude'=>'required',
           'longitude'=>'required',
        ];
    }
}
