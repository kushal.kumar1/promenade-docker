<?php

namespace Niyotail\Http\Requests\Api\Agent\V1\Store;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'gstin' => 'nullable|string',
            'address' => 'required|string',
            'landmark' => 'required|string',
            'pincode' => 'required|string',
            'location' => 'required',
            'lat'=>['regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng'=>['regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'contact_person'=>'required',
            'contact_mobile'=>'required',
            'beat_id'=>'required'
        ];
    }
}
