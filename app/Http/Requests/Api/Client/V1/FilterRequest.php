<?php

namespace Niyotail\Http\Requests\Api\Client\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'start_date' => 'date_format:"Y-m-d"',
            'end_date' => 'date_format:"Y-m-d"',
        ];
    }
}
