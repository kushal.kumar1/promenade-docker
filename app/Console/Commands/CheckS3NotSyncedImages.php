<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Storage;
use Niyotail\Models\ProductImage;
use Illuminate\Http\File;

class CheckS3NotSyncedImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:s3-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to check images not in s3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $path = "media/17003/60b4a4eed6307.jpg";
        // if (!Storage::disk('s3')->exists($path)) {
        //     $image = Storage::disk('public')->get($path);
        //     Storage::disk('s3')->put($path, $image);
        // }
        // return;
        ini_set('max_execution_time', '3600');
        $productImages = ProductImage::with('product')->get();
        $i = 0;
        foreach($productImages as $productImage) {
          $i++;
          if ($productImage->product->status == 0) {
              continue;
          }
          $this->info('id: '.$productImage->id.'    $i:: '.$i);
          $path = "media/$productImage->product_id/$productImage->name";
          if (!Storage::disk('s3')->exists($path)) {
              if (!Storage::disk('public')->exists($path)) {
                  $resizedPath = "media/cache/150X/$productImage->product_id/$productImage->name";
                  if (Storage::disk('public')->exists($resizedPath)) {
                      Storage::disk('public')->delete($resizedPath);
                  }
                  $this->error('Not Found id: '.$productImage->id.'    ProductID:  '.$productImage->product_id. '  Image:: '.$productImage->name);
                  continue;
              }
              $image = Storage::disk('public')->get($path);
              Storage::disk('s3')->put($path, $image);
              $this->error('id: '.$productImage->id.'    ProductID:  '.$productImage->product_id. '  Image:: '.$productImage->name);
          }
        }
    }
}
