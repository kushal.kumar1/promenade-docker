<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Helpers\Integration\ProductSync;
use Niyotail\Models\Product;
use Niyotail\Models\ProductPriceMap;
use Niyotail\Models\Sync;

class SyncPricesToPos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync product prices to POS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Fetching ids separately and not using whereHas because of replica connection for ProductPriceMap model
        $productIdsWithPriceChange = ProductPriceMap::whereRaw('`old_sp` != cast(`new_rt_sp` as decimal(7,2))')->get()->pluck('nt_id');
        $products = Product::with('priceMap')
            ->whereIn('id', $productIdsWithPriceChange)
            ->whereDoesntHave('tags', function ($query) {
                //1k-mall tag_id = 14
                $query->where('id', 14);
            })
            ->get();

        if ($products->isNotEmpty()) {
            foreach ($products as $product) {
                try {
                    ProductSync::pushPrice($product);
                } catch (\Exception $e) {
                    \Log::error("Error occurred PID: $product->id | " . $e->getMessage());
                }
            }
        }
    }
}
