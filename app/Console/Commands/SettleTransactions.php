<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Store;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Invoice;
use Niyotail\Models\Transaction;
use Niyotail\Models\TransactionsSettlement;

class SettleTransactions extends Command
{
    protected $signature = 'transactions:settle  {stores?*}';

    protected $description = 'Command to settle old invoices';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $storeIDs = $this->argument('stores');
        $storeQuery = Store::with(['transactions' => function($query) {
                          $query->where('type', 'credit');
                        }])
                        ->whereHas('transactions', function($query) {
                          $query->where('type', 'credit');
                        });
        if (!empty($storeIDs)) {
            $storeQuery = $storeQuery->whereIn('id', $storeIDs);
        }
        $stores = $storeQuery->get();
        $startTime = time();
        var_dump('Start Time: '.$startTime);
        foreach($stores as $store) {
            var_dump('processing store_id:'.$store->id);
            $this->settleCreditNoteTransactions($store);
            $this->settleOthers($store);
            //$this->removeBouncedChequeTransactions($store);
        }
        var_dump('End Time: '.time(). '  Time Taken: '.(time()-$startTime));
    }

    private function settleCreditNoteTransactions($store) {
      $transactions = $store->transactions->where('source_type', 'credit_note');
      foreach ($transactions as $transaction) {
          $inv = [];
          $creditNote = $transaction->source;
          if (empty($creditNote)) {
              var_dump('not found credit note: '. $transaction->source_id);
              continue;
          }
          //var_dump('Store ID: '. $store->id.'  Transaction Source ID: '.$transaction->source_id. ' Credit Note: '.$creditNote->id);
          $invoice = Invoice::where('order_id', $creditNote->order_id)->where('shipment_id', $creditNote->returnOrder->shipment_id)->first();
          $sourceInvoiceTransaction = $invoice->transactions->first();
          $inv[$sourceInvoiceTransaction->id]['amount'] = $creditNote->amount;
          $inv[$sourceInvoiceTransaction->id]['created_at'] = $creditNote->created_at;
          $transaction->settledDebitTransactions()->attach($inv);
          if ($sourceInvoiceTransaction->amount == $creditNote->amount)
            $invoice->status = 'settled';
          else
            $invoice->status = 'partially_settled';
          $invoice->save();
      }
    }

    private function settleOthers($store) {
          $creditTransactions = $store->transactions->where('source_type', '!=', 'credit_note');
                                //->whereNotIn('payment_method', ['balance_transferred_winomkar', 'balance_transferred_marg']);

          //get all unsettled debit transactions
          $debitTransactions = Transaction::toBeSettled()->where('store_id', $store->id)
                                ->with('settledAmount')->get();

          foreach ($creditTransactions as $creditTransaction) {
              $creditTransaction->load('settledDebitTransactions');
              $creditAmountToSettle = $creditTransaction->amount;
              $alreadySettledDebitTransactionsAmount = 0;
              if (!empty($creditTransaction->settledDebitTransactions)) {
                $alreadySettledDebitTransactionsAmount = $creditTransaction->settledDebitTransactions->pluck('pivot')->sum('amount');
              }
              $creditAmountToSettle -= $alreadySettledDebitTransactionsAmount;
              if ($creditAmountToSettle <= 0) {
                continue;
              }
              $inv = [];
              foreach ($debitTransactions as $key => $debitTransaction) {
                  $debitTransaction->load('settledAmount');
                  $settledAmount = 0;
                  if (!empty($debitTransaction->settledAmount)) {
                    $settledAmount = $debitTransaction->settledAmount->amount;
                  }
                  $remainingAmount = $debitTransaction->amount - $settledAmount;
                  if (($remainingAmount == 0) || ($creditAmountToSettle == 0)) {
                    continue;
                  }
                  if ($remainingAmount <= $creditAmountToSettle) {
                    $inv[$debitTransaction->id]['amount'] = $remainingAmount;
                    $inv[$debitTransaction->id]['created_at'] = $creditTransaction->created_at;
                    $creditAmountToSettle -= $remainingAmount;
                    if (!empty($debitTransaction->source)) {
                      $invoice = $debitTransaction->source;
                      $invoice->status='settled';
                      $invoice->save();
                    }
                  } else {
                    $inv[$debitTransaction->id]['amount'] = $creditAmountToSettle;
                    $inv[$debitTransaction->id]['created_at'] = $creditTransaction->created_at;
                    $creditAmountToSettle = 0;
                    if (!empty($debitTransaction->source)) {
                      $invoice = $debitTransaction->source;
                      $invoice->status='partially_settled';
                      $invoice->save();
                    }
                    break;
                  }
              }
              if (count($inv) > 0) {
                  $creditTransaction->settledDebitTransactions()->attach($inv);
              }
          }
    }

    private function removeBouncedChequeTransactions($store) {
        $creditTransactions = $store->transactions->where('source_type', '!=', 'credit_note');
        $bouncedTransactions = Transaction::where('type', 'debit')->where('store_id', $store->id)
                                  ->where('payment_method', 'cheque_bounce')->where('payment_details', '!=', NULL)
                                  ->get();
        foreach($bouncedTransactions as $bouncedTransaction) {
            $correspondingChequeTransaction = $creditTransactions->where('payment_details', $bouncedTransaction->payment_details)->first();
            $correspondingChequeTransaction->load('settledDebitTransactions');
            $debitTransactions = $correspondingChequeTransaction->settledDebitTransactions;
            foreach ($debitTransactions as $debitTransaction) {
                if(!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice') {
                  $invoice = $debitTransaction->source;
                  $invoice->status = 'partially_settled';
                  $invoice->save();
                }
            }
            $debitIDs = $debitTransactions->pluck('id');
            $correspondingChequeTransaction->settledDebitTransactions()->detach($debitIDs);

            $bouncedTransaction->load('settledByCreditTransaction');
            $cbTransactions = $bouncedTransaction->settledByCreditTransaction;
            $creditIDs = $cbTransactions->pluck('id');
            $bouncedTransaction->settledByCreditTransaction()->detach($creditIDs);
        }
    }
}
