<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Niyotail\Models\Picklist;
use Niyotail\Services\PicklistService;

class GeneratePicklistPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:picklist-pdf {picklistId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate pdf for a picklist in case of any modifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $picklistId = $this->argument('picklistId');
        $picklist = Picklist::find($picklistId);
        abort_if(empty($picklist), 403, "Picklist not found");
        $this->info("Generating pdf picklist");
        App::make(PicklistService::class)->generatePdf($picklist);
    }
}
