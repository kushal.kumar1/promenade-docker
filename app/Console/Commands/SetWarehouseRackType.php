<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\WarehouseRack;
use Niyotail\Models\WarehouseRackType;

class SetWarehouseRackType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'warehouse-rack:set-type {--down}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Assign type_id in warehouse_racks ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(empty($this->option('down'))) {
            $types = WarehouseRack::selectRaw('distinct(type)')->get();
            $warehouseRackTypes = [];
            $this->info('Creating Types');
            foreach ($types as $key => $type) {
                $warehouseRackType = [
                    'type' => $type->type,
                    'position' => $key
                ];
                $warehouseRackTypes [] = $warehouseRackType;
            }
            WarehouseRackType::insert($warehouseRackTypes);
            $this->info('Types imported');
            $rackTypes = WarehouseRackType::all();
            $this->info('Inserting type ids');
            foreach ($rackTypes as $type) {
                WarehouseRack::where('type', $type->type)->update(['type_id' => $type->id]);
            }
            $this->info('Type ids inserted');
            return 0;
        }else{
            $rackTypes = WarehouseRackType::all();
            foreach ($rackTypes as $type){
                WarehouseRack::where('type_id', $type->id)->update(['type' => $type->type]);
            }
        }
        return 0;
    }
}
