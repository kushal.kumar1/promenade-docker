<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Niyotail\Models\Agent;

class CacheAgentData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:agent-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate cache for all agents dashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function clearCaches($agents)
    {
        Cache::forget("agent-dashboard-invoice-0");
        Cache::forget("agent-dashboard-return-0");
        Cache::forget("agent-dashboard-order-0");
        Cache::forget("agent-dashboard-stores-0");
        Cache::forget("agent-dashboard-focussed_products-0");
        foreach ($agents as $agent) {
          Cache::forget("agent-dashboard-invoice-$agent->id");
          Cache::forget("agent-dashboard-return-$agent->id");
          Cache::forget("agent-dashboard-order-$agent->id");
          Cache::forget("agent-dashboard-stores-$agent->id");
          Cache::forget("agent-dashboard-focussed_products-$agent->id");
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $start = time();
        // var_dump($start);
        $agents = Agent::where('type', 'sales')->where('status', 1)->get();
        $this->clearCaches($agents);

        $invoiceStats = $this->getData('invoice');
        $returnStats = $this->getData('return');
        $orderStats = $this->getData('order');
        $storeStats = $this->getData('stores');
        $fpKPIStats = $this->getData('focussed_products');
        foreach($agents as $agent) {
          $agentID = $agent->id;
          // $data = cache()->remember("agent-dashboard-invoice-$agentID", 15*60,
          //     function () use ($agentID, $invoiceStats) {
          //         return $invoiceStats->where('agent_id', $agentID);
          //     }
          //   );
          $data = $invoiceStats->where('agent_id', $agentID);
          cache(["agent-dashboard-invoice-$agentID" => $data] , 15*60);

          // $data = cache()->remember("agent-dashboard-return-$agentID", 15*60,
          //     function () use ($agentID, $returnStats) {
          //         return $returnStats->where('agent_id', $agentID);
          //     }
          //   );
          $data = $returnStats->where('agent_id', $agentID);
          cache(["agent-dashboard-return-$agentID" => $data] , 15*60);

          // $data = cache()->remember("agent-dashboard-order-$agentID", 15*60,
          //     function () use ($agentID, $orderStats) {
          //         return $orderStats->where('agent_id', $agentID);
          //     }
          //   );
          $data = $orderStats->where('agent_id', $agentID);
          cache(["agent-dashboard-order-$agentID" => $data] , 15*60);

          // $data = cache()->remember("agent-dashboard-stores-$agentID", 15*60,
          //     function () use ($agentID, $storeStats) {
          //         return $storeStats->where('agent_id', $agentID);
          //     }
          //   );
          $data = $storeStats->where('agent_id', $agentID);
          cache(["agent-dashboard-stores-$agentID" => $data] , 15*60);

          // $data = cache()->remember("agent-dashboard-focussed_products-$agentID", 15*60,
          //     function () use ($agentID, $fpKPIStats) {
          //         return $fpKPIStats->where('agent_id', $agentID);
          //     }
          //   );
          $data = $fpKPIStats->where('agent_id', $agentID);
          cache(["agent-dashboard-focussed_products-$agentID" => $data] , 15*60);
        }
        // $end = time();
        // var_dump($end);
        // var_dump($end-$start);
    }

    private function getData($type)
    {
        $select = '';
        $groupBy = '';
        $focussedJoin='';
        $focussedSelect='';
        switch ($type) {
          case 'invoice':
              $select = 'select sum(res.total) as invoice_total,
                        date(CONVERT_TZ(i.created_at,"+00:00","+05:30")) as invoice_created_at,
                        a22.id as agent_id,
                        sum(case when (res.is_focussed=1) then res.total else 0 end) as fp_total';
              $focussedSelect = ' IF(fp.id IS NULL,0,1) AS is_focussed,';
              $focussedJoin = ' LEFT JOIN focussed_products fp on fp.product_id=oi.product_id and fp.valid_on = date(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) ';
              $groupBy = ' group by agent_id, invoice_created_at having MONTH(invoice_created_at) = MONTH(CURDATE())';
            break;
          case 'return':
              $select = 'select sum(res.total) as reurn_total,
                        date(CONVERT_TZ(res.return_created_at,"+00:00","+05:30")) as return_created_at,
                        a22.id as agent_id,
                        sum(case when (res.is_focussed=1) then res.total else 0 end) as fp_total';
              $focussedSelect = ' IF(fp.id IS NULL,0,1) AS is_focussed,';
              $focussedJoin = ' LEFT JOIN focussed_products fp on fp.product_id=oi.product_id and fp.valid_on = date(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) ';
              $groupBy = ' group by agent_id, return_created_at having MONTH(return_created_at) = MONTH(CURDATE())';
            break;
          case 'order':
            $select = 'select sum(res.total) as order_total, date(CONVERT_TZ(res.order_created_at,"+00:00","+05:30")) as order_created_date, a22.id as agent_id';
            $groupBy = ' group by agent_id, order_created_date having MONTH(order_created_date) = MONTH(CURDATE())';
            break;
          case 'stores':
            $select = 'select o.store_id, res.product_id, res.order_item_id, res.sku, res.total, a22.id as agent_id, date(CONVERT_TZ(res.order_created_at,"+00:00","+05:30")) as order_created_date';
            $groupBy = ' and MONTH(date(CONVERT_TZ(res.order_created_at,"+00:00","+05:30"))) = MONTH(CURDATE())';
            break;
          case 'focussed_products':
            $select = 'select sum(res.total) as invoice_total,
                      date(CONVERT_TZ(i.created_at,"+00:00","+05:30")) as invoice_created_at,
                      a22.id as agent_id,
                      sum(case when (res.is_focussed=1) then res.total else 0 end) as fp_total';
            $focussedSelect = ' IF(fp.id IS NULL,0,1) AS is_focussed,';
            $focussedJoin = ' LEFT JOIN focussed_products fp on fp.product_id=oi.product_id and fp.valid_on = date(CONVERT_TZ(oi.created_at,"+00:00","+05:30")) ';
            $groupBy = ' group by agent_id, invoice_created_at having MONTH(invoice_created_at) = MONTH(CURDATE())';
            break;
        }
        $query =  ' FROM
                  (SELECT oi.order_id, oi.id AS order_item_id, oi.product_id, oi.sku, oi.mrp, oi.quantity, oi.total,
                  IF(roi.id IS NULL,oi.status,"returned") AS oi_final_status,'.$focussedSelect.'
                  oi.created_at AS order_created_at,
                  roi.return_order_id,
                  roi.id AS return_order_item_id,
                  roi.created_at AS return_created_at,
                  cn.reference_id AS cn_ref_id
                  FROM order_items oi LEFT JOIN return_order_items roi ON roi.order_item_id=oi.id
                  LEFT JOIN credit_notes cn ON cn.return_order_id=roi.return_order_id'.$focussedJoin .'
                  ) AS res
                  JOIN
                  (SELECT ord_cart2.*, a.id AS agent_id, a.name AS agent_name
                  FROM
                  (SELECT o2.id AS order_id2, c2.id AS cart_id, s2.id AS store_id2, s2.name AS store_name2,
                  c2.created_by_type AS cart_created_by_type, c2.created_by_id AS cart_created_by_id,
                  o2.source AS order_source, o2.created_by_type AS order_created_by_type, o2.created_by_id AS order_created_by_id,
                  IF(o2.source="agent_app","agent-app",
                  IF(o2.source="system","backend",
                  IF(o2.source="app",
                  IF(o2.created_by_type="user","app-user",
                  IF(s2.name LIKE "%Agent%","app-agent-hack","app-user-assisted")),NULL))) AS allocation
                  FROM orders o2, carts c2, stores s2
                  WHERE o2.id=c2.order_id
                  AND c2.store_id=s2.id) AS ord_cart2
                  LEFT JOIN agents a ON a.id=ord_cart2.order_created_by_id) AS ord_cart
                  on res.order_id=ord_cart.order_id2
                  JOIN orders o on o.id=res.order_id
                  JOIN shipments sh on sh.order_id=o.id
                  JOIN shipment_items shi on shi.order_item_id=res.order_item_id and sh.id=shi.shipment_id
                  JOIN invoices i on i.shipment_id=sh.id and i.order_id=o.id
                  JOIN stores s on o.store_id=s.id
                  JOIN beats b on s.beat_id=b.id
                  JOIN product_variants pv on res.sku=pv.sku AND res.product_id=pv.product_id
                  JOIN products p on res.product_id=p.id
                  JOIN brands br on p.brand_id=br.id
                  JOIN marketers m on br.marketer_id=m.id
                  LEFT JOIN agent_beat_mktr_map abmm ON abmm.beat_id=b.id AND abmm.marketer_id=m.id
                  LEFT JOIN agents a22 ON a22.id=abmm.agent_id
                  LEFT JOIN (select order_item_id, SUM(order_item_inventories.quantity) AS cost_qty, SUM(order_item_inventories.quantity * order_item_inventories.unit_cost_price) as cost_price from order_item_inventories, order_items where order_item_inventories.order_item_id = order_items.id group by order_item_id) oii
                  ON oii.order_item_id=res.order_item_id
                  WHERE
                  i.status!="cancelled"
                  AND sh.status!="cancelled"
                  AND ord_cart.order_id2=o.id
                  AND
                  ((MONTH(CONVERT_TZ(i.created_at,"+00:00","+05:30"))=MONTH(CURDATE()) AND (res.return_created_at IS NULL OR MONTH(CONVERT_TZ(res.return_created_at,"+00:00","+05:30"))>MONTH(CURDATE())))
                  OR (MONTH(CONVERT_TZ(res.return_created_at,"+00:00","+05:30"))=MONTH(CURDATE()) AND MONTH(CONVERT_TZ(i.created_at,"+00:00","+05:30"))<MONTH(CURDATE()))) ';

            $data = DB::select(
                    DB::raw(
                      $select.$query.$groupBy
                    )
                  );
          // $data = cache()->remember("agent-dashboard-$type-0", 15*60, function () use ($select, $query, $groupBy) {
          //           return DB::select(
          //                   DB::raw(
          //                     $select.$query.$groupBy
          //                   )
          //                 );
          //         });
          cache(["agent-dashboard-$type-0" => $data] , 15*60);
          return collect($data);
    }
}
