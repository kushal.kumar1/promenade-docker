<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Picklist;
use Niyotail\Services\PicklistService;

class RegeneratePicklistPdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'picklist:regenerate {picklistID}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate Picklist PDF';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $picklistId = $this->argument('picklistID');
        $this->info('Regenerating Picklist PDF..');
        if (empty($picklistId)) {
            $this->error('Please provide picklist id.');
            return 0;
        }
        $picklist = Picklist::find($picklistId);
        if (empty($picklist)) {
            $this->error('Invalid Picklist Id');
            return 0;
        }
        $path = "$picklist->id.pdf";
        if (Storage::disk('picklist')->exists($path)) {
            Storage::disk('picklist')->delete($path);
        }
        $service = new PicklistService();
        $service->generatePdf($picklist);
        $this->comment("\n" . "Picklist PDF regenerated!!");
        return 1;
    }
}
