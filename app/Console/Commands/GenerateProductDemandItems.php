<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Niyotail\Services\ProductDemandService;

class GenerateProductDemandItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:product-demand-items {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate pdf for a picklist in case of any modifications';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        Log::info("This $id");
        (new ProductDemandService())->processInitiatedDemands($id);
    }
}