<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Store;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Invoice;
use Niyotail\Helpers\InvoiceHelper;

class SettleInvoices extends Command
{
    protected $signature = 'invoices:settle';

    protected $description = 'Command to settle old invoices';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        // $stores = Store::with(['invoices' => function($query) {
        //                   $query->orderBy('created_at');
        //                 }])
        //                 ->with(['transactions' => function($query) {
        //                   $query->orderBy('created_at');
        //                 }])->whereHas('transactions')
        //                 ->get();
        // \DB::enableQueryLog();
        $stores = Store::with(['transactions' => function($query) {
                          $query->doesntHave('invoices')
                                ->where('source_type', 'credit_note')
                                ->orderBy('created_at');
                        }])
                        ->whereHas('transactions', function($query) {
                          $query->doesntHave('invoices')
                                ->where('source_type', 'credit_note')
                                ->orderBy('created_at');
                        })
                        ->get();
        // dd(\DB::getQueryLog(), $stores->pluck('transactions')->count());
        $i = 0;
        foreach($stores as $store) {
            // var_dump('processing store_id:'.$store->id);
            // $this->settleWinomkar($store);
            // $this->settleMarg($store);
            // $this->settleOthers($store);
            // $i += $store->transactions->count();
            // var_dump('Store ID:'. $store->id .'  id: '.$i);
            $this->settleCreditNoteTransactions($store);
        }
    }

    private function settleCreditNoteTransactions($store) {
      foreach ($store->transactions as $transaction) {
          $creditNote = $transaction->source;
          var_dump('Store ID: '. $store->id.'  Transaction Source ID: '.$transaction->source_id. ' Credit Note: '.$creditNote->id);
          $invoice = Invoice::where('order_id', $creditNote->order_id)->where('shipment_id', $creditNote->returnOrder->shipment_id)->first();
          $inv[$invoice->id]['amount'] = $creditNote->amount;
          $transaction->invoices()->attach($inv);
          if ($invoice->amount == $creditNote->amount)
            $invoice->status = 'settled';
          else
            $invoice->status = 'partially_settled';
          $invoice->save();
      }
    }

    private function settleOthers($store) {
          $store->load('transactions');
          $creditTransactions = $store->transactions->whereNotIn('payment_method', ['balance_transferred_winomkar', 'balance_transferred_marg'])
                                ->where('type', 'credit');
          foreach ($creditTransactions as $creditTransaction) {
              $creditTransaction->load('invoices');
              $settledCreditAmount = $creditTransaction->invoices->pluck('pivot')->sum('amount');
              if ($settledCreditAmount == $creditTransaction->amount) {
                  continue;
              }
              $store->load('invoices');
              $invoices = $store->invoices->where('status', '!=', 'settled');

              $creditAmount = $creditTransaction->amount - $settledCreditAmount;
              if ($creditAmount <= 0) {
                  continue;
              }
              $inv = [];
              foreach ($invoices as $key => $invoice) {
                  $invoice->load('invoiceTransactions');
                  $settledAmount = $invoice->invoiceTransactions->pluck('pivot')->sum('amount');
                  $remainingAmount = $invoice->amount - $settledAmount;
                  if ($remainingAmount <= $creditAmount) {
                    $inv[$invoice->id]['amount'] = $remainingAmount;
                    $creditAmount -= $remainingAmount;
                    //$invoices->forget($invoice->id);
                    $invoice->status='settled';
                    $invoice->save();
                    $invoices = $invoices->map(function ($c) use($key) {
                        return collect($c)->forget($key);
                    });
                  } else if($remainingAmount != 0) {
                    $invoice->status='partially_settled';
                    $invoice->save();
                    $inv[$invoice->id]['amount'] = $creditAmount;
                    break;
                  }
              }
              if (count($inv) > 0) {
                  $creditTransaction->invoices()->attach($inv);
              }
          }
    }

    private function settleWinomkar($store) {
        $paymentMethod = $store->transactions->where('payment_method', 'balance_transferred_winomkar');
        if ($paymentMethod->isNotEmpty()) {
            $amount = $paymentMethod->where('type', 'debit')->sum('amount') - $paymentMethod->where('type', 'credit')->sum('amount');
            if ($amount > 0) {
                $store->load('transactions');
                $creditTransactions = $store->transactions->where('payment_method', '!=', 'balance_transferred_winomkar')->where('type', 'credit');
                foreach ($creditTransactions as $creditTransaction) {
                    $creditTransaction->load('invoices');
                    $settledCreditAmount = $creditTransaction->invoices->pluck('pivot')->sum('amount');
                    if ($settledCreditAmount == $creditTransaction->amount) {
                        continue;
                    }
                    $store->load('invoices');
                    $invoices = $store->invoices->where('status', '!=', 'settled');
                    if ($amount <= 0) {
                      break;
                    }
                    $creditAmount = min($creditTransaction->amount - $settledCreditAmount, $amount);
                    $inv = [];
                    foreach ($invoices as $key => $invoice) {
                        $invoice->load('invoiceTransactions');
                        $settledAmount = $invoice->invoiceTransactions->pluck('pivot')->sum('amount');
                        $remainingAmount = $invoice->amount - $settledAmount;
                        if ($remainingAmount <= $creditAmount) {
                          $inv[$invoice->id]['amount'] = $remainingAmount;
                          $creditAmount -= $remainingAmount;
                          //$invoices->forget($invoice->id);
                          $invoice->status='settled';
                          $invoice->save();
                          $invoices = $invoices->map(function ($c) use($key) {
                              return collect($c)->forget($key);
                          });
                        } else {
                          $invoice->status='partially_settled';
                          $invoice->save();
                          $inv[$invoice->id]['amount'] = $creditAmount;
                          break;
                        }
                    }
                    if (count($inv) > 0) {
                      $creditTransaction->invoices()->attach($inv);
                    }
                    $amount -= $creditTransaction->amount;
                }
            }
        }
    }

    private function settleMarg($store) {
        $paymentMethod = $store->transactions->where('payment_method', 'balance_transferred_marg');
        if ($paymentMethod->isNotEmpty()) {
            $amount = $paymentMethod->where('type', 'debit')->sum('amount') - $paymentMethod->where('type', 'credit')->sum('amount');
            if ($amount > 0) {
                $store->load('transactions');
                $creditTransactions = $store->transactions->where('payment_method', '!=', 'balance_transferred_marg')->where('type', 'credit');
                foreach ($creditTransactions as $creditTransaction) {
                    $creditTransaction->load('invoices');
                    $settledCreditAmount = $creditTransaction->invoices->pluck('pivot')->sum('amount');
                    if ($settledCreditAmount == $creditTransaction->amount) {
                        continue;
                    }
                    $store->load('invoices');
                    $invoices = $store->invoices->where('status', '!=', 'settled');
                    if ($amount <= 0) {
                      break;
                    }
                    $creditAmount = min($creditTransaction->amount - $settledCreditAmount, $amount);
                    $inv = [];
                    foreach ($invoices as $key => $invoice) {
                        $invoice->load('invoiceTransactions');
                        $settledAmount = $invoice->invoiceTransactions->pluck('pivot')->sum('amount');
                        $remainingAmount = $invoice->amount - $settledAmount;
                        if ($remainingAmount <= $creditAmount) {
                          $inv[$invoice->id]['amount'] = $remainingAmount;
                          $creditAmount -= $remainingAmount;
                          //$invoices->forget($invoice->id);
                          $invoice->status='settled';
                          $invoice->save();
                          $invoices = $invoices->map(function ($c) use($key) {
                              return collect($c)->forget($key);
                          });
                        } else {
                          $invoice->status='partially_settled';
                          $invoice->save();
                          $inv[$invoice->id]['amount'] = $creditAmount;
                          break;
                        }
                    }
                    if (count($inv) > 0) {
                      $creditTransaction->invoices()->attach($inv);
                    }
                    $amount -= $creditTransaction->amount;
                }
            }
        }
    }
}
