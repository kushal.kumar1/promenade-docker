<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class EnableMultiWarehouse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'multiwarehouse:enable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs necessary migrations and db seeds when enabling multi-warehouse';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Running migrations...');
        Artisan::call('migrate --path=/database/migrations/2021_10_21_055111_add_warehouse_id_in_picklists_table.php');
        Artisan::call('migrate --path=/database/migrations/2021_10_21_114406_add_type_and_parent_id_field_in_warehouses_table.php');
        Artisan::call('migrate --path=/database/migrations/2021_10_25_125243_add_warehouse_id_field_in_manifests_table.php');
        Artisan::call('migrate --path=/database/migrations/2021_10_29_090539_add_warehouse_id_in_shipments_table.php');
        Artisan::call('migrate --path=/database/migrations/2021_10_30_080305_add_warehouse_id_in_purchase_invoice_grn.php');
        Artisan::call('migrate --path=/database/migrations/2021_10_30_080836_add_warehouse_id_in_purchase_invoice_debit_notes.php');
        $this->info('Seeding tables...');
        Artisan::call('db:seed --class=RunMultiWarehouseSeeds');
        return 0;
    }
}
