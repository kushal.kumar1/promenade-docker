<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\StockTransfer;
use Niyotail\Models\StockTransferItem;

class VerifyStockTransferItemsInvQuantity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify-stock-transfer:inventories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifies the flat-inventories created in Kundli are matching or not!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Generating results...");
        $headers = ['Product ID', 'Product Name', 'Received Quantity', 'Flat Inventory Count'];
        $stockTransferItems = StockTransferItem::whereHas('stockTransfer', function ($query) {
            $query->where('status', StockTransfer::STATUS_COMPLETED);
        })->with('product')->get();

        $data = collect([]);
        $startTime = now();
        $data->push([
            'product_id' => '-',
            'product_name' => '-',
            'received_quantity' =>'-',
            'flat_inventory_count' => '-'
        ]);
        
        foreach ($stockTransferItems as $item) {
            $flatInventoryCount = FlatInventory::where('source_type', 'stock_transfer_item')
                ->where('source_id', $item->id)->count();
            if ($item->recieved_quantiy > $flatInventoryCount) {
                $data->push([
                    'product_id' => $item->product_id,
                    'product_name' => $item->product->name,
                    'received_quantity' => $item->received_quantity,
                    'flat_inventory_count' => $flatInventoryCount
                ]);
            }
        }

        $this->table($headers, $data->toArray());
        $timeTaken = $startTime->diffInSeconds(now());
        $this->info("\n" . "Processed in $timeTaken seconds!");
        return true;
    }
}
