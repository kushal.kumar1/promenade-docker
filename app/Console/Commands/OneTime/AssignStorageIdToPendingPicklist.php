<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;

class AssignStorageIdToPendingPicklist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'picklist:set-storages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add storage id to pending picklists. After release of picker app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $picklistItems = PicklistItem::whereHas('picklist', function ($itemQuery) {
            $itemQuery->where('status', Picklist::STATUS_GENERATED)
                ->where('warehouse_id', 3);
        })->whereNull('storage_id')->with('orderItemInventory.inventory')->get();

        $bar = $this->output->createProgressBar($picklistItems->count());
        $bar->start();

        foreach ($picklistItems as $picklistItem) {
            if(!empty($picklistItem->orderItemInventory->inventory)){
                $picklistItem->storage_id = $picklistItem->orderItemInventory->inventory->storage_id;
                $picklistItem->save();
            }
            $bar->advance();
        }
        $bar->finish();
        return 0;
    }
}
