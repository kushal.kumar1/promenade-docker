<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\ReturnOrder;
use Niyotail\Services\ReturnOrder\ReturnOrderService;

class UpdateReturnOrderRefId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:return-ref-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $returnOrders = ReturnOrder::selectRaw("date(convert_tz(return_orders.created_at, '+00:00','+05:30')) as return_date, 
            orders.store_id, group_concat(return_orders.id) as return_order_ids")
            ->leftJoin('orders', 'return_orders.order_id', '=', 'orders.id')
            ->groupBy('orders.store_id','return_date')
            ->get();

        $progressBar = $this->output->createProgressBar($returnOrders->count());
        $progressBar->start();
        $service = new ReturnOrderService();

        foreach ($returnOrders as $returnOrder) {
            $refId = $service->generateReturnUniqueReferenceId($returnOrder->store_id, $returnOrder->return_date);
            ReturnOrder::whereIn('id', explode(',',$returnOrder->return_order_ids))->update(['reference_id' => $refId]);
            $progressBar->advance();
        }
        $progressBar->finish();
    }
}
