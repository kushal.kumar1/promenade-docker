<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\StoreOrder\StoreOrderService;

class RetryUnfulfilledStoreOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store-order:retry {storeOrderId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retry store order for external store purchases unfulfilled due to credit limit exceeded exception!';


    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws ServiceException
     */
    public function handle()
    {
        $whereCondition = [
            'status' => StoreOrder::STATUS_UNFULFILLED,
            'source' => StoreOrder::SOURCE_STORE_PURCHASE,
            'cancellation_reason' => "Credit limit exceeded!"
        ];
        $storeOrderId = $this->argument('storeOrderId');
        if(!empty($storeOrderId)){
            $storeOrders = StoreOrder::where($whereCondition)->find($storeOrderId);
        }else{
            $storeOrders = StoreOrder::where($whereCondition)->get();
        }
        $bar = $this->output->createProgressBar($storeOrders->count());
        $bar->start();
        $storeOrderService = new StoreOrderService();
        foreach ($storeOrders as $storeOrder){
            try{
                $storeOrderService->retryUnFulfilledOrder($storeOrder);
            }catch (\Exception $exception){
                Log::error($exception->getMessage());
                $this->error($exception->getMessage());
            }
            sleep(5);
            $bar->advance();
        }
        $bar->finish();
        return;
    }
}
