<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\StockTransferItem;
use Niyotail\Services\FlatInventoryService;

class CreateOrUpdateFlatInventoryForStockTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock-transfer-item:update {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create/Update Flat inventory for a given stock transfer item id.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('id');
        $flatInventoryService = new FlatInventoryService();
        if(empty($id)){
            $this->error('Please provide transfer item id! e.g stock-transfer-item:update 596');
            return 0;
        }
        $stockTransferItem = StockTransferItem::with('stockTransfer')->find($id);

        if(empty($stockTransferItem)){
            $this->error('Invalid transfer item id!');
            return 0;
        }

        if($stockTransferItem->stockTransfer->id > 57){
            $stockTransferItem->loadMissing('flatInventories');
            $inventoriesToBeTransferred = $stockTransferItem->flatInventories->take($stockTransferItem->received_quantity)->pluck('id')->toArray();
            $flatInventoryService->completeInventoryTransfer($inventoriesToBeTransferred, $stockTransferItem->stockTransfer->to_warehouse_id);
            $this->info('Completed the inventory transfer');
            return 0;
        }else{
            $flatInventoryService->create($stockTransferItem, $stockTransferItem->stockTransfer->to_warehouse_id, $stockTransferItem->received_quantity);
            $this->info("Created new flat inventory for stock transfer item id #$id");
        }
        return 0;
    }
}
