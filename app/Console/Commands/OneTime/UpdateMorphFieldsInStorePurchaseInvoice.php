<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\StorePurchaseInvoice;

class UpdateMorphFieldsInStorePurchaseInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store-purchase-invoice:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update store purchase invoices table morph relation';


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $storePurchaseInvoices = StorePurchaseInvoice::whereNotNull('assigned_id')
            ->get();

        $bar = $this->output->createProgressBar($storePurchaseInvoices->count());
        $bar->start();
        foreach ($storePurchaseInvoices as $storePurchaseInvoice){
            $storeOrder = StoreOrder::find($storePurchaseInvoice->assigned_id);
            $storePurchaseInvoice->assignedOrder()->associate($storeOrder);
            $storePurchaseInvoice->save();
            $bar->advance();
        }
        $bar->finish();
        return 0;
    }
}
