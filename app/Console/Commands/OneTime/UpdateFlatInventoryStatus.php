<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\FlatInventory;

class UpdateFlatInventoryStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-inventory:status {productId : Product ID} {oldStatus : Existing inventory status} {newStatus : New Inventory status} {warehouseId : Warehouse Id of inventory} {quantity : Total Quantity to be updated} {mfd? : Manufacturing date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update flat inventory status for given product id';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $whereCondition = [
            'product_id' => $arguments['productId'],
            'status' => $arguments['oldStatus'],
            'warehouse_id' => $arguments['warehouseId']
        ];

        if (!array_key_exists($arguments['oldStatus'], FlatInventory::getConstants('STATUS'))) {
            $this->error("Invalid inventory status {$arguments['oldStatus']}");
            return 0;
        }

        if (!array_key_exists($arguments['newStatus'], FlatInventory::getConstants('STATUS'))) {
            $this->error("Invalid inventory status {$arguments['newStatus']}");
            return 0;
        }

        $flatInventories = null;
        if(!empty($arguments['mfd'])){
            $flatInventories = FlatInventory::where($whereCondition)->where('manufacturing_date', $arguments['mfd'])
                ->limit($arguments['quantity'])->get();
        }


        if (!empty($flatInventories)) {
            $flatInventories = FlatInventory::where($whereCondition)
                ->limit($arguments['quantity'])->get();
            if (empty($flatInventories) || $flatInventories->isEmpty()) {
                $this->error('No Inventories found!');
                return 0;
            }
        }

        if ($flatInventories->count() < $arguments['quantity']) {
            $this->error("Requested quantity not available only {$flatInventories->count()} found!");
            return 0;
        }

        FlatInventory::whereIn('id', $flatInventories->pluck('id')->toArray())
            ->update(['status' => $arguments['newStatus']]);
        $this->info('DONE!');
        return 0;
    }
}
