<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Services\FlatInventoryService;

class GenerateFlatInventoryForOldOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flat-inventory:old-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Flat Inventory for old orders';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $orders = Order::with('items.inventories.inventory')
            ->whereNotIn('status',[Order::STATUS_CANCELLED])
            ->where('warehouse_id', 2)
            ->whereRaw("date(convert_tz(created_at,'+00:00','+05:30')) >= '2021-12-01'")
            ->whereRaw("date(convert_tz(created_at,'+00:00','+05:30')) < '2021-12-10'");

        $flatInventoryService = new FlatInventoryService();
        $orders->chunkById(100, function($orders) use($flatInventoryService) {
            foreach ($orders as $order){
                $this->comment("Generating flat inventory for $order->reference_id");
                $items = $order->items->whereNotIn('status',[OrderItem::STATUS_CANCELLED, OrderItem::STATUS_RETURNED, OrderItem::STATUS_RTO, OrderItem::STATUS_UNFULFILLED]);
                foreach ($items as $orderItem){
                    $flatInventoryService->generateSoldInventories($orderItem);
                }
            }
        });
        $this->info("Completed");
    }
}
