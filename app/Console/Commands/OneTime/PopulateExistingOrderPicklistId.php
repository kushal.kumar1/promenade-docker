<?php

namespace Niyotail\Console\Commands\OneTime;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Niyotail\Models\Order;
use Niyotail\Models\OrderPicklist;
use Niyotail\Models\Picklist;
use Niyotail\Services\PicklistService;

class PopulateExistingOrderPicklistId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order-picklist:populate-old-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to populate order_picklist table using old orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::with(['picklistItems'=>function($picklistQuery){
            $picklistQuery->select('picklist_id');
        }])->whereNotIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_MANIFESTING])
            ->whereDate('created_at', '>=', Carbon::today()->subDays(3)->toDateString())
            ->select('id')
            ->get();
        $picklistService = new PicklistService();
        if ($orders->isNotEmpty()) {
            $groupedByPicklistId = $orders->groupBy('picklistItems.*.picklist_id');
            foreach ($groupedByPicklistId as $picklistId=> $orders) {
                $picklist = Picklist::find($picklistId);
                $orderIds = $orders->pluck('id')->toArray();
                $picklistService->addOrdersInPicklist($orderIds, $picklist);
            }
        } else {
            $this->error("No Orders to Populate");
        }

    }

}
