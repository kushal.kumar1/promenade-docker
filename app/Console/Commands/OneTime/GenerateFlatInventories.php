<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Inventory;
use Niyotail\Services\FlatInventoryService;

class GenerateFlatInventories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:flat-inventories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate flat inventories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $flatInventoryService = new FlatInventoryService();
        $inventoryQuery = Inventory::with('grnItem')
            ->where('quantity', '>', 0)
            ->where('status', 1)
            ->whereNotNull('grn_item_id');
        $inventoryQuery->chunkById(1000, function ($inventories) use ($flatInventoryService) {
            foreach ($inventories as $inventory) {
                $this->info("Generating flat inventory for $inventory->id, Qty: $inventory->quantity");
                $flatInventoryService->create($inventory->grnItem, $inventory->warehouse_id, $inventory->quantity);
            }
        });
    }
}
