<?php

namespace Niyotail\Console\Commands\OneTime;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Order;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\StorePurchaseInvoice;

class AssociateExternalSaleOrderWithInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'associate-invoices:sale-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Associate the purchase invoices not associated yet with orders';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataToBeMapped = DB::table('temp_pi_sale_order')->groupBy(['pi_ref', 'order_ref'])
            ->get();

        $notFound = [];
        $alreadyExists = [];
        $storePurchaseInvoices = [];
        foreach ($dataToBeMapped as $data) {
            $purchaseInvoice = PurchaseInvoice::select('id')->with('storePurchaseInvoice')
                ->where('reference_id', $data->pi_ref)->first();
            $saleOrder = Order::where('reference_id', $data->order_ref)->select('id')->first();
            if (!empty($saleOrder)) {
                if (empty($purchaseInvoice)) {
                    $notFound [] = $data->pi_ref;
                    continue;
                }
                if (!empty($purchaseInvoice->storePurchaseInvoice)) {
                    $alreadyExists [] = $data->pi_ref;
                    continue;
                }
                $storePurchaseInvoices [] = [
                    'purchase_invoice_id' => $purchaseInvoice->id,
                    'store_id' => $data->store_id,
                    'assigned_type' => 'order',
                    'assigned_id' => $saleOrder->id
                ];
            }else{
                $notFound [] = $data->pi_ref;
            }
        }
        \Log::info('not found', $notFound);
        \Log::info('already exists', $alreadyExists);
        StorePurchaseInvoice::insert($storePurchaseInvoices);
    }
}
