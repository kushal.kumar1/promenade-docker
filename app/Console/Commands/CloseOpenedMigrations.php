<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Services\InventoryMigrationService;
use Niyotail\Services\Putaway\PutawayService;

class CloseOpenedMigrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close:migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close migrations which are eligible for closing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->closeOpenPutaways();

        $migrations = InventoryMigration::where('status', InventoryMigration::STATUS_OPEN)->get();
        $progressBar = $this->output->createProgressBar($migrations->count());
        $progressBar->start();
        $service = new InventoryMigrationService();
        foreach($migrations as $migration) {
            if($migration->canClose()) {
                $service->close($migration);
            }
            $progressBar->advance();
        }
        $progressBar->finish();

    }

    private function closeOpenPutaways()
    {
        $putaways = Putaway::with('items')->whereRaw('created_at < DATE_SUB(NOW(), INTERVAL 3 HOUR)')
                            ->where('status', Putaway::STATUS_STARTED)->get();

        $progressBar = $this->output->createProgressBar($putaways->count());
        $progressBar->start();
        $service = new PutawayService();
        foreach ($putaways as $putaway) {
            if($putaway->canClose()) {
                $service->close($putaway);
            }
            $progressBar->advance();
        }
        $progressBar->finish();
    }
}
