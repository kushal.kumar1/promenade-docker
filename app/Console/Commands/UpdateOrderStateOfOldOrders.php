<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Order;
use Niyotail\Models\Shipment;
use Niyotail\Services\Order\OrderService;

class UpdateOrderStateOfOldOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order-status:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates Order to its final states.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $shipments = Shipment::where('status', Shipment::STATUS_DISPATCHED)
//            ->whereHas('order', function ($orderQuery){
//               $orderQuery->where('status', Order::STATUS_BILLED);
//            })->get();
//
//        $bar = $this->output->createProgressBar($shipments->count());
//        $bar->start();
//        $orderService = app(OrderService::class);
//        $orderItemService = new OrderItemService();
//        $chunkedShipments = $shipments->chunk(10);
//        foreach ($chunkedShipments as $groupShipments) {
//            foreach ($groupShipments as $shipment) {
//                $shipment->loadMissing('items');
//                $orderService->orderInTransit($shipment->order);
//                $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_DISPATCHED);
//            }
//            $bar->advance($groupShipments->count());
//        }
//        $bar->finish();

        $orders = Order::where('status', Order::STATUS_BILLED)
            ->whereHas('shipments', function ($shipmentQuery) {
                $shipmentQuery->where('status', Shipment::STATUS_CANCELLED);
            })->get();
        $orderService = app(OrderService::class);
        foreach ($orders as $order) {
            $orderService->afterShipmentUpdate($order);
        }
        return 0;
    }
}
