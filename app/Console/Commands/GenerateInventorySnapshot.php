<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Inventory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class GenerateInventorySnapshot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inventory:snapshot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate dump for inventory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inventories = Inventory::get();
        if (empty($inventories)) {
          $this->info('Inventory data not available');
          return;
        }
        $dateTime = Carbon::now('Asia/Kolkata')->format('Y-m-d-H-i-s');
        $csv = storage_path('app')."/inventory/".$dateTime.".csv";
        if (file_exists($csv)) {
            unlink($csv);
        }
        $file = fopen($csv, "w+");
        fputcsv($file, array('inventory_id', 'product_id', 'warehouse_id','quanity', 'batch_number', 'unit_cost_price', 'created_at','updated_at'));
        foreach ($inventories as $inventory) {
            try {
                  fputcsv($file, array($inventory->id,
                      $inventory->product_id,
                      $inventory->warehouse_id,
                      $inventory->quantity,
                      $inventory->batch_number,
                      $inventory->unit_cost_price,
                      $inventory->created_at,
                      $inventory->updated_at)
                  );
              } catch (\Exception $e) {
                  $this->error($e->getMessage());
              }
        }
        fclose($file);
        $this->info('Inventory Snapshot ready.');
    }
}
