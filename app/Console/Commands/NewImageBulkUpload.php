<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Product;
use Niyotail\Models\ProductImage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class NewImageBulkUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:bulk-upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Temp command to upload bulk images from the directory path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
          $failPath = "upload/error/";
          $directories = Storage::disk('public')->directories('upload');
          foreach($directories as $directory) {
              if (in_array($directory, ['upload/error']) ) {
                continue;
              }
              $files = Storage::disk('public')->files($directory);
              $productId = Str::after($directory, '/');
              foreach($files as $file) {
                    // $product = DB::connection('mysql_replica')->select(DB::raw("select pp.id as product_id, pv.vendor_product_identifier as nt_product_id
                    //                   from pos.products pp, pos.product_vendor pv where pp.id = pv.product_id and pv.product_id = $posProductId"));
                    // if(empty($product) && empty($product[0])){
                    //     $this->error("Product doesn't exist for $file with $productId");
                    //     continue;
                    // }
                    // $productId = $product[0]->nt_product_id;

                    $product = Product::find($productId);
                    if(empty($product)) {
                        $this->error("Product doesn't exist for $file with $productId");
                        continue;
                    }

                    //as per naming convention of the file
                    $extension = Str::after($file, '.');
                    try {
                        $fileName = uniqid().".".$extension;
                        $filePath = "media/" . $productId . "/" . $fileName;
                        $image = Storage::disk('public')->get($file);
                        Storage::disk('s3')->put($filePath, $image);

                        $productImage = new ProductImage();
                        $productImage->product_id = $productId;
                        $productImage->name = $fileName;
                        $productImage->save();
                        //move image to success directory
                        Storage::disk('public')->delete($file);
                        $this->info('ProductID:  '.$productId.' Success File: '.$file);
                    } catch(Exception $e){
                        //move image to error directory
                        Storage::disk('upload')->move($file, str_replace("upload/", $failPath, $file));
                        $this->error('ProductID:  '.$productId.'  Moving Error File: '. str_replace("upload/", $failPath, $file));
                    }
              } //close foreach files
          } //close foreach directories

          foreach($directories as $directory) {
              if (in_array($directory, ['upload/error']) ) {
                continue;
              }
              $files = Storage::disk('public')->allFiles($directory);
              if (empty($files)) {
                  Storage::disk('public')->deleteDirectory($directory);
              }
          }
      }
}
