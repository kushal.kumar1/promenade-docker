<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;
use Niyotail\Services\PicklistService;

class ProcessPicklist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'picklist:process {picklistId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Picklists realtime';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws ServiceException
     */
    public function handle()
    {
        $id = $this->argument('picklistId');
        $picklist = Picklist::find($id);
        if (empty($picklist)) {
            $this->error('Invalid Picklist Id');
            return 0;
        }
        if (!in_array($picklist->status, [Picklist::STATUS_CLOSING, Picklist::STATUS_PROCESSING])) {
            $this->error('Picklist already processed!');
            return 0;
        }
        $picklistService = (new PicklistService());
        if ($picklist->status == Picklist::STATUS_PROCESSING) {
            $orderItems = $picklist->orderItems;
            $orderService = app(OrderService::class);
            $orderItemService = app(OrderItemService::class);
            $orderService->manifestOrders($orderItems->pluck('order_id')->unique()->toArray());
            $orderItemService->massStatusUpdate($orderItems->pluck('id')->toArray(), OrderItem::STATUS_MANIFESTED);
            $picklistService->updatePicklistStatus($picklist, Picklist::STATUS_GENERATED);
        } else {
            $picklist->items->loadMissing('orderItemInventory');
            $skippedItems = $picklist->items->where('status', PicklistItem::STATUS_SKIPPED)->groupBy('skip_reason');
            $orderItemService = new OrderItemService();
            foreach ($skippedItems as $skipReason => $items) {
                $orderItemIds = $items->pluck('order_item_id')->toArray();
                $orderItemService->massStatusUpdate($orderItemIds, OrderItem::STATUS_UNFULFILLED, $skipReason);
                $inventoryIds = $items->pluck('orderItemInventory.flat_inventory_id')->toArray();
                (new FlatInventoryService())->markBadInventories($inventoryIds, PicklistItem::INVENTORY_MAP_SKIP_REASONS[$skipReason]);
            }
            $orderIds = $picklist->orderItems->unique('order_id')->pluck('order_id')->toArray();
            $orders = Order::with('picklistItems')->whereIn('id', $orderIds)->get();
            if ($orders->isNotEmpty()) {
                foreach ($orders as $order) {
                    $shipmentService = new ShipmentService();
                    $shipmentService->createFromPicklist($picklist, $order);

                    //Update Order Status
                    app(OrderService::class)->afterPicklistUpdate($order);
                }
            }
            $picklistService->updatePicklistStatus($picklist, Picklist::STATUS_CLOSED);
        }
        return 0;
    }
}
