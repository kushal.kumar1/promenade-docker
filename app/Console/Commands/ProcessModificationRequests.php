<?php

namespace Niyotail\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\File;
use Niyotail\Models\ModificationRequest;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\PurchaseInvoiceItemModificationRequest;
use Niyotail\Models\PurchaseInvoiceModificationRequest;
use Niyotail\Models\Transaction;
use Niyotail\Models\TransactionDeletionRequest;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Services\ModificationRequestService;
use Niyotail\Services\PurchaseInvoiceService;
use PhpParser\Error;
use \File as BaseFile;

class ProcessModificationRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'modification-requests:process {modificationRequestType?} {modificationRequestId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process Modification Requests';


    /**
     * @throws Exception
     */
    public function handle()
    {
        $modificationRequestType = $this->argument('modificationRequestType');
        $modificationRequestId = $this->argument('modificationRequestId');

        $transactionDeletionRequests = null;
        $purchaseInvoiceModificationRequests = null;
        $purchaseInvoiceItemModificationRequests = null;

        if (!empty($modificationRequestType)) {
            if (!ModificationRequest::isValidType($modificationRequestType)) {
                $this->error('Invalid modificationRequestType!');
                return 1;
            }

            if (!empty($modificationRequestId)) {
                $modificationRequest = null;
                switch ($modificationRequestType) {
                    case ModificationRequest::TYPE_TRANSACTION:
                        $modificationRequest = TransactionDeletionRequest::findOrFail($modificationRequestId);
                        break;
                    case ModificationRequest::TYPE_PURCHASE_INVOICE:
                        $modificationRequest = PurchaseInvoiceModificationRequest::findOrFail($modificationRequestId);
                        break;
                    case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                        $modificationRequest = PurchaseInvoiceItemModificationRequest::findOrFail($modificationRequestId);
                        break;
                }

                if (empty($modificationRequest)) {
                    $this->error("Id $modificationRequestId doesnt exist for $modificationRequestType");
                    return 1;
                }

                if ($modificationRequest->status != ModificationRequest::STATUS_PENDING) {
                    $this->error("$modificationRequestType modification request with id $modificationRequestId is in status {$modificationRequest->status}. Cannot Process");
                    return 1;
                }

                // process single modification
                $this->handleModificationRequest($modificationRequest);

                if ($modificationRequest->status != ModificationRequest::STATUS_COMPLETED) {
                    $this->error("Failed to process ModificationRequest with id: {$modificationRequest->id}");
                } else {
                    $this->info("Processed ModificationRequest with id: {$modificationRequest->id}");
                }
                ModificationRequestService::sendProcessedEmailUpdate(collect([$modificationRequest]));
                return null;
            }

            switch ($modificationRequestType) {
                case ModificationRequest::TYPE_TRANSACTION:
                    $this->info(2);
                    $transactionDeletionRequests = TransactionDeletionRequest::query()->statusPending()->get();
                    break;
                case ModificationRequest::TYPE_PURCHASE_INVOICE:
                    $purchaseInvoiceModificationRequests = PurchaseInvoiceModificationRequest::query()->statusPending()->get();
                    break;
                case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                    $purchaseInvoiceItemModificationRequests = PurchaseInvoiceItemModificationRequest::query()->statusPending()->get();
                    break;
            }
        }

        if (!empty($modificationRequestId)) {
            $this->error("modificationRequestId is set but modificationRequestType is not set. Exiting!");
            return 1;
        }

        // TODO check for, and fix stuck modifications (status = processing, modifiedAt older than 1hr??)

        // get all modification requests with status = pending
        if (empty($modificationRequestType)) {
            $transactionDeletionRequests = TransactionDeletionRequest::query()->statusPending()->get();
            $purchaseInvoiceModificationRequests = PurchaseInvoiceModificationRequest::query()->statusPending()->get();
            $purchaseInvoiceItemModificationRequests = PurchaseInvoiceItemModificationRequest::query()->statusPending()->get();

            $this->info("Processing all Modification Requests");
        }

        // Process Modification Requests

        if (!empty($transactionDeletionRequests)) {
            foreach ($transactionDeletionRequests as $transactionDeletionRequest) {
                $this->handleModificationRequest($transactionDeletionRequest);
            }
        }
        if (!empty($purchaseInvoiceModificationRequests)) {
            foreach ($purchaseInvoiceModificationRequests as $purchaseInvoiceModificationRequest) {
                $this->handleModificationRequest($purchaseInvoiceModificationRequest);
            }
        }
        if (!empty($purchaseInvoiceItemModificationRequests)) {
            foreach ($purchaseInvoiceItemModificationRequests as $purchaseInvoiceItemModificationRequest) {
                $this->handleModificationRequest($purchaseInvoiceItemModificationRequest);
            }
        }

        ModificationRequestService::sendProcessedEmailUpdate(
            (new Collection())
                ->merge($transactionDeletionRequests)
                ->merge($purchaseInvoiceModificationRequests)
                ->merge($purchaseInvoiceItemModificationRequests)
        );

        // Print Stats
//        if (empty($modificationRequestType) || $modificationRequestType == ModificationRequest::TYPE_TRANSACTION) {
//            $failedTransactionDeletionRequests = $transactionDeletionRequests->filter(function ($modificationRequest) {
//                return $modificationRequest->status != ModificationRequest::STATUS_COMPLETED;
//            })->values();
//            if (empty($failedTransactionDeletionRequests->count())) {
//                $this->info("Processed all {$transactionDeletionRequests->count()} Transaction Deletion Requests Successfully");
//            } else {
//                $this->info("{$failedTransactionDeletionRequests->count()} Transaction Deletion Requests failed to process");
//            }
//        }
//        if (empty($modificationRequestType) || $modificationRequestType == ModificationRequest::TYPE_PURCHASE_INVOICE) {
//            $failedPurchaseInvoiceModificationRequests = $purchaseInvoiceModificationRequests->filter(function ($modificationRequest) {
//                return $modificationRequest->status != ModificationRequest::STATUS_COMPLETED;
//            })->values();
//            if (empty($failedPurchaseInvoiceModificationRequests->count())) {
//                $this->info("Processed all {$purchaseInvoiceModificationRequests->count()} Purchase Invoice Modification Requests Successfully");
//            } else {
//                $this->info("{$failedPurchaseInvoiceModificationRequests->count()} Purchase Invoice Modification Requests failed to process");
//            }
//        }
//        if (empty($modificationRequestType) || $modificationRequestType == ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM) {
//            $failedPurchaseInvoiceItemModificationRequests = $purchaseInvoiceItemModificationRequests->filter(function ($modificationRequest) {
//                return $modificationRequest->status != ModificationRequest::STATUS_COMPLETED;
//            })->values();
//            if (empty($failedPurchaseInvoiceItemModificationRequests->count())) {
//                $this->info("Processed all {$purchaseInvoiceItemModificationRequests->count()} Purchase Invoice Item Modification Requests Successfully");
//            } else {
//                $this->info("{$failedPurchaseInvoiceItemModificationRequests->count()} Purchase Invoice Item Modification Requests failed to process");
//            }
//        }

        return 0;
    }


    /**
     * @throws Exception
     */
    private function handleModificationRequest($modificationRequest)
    {
        $modificationRequest->status = ModificationRequest::STATUS_PROCESSING;
        $modificationRequest->save();

        switch ($modificationRequest->getModificationRequestType()) {
            case ModificationRequest::TYPE_TRANSACTION:
            {
                $this->info("Processing Transaction Deletion Request with modificationRequestId: $modificationRequest->id");
                try {
                    $transaction = Transaction::query()
                        ->where("transaction_id", "=", $modificationRequest->transaction_id)
                        ->firstOrFail();

                    if (empty($transaction->delete())) {
                        throw new Exception("Error deleting Transaction record with id: $transaction->id, transaction_id: $transaction->transaction_id");
                    }

                    $modificationRequest->status = ModificationRequest::STATUS_COMPLETED;
                    $modificationRequest->save();
                } catch (Exception $e) {
                    $modificationRequest->status = ModificationRequest::STATUS_FAILED;
                    $modificationRequest->save();

                    $this->error("transaction deletion request failed for id: $modificationRequest->id | " . $e->getMessage());
                    return;
                }
                break;
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE:
            {
                $this->info("Processing Purchase Invoice Modification Request with modificationRequestId: $modificationRequest->id");
                try {
                    $purchase_invoice = PurchaseInvoice::findOrFail($modificationRequest->purchase_invoice_id);
                    switch ($modificationRequest->action) {
                        case ModificationRequest::ACTION_EDIT:
                        {
                            $purchase_invoice->vendor_id = $modificationRequest->vendor_id;
                            $purchase_invoice->vendor_address_id = $modificationRequest->vendor_address_id;
                            $purchase_invoice->invoice_date = $modificationRequest->date;
                            if (!empty($modificationRequest->image)) {
                                $sourceImagePath = $modificationRequest->getPurchaseInvoiceImagePath();
                                $destinationImageDirectory = storage_path('app/purchased_invoice/invoice/' . $purchase_invoice->id);
                                if(!is_dir($destinationImageDirectory)){
                                    mkdir($destinationImageDirectory, 0777, true);
                                }
                                $destinationImagePath = $destinationImageDirectory . DIRECTORY_SEPARATOR . $modificationRequest->image;
                                if (!copy($sourceImagePath, $destinationImagePath)) {
                                    throw new Exception("Unable to copy file from {$sourceImagePath} to {$destinationImagePath}");
                                }

                                $purchase_invoice->scanned_file = $modificationRequest->image;
                            }
                            $purchase_invoice->platform_id = $modificationRequest->platform_id;
                            if (!empty($modificationRequest->eway_number)) {
                                $purchase_invoice->eway_number = $modificationRequest->eway_number;
                            }
                            if (!empty($modificationRequest->forecast_payment_date)) {
                                $purchase_invoice->forecast_payment_date = $modificationRequest->forecast_payment_date;
                            }

                            (new PurchaseInvoiceService())->recalculatePurchaseInvoice($purchase_invoice);
                            if (!$purchase_invoice->save()) {
                                throw new Exception("Error updating Purchase Invoice record with id: $purchase_invoice->id");
                            }
                            break;
                        }
                        case ModificationRequest::ACTION_DELETE:
                        {
                            if (empty($purchase_invoice->delete())) {
                                throw new Exception("Error deleting Purchase Invoice record with id: {$purchase_invoice->id}");
                            }
                            break;
                        }
                        default:
                        {
                            throw new Exception("Invalid value of action:{$$modificationRequest->action} for Purchase Invoice Modification Request: {$modificationRequest->purchase_invoice_id}");
                        }
                    }

                    $modificationRequest->status = ModificationRequest::STATUS_COMPLETED;
                    $modificationRequest->save();
                } catch (Exception $e) {
                    $modificationRequest->status = ModificationRequest::STATUS_FAILED;
                    $modificationRequest->save();

                    $this->error("purchase_invoice modification request failed for ModificationRequestId: $modificationRequest->id | " . $e->getMessage());
                    return;
                }
                break;
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
            {
                $this->info("Processing Purchase Invoice Item Modification Request with modificationRequestId: $modificationRequest->id");
                try {
                    $purchase_invoice_item = PurchaseInvoiceItem::findOrFail($modificationRequest->purchase_invoice_item_id);
                    switch ($modificationRequest->action) {
                        case ModificationRequest::ACTION_EDIT:
                        {
                            $purchase_invoice_item->product_id = $modificationRequest->product_id;
                            $purchase_invoice_item->quantity = $modificationRequest->quantity;
                            $purchase_invoice_item->base_cost = $modificationRequest->base_cost;
                            $purchase_invoice_item->scheme_discount = !empty($modificationRequest->scheme_discount) ? $modificationRequest->scheme_discount : 0;
                            $purchase_invoice_item->other_discount = !empty($modificationRequest->other_discount) ? $modificationRequest->other_discount : 0;
                            $purchase_invoice_item->post_tax_discount = !empty($modificationRequest->post_tax_discount) ? $modificationRequest->post_tax_discount : 0;

                            $purchase_invoice = PurchaseInvoice::findOrFail($purchase_invoice_item->purchase_invoice_id);
                            (new PurchaseInvoiceService())->recalculatePurchaseInvoice($purchase_invoice);

                            if (!$purchase_invoice_item->save()) {
                                throw new Exception("Error updating Purchase Invoice Item record with id: $purchase_invoice_item->id");
                            }
                            break;
                        }
                        case ModificationRequest::ACTION_DELETE:
                        {
                            if (empty($purchase_invoice_item->delete())) {
                                throw new Exception("Error deleting Purchase Invoice Item record with id: {$purchase_invoice_item->id}");
                            }
                            break;
                        }
                        default:
                        {
                            throw new Exception("Invalid value of action:{$$modificationRequest->action} for Purchase Invoice Item Modification Request: {$modificationRequest->purchase_invoice_item_id}");
                        }
                    }
                    $modificationRequest->status = ModificationRequest::STATUS_COMPLETED;
                    $modificationRequest->save();

                } catch (Exception $e) {
                    $modificationRequest->status = ModificationRequest::STATUS_FAILED;
                    $modificationRequest->save();

                    $this->error("purchase_invoice_item modification request failed for ModificationRequestId: $modificationRequest->id | " . $e->getMessage());
                    return;
                }
                break;
            }
        }
    }
}