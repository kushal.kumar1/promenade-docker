<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\FlatInventory;
use Illuminate\Support\Facades\DB;

class MarkPutawayItemsSalable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mark:ready-for-sale';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to move items from putaway to ready_for_sale for Store External Purchase.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('flat_inventories')->where('warehouse_id', 1)
          ->where('status', FlatInventory::STATUS_PUTAWAY)
          ->update(['status' => FlatInventory::STATUS_READY_FOR_SALE]);
    }
}
