<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Niyotail\Exceptions\OrderItemsNotFoundException;
use Niyotail\Jobs\ConvertToSaleOrderJob;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\Order\OrderService;

class GenerateSaleOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:sale-orders {ids?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate sale orders from store orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $storeOrderIDs =  $this->argument('ids');

        $query = StoreOrder::where('status', StoreOrder::STATUS_PENDING)
            ->whereNull('order_id')
            ->orderBy('created_at', 'asc');

        if (!empty($storeOrderIDs)) {
            $query = $query->whereIn('id', $storeOrderIDs);
        }

        $allStoreOrders = $query->limit(10)->get();


        foreach ($allStoreOrders as $order) {
            if(App::environment() != 'local') {
                ConvertToSaleOrderJob::dispatch($order)
                    ->onConnection('sqs')
                    ->onQueue(config('queue.convertToSaleOrder'));
            } else {
                dispatch(new ConvertToSaleOrderJob($order));
            }
        }
    }
}
