<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\CreditNote;
use Niyotail\Helpers\CreditNoteHelper;

class RegenerateCreditNote extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credit-note:regenerate {financial_year} {numbers?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to regenerate credit notes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = $this->argument('financial_year');
        $numbers = $this->argument('numbers');
        $creditNotes = CreditNote::where('financial_year', $year);

        if (!empty($numbers)) {
            $creditNotes = $creditNotes->whereIn('number', $numbers);
        } else {
            $this->info("No invoice numbers specified! Regenerating all invoices for year $year");
        }

        $count=0;
        $creditNotes = $creditNotes->chunk(50, function ($creditNotes) use (&$count) {
            $count += count($creditNotes);
            foreach ($creditNotes as $creditNote) {
                try {
                    $this->info('Generating invoice no.' . $creditNote->number);
                    CreditNoteHelper::generatePdf($creditNote);
                } catch (\Exception $e) {
                    $this->error('Error on invoice no.' . $creditNote->number);
                    $this->error($e->getMessage());
                }
            }
        });

        $this->info("Total $count credit notes generated");
    }
}
