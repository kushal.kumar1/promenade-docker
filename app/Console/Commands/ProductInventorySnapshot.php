<?php

namespace Niyotail\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProductInventorySnapshot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'snapshot:product-inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a file on S3 with inventory count on a given date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = DB::select('select p.barcode, p.id, p.name, p.group_id, b.name as brand, 
        (select pv.mrp from product_variants as pv where pv.product_id = p.id and pv.value = \'unit\' ) as unit_mrp,
        (select sum(i.quantity) from inventories as i where i.product_id = p.id group by i.product_id ) as quantity,
        wr.reference, p.status     
        from products as p 
        left join brands as b on p.brand_id = b.id
        left join rack_product as rp on p.id = rp.product_id
        left join warehouse_racks as wr on wr.id = rp.rack_id');

        $path = storage_path('app/product_inventory/');

        $fileName = Carbon::today()->format('Y-m-d').'.csv';

        if (file_exists($path.$fileName)) {
            unlink($path.$fileName);
        }

        $columns = array('Barcode', 'PID', 'Name', 'GID', 'Brand', 'Unit MRP', 'Stock Qty.', 'Rack Ref.', 'Status');
        $file = fopen($path.$fileName, 'w');

        fputcsv($file, $columns);
        foreach ($products as $product) {
            fputcsv($file, [
                $product->barcode, $product->id, $product->name, $product->group_id,
                $product->brand, $product->unit_mrp,
                $product->quantity, $product->reference, $product->status
            ]);
        }
        fclose($file);

        $this->info('Complete');
    }
}
