<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\Inventory;
use Niyotail\Models\Model;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class ReorderUnfulfilledItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reorder:unfulfilled  
    {storeOrderIds : Comma seperated store order ids whose unfulfilled quantity you want to select}
    {storeId? : ID of the store for which you need to create new order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reorder Unfulfilled Order Items';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $unfulfilledData = $this->getUnfulfilledData();
        if ($unfulfilledData->isEmpty()) {
            $this->info('No unfulfilled order items found!');
            return 0;
        }
        $groupedUnfulfilledItems = $unfulfilledData->groupBy('store_id');
        $storeOrders = collect([]);
        foreach ($groupedUnfulfilledItems as $storeId => $unfulfilledItems) {
            $this->comment("Creating order on unfulfilled items of store #$storeId ...");
            $cart = null;
            foreach ($unfulfilledItems as $unfulfilledItem) {
                $request = new Request();
                $productVariant = ProductVariant::where('sku', $unfulfilledItem->sku)->withTrashed()->first();
                $this->updateMissingInventory($productVariant->product_id,$unfulfilledItem->warehouse_id, $unfulfilledItem->remaining_qty);
                $data['id'] = $productVariant->id;
                $data['quantity'] = $unfulfilledItem->remaining_qty;
                $data['store_id'] = $storeId;
                $data['sku'] = $unfulfilledItem->sku;
                $request->replace($data);
                $cartService = new CartService();
                $createdBy = Model::$map[$unfulfilledItem->created_by_type]::find($unfulfilledItem->created_by_id);
                if (empty($cart)) {
                    $cart = $cartService->addToCart($request, $createdBy);
                } else {
                    $cartService->setCart($cart);
                    $cartService->addToCart($request, $createdBy);
                }
            }
            if (!empty($cart)) {
                $storeOrderService = new StoreOrderService();
                $storeOrder = $storeOrderService->create($cart);
                $storeOrders->push($storeOrder);
                $this->comment("Successfully created store-order #$storeOrder->id of unfulfilled items of store #$storeId");
            }
        }
        $this->convertToSaleOrders($storeOrders);
        return 0;
    }

    private function getUnfulfilledData()
    {
        $storeOrderIds = explode(',', $this->argument('storeOrderIds'));
        $storeId = $this->argument('storeId');
        $query = StoreOrder::selectRaw('
            store_orders.store_id as store_id,
            store_order_items.sku AS sku, 
            store_order_items.quantity AS ordered_qty,
            store_orders.warehouse_id AS warehouse_id,
            store_orders.created_by_id AS created_by_id,
            store_orders.created_by_type AS created_by_type,
            IFNULL(sum(order_items.quantity),0) AS fulfilled_qty,
            IF(product_variants.quantity * store_order_items.quantity - IFNULL(sum(order_items.quantity), 0) > 0, product_variants.quantity * store_order_items.quantity - ifnull(sum(order_items.quantity), 0), 0) AS remaining_qty')
            ->join('store_order_items', 'store_orders.id', '=', 'store_order_items.store_order_id')
            ->join('product_variants', 'store_order_items.sku','=', 'product_variants.sku')
            ->leftjoin('orders', 'store_orders.order_id', '=', 'orders.id')
            ->leftJoin('order_items', function ($join) {
                $join->on('orders.id', '=', 'order_items.order_id')
                    ->on('store_order_items.product_id', '=', 'order_items.product_id');
            });
        if (!empty($storeId)) {
            $query->where('store_orders.store_id', $storeId);
        }
        return $query->whereIn('store_orders.status', [StoreOrder::STATUS_PROCESSED, StoreOrder::STATUS_UNFULFILLED])
            ->whereIn('store_orders.id', $storeOrderIds)
            ->groupBy('sku', 'store_id')
            ->having('remaining_qty', '>', 0)
            ->get();
    }

    private function updateMissingInventory($productId, $warehouseId, $quantity)
    {
        $inventories = Inventory::where('product_id', $productId)
            ->where('warehouse_id', $warehouseId)->orderBy('created_at', 'desc')->get();
        if($inventories->isEmpty()){
            Log::info('reorder', ["Inventory not found for $productId"]);
        }
        if($inventories->isNotEmpty()){
            if ($inventories->sum->quantity == $quantity || $inventories->sum->quantity > $quantity) {
                return true;
            }
            $inventory = $inventories->first();
            $inventory->quantity = $quantity;
            $inventory->save();
        }
        return true;
    }

    private function convertToSaleOrders($storeOrders)
    {
        foreach ($storeOrders as $order) {
            try {
                $orderService = App::make(OrderService::class);
                $orderService->create($order);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                $order->status = StoreOrder::STATUS_UNFULFILLED;
                $order->save();
            }
        }
    }
}
