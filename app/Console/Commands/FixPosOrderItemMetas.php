<?php


namespace Niyotail\Console\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use Niyotail\Models\Inventory;
use Niyotail\Models\PosOrder;
use Niyotail\Services\PosOrder\SyncPosOrderService;

class FixPosOrderItemMetas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:pos_order_item_metas {order_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to fix order item metas for POS order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderId = $this->argument('order_id');
        $order = PosOrder::find($orderId);

        $store = $order->store;
        $storeCommissionTags = $store->commissionTags;

        $items = $order->items;

        $syncPosOrderService = new SyncPosOrderService();

        foreach ($items as $item) {
            $syncPosOrderService->storeOrderItemMeta($item, [], $storeCommissionTags);
        }
    }
}