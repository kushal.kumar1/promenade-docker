<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Niyotail\Models\Cart;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\StorePurchaseInvoice;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class GenerateDirectPurchaseOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:direct-purchase-order {pid?* : Purchase Invoice Id\'s for which you want to generate direct purchase order}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to generate store order from store direct purchases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (empty($this->argument('pid'))) {
            $storePurchaseInvoices = StorePurchaseInvoice::with('purchaseInvoice.items')
                ->whereNull('store_order_id')->orderBy('created_at', 'asc');
        }else{
            $storePurchaseInvoices = StorePurchaseInvoice::whereIn('purchase_invoice_id', $this->argument('pid'));
        }

        $storePurchaseInvoices->chunk(5, function ($storeInvoices) {
            foreach ($storeInvoices as $storeInvoice) {
                $purchaseInvoice = $storeInvoice->purchaseInvoice;
                $cartService = new CartService();
                try {
                    //add items to carts
                    foreach ($purchaseInvoice->items as $item) {
                        /* @var PurchaseInvoiceItem $item */
                        $grnItems = $item->grnItems;
                        $receivedQty = $grnItems ? $grnItems->sum('quantity') : 0;
                        $debitNoteItems = $item->debitNoteItems;
                        $shortQty = $debitNoteItems ? $debitNoteItems->sum('quantity') : 0;

                        /* add item to order only if received quantity is > 0 */
                        $qtyToOrder = $receivedQty - $shortQty;
                        if ($qtyToOrder <= 0) continue;

                        $productVariant = ProductVariant::where('sku', $item->sku)->withTrashed()->first();
                        $request = new Request([
                            'source' => Cart::SOURCE_STORE_PURCHASE,
                            'warehouse_id' => 1,
                            'store_id' => $storeInvoice->store_id,
                            'id' => $productVariant->id,
                            'quantity' => $qtyToOrder // quantity of the received variant
                        ]);

                        $cart = $cartService->addToCart($request, $purchaseInvoice->createdBy);
                    }

                    // Create Store Order
                    $storeOrderService = new StoreOrderService();
                    $storeOrder = $storeOrderService->create($cart, $storeInvoice);

//                     Assign store order
//                    $storeInvoice->assignedOrder()->associate($storeOrder);
//                    $storeInvoice->save();
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                }
            }
        });
    }
}
