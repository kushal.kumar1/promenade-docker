<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;

use Niyotail\Models\Product;
use Niyotail\Models\Attribute;
use Niyotail\Models\ProductAttribute;
use Niyotail\Helpers\Exception;

class ProductAttributes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:attributes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create product attributes from name and change the original name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::active()->whereNull('meta_title')->get();

        foreach($products as $org)
        {
            $product = Product::find($org->id);
            try {
              $product->meta_title = $this->parseAttributes($product);
            } catch(\Exception $e){

            }
            $product->updated_at = $product->updated_at;
            // $product->name            =   $org->name;
            $product->save();
        }

    }

    public function parseAttributes($product)
    {
        $data = $this->parseName($product->name);
        print_r($data);

        if(!empty($data['weight']))
        {
            $productAttribute = new ProductAttribute();
            $productAttribute->product_id = $product->id;
            $productAttribute->attribute_id = 1;
            $productAttribute->value = $data['weight'];
            $productAttribute->save();
        }

        if(!empty($data['price']))
        {
            $productAttribute = new ProductAttribute();
            $productAttribute->product_id = $product->id;
            $productAttribute->attribute_id = 6;
            $productAttribute->value = $data['price'];
            $productAttribute->save();
        }


        return $data['remaining'];
    }

    public function parseName($name)
    {
        $match = [];
        $data = [];

        $data['weight'] = '';
        if(preg_match('/\d+\.?\d*\s?(g|gm|Gm|kg|KG|Kg|ml|ML|Ml|L|l)/',$name,$match))
        {
            $data['weight'] = $match[0];
            $name = str_replace($match[0],'',$name);
        }

        $data['price'] = '';
        if(preg_match('/(Rs|RS)\.?\s?\d+\.?\d*/',$name,$match))
        {
            $data['price'] = $match[0];
            $name = str_replace($match[0],'',$name);
        }

        $data['remaining'] = $name;
        return $data;
    }
}
