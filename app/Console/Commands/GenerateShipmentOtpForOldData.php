<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Shipment;
use Niyotail\Services\Order\ShipmentService;

class GenerateShipmentOtpForOldData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shipment:generate-otp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'One time command to generate otps for existing shipments!';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shipments = Shipment::where('status', Shipment::STATUS_DISPATCHED)
            ->get();
        $service = new ShipmentService();
        $this->comment('Generating Otps for existing shipments...');
        foreach ($shipments as $shipment){
            $service->setOtp($shipment);
        }
        $this->comment('Success..');
    }
}
