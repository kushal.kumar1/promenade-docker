<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class RetrySqsFailedJobs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retry:sqs-failed {ids?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retry all sqs type failed jobs!';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $jobIds = $this->argument('ids');
        if (!empty($jobIds)) {
            $jobsToBeRetried = DB::table('failed_jobs')
                ->where('connection', 'sqs')
                ->whereIn('id', $jobIds)
                ->get();
        } else {
            $jobsToBeRetried = DB::table('failed_jobs')
                ->where('connection', 'sqs')
                ->get();
        }
        $bar = $this->output->createProgressBar($jobsToBeRetried->count());
        $this->comment('Retrying jobs...');
        $bar->start();
        foreach ($jobsToBeRetried as $job) {
            Artisan::call("queue:retry $job->id");
            $bar->advance();
        }
        $bar->finish();
        $this->info("\n All jobs pushed back on queue..");
        return 0;
    }
}
