<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Invoice;
use Niyotail\Helpers\InvoiceHelper;

class RegenerateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:regenerate {financial_year} {storeId?} {numbers?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to regenerate invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $year = $this->argument('financial_year');
        $storeId = $this->argument('storeId');
        $numbers = $this->argument('numbers');
        $invoices = Invoice::where('financial_year', $year);

        if(!empty($storeId)){
            $invoices->whereHas('order', function ($orderQuery) use ($storeId){
                $orderQuery->where('store_id', $storeId);
            });
        }

        if (!empty($numbers)) {
            $invoices = $invoices->whereIn('number', $numbers);
        } else {
            $this->info("No invoice numbers specified! Regenerating all invoices for year $year");
        }

        $count=0;
        $invoices = $invoices->chunk(50, function ($invoices) use (&$count) {
            $count += count($invoices);
            foreach ($invoices as $invoice) {
                try {
                    $this->info('Generating invoice no.' . $invoice->number);
                    InvoiceHelper::generatePdf($invoice);
                } catch (\Exception $e) {
                    $this->error('Error on invoice no.' . $invoice->number);
                    $this->error($e->getMessage());
                }
            }
        });

        $this->info("Total $count invoices generated");
    }
}
