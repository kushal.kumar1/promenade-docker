<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Shipment;
use Niyotail\Models\WarehouseScope;
use Niyotail\Services\Order\ShipmentService;

class ProcessDirectPurchaseOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:direct-purchase-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create generate shipments for orders related to store direct purchase';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::withoutGlobalScope(WarehouseScope::class)
            ->where('status', Order::STATUS_CONFIRMED)->where('warehouse_id', 1);
        $shipmentService = new ShipmentService();
        $orders->chunk(5, function ($orders) use($shipmentService){
            foreach ($orders as $order) {
                try {
                    $shipmentService->completeDirectPurchase($order);
                }
                catch(ServiceException $e) {
                    \Log::error($e->getMessage());
                }
            }
        });
    }
}
