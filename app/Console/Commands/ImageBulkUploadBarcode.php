<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Product;
use Niyotail\Models\ProductImage;

class ImageBulkUploadBarcode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:upload-barcode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Temp command to upload images from the directory path';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $files = Storage::disk('upload')->files();
        foreach($files as $file){
            //as per naming convention of the file
            $productNameArray = explode(".", $file);
            $productBarcode = (int) $productNameArray[0];
            $extension = $productNameArray[count($productNameArray) - 1];

            $products = Product::where('barcode', $productBarcode)->get();
            if(empty($products) || ($products->count() == 0)){
                $this->error("Products not found for $file with $productBarcode");
            }

            $i = 0;
            $count = $products->count();
            foreach ($products as $product) {
              $i++;
              try {
                  $fileName = uniqid().".".$extension;
                  $filePath = "media/" . $product->id . "/" . $fileName;
                  $image = Storage::disk('upload')->get($file);
                  Storage::disk('public')->put($filePath, $image);

                  $productImage = new ProductImage();
                  $productImage->product_id = $product->id;
                  $productImage->name = $fileName;
                  $productImage->save();
                  //move image to success directory
                  if ($i == $count) {
                    Storage::disk('upload')->move($file, "success_new/$file");
                  }
              } catch(Exception $e){
                  $this->error("Upload Error for $file with $productId");
                  // $this->error("Moving image to error directory");
                  //move image to error directory
                    // Storage::disk('upload')->move($file, "error_new/$file");
              }
            }
        }
    }
}
