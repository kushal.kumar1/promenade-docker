<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Store;

class GenerateStoreLedger extends Command
{
    protected $signature = 'generate:ledger {stores?*}';

    protected $description = 'Command to generate consolidated ledger for stores';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $storeIDs = $this->argument('stores');
        $storeQuery = Store::with(['transactions' => function($query) {
                          $query->where('status', 'completed');
                          $query->orderBy('transaction_date');
                        }])->whereHas('transactions');
        if (!empty($storeIDs)) {
            $storeQuery = $storeQuery->whereIn('id', $storeIDs);
        } else {
          $this->error('Invalid Store Id');
          return;
        }
        $stores = $storeQuery->get();
        foreach($stores as $store) {
            var_dump('processing store_id:'.$store->id);
            $this->generateLedger($store);
        }
    }

    private function generateLedger($store) {
        $transactions = $store->transactions;
        $previousTransaction = null;
        foreach ($transactions as $transaction) {
            $previousBalance = empty($previousTransaction) ? 0 : $previousTransaction->balance;
            $balance = 0;
            switch ($transaction->type) {
              case 'debit':
                $balance = $previousBalance - $transaction->amount;
                break;
              case 'credit':
                $balance = $previousBalance + $transaction->amount;
                break;
            }
            $transaction->balance = $balance;
            $transaction->updated_at = $transaction->updated_at;
            $transaction->save();
            $previousTransaction = $transaction;
        }
    }
}
