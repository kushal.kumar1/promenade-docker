<?php


namespace Niyotail\Console\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use Niyotail\Jobs\StoreRateCreditNote;
use Niyotail\Models\PosOrder;

class GenerateRateCreditNotes extends Command
{
    protected $signature = 'generate:rate-credit-note {date?} {end_date?} {register_number?}';

    protected $description = 'Command to generate rate credit notes for all stores on a given day';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $date = $this->argument('date');
        $date = $date ? Carbon::parse($date) : Carbon::yesterday();
        $date->setTimezone('Asia/Kolkata');

        $endDate = $this->argument('end_date');
        $endDate = $endDate ? Carbon::parse($endDate)->setTimezone('Asia/Kolkata') : null;

        if(!empty($date) && !empty($endDate)) {
            $posOrdersByStore = PosOrder::whereRaw("date(convert_tz(invoiced_at, '+00:00', '+05:30')) >= '".$date->copy()->format('Y-m-d')."' and 
            date(convert_tz(invoiced_at, '+00:00', '+05:30')) <= '".$endDate->copy()->format('Y-m-d')."'")->doesntHave('creditNote')->get()->groupBy('store_id');
        }
        else {
            $posOrdersByStore = PosOrder::whereDate('invoiced_at', '<=', $date)->doesntHave('creditNote')->get()->groupBy('store_id');
        }

        $startDate = $date->copy()->format('Y-m-d 00:00:00');
        $endDate = $endDate ? $endDate->format('Y-m-d 23:59:59') : $date->copy()->format('Y-m-d 23:59:59');

        $registerNumber = $this->argument('register_number');

        foreach ($posOrdersByStore as $storeId=>$orders) {
            dispatch(new StoreRateCreditNote($storeId, $startDate, $endDate, $registerNumber));
        }
    }
}