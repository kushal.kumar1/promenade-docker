<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Events\ReturnOrderCompleted;
use Niyotail\Models\ReturnOrder;

class SyncReturnShipment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:returns {returnId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync returns. To be used with caution';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $returnId = $this->argument('returnId');
        $returnOrder = ReturnOrder::findOrFail($returnId);
        event(new ReturnOrderCompleted($returnOrder));
    }
}
