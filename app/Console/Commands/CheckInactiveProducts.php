<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Niyotail\Models\Product;

class CheckInactiveProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:inactive-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to make inactive products with stock active';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where('status', 0)->whereHas('inventories', function ($query){
            $query->where('quantity', '>', '0');
        })->get();
        foreach($products as $product) {
            $product->status = 1;
            $product->save();
            $this->info("Making Product active $product->id: $product->name");
        }
    }
}
