<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Matrix\Exception;
use Niyotail\Helpers\Integration\ProductSync;
use Niyotail\Helpers\IntegrationQueueHelper;
use Niyotail\Models\Product;
use Niyotail\Models\Sync;

class SyncProductsToPos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync products to POS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //fetch last synced date
        $lastSync = Sync::firstOrNew([
            'type' => Sync::TYPE_PRODUCTS
        ]);
        $syncedAt = $lastSync->synced_at ?? "2021-01-01 00:00:00";

        //get products updated since last sync
        $products = Product::active()->with('brand.marketer.level', 'unitVariant', 'group','category')
            ->where('group_id','!=', 21805) //GID for infra related products
            ->where('updated_at','>=', $syncedAt)
            ->orderBy('updated_at', 'asc')
            ->get();

        if($products->isNotEmpty()) {
            foreach($products as $product){
                try {
                    ProductSync::push($product);
                }
                catch(\Exception $e){
                    \Log::error("Product Sync PID: $product->id | ". $e->getMessage());
                }
            }
            // update synced_at in syncs table
            $lastSync->synced_at = $products->last()->getOriginal('updated_at');
            $lastSync->save();
        }
    }
}
