<?php


namespace Niyotail\Console\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Niyotail\Models\Order;
use Niyotail\Models\Product;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;

class CancelOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:cancel {ids?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to cancel multiple orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderIds = $this->argument('ids');
        $orders = Order::whereIn('id', $orderIds);

        $orders->chunk(5, function ($saleOrders) {
            foreach ($saleOrders as $order) {
                try {
                    $orderService = App::make(OrderService::class);
                    $orderService->cancel($order->id, 'Placed due to system error. Cancelled using command');
                } catch (\Exception $e) {
                    \Log::error($e->getMessage());
                    $order->status = StoreOrder::STATUS_UNFULFILLED;
                    $order->save();
                }
            }
        });

    }
}