<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Order;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\Cart;
use Niyotail\Models\Store;
use Niyotail\Services\Order\OrderService;
use Niyotail\Helpers\InvoiceHelper;
use Illuminate\Support\Facades\Artisan;

class ChangeOrderStoreId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:order-store {orderId} {fromStore} {toStore}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move order from one store to another';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OrderService $orderService)
    {
        $fromStoreId = $this->argument('fromStore');
        $toStoreId = $this->argument('toStore');
        $orderId = $this->argument('orderId');

        $toStore = Store::find($toStoreId);
        if (empty($toStore)) {
            $this->error('Invalid destination store id');
            return;
        }
        $order = Order::where('store_id', $fromStoreId)->find($orderId);
        if (empty($order)) {
            $this->error('Order does not belong to source store');
            return;
        }

        $this->moveOrder($order, $fromStoreId, $toStore, $orderService);
    }

    private function moveOrder($order, $fromStoreId, $toStore, $orderService)
    {
        DB::transaction(function () use($order,  $fromStoreId, $toStore, $orderService) {
            $toStoreId = $toStore->id;
            $storeOrder = StoreOrder::where('order_id', $order->id)->first();
            $storeOrder->store_id = $toStoreId;
            $storeOrder->save();

            $cart = Cart::where('store_order_id', $storeOrder->id)->first();
            $cart->store_id = $toStoreId;
            $cart->save();

            $order->store_id = $toStoreId;
            $order->save();

            $orderService->addAddress($order, $toStore);
            $invoices = $order->invoices->where('status', '!=', 'cancelled');
            foreach($invoices as $invoice) {
                $this->info('Generating invoice no.' . $invoice->number);
                InvoiceHelper::generatePdf($invoice);
                $transaction = $invoice->transactions->first();
                $transaction->store_id = $toStoreId;
                $transaction->save();
            }
            Artisan::call("generate:ledger $toStoreId $fromStoreId");
        });
    }
}
