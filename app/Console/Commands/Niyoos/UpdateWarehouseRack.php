<?php

namespace Niyotail\Console\Commands\Niyoos;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Product;
use Niyotail\Models\WarehouseRack;

use Carbon\Carbon;

class UpdateWarehouseRack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:warehouse-rack {dbName} {store}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to insert or update inventory after stock validation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
     //TODO:: Improve syncing code, allow delta sync, store image urls
    public function handle()
    {
        DB::transaction(function () {
            $db = $this->argument('dbName');
            $storeID = $this->argument('store');
            // $productRacks =  DB::select(DB::raw("select i.product_id, pv.vendor_product_identifier, group_concat(distinct(sam.shelf_reference)) as racks
            //                                         from $db.inventories i join $db.product_vendor pv on i.product_id = pv.vendor_product_identifier
            //                                         join $db.stock_audit_metas sam on sam.inventory_id = i.id where i.store_id = $storeID
            //                                         and i.created_at >= '2020-07-30 00:00:00' and sam.shelf_reference is not null group by i.product_id "
            //                   ));
            // foreach($productRacks as $productRack) {
            //       $racks = explode(',',$productRack->racks);
            //       foreach ($racks as $rack) {
            //         $this->info("$rack, $productRack->vendor_product_identifier");
            //         DB::statement("insert into rack_product(rack_id, product_id) values ($rack, $productRack->vendor_product_identifier)");
            //       }
            // }
            $productRacks =  DB::select(DB::raw("select product_id, rack from temp_racks order by product_id"));
            foreach($productRacks as $productRack) {
                $racks = explode(',', $productRack->rack);
                foreach($racks as $rack) {
                    $extracted = explode('-',$rack);
                    $modified = '';
                    if (count($extracted) == 4) {
                        $extracted[1] = $extracted[1]+0;
                        $extracted[3] = $extracted[3]+0;

                        if (strcasecmp($extracted[0], 'R') == 0) {
                            $modified .= 'R-'. (($extracted[1] < 10) ? '00' : ($extracted[1] < 100 ? '0' : '')).$extracted[1];
                            $modified .= ' --- ';
                            if (strcasecmp($extracted[2], 'S') == 0) {
                                $modified .= 'S-0'. $extracted[3];
                            }
                            try {
                                    DB::statement("insert into rack_product(rack_id, product_id, status) values ((select id from warehouse_racks where reference='$modified'), $productRack->product_id, 1)");
                             } catch(\Exception $e) {
                                     $this->info('R Error: '. $rack. 'Modified: '.$modified. '  Prouct_id:'.$productRack->product_id);
                             }
                        }
                    }

                    if (count($extracted) == 2) {
                        $extracted[1] = $extracted[1]+0;
                        if (strcasecmp($extracted[0], 'W') == 0) {
                            $modified .= 'W-'. (($extracted[1] < 10) ? '00' : ($extracted[1] < 100 ? '0' : '')).$extracted[1];
                            try {
                                     DB::statement("insert into rack_product(rack_id, product_id, status) values ((select id from warehouse_racks where reference='$modified'), $productRack->product_id, 1)");
                             } catch(\Exception $e) {
                                     $this->info('W Error: '. $rack. 'Modified: '.$modified. '  Prouct_id:'.$productRack->product_id);
                             }
                        }

                        else if (strcasecmp($extracted[0], 'P') == 0) {
                            $extracted[1] = $extracted[1]+0;
                            $modified .= 'P-'. (($extracted[1] < 10) ? '00' : ($extracted[1] < 100 ? '0' : '')).$extracted[1];
                            try {
                                     DB::statement("insert into rack_product(rack_id, product_id, status) values ((select id from warehouse_racks where reference='$modified'), $productRack->product_id, 1)");
                             } catch(\Exception $e) {
                                     $this->info('P Error: '. $rack. 'Modified: '.$modified.'  Prouct_id:'.$productRack->product_id);
                             }
                        }

                        else if (strcasecmp($extracted[0], 'PRS') == 0) {
                            $extracted[1] = $extracted[1]+0;
                            $modified .= 'PRS-'. (($extracted[1] < 10) ? '00' : ($extracted[1] < 100 ? '0' : '')).$extracted[1];
                            try {
                                     DB::statement("insert into rack_product(rack_id, product_id, status) values ((select id from warehouse_racks where reference='$modified'), $productRack->product_id, 1)");
                             } catch(\Exception $e) {
                                     $this->info('PRS Error: '. $rack. 'Modified: '.$modified.'  Prouct_id:'.$productRack->product_id);
                             }
                        }

                        else if (strcasecmp($extracted[0], 'BOX') == 0) {
                           $extracted[1] = $extracted[1]+0;
                           $modified .= 'BOX-'. (($extracted[1] < 10) ? '00' : ($extracted[1] < 100 ? '0' : '')).$extracted[1];
                           try {
                                    DB::statement("insert into rack_product(rack_id, product_id, status) values ((select id from warehouse_racks where reference='$modified'), $productRack->product_id, 1)");
                            } catch(\Exception $e) {
                                    $this->info('BOX Error: '. $rack. 'Modified: '.$modified.'  Prouct_id:'.$productRack->product_id);
                            }
                       }
                    }
                }
            }
        });
    }
}
