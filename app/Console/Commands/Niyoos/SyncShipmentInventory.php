<?php

namespace Niyotail\Console\Commands\Niyoos;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Shipment;
use Niyotail\Events\Shipment\ShipmentDelivered;

class SyncShipmentInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:niyoos-inventory {shipment_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Niyoos Inventory based on delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $shipmentID = $this->argument('shipment_id');
        $shipment = Shipment::where('id', $shipmentID)->with('order.store')->first();
        if (!empty($shipment) && $shipment->status == Shipment::STATUS_DELIVERED) {
            event(new ShipmentDelivered($shipment));
        }

    }
}
