<?php

namespace Niyotail\Console\Commands\Niyoos;

use Illuminate\Console\Command;
use Niyotail\Events\Inventory\MallProductsInventorySync;
use Niyotail\Models\Product;
use function event;


class SyncMallProductsInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:mall-inventory {products?*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to sync mall products to niyoos';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productIDs = $this->argument('products');
        $warehouseId = 2;
        $productQuery = Product::active()->inventoryCount([$warehouseId])
                        ->whereHas('tags', function($query) {
                            $query->where('id', 14);
                        });
        if (!empty($productIDs)) {
            $productQuery = $productQuery->whereIn('id', $productIDs);
        }
        $products = $productQuery->get();
        if($products->isNotEmpty()) {
            foreach($products as $product){
                try {
                    event(new MallProductsInventorySync($product));
                }
                catch(\Exception $e){
                    \Log::error("Product Sync PID: $product->id | ". $e->getMessage());
                }
            }
        }
        
    }
}
