<?php

namespace Niyotail\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\InventoryTransaction;

class RemoveDuplicatesFromInventoryTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:inventory-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove duplicates from inventory transactions';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $inventoryTransactions = InventoryTransaction::where('type', InventoryTransaction::TYPE_SALE_CANCELLATION)->get();
//
//        $groupedByValue = $inventoryTransactions->groupBy('source_id');
//
//        $duplicates = $groupedByValue->filter(function ($groups) {
//            return $groups->count() > 1;
//        });
//        $this->info('Removing Transactions...');
//        $toBeDeletedIds = collect([]);
//        foreach ($duplicates as $duplicateTransactions){
//            $transactionsToBeRemoved = $duplicateTransactions->sortByDesc('id');
//            //Keep only 1 transaction
//            $transactionsToBeRemoved->pop();
//            $toBeDeletedIds->push($transactionsToBeRemoved->pluck('id')->toArray());
//        }
//        $chunked = array_chunk($toBeDeletedIds->flatten()->toArray(),500);
//        foreach ($chunked as $ids){
//            InventoryTransaction::whereIn('id', $ids)->delete();
//        }
//        $this->comment('Duplicate transactions removed');
    }
}
