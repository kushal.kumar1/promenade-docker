<?php

namespace Niyotail\Console;

use Niyotail\Console\Commands\GenerateInventorySnapshot;
use Niyotail\Console\Commands\ProductInventorySnapshot;
use Niyotail\Console\Commands\GenerateStoreLedger;
use Niyotail\Console\Commands\MarkPutawayItemsSalable;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    const TIMEZONE = 'Asia/Kolkata';
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        GenerateInventorySnapshot::class,
        GenerateStoreLedger::class,
        ProductInventorySnapshot::class,
        MarkPutawayItemsSalable::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inventory:snapshot')
            ->cron('0 */4 * * *')
            ->unlessBetween('00:15', '08:00')
            ->timezone(self::TIMEZONE);

        $schedule->command('snapshot:product-inventory')
            ->cron('0 23 * * *')
            ->unlessBetween('00:15', '08:00')
            ->timezone(self::TIMEZONE);

        $schedule->command('generate:ledger')
          ->dailyAt('04:00')
          ->timezone(self::TIMEZONE);

//        $schedule->command('generate:rate-credit-note')
//            ->weeklyOn(1, '06:30')
//            ->timezone(self::TIMEZONE);

//        $schedule->command('generate:sale-orders')
//            ->everyFiveMinutes()
//            ->timezone('Asia/Kolkata');
//            ->between('9:00','15:00');

//        $schedule->command('generate:direct-purchase-order')
//            ->everyThirtyMinutes()
//            ->timezone('Asia/Kolkata');
//
//        $schedule->command('process:direct-purchase-orders')
//            ->hourly()
//            ->timezone('Asia/Kolkata');

        $schedule->command('sync:products')
            ->everyTenMinutes()
            ->timezone('Asia/Kolkata');

        $schedule->command('sync:prices')
            ->dailyAt('03:00')
            ->timezone(self::TIMEZONE);

        $schedule->command('modification-requests:process')
            ->everyThirtyMinutes()
            ->timezone(self::TIMEZONE);

        $schedule->command('mark:ready-for-sale')
            ->everyFiveMinutes()
            ->timezone('Asia/Kolkata');

        $schedule->command('sync:mall-inventory')
            ->cron('0 */8 * * *')
            ->timezone(self::TIMEZONE);

        $schedule->command('close:migrations')
            ->everyFiveMinutes()
            ->timezone(self::TIMEZONE);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');
        require base_path('routes/console.php');
    }
}
