<?php

namespace Niyotail\Auth;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;


class OtpUserProvider extends EloquentUserProvider implements UserProvider
{

    public function __construct(HasherContract $hasher, $model)
    {
        parent::__construct($hasher, $model);
    }


    public function validateCredentials(AuthenticatableContract $user, array $credentials)
    {
        if (!empty($credentials['otp'])) {
            $otp = $credentials['otp'];
            if ($user->getOtp() == $otp) {
                if (strtotime('now') > strtotime($user->getOtpExpiry()))
                    return false;
                return TRUE;
            }
            return false;
        }
        return false;
    }

    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials)) {
            return;
        }

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // Eloquent User "model" that will be utilized by the Guard instances.
        $query = $this->createModel()->newQuery();

        foreach ($credentials as $key => $value) {
            if (! Str::contains($key, 'otp')) {
                $query->where($key, $value);
            }
        }
        return $query->first();
    }

} 