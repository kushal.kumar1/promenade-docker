<?php

namespace Niyotail\Auth;

interface OtpAuthenticateContract
{
    public function getOtp();

    public function getOtpExpiry();
}