<?php

namespace Niyotail\Services;

use Niyotail\Models\TaxClass;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;

class TaxClassService extends Service
{
    public function create(Request $request)
    {
        $taxClass = $this->processData($request, new TaxClass());
        $taxClass->save();
        return $taxClass;
    }
    
    public function update(Request $request)
    {
        $taxClass = TaxClass::find($request->id);
        if (empty($taxClass)) {
            throw new ServiceException("Tax Class doesn't exist !");
        }
        $taxClass = $this->processData($request, $taxClass);
        $taxClass->save();
        return $taxClass;
    }

    private function processData(Request $request, TaxClass $taxClass)
    {
        $taxClass->name = $request->name;
        $taxClass->status = $request->status;
        return $taxClass;
    }
}
