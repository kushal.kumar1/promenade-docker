<?php


namespace Niyotail\Services;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\Utils;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\PurchaseInvoiceCartShortItem;
use Niyotail\Models\PurchaseOrder;
use Niyotail\Models\Shipment;
use Niyotail\Models\Store;
use Niyotail\Models\StorePurchaseInvoice;
use Niyotail\Models\Vendor;

class PurchaseInvoiceCartService
{
    protected $purchaseInvoiceService = null;
    protected $purchaseOrderService;
    protected $purchaseInvoiceGrnService;
    protected $purchaseInvoiceCartItemService;

    public function __construct()
    {
        $this->purchaseInvoiceService = new PurchaseInvoiceService();
        $this->purchaseOrderService = new PurchaseOrderService();
        $this->purchaseInvoiceGrnService = new PurchaseInvoiceGrnService();
        $this->purchaseInvoiceCartItemService = new PurchaseInvoiceCartItemService();
    }

    public function getCartByCartId($id)
    {
        return PurchaseInvoiceCart::findOrFail($id);
    }

    public function getOrCreateCartByVendorId($vendorId)
    {
        $cart = PurchaseInvoiceCart::Pending()->VendorId($vendorId)->first();
        if (empty($cart)) {
            $cart = new PurchaseInvoiceCart();
            $cart->vendor_id = $vendorId;
            $cart->save();
        }

        return $cart;
    }

    public function createVendorCart($vendorId, $warehouseId = null, $type = 'default')
    {
        $selectedWarehouse = !empty($warehouseId) ? $warehouseId : app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        $cart = new PurchaseInvoiceCart();
        $cart->vendor_id = $vendorId;
        $cart->type = $type;
        $cart->status = PurchaseInvoiceCart::STATUS_DRAFT;
        $cart->warehouse_id = $selectedWarehouse;
        $cart->is_direct_store_purchase = $selectedWarehouse == 1;
        $cart->save();
        return $cart;
    }

    public function updatePurchaseInvoiceCart($request, PurchaseInvoiceCart $cart)
    {
        $request->validate([
            'total_invoice_value' => 'required|numeric'
        ]);

        $cart->vendor_id = $request->vendor_id;
        $cart->vendor_address_id = $request->vendor_address_id;
        $cart->invoice_date = $request->invoice_date;
        $cart->vendor_ref_id = $request->vendor_ref_id;
        $cart->platform_id = $request->platform_id;
        $cart->total_invoice_value = $request->total_invoice_value;
        $cart->is_direct_store_purchase = $request->has('is_direct_store_purchase') ? $request->is_direct_store_purchase : 0;

        if ($request->has('source')) {
            $cart->source = $request->source;
        }
        if ($cart->is_direct_store_purchase) {
            $cart->store_id = $request->store_id;
        } else $cart->store_id = null;
        $cart->is_local_purchase = $request->has('is_local_purchase') ? $request->is_local_purchase : 0;
        if ($cart->is_local_purchase) {
            $cart->vendor_ref_id = $this->getCurrentVendorReferenceId();
            $cart->is_direct_store_purchase = 1;
        }
        $cart->warehouse_id = $request->warehouse_id;
        if ($request->has('eway_number')) {
            $cart->eway_number = $request->eway_number;
        }
        $cart->save();
        return $cart;
    }

    public function createInvoiceFromCart(PurchaseInvoiceCart $cart, $deliveryDate, $ewayNumber, $tcs)
    {
        return DB::transaction(function () use ($cart, $deliveryDate, $ewayNumber, $tcs) {

            if ($cart->status == PurchaseInvoiceCart::STATUS_INVOICE_GENERATED) {
                throw new ServiceException('Purchase Invoice also generated for this cart');
            }

            $invoiceReq = new Request();

            $reqData = $cart->toArray();
            $reqData['eway_number'] = $ewayNumber;
            $reqData['tcs'] = $tcs;
            $invoiceReq->replace($reqData);
            $invoice = $this->purchaseInvoiceService->createPurchaseInvoice($invoiceReq);

            $cartItems = $cart->items;

            if ($cartItems->where('received_quantity', '>', 0)->count()
                && empty($deliveryDate)
            ) {
                throw new \Exception('Delivery date is required with received products');
            }
            if ($ewayNumber) {
                $cart->eway_number = $ewayNumber;
            }
            if ($cart->getCartTotal() >= 50000 && !$ewayNumber) {
                throw new \Exception('EWAY number required for invoices with value equal to or more than INR 50,000');
            }
            if (empty($cart->purchase_order_id)) {
                // create purchaseOrder if cart is not associated to any po
                $purchaseOrder = $this->purchaseOrderService->createPurchaseOrderFromInvoiceCart($cart);
                $poId = $purchaseOrder->id;
                $cart->purchase_order_id = $poId;
                $cart->save();

                $reqData['purchase_order_id'] = $poId;
                foreach ($cartItems as $cartItem) {
                    if (!$cartItem->is_requested) {
                        // add purchase order item
                        $orderItem = $this->purchaseOrderService->createOrderItemFromInvoiceCart($poId, $cartItem);
                        $cartItem->purchase_order_item_id = $orderItem->id;
                    }
                }

                $purchaseOrder->status = PurchaseOrder::STATUS_INVOICE_GENERATED;
                $purchaseOrder->save();
            }
            $cartItemsArr = $cartItems->toArray();

            $this->purchaseInvoiceService->addInvoiceItems($invoice, $cartItemsArr, $cart);
            $this->purchaseInvoiceService->addShortItems($invoice, $cart);

            $cart->status = PurchaseInvoiceCart::STATUS_INVOICE_GENERATED;
            $cart->save();

            if ($cartItems->where('received_quantity', '>', 0)->count() > 0) {
                $this->purchaseInvoiceGrnService->generateGrnForReceivedItems($invoice, $deliveryDate);
            }

            if ($cart->is_direct_store_purchase) {
                /* Create entry in store purchase invoice */
                $storePurchaseInvoice = new StorePurchaseInvoice();
                $storePurchaseInvoice->purchase_invoice_id = $invoice->id;
                $storePurchaseInvoice->store_id = $cart->store_id;
                $storePurchaseInvoice->save();
            }

            return $invoice;
        });
    }

    public function createShortProduct($cartId, Request $request)
    {
        $product = $this->parseShortProductFromRequest($request);
        $invoiceCost = $product['effective_cost'] + $product['post_tax_discount'];
        $product['invoice_cost'] = $invoiceCost;
        $item = new PurchaseInvoiceCartShortItem();
        $item->cart_id = $cartId;
        foreach ($product as $key => $val) {
            $item->{$key} = $val;
        }

        $item->save();
        return $item;
    }

    public function removeShortProduct(Request $request)
    {
        $product = PurchaseInvoiceCartShortItem::id($request->get('item_id'))->cart($request->get('cart_id'))->firstOrFail();
        $product->delete();
    }

    private function parseShortProductFromRequest(Request $request)
    {
        $discount = $request->get('discount') > 0 ? $request->get('discount') : 0;
        $ptd = $request->get('post_tax_discount') > 0 ? $request->get('post_tax_discount') : 0;
        return [
            'name' => $request->get('name'),
            'hsn' => $request->get('hsn'),
            'mrp' => $request->get('mrp'),
            'quantity' => $request->get('quantity'),
            'discount' => $discount,
            'tax_class' => $request->get('tax_class'),
            'post_tax_discount' => $ptd,
            'effective_cost' => $request->get('effective_cost')
        ];
    }

    public function removeInvoiceCart($cartId)
    {
        $cart = PurchaseInvoiceCart::find($cartId);
        $cart->delete();
    }

    public function getCurrentVendorReferenceId()
    {
        $currentFinancialYear = Utils::getCurrentFinancialYear();

        $purchaseInvoiceCart = PurchaseInvoiceCart::where('is_local_purchase', 1)
            ->where('vendor_ref_id', 'like', "%LCL/$currentFinancialYear/%")
            ->orderBy('vendor_ref_id', 'desc')->first();

        if (empty($purchaseInvoiceCart)) {
            $purchaseInvoice = PurchaseInvoice::where('is_local_purchase', 1)->orderBy('invoice_date', 'desc')->first();
            if ($purchaseInvoice) {
                return ++$purchaseInvoice->vendor_ref_id;
            }
            return "LCL/$currentFinancialYear/000001";
        }

        return ++$purchaseInvoiceCart->vendor_ref_id;
    }

    /**
     *
     * This function is used to create purchase invoice for stock transfers
     * @param Shipment $shipment
     * @return void
     */
    public function createCartFromShipment(Shipment $shipment)
    {
        $shipment->loadMissing('order.items.itemInventory.inventory');
        $order = $shipment->order;
        $vendor = Vendor::with('addresses')->where('warehouse_id', $order->warehouse_id)->first();
        $destinationWarehouse = Store::find($order->store_id)->warehouse_id;
        $cart = $this->createVendorCart($vendor->id, $destinationWarehouse, PurchaseInvoiceCart::TYPE_STOCK_TRANSFER);
        $cart->shipment_id = $shipment->id;
        $cart->save();
        $cartRequest = new Request();
        $data = [
            'total_invoice_value' => $order->total,
            'vendor_id' => $vendor->id,
            'vendor_address_id' => $vendor->addresses->first()->id,
            'vendor_ref_id' => "STO-PI-$shipment->id",
            'platform_id' => 16,
            'warehouse_id' => $destinationWarehouse,
            'invoice_date' => Carbon::parse($shipment->updated_at)->toDateString()
        ];
        $cartRequest->replace($data);
        $this->updatePurchaseInvoiceCart($cartRequest, $cart);
    }
}