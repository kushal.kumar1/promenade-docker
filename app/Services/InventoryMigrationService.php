<?php

namespace Niyotail\Services;

use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Storage;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Models\InventoryMigration\InventoryMigrationItemInventory;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Putaway\PutawayService;
use Niyotail\Events\Putaway\PutawayItemAdded;

class InventoryMigrationService extends Service
{
    public function create(int $warehouseId, Employee $employee): InventoryMigration
    {
        $migration = new InventoryMigration();
        $migration->warehouse_id = $warehouseId;
        $migration->status = InventoryMigration::STATUS_OPEN;
        $migration->employee_id = $employee->id;
        $migration->save();
        return $migration;
    }

    public function close(InventoryMigration $migration): InventoryMigration
    {
        if (!$migration->canClose()) throw new ServiceException("Inventory Migration cannot be closed! ");
        $migration->status = InventoryMigration::STATUS_CLOSED;
        $migration->save();
        return $migration;
    }

    private function processItemData(int $productId,int $quantity,int $fromStorageId,int $assignedEmployeeId, $toStorageId = null):InventoryMigrationItem
    {
        $migrationItem = new InventoryMigrationItem();
        $migrationItem->product_id = $productId;
        $migrationItem->quantity = $quantity;
        $migrationItem->from_storage_id = $fromStorageId;
        $migrationItem->to_storage_id = $toStorageId;
        $migrationItem->employee_id = $assignedEmployeeId;
        $migrationItem->status = InventoryMigrationItem::STATUS_PENDING;
        return $migrationItem;
    }

    public function addItem(InventoryMigration $migration, int $productId, int $quantity, int $fromStorageId, int $assignedEmployeeId, $toStorageId = null)
    {
        return DB::transaction(function () use ($migration, $productId, $quantity, $fromStorageId, $assignedEmployeeId, $toStorageId) {
            if ($migration->status != InventoryMigration::STATUS_OPEN) {
                throw new ServiceException("Cannot add the item! InventoryMigration has been $migration->status!");
            }

            $fromStorage = Storage::where('warehouse_id', $migration->warehouse_id)->find($fromStorageId);
            $toStorage = Storage::find($toStorageId);
            if (empty($fromStorage)) {
                throw new ServiceException("Cannot add the item! Source Storage id incorrect for warehouse!");
            }

            if (!empty($toStorageId)) {
                $toStorage = Storage::where('warehouse_id', $migration->warehouse_id)->find($toStorageId);
                if (empty($toStorage)) {
                    throw new ServiceException("Cannot add the item! Destination Storage id incorrect for warehouse!");
                }
            }

            $inventories = FlatInventory::getStorageInventoriesForUpdate($migration->warehouse_id, $productId, FlatInventory::STATUS_READY_FOR_SALE, $fromStorageId, $quantity);
            if (count($inventories) < $quantity) {
                throw new ServiceException("Migration quantity not available! Try again!");
            }

            $migrationItem = $this->processItemData($productId, $quantity, $fromStorageId, $assignedEmployeeId, $toStorageId);
            $migration->items()->save($migrationItem);
            $migrationItem->save();

            $migrationItemFlatInventories = $inventories->pluck('id')->flatten()->toArray();
            //Lock these inventories. Change status HOLD
            (new FlatInventoryService())->markGoodInventories($migrationItemFlatInventories, FlatInventory::STATUS_HOLD);
            $migrationItemFlatInventories = [];
            foreach($inventories as $inventory) {
                $migrationItemFlatInventories [] = [
                    'inventory_migration_item_id' => $migrationItem->id,
                    'flat_inventory_id' => $inventory->id,
                ];
            }
            foreach (array_chunk($migrationItemFlatInventories, 1000) as $chunkedMigrationItemFlatInventories) {
                InventoryMigrationItemInventory::insert($chunkedMigrationItemFlatInventories);
            }

            return $migrationItem;
        });
    }


    public function addItemToCart($request, $employee)
    {
        return DB::transaction(function () use ($request, $employee) {
            $migrationItem = InventoryMigrationItem::where(['employee_id' => $employee->id,
                                        'product_id' => $request->product_id,
                                        'from_storage_id' => $request->source_storage_id]
                                      )->where('status', '!=', InventoryMigrationItem::STATUS_COMPLETED)
                                      ->find($request->migration_item_id);
            if (empty($migrationItem)) {
                throw new ServiceException('Invalid item selected for migration!');
            }
            if (!empty($migrationItem->putawayItems->where('placed_quantity', null)->first())) {
                throw new ServiceException('Item already added to cart!');
            }
            $request->merge(['destination_storage_id' => $migrationItem->to_storage_id, 'migration_id' => $migrationItem->inventory_migration_id]);

            $placedQuantity = $migrationItem->putawayItems->sum('placed_quantity');
            if ($request->quantity > ($migrationItem->quantity - $placedQuantity)) {
                throw new ServiceException('Can not add the required quantity to cart. It is exceeding total assigned migration quantity!');
            }

            $putawayService = new PutawayService();
            $putaway = Putaway::byWarehouseEmployee($request->warehouse_id, $employee->id)
                              ->where(['status' => Putaway::STATUS_OPEN, 'inventory_migration_id' => $request->migration_id])
                              ->first();
            if (empty($putaway)) {
                $putaway = $putawayService->create($request->warehouse_id, $employee, $request->migration_id);
            }
            $putawayItem = $putawayService->addItem($putaway, $request);
            $putaway->load('items.product', 'items.storage');

            $migrationItem->status = InventoryMigrationItem::STATUS_PROCESSING;
            $migrationItem->save();
            return $putaway;
        });
    }


    /**
     * @throws ServiceException
     *
     * TODO: Raise an event MigrationItemCompleted and close migration automatically on event.
     */
    public function markItemStatusComplete(InventoryMigrationItem $migrationItem)
    {
        if($migrationItem->quantity != $migrationItem->putawayItems->sum('placed_quantity')){
            $migrationItem->status = InventoryMigrationItem::STATUS_PARTIAL;
            // throw new ServiceException("Please place the remaining quantity of item before marking complete!");
        } else {
            $migrationItem->status = InventoryMigrationItem::STATUS_COMPLETED;
        }
        $migrationItem->save();

        $migrationItem->loadMissing('inventoryMigration');
        $migration = $migrationItem->inventoryMigration;
    }
}
