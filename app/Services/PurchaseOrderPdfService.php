<?php


namespace Niyotail\Services;


use Barryvdh\Snappy\Facades\SnappyPdf;
use Niyotail\Models\PurchaseOrder;

class PurchaseOrderPdfService
{
    public function generatePdf(PurchaseOrder $purchaseOrder)
    {
        $vendor = $purchaseOrder->vendor;
        $vendorAddress = $purchaseOrder->vendorAddress;
        $warehouse = $purchaseOrder->warehouse;
        $items = $purchaseOrder->items;
        if(!$items->count())
        {
            throw new \Exception('No purchase order items found to be used in invoice');
        }

        $headerMargin = "90mm";

        $header = view()->make('pdf.purchase_order.header', compact('purchaseOrder', 'vendor', 'vendorAddress'))->render();

        return SnappyPdf::loadView('pdf.purchase_order.sales',
                [
                    'purchaseOrder' => $purchaseOrder,
                    'vendor' => $vendor,
                    'vendor_address' => $vendorAddress,
                    'warehouse' => $warehouse,
                    'items' => $items
                ]
            )
            ->setOption('margin-top', $headerMargin)
            ->setOption('margin-bottom', '65mm')
            ->setOption('header-html', $header)
//            ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }
}