<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\CustomBarcode;
use Niyotail\Models\Inventory;
use Niyotail\Models\Product;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Attribute;
use Niyotail\Models\ProductGroup;
use Niyotail\Models\CategoryL2;
use Niyotail\Models\CommissionTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Tag;


class ProductService extends Service
{
    public function create(Request $request)
    {
        $product = new Product();
        $product = DB::transaction(function () use ($product, $request) {
            $product = $this->processData($product, $request);
            $product->status = 0;
            $product->save();

            $product->attributes()->sync($request->get('attributes'));
            $product->tags()->sync($request->get('tags'));
            $product->warehouseRacks()->sync($request->get('racks'));

            /* Activate product if all required params are present */
            if ($product->canActivate() && $request->status == 1) {
                $product->status = 1;
                $product->save();
            }
        });
        return $product;
    }

    public function createFromRequest(ProductRequest $productRequest)
    {
        $barcode = $productRequest->getBarcode();

        // search product by barcode
        $productByBarcode = Product::barcode($barcode)->first();
        if (!empty($productByBarcode)) {
            // throw new \Exception('A product with given barcode '.$barcode.' already exists.');
        }

        return DB::transaction(function () use ($productRequest) {

            $freeBarcode = null;
            if ($productRequest->auto_assign_barcode) {
                $freeBarcode = CustomBarcode::whereNull('product_id')->first();
                $barcode = $freeBarcode->nt_barcode;
            } else {
                $barcode = $productRequest->barcode;
            }

            // create new product
            $product = new Product();
            $product->name = $productRequest->getName();
            $product->slug = $this->createSlug(Product::class, $productRequest->getName());
            $product->description = $productRequest->getDescription();
            $product->uom = 'pc';
            $product->hsn_sac_code = $productRequest->getHsnCode();
            $product->tax_class_id = $productRequest->getTaxClassId();
            $product->allow_back_orders = 0;
            $product->status = 0;
            $product->brand_id = $productRequest->getBrandId();
            $product->barcode = preg_replace('/\s+/', '', $barcode);
            $product->cl4_id = $productRequest->getCl4Id();
            $product->save();
            $product->collections()->sync([$productRequest->getCollectionId()]);

            if (!empty($freeBarcode)) {
                $freeBarcode->product_id = $product->id;
                $freeBarcode->save();
            }

            $productId = $product->id;

            $variants = array(
                [
                    'product_id' => $productId,
                    'value' => 'unit',
                    'uom' => $productRequest->getUom(),
                    'mrp' => $productRequest->getMrp(),
                    'quantity' => 1
                ]
            );

            if (!empty($productRequest->getOuterUnits())) {
                $outer = [
                    'product_id' => $productId,
                    'value' => 'outer',
                    'uom' => $productRequest->getUom(),
                    'mrp' => $productRequest->getMrp() * $productRequest->getOuterUnits(),
                    'quantity' => $productRequest->getOuterUnits()
                ];
                array_push($variants, $outer);
            }

            if (!empty($productRequest->getCaseUnits())) {
                $case = [
                    'product_id' => $productId,
                    'value' => 'case',
                    'uom' => $productRequest->getUom(),
                    'mrp' => $productRequest->getMrp() * $productRequest->getCaseUnits(),
                    'quantity' => $productRequest->getCaseUnits()
                ];
                array_push($variants, $case);
            }

            foreach ($variants as $variant) {
                $variant['sku'] = $variant['product_id'] . '-' . $variant['value'];
                $productVariant = ProductVariant::firstOrNew($variant);
                $productVariant->save();
            }

            if (empty($productRequest->group_id)) {
                // create new group
                $group = new ProductGroup();
                $group->name = $product->name;
                $group->brand_id = $product->brand_id;
//                $group->collection_id = $productRequest->getCollectionId();
                $group->save();
            } else {
                $group = ProductGroup::findOrFail($productRequest->group_id);
            }

            // sync group
            $product->group_id = $group->id;
            $product->save();

            // set created product id in request table
            $productRequest->product_id = $product->id;
            $productRequest->status = ProductRequest::STATUS_CREATED;
            $productRequest->save();
            return $product;
        });
    }


    public function update(Request $request)
    {
        $product = Product::find($request->id);
        if (empty($product)) {
            throw new ServiceException("Product doesn't exist !");
        }
        return DB::transaction(function () use ($product, $request) {

            if(Gate::allows('update-product') || Gate::allows('update-product-racks')) {
                $product->warehouseRacks()->sync($request->get('racks'));
            }

            if(Gate::allows('update-product')) {
                $product = $this->processData($product, $request, self::TYPE_UPDATE);
                $product->save();

                $product->attributes()->sync($request->get('attributes'));
                $product->tags()->sync($request->get('tags'));


                if ($request->filled('inventory') && !empty(array_filter($request->inventory))) {
                    $product->load('inventories');
                    foreach ($request->inventory as $warehouseId => $value) {
                        $inventory = $product->inventories->where('warehouse_id', $warehouseId);
                        if ($inventory->isNotEmpty()) {
                            $inventoryModel = $inventory->first();
                            $inventoryModel->quantity = $value['quantity'];
                            $inventoryModel->save();
                            return;
                        }

                        $inventory = new Inventory([
                            'warehouse_id' => $warehouseId,
                            'quantity' => $value['quantity']
                        ]);
                        $product->inventories()->save($inventory);
                    }
                }

                if ($product->canActivate() && $request->status == 1) {
                    $product->status = 1;
                } elseif ($request->status == 0) {
                    $product->status = $request->status;
                }
                $product->save();
            }

            return $product;
        });
    }

    /**
     * @param Request $request
     * @return mixed|Product
     */
    public function createImportData(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $product = new Product();
            $product = $this->processImportData($product, $request);

            if (empty($product->group_id)) {
                $group = $this->createGroup($request);
                $product->group_id = $group->id;
            }
            $product->status = 0;
            $product->save();

            $this->assignNewBarcode($product);

            $attributes = $this->processImportAttributes($request);
            if (count($attributes) > 0) {
                $product->attributes()->sync($attributes);
            }

            if (isset($request->mrp)) {
                $this->createProductVariant($product->id, $request);
            }

            if (!empty($request->tag_ids)) {
                $tags = Tag::whereIn('id', $request->tag_ids)->where('type', '!=', Tag::TYPE_PRODUCT)->get();
                if($tags->isNotEmpty()){
                    throw new ServiceException('Tags should be of type product');
                }
                $product->tags()->sync($request->tag_ids);
            }

            if (isset($request->commission_tag_id) && !empty(CommissionTag::find($request->commission_tag_id))) {
                $product->commissionTags()->sync($request->commission_tag_id);
            }

            if ($product->canActivate() && $request->status == 1) {
                $product->status = 1;
                $product->save();
            }

            return $product;
        });
    }

    private function createGroup(Request $request)
    {
        $group = new ProductGroup();
        $group->name = $request->name;
        $group->brand_id = $request->brand_id;
        $group->save();
        return $group;
    }

    public function createProductVariant($id, Request $request)
    {
        $mrp = $request->mrp;
        $variantRequest = new Request();
        $variantRequest->merge(['mrp' => $mrp,
            'min_price' => !empty($request->min_price) ? $request->min_price : $mrp,
            'max_price' => $request->mrp,
            'price' => $mrp,
            'value' => 'unit',
            'quantity' => 1,
            'moq' => 1,
            'sku' => $id . '-unit',
        ]);

        $service = new ProductVariantService();
        $service->create($id, $variantRequest);
    }

    public function updateImportData(Request $request)
    {
        $product = Product::find($request->id);
        if (empty($product)) {
            throw new ServiceException("Product doesn't exist !");
        }
        return DB::transaction(function () use ($product, $request) {
            $product = $this->processImportData($product, $request, self::TYPE_UPDATE);
            $product->save();

            $this->assignNewBarcode($product);

            $attributes = $this->processImportAttributes($request);
            if (count($attributes) > 0) {
                $product->attributes()->sync($attributes);
            }

            if (isset($request->commission_tag_id) && !empty(CommissionTag::find($request->commission_tag_id))) {
                $product->commissionTags()->sync($request->commission_tag_id);
            }

            if (!empty($request->tag_ids)) {
                $tags = Tag::whereIn('id', $request->tag_ids)->where('type', '!=', Tag::TYPE_PRODUCT)->get();
                if($tags->isNotEmpty()){
                    throw new ServiceException('Tags should be of type product');
                }
                $product->tags()->sync($request->tag_ids);
            }

            if ($product->canActivate() && $request->status == 1) {
                $product->status = 1;

            } elseif ($request->status == 0) {
                $product->status = $request->status;
            }
            $product->save();
            return $product;
        });
    }

    private function processImportAttributes($request)
    {
        $attributes = [];
        $requestArray = $request->all();
        foreach ($requestArray as $key => $value) {
            if (substr($key, 0, 9) == "attribute") {
                $attributeName = explode('_', $key)[1];
                $attribute = Attribute::where('name', '=', $attributeName)->first();
                if (!empty($attribute)) {
                    $attributes[$attribute->id]['value'] = $value;
                }
            }
        }
        return $attributes;
    }

    private function processImportData(Product $product, $request, $type = self::TYPE_CREATE)
    {
        if ($type == self::TYPE_CREATE) {
            $product->slug = $this->createSlug(Product::class, $request->name);
        }
        $product->name = $request->has('name') ? preg_replace('/[^a-zA-Z0-9-.()&@%]/', " ", $request->name) : $product->name;
        $product->description = isset($request->description) ? $request->description : $product->description;
        $product->meta_title = isset($request->meta_title) ? $request->meta_title : $product->meta_title;
        $product->meta_description = isset($request->meta_description) ? $request->meta_description : $product->meta_description;
        if (empty($product->published_at) && $product->status == 1) {
            $product->published_at = Carbon::now()->toDateTimeString();
        }
        $product->hsn_sac_code = isset($request->hsn_sac_code) ? $request->hsn_sac_code : $product->hsn_sac_code;
        $product->code = isset($request->code) ? $request->code : $product->code;
        $product->tax_class_id = isset($request->tax_class_id) ? $request->tax_class_id : $product->tax_class_id;
        $product->brand_id = isset($request->brand_id) ? $request->brand_id : $product->brand_id;
        $product->barcode = isset($request->barcode) ? preg_replace('/\s+/', '', $request->barcode) : $product->barcode;
        $product->uom = isset($request->uom) ? $request->uom : $product->uom;
        $product->cl4_id = isset($request->cl4_id) ? $request->cl4_id : $product->cl4_id;
        $product->group_id = isset($request->group_id) ? $request->group_id : ($product->group_id ? $product->group_id : null);
        return $product;
    }

    private function processData(Product $product, $request, $type = self::TYPE_CREATE)
    {
        $hsnCode = $request->hsn_sac_code;
        if (!empty($hsnCode)) {
            $hsnCode = str_pad($hsnCode, 8, '0', STR_PAD_RIGHT);
        }

        $product->name = $request->name;
        if ($type == self::TYPE_CREATE) {
            $product->slug = $this->createSlug(Product::class, $request->name);
        }
        $product->description = $request->description;
        $product->brand_id = $request->brand_id;
        $product->hsn_sac_code = $hsnCode;
        $product->code = $request->code;
        $product->tax_class_id = $request->tax_class_id;
        $product->meta_title = $request->meta_title;
        $product->meta_description = $request->meta_description;
        $product->barcode = preg_replace('/\s+/', '', $request->barcode);

        $product->uom = isset($request->uom) ? $request->uom : $product->uom;
        $product->allow_back_orders = $request->allow_back_orders;
        $product->cl4_id = isset($request->cl4_id) ? $request->cl4_id : $product->cl4_id;
        $product->group_id = $request->group_id;
        if (empty($product->published_at) && $product->status == 1) {
            $product->published_at = Carbon::now()->toDateTimeString();
        }
        return $product;
    }

    public static function processProductToHideVariantsForUom($products)
    {
        return $products->map(function ($product) {
            if ($product->uom !== 'pc' && $product->variants->where('value', 'unit')->count() > 0) {
                $product->variants = $product->variants->filter(function ($value, $key) {
                    return ($value->value === 'unit');
                });
            }
            return $product;
        });
    }

    private function mapGroupAndCl2($product, Request $request)
    {
        if (isset($request->cl2_id) && !empty(CategoryL2::find($request->cl2_id))) {
            $this->mapCategoryL2($product->id, $request);
        }

        if (!$request->has('group_id')) {
            $group = new ProductGroup();
            $group->name = $product->name;
            $group->brand_id = $product->brand_id;
            $group->save();
        }

//        $pgMap = GroupProduct::where('product_id', $product->id)->first();
//        if($pgMap) {
//            $pgMap->group_id = $group->id;
//        }
//        else {
//            $pgMap = new GroupProduct();
//            $pgMap->group_id = $group->id;
//            $pgMap->product_id = $product->id;
//        }
//        $pgMap->save();

        if (isset($request->cl2_id) && !empty(CategoryL2::find($request->cl2_id))) {
            $this->mapCategoryL2($product->id, $request);
        }
    }

    /**
     * @param Product $product
     * @return bool|Product
     */
    public function assignNewBarcode(Product $product)
    {
        if (!empty($product->barcode)) return true;

        /* @var CustomBarcode $freeBarcode */
        $freeBarcode = CustomBarcode::whereNull('product_id')->first();
        $barcode = $freeBarcode->nt_barcode;

        $freeBarcode->product_id = $product->id;
        $freeBarcode->save();

        $product->barcode = preg_replace('/\s+/', '', $barcode);
        $product->save();
        return $product;
    }

}
