<?php


namespace Niyotail\Services;


use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\File;
use Niyotail\Models\ProductRequest;

class ProductRequestService extends Service
{
    public function create(Request $request)
    {
        $productRequest = $this->processData($request, (new ProductRequest()));
        $productRequest->save();
        if ($request->has('file')) {
            $this->setFile($productRequest, $request);
        }
        return $productRequest;
    }

    private function processData(Request $request, ProductRequest $productRequest)
    {
        $productRequest->group_id = $request->group_id;
        $productRequest->is_requested_group = $request->is_requested_group;
        $productRequest->name = $request->name;
        $productRequest->barcode = $request->barcode;
        $productRequest->auto_assign_barcode = $request->auto_assign_barcode == "on";
        $productRequest->description = $request->description;
        $productRequest->brand_id = $request->brand_id;
        $productRequest->uom = $request->uom;
        $productRequest->unit_mrp = $request->mrp;
        $productRequest->outer_units = $request->outer_units;
        $productRequest->case_units = $request->case_units;
        $productRequest->tax_class_id = $request->tax_class_id;
        $productRequest->hsn_code = $request->hsn_code;
        $productRequest->source = $request->source;
        $productRequest->source_id = $request->source_id;
        $productRequest->cl4_id = $request->cl4_id;
        $productRequest->collection_id = $request->collection_id;
        if ($request->has('file')) {
            $this->setFile($productRequest, $request);
        }

        return $productRequest;
    }

    private function setFile(ProductRequest $productRequest, Request $request)
    {
        $image = $request->file('file');
        $targetDirectory = storage_path('app/product_request/' . $productRequest->id);
        $file = new File($image);
        $file->setName(uniqid() . "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $productRequest->file = $file->getName();
        $file->save();
        $productRequest->save();
    }

    public function update(Request $request)
    {
        $productRequest = ProductRequest::find($request->id);
        if (empty($productRequest)) {
            throw new ServiceException('Invalid product request!');
        }
        $productRequest = $this->processData($request, $productRequest);
        $productRequest->save();
        return $productRequest;
    }
}