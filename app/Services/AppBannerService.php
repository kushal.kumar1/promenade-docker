<?php

namespace Niyotail\Services;

use Niyotail\Models\AppBanner;
use Niyotail\Models\DashboardImage;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Niyotail\Helpers\File;
use Carbon\Carbon;

class AppBannerService extends Service
{
    public function create(Request $request)
    {
        $appBanner = $this->processData($request, new AppBanner());
        $appBanner->save();
        $this->saveImage($appBanner, $request);
        return $appBanner;
    }

    public function update(Request $request)
    {
        $appBanner = AppBanner::find($request->id);
        if (empty($appBanner)) {
            throw new ServiceException("App Banner doesn't exist !");
        }
        $appBanner = $this->processData($request, $appBanner);
        $appBanner->save();
        if ($request->has('file')) {
            $this->saveImage($appBanner, $request);
        }
        return $appBanner;
    }

    private function processData(Request $request, AppBanner $appBanner)
    {
        $appBanner->position = $request->position;
        $appBanner->valid_from =  $request->filled('valid_from') ? Carbon::parse($request->valid_from)->toDateTimeString() : null;
        $appBanner->valid_to =  $request->filled('valid_to') ? Carbon::parse($request->valid_to)->toDateTimeString() : null;
        $appBanner->status = $request->status;
        $appBanner->link = $request->link;
        return $appBanner;
    }

    private function saveImage($appBanner, Request $request)
    {
        $image = $request->file('file');
        $targetDirectory = public_path('media/app_banner/' . $appBanner->id);
        $file = new File($image);
        $file->setName(uniqid(). "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $appBanner->file = $file->getName();
        $appBanner->save();
    }

    public function createAgentDashboardImage(Request $request)
    {
        $dashboardImage = $this->processAgentDashboardImageData($request, new DashboardImage());
        $dashboardImage->save();
        $this->saveDashboardImage($dashboardImage, $request);
        return $dashboardImage;
    }

    public function updateAgentDashboardImage(Request $request)
    {
        $dashboardImage = DashboardImage::find($request->id);
        if (empty($dashboardImage)) {
            throw new ServiceException("Dashboard Image doesn't exist !");
        }
        $dashboardImage = $this->processAgentDashboardImageData($request, $dashboardImage);
        $dashboardImage->save();
        if ($request->has('file')) {
            $this->saveDashboardImage($dashboardImage, $request);
        }
        return $dashboardImage;
    }

    private function processAgentDashboardImageData(Request $request, DashboardImage $dashboardImage)
    {
        $dashboardImage->valid_from =  $request->filled('valid_from') ? Carbon::parse($request->valid_from)->toDateTimeString() : null;
        $dashboardImage->status = $request->status;
        return $dashboardImage;
    }

    private function saveDashboardImage($dashboardImage, Request $request)
    {
        $image = $request->file('file');
        $targetDirectory = public_path('media/dashboard_image/' . $dashboardImage->id);
        $file = new File($image);
        $file->setName(uniqid(). "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $dashboardImage->image = $file->getName();
        $dashboardImage->save();
    }
}
