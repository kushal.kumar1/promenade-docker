<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Niyotail\Exceptions\ServiceException;

use Niyotail\Jobs\SendOtpToEmployee;
use Niyotail\Models\Employee;

class EmployeeService extends Service
{
    public function resetPassword(Employee $employee, $newPassword)
    {
        $employee->password = bcrypt($newPassword);
        $employee->save();
        return true;
    }

    public function create(Request $request)
    {
        if ($this->isDuplicateEntry($request)) {
            return false;
        }
        $employee = $this->processData($request, new Employee());
        $employee->save();
        $warehouses = !empty($request->warehouses) ? $request->warehouses : [];
        $employee->warehouses()->sync($warehouses);
        return $employee;
    }


    /**
     * @throws ServiceException
     */
    public function isDuplicateEntry(Request $request, $id = null): bool
    {
        if($request->filled('mobile')){
            $employee =  Employee::where('mobile', "+91$request->mobile")
                ->where('id', '!=', $id)->first();
            if (!empty($employee)) {
                throw new ServiceException("Mobile Number Already Taken!");
            }
        }
        if($request->filled('email')){
            $employee = Employee::where('email', $request->email)->where('id', '!=', $id)->first();
            if (!empty($employee)) {
                throw new ServiceException("Email Already Taken");
            }
        }
        return false;
    }

    private function processData(Request $request, Employee $employee, $type = self::TYPE_CREATE)
    {
        $employee->name = $request->name;
        $employee->email = $request->email;
        if($request->filled('mobile')){
            $employee->mobile = "+91$request->mobile";
        }
        if ($type == self::TYPE_UPDATE) {
            $employee->status = $request->status;
            if ($request->has('change-password')) {
                $employee->password = bcrypt($request->password);
            }
        } else {
            $employee->password = bcrypt($request->password);
        }
        return $employee;
    }

    public function update(Request $request)
    {
        $employee = Employee::find($request->id);
        if (empty($employee)) {
            throw new ServiceException("Employee doesn't exist !");
        }
        if ($this->isDuplicateEntry($request, $request->id)) {
            throw new ServiceException("Duplicate Entry");
        }
        $employee = $this->processData($request, $employee, self::TYPE_UPDATE);
        $roles = $request->has('roles') ? $request->roles : [];
        $employee->roles()->sync($roles);
        $warehouses = !empty($request->warehouses) ? $request->warehouses : [];
        $employee->warehouses()->sync($warehouses);
        return $employee->save();
    }

    public function updateProfile(Employee $employee, $request)
    {
        $employee->name = $request->name;
        if ($request->has('change-password')) {
            $employee->password = bcrypt($request->password);
        }
        return $employee->save();
    }


    public function sendOtp(Employee $employee)
    {
        if (empty($employee->otp_expiry) || (Carbon::now()->toDateTimeString() > Carbon::parse($employee->otp_expiry)->toDateTimeString())) {
            $otp = generateOtp();
            if (App::environment() != 'production') {
                $otp = '123456';
            }
            $employee->otp = $otp;
            $employee->otp_expiry = Carbon::now()->addHour()->toDateTimeString();
            $employee->otp_sent_count = 1;
            $employee->save();
        } elseif ($employee->otp_sent_count < 3) {
            $employee->otp_sent_count += 1;
            $employee->save();
        } else {
            throw new ServiceException("You have reached maximum OTP limit. Please try after 1 hour !!");
        }
        dispatch(new SendOtpToEmployee($employee));
    }

    public function resetOtpDetails(Employee $employee)
    {
        $employee->otp = null;
        $employee->otp_expiry = null;
        $employee->otp_sent_count = 0;
        $employee->save();
    }
}
