<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Marketer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarketerService extends Service
{
    public function create(Request $request): Marketer
    {
        $marketer = $this->processData($request, new Marketer());
        $marketer->save();
        return $marketer;
    }

    /**
     * @throws ServiceException
     */
    public function update(Request $request): Marketer
    {
        $marketer = Marketer::find($request->id);
        if (empty($marketer)) {
            throw new ServiceException("Marketer doesn't exist !");
        }

        $marketer = $this->processData($request, $marketer);
        $marketer->save();
        return $marketer;
    }

    private function processData(Request $request, Marketer $marketer): Marketer
    {
        $marketer->name = $request->name;
        $marketer->alias = $request->alias;
        $marketer->code = $request->code;
        $marketer->pricing = $request->pricing;
        $marketer->level_id = $request->level_id;
        return $marketer;
    }
}
