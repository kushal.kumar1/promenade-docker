<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Niyotail\Jobs\SendOtpToCustomer;
use Niyotail\Models\DeviceParameter;
use Niyotail\Models\User;
use Niyotail\Models\Order;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

/**
 * Class CustomerService
 *
 * @package Niyotail\Services
 *
 */
class UserService extends Service
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool|\Niyotail\Models\User
     *
     */
    public function create(Request $request)
    {
        $user = new User();
        $this->isDuplicate($request);

        $user = $this->processData($user, $request, self::TYPE_CREATE);
        $user->save();
        return $user;
    }

    public function update(Request $request, User $user)
    {
        $this->isDuplicate($request, $user->id);
        $user = $this->processData($user, $request, self::TYPE_UPDATE);
        $user->save();
        return $user;
    }

    public function saveNotes(Request $request, User $user)
    {
        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            $user->notes = $request->notes;
            $user->save();
            return $user;
        }

        throw new ServiceException("Unauthorised Access");
    }

    public function saveStatus(Request $request, User $user)
    {
        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            $user->status = $request->status;
            $user->save();
            return $user;
        }
        throw new ServiceException("Unauthorised Access");
    }

    public function firstOrCreate($mobile):User
    {
        $user = User::firstOrCreate(['mobile' => $mobile]);
        return $user;
    }

    public function updateBadges(Request $request, User $user)
    {
        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            $user->badges()->sync($request->badges);
            return $user;
        }

        throw new ServiceException("Unauthorised Access");
    }

    public function addBadges(Request $request, User $user)
    {
        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            $user->badges()->attach($request->badges);
            return $user;
        }

        throw new ServiceException("Unauthorised Access");
    }

    public function toggleWish(Request $request, User $user)
    {
        $user->wishList()->toggle([$request->product_variant_id]);
        return $user;
    }

    public function addToWish(Request $request, User $user)
    {
        $user->wishList()->syncWithoutDetaching([$request->product_variant_id]);
        return $user;
    }

    private function isDuplicate($request, $id = null)
    {
        if ($request->has('mobile')) {
            $userQuery = User::where('mobile', $request->mobile);
            if (!empty($id)) {
                $userQuery->where('id', '!=', $id);
            }
            $user = $userQuery->first();
            if (!empty($user)) {
                throw new ServiceException("Customer with mobile number already exist!");
            }
        }
        return false;
    }

    private function processData(User $user, $request, $type)
    {
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if ($this->checkAuthority(self::AUTHORITY_ADMIN) || $type == self::TYPE_CREATE) {
            $user->mobile = $request->mobile;
        }
        $user->email = $request->email;
        return $user;
    }

    public function sendOtp(User $user)
    {
        if (empty($user->otp_expiry) || (Carbon::now()->toDateTimeString() > Carbon::parse($user->otp_expiry)->toDateTimeString())) {
            $user->otp = $otp = generateOtp();
            $user->otp_expiry = Carbon::now()->addHour()->toDateTimeString();
            $user->otp_sent_count = 1;
            $user->save();
        } elseif ($user->otp_sent_count < 3) {
            $user->otp_sent_count += 1;
            $user->save();
        } else {
            throw new ServiceException("You have reached maximum OTP limit. Please try after 1 hour !!");
        }
        dispatch(new SendOtpToCustomer($user));
    }

    public function resetOtpDetails(User $user)
    {
        $user->otp = null;
        $user->otp_expiry = null;
        $user->otp_sent_count = 0;
        $user->save();
    }

    public function verifyMobile(User $user)
    {
        if (!$user->isVerified()) {
            $user->verified = 1;
            $user->save();
        }
    }

    public function onBoarding(Request $request, User $user)
    {
        $user = DB::transaction(function () use ($request, $user) {
            $user = $this->processData($user, $request, self::TYPE_UPDATE);
            $storeService = new StoreService();
            $store = $storeService->createFromOnBoarding($request, $user);
            $storeService->addDefaultUser($store, $user);
            $user->save();
            return $user;
        });
        return $user;
    }

    public function storeDeviceParameters($request, $createdBy)
    {
        DB::transaction(function () use ($request, $createdBy) {
            $query = DeviceParameter::where('device_id', $request->device_id)->where('created_by_id', $createdBy->id);
            if ($createdBy instanceOf User) {
                $query->where('created_by_type', 'user');
            } else {
              $query->where('created_by_type', 'agent');
            }
            $deviceParams = $query->first();
            if (empty($deviceParams)) {
                $deviceParams = new DeviceParameter();
            }
            $deviceParams = $this->processDeviceData($deviceParams, $request);
            $deviceParams->createdBy()->associate($createdBy);
            $deviceParams->save();
        });
    }

    public function processDeviceData($deviceParams, $request)
    {
        $deviceParams->device_id = $request->device_id;
        if ($request->filled('build_no')) {
          $deviceParams->build_no = $request->build_no;
        }
        return $deviceParams;
    }
}
