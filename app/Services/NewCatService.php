<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Models\NewCatL1;
use Niyotail\Models\NewCatL2;
use Niyotail\Models\NewCatL3;
use Niyotail\Models\NewCatL4;

class NewCatService extends Service
{
    public function createCl1(Request $request): NewCatL1
    {
        $cl1 = new NewCatL1();
        $cl1->name = $request->name;
        $cl1->save();
        return $cl1;
    }

    public function createCl2(Request $request): NewCatL2
    {
        $cl2 = new NewCatL2();
        $cl2->name = $request->name;
        $cl2->newcat_l1_id = $request->cl1_id;
        $cl2->save();
        return $cl2;
    }

    public function createCl3(Request $request): NewCatL3
    {
        $cl3 = new NewCatL3();
        $cl3->name = $request->name;
        $cl3->newcat_l2_id = $request->cl2_id;
        $cl3->save();
        return $cl3;
    }

    public function createCl4(Request $request): NewCatL4
    {
        $cl4 = new NewCatL4();
        $cl4->name = $request->name;
        $cl4->newcat_l3_id = $request->cl3_id;
        $cl4->save();
        return $cl4;
    }
}