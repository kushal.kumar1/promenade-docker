<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\File;
use Niyotail\Models\Collection;
use Niyotail\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CollectionService extends Service
{
    public function create(Request $request)
    {
        $collection = new Collection();
        $parentNode = Collection::find($request->parent_id);
        $leftNode = Collection::find($request->left_node_id);
        $collection = DB::transaction(function () use ($collection, $parentNode, $leftNode, $request) {
            if (!empty($leftNode) && !empty($parentNode)) {
                if (!$leftNode->isChildOf($parentNode)) {
                    $leftNode = null;
                }
            }
            if (empty($leftNode) && !empty($parentNode)) {
                $leftNode = Collection::where('rgt', $parentNode->rgt - 1)->first();
                if (empty($leftNode)) {
                    Collection::where('lft', '>', $parentNode->rgt)->increment('lft', 2);
                    Collection::where('rgt', '>', $parentNode->rgt)->increment('rgt', 2);
                    $parentNode->rgt += 2;
                    $parentNode->save();
                }
            } elseif (empty($leftNode) && empty($parentNode)) {
                $leftNode = Collection::orderBy('rgt', 'desc')->first();
            }
            if (!empty($leftNode)) {
                Collection::where('rgt', '>', $leftNode->rgt)->increment('rgt', 2);
                Collection::where('lft', '>', $leftNode->rgt)->increment('lft', 2);
            }
            $collection = $this->processData($collection, $leftNode, $parentNode, $request);
            $collection->save();
            if ($request->has('icon')) {
                $collection = $this->addIcon($collection, $request);
            }
            return $collection;
        });
        return $collection;
    }

    private function processData(Collection $collection, Collection $leftNode = null, Collection $parentNode = null, $request)
    {
        $collection->name = $request->name;
        $collection->slug = str_slug($request->name);
        if (!empty($leftNode)) {
            $collection->lft = $leftNode->rgt + 1;
            $collection->rgt = $leftNode->rgt + 2;
        } elseif (!empty($parentNode)) {
            $collection->lft = $parentNode->lft + 1;
            $collection->rgt = $parentNode->lft + 2;
        } else {
            $collection->lft = 1;
            $collection->rgt = 2;
        }
        $collection->status = $request->status;
        $collection->is_visible = $request->visible;
        return $collection;
    }

    public function update($request)
    {
        $collection = Collection::find($request->id);
        if (empty($collection)) {
            throw new ServiceException('Invalid collection');
        }
        $collection->name = $request->name;
        $collection->status = $request->status;
        $collection->is_visible = $request->visible;
        $collection->save();
        if ($request->has('icon')) {
            $this->addIcon($collection, $request);
        }
    }

    public function delete($id)
    {
        $collection = Collection::find($id);
        if (empty($collection)) {
            throw new ServiceException('Invalid collection');
        }
        if ($collection->isLeafNode()) {
            return DB::transaction(function () use ($collection) {
                Collection::where('lft', '>', $collection->rgt)->decrement('lft', 2);
                Collection::where('rgt', '>', $collection->rgt)->decrement('rgt', 2);
                return $collection->delete();
            });
        } else {
            return DB::transaction(function () use ($collection) {
                Collection::where('lft', '>', $collection->rgt)->decrement('lft', 2);
                Collection::where('rgt', '>', $collection->rgt)->decrement('rgt', 2);
                Collection::where('lft', '>', $collection->lft)->where('rgt', '<', $collection->rgt)->decrement('lft', 1);
                Collection::where('lft', '>=', $collection->lft)->where('rgt', '<', $collection->rgt)->decrement('rgt', 1);
                return $collection->delete();
            });
        }
    }

    public function sort($request)
    {
        $collection = Collection::find($request->id);
        if (empty($collection)) {
            throw new ServiceException('Invalid collection');
        }
        $parentId = $request->parent_id;
        $leftNodeId = $request->left_node_id;
        DB::transaction(function () use ($collection, $parentId, $leftNodeId) {
            if (empty($parentId) && empty($leftNodeId)) {
                $startValue = 1;
            } elseif (!empty($parentId) && empty($leftNodeId)) {
                $parentCollection = Collection::find($parentId);
                $startValue = $parentCollection->lft + 1;
            } else {
                $leftCollection = Collection::find($leftNodeId);
                $startValue = $leftCollection->rgt + 1;
            }
            return $this->moveCollection($collection, $startValue);
        });
    }

    private function moveCollection($collection, $startValue)
    {
        $width = $collection->getWidth();
        $collectionWithChildren = Collection::getChildren($collection->id)->pluck('id')->toArray();
        if ($startValue < $collection->lft) {
            Collection::where('lft', '>=', $startValue)->where('lft', '<', $collection->rgt)->whereNotIn('id', $collectionWithChildren)->increment('lft', $width);
            Collection::where('rgt', '>=', $startValue)->where('rgt', '<', $collection->rgt)->whereNotIn('id', $collectionWithChildren)->increment('rgt', $width);
            $decrementValue = $collection->lft - $startValue;
            Collection::whereIn('id', $collectionWithChildren)->decrement('lft', $decrementValue);
            Collection::whereIn('id', $collectionWithChildren)->decrement('rgt', $decrementValue);
        } else {
            Collection::where('lft', '>', $collection->rgt)->where('lft', '<', $startValue)->whereNotIn('id', $collectionWithChildren)->decrement('lft', $width);
            Collection::where('rgt', '>', $collection->rgt)->where('rgt', '<', $startValue)->whereNotIn('id', $collectionWithChildren)->decrement('rgt', $width);
            $incrementValue = $startValue - $collection->lft - $width;
            Collection::whereIn('id', $collectionWithChildren)->increment('lft', $incrementValue);
            Collection::whereIn('id', $collectionWithChildren)->increment('rgt', $incrementValue);
        }
    }

    private function addIcon($collection, Request $request)
    {
        $icon = $request->file('icon');
        $targetDirectory = public_path('media/collection/' . $collection->id);
        $file = new File($icon);
        $file->setName(uniqid() . "." . $icon->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $collection->icon = $file->getName();
        $collection->save();
    }
}
