<?php

namespace Niyotail\Services;

use Niyotail\Models\Agent\AgentTarget;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Illuminate\Support\Facades\DB;

class AgentTargetService extends Service
{
    private function generateJson($request)
    {
        $data['revenue'] = $request['revenue'];
        $data['unique_stores'] = $request['unique_stores'];
        $data['points'] = $request['points'];

        return json_encode($data);
    }

    public function create(Request $request)
    {
        $agentTarget = new AgentTarget();
        $agentTarget->agent_id = $request->agent_id;
        $agentTarget->store_id = $request->store_id;
        $agentTarget->valid_from = $request->valid_from;
        $agentTarget->valid_to = $request->valid_to;
        $agentTarget->values = $this->generateJson($request);
        $agentTarget->save();
    }
}
