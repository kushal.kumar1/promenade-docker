<?php

namespace Niyotail\Services;

use Illuminate\Support\Str;
use Niyotail\Exceptions\ServiceException;

abstract class Service
{
    const TYPE_UPDATE = 'update';

    const TYPE_CREATE = 'create';

    const AUTHORITY_ADMIN = 'admin';

    const AUTHORITY_CUSTOMER = 'customer';

    const AUTHORITY_AGENT = 'agent';

    const AUTHORITY_SYSTEM = 'system';

    private $authority = self::AUTHORITY_CUSTOMER;

    /**
     * @param $authority
     *
     * @return bool
     *
     */
    protected function checkAuthority($authority)
    {
        if (($this->getAuthority() == self::AUTHORITY_SYSTEM)) {
            return true;
        }
        return ($this->getAuthority() == $authority);
    }

    /**
     * @return string
     */
    protected function getAuthority()
    {
        return $this->authority;
    }

    /**
     * @param $authority
     *
     * @return $this
     *
     */
    public function setAuthority($authority)
    {
        $this->authority = $authority;
        return $this;
    }

    protected function checkAuthorisation($authority)
    {
        if (($this->getAuthority() != self::AUTHORITY_SYSTEM)) {
            if ($this->getAuthority() != $authority) {
                throw new ServiceException('You are not authorised for this action');
            }
        }
    }

    /**
     * @param $model
     * @param $name
     * @param int $count
     *
     * @return string
     */
    protected function createSlug($model, $name, int $count = 0): string
    {
        $slug = Str::slug($name);
        if ($count > 0) {
            $slug = "$slug-$count";
        }
        $attribute = $model::where('slug', $slug)->first();
        if (!empty($attribute)) {
            return $this->createSlug($model, $name, ++$count);
        }
        return $slug;
    }
}
