<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Niyotail\Jobs\SendOtpToCustomer;
use Niyotail\Models\UserFeedback;
use Niyotail\Models\Order;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

/**
 * Class CustomerService
 *
 * @package Niyotail\Services
 *
 */
class UserFeedbackService extends Service
{

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool|\Niyotail\Models\User
     *
     */
    public function create(Request $request)
    {
        $userFeedback = $this->processData($request, new UserFeedback());
        $userFeedback->save();
        return $userFeedback;
    }

    private function processData(Request $request, UserFeedback $feedback)
    {
        $feedback->user_id =  $request->id;
        $feedback->type = $request->type;
        $feedback->remarks = $request->feedback;
        return $feedback;
    }
}
