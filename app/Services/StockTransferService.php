<?php

namespace Niyotail\Services;

use Barryvdh\Snappy\Facades\SnappyPdf;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Jobs\StockTransfer\CompleteStockTransferJob;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\StockTransfer;
use Niyotail\Models\StockTransferItem;
use Niyotail\Models\StockTransferNote;
use Niyotail\Models\WarehouseScope;

class StockTransferService extends Service
{
    public function create(Request $request)
    {
        $stockTransfer = new StockTransfer();
        $stockTransfer->from_warehouse_id = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        $stockTransfer->to_warehouse_id = $request->warehouse_id;
        $stockTransfer->status = StockTransfer::STATUS_INITIATED;
        $stockTransfer->save();
        return $stockTransfer;
    }

    public function update(Request $request)
    {
        $transfer = StockTransfer::findOrFail($request->id);
        $transfer->vehicle_number = $request->vehicle_number;
        $transfer->eway_bill_no = $request->eway_bill_no;
        $transfer->save();
        return $transfer;
    }

    public function addItem(StockTransfer $transfer, Request $request)
    {
        $item = StockTransferItem::where('stock_transfer_id', $transfer->id)
            ->where('product_id', $request->product_id)->first();
        if (empty($item)) {
            $item = new StockTransferItem();
            $item->product_id = $request->product_id;
            $item->stockTransfer()->associate($transfer);
            $item->requested_quantity = $request->quantity;
            $item->save();
            return $item;
        }
        $item->requested_quantity += $request->quantity;
        $item->save();
        return $item;
    }

    public function markCompleted(StockTransfer $transfer)
    {
        $transfer->loadMissing('items.flatInventories');
        if (!$transfer->canComplete()) {
            throw new ServiceException("Cannot complete the stock transfer!");
        }

        return DB::transaction(function () use ($transfer) {
           $transfer->status = StockTransfer::STATUS_TRANSFERRING;
           $transfer->save();
           CompleteStockTransferJob::dispatch($transfer)
               ->onConnection('sqs')->onQueue(config('queue.stockTransferQueue'));
        });

    }

    public function markConfirm(StockTransfer $transfer)
    {
        $transfer->loadMissing('items');
        if (!$transfer->canConfirm()) {
            throw new ServiceException("Cannot confirm the stock transfer!");
        }
        return DB::transaction(function () use ($transfer) {
            $transfer->status = StockTransfer::STATUS_CONFIRMED;
            foreach ($transfer->items as $item) {
                $this->markInventoriesForTransfer($item);
            }
            $transfer->save();
            return $transfer;
        });
    }

    public function markInventoriesForTransfer(StockTransferItem $item)
    {
        $item->loadMissing('stockTransfer');
        $flatInventories = FlatInventory::getInventoriesForUpdate($item->stockTransfer->from_warehouse_id, $item->product_id, FlatInventory::STATUS_READY_FOR_SALE, $item->requested_quantity);

        $flatInventoryService = new FlatInventoryService();
        $item->confirmed_quantity = $flatInventories->count();
        $inventoriesId = $flatInventories->pluck('id')->flatten()->toArray();
        $item->flatInventories()->sync($inventoriesId);
        $flatInventoryService->markInventoriesForTransfer($inventoriesId);
        $item->save();
    }

    public function addReceivedQuantity(StockTransferItem $item, Request $request)
    {
        if ($request->quantity > $item->sent_quantity) {
            throw new ServiceException('Received Quantity cannot be greater than sent quantity');
        }
        $item->received_quantity = $request->quantity;
        $item->save();
    }

    public function addSentQuantity(StockTransferItem $item, Request $request)
    {
        if ($item->stockTransfer->status == StockTransfer::STATUS_INITIATED || $item->stockTransfer->status == StockTransfer::STATUS_COMPLETED) {
            throw new ServiceException('Sent quantity can be added only on confirmed stock transfer');
        }
        if ($request->sent_quantity > $item->confirmed_quantity) {
            throw new ServiceException('Sent Quantity cannot be greater than confirmed quantity');
        }
        if (($request->sent_quantity < $item->confirmed_quantity) && empty($request->deficiency_reason)) {
            throw new ServiceException('Please add the deficiency reason');
        }
        $item->sent_quantity = $request->sent_quantity;
        $item->deficiency_reason = $request->sent_quantity < $item->confirmed_quantity ? $request->deficiency_reason : '';
        $item->save();
    }

    public function createStockTransferNote(StockTransfer $transfer)
    {
        $transferNote = new StockTransferNote();
        $transferNote->stockTransfer()->associate($transfer);
        $transferNote->number = 1;
        $transferNote->financial_year = $this->getFinancialYear($transferNote->stockTransfer);
        $lastInvoice = $this->getLastNote($transferNote->financial_year);
        if (!empty($lastInvoice)) {
            $transferNote->number = $lastInvoice->number + 1;
        }
        $transferNote->reference_id = "NT/" . $transferNote->financial_year . "/" . sprintf('%06d', $transferNote->number);
        $transferNote->pdf = $transferNote->reference_id . '.pdf';
        $total = 0;
        foreach ($transfer->items as $item) {
            $total += $item->confirmed_quantity * $item->product->unitVariant->mrp;
        }
        $transferNote->total = $total;
        $transferNote->save();
        $this->createPdf($transferNote);
    }

    private function getFinancialYear($stockTransferNote)
    {
        $creationDateTime = new DateTime($stockTransferNote->created_at);
        $year = $creationDateTime->format("y");
        $month = $creationDateTime->format("n");
        if ($month < 4) {
            return ($year - 1) . '-' . $year;
        }
        return $year . '-' . ($year + 1);
    }

    private function getLastNote($financialYear)
    {
        return StockTransferNote::withoutGlobalScope(WarehouseScope::class)->where('financial_year', $financialYear)
            ->orderBy('number', 'desc')->first();
    }

    //To be removed once all stock transfers before 13th have been resolved!.

    public function createPdf(StockTransferNote $note)
    {
        $note->loadMissing('stockTransfer.items.product.unitVariant', 'stockTransfer.fromWarehouse.city', 'stockTransfer.toWarehouse.city');
        $header = view()->make('pdf.stock_transfer_note.header', ['stockTransferNote' => $note]);
        $footer = view()->make('pdf.stock_transfer_note.footer', ['stockTransferNote' => $note]);

        $pdf = SnappyPdf::loadView('pdf.stock_transfer_note.body', ['stockTransferNote' => $note])
            ->setOption('margin-top', '90mm')
            ->setOption('margin-bottom', '65mm')
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            // ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100)
            ->output();

        $path = "transfer-notes/$note->financial_year/$note->number.pdf";
        Storage::put($path, $pdf);
    }
}
