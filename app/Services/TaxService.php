<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Tax;
use Illuminate\Http\Request;

class TaxService extends Service
{
    public function create(Request $request)
    {
        $tax = $this->processData(new Tax(), $request);
        $tax->save();
        return $tax;
    }

    public function update(Request $request)
    {
        $tax = Tax::find($request->id);
        if (empty($tax)) {
            throw new ServiceException('Tax not found!');
        }
        $tax = $this->processData($tax, $request);
        $tax->save();
        return $tax;
    }

    private function processData(Tax $tax, $request)
    {
        $tax->name = $request->name;
        $tax->tax_class_id = $request->tax_class_id;
        $tax->source_state_id = $request->source_state_id;
        $tax->supply_state_id = $request->supply_state_id;
        $tax->supply_country_id = $request->supply_country_id;
        $tax->percentage = $request->percentage;
        $tax->min_price = $request->min_price;
        $tax->max_price = $request->max_price;
        $tax->start_date = mysqlDateTime($request->start_date);
        $tax->end_date = !empty($request->end_date) ? mysqlDateTime($request->end_date) : null;
        return $tax;
    }
}
