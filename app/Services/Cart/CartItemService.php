<?php

namespace Niyotail\Services\Cart;

use Niyotail\Events\Cart\ItemAddedToCart;
use Niyotail\Events\Cart\ItemQuantityUpdatedInCart;
use Niyotail\Events\Cart\ItemRemovedFromCart;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\TaxHelper;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Pincode;
use Niyotail\Models\ProductVariant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Warehouse;

trait CartItemService
{
    protected function addItem(Request $request, Cart $cart)
    {
        $cart->loadMissing('store');
        $productVariant = ProductVariant::with('product')->withTrashed()->find($request->id);
        if (empty($productVariant)) {
            throw new ServiceException('Invalid product');
        }

        $cartItem = CartItem::where(['cart_id' => $cart->id, 'sku' => $productVariant->sku])->first();
        $quantity = $request->quantity;

        $price = $request->has('price') ? $request->price : $productVariant->getFinalPrice();

        if (empty($cartItem)) {
            $cartItem = new CartItem();
            $quantity = $this->moqQuantity($productVariant, $quantity);
            $cartItem->cart_id = $cart->id;
            $cartItem->sku = $productVariant->sku;
            $cartItem->quantity = $quantity;
            $cartItem->product_id = $productVariant->product_id;
            $cartItem->variant = $productVariant->value;
            $cartItem->mrp = $productVariant->mrp;
            $cartItem->price = $price;
            $cartItem->discount = 0;
            $cartItem->save();
            event(new ItemAddedToCart($cart));
            return $cartItem;
        }

        //call update function in case of existing sku
        $quantity += $cartItem->quantity;
        $updateRequest = new Request();
        $updateRequest->merge(['id' => $cartItem->id]);
        $updateRequest->merge(['quantity' => $quantity]);
        $this->updateItemQuantity($updateRequest, $cartItem);
    }

    protected function updateItemQuantity(Request $request, CartItem $cartItem)
    {
        $productVariant = $cartItem->productVariant;
        $quantity = $request->quantity;
        $quantity = $this->moqQuantity($productVariant, $quantity);
        $cartItem->quantity = $quantity;
        $cartItem->mrp = $cartItem->productVariant->mrp;
        if($cartItem->cart->source != Cart::SOURCE_WHOLESALE_CART_IMPORTER){
            $cartItem->price = $cartItem->productVariant->getFinalPrice();
        }
        $cartItem->discount = 0;
        $cartItem->save();
        event(new ItemQuantityUpdatedInCart($cartItem));
    }

    public function removeItem(Request $request)
    {
        $cart = DB::transaction(function () use ($request) {
            $cartItem = CartItem::with('cart')->find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }
            $cart = $cartItem->cart;
            $cartItem->delete();
            event(new ItemRemovedFromCart($cart));
            return $cart->refresh();
        });
        return $cart;
    }

    public function calculateItemsPrice(Cart $cart)
    {
        $cart->load('items');
        foreach ($cart->items as $item) {
            $item->price = $item->productVariant->getFinalPrice();
            $item->mrp = $item->productVariant->mrp;
        }
        $cart->items()->saveMany($cart->items);
    }

    public function calculateItemsTaxes(Cart $cart)
    {
        $cart->load('items.product', 'store');
        $pincode = Pincode::with('city.state')->where('pincode', $cart->store->pincode)->first();
        foreach ($cart->items as $item) {
            $taxAmount = 0;
            //TODO:: make it dynamic basis warehouse
            $sourceStateId = $cart->warehouse->city->state_id;
            $supplyState =  $pincode->city->state;
            $taxes = TaxHelper::getTaxes($item->product->tax_class_id, $item->mrp, $sourceStateId, $supplyState->id, $supplyState->country_id);
            if (!empty($taxes)) {
                $totalTaxRate = $taxes->sum('percentage');
                $additionalTax = $taxes->sum('additional_charges_per_unit') * $item->quantity * $item->productVariant->quantity;
                $taxAmount += inclusiveTaxAmount(($item->quantity * $item->price) - $item->discount, $totalTaxRate, $additionalTax);
            }
            $item->tax = $taxAmount;
        }
        $cart->items()->saveMany($cart->items);
    }

    public function calculateItemsTotal(Cart $cart)
    {
        $cart->load('items');
        foreach ($cart->items as $item) {
            $item->subtotal = $this->getSubtotalForItem($item->quantity, $item->price, $item->tax);
            $item->total = $item->subtotal - $item->discount + $item->tax;
        }
        $cart->items()->saveMany($cart->items);
    }

    private function getSubtotalForItem($quantity, $price, $tax)
    {
        $subtotal = ($quantity * $price);
        $subtotal -= $tax;
        return $subtotal;
    }

    private function moqQuantity($productVariant, $quantity)
    {
        if ($quantity < $productVariant->moq) {
            $quantity = $productVariant->moq;
        }
        return $quantity;
    }
}
