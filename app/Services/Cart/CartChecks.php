<?php

namespace Niyotail\Services\Cart;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\CartItem;
use Niyotail\Models\ProductVariant;

trait CartChecks
{
    private $checks=[
        'default'=>[
            'trashed'=>true,
            'product_status'=>true,
            'beat_salablity'=>true,
            'variant_moq_stock_check'=>true,
            'stock'=>true,
            'add_moq'=>true,
        ],
        CARTITEM::SOURCE_IMPORTER=>[
            'trashed'=>false,
            'product_status'=>true,
            'beat_salablity'=>true,
            'variant_moq_stock_check'=>false,
            'stock'=>true,
            'add_moq'=>false,
        ],
        CARTITEM::SOURCE_REPLENISH_BOT=>[
            'trashed'=>false,
            'product_status'=>true,
            'beat_salablity'=>false,
            'variant_moq_stock_check'=>false,
            'stock'=>false,
            'add_moq'=>false,
        ],

    ];

    private function check($condition,$source)
    {
        if(!empty($this->checks[$source])){
            return $this->checks[$source][$condition];
        }
        return $this->checks['default'][$condition];
    }


    private function validateProduct($variant, $store, $source)
    {
        $variant->loadMissing('product.totalInventory');
        $product = $variant->product;
        $beatId = $store->beat_id;
        $warehouseIds = $store->beat->warehouses->pluck('id')->toArray();
        $skipMoqStockCheck = $store->is1KStore() ? true : false;


        //Status
        if($this->check('product_status',$source)) {
            if ($product->status != 1) {
                throw new ServiceException("Product $product->id is not available!");
            }
        }

        //Beat Salability
        if($this->check('beat_salablity',$source)) {
            if (!$product->isSalable($beatId)) {
                throw new ServiceException("Product is not salable in your area");
            }
        }

        // Stock
        if($this->check('variant_moq_stock_check',$source)) {
            if ($skipMoqStockCheck) {
              return true;
            }
            if (!$variant->isSalable($product, false, $warehouseIds)) {
                throw new ServiceException("Product is currently out of stock");
            }
        }

        if($this->check('stock',$source)) {
            if ($skipMoqStockCheck) {
              return true;
            }
            if (!$variant->isSalable($product, true, $warehouseIds)) {
                throw new ServiceException("Product is currently out of stock");
            }
        }
    }

    private function moqQuantity($productVariant,$quantity,$source)
    {
        if($this->check('add_moq',$source)) {
            if ($quantity < $productVariant->moq) {
                $quantity = $productVariant->moq;
            }
        }
        return $quantity;
    }

}
