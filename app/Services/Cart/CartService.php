<?php

namespace Niyotail\Services\Cart;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\AdditionalOrderInfo;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Store;
use Niyotail\Models\Warehouse;
use Niyotail\Services\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartService extends Service
{
    use CartItemService, CartAddressService;
    private $cart = null;
    const DEFAULT_WAREHOUSE = 2;

    public function setCart(Cart $cart = null): CartService
    {
        $this->cart = $cart;
        return $this;
    }

    public function addToCart($request, $createdBy): Cart
    {
        $cart = DB::transaction(function () use ($request, $createdBy) {
            $source = $request->filled('source') ? $request->source: Cart::SOURCE_SYSTEM;
            $request->merge(['source' => $source]);
            if (empty($this->cart)) {
                $warehouseId = $request->warehouse_id ?? self::DEFAULT_WAREHOUSE;
                $additionalInfo = $request->additional_info;
                $store = Store::with('pincodeRel.city')->find($request->store_id);
                $type = ($store->type == Store::TYPE_WAREHOUSE) ? Cart::TYPE_STOCK_TRANSFER : Cart::TYPE_RETAIL;
                if($type == Cart::TYPE_STOCK_TRANSFER){
                    $additionalInfo = ['st_type' => $this->getStockTransferType($store, $warehouseId)];
                }
                $this->createCart($createdBy, $request->store_id, $source, $request->tag, $additionalInfo, $warehouseId, $type);
            }

            $this->addItem($request, $this->cart);
            return $this->cart->refresh();
        });
        return $cart;
    }

    public function updateQuantity(Request $request, $withPrice = false): Cart
    {
        $cart = DB::transaction(function () use ($request, $withPrice) {
            $cartItem = CartItem::with('productVariant.product.totalInventory', 'cart.store')->find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }

            $this->updateItemQuantity($request, $cartItem);
            return Cart::with('items')->whereHas('items', function ($query) use ($request) {
                $query->where('id', $request->id);
            })->first();
        });
        return $cart;
    }

    public function deleteCart($cart)
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $cart->delete();
    }

    public function calculateShipping($cart)
    {
        $charges = 0;
        $cart->shipping_charges = $charges;
        $cart->save();
    }

    public function calculateCartTotal($cart)
    {
        $subTotal = $discount = $tax = 0;
        $items = CartItem::where('cart_id', $cart->id)->get();
        foreach ($items as $item) {
            $subTotal += $item->subtotal;
            $discount += $item->discount;
            $tax += $item->tax;
        }
        $cart->subtotal = $subTotal;
        $cart->discount = $discount;
        $cart->tax = $tax;
        $cart->total = $subTotal - $discount + $tax + $cart->shipping_charges;
        $cart->save();
    }

    private function createCart($createdBy, $storeId, $source, $tag, $additionalInfo, $warehouseId, $type)
    {
        $store = Store::find($storeId);
        $cart = new Cart();
        $cart->store_id = $store->id;
        $cart->warehouse_id = $warehouseId;
        $cart->currency = 'INR';
        $cart->source = $source;
        $cart->tag = $tag;
        $cart->type = $type;
        if(!empty($additionalInfo) && is_array($additionalInfo)) {
            $cart->additional_info = !empty($additionalInfo) ? json_encode($additionalInfo) : null;
        } else {
            $cart->additional_info = !empty($additionalInfo) ? $additionalInfo : null;
        }
        $cart->createdBy()->associate($createdBy);
        $cart->save();

        $this->setCart($cart);
    }

    //TODO:: identify why this function is being used
    public static function processProductsForCartItem($products, $cart)
    {
        $products->map(function ($product) use ($cart) {
            return $product->variants->map(function ($variant) use ($cart, $product) {
                if(empty($cart)) {
                    $variant['cartItem'] = null;
                    return $variant;
                }

                $sku = $product->variants->where('id', $variant->id)->first()->sku;
                $cartItem = $cart->items()->where(['cart_id' => $cart->id, 'sku' => $sku])->first();

                if(empty($cartItem)) {
                    $variant['cartItem'] = null;
                    return $variant;
                }

                $variant['cartItem'] = $cartItem;
                return $variant;
            });
        });

        return $products;
    }

    private function getStockTransferType(Store $store, int $sourceWarehouseId): string
    {
        $source = Warehouse::with('city')->find($sourceWarehouseId);
        $destinationState = $store->pincodeRel->city->state_id;
        if($source->city->state_id != $destinationState){
            return AdditionalOrderInfo::TYPE_INTERSTATE;
        }
        return AdditionalOrderInfo::TYPE_INTRASTATE;
    }
}
