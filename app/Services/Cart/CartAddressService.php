<?php

namespace Niyotail\Services\Cart;

use Niyotail\Events\Cart\AddressUpdatedInCart;
use Niyotail\Models\Cart;
use Niyotail\Models\CartAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Pincode;

trait CartAddressService
{
    public function createOrUpdateAddress(Cart $cart): Cart
    {
        $cart->load('shippingAddress', 'billingAddress');
        $cart = DB::transaction(function () use ($cart) {
            $shippingAddress = empty($cart->shippingAddress) ? (new CartAddress()) : $cart->shippingAddress;
            $shippingAddress = $this->processAddressData($shippingAddress, $cart->store);
            $billingAddress = $shippingAddress;
            $shippingAddress->save();
            $billingAddress->save();
            $cart->shippingAddress()->associate($shippingAddress);
            $cart->billingAddress()->associate($billingAddress);
            $cart->save();
            // event(new AddressUpdatedInCart($cart));
            return $cart;
        });
        return $cart;
    }

    private function processAddressData(CartAddress $cartAddress, $store): CartAddress
    {
        $pincode = Pincode::where('pincode', $store->pincode)->first();
        $cartAddress->name = $store->name;
        $cartAddress->mobile = $store->contact_mobile;
        $cartAddress->address = $store->address;
        $cartAddress->landmark = $store->landmark;
        $cartAddress->pincode = $store->pincode;
        $cartAddress->lat = $store->lat;
        $cartAddress->lng = $store->lng;
        $cartAddress->city_id = $pincode->city_id;
        return $cartAddress;
    }
}
