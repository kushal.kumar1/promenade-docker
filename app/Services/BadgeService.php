<?php

namespace Niyotail\Services;

use Niyotail\Models\Badge;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
/**
 * Class BadgeService
 *
 * @package Niyotail\Services
 *
 */
class BadgeService extends Service
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Niyotail\Models\Badge
     *
     */
    public function create(Request $request)
    {
        $this->isDuplicate($request);
        $badge = new Badge();
        $badge = $this->processData($badge, $request);
        $badge->save();
        return $badge;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool|\Niyotail\Models\Badge
     *
     */
    public function update(Request $request)
    {
        $this->isDuplicate($request);
        $badge = Badge::find($request->id);
        if (empty($badge)) {
          throw new ServiceException("Badge doesn't exist !");
        }

        $badge = $this->processData($badge, $request);
        $badge->save();
        return $badge;
    }


    /**
     * @param \Niyotail\Models\Badge $badge
     * @param                           $request
     *
     * @return \Niyotail\Models\Badge
     *
     */
    private function processData(Badge $badge, $request)
    {
        $badge->name = $request->name;
        $badge->description = $request->description;
        return $badge;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     *
     */
    public function delete(Request $request)
    {
        $badge = Badge::find($request->id);
        if (empty($badge)) {
            throw new ServiceException("Badge doesn't exist !");
        }
        if ($this->isDuplicate($request, $request->id))
            return false;
        $badge->delete();
        return true;
    }

    private function isDuplicate($request, $id = null)
    {

        $badgeQuery = Badge::where('name', $request->name);
        if (!empty($id))
            $badgeQuery->where('id', '!=', $id);
        $badge=$badgeQuery->first();
        if (!empty($badge)) {
              throw new ServiceException("Badge with same name already exist !");
        }
        return false;
    }
}
