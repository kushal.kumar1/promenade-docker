<?php

namespace Niyotail\Services;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Employee;
use Niyotail\Models\Manifest;
use Niyotail\Models\Order;
use Niyotail\Models\Shipment;
use Niyotail\Services\Order\ShipmentService;

class ManifestService extends Service
{
    public function create(Request $request, Employee $employee)
    {
        return DB::transaction(function () use ($request, $employee) {
            $shipments = Shipment::with('order')->whereIn('id', $request->selected)->get();

            if ($shipments->unique('order.type')->count() != 1 && $shipments->where('order.type', Order::TYPE_STOCK_TRANSFER)->count() > 0) {
                throw new ServiceException('Stock transfer type shipments and other shipments cannot be created in single manifest!');
            }

            $manifest = new Manifest();
            $manifest->status = Manifest::STATUS_GENERATED;
            $manifest->createdBy()->associate($employee);
            $manifest->warehouse_id = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
            $manifest->save();
            $manifest->shipments()->sync($request->selected);
            $manifest->load('shipments.orderItems');
            $shipmentService = new ShipmentService();
            foreach ($manifest->shipments as $shipment) {
                $shipmentService->updateShipment($shipment, Shipment::STATUS_READY_TO_SHIP);
            }
            return $manifest;
        });
    }

    public function removeShipment(Request $request)
    {
        $manifest = Manifest::find($request->id);
        if ($manifest->status != Manifest::STATUS_GENERATED) throw new ServiceException("Cannot remove shipment!");
        return DB::transaction(function () use ($request, $manifest) {
            $manifest->shipments()->detach($request->shipment_id);
            $shipment = Shipment::find($request->shipment_id);
            $shipmentService = new ShipmentService();
            $shipmentService->updateShipment($shipment, Shipment::STATUS_GENERATED);
        });
    }

    public function cancel(Request $request)
    {
        $manifest = Manifest::with('shipments.orderItems')->find($request->id);
        if (!$manifest->canCancel()) {
            throw new ServiceException("$manifest->status Manifest can't be cancelled.");
        }
        return DB::transaction(function () use ($manifest) {
            $manifest->status = Manifest::STATUS_CANCELLED;
            $manifest->save();
            $shipmentService = new ShipmentService();
            foreach ($manifest->shipments as $shipment) {
                $shipmentService->updateShipment($shipment, Shipment::STATUS_GENERATED);
            }
            return $manifest;
        });
    }

    public function close(Request $request)
    {
        $manifest = Manifest::with('shipments')->find($request->id);
        if (!$manifest->canClose()) {
            throw new ServiceException("$manifest->status Manifest can't be closed.");
        }
        return DB::transaction(function () use ($manifest) {
            $manifest->status = Manifest::STATUS_CLOSED;
            $manifest->save();
            return $manifest;
        });
    }

    public function dispatch(Request $request)
    {
        $manifest = Manifest::with('shipments.orderItems', 'shipments.order')->find($request->id);
        if ($manifest->shipments->where('status', '!=', Shipment::STATUS_CANCELLED)->where('boxes', null)->count() != 0) {
            throw new ServiceException("Please update boxes for each shipment");
        }

        if (!$manifest->canDispatch()) {
            throw new ServiceException("$manifest->status Manifest can't be Dispatched.");
        }
        return DB::transaction(function () use ($manifest) {
            $manifest->status = Manifest::STATUS_DISPATCHED;
            $manifest->save();
            $shipmentService = new ShipmentService();
            //Some shipments can be cancelled post manifest creation, filtering out the non cancelled shipments
            $shipments = $manifest->shipments->where('status', '!=', Shipment::STATUS_CANCELLED);
            foreach ($shipments as $shipment) {
                $shipmentService->updateShipment($shipment, Shipment::STATUS_DISPATCHED);
            }
            $this->generateManifestPdf($manifest);
            return $manifest;
        });
    }

    public function generateManifestPdf(Manifest $manifest)
    {
        $manifest->loadMissing('shipments.order', 'shipments.invoice', 'agent');
        $pdf = SnappyPdf::loadView('pdf.shipping_manifest', ['manifest' => $manifest])
            ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100)->output();

        $path = $manifest->id . ".pdf";
        Storage::disk('manifest')->put($path, $pdf);
    }

    public function markShipmentsDelivered(Request $request)
    {
        $manifest = Manifest::with(['shipments' => function ($query) use ($request) {
            $query->whereIn('shipments.id', array_keys($request->otps));
        }])->find($request->id);

        return DB::transaction(function () use ($manifest, $request) {
            $shipmentService = new ShipmentService();
            foreach ($manifest->shipments as $shipment) {
                //deliver only shipments which are dispatched
                if(!empty($shipment->otp)){
                    $shipmentService->verifyOtp($shipment, $request->otps[$shipment->id]);
                }
                if ($shipment->status == Shipment::STATUS_DISPATCHED) {
                    $shipmentService->updateShipment($shipment, Shipment::STATUS_DELIVERED);
                }
            }
        });
    }

    public function updateDetails(Request $request)
    {
        $manifest = Manifest::find($request->id);
        if ($manifest->status != Manifest::STATUS_GENERATED) throw new ServiceException("Manifest cannot be updated!");
        $manifest->vehicle_number = $request->vehicle_number;
        $manifest->agent_id = $request->agent_id;
        if ($request->has('locations')) {
            $manifest->locations()->sync($request->locations);
        }
        $manifest->save();
        return $manifest;
    }

    public function getPdf(Manifest $manifest)
    {
        $manifest->loadMissing('shipments.order', 'shipments.invoice', 'agent');
        return SnappyPdf::loadView('pdf.shipping_manifest', ['manifest' => $manifest])
            ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }
}
