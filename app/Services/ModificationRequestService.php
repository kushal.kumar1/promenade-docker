<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\File;
use Niyotail\Models\ModificationRequest;
use Niyotail\Models\PurchaseInvoiceItemModificationRequest;
use Niyotail\Models\PurchaseInvoiceModificationRequest;
use Niyotail\Models\TransactionDeletionRequest;

class ModificationRequestService extends Service
{

    // create a modification request
    // params:  1. type of modification_request
    //          2. params of that modification request

    // return: handle to created request object


    /**
     * @throws ServiceException
     * @throws Exception
     * @throws \Throwable
     */
    public
    function createModificationRequest(Request $request): ModificationRequest
    {
        $modificationRequest = null;

        switch ($modificationRequestType = $request->modificationRequestType) {
            case ModificationRequest::TYPE_TRANSACTION:
                $modificationRequest = $this->buildTransactionDeletionRequest($request);
                break;
            case ModificationRequest::TYPE_PURCHASE_INVOICE:
                $modificationRequest = $this->buildPurchaseInvoiceModificationRequest($request);
                break;
            case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                $modificationRequest = $this->buildPurchaseInvoiceItemModificationRequest($request);
                break;
            default:
                throw new ServiceException("Invalid value passed for createModificationRequest::modificationRequestType. value:$modificationRequestType");
        }

        //validate modification request
        $modificationRequest->validateModificationRequest();

        //save request to db
        $modificationRequest->save();

        static::sendCreationEmailUpdate($modificationRequest);

        return $modificationRequest;
    }

    private function buildTransactionDeletionRequest($request): TransactionDeletionRequest
    {
        $transactionDeletionRequest = new TransactionDeletionRequest();

        $transactionDeletionRequest->transaction_id = $request->transaction_id;
        $transactionDeletionRequest->delete_reason = $request->delete_reason;
        $transactionDeletionRequest->employee_id = $request->employee_id;
        $transactionDeletionRequest->status = ModificationRequest::STATUS_PENDING;

        return $transactionDeletionRequest;
    }

    /**
     * @throws Exception
     * @throws \Throwable
     */
    private function buildPurchaseInvoiceModificationRequest(Request $request): PurchaseInvoiceModificationRequest
    {
        $purchaseInvoiceModificationRequest = new PurchaseInvoiceModificationRequest();
        $purchaseInvoiceModificationRequest->purchase_invoice_id = $request->purchase_invoice_id;
        $purchaseInvoiceModificationRequest->employee_id = $request->employee_id;
        $purchaseInvoiceModificationRequest->action = $request->action_type;
        $purchaseInvoiceModificationRequest->status = ModificationRequest::STATUS_PENDING;

        if ($request->action_type == ModificationRequest::ACTION_EDIT) {
            $purchaseInvoiceModificationRequest->vendor_id = $request->vendor_id;
            $purchaseInvoiceModificationRequest->vendor_address_id = $request->vendor_address_id;
            $purchaseInvoiceModificationRequest->date = $request->date;
            if(!empty($request->image)) {
                $purchaseInvoiceModificationRequest->setPurchaseInvoiceImage($request->image);
            }
            $purchaseInvoiceModificationRequest->platform_id = $request->platform_id;
            if(!empty($request->eway_number)) {
                $purchaseInvoiceModificationRequest->eway_number = $request->eway_number;
            }
            $purchaseInvoiceModificationRequest->forecast_payment_date = $request->forecast_payment_date;
        }
        if ($request->action_type == ModificationRequest::ACTION_DELETE) {
            $purchaseInvoiceModificationRequest->delete_reason = $request->delete_reason;
        }

        return $purchaseInvoiceModificationRequest;
    }

    /**
     * @throws Exception
     */
    private function buildPurchaseInvoiceItemModificationRequest($request): PurchaseInvoiceItemModificationRequest
    {
        $purchaseInvoiceItemModificationRequest = new PurchaseInvoiceItemModificationRequest();
        $purchaseInvoiceItemModificationRequest->purchase_invoice_item_id = $request->purchase_invoice_item_id;
        $purchaseInvoiceItemModificationRequest->employee_id = $request->employee_id;
        $purchaseInvoiceItemModificationRequest->action = $request->action_type;
        $purchaseInvoiceItemModificationRequest->status = ModificationRequest::STATUS_PENDING;

        if ($request->action_type == ModificationRequest::ACTION_EDIT) {
            $purchaseInvoiceItemModificationRequest->product_id = $request->product_id;
            $purchaseInvoiceItemModificationRequest->quantity = $request->quantity;
            $purchaseInvoiceItemModificationRequest->base_cost = $request->base_cost;
            $purchaseInvoiceItemModificationRequest->scheme_discount = $request->scheme_discount;
            $purchaseInvoiceItemModificationRequest->other_discount = $request->other_discount;
            $purchaseInvoiceItemModificationRequest->post_tax_discount = $request->post_tax_discount;
        }
        if ($request->action_type == ModificationRequest::ACTION_DELETE) {
            $purchaseInvoiceItemModificationRequest->delete_reason = $request->delete_reason;
        }

        return $purchaseInvoiceItemModificationRequest;
    }

    /**
     * @throws ServiceException
     */
    public function getModificationRequest(Request $request)
    {
        $modificationRequestId = property_exists($request, 'modificationRequestId') && isset($request->modificationRequestId) ? $request->modificationRequestId : null;
        $modificationRequestType = property_exists($request, 'modificationRequestType') && isset($request->modificationRequestType) ? $request->modificationRequestType : null;
        $modificationRequestStatus = property_exists($request, 'modificationRequestType') && isset($request->modificationRequestStatus) ? $request->modificationRequestStatus : null;


        if (!is_null($modificationRequestId)) {
            if (!ModificationRequest::isValidType($modificationRequestType)) {
                throw new ServiceException("{$modificationRequestType} is not a valid value for ModificationRequest::modificationRequestType");
            }
            switch ($modificationRequestType) {
                case ModificationRequest::TYPE_TRANSACTION:
                    return TransactionDeletionRequest::findOrFail($modificationRequestId);
                case ModificationRequest::TYPE_PURCHASE_INVOICE:
                    return PurchaseInvoiceModificationRequest::findOrFail($modificationRequestId);
                case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                    return PurchaseInvoiceItemModificationRequest::findOrFail($modificationRequestId);
            }
        }

        switch ((is_null($modificationRequestType) ? 1 : 0 * 2) + (is_null($modificationRequestStatus) ? 1 : 0)) {
            case 0:
                return array_merge(
                    TransactionDeletionRequest::all(),
                    PurchaseInvoiceModificationRequest::all(),
                    PurchaseInvoiceItemModificationRequest::all(),
                );
            case 1:
                if (!ModificationRequest::isValidStatus($modificationRequestStatus)) {
                    throw new ServiceException("{$modificationRequestStatus} is not a valid status value for ModificationRequest::status");
                }
                return array_merge(
                    TransactionDeletionRequest::query()->withStatus($modificationRequestStatus)->get(),
                    PurchaseInvoiceModificationRequest::query()->withStatus($modificationRequestStatus)->get(),
                    PurchaseInvoiceItemModificationRequest::query()->withStatus($modificationRequestStatus)->get()
                );
            case 2:
                if (!ModificationRequest::isValidType($modificationRequestType)) {
                    throw new ServiceException("{$modificationRequestType} is not a valid value for ModificationRequest::modificationRequestType");
                }
                switch ($modificationRequestType) {
                    case ModificationRequest::TYPE_TRANSACTION:
                        return TransactionDeletionRequest::all();
                    case ModificationRequest::TYPE_PURCHASE_INVOICE:
                        return PurchaseInvoiceModificationRequest::all();
                    case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                        return PurchaseInvoiceItemModificationRequest::all();
                }
            case 3:
                if (!ModificationRequest::isValidType($modificationRequestType)) {
                    throw new ServiceException("{$modificationRequestType} is not a valid value for ModificationRequest::modificationRequestType");
                }
                if (!ModificationRequest::isValidStatus($modificationRequestStatus)) {
                    throw new ServiceException("{$modificationRequestStatus} is not a valid status value for ModificationRequest::status");
                }
                switch ($modificationRequestType) {
                    case ModificationRequest::TYPE_TRANSACTION:
                        return TransactionDeletionRequest::query()->withStatus($modificationRequestStatus)->get();
                    case ModificationRequest::TYPE_PURCHASE_INVOICE:
                        return PurchaseInvoiceModificationRequest::query()->withStatus($modificationRequestStatus)->get();
                    case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                        return PurchaseInvoiceItemModificationRequest::query()->withStatus($modificationRequestStatus)->get();
                }
        }

        return [];
    }

    /**
     * @throws ServiceException
     * @throws Exception
     */
    public function deleteModificationRequest(Request $request): bool
    {
        switch ($request->modificationRequestType) {
            case ModificationRequest::TYPE_TRANSACTION:
                return TransactionDeletionRequest::query()
                    ->findOrFail($request->modificationRequestId)
                    ->delete();
            case ModificationRequest::TYPE_PURCHASE_INVOICE:
                return PurchaseInvoiceModificationRequest::query()
                    ->findOrFail($request->modificationRequestId)
                    ->delete();
            case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
                return PurchaseInvoiceItemModificationRequest::query()
                    ->findOrFail($request->modificationRequestId)
                    ->delete();
            default:
                throw new ServiceException("Invalid value passed for createModificationRequest::modificationRequestId. value:{$request->modificationRequestId}");
        }

    }

    public static function sendCreationEmailUpdate($modificationRequest)
    {
        $subject = "Modification Request Update - ".$modificationRequest->modificationRequestType;
        Mail::send(
            'emails.modificationRequestCreate',
            ['modificationRequest' => $modificationRequest],
            function ($message) use ($subject) {
                $message->to('modification.requests@niyotail.com', 'Modification Requests')
                    ->cc('saksham.khanna@niyotail.com', 'Saksham Khanna')
                    ->from('support@1knetworks.com', '1K Kirana Bazaar')
                    ->subject($subject);
            });
    }

    public static function sendProcessedEmailUpdate(Collection $modificationRequests)
    {
        if (count($modificationRequests) > 0) {
            Mail::send(
                'emails.modificationRequestProcess', [
                'modificationRequests' => $modificationRequests,
            ], function ($message) {
                $message->to('modification.requests@niyotail.com', 'Modification Requests')
                    ->cc('saksham.khanna@niyotail.com', 'Saksham Khanna')
                    ->from('support@1knetworks.com', '1K Kirana Bazaar')
                    ->subject('Modification requests have been processed');
            });
        }
    }
}


