<?php

namespace Niyotail\Services;

use Niyotail\Models\Beat;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Illuminate\Support\Facades\DB;

class BeatService extends Service
{
    public function create(Request $request)
    {
        $beat = DB::transaction(function () use ($request) {
            $beat = $this->processData($request, new Beat());
            $beat->save();
            if ($request->has('brands')) {
                $beat->exceptions()->sync($request->brands);
            }
            return $beat;
        });
        return $beat;
    }

    public function update(Request $request)
    {
        $beat = Beat::find($request->id);
        if (empty($beat)) {
            throw new ServiceException("Beat doesn't exist !");
        }
        $beat = DB::transaction(function () use ($request, $beat) {
            $beat = $this->processData($request, $beat);
            $beat->save();
            $beat->exceptions()->sync($request->brands);
            //TODO: If moved to redis, remove only beat_exceptions tagged cache.
            //Make appropriate changes in Product Queries as well.
            cache()->flush();
            return $beat;
        });
        return $beat;
    }

    private function processData(Request $request, Beat $beat)
    {
        $beat->name = $request->name;
        $beat->long_name = $request->long_name;
        $beat->status = $request->status;
        return $beat;
    }
}
