<?php


namespace Niyotail\Services;


use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\PurchaseInvoiceCartItems;

class PurchaseInvoiceCartItemService
{
    public function addItem($request, PurchaseInvoiceCart $cart)
    {
        $cartItem = new PurchaseInvoiceCartItems();

        $existingCartItem = $this->checkDuplicate($request, $cart);
        if (!empty($existingCartItem)) {
            $cartItem = $existingCartItem;
        }

        $expiryDate = Carbon::parse($request->expiry_date);
        $today = Carbon::today();

        if(!$expiryDate->gt($today)) {
            throw new \Exception('Expiry date should be greater than today');
        }

        $cartId = $cart->id;
        $productId = $request->product_id;
        $rackId = $request->product_rack;
        $sku = $request->sku;
        $quantity = $request->quantity;
        $expiryDate = empty($request->expiry_date) ? null : Carbon::parse($request->expiry_date)->toDateString();
        $manufacturingDate = empty($request->manufacturing_date) ? null : Carbon::parse($request->manufacturing_date)->toDateString();
        $vendorBatchNo = $request->vendor_batch_number;
        $valueType = $request->value_type;
        $priceType = $request->price_type;
        $discountType = $request->discount_type;
        $cost = $request->cost;
        $discount = $request->discount;
        $cashDiscount = $request->cash_discount;
        $receivedQuantity = $request->input('received_quantity', 0);
        $postTaxDiscount = $request->post_tax_discount;
        $postTaxDiscountType = $request->post_tax_discount_type;
        
        $cartItem->purchase_invoice_cart_id = $cartId;
        $cartItem->sku = $sku;
        $cartItem->product_id = $productId;
        $cartItem->rack_id = $rackId;
        $cartItem->expiry_date = $expiryDate;
        $cartItem->quantity = $quantity;
        $cartItem->manufacturing_date = $manufacturingDate;
        $cartItem->vendor_batch_number = $vendorBatchNo;
        $cartItem->value_type = $valueType;
        $cartItem->price_type = $priceType;
        $cartItem->discount_type = $discountType;
        $cartItem->cost = $cost;
        $cartItem->cash_discount = $cashDiscount;
        $cartItem->discount = $discount;
        $cartItem->post_tax_discount = $postTaxDiscount;
        $cartItem->post_tax_discount_type = $postTaxDiscountType;
        $cartItem->received_quantity = $receivedQuantity;
        $cartItem->save();

        $this->recalculatePurchaseInvoice($cart);

        return $cartItem;
    }

    private function getTaxes($taxField)
    {
        $taxes = [];
        if (!empty($taxField)) {
            $taxDetails = explode(',', $taxField);
            foreach ($taxDetails as $tax) {
                $tax = explode('_', $tax);
                if (count($tax) == 3) {
                    $taxes[] = [
                        'name' => $tax[0],
                        'percentage' => $tax[1],
                        'amount' => $tax[2],
                    ];
                }
            }
        }
        return collect($taxes);
    }

    private function checkDuplicate($request, $cart)
    {
        return PurchaseInvoiceCartItems::cart($cart->id)
            ->sku($request->sku)
            ->product($request->product_id)
            ->vendorBatchNo($request->vendor_batch_number)
            ->first();
    }

    public function recalculatePurchaseInvoice(PurchaseInvoiceCart $cart)
    {
        $purchaseInvoiceService = new PurchaseInvoiceService();

        $items = $cart->items;
        $total = 0;
        $taxTotal = 0;

        $request = new Request();

        $reqParams = [
            'vendor_address_id' => $cart->vendor_address_id,
            'warehouse_id' => $cart->warehouse_id
        ];

        foreach ($items as $item) {

            $reqParams['sku'] = $item->sku;
            $reqParams['unit_type'] = $item->value_type;
            $reqParams['price_type'] = $item->price_type;
            $reqParams['discount_type'] = $item->discount_type;
            $reqParams['cost'] = $item->cost;
            $reqParams['discount'] = $item->discount;
            $reqParams['quantity'] = $item->quantity;
            $reqParams['post_tax_discount'] = $item->post_tax_discount;
            $reqParams['post_tax_discount_type'] = $item->post_tax_discount_type;

            $request->replace($reqParams);
            $values = $purchaseInvoiceService->getInvoiceItemPreview($request);

            $taxTotal += $values['total_tax'];
            $total += $values['total_effective'];
        }

        $cart->tax = $taxTotal;
        $cart->total = round($total);
        $cart->round = $total - $cart->total;
        $cart->save();
    }

    public function updateItemQuantity($itemId, $quantity, PurchaseInvoiceCart $cart)
    {
        $item = PurchaseInvoiceCartItems::cart($cart->id)->find($itemId);
        abort_if(empty($item), 404);

        $item->quantity = $quantity;
        $item->save();

        $this->recalculatePurchaseInvoice($cart);

        return $item;
    }

    public function removeItem($itemId, PurchaseInvoiceCart $cart)
    {
        $item = PurchaseInvoiceCartItems::cart($cart->id)->find($itemId);
        abort_if(empty($item), 404);
        return $item->delete();
    }

    private function getUnitValues($unitFactor, $n)
    {
        return round($n/$unitFactor, 6);
    }
}