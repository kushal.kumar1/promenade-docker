<?php

namespace Niyotail\Services;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\City;
use Niyotail\Models\Pincode;
use Niyotail\Models\State;

class PincodeService extends Service
{
    public function create(Request $request)
    {
        $pincode = Pincode::create($request->all());
        return $pincode;
    }

    public function update(Request $request, $id)
    {
        $pincode = Pincode::find($id);
        $pincode->update($request->all());

        return $pincode;
    }
   
}
