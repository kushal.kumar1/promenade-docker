<?php

namespace Niyotail\Services\ReturnOrder;

use Carbon\Carbon;
use Niyotail\Events\ReturnOrderCompleted;
use Niyotail\Exceptions\ServiceException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\ReturnOrderItem;
use Niyotail\Models\Shipment;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\ReturnCart;

use Niyotail\Services\CreditNoteService;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Service;

class ReturnOrderService extends Service
{
    use ReturnOrderItemService;

    public function create(Request $request, $createdBy): ReturnOrder
    {
        $shipment = Shipment::with(['orderItems' => function ($query) {
            $query->canReturn();
        }])->with('order')->find($request->shipment_id);

        if (empty($shipment)) {
            throw new ServiceException('Invalid Shipment! Cannot create return');
        }

        $returnOrder = DB::transaction(function () use ($request, $shipment, $createdBy) {
            $returnOrder = $this->createReturnOrder($shipment, $request);
            $createMutiple = $request->createMultiple === true;
            if($createMutiple)
            {
                $returnItems = $request->items;
            }
            else
            {
                $returnItems = $this->getFormattedReturnOrderItems($request);
            }

            $request->merge(['items' => $returnItems]);

            $this->addItems($returnOrder, $shipment, $request);
            $returnOrder->load('order.store', 'items', 'orderItems.inventories', 'orderItems.productVariant');
            $returnOrder->refresh();
            $returnOrder->createdBy()->associate($createdBy);
            $returnOrder->warehouse_id = $shipment->warehouse_id;
            $returnOrder->reference_id = $request->ref_id ?? $this->generateReturnUniqueReferenceId($shipment->order->store_id);
            $returnOrder->save();
            $orderService = app(OrderService::class)->setAuthority(OrderService::AUTHORITY_ADMIN);
            $orderService->createLog($returnOrder->order, "Return #$returnOrder->id created !");
            return $returnOrder;
        });
        return $returnOrder;
    }

    private function createReturnOrder(Shipment $shipment, $request): ReturnOrder
    {
        $returnOrder = new ReturnOrder();
        $returnOrder->order_id = $shipment->order_id;
        $returnOrder->shipment_id = $shipment->id;
        $returnOrder->status = ReturnOrder::STATUS_INITIATED;
        $returnOrder->remarks = $request->remarks;
        $returnOrder->warehouse_id = $request->warehouse_id;
        $returnOrder->save();
        return $returnOrder;
    }

    public function cancel(Request $request)
    {
        $returnOrder = ReturnOrder::with('orderItems')->find($request->id);
        if(!$returnOrder->canCancel())
            throw new ServiceException("$returnOrder->status Return can't be cancelled.");
        return DB::transaction(function () use ($returnOrder) {
            $returnOrder->status = ReturnOrder::STATUS_CANCELLED;
            $returnOrder->save();
            $orderItemService = new OrderItemService();
            foreach ($returnOrder->orderItems as $orderItem) {
                $orderItemService->markDelivered($orderItem);
            }
            $orderService = app(OrderService::class)->setAuthority(OrderService::AUTHORITY_ADMIN);
            $orderService->createLog($returnOrder->order, "Return #$returnOrder->id cancelled !");
            return $returnOrder;
        });

    }

    public function confirmReturn($return, Request $request) {
        return DB::transaction(function() use($return, $request) {
            $returnItems = $request->return_item;
            $confirmedReturnItems = array();
            foreach ($returnItems as $returnItem) {
                $sku = $returnItem['sku'];
                $returnQty = $returnItem['quantity'];
                $variant = ProductVariant::where('sku', $sku)->withTrashed()->firstOrFail();
                $returnOrderItems = ReturnOrderItem::with(['orderItem' => function($q) use ($returnItem) {
                    $q->where('sku', $returnItem['sku']);
                }])->whereHas('orderItem', function ($q) use($returnItem) {
                    $q->where('sku', $returnItem['sku']);
                })->where('return_order_id', $return->id)->get();

                $initialReturnUnits = $returnOrderItems->sum('units');

                if($initialReturnUnits != $returnItem['quantity']) {
                    // quantity changed during confirmation
                    $ceilInitialReturnVariantCount = $returnOrderItems->count();
                    $ceilNewReturnVariantCount = ceil($returnQty/$variant->quantity);

                    // if ceil variant count is not same - then we need to add or remove a new return item accordingly
                    if($ceilInitialReturnVariantCount > $ceilNewReturnVariantCount) {
                        // Remove one return variant and distribute qty accordingly
                        $itemsToRemove = $ceilInitialReturnVariantCount - $ceilNewReturnVariantCount;

                        for($i = 0; $i < $itemsToRemove; $i++) {
                            $returnItemToRemove = $returnOrderItems[$i];
                            $orderItemToReset = $returnItemToRemove->orderItem;

                            $orderItemToReset->status = OrderItem::STATUS_DELIVERED;
                            $orderItemToReset->save();

                            $returnItemToRemove->delete();
                            $returnOrderItems->forget($i);
                        }

                    }
                    else if($ceilInitialReturnVariantCount < $ceilNewReturnVariantCount) {
                        $newReturnIncrement = $ceilInitialReturnVariantCount < $ceilNewReturnVariantCount;

                        // check if order item to return is available
                        $orderItemsWithReturnSku = OrderItem::where('order_id', $return->order_id)
                            ->where('sku', $sku)->where('status', OrderItem::STATUS_DELIVERED)->get();

                        if($orderItemsWithReturnSku->count() < $newReturnIncrement) {
                            throw new \Exception('Invalid return quantity for '.$sku);
                        }

                        // Add return variants
                        $newItemsToBeReturned = $orderItemsWithReturnSku->take($newReturnIncrement);
                        foreach ($newItemsToBeReturned as $item) {
                            // update order item status
                            $item->status = OrderItem::STATUS_PENDING_RETURN;
                            $item->save();

                            // create new return item
                            $newReturnItem = new ReturnOrderItem();
                            $newReturnItem->return_order_id = $return->id;
                            $newReturnItem->order_item_id = $item->id;
                            $newReturnItem->units = $variant->quantity;
                            $newReturnItem->save();
                        }
                    }
                }

                // Distribute return quantity amongst return items
                // fetch fresh return order items
                $returnOrderItems = ReturnOrderItem::with(['orderItem' => function($q) use ($sku) {
                    $q->where('sku', $sku);
                }])->whereHas('orderItem', function ($q) use($sku) {
                    $q->where('sku', $sku);
                })->where('return_order_id', $return->id)->get();
                foreach ($returnOrderItems as $item) {
                    $item->units = $returnQty > $variant->quantity ? $variant->quantity : $returnQty;
                    $item->reason = $returnItem['reason'];
                    $item->save();
                    $returnQty -= $variant->quantity;
                }

                $confirmedReturnItems = array_merge($confirmedReturnItems, $returnOrderItems->toArray());
            }

            return $this->complete($return, $confirmedReturnItems);
        });
    }

    public function confirmSpecialReturn($return) {
        return DB::transaction(function() use($return) {
            $returnItems = $return->items->toArray();
            $this->completeSpecialReturn($return, $returnItems);
        });
    }

    public function completeSpecialReturn($returnOrder, $confirmedReturnItems) {
        if(!$returnOrder->canComplete())
        {
            throw new ServiceException("$returnOrder->status Return can't be completed.");
        }

        return DB::transaction(function () use ($returnOrder, $confirmedReturnItems) {
            $orderItemService = new OrderItemService();
            foreach ($confirmedReturnItems as $returnItem) {
                $returnItem = ReturnOrderItem::find($returnItem['id']);
                $orderItem = OrderItem::find($returnItem['order_item_id']);
                $orderItemService->markSpecialReturn($orderItem, $returnItem);
            }

            $returnOrder->status = ReturnOrder::STATUS_COMPLETED;
            $returnOrder->save();

            (new CreditNoteService())->setAuthority(CreditNoteService::AUTHORITY_SYSTEM)->create($returnOrder);
            $orderService = app(OrderService::class)->setAuthority(OrderService::AUTHORITY_ADMIN);
            $orderService->createLog($returnOrder->order, "Return #$returnOrder->id completed !");
            event(new ReturnOrderCompleted($returnOrder));
            return $returnOrder;
        });
    }

    /**
     * @throws ServiceException
     */
    public function complete($returnOrder, $confirmedReturnItems)
    {
        if(!$returnOrder->canComplete())
        {
            throw new ServiceException("$returnOrder->status Return can't be completed.");
        }

        return DB::transaction(function () use ($returnOrder, $confirmedReturnItems) {
            $orderItemService = new OrderItemService();
            $itemsToBeReturned = collect([]);
            foreach ($confirmedReturnItems as $returnItem) {
                $returnItem = ReturnOrderItem::with('orderItem.itemInventory')->find($returnItem['id']);
                if(empty($returnItem->orderItem->itemInventory)){
                    (new FlatInventoryService())->generateSoldInventories($returnItem->orderItem);
                }
                $returnItem->refresh();
                $itemsToBeReturned->push($returnItem);
            }
            $orderItemService->markReturn($itemsToBeReturned);
            $returnOrder->status = ReturnOrder::STATUS_COMPLETED;
            $returnOrder->save();
            (new CreditNoteService())->setAuthority(CreditNoteService::AUTHORITY_SYSTEM)->create($returnOrder);
            $orderService = app(OrderService::class)->setAuthority(OrderService::AUTHORITY_ADMIN);
            $orderService->createLog($returnOrder->order, "Return #$returnOrder->id completed !");
            event(new ReturnOrderCompleted($returnOrder));
            return $returnOrder;
        });
    }

    private function getFormattedReturnOrderItems($request)
    {
        $returnItems = [];
        foreach ($request->items as $item)
        {
            if(empty($item['sku'])) continue;

            $variant = ProductVariant::sku($item['sku'])->withTrashed()->first();
            $product = $variant->product;
            $allVariants = $product->allVariants;
            $totalUnits = 0;
            foreach ($allVariants as $v)
            {
                $q = $v->quantity;
                $returnUnits = 0;
                if(!empty($item[$v->value]))
                {
                    $returnUnits = $q*$item[$v->value];
                }
                $totalUnits += $returnUnits;
            }

            $returnItems[$item['sku']]['sku'] = $item['sku'];
            $returnItems[$item['sku']]['quantity'] = ceil($totalUnits/$variant->quantity);
            $returnItems[$item['sku']]['units'] = $totalUnits;
            $returnItems[$item['sku']]['reason'] = $item['reason'];
        }

        return $returnItems;
    }

    public function createMultiple(Request $request, ReturnCart $cart, $createdBy)
    {
        $cart = DB::transaction(function () use ($request, $cart, $createdBy) {
            $refId = $this->generateReturnUniqueReferenceId($cart->store_id);
            foreach($cart->itemsByShipment as $shipmentID => $items) {
                $returnRequest = new Request();
                $returnRequest->merge(['shipment_id' => $shipmentID, 'warehouse_id' => $cart->warehouse_id, 'ref_id' => $refId]);
                $returnItems = [];
                foreach($items as $item) {
                  $returnItems[$item->sku]['sku'] = $item->sku;
                  $returnItems[$item->sku]['quantity'] = $item->quantity;
                  $returnItems[$item->sku]['units'] = $item->units;
                  $returnItems[$item->sku]['reason'] = $item->reason;
                }

                $returnRequest->merge(['createMultiple' => true]);
                $returnRequest->merge(['items' => $returnItems]);
                $returnOrder = $this->setAuthority(ReturnOrderService::AUTHORITY_ADMIN)->create($returnRequest, $createdBy);
                DB::table('return_cart_items')
                   ->where('return_cart_id', $cart->id)
                   ->where('shipment_id', $shipmentID)
                   ->update(['return_order_id' => $returnOrder->id]);
            }
            $cart->status = 'completed';
            $cart->save();
            return $cart;
        });
        return $cart;
    }

    public function generateReturnUniqueReferenceId($storeId, $date = null): string
    {
        $createdDate = Carbon::now('Asia/Kolkata')->format('Ymd');
        if (!empty($date)) {
            $createdDate = Carbon::parse($date)->format('Ymd');
        }
        return 'RO-'.$storeId.'-'.$createdDate;
    }
}
