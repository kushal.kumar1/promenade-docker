<?php

namespace Niyotail\Services\ReturnOrder;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Shipment;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\ReturnOrderItem;
use Illuminate\Http\Request;
use Niyotail\Services\Order\OrderItemService;

// Note: Last changed by Navi on May 19, 2020

trait ReturnOrderItemService
{
    private function addItems(ReturnOrder $returnOrder, Shipment $shipment, Request $request)
    {
        $returnOrderItems = [];
        // calculate quantity in units

        foreach ($request->items as $item) {

            $itemSku = $item['sku'];

            $itemsToBeReturned = $shipment->returnableOrderItems->where('sku', $itemSku)->sortByDesc('quantity')->toArray();

            $itemsToBeReturned = collect(array_values($itemsToBeReturned));

            $productVariant = ProductVariant::sku($itemSku)->withTrashed()->first();
            $variantQty = $productVariant->quantity;

            if ($itemsToBeReturned->isEmpty()) {
                 throw new ServiceException('Invalid items given, Cannot return items.');
//                \Log::info($itemSku);
//                continue;
            }

            if(($item['quantity'] - (int) $item['quantity']) != 0) {
                if ($itemsToBeReturned->where('quantity', number_format($item['quantity'] - (int) $item['quantity'], 3))->count() === 0) {
                    throw new ServiceException('Invalid item quantity. Cannot return.');
                }
            }

            if ($itemsToBeReturned->count() < $item['quantity']) {
                throw new ServiceException($itemSku. " can't be returned");
            }

            $unitsToReturn = $item['units'];
            $returnItems = [];
            for($i = 0; $i < $item['quantity']; $i++) {
                $orderItem = OrderItem::findOrFail($itemsToBeReturned[$i]['id']);
                $isFullReturn = $unitsToReturn > $variantQty;

                $returnItems[] = [
                    "return_order_id" => $returnOrder->id,
                    "order_item_id" => $orderItem->id,
                    "units" => $isFullReturn ? $variantQty : $unitsToReturn,
                    "reason" => !empty($item['reason']) ? $item['reason'] : $request->reason
                ];

                $orderItemService = app(OrderItemService::class);
                $orderItemService->markPendingReturn($orderItem);

                $unitsToReturn -= $variantQty;
            }

            ReturnOrderItem::insert($returnItems);
        }


    }
}
