<?php


namespace Niyotail\Services\PosOrder;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\Category\Cl4CostMap;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Pincode;
use Niyotail\Models\PosOrder;
use Niyotail\Models\PosOrderItem;
use Niyotail\Models\PosOrderItemMeta;
use Niyotail\Models\PosOrderItemShipment;
use Niyotail\Models\PosOrderItemTax;
use Niyotail\Models\Product;
use Niyotail\Models\Tax;

/**
 * Class SyncOrderService
 * @package Niyotail\Services\PosOrder
 */
class SyncPosOrderService
{

    /**
     * @param $data
     * @return mixed
     */
    public function syncOrder($data) {
        $data = $this->getFormattedOrderDataFromSqs($data);
        $store = $data['store'];
        $order = $this->storeOrder($data['order'], $store);
        $this->storeOrderItems($data['items'], $order);
        return $order;
    }

    /**
     * @param $orderDetails
     * @param $store
     * @return mixed
     */
    private function storeOrder($orderDetails, $store) {
        $invoiced_at = null;

//        check case-sensitive issue of invoiced_At
        if(!empty($orderDetails['invoiced_At'])) $invoiced_at = $orderDetails['invoiced_At'];
        else if(!empty($orderDetails['invoiced_at'])) $invoiced_at = $orderDetails['invoiced_at'];

        $order = PosOrder::orderId($orderDetails['id'])->first();
        if(empty($order)) {
            $order = new PosOrder();
            $order->order_id = $orderDetails['id'];
        }

        $order->ref_id = $orderDetails['ref_id'];
        $order->invoice_number = $orderDetails['invoice_number'];
        $order->store_id = $store['branding'];
        $order->invoiced_at = $invoiced_at;
        $order->ordered_at = $orderDetails['ordered_at'];
        $order->status = $orderDetails['status'];
        $order->pos_customer_id = !empty($orderDetails['customer_id']) ? $orderDetails['customer_id'] : null;
        $order->platform = $orderDetails['platform'];
        $order->type = !empty($orderDetails['type']) ? $orderDetails['type'] : 'retail';
        $order->save();
        return $order;
    }

    /**
     * @param $items
     * @param $order
     */
    private function storeOrderItems($items, $order) {

        /* Delete all existing items of this order - if any  */
        PosOrderItem::orderId($order->id)->delete();

        $store = $order->store;
        $storeCommissionTags = $store->commissionTags;
        $pincode = Pincode::where('pincode', $store->pincode)->first();
        $stateId = $pincode->city->state_id;

        $isMallOrder = $order->isMallOrder();
        $mallOrderCommission = [];
        if($isMallOrder) {
            $mallOrderCommission = $this->getMallOrderCommission($items, $order);
        }

        $formattedItems = $this->getFormattedOrderItems($items, $order);
        foreach ($formattedItems as $item) {

            $product = Product::find($item['niyotail_product_id']);
            if(empty($product)) {
                Log::info('RCN ISSUE - Product not found in order '.$order->id.' product id - '.$item['niyotail_product_id']);
                continue;
            }

            $orderItem = new PosOrderItem();
            $orderItem->item_id = $item['id'];
            $orderItem->parent_id = $item['parent_id'];
            $orderItem->product_id = $item['niyotail_product_id'];
            $orderItem->pos_order_id = $order->id;
            $orderItem->pos_product_id = $item['product_id'];
            $orderItem->uom = $item['uom'];
            $orderItem->quantity = $item['quantity'];
            $orderItem->price = $item['price'];
            $orderItem->original_price = $item['original_price'];
            $orderItem->mrp = $item['mrp'];
            $orderItem->subtotal = $item['subtotal'];
            $orderItem->tax = $item['tax'];
            $orderItem->discount = $item['discount'];
            $orderItem->total = $item['total'];
            $orderItem->status = $item['status'];
            $orderItem->source = $item['source'];
            $orderItem->save();

            $this->storeOrderItemShipments($orderItem, $item['shipments']);

            $discounts = $item['discounts'];
            $this->storeOrderItemTaxes($orderItem, $stateId);
            $this->storeOrderItemMeta($orderItem, $discounts, $storeCommissionTags, $mallOrderCommission);
        }
    }

    /**
     * @param $data
     * @return array
     */
    private function getFormattedOrderDataFromSqs($data) {
        $orderData = $data['payload'];
        $order = $orderData['order'];
        $store  = $orderData['store'];
        $items = $orderData['items'];
        unset($orderData['items'], $orderData['store']);
        return [
            'order' => $order,
            'store' => $store,
            'items' => $items
        ];
    }

    /**
     * @param $items
     * @param $order
     * @return array
     */
    private function getFormattedOrderItems($items, $order) {
        $orderId = $order->id;
        $formattedItems = [];
        foreach ($items as $item) {
            $productId = $item['niyotail_product_id'];
            $item['order_id'] = $orderId;

            $inventories = $item['inventories'];
            $shipments = $this->getFormattedOrderItemShipments($inventories, $productId);

            $item['shipments'] = $shipments;
            $formattedItems[] = $item;
        }

        return $formattedItems;
    }

    /**
     * @param $inventoryTransactions
     * @param $pId
     */
    private function getFormattedOrderItemShipments($inventories, $pId) {
        $shipmentCosts = [];
        if(!empty($inventories)) {
            foreach($inventories as $inventory) {

                // here
                $quantity = abs($inventory['quantity']);
                $batchNumber = null;
                $shipmentId = null;
                $unitCost = 0;
                $sellingPrice = 0;
                if(!empty($inventory['batch_number'])) {
                    $batchNumber = $inventory['batch_number'];
                    $decodedBatchNo = explode('-', $batchNumber);
                    $countBatchElements = count($decodedBatchNo);

                    if($countBatchElements >= 7) {
                        // check if NT_SH
                        $shipmentIdentifier = $decodedBatchNo[5];
                        if(strpos($shipmentIdentifier, 'NT_SH') !== false) {
                            // Shipment is from niyotail -
                            // get shipment id
                            $decodedShipment = explode('_', $shipmentIdentifier);
                            $shipmentId = $decodedShipment[2];
                        }
                    }
                }

                if(!empty($shipmentId)) {
                    $costDetails = $this->getProductCostByShipment($shipmentId, $pId);
                    $unitCost = $costDetails['unit_cost'];
                    $sellingPrice = $costDetails['selling_price'];
                }

                $shipmentCosts[] = [
                    'unit_cost' => $unitCost,
                    'selling_price' => $sellingPrice,
                    'shipment_id' => $shipmentId,
                    'batch_number' => $batchNumber,
                    'quantity' => $quantity
                ];
            }
        }

        return $shipmentCosts;
    }

    /**
     * @param $shipmentId
     * @param $productId
     * @return array
     */
    private function getProductCostByShipment($shipmentId, $productId) {
        $unitCost = 0;
        $sellingPrice = 0;

        /* Using raw queries to avoid loading unnecessary relations */
        $shipmentItemCost = DB::select("select
            round((sum((oii.quantity*oii.unit_cost_price))/sum(oii.quantity)),6) AS unit_cost,
            round((sum((oi.quantity*fi.unit_cost))/sum(oi.quantity)),6) AS fi_unit_cost
            from shipment_items shi
            left join shipments sh on sh.id = shi.shipment_id
            left join order_items oi on oi.id = shi.order_item_id
            left join order_item_inventories oii on oii.order_item_id = oi.id
            left join order_item_flat_inventories oifi on oifi.order_item_id = oi.id
            left join flat_inventories fi on oifi.flat_inventory_id = fi.id
            left join product_variants pv on pv.sku = oi.sku
            where sh.status not in ('cancelled') and sh.id = $shipmentId and oi.product_id = $productId
            group by shi.shipment_id, oi.product_id");

        if(count($shipmentItemCost)) {
            $unitCost = $shipmentItemCost[0]->unit_cost ?? $shipmentItemCost[0]->fi_unit_cost;
        }

        $shipmentItemSp = DB::select("select oi.total/pv.quantity as total from shipment_items si
            left join order_items as oi on si.order_item_id = oi.id
            left join product_variants pv on oi.sku = pv.sku
            where si.shipment_id = $shipmentId and oi.product_id = $productId group by oi.product_id");

        if(count($shipmentItemSp)) {
            $sellingPrice = $shipmentItemSp[0]->total;
        }

        return [
            'unit_cost' => $unitCost,
            'selling_price' => $sellingPrice
        ];
    }

    /**
     * @param PosOrderItem $orderItem
     * @param $discounts
     * @param $storeCommissionTags
     */
    public function storeOrderItemMeta(PosOrderItem $orderItem, $discounts, $storeCommissionTags, $mallOrderDetails = []) {
        if($orderItem->status == 'cancelled') {
            $meta = [
                'discount' => 0,
                'promo' => 0,
                'commission' => 0
            ];
        }
        else {
            $meta = $this->getOrderItemDiscountAndPromo($orderItem, $discounts, $mallOrderDetails);
        }

        $orderItemMeta = PosOrderItemMeta::firstOrNew(['pos_order_item_id' => $orderItem->id]);
        $orderItemMeta->discount = $meta['discount'];
        $orderItemMeta->promotion = $meta['promo'];
        $orderItemMeta->commission = $meta['commission'];
        $orderItemMeta->save();
    }

    /**
     * @param PosOrderItem $orderItem
     * @param $shipments
     */
    private function storeOrderItemShipments(PosOrderItem $orderItem, $shipments) {
        /* Delete all existing order items shipments */
        PosOrderItemShipment::OrderItem($orderItem->id)->forceDelete();

        /* HACK TO FIX DUPLICATE SHIPMENTS FROM POS */
        $shipCollect = collect($shipments);
        if($shipCollect->sum('quantity') == $orderItem->quantity*2) {
            if(!empty($shipments[1])) { // TO BE DOUBLY SURE
                unset($shipments[1]);
            }
        }

        foreach ($shipments as $shipment) {
            $orderItemShipment = new PosOrderItemShipment();
            $orderItemShipment->pos_order_item_id = $orderItem->id;
            $orderItemShipment->shipment_id = $shipment['shipment_id'];
            $orderItemShipment->batch_number = $shipment['batch_number'];
            $orderItemShipment->quantity = $shipment['quantity'];
            $orderItemShipment->unit_cost = $shipment['unit_cost'];
            $orderItemShipment->selling_price = $shipment['selling_price'];
            $orderItemShipment->save();
        }
    }

    /**
     * @param PosOrderItem $orderItem
     * @param $discounts
     * @param $storeCommissionTags
     * @return array
     */
    private function getOrderItemDiscountAndPromo(PosOrderItem $orderItem, $discounts, $mallItemCommission = [])
    {

        /* @var Product $product */
        $product = $orderItem->product;
        $unitVariant = $product->unitVariant;
        $unitMrp = $unitVariant->mrp;

        $shipments = $orderItem->shipments;
        $ntShipments = $shipments->where('shipment_id', '!=', null);

        $qtySoldByNt = $ntShipments->sum('quantity');

        if(!$ntShipments->count() || !$qtySoldByNt) {
            $ntSellingPrice = $unitMrp;
        }
        else {
            $ntSellingPrice = 0;
            foreach ($ntShipments as $ntShipment) {
                $ntSellingPrice += $ntShipment->selling_price*$ntShipment->quantity;
            }
            $ntSellingPrice = round($ntSellingPrice/$qtySoldByNt, 6);
        }

        /* New commission tag from cost map */
        $marketer = $product->brand->marketer;
        $marketerLevel = $marketer->level_id;
        $cl4Id = $product->cl4_id;

        $productCommissionTag = null;
        $totalCommission = 0;

        if (!empty($mallItemCommission) && isset($mallItemCommission[$orderItem->item_id]) ) {
            /* 1K Mall new commission calculation */
            $totalCommission = $mallItemCommission[$orderItem->item_id];
        } else {
            $costMap = Cl4CostMap::marketerLevelId($marketerLevel)->Cl4Id($cl4Id)->first();
            if($costMap) {
                $productCommissionTag = $costMap->commissionTag;
                if($productCommissionTag) {
                    $basicCommission = $productCommissionTag->base_commission;
                    $totalCommission = $orderItem->total*($basicCommission);
                }
            }
        }

        $finalQty = $orderItem->quantity < 0 ? $orderItem->quantity : $qtySoldByNt;

        if($ntSellingPrice > $unitMrp) $ntSellingPrice = $unitMrp;
        $totalBillingPrice = $ntSellingPrice*$finalQty;
        $priceGapDiscount = $totalBillingPrice - $orderItem->price*$finalQty;
        $promotionalDiscount = 0;
        $storeDiscount = 0;
        foreach ($discounts as $discount) {
            if($discount['rule_id']) {
                $promotionalDiscount += $discount['value'];
            }
            else {
                $storeDiscount += $discount['value'];
            }
        }

        $finalPromo = $priceGapDiscount + $promotionalDiscount;
        $finalPromo = $finalPromo < 0 && $finalQty > 0 ? 0 : $finalPromo;

        return [
            'discount' => $storeDiscount,
            'promo' => $finalPromo,
            'commission' => $totalCommission
        ];
    }

    /**
     * @param OrderItem $orderItem
     * @param $taxes
     */
    public function storeOrderItemTaxes(PosOrderItem $orderItem, $stateId) {
        PosOrderItemTax::ItemId($orderItem->id)->forceDelete();

        $product = $orderItem->product;
        $taxClass = $product->tax_class_id;
        $taxes = Tax::where('tax_class_id', $taxClass)
            ->where('source_state_id', 13)
            ->where(function ($query) use ($stateId) {
                if($stateId == 13) {
                    $query->where('supply_state_id', 13);
                } else {
                    $query->whereNull('supply_state_id');
                }
            })->get();

        $itemTotal = $orderItem->total;
        $quantity = $orderItem->quantity;

        $totalPercent = $taxes->sum('percentage');
        $totalTaxVal = round($itemTotal - ($itemTotal/(1 + ($totalPercent/100))), 6);

        foreach ($taxes as $tax) {
            $taxRow = new PosOrderItemTax();
            $taxRow->pos_order_item_id = $orderItem->id;
            $taxRow->name = $tax->name;
            $taxRow->percentage = $tax->percentage;
            if(!empty($tax->percentage))
            {
                $taxRow->value = round(($tax->percentage/$totalPercent)*$totalTaxVal, 6);
            }
            else if (!empty($tax->additional_charges_per_unit)) {
                $taxRow->value = round($tax->additional_charges_per_unit*$quantity, 6);
            }

            $taxRow->save();
        }
    }

    private function getMallOrderCommission($orderItems) {
        $items = collect($orderItems);
        $itemWiseCommission = [];
        /*
         * case
                when sum(poi2.total) < -500  then -100
                when sum(poi2.total) < -200 then -50
                when sum(poi2.total) < 200  then 0
                when sum(poi2.total) <= 500 then 50
                when sum(poi2.total) > 500 then 100
           end
       * */

        /*
         * step 1 - segregate +ve and -ve items
         * step 2 - get order total on the basis of +ve items and calculate order level commission and assign to items
         * step 3 - get unique orders from -ve items
         * step 4 - for each order get initial commission and then revised commission
         * step 5 - get Rc - ic and divide the new commission difference accordingly in -ve items
         * */

        $fulfilledItems = $items->where('total', '>', 0);
        $fulfilledTotal = $fulfilledItems->sum('total');
        $fulfilledCommission = $this->getMallCommissionByTotal($fulfilledTotal);
        foreach ($fulfilledItems as $item) {
            $itemWiseCommission[$item['id']] = $item['total']*$fulfilledCommission/$fulfilledTotal;
        }

        $returnedItems = $items->where('total', '<', 0);
        $validReturnedItems = $returnedItems->where('parent_id', '!=', null);
        $returnParentOrders = [];

        foreach ($validReturnedItems as $returnedItem) {
            $parentItem = PosOrderItem::where('item_id', $returnedItem['parent_id'])->first();
            if(empty($parentItem)) {
                Log::error('Invalid parent item id '.$returnedItem['parent_id']);
                continue;
            }
            $returnParentOrders[] = $parentItem['pos_order_id'];
        }

        $returnedOrders = PosOrder::whereIn('id', $returnParentOrders)->with('items.returnItem')->get();
        foreach ($returnedOrders as $order) {
            $orderTotal = $order->items->sum('total');
            $returnTotal = 0;
            foreach ($order->items as $item) {
                if($item->returnItem) $returnTotal += $item->returnItem->sum('total');
            }

            $revisedTotal = $orderTotal + $returnTotal;
            $commission = $this->getMallCommissionByTotal($revisedTotal);
            $returnItem = $items->whereIn('parent_id', $order->items->pluck('item_id'))->first();
            if($returnItem) {
                $revisedTotal = $revisedTotal + $returnItem['total'];
                $rc = $this->getMallCommissionByTotal($revisedTotal);
                $c = $rc - $commission;
                $itemWiseCommission[$returnItem['id']] = $c;
            }
        }

        return $itemWiseCommission;
    }

    private function getMallCommissionByTotal($total) {
        $totalCommission = 0;

        if($total > 0 && $total < 200) $totalCommission = 0;
        if($total >= 200 && $total <= 500) $totalCommission = 50;
        if($total > 500) $totalCommission = 100;

        if($total < 0 and $total > -200) $totalCommission = 0;
        if($total <= -200 and $total >= -500) $totalCommission = -50;
        if($total < -500) $totalCommission = -100;

        return $totalCommission;
    }
}
