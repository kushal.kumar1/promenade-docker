<?php


namespace Niyotail\Services\PosOrder;


use Illuminate\Http\Request;
use Niyotail\Models\Employee;
use Niyotail\Models\ProductVariant;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class OneKMallOrderService
{
    /**
     * @var
     */
    protected $employee;
    /**
     * @var CartService
     */
    protected $cartService;
    /**
     * @var StoreOrderService
     */
    protected $orderService;

    /**
     * OneKMallOrderService constructor.
     */
    public function __construct()
    {
        /* 1K Mall Employee */
        $this->employee = Employee::find('75');
        $this->cartService = new CartService();
        $this->orderService = new StoreOrderService();
    }

    /**
     * @param $data
     */
    public function createOrder($data) {
        $cartItems = $data['payload'];

        $cart = null;
        foreach ($cartItems as $cartItem) {
            $productVariant = ProductVariant::where('sku', $cartItem['sku'])->withTrashed()->first();
            $cartItem['id'] = $productVariant->id;
            $request = new Request();
            $request->replace($cartItem);
            $cart = $this->cartService->addToCart($request, $this->employee);
        }

        $storeOrderService = new StoreOrderService();
        $storeOrderService->create($cart);
    }
}