<?php

namespace Niyotail\Services;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Agent;
use Niyotail\Models\Invoice;
use Niyotail\Models\InvoiceTransaction;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\RateCreditNote;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Store;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\Transaction;
use Niyotail\Helpers\File;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

class TransactionService extends Service
{
    private function getTransactionId()
    {
        return 'NT-' . time() . rand(10, 99);
    }

    public function deleteTransaction(Request $request, Authenticatable $user, Transaction $transaction)
    {
        return DB::transaction(function() use($request, $user, $transaction) {
            $transaction->remarks = !empty($request->remarks) ? $request->remarks : null;
            if ($transaction->payment_method != Transaction::PAYMENT_METHOD_CHEQUE) {
                $transaction->status = Transaction::STATUS_CANCELLED;
                $transaction->save();
                //$this->revertInvoices($transaction);
                $this->revertTransactions($transaction);
                $this->modifyOtherTransactionBalances($transaction);
                $transaction->delete();
            } else {
               $this->removeChequeTransaction($transaction);
            }
            return $transaction;
        });
    }

    public function approveTransaction(Request $request, Authenticatable $user, Transaction $transaction)
    {
        return DB::transaction(function () use ($request, $user, $transaction) {
            $this->checkIfValidForApproval($transaction);
            $transaction->remarks = !empty($request->remarks) ? $request->remarks : null;
            $transaction->status = Transaction::STATUS_COMPLETED;
            if ($request->payment_tag != null) {
                $transaction->payment_tag = $request->payment_tag;
            }
            $date = (new DateTime())->format('Y-m-d H:i:s');
            $transaction->transaction_date = $date;
            $transaction->save();
            $this->modifyTransactionBalanceAfterApproval($transaction);
            return $transaction;
        });
    }

    public function createForInvoice(Invoice $invoice, $type)
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);

        DB::transaction(function () use ($type, $invoice) {
            $invoice->load('order');
            $transaction = new Transaction();
            $transaction->transaction_id = 'NT-' . time() . rand(10, 99);
            $transaction->store_id = $invoice->order->store_id;
            $transaction->description = "Invoice number $invoice->reference_id $invoice->status.";
            $transaction->type = $type;
            $transaction->amount = $invoice->amount;
            $transaction->status = Transaction::STATUS_COMPLETED;
            $transaction->transaction_date = (new DateTime())->format('Y-m-d H:i:s');

            $invoice->transactions()->save($transaction);
            $this->modifyBalance($transaction);

            $storeOrder = StoreOrder::where('order_id', $invoice->order_id)->first();
            $purchaseInvoiceId = DB::selectOne('select purchase_invoice_id from store_purchase_invoices where store_id = ? and assigned_id = ?;', [
                    $storeOrder->store_id,
                    $storeOrder->id
                ])->purchase_invoice_id ?? null;
            $purchaseInvoice = PurchaseInvoice::find($purchaseInvoiceId);
//            if (empty($purchaseInvoice)) {
//                return $transaction;
//            }

            if (($purchaseInvoice->is_direct_store_purchase ?? false) and $type == Transaction::TYPE_DEBIT) {

                if ($purchaseInvoice->transactions()->where('type', Transaction::TYPE_CREDIT)->count() == 0) {

                    $purchaseReimbursementTransaction = new Transaction();
                    $purchaseReimbursementTransaction->transaction_id = 'NT-' . time() . rand(10, 99);
                    $purchaseReimbursementTransaction->store_id = $invoice->order->store_id;
                    $purchaseReimbursementTransaction->description = "Direct Store Purchase Reimbursement for Invoice number $invoice->reference_id and Purchase Invoice Number $purchaseInvoice->reference_id.";
                    $purchaseReimbursementTransaction->type = Transaction::TYPE_CREDIT;
                    $purchaseReimbursementTransaction->amount = $purchaseInvoice->total_invoice_value;
                    $purchaseReimbursementTransaction->status = Transaction::STATUS_COMPLETED;
                    $purchaseReimbursementTransaction->transaction_date = (new DateTime())->format('Y-m-d H:i:s');

                    $purchaseReimbursementTransaction->save();
                    $purchaseInvoice->transactions()->sync([$transaction->id, $purchaseReimbursementTransaction->id]);
                    $this->modifyBalance($purchaseReimbursementTransaction);
                } else {
                    Log::warning("Not creating a Reimbursement for Store External Purchase because a Credit Transaction Already exists for the PurchaseInvoice", [
                        'storeOrder' => $storeOrder,
                        'invoice' => $invoice,
                        'purchaseInvoice' => $purchaseInvoice
                    ]);
                }
            }

            return $transaction;
        });
    }

    public function removeForCancelledInvoice(Invoice $invoice)
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $transaction = Transaction::where('source_type', array_search(Invoice::class, Transaction::$map))
            ->where('source_id', $invoice->id)->where('type', Transaction::TYPE_DEBIT)->first();
        if (!empty($transaction)) {
            $this->modifyOtherTransactionBalances($transaction);
            $transaction->delete();
        }
    }

    public function createForCreditNote(CreditNote $creditNote)
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $creditNote->load('order');
        $transaction = new Transaction();
        $transaction->transaction_id = 'NT-' . time() . rand(10, 99);
        $transaction->store_id = $creditNote->order->store_id;
        $transaction->description = "Credit Note Generated $creditNote->reference_id.";
        $transaction->type = Transaction::TYPE_CREDIT;
        $transaction->amount = $creditNote->amount;
        $transaction->status = Transaction::STATUS_COMPLETED;
        $date = (new DateTime())->format('Y-m-d H:i:s');
        $transaction->transaction_date = $date;
        $creditNote->transaction()->save($transaction);

        $request = new Request();
        $invoice = Invoice::where('order_id', $creditNote->order_id)->where('shipment_id', $creditNote->returnOrder->shipment_id)->first();
        // $request->request->add(['auto_settle' => 0]);
        // $inv[$invoice->id]['amount'] = $creditNote->amount;
        // $request->merge(['settle_invoices' => $inv]);
        // $settledInvoices = $this->settleInvoices($transaction, $request);
        // $transaction->invoices()->sync($settledInvoices['amount']);

        // TODO - Check for transactions table character encoding
        $transactions = DB::table('transactions')
            ->where('source_type', 'invoice')
            ->where('source_id', $invoice->id)
            ->get();
        $sourceInvoiceTransaction = $transactions->first();
//        $sourceInvoiceTransaction = $invoice->transactions->first();

        $inv[$sourceInvoiceTransaction->id]['amount'] = $creditNote->amount;
        $request->merge(['settle_transactions' => $inv]);
        $settledTransactions = $this->settleTransactions($transaction, $request);
        $transaction->settledDebitTransactions()->sync($settledTransactions['amount']);
        $this->modifyBalance($transaction);
        return $transaction;
    }

    private function assignAgent($transaction, Agent $agent)
    {
        $transaction->created_by_type = 'agent';
        $transaction->created_by_id = $agent->id;
        return $transaction;
    }

    private function revertInvoices($transaction)
    {
        $invoices = $transaction->invoices;
        foreach ($invoices as $invoice) {
            $invoiceTransaction = InvoiceTransaction::where('invoice_id', $invoice->id)->where('transaction_id', $transaction->id)->first();
            $remainingAmount = $invoice->amount - $invoiceTransaction->amount;
            if ($remainingAmount == 0) {
              $invoice->status = 'generated';
            } else {
              $invoice->status = 'partially_settled';
            }
            $invoice->save();
            $invoiceTransaction->delete();
        }
    }

    private function modifyOtherTransactionBalances($transaction)
    {
      $operation = ($transaction->type == 'debit') ? '+': '-';
      $transaction->balance = 0;
      DB::table('transactions')
         ->where('transaction_date', '>=', $transaction->transaction_date)
         ->where('store_id', $transaction->store_id)
         ->where('status', Transaction::STATUS_COMPLETED)
         ->update(['balance' => DB::raw("balance $operation ".$transaction->amount)]);
      $transaction->save();
    }

    private function modifyBalance($transaction) {
        if ($transaction->status != Transaction::STATUS_PENDING) {
          $store = Store::find($transaction->store_id);
          $transaction->balance = $store->totalBalance->credit_amount - $store->totalBalance->debit_amount;
        } else {
          $transaction->balance = 0;
        }
        $transaction->save();
    }

    private function checkIfValidForApproval($transaction)
    {
        if ($transaction->payment_method == Transaction::PAYMENT_METHOD_CHEQUE && !empty($transaction->payment_details)) {
            $chequeDetail = json_decode($transaction->payment_details);
            $chequeDate = Carbon::createFromFormat("d M, Y", $chequeDetail->cheque_date)->format('Y-m-d');
            $today = Carbon::today()->setTimezone('Asia/Kolkata')->format('Y-m-d');
            if ($today < $chequeDate) {
                throw new ServiceException("This cheque can only be approved on or after $chequeDetail->cheque_date.");
            }
        }
    }

    private function getTransactionStatusForCheque($chequeDate)
    {
      // $chequeDate = Carbon::createFromFormat("d M, Y", $chequeDate);
      // $today = Carbon::today();
      // if ($today < $chequeDate) {
      //     return Transaction::STATUS_PENDING;
      // }
      // return Transaction::STATUS_COMPLETED;
      return Transaction::STATUS_PENDING;
    }

    private function modifyTransactionBalanceAfterApproval($transaction)
    {
        $balance = Transaction::selectRaw("SUM(CASE WHEN type = 'credit' THEN amount ELSE - amount END) as balance_amount")
                          ->where('store_id', $transaction->store_id)
                          ->where('status', Transaction::STATUS_COMPLETED)
                          ->where('transaction_date', '<=', $transaction->transaction_date)->first();
        $transaction->balance = $balance->balance_amount;
        $transaction->save();
        // $operation = ($transaction->type == 'credit') ? '+': '-';
        // DB::table('transactions')
        //    ->where('transac', '>', $transaction->id)
        //    ->where('store_id', $transaction->store_id)
        //    ->where('status', Transaction::STATUS_COMPLETED)
        //    ->update(['balance' => DB::raw("balance $operation ".$transaction->amount)]);
    }

    private function validateChequeTransaction($request)
    {
        $chequeDate = Carbon::createFromFormat("d M, Y", $request->cheque_date);
        $previous7Day = Carbon::today()->subDays(8);
        if ($chequeDate < $previous7Day) {
            throw new ServiceException("Cheques older than 7 days can not be entered.");
        }
        $checkChequeCount = Transaction::where('store_id', $request->store_id)->whereRaw('payment_details like "%'. $request->cheque_number . '%'. $request->cheque_issuer.'%"')->count();
        if (($checkChequeCount % 2) == 1) {
            throw new ServiceException("Same Cheque details already exist for another transaction.");
        }
    }

    public function storeTransaction(Request $request, Authenticatable $user)
    {
        return DB::transaction(function () use ($request, $user) {
            $transaction = new Transaction();
            $discountTransaction = null;

            $transaction = $this->processTransactionData($request, $transaction);

            if ($request->filled('agent_id')) {
                $agent = Agent::active()->find($request->agent_id);
                if (!empty($agent)) {
                    $transaction = $this->assignAgent($transaction, $agent);
                }
            }
            else {
              $transaction->createdBy()->associate($user);
            }
            $transaction->save();
            $this->modifyBalance($transaction);

            if ($request->type == Transaction::TYPE_CREDIT) {
                $settledTransactions = $this->settleTransactions($transaction, $request);
                $transaction->settledDebitTransactions()->sync($settledTransactions['amount']);
                if (array_key_exists('cash_discount', $settledTransactions)) {
                    $transaction->amount = array_sum(array_column($settledTransactions['amount'], 'amount'));
                    $transaction->save();
                    $discountTransaction = $transaction->replicate();
                    $discountTransaction->transaction_id = $this->getTransactionId();
                    $discountTransaction->payment_method = Transaction::PAYMENT_METHOD_CASH_DISCOUNT;
                    $discountTransaction->amount = array_sum(array_column($settledTransactions['cash_discount'], 'amount'));
                    $discountTransaction->payment_details = null;
                    $date = (new DateTime())->format('Y-m-d H:i:s');
                    $discountTransaction->transaction_date = $date;
                    $discountTransaction->push();
                    $discountTransaction->settledDebitTransactions()->sync($settledTransactions['cash_discount']);
                    $this->modifyBalance($discountTransaction);
                }
            }

            if ($request->hasFile('image')) {
                $this->saveImage($transaction,$request);
                // $file = $request->image->storeAs("media/transactions/$transaction->id", uniqid() . "." . $request->image->guessExtension());
                // $transaction->image = $file;
                // $transaction->save();
            }
            if ($transaction->payment_method == Transaction::PAYMENT_METHOD_CHEQUE) {
                $transactionId = $transaction->id;
                $chequeNumber = $request->cheque_number;
                $bankId = $request->cheque_issuer;
                $chequeIssueDate = Carbon::parse($request->cheque_date)->toDateString();
                (new ChequeMetaService())->create($transactionId, $chequeIssueDate, $chequeNumber, $bankId, $image = null);
           }

            return $transaction;
        });
    }

    private function saveImage($transaction, Request $request)
    {
        $image = $request->file('image');
        $targetDirectory = public_path('media/transactions/' . $transaction->id);
        $file = new File($image);
        $file->setName(uniqid(). "." . $image->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $transaction->image = $file->getName();
        $transaction->save();
    }

    private function processTransactionData($request, Transaction $transaction)
    {
        $transaction->transaction_id = $this->getTransactionId();
        $transaction->store_id = $request->store_id;
        $transaction->description = $request->description;
        $transaction->payment_method = $request->payment_method;
        $transaction->type = $request->type;
        if($transaction->type == 'credit')
            $transaction->payment_tag = $request->payment_tag ?? null;
        $transaction->amount = $request->amount;
        $transaction->status = Transaction::STATUS_COMPLETED;
        $date = (new DateTime())->format('Y-m-d H:i:s');
        $transaction->transaction_date = $date;
        if ($this->checkAuthority(self::AUTHORITY_AGENT)) {
          $transaction->status = Transaction::STATUS_PENDING;
        }
        if (in_array($request->payment_method, [Transaction::PAYMENT_METHOD_UPI, Transaction::PAYMENT_METHOD_NET_BANKING])) {
            $paymentDetails['reference_id'] = $request->reference_id;
        }
        if (in_array($request->payment_method, [Transaction::PAYMENT_METHOD_CHEQUE, Transaction::PAYMENT_METHOD_CHEQUE_BOUNCE])) {
            $paymentDetails['cheque_date'] = $request->cheque_date;
            if (in_array($request->payment_method, [Transaction::PAYMENT_METHOD_CHEQUE])) {
              $this->validateChequeTransaction($request);
              $transaction->status = $this->getTransactionStatusForCheque($request->cheque_date);
            }
            $paymentDetails['cheque_number'] = $request->cheque_number;
            $paymentDetails['cheque_issuer'] = $request->cheque_issuer;
        }
        if (!empty($paymentDetails))
            $transaction->payment_details = json_encode($paymentDetails);

        if (!empty($request->payment_details)) {
            $transaction->payment_details = $request->payment_details;
        }
        return $transaction;
    }

    private function settleTransactions(Transaction $transaction, $request)
    {
        $storeId = $transaction->store_id;
        $settledAmountTransaction = [];
        $settledDiscountTransaction = [];
        if ($request->has('auto_settle') && $request->auto_settle == 1) {
            $transactions = Transaction::toBeSettled()->where('store_id',$storeId)
                                  ->with('settledAmount')->get();
            $amount = $transaction->amount;
            foreach ($transactions as $transaction) {
                $settledAmount = !empty($transaction->settledAmount) ? $transaction->settledAmount->amount : 0;
                $unSettledAmount = $transaction->amount - $settledAmount;
                if ($unSettledAmount < $amount) {
                    $settledAmountTransaction[$transaction->id]['amount'] = $unSettledAmount;
                    $amount -= $unSettledAmount;
                    //TODO: Invoice status logic
                    if (!empty($transaction->source)) {
                      $invoice = $transaction->source;
                      $invoice->status='settled';
                      $invoice->save();
                    }
                } else {
                    $settledAmountTransaction[$transaction->id]['amount'] = $amount;
                    //TODO: Invoice status logic
                    if (!empty($transaction->source)) {
                      $invoice = $transaction->source;
                      $invoice->status='partially_settled';
                      $invoice->save();
                    }
                    break;
                }
            }
        } else {
            $totalAmountToSettle = 0;
            $transactionAmountToSettle = [];
            foreach ($request->settle_transactions as $transactionID => $transactionToSettle) {
              $transactionAmountToSettle[$transactionID] = 0;
            }
            foreach ($request->settle_transactions as $transactionID => $transactionToSettle) {
                if (array_key_exists('amount', $transactionToSettle) && ($transactionToSettle['amount'] > 0)) {
                  $transactionAmountToSettle[$transactionID] += $transactionToSettle['amount'];
                  $this->checkTransaction($transactionID, $transactionAmountToSettle[$transactionID], $transaction);
                  $settledAmountTransaction[$transactionID]['amount'] = $transactionToSettle['amount'];
                  $totalAmountToSettle += $transactionToSettle['amount'];
                }
                if (array_key_exists('cash_discount', $transactionToSettle) && ($transactionToSettle['cash_discount'] > 0)) {
                  $transactionAmountToSettle[$transactionID] += $transactionToSettle['cash_discount'];
                  $this->checkTransaction($transactionID, $transactionAmountToSettle[$transactionID], $transaction);
                  $settledDiscountTransaction[$transactionID]['amount'] = $transactionToSettle['cash_discount'];
                  $totalAmountToSettle += $transactionToSettle['cash_discount'];
                }
            }
            if ($totalAmountToSettle != $transaction->amount) {
                throw new ServiceException("Amount settled does not match transaction amount");
            }
        }
        $settledTransaction['amount'] = $settledAmountTransaction;
        if (!empty($settledDiscountTransaction)) {
          $settledTransaction['cash_discount'] = $settledDiscountTransaction;
        }
        return $settledTransaction;
    }

    private function checkTransaction($id, $transactionToSettleAmount, $transaction)
    {
        // TODO - Check for transactions table character encoding
        $debitTransaction = Transaction::fromQuery("select * from transactions where id = '".$id."'")->first();
        //$debitTransaction = DB::table('transactions')->where('id', $id)->get()->first();
        //Transaction::find($id);
        $debitTransaction->load('settledAmount');
        // if (!$invoice->store_id != $transaction->store_id) {
        //     throw new ServiceException("$invoice->reference_id invoice does not belong to the Store.");
        // }
        // if (!$invoice->canBeSettled()) {
        //     throw new ServiceException("$invoice->reference_id invoice is $invoice->status.");
        // }
        $settledAmount = !empty($debitTransaction->settledAmount) ? $debitTransaction->settledAmount->amount : 0;
        if (strpos($transaction->description, 'Credit Note Generated') !== false) {
            // $settledAmount = ($settledAmount>0) ?  ($settledAmount - $transaction->amount) : 0;
            $settledAmount = 0;
        }
        $unSettledAmount = $debitTransaction->amount - $settledAmount;
        if ($unSettledAmount < $transactionToSettleAmount) {
            throw new ServiceException("$debitTransaction->invoice->reference_id  settle amount is incorrect.");
        }
        //TODO : Invoice status logic
        if (!empty($transaction->source)) {
          $invoice = $transaction->source;
          if ($unSettledAmount == $transactionToSettleAmount) {
              $invoice->status='settled';
          } else {
              $invoice->status='partially_settled';
          }
          $invoice->save();
        }
    }

    private function revertTransactions($transaction)
    {
        $debitTransactions = $transaction->settledDebitTransactions;
        foreach ($debitTransactions as $debitTransaction) {
            if(!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice') {
              $invoice = $debitTransaction->source;
              $invoice->status = 'partially_settled';
              $invoice->save();
            }
        }
        $debitIDs = $debitTransactions->pluck('id');
        $transaction->settledDebitTransactions()->detach($debitIDs);
    }

    public function createForRateCreditNote(RateCreditNote $creditNote) {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $transaction = new Transaction();
        $transaction->transaction_id = 'NT-' . time() . rand(10, 99);
        $transaction->store_id = $creditNote->store_id;
        $transaction->description = "Rate Credit Note Generated $creditNote->reference_id.";
        $transaction->type = Transaction::TYPE_CREDIT;
        $transaction->amount = $creditNote->total;
        $transaction->status = Transaction::STATUS_COMPLETED;
        $date = $creditNote->generated_on;
        $transaction->transaction_date = $date;
        $creditNote->transaction()->save($transaction);

        $request = new Request();
        $request->merge(['auto_settle' => 1]);
        $settledTransactions = $this->settleTransactions($transaction, $request);
        $transaction->settledDebitTransactions()->sync($settledTransactions['amount']);
        $this->modifyBalance($transaction);
        return $transaction;
    }

}
