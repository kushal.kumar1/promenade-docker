<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\ChequeMeta;

class ChequeMetaService extends Service
{
    public function create($transactionId, $chequeIssueDate, $chequeNumber, $bankId, $image = null)
    {
        $data = [
            'transaction_id' => $transactionId,
            'cheque_issue_date' => $chequeIssueDate,
            'cheque_number' => $chequeNumber,
            'bank_id' => $bankId,
            'image' => $image
        ];
        $chequeMeta = $this->processData((new ChequeMeta()), $data);
        $chequeMeta->save();
    }

    public function update($id,$transactionId, $chequeIssueDate, $chequeNumber, $bankId, $image = null)
    {
        $data = [
            'id' => $id,
            'transaction_id' => $transactionId,
            'cheque_issue_date' => $chequeIssueDate,
            'cheque_number' => $chequeNumber,
            'bank_id' => $bankId,
            'image' => $image
        ];

        $chequeMetaCollection = ChequeMeta::find($id);
        if (empty($chequeMetaCollection)) {
            throw new ServiceException('Cheque not found.');
        }
        $chequeMeta = $this->processData($chequeMetaCollection, $data);
        $chequeMeta->update();
    }

    public function delete(Request $request)
    {
        $chequeMeta = ChequeMeta::find($request->id);
        if (empty($chequeMeta)) {
            throw new ServiceException('Cheque not found.');
        }
        $chequeMeta->delete();
        return true;
    }

    private function processData(ChequeMeta $chequeMeta, array $data)
    {
        $chequeMeta->transaction_id = $data['transaction_id'];
        $chequeMeta->cheque_issue_date = $data['cheque_issue_date'];
        $chequeMeta->cheque_number = $data['cheque_number'];
        $chequeMeta->bank_id = $data['bank_id'];
        $chequeMeta->image = $data['image'];
        return $chequeMeta;
    }
}