<?php


namespace Niyotail\Services;


use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\CreditNoteHelper;
use Niyotail\Helpers\TaxHelper;
use Niyotail\Models\Pincode;
use Niyotail\Models\Product;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceDebitNote;
use Niyotail\Models\PurchaseInvoiceDebitNoteItem;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\PurchaseInvoiceShortItem;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Models\Warehouse;

class DebitNoteService
{
    protected $purchaseInvoiceGrnService;

    public function __construct()
    {
        $this->purchaseInvoiceGrnService = new PurchaseInvoiceGrnService();
    }

    public function generateDebitNote(PurchaseInvoice $invoice, $generatedDate)
    {
        $items = $this->purchaseInvoiceGrnService->getInvoiceItemsForGrn($invoice);
        $pendingItems = [];
        foreach ($items as $item) {
            if ($item['quantity']['remaining'] > 0) $pendingItems[] = $item;
        }

        $shortItems = $invoice->shortItems;

        if (!count($pendingItems) && !$shortItems->count()) {
            return true;
            //throw new \Exception('No pending items for debit note');
        }

        return DB::transaction(function () use ($invoice, $pendingItems, $shortItems, $generatedDate) {
            $financialYear = CreditNoteHelper::getFinancialYear($generatedDate);
            $counter = PurchaseInvoiceDebitNote::where('financial_year', $financialYear)->count() + 1;
            $debitNote = new PurchaseInvoiceDebitNote();
            $debitNote->purchase_invoice_id = $invoice->id;
            $debitNote->warehouse_id = $invoice->warehouse_id;
            $debitNote->type = 'short_inventory';
            $debitNote->financial_year = $financialYear;
            $debitNote->generated_at = $generatedDate;
            $debitNote->ref_id = "DN/" . $debitNote->financial_year . "/" . sprintf('%06d', $counter);
            $debitNote->createdBy()->associate(Auth::guard('admin')->user());
            $debitNote->save();

            foreach ($pendingItems as $pendingItem) {
                $invoiceItemId = $pendingItem['invoice_item_id'];
                $pendingQuantity = $pendingItem['quantity']['remaining'];
                $debitNoteItem = new PurchaseInvoiceDebitNoteItem();
                $debitNoteItem->debit_note_id = $debitNote->id;
                $debitNoteItem->source_type = 'purchase_invoice_item';
                $debitNoteItem->source_id = $invoiceItemId;
                $debitNoteItem->quantity = $pendingQuantity;
                $debitNoteItem->save();
            }

            foreach ($shortItems as $shortItem) {
                $debitNoteItem = new PurchaseInvoiceDebitNoteItem();
                $debitNoteItem->debit_note_id = $debitNote->id;
                $debitNoteItem->source_type = 'purchase_invoice_short_item';
                $debitNoteItem->source_id = $shortItem->id;
                $debitNoteItem->quantity = $shortItem->quantity;
                $debitNoteItem->save();
            }

            return $debitNote;
        });
    }

    public function generateReturnDebitNote(PurchaseInvoice $invoice, $returnItems)
    {
        return DB::transaction(function () use ($invoice, $returnItems) {
            $debitNote = new PurchaseInvoiceDebitNote();
            $debitNote->purchase_invoice_id = $invoice->id;
            $debitNote->warehouse_id = $invoice->warehouse_id;
            $debitNote->type = 'return_inventory';
            $debitNote->createdBy()->associate(Auth::guard('admin')->user());
            $debitNote->save();

            foreach ($returnItems as $itemId => $qty) {
                $debitNoteItem = new PurchaseInvoiceDebitNoteItem();
                $debitNoteItem->debit_note_id = $debitNote->id;
                $debitNoteItem->item_id = $itemId;
                $debitNoteItem->quantity = $qty;
                $debitNoteItem->save();
            }

            return $debitNote;
        });
    }

    public function generatePdf(PurchaseInvoiceDebitNote $note)
    {
        $invoice = $note->invoice;

        $vendorAddressId = $invoice->vendor_address_id;
        $warehouseId = $invoice->warehouse_id;
        $warehouse = Warehouse::findOrFail($warehouseId);
        $warehouseCity = $warehouse->city;

        $vendorAddress = VendorsAddresses::findOrFail($vendorAddressId);
        $vendorStateId = Pincode::where('pincode', $vendorAddress->pincode)->firstOrFail()->city->state_id;
        $warehouseStateId = $warehouseCity->state_id;
        $supplyCountryId = $warehouseCity->state->country_id;

        $items = $note->items;

        $finalItems = [];
        $uniqueTaxes = [];

        foreach ($items as $item) {
            $sourceItem = $item->source;
            if ($sourceItem instanceof PurchaseInvoiceShortItem) {
                $taxes = TaxHelper::getTaxes(
                    $sourceItem->tax_class,
                    $sourceItem->mrp,
                    $warehouseStateId,
                    $vendorStateId,
                    $supplyCountryId);

                $totalTaxPerc = $taxes->sum('percentage');
                $additionalPerUnit = $taxes->sum('additional_charge_per_unit');

                $afterAdditionalCharge = $sourceItem->invoice_cost - ($additionalPerUnit * $sourceItem->quantity);
                $taxable = $afterAdditionalCharge / (1 + $totalTaxPerc / 100);

                $thisItem = [
                    'name' => $sourceItem->name,
                    'hsn' => $sourceItem->hsn,
                    'mrp' => $sourceItem->mrp,
                    'quantity' => $item->quantity,
                    'taxable' => $taxable,
                    'taxes' => [],
                    'total_tax' => $sourceItem->invoice_cost - $taxable,
                    'invoiced_cost' => $sourceItem->invoice_cost,
                    'post_tax_discount' => $sourceItem->post_tax_discount,
                    'effective_cost' => $sourceItem->effective_cost,
                ];

                foreach ($taxes as $tax) {
                    $uniqueTaxes[] = $tax->name;
                    $thisItem['taxes'][$tax->name] = [
                        'percentage' => $tax->percentage,
                        'value' => $tax->percentage * $taxable / 100,
                        'addition_charges' => $tax->additional_charges_per_unit * $thisItem['quantity']
                    ];
                }

                $finalItems[] = $thisItem;
            } else if ($sourceItem instanceof PurchaseInvoiceItem) {
                /* @var Product $product */
                $product = $sourceItem->product;
                $unitVariant = $product->unitVariant;
                $taxes = TaxHelper::getTaxes(
                    $product->tax_class_id,
                    $unitVariant->mrp,
                    $warehouseStateId,
                    $vendorStateId,
                    $supplyCountryId);

                $taxable = $sourceItem->total_taxable * $item->quantity;

                $thisItem = [
                    'name' => $product->name,
                    'hsn' => $product->hsn_sac_code,
                    'mrp' => $unitVariant->mrp,
                    'quantity' => $item->quantity,
                    'taxable' => $taxable,
                    'taxes' => [],
                    'total_tax' => $sourceItem->total_invoiced_cost * $item->quantity - $taxable,
                    'invoiced_cost' => $sourceItem->total_invoiced_cost * $item->quantity,
                    'post_tax_discount' => $sourceItem->post_tax_discount * $item->quantity,
                    'effective_cost' => $sourceItem->total_effective_cost * $item->quantity,
                ];

                foreach ($taxes as $tax) {
                    $uniqueTaxes[] = $tax->name;
                    $thisItem['taxes'][$tax->name] = [
                        'percentage' => $tax->percentage,
                        'value' => $tax->percentage * $taxable / 100,
                        'addition_charges' => $tax->additional_charges_per_unit * $thisItem['quantity']
                    ];
                }

                $finalItems[] = $thisItem;
            }
        }

        if (empty($finalItems)) {
            throw new \Exception('No items assigned to debit note');
        }

        $note->load('createdBy');
        $invoice->load('createdBy', 'purchaseOrder.vendor', 'purchaseOrder.createdBy');

        $headerMargin = "90mm";

        $header = view()->make('pdf.debit_note.header', compact('invoice', 'note'))->render();

        return SnappyPdf::loadView('pdf.debit_note.invoice_items',
            [
                'note' => $note,
                'items' => $finalItems,
                'tax_types' => array_unique($uniqueTaxes)
            ]
        )
            ->setOption('margin-top', $headerMargin)
            ->setOption('margin-bottom', '65mm')
            ->setOption('header-html', $header)
            ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }
}