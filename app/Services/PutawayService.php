<?php

namespace Niyotail\Services;

use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Product;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;

class PutawayService extends Service
{
    /**
     * @param int $warehouseId
     * @param Employee $employee
     * @return Putaway
     */
    public function create(int $warehouseId, Employee $employee): Putaway
    {
        $putaway = new Putaway();
        $putaway->warehouse_id = $warehouseId;
        $putaway->status = Putaway::STATUS_OPEN;
        $putaway->employee_id = $employee->id;
        $putaway->save();
        return $putaway;
    }

    /**
     * @param Putaway $putaway
     * @return Putaway
     * @throws ServiceException
     */
    public function close(Putaway $putaway): Putaway
    {
        if (!$putaway->canClose()) throw new ServiceException("Putaway cannot be closed! ");
        $putaway->status = Putaway::STATUS_CLOSED;
        $putaway->save();
        return $putaway;
    }

    /**
     * @param Putaway $putaway
     * @return Putaway
     * @throws ServiceException
     */
    public function start(Putaway $putaway): Putaway
    {
        if ($putaway->status != Putaway::STATUS_OPEN) throw new ServiceException("Putaway cannot be started! ");
        $putaway->status = Putaway::STATUS_STARTED;
        $putaway->save();
        return $putaway;
    }

    /**
     * @param Putaway $putaway
     * @param int $productId
     * @param int $quantity
     * @return PutawayItem
     * TODO:: check for available qty to be put via open putaways for the warehouse
     */
    public function putItem(Putaway $putaway, int $productId, int $quantity): PutawayItem
    {
        return DB::transaction(function () use ($putaway, $productId, $quantity) {
            if ($putaway->status != Putaway::STATUS_OPEN) {
                throw new ServiceException("Cannot add the item! Putaway has been $putaway->status!");
            }

            $whereCondition = ['status' => FlatInventory::STATUS_PUTAWAY, 'product_id' => $productId, 'warehouse_id' => $putaway->warehouse_id];
            $count = 0;
            $count = FlatInventory::where($whereCondition)->limit($quantity)->count();
            if ($count < $quantity) {
                throw new ServiceException("Putaway quantity not available! Try again!");
            }
            $putawayItem = new PutawayItem();
            $putawayItem->product_id = $productId;
            $putawayItem->quantity = $quantity;
            $putaway->items()->save($putawayItem);
            return $putawayItem;
        });
    }


    /**
     * @param PutawayItem $item
     * @param $storageId
     * @return mixed
     * @throws ServiceException
     */
    public function assignStorageToItem(PutawayItem $item, $storageId)
    {
        if ($item->putaway->status != Putaway::STATUS_STARTED) {
            throw new ServiceException("Cannot assign storage, when putaway is {$item->putaway->status}!");
        }
        return DB::transaction(function () use ($item,$storageId){
            $item->storage_id = $storageId;
            $item->save();
            (new FlatInventoryService())->updatePutawayItem($item);
            return $item;
        });
    }

    public function removeItem(PutawayItem $putawayItem): ?bool
    {
        if ($putawayItem->putaway->status != Putaway::STATUS_OPEN) {
            throw new ServiceException("Cannot delete item! Putaway has been {$putawayItem->putaway->status}!");
        }
        return $putawayItem->delete();
    }
}
