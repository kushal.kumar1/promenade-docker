<?php

namespace Niyotail\Services;

use Illuminate\Database\Eloquent\Model;
use Niyotail\Helpers\File;
use Illuminate\Http\Request;
use Niyotail\Services\Service;
use Niyotail\Jobs\DataImportJob;
use Niyotail\Models\ImporterAction;
use Niyotail\Importers\Excel\ProductImporter;
use Illuminate\Support\Facades\Auth;

class ImporterActionService extends Service
{
    public function create(Request $request)
    {
        $requestFile = $request->file('file');

        $file = new File($requestFile);
        $fileName = $request->type."-".time().".".$requestFile->getClientOriginalExtension();
        $file->setName($fileName);
        $file->setDirectory(public_path("importer"));
        $file->save();

        $importerAction = new ImporterAction();
        $importerAction->type = $request->type;
        $importerAction->input = $fileName;
        $importerAction->status = ImporterAction::STATUS_PENDING;
        $importerAction->employee_id = Auth::guard('admin')->user()->id;
        $importerAction->save();
        DataImportJob::dispatch($importerAction);

        return $importerAction;
    }

    public function setCompleted(ImporterAction $action, $output,Model $resource=null)
    {
        if (!empty($output)) {
            $action->output = $output;
        }
        $action->status = ImporterAction::STATUS_COMPLETED;
        if(!empty($resource)){
            $action->resource()->associate($resource);
        }
        $action->save();
    }
}
