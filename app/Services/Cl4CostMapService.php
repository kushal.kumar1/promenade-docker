<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Category\Cl4CostMap;

class Cl4CostMapService extends Service
{
    /**
     * @throws ServiceException
     */
    public function create(Request $request): Cl4CostMap
    {
        $alreadyExists = Cl4CostMap::where('category_l4_id', $request->cl4_id)
            ->where('marketer_level_id', $request->marketer_level_id)->first();
        if (!empty($alreadyExists)) {
            throw new ServiceException('Cost Map already exists');
        }
        $cl4CostMap = new Cl4CostMap();
        $cl4CostMap->category_l4_id = $request->cl4_id;
        $cl4CostMap->marketer_level_id = $request->marketer_level_id;
        $cl4CostMap->commission_tag_id = $request->commission_tag_id;
        $cl4CostMap->price_basis = $request->price_basis;
        $cl4CostMap->latest_cost_basis = $request->latest_cost_basis;
        $cl4CostMap->mrp_basis = $request->mrp_basis;
        $cl4CostMap->std_vcm = $request->std_vcm;
        $cl4CostMap->cost_sp = $request->cost_sp;
        $cl4CostMap->cost_mrp = $request->cost_mrp;
        $cl4CostMap->mrp_slab = $request->mrp_slab;
        $cl4CostMap->store_a = $request->store_a;
        $cl4CostMap->store_b = $request->store_b;
        $cl4CostMap->store_c = $request->store_c;
        $cl4CostMap->min_oq = $request->min_oq;
        $cl4CostMap->max_oq = $request->max_oq;
        $cl4CostMap->save();
        return $cl4CostMap;
    }
}