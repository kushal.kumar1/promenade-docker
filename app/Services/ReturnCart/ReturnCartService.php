<?php

namespace Niyotail\Services\ReturnCart;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\Utils;
use Niyotail\Services\Service;

use Niyotail\Models\ReturnCart;
use Niyotail\Models\ReturnCartItem;
use Niyotail\Models\Store;
use Niyotail\Models\User;
use Niyotail\Models\ProductVariant;

class ReturnCartService extends Service
{
    use ReturnCartItemService;

    private $cart = null;

    public function setCart(ReturnCart $cart = null): ReturnCartService
    {
        $this->cart = $cart;
        return $this;
    }

    public function addToCart($request, $createdBy): ReturnCart
    {
        $cart = DB::transaction(function () use ($request, $createdBy) {
            if (empty($this->cart)) {
                $this->createCart($createdBy, $request->store_id);
            }
            $this->addItem($request, $this->cart);
            return $this->cart->refresh();
        });
        return $cart;
    }

    public function updateQuantity(Request $request): ReturnCart
    {
        $cart = DB::transaction(function () use ($request) {
            $cartItem = ReturnCartItem::find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }
            $this->updateItemQuantity($request, $cartItem);
            return ReturnCart::with('items')->whereHas('items', function ($query) use ($request) {
                $query->where('id', $request->id);
            })->first();
        });
        return $cart;
    }

    public function removeCartItem(Request $request)
    {
        $cart = $this->removeItem($request);
        $cartItemCount = ReturnCartItem::where('return_cart_id', $cart->id)->count();
        if ($cartItemCount > 0) {
            return $cart;
        } else {
            $this->deleteCart($cart);
        }
    }

    public function deleteCart($cart)
    {
        $cart->delete();
    }

    private function createCart($createdBy, $storeId)
    {
        $store = Store::find($storeId);
        $cart = new ReturnCart();
        $cart->store_id = $store->id;
        $cart->warehouse_id = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        $cart->createdBy()->associate($createdBy);
        $cart->save();
        $this->setCart($cart);
    }
}
