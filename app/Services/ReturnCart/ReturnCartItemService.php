<?php

namespace Niyotail\Services\ReturnCart;

use Niyotail\Events\Cart\ItemQuantityUpdatedInCart;
use Niyotail\Exceptions\ServiceException;

use Niyotail\Models\ReturnCart;
use Niyotail\Models\ReturnCartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Shipment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait ReturnCartItemService
{
    protected function addItem(Request $request, ReturnCart $cart)
    {
        $productVariant = $this->getProductVariant($request->sku);
        if (empty($productVariant)) {
            throw new ServiceException('Invalid product');
        }

        $returnCartItem = ReturnCartItem::where(['return_cart_id' => $cart->id, 'sku' => $productVariant->sku, 'shipment_id' => $request->shipment_id])->first();
        $product =  $productVariant->product;
        $allVariants = $product->allVariants;

        $unitsToReturn = 0;
        foreach ($allVariants as $variant) {
            $unitsToReturn += $request->{$variant->value}*$variant->quantity;
        }

        $shipment = Shipment::where('id', $request->shipment_id)->with(['orderItems' => function($q) use($request) {
            $q->canReturn()->where('sku', $request->sku);
        }])->first();
        $shipmentOrderItems = $shipment->orderItems;
        $maxQuantity = $shipmentOrderItems->sum('quantity');
        $quantity = ceil($unitsToReturn/$productVariant->quantity);

        if($quantity > $maxQuantity)
        {
            throw new ServiceException('Return quantity ('.$quantity.') exceeds returnable quantity ('.$maxQuantity.')');
        }

        if (empty($returnCartItem)) {
            $cartItem = new ReturnCartItem();

            $cartItem->return_cart_id = $cart->id;
            $cartItem->shipment_id = $request->shipment_id;
            $cartItem->sku = $productVariant->sku;
            $cartItem->quantity = $quantity;
            $cartItem->units = $unitsToReturn;
            $cartItem->reason = $request->reason;

            $cartItem->save();
            return $cartItem;
        }

        //call update function in case of existing sku
        $quantity += $returnCartItem->quantity;
        $updateRequest = new Request();
        $updateRequest->merge(['id' => $returnCartItem->id]);
        $updateRequest->merge(['quantity' => $quantity]);
        $updateRequest->merge(['units' => $unitsToReturn]);
        $updateRequest->merge(['reason' => $request->reason]);
        $this->updateItemQuantity($updateRequest, $returnCartItem);
    }

    protected function updateItemQuantity(Request $request, ReturnCartItem $cartItem)
    {
        $shipment = Shipment::where('id', $cartItem->shipment_id)->with(['orderItems' => function($q) use($cartItem) {
                      $q->canReturn()->where('sku', $cartItem->sku);
                    }])->first();
        $shipmentSKUQuantity = $shipment->orderItems->sum('quantity');

        $productVariant = ProductVariant::sku($cartItem->sku)->firstOrFail();
        $request->quantity = ceil($request->units/$productVariant->quantity);

        if ($request->quantity > $shipmentSKUQuantity) {
            $maxQty = $shipmentSKUQuantity*$productVariant->quantity;
          throw new ServiceException("Invalid item quantity $request->units unit(s).  Only maximum $maxQty units ". str_plural('item', $shipmentSKUQuantity)." eligible for return");
        }
        $quantity = intval($request->quantity);
        $cartItem->quantity = $quantity;
        $cartItem->units = $request->units;
        $cartItem->reason = $request->reason;
        $cartItem->save();
    }

    public function removeItem(Request $request)
    {
        $cart = DB::transaction(function () use ($request) {
            $cartItem = ReturnCartItem::with('returnCart')->find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }
            $cart = $cartItem->returnCart;
            $cartItem->delete();
            return $cart->refresh();
        });
        return $cart;
    }

    private function getProductVariant($sku)
    {
        return ProductVariant::with('product')->where('sku', $sku)->first();
    }
}
