<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Payments\PaymentException;
use Niyotail\Models\Order;
use Niyotail\Models\Payment;
use Niyotail\Helpers\Payments\Payment as PaymentHelper;
use Niyotail\Models\ReturnOrder;
use Niyotail\Services\Order\OrderService;

class PaymentService extends Service
{

    public function  createOrderPayment(Order $order, $method,$transactionId, $amount, $type,$status,$returnOrderId=null,$remarks=null):Payment
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $payment = new Payment();
        $payment->transaction_id = empty($transactionId)?time() . $order->id . rand(10, 99):$transactionId;
        $payment->method = $method;
        $payment->amount = $amount;
        $payment->status = $status;
        $payment->type = $type;
        $payment->remarks = $remarks;
        $payment->return_order_id = $returnOrderId;
        $order->payments()->save($payment);
        $orderService=new OrderService();
        $orderService->createLog($order,"Payment Initiated ! (Method: $method, Amount: $amount, Transaction id: $payment->transaction_id )");
        return $payment;
    }

    public function updateStatus(Payment $payment,$status)
    {
        $this->checkAuthorisation(self::AUTHORITY_SYSTEM);
        $this->statusUpdateCheck( $payment,$status);
        $payment->status = $status;
        $payment->save();
        $payment->loadMissing('order');
        $orderService=new OrderService();
        $orderService->createLog($payment->order,"Payment $status ! (Method: $payment->method, Transaction id: $payment->transaction_id )");
    }

    public function refundAfterCancellation(Order $order)
    {
        $payments=Payment::credit()->where('order_id',$order->id)->get();
        foreach ($payments as $payment){
            try {
                (new PaymentHelper())->driver($payment->method)->refund($order,$payment->amount,'Order Cancelled');
            } catch (PaymentException $exception) {
                throw new ServiceException($exception->getMessage());
            }
        }
    }

    public function refundAfterPaymentFailed(Order $order)
    {
        $payments=Payment::credit()->where('order_id',$order->id)->get();
        foreach ($payments as $payment){
            try {
                (new PaymentHelper())->driver($payment->method)->refund($order,$payment->amount,'Order Payment Failed');
            } catch (PaymentException $exception) {
                throw new ServiceException($exception->getMessage());
            }
        }
    }

    public function refundAfterReturn(ReturnOrder $returnOrder, $paymentMethod,$amount)
    {
        try {
            (new PaymentHelper())->driver($paymentMethod)->refund($returnOrder,$amount,'Order Returned');
        } catch (PaymentException $exception) {
            throw new ServiceException($exception->getMessage());
        }
    }

    private function statusUpdateCheck(Payment $payment,$status)
    {
        if (in_array($status,[Payment::STATUS_PAID,Payment::STATUS_FAILED]) && $payment->status != Payment::STATUS_PENDING) {
            throw new ServiceException("Invalid Payment Status");
        }
    }
}