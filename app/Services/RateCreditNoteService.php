<?php


namespace Niyotail\Services;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Niyotail\Helpers\CreditNoteHelper;
use Niyotail\Helpers\TaxHelper;
use Niyotail\Models\Pincode;
use Niyotail\Models\PosOrder;
use Niyotail\Models\PosOrderItem;
use Niyotail\Models\RateCreditNote;
use Niyotail\Models\RateCreditNoteOrder;
use Niyotail\Models\Store;
use Niyotail\Models\Tax;

class RateCreditNoteService
{
    /**
     * @param $storeId
     * @param $date
     * @param $endDate
     */
    public function createRateCreditNotesByStoreAndDate($storeId, $date, $endDate, $registerNumber = null) {
        $pdfInfo = $this->getOrderItemsForCreditNotes($storeId, $date, $endDate)->sortBy('invoice_dt');
        if(!$pdfInfo->count()) return;

        $retailItems = $pdfInfo->where('order_type', 'retail');

        /* Adding sleep to handle duplicate transaction ref id */
        $this->create($storeId, $pdfInfo, RateCreditNote::TYPE_PROMOTION, $date, $endDate, $registerNumber);
        sleep(1);

        $this->create($storeId, $pdfInfo, RateCreditNote::TYPE_DISCOUNT, $date, $endDate, $registerNumber);
        sleep(1);

        /* No commission to be provided for wholesale items */
        $this->create($storeId, $retailItems, RateCreditNote::TYPE_COMMISSION, $date, $endDate, $registerNumber);
    }

    /**
     * @param $storeId
     * @param $date
     * @return \Illuminate\Support\Collection
     */
    private function getOrderItemsForCreditNotes($storeId, $date, $endDate) {
        $financialYear = CreditNoteHelper::getFinancialYear($date);

        $query = PosOrder::with('items.product', 'items.taxes', 'items.itemMeta', 'items.shipments')
            ->storeId($storeId)
            ->doesntHave('creditNote');

        if(!empty($date) && !empty($endDate)) {
            $query = $query->whereRaw("date(convert_tz(invoiced_at, '+00:00', '+05:30')) >= '".$date->copy()->format('Y-m-d')."' and 
            date(convert_tz(invoiced_at, '+00:00', '+05:30')) <= '".$endDate->copy()->format('Y-m-d')."'");
        }
        else {
            $query = $query->whereDate('invoiced_at', '<=', $date);
        }

        $orders = $query->get();

        if(!$orders->count()) return collect();

        $store = Store::find($storeId);
        $pincode = Pincode::where('pincode', $store->pincode)->first();
        $stateId = $pincode->city->state_id;

        $pdfInfo = collect();
        foreach ($orders as $order) {
            foreach ($order->items as $item) {
                if($item->status == 'cancelled') continue;

                $product = $item->product;
                $taxClass = $product->tax_class_id;

                $unitVariant = $product->unitVariant;
                $mrp = $unitVariant->mrp;

                $taxElements = Tax::where('tax_class_id', $taxClass)
                    ->where('source_state_id', 13)
                    ->where(function ($query) use ($stateId) {
                        if($stateId == 13) {
                            $query->where('supply_state_id', 13);
                        } else {
                            $query->whereNull('supply_state_id');
                        }
                    })->get();

                $itemTaxes = $item->taxes;

                $shipments = $item->shipments;

                $ntShipments = $shipments->where('shipment_id', '!=', null);

                foreach ($ntShipments as $shipment) {
                    $ntInvoice = $shipment->niyotailInvoice;
                    $invoiceRefId = $ntInvoice ? $ntInvoice->reference_id : 'N/A';
                    $shipmentQty = $shipment->quantity;
                    $itemQuantity = $item->quantity;

                    if($itemQuantity == 0) continue;

                    $qtyRatio = $itemQuantity  == 0 ? 0 : abs($shipment->quantity/$itemQuantity);
                    $unitSp = $shipment->selling_price ? $shipment->selling_price : $mrp;

                    $cgstVal = 0;
                    $sgstVal = 0;
                    $igstVal = 0;
                    $cessVal = 0;
                    $splCessVal = 0;

                    $discountTotal = $item->itemMeta ? round($item->itemMeta->discount*$qtyRatio, 2) : 0;
                    $promoTotal = $item->itemMeta ? round($item->itemMeta->promotion*$qtyRatio, 2) : 0;
                    $commissionTotal = $item->itemMeta ? round($item->itemMeta->commission*$qtyRatio, 2) : 0;

                    $billingValue = round($unitSp*$itemQuantity*$qtyRatio, 4);
                    $sellingValue = round($item->total*$qtyRatio, 4);

                    if($item->total == 0) {
                        $discountPerc = $item->itemMeta->discount > 0 ? 1 : 0;
                        $promotionPerc = $item->itemMeta->promotion > 0 ? 1 : 0;
                        $commissionPerc = 0;

                        $totalValue = $discountPerc ? $item->itemMeta->discount : $item->itemMeta->promotion;
                        $value = $totalValue*$qtyRatio;

                        $totalPercent = $itemTaxes->sum('percentage');
                        $totalTaxVal = round($value - ($value/(1 + ($totalPercent/100))), 6);

                        foreach ($taxElements as $tax) {
                            if(!empty($tax->percentage))
                            {
                                $val = round(($tax->percentage/$totalPercent)*$totalTaxVal, 6);
                            }
                            else if (!empty($tax->additional_charges_per_unit)) {
                                $val = round($tax->additional_charges_per_unit*$itemQuantity, 6);
                            }

                            switch ($tax->name){
                                case "CGST":
                                    $cgstVal = $val;
                                    break;
                                case "SGST":
                                    $sgstVal = $val;
                                    break;
                                case "IGST":
                                    $igstVal = $val;
                                    break;
                                case "CESS":
                                    $cessVal = $val;
                                    break;
                                case "SPL CESS":
                                    $splCessVal = $val;
                                    break;
                            }
                        }
                    }
                    else {

                        $discountPerc = !empty($discountTotal) ? round($discountTotal/($item->total*$qtyRatio), 6) : 0;
                        $promotionPerc = !empty($promoTotal) ? round($promoTotal/($item->total*$qtyRatio), 6) : 0;
                        $commissionPerc = !empty($commissionTotal) ? round($commissionTotal/($item->total*$qtyRatio), 6) : 0;

                        $cgstTax = $itemTaxes->where('name', 'CGST')->first();
                        $cgstVal = $cgstTax ? $cgstTax->value*$qtyRatio : 0;

                        $sgstTax = $itemTaxes->where('name', 'SGST')->first();
                        $sgstVal = $sgstTax ? $sgstTax->value*$qtyRatio : 0;

                        $igstTax = $itemTaxes->where('name', 'IGST')->first();
                        $igstVal = $igstTax ? $igstTax->value*$qtyRatio : 0;

                        $cessTax = $itemTaxes->where('name', 'CESS')->first();
                        $cessVal = $cessTax ? $cessTax->value*$qtyRatio : 0;

                        $sCessTax = $itemTaxes->where('name', 'SPL CESS')->first();
                        $splCessVal = $sCessTax ? $sCessTax->value*$qtyRatio : 0;
                    }

                    $row['order_id'] = $order->id;
                    $row['order_type'] = $order->type;
                    $row['invoice_ref_id'] = $invoiceRefId;
                    $row['product_id'] = $item->product_id;
                    $row['mrp'] = $mrp;
                    $row['name'] = $item->product->name;
                    $row['quantity'] = $shipmentQty;
                    $row['total'] = round($sellingValue, 2);
                    $row['nt_selling_price'] = round($billingValue, 2);
                    $row['discount_total'] = $discountTotal;
                    $row['discount'] = [
                        'total' => $discountTotal,
                        'cgst' => $cgstVal && $discountPerc ? round($discountPerc*$cgstVal, 6) : 0,
                        'sgst' => $sgstVal && $discountPerc ? round($discountPerc*$sgstVal, 6) : 0,
                        'igst' => $igstVal && $discountPerc ? round($discountPerc*$igstVal, 6) : 0,
                        'cess' => $cessVal && $discountPerc ? round($discountPerc*$cessVal, 6) : 0,
                        'spl_cess' => $splCessVal && $discountPerc ? round($discountPerc*$splCessVal, 6) : 0,
                    ];
                    $row['promotion_total'] = $promoTotal;
                    $row['promotion'] = [
                        'total' => $promoTotal,
                        'cgst' => $cgstVal && $promotionPerc ? round($promotionPerc*$cgstVal, 6) : 0,
                        'sgst' => $sgstVal && $promotionPerc ? round($promotionPerc*$sgstVal, 6) : 0,
                        'igst' => $igstVal && $promotionPerc ? round($promotionPerc*$igstVal, 6) : 0,
                        'cess' => $cessVal && $promotionPerc ? round($promotionPerc*$cessVal, 6) : 0,
                        'spl_cess' => $splCessVal && $promotionPerc ? round($promotionPerc*$splCessVal, 6) : 0,
                    ];
                    $row['commission_total'] = $commissionTotal;
                    $row['commission'] = [
                        'total' => $commissionTotal,
                        'cgst' => $cgstVal && $commissionPerc ? round($commissionPerc*$cgstVal, 6) : 0,
                        'sgst' => $sgstVal && $commissionPerc ? round($commissionPerc*$sgstVal, 6) : 0,
                        'igst' => $igstVal && $commissionPerc ? round($commissionPerc*$igstVal, 6) : 0,
                        'cess' => $cessVal && $commissionPerc ? round($commissionPerc*$cessVal, 6) : 0,
                        'spl_cess' => $splCessVal && $commissionPerc ? round($commissionPerc*$splCessVal, 6) : 0,
                    ];

                    $pdfInfo = $pdfInfo->push(collect($row));
                }
            }
        }

        return $pdfInfo;
    }

    /**
     * @param $storeId
     * @param $rows
     * @param $type
     * @param $date
     * @return mixed
     */
    private function create($storeId, $rows, $type, $date, $endDate, $registerNumber = null) {
        $store = Store::find($storeId);
        if($rows->sum($type."_total") == 0) {
            return null;
        }
        return DB::transaction(function() use ($store, $rows, $type, $date, $endDate, $registerNumber) {
            $uniqueOrders = $rows->unique('order_id')->pluck('order_id');
            $formattedRows = $rows->where($type."_total")->map(function($row) use ($type) {
                $values = $row[$type];
                return array_merge([
                    'invoice_ref_id' => $row['invoice_ref_id'],
                    'product_id' => $row['product_id'],
                    'quantity' => $row['quantity'],
                    'total_billed' => $row['nt_selling_price'],
                    'total_sp' => $row['total'],
                    'name' => $row['name'],
                    'subtotal' => $values['total'] - ($values['cgst'] + $values['sgst'] + $values['igst'] + $values['cess'] + $values['spl_cess'])
                ], $values);
            });

            $creditNote = $this->createCreditNote($store, $type, $rows, $date, $endDate, $registerNumber);
            $pdf = self::getPdf($store, $creditNote, $formattedRows, $type)->output();
            $name = 'dcn_'.$creditNote->id.".pdf";
            Storage::disk('s3')->put('discount_credit_note/'.$name, $pdf, $name);

            /* Do not create transaction entries for FY 20-21 credit notes */
            if($creditNote->financial_year != '20-21') {
                $this->createTransaction($creditNote);
            }

            $creditNoteOrders = [];
            foreach ($uniqueOrders as $uniqueOrder) {
                $creditNoteOrders[] = [
                    'pos_order_id' => $uniqueOrder,
                    'rate_credit_note_id' => $creditNote->id,
                ];
            }

            RateCreditNoteOrder::insert($creditNoteOrders);

            return $creditNote;
        });
    }

    /**
     * @param $store
     * @param $type
     * @param $rows
     * @param $date
     * @return RateCreditNote
     */
    private function createCreditNote($store, $type, $rows, $date, $endDate, $registerNumber = null) {
        $creditNote = new RateCreditNote();
        $financialYear = CreditNoteHelper::getFinancialYear($date);

        $number = RateCreditNote::max('number') + 1;

        if($registerNumber && $endDate) {
            $count = RateCreditNote::where('reference_id', 'LIKE', "%RCN/$financialYear/$registerNumber/%")->count() + 1;
            $referenceId =  "RCN/$financialYear/$registerNumber/".sprintf("%06d", $count);
            $generatedOn = Carbon::parse($endDate)->format('Y-m-d H:i:s');
            // "RCN/".$financialYear."/".sprintf("%06d', $count);
        } else {
            $count = RateCreditNote::count() + 1;
            $referenceId = "RCN/".$financialYear."/".sprintf('%06d', $count);
            $generatedOn = Carbon::now()->format('Y-m-d H:i:s');
        }

        $totalValues = $this->getTotalValuesByType($rows, $type);

        $creditNote->reference_id = $referenceId;
        $creditNote->store_id = $store->id;
        $creditNote->type = $type;
        $creditNote->financial_year = $financialYear;
        $creditNote->number = $number;
        // $count;
        $creditNote->tax = $totalValues['tax'];
        $creditNote->total = $totalValues['total'];
        $creditNote->subtotal = $totalValues['sub_total'];
        $creditNote->status = RateCreditNote::STATUS_GENERATED;
        $creditNote->start_date = $date;
        $creditNote->end_date = $endDate;
        $creditNote->generated_on = $generatedOn;
        //Carbon::now()->format('Y-m-d H:i:s');
        $creditNote->save();
        return $creditNote;
    }

    /**
     * @param $rows
     * @param $key
     * @return array
     */
    private function getTotalValuesByType($rows, $key) {
        $total = 0;
        $totalTax = 0;

        foreach ($rows as $row) {
            $values = $row[$key];
            $total += $values['total'];
            $totalTax += ($values['cgst'] + $values['sgst'] + $values['igst'] + $values['cess'] + $values['spl_cess']);
        }

        $subTotal = $total - $totalTax;

        return [
            'total' => $total,
            'sub_total' => $subTotal,
            'tax' => $totalTax
        ];
    }

    /**
     * @param Store $store
     * @param RateCreditNote $creditNote
     * @param $items
     * @param $type
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function getPdf(Store $store, RateCreditNote $creditNote, $items, $type)
    {
        $header = view()->make('pdf.rate_credit_note.header', compact('store', 'creditNote', 'type'))->render();
        $footer = view()->make('pdf.rate_credit_note.footer', compact('creditNote'))->render();

        return SnappyPdf::loadView('pdf.rate_credit_note.sales', [
            'creditNote' => $creditNote,
            'store' => $store,
            'items' => $items,
            'type' => $type
        ])
            ->setOption('margin-top', '70mm')
            ->setOption('margin-bottom', '60mm')
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }

    /**
     * @param RateCreditNote $creditNote
     * @return \Niyotail\Models\Transaction
     */
    public function createTransaction(RateCreditNote $creditNote) {
        $transactionService = (new TransactionService())->setAuthority(TransactionService::AUTHORITY_SYSTEM);
        return $transactionService->createForRateCreditNote($creditNote);
    }

    private function createCsvForCreditNoteRows($rows) {
        $dateTime = Carbon::now('Asia/Kolkata')->format('Y-m-d-H-i-s');
        $csv = storage_path('app')."/rate_cn_csv/".$dateTime.".csv";
        if (file_exists($csv)) {
            unlink($csv);
        }
        $file = fopen($csv, "w+");
        fputcsv($file, array('order_id', 'invoice_number', 'invoice_date', 'product_id', 'mrp', 'nt_selling_price', 'name', 'quantity',
             'nt_qty', 'total', 'discount_total', 'promotion_total', 'commission_total'));
        foreach ($rows as $row) {
            try {
                fputcsv($file, array($row['order_id'],
                        $row['invoice_no'],
                        $row['invoice_dt'],
                        $row['product_id'],
                        $row['mrp'],
                        $row['nt_selling_price'],
                        $row['name'],
                        $row['quantity'],
                        $row['nt_qty'],
                        $row['total'],
                        $row['discount_total'],
                        $row['promotion_total'],
                        $row['commission_total'])
                );
            } catch (\Exception $e) {
                $e->getMessage();
            }
        }
        fclose($file);
    }

    private function getFormattedNoShipmentItem(PosOrder $order, PosOrderItem $item, $itemTaxes, $taxElements, $mrp) {
        $qtyRatio = 1;
        $itemQuantity = $item->quantity;

        $cgstVal = 0;
        $sgstVal = 0;
        $igstVal = 0;
        $cessVal = 0;
        $splCessVal = 0;

        if($item->total == 0) {
            $discountPerc = $item->itemMeta->discount > 0 ? 1 : 0;
            $promotionPerc = $item->itemMeta->promotion > 0 ? 1 : 0;
            $commissionPerc = 0;

            $totalValue = $discountPerc ? $item->itemMeta->discount : $item->itemMeta->promotion;
            $value = $totalValue*$qtyRatio;

            $totalPercent = $itemTaxes->sum('percentage');
            $totalTaxVal = round($value - ($value/(1 + ($totalPercent/100))), 6);

            foreach ($taxElements as $tax) {
                if(!empty($tax->percentage))
                {
                    $val = round(($tax->percentage/$totalPercent)*$totalTaxVal, 6);
                }
                else if (!empty($tax->additional_charges_per_unit)) {
                    $val = round($tax->additional_charges_per_unit*$itemQuantity, 6);
                }

                switch ($tax->name){
                    case "CGST":
                        $cgstVal = $val;
                        break;
                    case "SGST":
                        $sgstVal = $val;
                        break;
                    case "IGST":
                        $igstVal = $val;
                        break;
                    case "CESS":
                        $cessVal = $val;
                        break;
                    case "SPL CESS":
                        $splCessVal = $val;
                        break;
                }
            }
        }
        else {
            $discountPerc = !empty($item->itemMeta->discount) ? $item->itemMeta->discount/$item->total : 0;
            $promotionPerc = !empty($item->itemMeta->promotion) ? $item->itemMeta->promotion/$item->total : 0;
            $commissionPerc = !empty($item->itemMeta->commission) ? $item->itemMeta->commission/$item->total : 0;

            $cgstTax = $itemTaxes->where('name', 'CGST')->first();
            $cgstVal = $cgstTax ? $cgstTax->value*$qtyRatio : 0;

            $sgstTax = $itemTaxes->where('name', 'SGST')->first();
            $sgstVal = $sgstTax ? $sgstTax->value*$qtyRatio : 0;

            $igstTax = $itemTaxes->where('name', 'IGST')->first();
            $igstVal = $igstTax ? $igstTax->value*$qtyRatio : 0;

            $cessTax = $itemTaxes->where('name', 'CESS')->first();
            $cessVal = $cessTax ? $cessTax->value*$qtyRatio : 0;

            $sCessTax = $itemTaxes->where('name', 'SPL CESS')->first();
            $splCessVal = $sCessTax ? $sCessTax->value*$qtyRatio : 0;
        }

        $discountTotal = round($item->itemMeta->discount*$qtyRatio, 2);
        $promoTotal = round($item->itemMeta->promotion*$qtyRatio, 2);
        $commissionTotal = round($item->itemMeta->commission*$qtyRatio, 2);

        $row['order_id'] = $order->id;
        $row['invoice_ref_id'] = 'N/A';
        $row['product_id'] = $item->product_id;
        $row['mrp'] = $mrp;
        $row['name'] = $item->product->name;
        $row['quantity'] = $itemQuantity;
        $row['total'] = round($item->total*$qtyRatio, 2);
        $row['nt_selling_price'] = $mrp;
        $row['discount_total'] = $discountTotal;
        $row['discount'] = [
            'total' => $discountTotal,
            'cgst' => $cgstVal && $discountPerc ? round($discountPerc*$cgstVal, 2) : 0,
            'sgst' => $sgstVal && $discountPerc ? round($discountPerc*$sgstVal, 2) : 0,
            'igst' => $igstVal && $discountPerc ? round($discountPerc*$igstVal, 2) : 0,
            'cess' => $cessVal && $discountPerc ? round($discountPerc*$cessVal, 2) : 0,
            'spl_cess' => $splCessVal && $discountPerc ? round($discountPerc*$splCessVal, 2) : 0,
        ];
        $row['promotion_total'] = $promoTotal;
        $row['promotion'] = [
            'total' => $promoTotal,
            'cgst' => $cgstVal && $promotionPerc ? round($promotionPerc*$cgstVal, 2) : 0,
            'sgst' => $sgstVal && $promotionPerc ? round($promotionPerc*$sgstVal, 2) : 0,
            'igst' => $igstVal && $promotionPerc ? round($promotionPerc*$igstVal, 2) : 0,
            'cess' => $cessVal && $promotionPerc ? round($promotionPerc*$cessVal, 2) : 0,
            'spl_cess' => $splCessVal && $promotionPerc ? round($promotionPerc*$splCessVal, 2) : 0,
        ];
        $row['commission_total'] = $commissionTotal;
        $row['commission'] = [
            'total' => $commissionTotal,
            'cgst' => $cgstVal && $commissionPerc ? round($commissionPerc*$cgstVal, 2) : 0,
            'sgst' => $sgstVal && $commissionPerc ? round($commissionPerc*$sgstVal, 2) : 0,
            'igst' => $igstVal && $commissionPerc ? round($commissionPerc*$igstVal, 2) : 0,
            'cess' => $cessVal && $commissionPerc ? round($commissionPerc*$cessVal, 2) : 0,
            'spl_cess' => $splCessVal && $commissionPerc ? round($commissionPerc*$splCessVal, 2) : 0,
        ];

        return $row;
    }
}