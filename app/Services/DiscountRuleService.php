<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Niyotail\Models\DiscountRule;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

/**
 * Class CouponService
 *
 * @package Niyotail\Services
 *
 */
class DiscountRuleService extends Service
{
    public function create(Request $request)
    {
        if ($this->isDuplicate($request)) {
            return false;
        }
        $discountRule = new DiscountRule();
        $discountRule = $this->processData($discountRule, $request);
        $discountRule->save();
        return $discountRule;
    }

    public function update(Request $request)
    {
        if ($this->isDuplicate($request, $request->id)) {
            return false;
        }
        $discountRule = DiscountRule::find($request->id);
        if (empty($discountRule)) {
            throw new ServiceException("Coupon doesn't exist !");
        }
        $discountRule = $this->processData($discountRule, $request);
        $discountRule->save();
        return $discountRule;
    }

    private function processData(DiscountRule $discountRule, $request)
    {
        $discountRule->code = $request->code;
        $discountRule->description = $request->description;
        $discountRule->conditions = json_encode($request->conditions);
        $discountRule->actions = json_encode($request->actions);
        $discountRule->total_usage_limit = $request->total_usage_limit;
        $discountRule->per_customer_limit = $request->per_customer_limit;
        $discountRule->start_date = Carbon::parse($request->start_date)->format('Y-m-d H:i:s');
        $discountRule->end_date = !empty($request->end_date) ? Carbon::parse($request->end_date)->format('Y-m-d H:i:s') : null;
        return $discountRule;
    }

    private function isDuplicate($request, $id = null)
    {
        $ruleQuery = DiscountRule::where('code', $request->code);
        if (!empty($id)) {
            $ruleQuery->where('id', '!=', $id);
        }
        $discountRule = $ruleQuery->first();
        if (!empty($discountRule)) {
            throw new ServiceException("Discount Rule with same code already exist !!");
            return true;
        }
        return false;
    }
}
