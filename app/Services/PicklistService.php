<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Niyotail\Events\Picklist\{PicklistClosed, PicklistCreated};
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Helpers\PicklistHelper;
use Niyotail\Models\{Employee, FlatInventory, Order, OrderPicklist, Picklist, PicklistItem};

class PicklistService extends Service
{
    public function create(Request $request, Employee $employee)
    {
        return DB::transaction(function () use ($request, $employee) {
            if (count($request->selected) >= 10) {
                throw new ServiceException('Can not create single picklist of more than 10 orders.');
            }

            $orders = Order::with('items.itemInventory.inventory')->where('status', Order::STATUS_CONFIRMED)
                ->whereIn('id', $request->selected)
                ->lockForUpdate()
                ->get();

            if (count($request->selected) > $orders->count()) {
                throw new ServiceException('Please select orders in confirmed status');
            }

            Order::whereIn('id', $request->selected)->update(['status' => Order::STATUS_MANIFESTING]);

            $orderItems = $orders->pluck('items')->flatten();

            if ($orderItems->isEmpty()) {
                throw new ServiceException("No items found for adding to picklist!");
            }

            $picklist = new Picklist();
            $picklist->status = Picklist::STATUS_PROCESSING;
            $picklist->createdBy()->associate($employee);
            $picklist->warehouse_id = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
            $picklist->save();
            $this->attachItems($picklist, $orderItems);
            $ordersId = $orders->pluck('id')->toArray();
            $this->addOrdersInPicklist($ordersId, $picklist);
            event(new PicklistCreated($picklist, $orderItems));
            return $picklist;
        });
    }

    private function attachItems(Picklist $picklist, $orderItems)
    {
        $picklistItems = array();
        foreach ($orderItems as $orderItem) {
            $picklistItems[] = [
                'picklist_id' => $picklist->id,
                'order_item_id' => $orderItem->id,
                'status' => PicklistItem::STATUS_PENDING,
                'storage_id' => $orderItem->itemInventory->inventory->storage_id
            ];
        }
        $chunkedPicklistItems = array_chunk($picklistItems, 100);
        foreach ($chunkedPicklistItems as $items) {
            PicklistItem::insert($items);
        }
    }

    public function updateAgent(Request $request)
    {
        $picklist = Picklist::find($request->id);
        $picklist->agent_id = $request->agent_id;
        $picklist->save();
        if ($picklist->warehouse_id == 3) {
            $items = PicklistItem::getLocationWiseItems($picklist->id);
        } else {
            $items = PicklistItem::getItemsWithLocations($picklist);
        }
        PicklistHelper::generatePdf($picklist, $items);
        return $picklist;
    }

    /**
     * @param Picklist $picklist
     * @param Employee $employee
     * @return Picklist
     */
    public function assignPicker(Picklist $picklist, Employee $employee): Picklist
    {
        $picklist->picker()->associate($employee);
        $picklist->save();
        return $picklist;
    }

    /**
     * Closes picklist by changing the status to closed.
     * Triggers PicklistClosed event to generate shipments.
     * @param Picklist $picklist
     * @return mixed
     * @throws ServiceException
     */
    public function close(Picklist $picklist)
    {
        if (!$picklist->canClose()) {
            throw new ServiceException("Picklist can't be closed! Some items are still in pending state.");
        }
        return DB::transaction(function () use ($picklist) {
            $picklist->status = Picklist::STATUS_CLOSING;
            $picklist->save();
            event(new PicklistClosed($picklist));
            return $picklist;
        });
    }

    /**
     * Function to process items for picklist during scanning process.
     * Picked qty will moved to picked status and Skipped qty will be move to skipped status with skip reason.
     * @param Picklist $picklist
     * @param int $productId
     * @param int $pickedQuantity
     * @param array $skipData
     * @param null $storageId
     * @throws ServiceException
     */
    public function processItems(Picklist $picklist, int $productId, int $pickedQuantity, array $skipData = [], $storageId = null)
    {
        if ($picklist->status == Picklist::STATUS_CLOSED) throw new ServiceException("Picklist is closed!");
        $pendingItems = PicklistItem::getItemsByStatus($picklist, $productId, PicklistItem::STATUS_PENDING);
        if(!empty($storageId)){
            $pendingItems = $pendingItems->where('storage_id', $storageId);
        }
        // Validate request data
        $skipDataCollection = collect($skipData);
        $totalQuantity = $pickedQuantity + $skipDataCollection->sum('quantity');
        if ($totalQuantity != $pendingItems->count()) throw new ServiceException("Total quantity entered should match pending items quantity.");

        DB::transaction(function () use ($picklist, $pendingItems, $pickedQuantity, $skipData) {
            if ($picklist->status != Picklist::STATUS_PICKING) {
                $picklist->status =  Picklist::STATUS_PICKING;
                $picklist->picking_started_at = Carbon::now()->toDateTimeString();
                $picklist->save();
            }
            // Pick items
            $picklistItemIds = $pendingItems->splice(0, $pickedQuantity)->pluck('id')->toArray();
            $inventoriesToPicked = PicklistItem::whereIn('id', $picklistItemIds)->with('orderItemInventory')->get()
                ->pluck('orderItemInventory.flat_inventory_id')->toArray();
            (new FlatInventoryService())->markGoodInventories($inventoriesToPicked, FlatInventory::STATUS_PICKED);
            foreach (array_chunk($picklistItemIds, 500) as $chunkedItemIds) {
                PicklistItem::whereIn('id', $chunkedItemIds)->update([
                    'status' => PicklistItem::STATUS_PICKED
                ]);
            }

            // Skip items
            if (!empty($skipData)) {
                foreach ($skipData as $data) {
                    $quantityToSkip = $data['quantity'];
                    $picklistItemIds = $pendingItems->splice(0, $quantityToSkip)->pluck('id')->toArray();
                    foreach (array_chunk($picklistItemIds, 500) as $chunkedItemIds) {
                        PicklistItem::whereIn('id', $chunkedItemIds)
                            ->where('status', PicklistItem::STATUS_PENDING)
                            ->update([
                                'status' => PicklistItem::STATUS_SKIPPED,
                                'skip_reason' => $data['reason']
                            ]);
                    }
                }
            }
            $picklist->refresh();
            if($picklist->canClose()){
                $picklist->status = Picklist::STATUS_PICKED;
                $picklist->save();
            }

        });
    }

    /**
     * Function to move all items to pending status and set skip_reason to null.
     * Resets the scanned products to initial state.
     * @param Picklist $picklist
     * @param int $productId
     * @param null $storageId
     * @throws ServiceException
     */
    public function removeItems(Picklist $picklist, int $productId, $storageId = null)
    {
        if ($picklist->status == Picklist::STATUS_CLOSED) throw new ServiceException("Picklist is closed!");
        $picklistItems = PicklistItem::getItemsByProduct($picklist, $productId);
        if ($picklistItems->isEmpty()) throw new ServiceException("No matching picklist item found!");

        $picklistItemIds = $picklistItems->pluck('id')->toArray();
        if(!empty($storageId)){
            $picklistItemIds = $picklistItems->where('storage_id', $storageId)->pluck('id')->toArray();
        }
        $inventoriesToBeUnPicked = PicklistItem::whereIn('id', $picklistItemIds)->with('orderItemInventory')->get()
            ->pluck('orderItemInventory.flat_inventory_id')->toArray();
        (new FlatInventoryService())->markGoodInventories($inventoriesToBeUnPicked, FlatInventory::STATUS_MANIFESTED);
        $chunkedItems = array_chunk($picklistItemIds, 500);
        foreach ($chunkedItems as $itemIds) {
            PicklistItem::whereIn('id', $itemIds)->update([
                'status' => PicklistItem::STATUS_PENDING,
                'skip_reason' => null
            ]);
        }
        if($picklist->status == Picklist::STATUS_PICKED){
            $picklist->status = Picklist::STATUS_PICKING;
            $picklist->save();
        }
    }

    public function updatePicklistStatus(Picklist $picklist, $status)
    {
        if (!in_array($status, Picklist::getConstants('STATUS'))) {
            throw new ServiceException('Invalid picklist status');
        }

        if($status == Picklist::STATUS_PICKED){
            $picklist->picking_ended_at = Carbon::now()->toDateTimeString();
        }
        $picklist->status = $status;
        $picklist->save();
        return $picklist;
    }

    /**
     * @throws ServiceException
     */
    public function updateItemsStatus(array $itemIds, $status)
    {
        if (!in_array($status, PicklistItem::getConstants('STATUS'))) {
            throw new ServiceException('Invalid status provided');
        }
        $chunkedItemIds = array_chunk($itemIds, 500);
        foreach ($chunkedItemIds as $ids) {
            PicklistItem::whereIn('id', $ids)->update(['status' => $status]);
        }
    }

    public function addOrdersInPicklist($ordersId, Picklist $picklist)
    {
        $picklist->orders()->sync($ordersId);
    }
}
