<?php

namespace Niyotail\Services\ShipmentCart;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Utils;
use Niyotail\Services\Service;

use Niyotail\Models\ShipmentCart;
use Niyotail\Models\ShipmentCartItem;
use Niyotail\Models\Order;
use Niyotail\Models\ProductVariant;

class ShipmentCartService extends Service
{
    use ShipmentCartItemService;

    private $cart = null;

    public function setCart(ShipmentCart $cart = null): ShipmentCartService
    {
        $this->cart = $cart;
        return $this;
    }

    public function addToCart($request, $createdBy): ShipmentCart
    {
        $cart = DB::transaction(function () use ($request, $createdBy) {
            if (empty($this->cart)) {
                $this->createCart($createdBy, $request->order_id);
            }
            $this->addItem($request, $this->cart);
            return $this->cart->refresh();
        });
        return $cart;
    }

    public function updateQuantity(Request $request): ShipmentCart
    {
        $cart = DB::transaction(function () use ($request) {
            $cartItem = ShipmentCartItem::find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }
            $this->updateItemQuantity($request, $cartItem);
            return ShipmentCart::with('items')->whereHas('items', function ($query) use ($request) {
                $query->where('id', $request->id);
            })->first();
        });
        return $cart;
    }

    public function removeCartItem(Request $request)
    {
        $cart = $this->removeItem($request);
        $cartItemCount = ShipmentCartItem::where('shipment_cart_id', $cart->id)->count();
        if ($cartItemCount > 0) {
            return $cart;
        } else {
            $this->deleteCart($cart);
        }
    }

    public function deleteCart($cart)
    {
        $cart->delete();
    }

    private function createCart($createdBy, $orderId)
    {
        $order = Order::find($orderId);
        $cart = new ShipmentCart();
        $cart->order_id = $order->id;
        $cart->createdBy()->associate($createdBy);
        $cart->save();
        $this->setCart($cart);
    }
}
