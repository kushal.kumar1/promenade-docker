<?php

namespace Niyotail\Services\ShipmentCart;

use Niyotail\Exceptions\ServiceException;

use Niyotail\Models\ShipmentCart;
use Niyotail\Models\ShipmentCartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Product;
use Niyotail\Models\OrderItem;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait ShipmentCartItemService
{
    protected function addItem(Request $request, ShipmentCart $cart)
    {
        $originalProductVariant = $this->getProductVariant($request->original_sku);
        $productVariant = $this->getProductVariant($request->sku);
        if (empty($productVariant)) {
            throw new ServiceException('Invalid product');
        }

        $this->validateVariant($originalProductVariant, $productVariant, $cart, $request->quantity);
        $shipmentCartItem = ShipmentCartItem::where(['shipment_cart_id' => $cart->id, 'original_sku' => $originalProductVariant->sku, 'sku' => $productVariant->sku])->first();
        $product =  $productVariant->product;

        if (empty($shipmentCartItem)) {
            $cartItem = new shipmentCartItem();

            $cartItem->shipment_cart_id = $cart->id;
            $cartItem->original_sku = $request->original_sku;
            $cartItem->product_id = $product->id;
            $cartItem->sku = $productVariant->sku;
            $cartItem->quantity = $request->quantity;

            $cartItem->save();
            return $cartItem;
        }

        //call update function in case of existing sku
        $quantity = $shipmentCartItem->quantity + $request->quantity;
        $shipmentCartItem->quantity = $quantity;
        $shipmentCartItem->save();
        return $shipmentCartItem;
    }

    protected function updateItemQuantity(Request $request, ShipmentCartItem $cartItem)
    {
        $originalProductVariant = $this->getProductVariant($cartItem->original_sku);
        $productVariant = $this->getProductVariant($cartItem->sku);

        $this->validateVariant($originalProductVariant, $productVariant, $cartItem->shipmentCart, $request->quantity);

        $cartItem->quantity = $request->quantity;
        $cartItem->save();
    }

    public function removeItem(Request $request)
    {
        $cart = DB::transaction(function () use ($request) {
            $cartItem = ShipmentCartItem::with('shipmentCart')->find($request->id);
            if (empty($cartItem)) {
                throw new ServiceException('Invalid cart Item');
            }
            $cart = $cartItem->shipmentCart;
            $cartItem->delete();
            return $cart->refresh();
        });
        return $cart;
    }

    private function getProductVariant($sku)
    {
        return ProductVariant::withTrashed()->with('product')->where('sku', $sku)->first();
    }

    private function validateVariant($originalProductVariant, $currentProductVariant, $cart, $quantity)
    {
        $orderItems = OrderItem::where('order_id', $cart->order_id)->where('sku', $originalProductVariant->sku)->canShip()->get();

        $canShipQuantity = $orderItems->sum('quantity') * $originalProductVariant->quantity;
        $alreadyInCartGroupItems = ShipmentCartItem::selectRaw('original_sku, sum(shipment_cart_items.quantity * product_variants.quantity) as cart_quantity')
                                    ->join('product_variants', 'product_variants.sku', '=', 'shipment_cart_items.sku')
                                    ->where(['shipment_cart_id' => $cart->id, 'original_sku' => $originalProductVariant->sku])
                                    ->groupBy('original_sku')->first();

        $addedInCartQuantity = !empty($alreadyInCartGroupItems) ? $alreadyInCartGroupItems->cart_quantity : 0;

        $toShipQuantity = $addedInCartQuantity + ($quantity * $currentProductVariant->quantity);
        if ($toShipQuantity > $canShipQuantity) {
          throw new ServiceException('Shipment item quantity can not exceed ordered quantity.');
        }

        // $shipmentCartItem = ShipmentCartItem::selectRaw('shipment_cart_items.product_id, sum(shipment_cart_items.quantity * product_variants.quantity) as addedQuantity')
        //                         ->join('product_variants', 'product_variants.sku', '=', 'shipment_cart_items.sku')
        //                         ->where(['shipment_cart_id' => $cart->id, 'shipment_cart_items.product_id' => $currentProductVariant->product_id])
        //                         ->first();
        //
        // $productShipmentCartItemQuantity = !empty($shipmentCartItem->addedQuantity) ? $shipmentCartItem->addedQuantity : 0;
        // $toShipQuantity = $productShipmentCartItemQuantity + ($quantity * $currentProductVariant->quantity);
        // $productInventory = Product::with('totalInventory')->find($currentProductVariant->product_id);
        //
        // if (($originalProductVariant->product_id != $currentProductVariant->product_id) && ($toShipQuantity > $productInventory->total_inventory)) {
        //   throw new ServiceException('Product quantity for generating shipment is not in stock.');
        // }
    }
}
