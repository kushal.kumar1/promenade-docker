<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Niyotail\Models\Product;
use Niyotail\Models\ProductImage;
use Niyotail\Helpers\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;

class ProductImageService extends Service
{
    private function getLastPosition($productId)
    {
        $image =  ProductImage::where('product_id', $productId)->orderBy('id', 'desc')->first();
        return $image ? $image->position: null;
    }

    public function create(Request $request)
    {
        $product = Product::find($request->id);
        $position = $this->getLastPosition($request->id);
        $images = $request->file('images');
        $targetDirectory = public_path("media/" . $request->id);
        foreach ($images as $key => $image) {
            $file = new File($image);
            $file->setName(uniqid(). "." . $image->guessExtension());
            $file->setDirectory($targetDirectory);
            $file->save();

            $position = $position ? $position+1 : $key+1;
            $productImage = new ProductImage();
            $productImage->product_id = $request->id;
            $productImage->name = $file->getName();
            $productImage->position = $position;
            $productImage->save();

            $path = 'media/'.$request->id.'/'.$file->getName();
            if(Storage::disk('public')->exists($path)) {
                $uploadedImage = Storage::disk('public')->get($path);
                Storage::disk('s3')->put($path, $uploadedImage);
            }
        }
    }

    public function update($request)
    {
        DB::transaction(function () use ($request) {
            foreach ($request->images as $key => $imageId) {
                $productImage = ProductImage::find($imageId);
                if (empty($productImage)) {
                    throw new ServiceException("Image does not exist");
                }
                $productImage->position = $key + 1;
                $productImage->save();
            }
        });
    }

    public function delete(Request $request)
    {
        $productImage = ProductImage::find($request->id);
        if (empty($productImage)) {
          throw new ServiceException("Image does not exist");
        }
        $productImage->delete();
    }
}
