<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Models\Bank;

class BankService extends Service
{
    public function create(Request $request)
    {
        return Bank::create($request->all());
    }

    public function update(Request $request)
    {
        $bank = Bank::find($request->id);
        $bank->update($request->all());
        return $bank;
    }
}