<?php

namespace Niyotail\Services;

use Niyotail\Models\Attribute;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

/**
 * Class AttributeService
 *
 * @package Niyotail\Services
 *
 */
class AttributeService extends Service
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Niyotail\Models\Attribute
     *
     */
    public function create(Request $request)
    {
        $attribute = new Attribute();
        $attribute =$this->processData($attribute, $request);
        $attribute->save();
        return $attribute;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool|\Niyotail\Models\Attribute
     *
     */
    public function update(Request $request)
    {
        $attribute = Attribute::find($request->id);
        if (empty($attribute)) {
            throw new ServiceException("Attribute doesn't exist !");
        }
        $attribute =$this->processData($attribute, $request, self::TYPE_UPDATE);
        $attribute->save();
        return $attribute;
    }


    /**
     * @param \Niyotail\Models\Attribute $attribute
     * @param                           $request
     * @param string                    $type
     *
     * @return \Niyotail\Models\Attribute
     *
     */
    private function processData(Attribute $attribute, $request, $type=self::TYPE_CREATE)
    {
        $attribute->name = $request->name;
        $attribute->is_filter = $request->has('is_filter')?1:0;
        if ($type==self::TYPE_CREATE) {
            $attribute->slug = $this->createSlug(Attribute::class, $request->name);
        }
        return $attribute;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     *
     */
    public function delete(Request $request)
    {
        $attribute = Attribute::find($request->id);
        if (empty($attribute)) {
            throw new ServiceException("Attribute doesn't exist !");
        }
        $attribute->delete();
        return true;
    }
}
