<?php

namespace Niyotail\Services;

use Niyotail\Models\Lead;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\User;
use Niyotail\Services\Service;

class LeadService extends Service
{
    public function create(Request $request,User $user)
    {
        $lead = $this->processData($request, new Lead(),$user);
        $lead->save();
        return $lead;
    }
    
    private function processData(Request $request, Lead $lead,User $user)
    {
        $productVariant=ProductVariant::where('sku',$request->sku)->first();
        if(empty($productVariant))
            throw new ServiceException('Invalid Sku given');
        $lead->user_id = $user->id;
        $lead->product_id = $productVariant->product_id;
        $lead->sku = $productVariant->sku;
        $lead->variant = "$productVariant->value ( $productVariant->quantity $productVariant->uom ) ";
        $lead->quantity=$request->quantity;
        return $lead;
    }
}
