<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\TaxHelper;
use Niyotail\Helpers\WarehouseHelper;
use Niyotail\Models\Pincode;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoice;
use Illuminate\Http\Request;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\PurchaseInvoiceDebitNote;
use Niyotail\Models\PurchaseInvoiceGrn;
use Niyotail\Models\PurchaseInvoiceGrnItem;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\PurchaseInvoiceItemTax;
use Niyotail\Models\PurchaseInvoiceShortItem;
use Niyotail\Models\StorePurchaseInvoice;
use Niyotail\Models\VendorsAddresses;
use Niyotail\Models\Warehouse;
use function foo\func;

class PurchaseInvoiceService extends Service
{
    public function createOrUpdate(Request $request)
    {
        DB::transaction(function () use ($request) {
            $purchaseInvoice = $this->createPurchaseInvoice($request);
            $this->addItem($request, new PurchaseInvoiceItem(), $purchaseInvoice);
            $this->recalculatePurchaseInvoice($purchaseInvoice);
            // commenting inventory since it is to be done manually now after the invoice is approved

//            $inventoryService = new InventoryService();
//            $inventoryService->createFromPurchaseInvoiceItem($purchaseInvoiceItem);
        });
    }

    public function createPurchaseInvoice(Request $request)
    {

        $purchaseInvoice = new PurchaseInvoice([
            'vendor_ref_id' => $request->vendor_ref_id,
            'vendor_id' => $request->vendor_id,
            'purchase_order_id' => $request->purchase_order_id ? $request->purchase_order_id : null,
            'vendor_address_id' => $request->vendor_address_id,
            'warehouse_id' => $request->warehouse_id,
            'total_invoice_value' => $request->total_invoice_value,
            'invoice_date' => $request->invoice_date,
            'eway_number' => $request->eway_number,
            'tcs' => $request->tcs,
            'platform_id' => $request->platform_id,
            'is_direct_store_purchase'=> $request->is_direct_store_purchase,
            'is_local_purchase' => $request->is_local_purchase,
            'type' => $request->type
        ]);
        $purchaseInvoice->purchase_order_id = $request->purchase_order_id;
        $today = Carbon::today();
        $month = $today->copy()->format('m');
        $year = $today->copy()->format('y');
        $startYear = $month <=3 ? $startYear = $year - 1 : $startYear = $year;
        $endYear = $startYear + 1;
        $purchaseInvoice->save();
        $purchaseInvoice->reference_id = "NT/PI/".$startYear."-".$endYear."/".$purchaseInvoice->id;
        $purchaseInvoice->createdBy()->associate(Auth::guard('admin')->user());
        $purchaseInvoice->invoice_date = Carbon::parse($request->invoice_date)->toDateString();
        $purchaseInvoice->scanned_file = $request->scanned_file;
        $purchaseInvoice->cart_id = $request->id;
        $purchaseInvoice->save();
        return $purchaseInvoice;
    }

    public function createPurchaseInvoiceFromPo(
        $poId, $vendorId, $warehouseId, $vendorAddressId
    )
    {
        $invoice = new PurchaseInvoice();
        $today = Carbon::today();
        $month = $today->copy()->format('m');
        $year = $today->copy()->format('y');
        $startYear = $month <=3 ? $startYear = $year - 1 : $startYear = $year;
        $endYear = $startYear + 1;
        $invoice->vendor_id = $vendorId;
        $invoice->purchase_order_id = $poId;
        $invoice->warehouse_id = $warehouseId;
        $invoice->vendor_address_id = $vendorAddressId;
        $invoice->save();
        $invoice->createdBy()->associate(Auth::guard('admin')->user());
        $invoice->invoice_date = Carbon::today()->toDateString();
        $invoice->reference_id = "NT/PI/".$startYear."-".$endYear."/".$invoice->id;
        $invoice->save();
        return $invoice;
    }

    public function updatePurchaseInvoice(Request $request)
    {
        $invoice = PurchaseInvoice::find($request->id);
        abort_if(empty($invoice), 404);

//        $invoice->reference_id = $request->reference_id;
        $invoice->vendor_address_id = $request->vendor_address_id;
        $invoice->vendor_ref_id = $request->vendor_ref_id;
        $invoice->warehouse_id = $request->warehouse_id;
        $invoice->invoice_date = Carbon::parse($request->invoice_date)->toDateString();
        $invoice->save();

        return $invoice;
    }

    public function addInvoiceItems(PurchaseInvoice $invoice, $items, PurchaseInvoiceCart $cart)
    {
        foreach ($items as $item)
        {
            $itemReq = new Request();
            $itemReq->replace($item);
            $this->addItem($itemReq, new PurchaseInvoiceItem(), $invoice, $cart);
        }

        $this->recalculatePurchaseInvoice($invoice);
    }

    public function addShortItems(PurchaseInvoice $invoice, PurchaseInvoiceCart $cart) {
        $shortItems = $cart->shortItems;
        if(!$shortItems->count()) return;

        foreach ($shortItems as $shortItem) {
            $item = new PurchaseInvoiceShortItem();
            $item->purchase_invoice_id = $invoice->id;
            $item->name = $shortItem->name;
            $item->hsn = $shortItem->hsn;
            $item->quantity = $shortItem->quantity;
            $item->mrp = $shortItem->mrp;
            $item->discount = $shortItem->discount;
            $item->post_tax_discount = $shortItem->post_tax_discount;
            $item->invoice_cost = $shortItem->invoice_cost;
            $item->effective_cost = $shortItem->effective_cost;
            $item->tax_class = $shortItem->tax_class;
            $item->save();
        }
    }

    private function addItem($request, PurchaseInvoiceItem $purchaseInvoiceItem, PurchaseInvoice $purchaseInvoice, PurchaseInvoiceCart $cart)
    {
        $existingItem = $this->checkDuplicate($request,$purchaseInvoice);
        if(!empty($existingItem))
        {
            $purchaseInvoiceItem = $existingItem;
        }

        $expiryDate = empty($request->expiry_date) ? null : Carbon::parse($request->expiry_date)->toDateString();
        $manufacturingDate = empty($request->manufacturing_date) ? null : Carbon::parse($request->manufacturing_date)->toDateString();

        if(!empty($request->is_requested))
        {
            $productVariant = ProductRequest::where('id', $request->product_id)->first();
            $productId = $productVariant->id;
        }
        else
        {
            $productVariant = ProductVariant::withTrashed()->where('sku', $request->sku)->first();
            $productId = $productVariant->product_id;
        }

        $sku = empty($request->is_requested) ? $productVariant->sku : null;
        $purchaseInvoiceItem->purchase_order_item_id = $request->purchase_order_item_id;
        $purchaseInvoiceItem->is_requested = $request->get('is_requested', 0);
        $purchaseInvoiceItem->quantity = $request->quantity;
        $purchaseInvoiceItem->received_quantity = $request->received_quantity;
        $purchaseInvoiceItem->sku = $sku;
        $purchaseInvoiceItem->product_id = $productId;
        $purchaseInvoiceItem->rack_id = $request->rack_id;
        $purchaseInvoiceItem->purchase_invoice_id = $purchaseInvoice->id;
        $purchaseInvoiceItem->expiry_date = $expiryDate;
        $purchaseInvoiceItem->manufacturing_date = $manufacturingDate;
        $purchaseInvoiceItem->vendor_batch_number = $request->vendor_batch_number;

        $reqParams = [
            'vendor_address_id' => $cart->vendor_address_id,
            'warehouse_id' => $cart->warehouse_id
        ];

        $reqParams['sku'] = $request->sku;
        $reqParams['unit_type'] = $request->value_type;
        $reqParams['price_type'] = $request->price_type;
        $reqParams['discount_type'] = $request->discount_type;
        $reqParams['cost'] = $request->cost;
        $reqParams['cash_discount'] = $request->cash_discount;
        $reqParams['discount'] = $request->discount;
        $reqParams['quantity'] = $request->quantity;
        $reqParams['post_tax_discount'] = $request->post_tax_discount;
        $reqParams['post_tax_discount_type'] = $request->post_tax_discount_type;


        $request->replace($reqParams);
        $values = $this->getInvoiceItemPreview($request);

        $purchaseInvoiceItem->base_cost = $values['unit_base_cost'];
        $purchaseInvoiceItem->other_discount = $values['unit_discount'];
        $purchaseInvoiceItem->total_taxable = $values['unit_taxable_cost'];
        $purchaseInvoiceItem->post_tax_discount = $values['unit_post_tax_discount'];
        $purchaseInvoiceItem->taxes = $values['tax_percentage'];


        foreach ($values['tax_breakup'] as $taxName => $taxVal) {
            $purchaseInvoiceItem->{$taxName} = $taxVal;
        }

        $purchaseInvoiceItem->tax = $values['unit_invoice_cost'] - $values['unit_taxable_cost'];
        $purchaseInvoiceItem->total_invoiced_cost = $values['unit_invoice_cost'];
        $purchaseInvoiceItem->total_effective_cost = $values['unit_effective_cost'];
        $purchaseInvoiceItem->save();
        return $purchaseInvoiceItem;
    }

    private function checkDuplicate($request,$purchaseInvoice)
    {
        $sku = empty($request->is_requested) ? $request->sku : null;
        return PurchaseInvoiceItem::where('sku', $sku)
            ->where('purchase_invoice_id',$purchaseInvoice->id)
            ->where('vendor_batch_number',$request->vendor_batch_number)
            ->where('product_id', $request->product_id)
            ->where('is_requested', $request->is_requested)
            ->first();
    }

    private function addTaxes($taxes, PurchaseInvoiceItem $purchaseInvoiceItem)
    {
        PurchaseInvoiceItemTax::where('purchase_invoice_item_id', $purchaseInvoiceItem->id)->delete();

        foreach ($taxes as $tax) {
            $purchaseInvoiceItemTax = new PurchaseInvoiceItemTax();
            $purchaseInvoiceItemTax->purchase_invoice_item_id = $purchaseInvoiceItem->id;
            $purchaseInvoiceItemTax->name = $tax['name'];
            $purchaseInvoiceItemTax->percentage = $tax['percentage'];
            $purchaseInvoiceItemTax->amount = $tax['amount'];
            $purchaseInvoiceItemTax->save();
        }
    }

    private function getTaxes($taxField)
    {
        $taxes = [];
        if (!empty($taxField)) {
            $taxDetails = explode(',', $taxField);
            foreach ($taxDetails as $tax) {
                $tax = explode('_', $tax);
                if (count($tax) == 3) {
                    $taxes[] = [
                        'name' => $tax[0],
                        'percentage' => $tax[1],
                        'amount' => $tax[2],
                    ];
                }
            }
        }
        return $taxes;
    }

    public function updateItemCostAndQuantity(Request $request, PurchaseInvoice $invoice) {
        $itemId = $request->input('item_id'); 
        $itemCost = $request->input('new_cost');
        $itemQty = $request->input('new_qty');
        DB::transaction(function () use($itemId, $itemCost, $itemQty, $invoice) {
            $invoiceId = $invoice->id;
            $invoiceItem = PurchaseInvoiceItem::invoice($invoiceId)
            ->id($itemId)->firstOrFail();

            $ptd = $invoiceItem->post_tax_discount;
            if($ptd > 0) {
                $ptd = $itemCost*$ptd/$invoiceItem->total_invoiced_cost;
                $invoiceItem->post_tax_discount = $ptd;
                $invoiceItem->total_effective_cost = $itemCost - $ptd;
            }

            $invoiceItem->total_invoiced_cost = $itemCost;

            $invoiceItem->quantity = $itemQty;

            $reqParams['sku'] = $invoiceItem->sku;
            $reqParams['unit_type'] = 'per_unit';
            $reqParams['price_type'] = "all_inclusive";
            $reqParams['discount_type'] = "flat";
            $reqParams['cost'] = $invoiceItem->total_invoiced_cost;
            $reqParams['discount'] = $invoiceItem->other_discount;
            $reqParams['quantity'] = $invoiceItem->quantity;
            $reqParams['post_tax_discount'] = $invoiceItem->post_tax_discount;
            $reqParams['post_tax_discount_type'] = 'flat';
            $reqParams['warehouse_id'] = $invoice->warehouse_id;
            $reqParams['vendor_address_id'] = $invoice->vendor_address_id;

            $request = new Request();
            $request->replace($reqParams);
            $values = $this->getInvoiceItemPreview($request);

            $invoiceItem->tax = $values['unit_tax'];

            $invoiceItem->base_cost = $values['unit_base_cost'];
            $invoiceItem->other_discount = $values['unit_discount'];
            $invoiceItem->total_taxable = $values['unit_taxable_cost'];
            $invoiceItem->post_tax_discount = $values['unit_post_tax_discount'];
            $invoiceItem->taxes = $values['tax_percentage'];

            foreach ($values['tax_breakup'] as $taxName => $taxVal) {
                $invoiceItem->{$taxName} = $taxVal;
            }

            $invoiceItem->tax = $values['unit_invoice_cost'] - $values['unit_taxable_cost'];
            $invoiceItem->total_invoiced_cost = $values['unit_invoice_cost'];
            $invoiceItem->total_effective_cost = $values['unit_effective_cost'];

            $invoiceItem->save();
        });
        $this->recalculatePurchaseInvoice($invoice);
    }

    public function recalculatePurchaseInvoice(PurchaseInvoice $purchaseInvoice)
    {
        DB::transaction(function() use($purchaseInvoice) {
            $purchaseInvoice->load('items');
            $items = $purchaseInvoice->items;
            $shortItems = $purchaseInvoice->shortItems;
            $total = 0;
            $taxTotal = 0;

            foreach ($items as $item) {
                $productVariant = ProductVariant::withTrashed()->sku($item->sku)->first();
                $taxTotal += $item->quantity*$item->tax*$productVariant->quantity;
                $total += $item->quantity*$item->total_effective_cost*$productVariant->quantity;
            }

            foreach ($shortItems as $shortItem) {
                $taxInfo = $shortItem->getPrettyRates();
                $total += $taxInfo['effective_cost'];
                $taxTotal += $taxInfo['tax_amount'];
            }

            $tcs = $purchaseInvoice->tcs ? $purchaseInvoice->tcs : 0;
            $total += $tcs;

            $purchaseInvoice->tax = $taxTotal;
            $purchaseInvoice->total = round($total);
            $purchaseInvoice->round = $total - $purchaseInvoice->total + $tcs;
            $purchaseInvoice->save();
        });
    }

    public function markComplete(PurchaseInvoice $invoice)
    {
        $invoice->status = PurchaseInvoice::STATUS_COMPLETED;
        $invoice->save();
        return $invoice;
    }

    public function saveGRNPath(PurchaseInvoice $invoice, $pdf)
    {
        $invoice->grn_path = $pdf;
        $invoice->save();
        return $invoice;
    }

    public function importInvoiceItemsToInventory(PurchaseInvoice $invoice)
    {
        $items = $invoice->items->where('is_requested', 0);
        if($items->count())
        {
            $importAction = [
                "type" => 'inventory',
                "input" => $invoice->id,
                "resource_type" => 'purchase_invoice',
            ];

            $importReq = new Request();
            $importReq->replace($importAction);

            $importerActionService = new ImporterActionService();
            $importAction = $importerActionService->create($importReq);
        }
        else
        {
            throw new \Exception('No items to import');
        }
    }

    public function getInvoiceItemsByInvoiceId($invoiceId)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoiceId), 400);

        return PurchaseInvoiceItem::Invoice($invoiceId)->NotRequested()->with('purchaseInvoice');
    }

    public function approvePurchaseInvoice($invoiceId)
    {
        $invoice = PurchaseInvoice::find($invoiceId);
        abort_if(empty($invoiceId), 400);

        $invoice->status = PurchaseInvoice::STATUS_APPROVED;
        $invoice->save();
        return $invoice;
    }

    public function createNewGrn($path, $invoiceId)
    {
        $grn = new PurchaseInvoiceGrn();
        $grn->purchase_invoice_id = $invoiceId;
        $grn->grn_path = $path;
        $grn->save();
        return $grn;
    }

    public function assignGrnToInvoiceItems($items, $grn)
    {
        foreach ($items as $item) {
            $item->grn_id = $grn->id;
            $item->save();
        }
    }

    public function toCheckAndFixIrregularInventoryAgainInvoiceItem() {
        $irregularItems = [];
        $i = 0;
        $invoiceItems = PurchaseInvoiceItem::all();
        foreach ($invoiceItems as $invoiceItem) {
            $baseCost = $invoiceItem->base_cost;
            $taxAmount = $invoiceItem->tax;
            $totalEffectiveCost = $invoiceItem->total_effective_cost;

            $grnItems = $invoiceItem->grnItems;
            foreach ($grnItems as $grnItem) {
                $inventoryItem = $grnItem->inventoryItem;
                $inventoryCost = $inventoryItem->effective_unit_cost_price;
                if($inventoryCost != $totalEffectiveCost) {
                    $inventoryItem->base_cost = $baseCost;
                    $inventoryItem->scheme_discount = $invoiceItem->scheme_discount;
                    $inventoryItem->total_taxable = $invoiceItem->total_taxable;
                    $inventoryItem->unit_cost_price = $totalEffectiveCost;
                    $inventoryItem->tax = $taxAmount;
                    $inventoryItem->total_invoiced_cost = $invoiceItem->total_invoiced_cost;
                    $inventoryItem->total_effective_cost = $invoiceItem->total_effective_cost;
                    $inventoryItem->effective_unit_cost_price = $invoiceItem->total_effective_cost;
                    $inventoryItem->save();

                    $irregularItems[$i] = [
                        'pid' => $invoiceItem->product_id,
                        'invoice_id' => $invoiceItem->purchase_invoice_id,
                        'inventory_unit_cost' => $inventoryCost,
                        'invoice_unit_cost' => $totalEffectiveCost
                    ];
                    $i ++;
                }
            }
        }

        $html = '<table><thead><tr>
            <th></th>
            <th>PID</th>
            <th>Invoice ID</th>
            <th>Inventory</th>
            <th>Invoice</th>
            </tr></thead><tbody>';

        foreach ($irregularItems as $k=>$irregularItem) {
            $html .= '<tr>
                    <td>'.($k + 1).'</td>
                    <td>'.$irregularItem['pid'].'</td>
                    <td>'.$irregularItem['invoice_id'].'</td>
                    <td>'.$irregularItem['inventory_unit_cost'].'</td>
                    <td>'.$irregularItem['invoice_unit_cost'].'</td>
                </tr>';
        }

        $html .= '</tbody></table>';

        echo $html;
    }

    public function getInvoiceItemPreview(Request $request) {
        $sku = $request->get('sku');
        $variant = ProductVariant::withTrashed()->sku($sku)->firstOrFail();
        $product = $variant->product;

        $vendorAddressId = $request->get('vendor_address_id');
        $warehouseAddressId = WarehouseHelper::getWarehouseForPurchase($request->is_direct_store_purchase);

        $taxes = $this->getItemTax($vendorAddressId, $warehouseAddressId, $product, $variant);

        $invoiceQty = $request->get('quantity');
        $unitsType = $request->get('unit_type');
        $priceType = $request->get('price_type');
        $cost = $request->get('cost');
        $discType = $request->get('discount_type');
        $discount = $request->get('discount');
        $cashDiscount = $request->get('cash_discount');
        $postTaxDiscount = $request->get('post_tax_discount');
        $postTaxDiscountType = $request->get('post_tax_discount_type');
        $totalUnits = $invoiceQty*$variant->quantity;
        $totalTaxPercent = $taxes->sum('percentage');
        $additionalTaxPerUnit = $taxes->sum('additional_charges_per_unit');

        if($unitsType == "total_units") {
            $cost = $cost/$totalUnits;
        }

        $costValues = [];
        $discountAmount = 0;
        $postTaxDiscountAmount = 0;
        if($priceType == "base_price") {

            $preTaxDiscountAmount = 0;
            if(!empty($discount) || !empty($cashDiscount)) {
                if($discType == 'percentage') {
                    $preTaxDiscountAmount = $cost*($discount + $cashDiscount)/100;
                } else {
                    $preTaxDiscountAmount = $cost - ($discount + $cashDiscount)/($unitsType == "total_units" ? $totalUnits : 1);
                }
            }

            $baseCost = $cost;
            $taxableCost = $cost - $preTaxDiscountAmount;
            $tax = $taxableCost*($totalTaxPercent/100);
            $tax += $additionalTaxPerUnit;
            $invoiceCost = $taxableCost + $tax;

            if(!empty($postTaxDiscount)) {
                $postTaxDiscountAmount = $postTaxDiscountType == "percentage" ? $invoiceCost*($postTaxDiscount/100) : $postTaxDiscount/($unitsType == "total_units" ? $totalUnits : 1);
            }

            $effectiveCost = $invoiceCost - $postTaxDiscountAmount;
        }
        else {
            $invoiceCost = $cost;
            if(!empty($postTaxDiscount)) {
                $postTaxDiscountAmount = $postTaxDiscountType == "percentage" ? $cost*($postTaxDiscount/100) : $postTaxDiscount;
            }
            $effectiveCost = $cost - $postTaxDiscountAmount;
            $taxableCost = ($invoiceCost - $additionalTaxPerUnit) / (1 + ($totalTaxPercent/100));
            if($discType == "percent") {
                $baseCost = $taxableCost*(1 - ($discount + $cashDiscount)/100);
            } else {
                $baseCost = $taxableCost + ($discount + $cashDiscount)/($unitsType == "total_units" ? $totalUnits : 1);
            }
            $discountAmount = $baseCost - $taxableCost;
            $tax = $invoiceCost - $taxableCost;
        }

        $taxBreakup = [];
        foreach ($taxes as $taxLine) {
            $name = strtolower(str_replace(' ', '_', $taxLine['name']));
            $percent = $taxLine['percentage'];
            $additionalCharge =$taxLine['additional_charges_per_unit'];
            $value = $taxableCost*$percent/100 + $additionalCharge;
            $taxBreakup[$name] = $value;
        }

//        $taxBreakup['additional_tax_per_unit'] = $additionalTaxPerUnit;

        $totalValues = [
            "base_cost" => $baseCost*$totalUnits,
            "pre_tax_discount" => $discountAmount*$totalUnits,
            "taxable_cost" => $taxableCost*$totalUnits,
            "tax" => $tax*$totalUnits,
            "invoice_cost" => $invoiceCost*$totalUnits,
            "post_tax_discount" => $postTaxDiscountAmount*$totalUnits,
            "effective_cost" => $effectiveCost*$totalUnits
        ];

        return array(
            'tax_percentage' => $totalTaxPercent,
            'unit_base_cost' => $baseCost,
            'unit_discount' => $discountAmount,
            'unit_taxable_cost' => $taxableCost,
            'unit_invoice_cost' => $invoiceCost,
            'unit_tax' => $tax,
            'unit_post_tax_discount' => $postTaxDiscountAmount,
            'unit_effective_cost' => $effectiveCost,

            'total_units' => $totalUnits,

            'total_base_cost' => $totalValues['base_cost'],
            'total_discount' => $totalValues['pre_tax_discount'],
            'total_taxable' => $totalValues['taxable_cost'],
            'total_tax' => $totalValues['tax'],
            'total_invoice' => $totalValues['invoice_cost'],
            'total_post_tax_discount' => $totalValues['post_tax_discount'],
            'total_effective' => $totalValues['effective_cost'],
            "tax_breakup" => $taxBreakup
        );
    }

    public function getItemTax($vendorAddressId, $warehouseAddressId, $product, $variant) {

        $warehouse = Warehouse::findOrFail($warehouseAddressId);
        $warehouseCity = $warehouse->city;

        $vendorAddress = VendorsAddresses::findOrFail($vendorAddressId);
        $vendorStateId = Pincode::where('pincode', $vendorAddress->pincode)->firstOrFail()->city->state_id;
        $warehouseStateId = $warehouseCity->state_id;
        $supplyCountryId = $warehouseCity->state->country_id;
        $taxClassId = $product->tax_class_id;
        $variantMrp = $variant->mrp;

        return TaxHelper::getTaxes($taxClassId, $variantMrp, $warehouseStateId, $vendorStateId, $supplyCountryId);
    }

    public function createReturnDebitNote(Request $request) {
        DB::transaction(function() use($request) {
            $selectedItems = $request->input('selected');
            $returningQty = $request->input('return_qty');

            $itemIds = [];
            $returnItems = [];
            foreach ($selectedItems as $itemId) {
                $itemQty = $returningQty[$itemId];

                if($itemQty > 0) {
                    $returnItems[$itemId] = $itemQty;
                    array_push( $itemIds, $itemId);
                }
            }

            $purchaseInvoiceItems = PurchaseInvoiceItem::whereIn('id', $itemIds)->get();
            $purchaseInvoiceCount = $purchaseInvoiceItems->pluck('purchase_invoice_id')->count();
            if($purchaseInvoiceCount > 1) {
                throw new Exception('Items belong to multiple invoices.');
            }
            $purchaseInvoice = $purchaseInvoiceItems->first()->purchaseInvoice;

            $debtNoteService = new DebitNoteService();
            return $debtNoteService->generateReturnDebitNote($purchaseInvoice, $returnItems);
        });
    }

    public function resetItemImportQty($itemId) {
        return DB::transaction(function() use($itemId) {

            $invItem = PurchaseInvoiceItem::findOrFail($itemId);
            $invItem->received_quantity = 0;
            $invItem->save();

            $grnItems = PurchaseInvoiceGrnItem::where('item_id', $itemId)->get();

            foreach ($grnItems as $grnItem) {
                $grn = $grnItem->grn;
                if($grn->inventory_imported == 1) {
                    throw new Exception('Item cannot be reset once grn is imported');
                }

                $grnItem->delete();
            }

            return $invItem;
        });
    }

    public function deletePurchaseInvoice(PurchaseInvoice $invoice) {
        return DB::transaction(function () use($invoice) {
            $grns = $invoice->all_grn;
            if($grns) {
                $importedGrns = $grns->where('inventory_imported', 1)->count();
                if($importedGrns > 0) {
                    throw new Exception('Invoice cannot be cancelled after importing a GRN');
                }
            }

            PurchaseInvoiceGrn::where('purchase_invoice_id', $invoice->id)->delete();

            $cartId = $invoice->cart_id;

            PurchaseInvoiceDebitNote::where('purchase_invoice_id', $invoice->id)->delete();

            StorePurchaseInvoice::where('purchase_invoice_id', $invoice->id)->delete();

            $invoice->delete();

            $cart = PurchaseInvoiceCart::findOrFail($cartId);

            $cart->purchase_order_id = null;
            $cart->status = 'pending';
            $cart->save();

            return $cart;
        });
    }

    public function getDetailedTaxBreakup($taxes, $taxableAmount, $additionalTaxPerUnit) {
        $taxElements = [
            "additional_charges" => $additionalTaxPerUnit
        ];
        foreach ($taxes as $tax) {
            $taxName = strtolower(str_replace(' ', '_', $tax->name));
            $taxAmount = $taxableAmount*$tax->percentage/100;
            $taxElements[$taxName] = $taxAmount;
        }
        return $taxElements;
    }

    public function updateShortProduct(PurchaseInvoiceShortItem $product, Request $request) {
        $details = $this->parseShortProductFromRequest($request);
        $invoiceCost = $details['effective_cost'] + $details['post_tax_discount'];
        $details['invoice_cost'] = $invoiceCost;

        foreach ($details as $key=>$val) {
            $product->{$key} = $val;
        }

        $product->save();
        return $product;
    }

    private function parseShortProductFromRequest(Request $request) {
        $discount = $request->get('discount') > 0 ? $request->get('discount') : 0;
        $ptd = $request->get('post_tax_discount') > 0 ? $request->get('post_tax_discount') : 0;
        return [
            'name' => $request->get('name'),
            'hsn' => $request->get('hsn'),
            'mrp' => $request->get('mrp'),
            'quantity' => $request->get('quantity'),
            'discount' => $discount,
            'tax_class' => $request->get('tax_class'),
            'post_tax_discount' => $ptd,
            'effective_cost' => $request->get('effective_cost')
        ];
    }
}
