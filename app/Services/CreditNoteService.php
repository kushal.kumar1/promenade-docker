<?php

namespace Niyotail\Services;

use DateTime;
use Niyotail\Services\Service;
use Niyotail\Models\CreditNote;
use Niyotail\Models\ReturnOrder;
use Niyotail\Helpers\CreditNoteHelper;
use Niyotail\Events\CreditNote\CreditNoteGenerated;
use Niyotail\Events\CreditNote\CreditNoteCancelled;
use Illuminate\Support\Facades\DB;

class CreditNoteService extends Service
{
    private $startingNumber = 1;

    public function create(ReturnOrder $returnOrder)
    {
        $returnOrder->load('order.store', 'orderItems.returnOrderItems');

        $refundTotal = 0;

        foreach($returnOrder->orderItems as $orderItem)
        {
            $variant = $orderItem->productVariant;
            $variantQty = $variant->quantity;
            $returnItem = $orderItem->returnOrderItems->where('return_order_id', $returnOrder->id)->first();
            $returnUnits = $returnItem->units;
            $returnRatio = $returnUnits/$variantQty;
            $refund = $orderItem->total*$returnRatio;
            // echo $refund."<br>";
            $refundTotal += $refund;
        }

        $order = $returnOrder->order;
        $creditNote = DB::transaction(function () use ($returnOrder, $order, $refundTotal) {
            $creditNote = new CreditNote();
            $creditNote->order()->associate($order);
            $creditNote->returnOrder()->associate($returnOrder);
            $creditNote->financial_year = CreditNoteHelper::getFinancialYear($returnOrder->created_at);
            $lastEntry = $this->getLastEntry($creditNote->financial_year);
            $creditNote->number  = empty($lastEntry) ?  $this->startingNumber : $lastEntry->number + 1;
            $creditNote->reference_id = "CN/".$creditNote->financial_year."/".sprintf('%06d', $creditNote->number);
            $creditNote->amount = round($refundTotal);
            $creditNote->status = CreditNote::STATUS_GENERATED;
            $creditNote->save();

            CreditNoteHelper::generatePdf($creditNote);
            event(new CreditNoteGenerated($creditNote));
            return $creditNote;
        });
        return $creditNote;
    }

    public function cancel(CreditNote $creditNote)
    {
        $creditNote->status=CreditNote::STATUS_CANCELLED;
        $creditNote->save();
        event(new CreditNoteCancelled($creditNote));
    }

    private function getLastEntry($financialYear)
    {
        return CreditNote::where('financial_year', $financialYear)
              ->orderBy('number', 'desc')->first();
    }
}
