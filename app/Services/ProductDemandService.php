<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\UpdateProductDemandItems;
use Niyotail\Models\Brand;
use Niyotail\Models\Product;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\ProductDemandItem;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorMarketerMap;

class ProductDemandService extends Service
{
    public function saveDemand(Request $request, $id = null)
    {
        return DB::transaction(function () use ($request, $id) {

            /* Specific to importer */
            if($request->has('vendor_id') && $request->filled('vendor_id')){
                if(!$this->checkIfVendorHasProduct($request->vendor_id, $request->group_id, $request->product_id)) {
                    throw new ServiceException('Invalid vendor/brand/marketer for the given product!');
                }
            }

            $source = $request->has('source') ? $request->source : ProductDemand::SOURCE_SYSTEM;
            if (!empty($id)) {
                $demand = ProductDemand::findOrFail($id);
                if (empty($demand->created_by_id)) {
                    $demand->createdBy()->associate(Auth::guard('admin')->user());
                }
                if ($request->has('generate_demand') && $request->input('generate_demand') == 1) {
                    $demand->status = ProductDemand::STATUS_GENERATED;
                }

            } else {
                $demand = new ProductDemand();
                $demand->status = $request->has('status') ? $request->status : ProductDemand::STATUS_INITIATED;
                $createdBy = $request->has('created_by') ? $request->created_by : Auth::guard('admin')->user();
                $demand->createdBy()->associate($createdBy);
                $demand->source = $source;
            }

            $params = $this->parseFromRequest($request);

            $demand->channel = $params['channel'];
            $demand->date_from = $params['date_from'];
            $demand->date_to = $params['date_to'];
            $demand->sale_days = $params['sale_days'];
            $demand->inventory_days = $params['inventory_days'];
            $demand->reorder_days = $params['reorder_days'];
            $demand->vendor_id = $params['vendor_id'];
            $demand->brand_id = $params['brand_id'];
            $demand->marketer_id = $params['marketer_id'];
            $demand->cl1_id = $params['cl1'];
            $demand->cl2_id = $params['cl2'];
            $demand->cl3_id = $params['cl3'];
            $demand->cl4_id = $params['cl4'];
            $demand->warehouse_id = $request->warehouse_id;
            $demand->save();

            if ($source == ProductDemand::SOURCE_SYSTEM) {
                UpdateProductDemandItems::dispatch($demand);
            } else {
                $this->processImportItems($demand, $request);
            }
            return $demand;
        });
    }

    public function parseFromRequest(Request $request)
    {
        return [
            'channel' => $request->input('channel'),
            'date_from' => $request->input('date_from'),
            'date_to' => $request->input('date_to'),
            'sale_days' => $request->input('sale_days'),
            'inventory_days' => $request->input('inventory_days'),
            'reorder_days' => $request->input('reorder_days'),
            'vendor_id' => $request->input('vendor_id'),
            'brand_id' => $request->input('brand_id'),
            'marketer_id' => $request->input('marketer_id'),
            'cl1' => $request->input('cl1_id'),
            'cl2' => $request->input('cl2_id'),
            'cl3' => $request->input('cl3_id'),
            'cl4' => $request->input('cl4_id'),
        ];
    }

    public function processInitiatedDemands($id = null)
    {
        if (!empty($id)) {
            $demand = ProductDemand::findOrFail($id);
            $this->saveProductsByDemand($demand);
        } else {
            ProductDemand::initiated()->chunk(10, function ($demands) {
                foreach ($demands as $demand) {
                    $this->saveProductsByDemand($demand);
                }
            });
        }
    }

    public function saveProductsByDemand(ProductDemand $demand)
    {
        $channel = $demand->channel;
        $dateRange = $this->getDateRangeForSale($demand->sale_days, $demand->date_from, $demand->date_to);

        $demand->date_from = $dateRange['start'];
        $demand->date_to = $dateRange['end'];
        $demand->save();

        $brandId = $demand->brand_id;
        $vendorId = $demand->vendor_id;
        $marketerId = $demand->marketer_id;
        $cl1 = $demand->cl_1;
        $cl2 = $demand->cl_2;
        $cl3 = $demand->cl_3;
        $cl4 = $demand->cl_4;
        $inventoryDays = $demand->inventory_days;
        $reorderDays = $demand->reorder_days;
        $warehouseId = $demand->warehouse_id;

        /* get query for demand */
        $query = $this->getTopSellingItemsQuery($channel, $dateRange, $brandId, $vendorId,
            $marketerId, $cl1, $cl2, $cl3, $cl4, $inventoryDays, $reorderDays, $warehouseId);

        /* prepare demand items */
        $products = DB::select($query);

        $demandItems = array_map(function ($product) use ($demand) {
            $product->product_demand_id = $demand->id;
            return (array)$product;
        }, $products);

        ProductDemandItem::demand($demand->id)->forceDelete();

        /* Save demand items */
        ProductDemandItem::insert($demandItems);

//        $demand->status = ProductDemand::STATUS_GENERATED;
        $demand->save();
    }

    public function getDateRangeForSale($saleDays, $dateFrom, $dateTo)
    {
        if (!empty($saleDays)) {
            $dateTo = Carbon::today('Asia/Kolkata');
            $dateFrom = $dateTo->copy()->subDays($saleDays);
        } else if (!empty($dateFrom) && !empty($dateTo)) {
            $dateFrom = Carbon::parse($dateFrom);
            $dateTo = Carbon::parse($dateTo);
            if ($dateFrom->gt($dateTo)) {
                throw new \Exception('Date from cannot be greater than date from field.');
            }
        } else {
            throw new \Exception('Sale days or date range is required.');
        }

        return [
            'start' => $dateFrom->format('Y-m-d'),
            'end' => $dateTo->format('Y-m-d'),
        ];
    }

    public function getTopSellingItemsQuery($channel, $dateRange, $brandId, $vendorId, $marketerId, $cl1, $cl2,
                                            $cl3, $cl4, $inventoryDays, $reorderDays, $warehouseId)
    {
        $query = null;
        switch ($channel) {
            case "primary":
                $query = $this->getPrimarySalesQuery($dateRange, $brandId, $vendorId, $marketerId, $cl1, $cl2,
                    $cl3, $cl4, $inventoryDays, $reorderDays, $warehouseId);
                break;
            case "secondary":
                $query = $this->getSecondarySalesQuery($dateRange, $brandId, $vendorId, $marketerId, $cl1, $cl2,
                    $cl3, $cl4, $inventoryDays);
                break;
        }

        return $query;
    }

    public function getPrimarySalesQuery($dateRange, $brandId, $vendorId, $marketerId, $cl1, $cl2,
                                         $cl3, $cl4, $inventoryDays, $reorderDays, $warehouseId)
    {

        return "select p.group_id,  
                sum(oi.quantity*pv.quantity) as total_quantity_sold,
                sum(oi.quantity*pv.quantity)/" . $inventoryDays . " as daily_avg_unit_sale, 
                IFNULL(inv.inventory, 0) as available_stock, 
                round(IFNULL(inv.inventory, 0)/(sum(oi.quantity*pv.quantity)/71), 2) as stock_days,
                round((sum(oi.quantity*pv.quantity)/" . $inventoryDays . ")*" . $reorderDays . ") as reorder_quantity
                from invoices i 
                left join shipments s on i.shipment_id = s.id 
                left join shipment_items si on si.shipment_id = s.id 
                left join order_items oi on si.order_item_id = oi.id 
                left join product_variants pv on oi.sku = pv.sku
                left join products p on oi.product_id = p.id
                " . ($brandId ? 'left join brands b on p.brand_id = b.id' : '') . "
                " . ($marketerId ? 'left join marketers m on b.marketer_id = m.id' : '') . "
                " . ($vendorId ? 'left join vendor_marketer_map vmp on vmp.marketer_id = m.id' : '') . "
                " . ($vendorId ? 'left join vendors v on vmp.vendor_id = v.id' : '') . "
                " . ($cl4 ? 'left join newcat_l4 cl4 on p.cl4_id = cl4.id' : '') . "
                " . ($cl3 ? 'left join newcat_l3 cl3 on cl4.newcat_l3_id' : '') . "
                " . ($cl2 ? 'left join newcat_l2 cl2 on cl3.newcat_l2_id = cl2.id' : '') . "
                " . ($cl1 ? 'left join newcat_l1 cl1 on cl2.newcat_l1_id = cl1.id' : '') . "
                left join (
                    select fi.product_id as product_id, count(*) as inventory from flat_inventories fi 
                    where fi.status = 'ready_for_sale' and fi.warehouse_id = ". $warehouseId.") as inv on p.id = inv.product_id
                where date(CONVERT_TZ(i.created_at, '+00:00', '+05:30')) BETWEEN
                '" . $dateRange['start'] . "' and 
                '" . $dateRange['end'] . "'       
                " . ($brandId ? 'and b.id = ' . $brandId : '') . "
                " . ($marketerId ? 'and m.id = ' . $marketerId : '') . "
                " . ($vendorId ? 'and v.id = ' . $vendorId : '') . "
                " . ($cl4 ? 'and cl4.id = ' . $cl4 : '') . "
                " . ($cl3 ? 'and cl3.id = ' . $cl3 : '') . "
                " . ($cl2 ? 'and cl2.id = ' . $cl2 : '') . "
                " . ($cl1 ? 'and cl1.id = ' . $cl1 : '') . "
                and s.status not in ('cancelled', 'rto') and i.status != 'cancelled' and oi.status = 'delivered'
                group by p.group_id
                having stock_days < " . $inventoryDays . "
                order by total_quantity_sold desc
                limit 500";
    }

    public function getSecondarySalesQuery($dateRange, $brandId, $vendorId, $marketerId, $cl1, $cl2,
                                           $cl3, $cl4, $inventoryDays)
    {
        return '';
    }

    public function getDemandItems(ProductDemand $demand)
    {
        return $demand->loadMissing('items.product', 'items.product.brand.marketer.vendorMarketerMaps.vendor');
    }

    public function updateDemandItems(ProductDemand $demand, $items, $finalizeDemand)
    {
        DB::transaction(function () use ($demand, $items, $finalizeDemand) {
            $selectedDemandItemIDs = array_keys($items);
            ProductDemandItem::Demand($demand->id)->whereNotIn('id', $selectedDemandItemIDs)->delete();
            foreach ($items as $demandItemId => $item) {
                ProductDemandItem::Id($demandItemId)->update(['reorder_quantity' => $item['reorder_quantity']]);
            }

            $demand->status = ProductDemand::STATUS_FINALIZED;
            $demand->save();
        });
    }

    public function getFormattedDemandItems(ProductDemand $demand)
    {
        $demandItems = $demand->items;

        foreach ($demandItems as $demandItem) {
            dd($demandItem);
        }
    }

    private function processImportItems(ProductDemand $demand, Request $request)
    {
        $demand->loadMissing('items');
        $demandItem = $demand->items->where('sku', $request->sku)->first();
        if (empty($demandItem)) {
            $demandItem = new ProductDemandItem();
            $demandItem->product_demand_id = $demand->id;
            $demandItem->product_id = $request->product_id;
            $demandItem->sku = $request->sku;
            $demandItem->reorder_quantity = $request->reorder_quantity;
            $demandItem->total_quantity_sold = 0;
            $demandItem->daily_avg_unit_sale = 0;
            $demandItem->available_stock = 0;
            $demandItem->stock_days = 0;
            $demandItem->cost_price_by = $request->cost_price_by;
            $demandItem->product_id = $request->product_id;
            $demandItem->cost_price = $request->cost_price;
            $demandItem->vendor_id = $request->vendor_id;
        } else {
            $demandItem->reorder_quantity += $request->reorder_quantity;
        }
        $demandItem->save();
        return $demandItem;
    }

    public function getFormattedItemsForPreview($items) {
        return $items->map(function($item, $key) {
            $arr = [
                "id" => $item->id,
                "product_demand_id" => $item->product_demand_id,
                "group_id" => $item->group_id,
                "group_name" => $item->group->name,
                "total_quantity_sold" => $item->total_quantity_sold,
                "daily_avg_unit_sale" => $item->daily_avg_unit_sale,
                "available_stock" => $item->available_stock,
                "stock_days" => $item->stock_days,
                "reorder_quantity" => $item->reorder_quantity,
                "vendor_id" => $item->vendor_id
            ];

            if(!empty($arr['vendor_id'])) {
                $arr['vendor_name'] = $item->vendor->name;
            }

            $arr['units'] = $item->reorder_quantity;

            $arr['brand_id'] = $item->group->brand->id;
            $arr['marketer_id'] = $item->group->brand->marketer->id;

            $arr['brand_name'] = $item->group->brand->name;
            $arr['marketer_name'] = $item->group->brand->marketer->name;

            $vendorMaps = $item->group->brand->marketer->vendorMarketerMaps;
            $vendorMaps->loadMissing('vendor');
            if(!$vendorMaps->count()) {
                $arr['has_vendors'] = false;
                $arr['vendors'] = [];
            }
            else {
                $arr['has_vendors'] = true;
                $vendorMaps = $vendorMaps->sortBy('priority');
                $arr['vendors'] = [];
                foreach ($vendorMaps as $map) {
                    $vendor = $map->vendor->toArray();
                    $vendor['priority'] = $map->priority;
                    $arr['vendors'][] = $vendor;
                }
            }

            return $arr;
        });
    }

    public function getFormattedItemsForPO($items) {

        $formattedItems = $items->map(function($item, $key) {
            $arr = [
                "id" => $item->id,
                "product_demand_id" => $item->product_demand_id,
                "group_id" => $item->group_id,
                'product_id' => $item->product_id,
                "group_name" => $item->group->name,
                "total_quantity_sold" => $item->total_quantity_sold,
                "daily_avg_unit_sale" => $item->daily_avg_unit_sale,
                "available_stock" => $item->available_stock,
                "stock_days" => $item->stock_days,
                "reorder_quantity" => $item->reorder_quantity,
                "vendor_id" => $item->vendor_id,
                'cost_price_by' => $item->cost_price_by,
                'cost_price' => $item->cost_price
            ];

            $arr['units'] = $item->reorder_quantity;

            $arr['brand_id'] = $item->group->brand->id;
            $arr['marketer_id'] = $item->group->brand->marketer->id;

            $arr['brand_name'] = $item->group->brand->name;
            $arr['marketer_name'] = $item->group->brand->marketer->name;
            $arr['vendor_name'] = $item->vendor->name;

            return $arr;
        });

        $vendors = [];

        foreach ($formattedItems as $item) {
            $vendorId = $item['vendor_id'];
            $marketerId = $item['marketer_id'];
            $key = $vendorId."_".$marketerId;
            if(!isset($vendors[$key])) {
                $vendors[$key] = [
                    'vendor_id' => $vendorId,
                    'marketer_id' => $marketerId,
                    'items' => []
                ];
            }

            $vendors[$key]['items'][] = $item;
        }

        return collect($vendors)->map(function($vendor, $key) {
            $vendor['mapping'] = VendorMarketerMap::with('vendor', 'marketer')->vendorId($vendor['vendor_id'])->marketerId($vendor['marketer_id'])->firstOrFail();
            return $vendor;
        });
    }

    public function createPurchaseOrderFromDemand(ProductDemand $demand) {
        $poService = new PurchaseOrderService();

        $items = $demand->items->where('vendor_id', '!=', null);
        $items->loadMissing('group', 'group.brand', 'group.brand.marketer',
            'group.brand.marketer.vendorMarketerMaps', 'group.brand.marketer.vendorMarketerMaps.vendor');

        $vendorDemands = $this->getFormattedItemsForPO($items->where('vendor_id', '!=', 'null'));

//        try {
            foreach ($vendorDemands as $vendorDemand) {
                DB::transaction(function() use($vendorDemand, $poService) {
                    $poService->createPurchaseOrderWithDemand($vendorDemand);
                });
            }
            $demand->status = ProductDemand::STATUS_PO_GENERATED;
            $demand->save();
//        }
//        catch (\Exception $ex) {
//            throw new \Exception($ex->getMessage()." - Demand ".$demand->id);
//        }
    }

    private function checkIfVendorHasProduct($vendorId, $groupId, $productId): bool
    {
        /* check vendor */
        $vendor = Vendor::findOrFail($vendorId);
        if(!$vendor->status) return false;

        /* when product ID is specified */
        if(!empty($productId)) {
            $product = Product::find($productId);
            $brand = Brand::find($product->brand_id);
            $vendorMarketer = VendorMarketerMap::where(['marketer_id'=>$brand->marketer_id, 'vendor_id' => $vendorId])->first();
            if(empty($vendorMarketer)) return false;
            return true;
        }

        $brandIds = Product::GroupId($groupId)->get()->pluck('brand_id')->unique('brand_id');
        if(empty($brandIds)) return false;

        $marketerIds = Brand::whereIn('id', $brandIds)->get()->pluck('marketer_id');
        if(empty($marketerIds)) return false;

        foreach ($marketerIds as $marketerId) {
            $vendorMarketer = VendorMarketerMap::where(['marketer_id'=>$marketerId, 'vendor_id' => $vendorId])->first();
            if(empty($vendorMarketer)) return false;
        }

        return true;
    }
}
