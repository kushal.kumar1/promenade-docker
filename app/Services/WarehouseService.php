<?php

namespace Niyotail\Services;

use Niyotail\Exceptions\ServiceException;
use Illuminate\Http\Request;
use Niyotail\Models\Warehouse;

class WarehouseService extends Service
{
    public function create(Request $request)
    {
        $warehouse = new Warehouse();
        $warehouse = $this->processData($warehouse, $request);
        $warehouse->save();
        return $warehouse;
    }

    public function update(Request $request)
    {
        $warehouse = Warehouse::find($request->id);
        if (empty($warehouse)) {
            throw new ServiceException('Invalid Warehouse id');
        }
        $warehouse = $this->processData($warehouse, $request);
        $warehouse->save();
        return $warehouse;
    }

    private function processData(Warehouse $warehouse, $request)
    {
        $warehouse->name = $request->name;
        $warehouse->email = $request->email;
        $warehouse->legal_name = $request->legal_name;
        $warehouse->address = $request->address;
        $warehouse->pincode = $request->pincode;
        $warehouse->pan = $request->pan;
        $warehouse->gstin = $request->gstin;
        $warehouse->invoice_code = $request->invoice_code;
        $warehouse->city_id = $request->city_id;
        $warehouse->status = $request->has('status') ? $request->status : 0;
        return $warehouse;
    }
}
