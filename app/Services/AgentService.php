<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\SendOtpToAgent;
use Niyotail\Models\Agent;
use Niyotail\Models\BeatMarketer;
use Niyotail\Models\DeviceParameter;


class AgentService extends Service
{
    public function create(Request $request)
    {
        $agent = new Agent();
        $this->isDuplicate($request);
        $agent = $this->processData($agent, $request, self::TYPE_CREATE);
        $agent->save();
        return $agent;
    }

    private function isDuplicate($request, $id = null)
    {
        if ($request->has('mobile')) {
            $agentQuery = Agent::where('mobile', $request->mobile);
            if (!empty($id)) {
                $agentQuery->where('id', '!=', $id);
            }
            $agent = $agentQuery->first();
            if (!empty($agent)) {
                throw new ServiceException("Agent with mobile number already exist!");
            }
        }
        return false;
    }

    private function processData(Agent $agent, $request, $type)
    {
        $agent->name = $request->name;
        $agent->code = $request->code;
        if ($this->checkAuthority(self::AUTHORITY_ADMIN) || $type == self::TYPE_CREATE) {
            $agent->mobile = $request->mobile;
        }
        $agent->email = $request->email;
        $agent->type = $request->type;
        return $agent;
    }

    public function update(Request $request, Agent $agent)
    {
        $this->isDuplicate($request, $agent->id);
        $agent = $this->processData($agent, $request, self::TYPE_UPDATE);
        $agent->save();
        return $agent;
    }

    public function updateBeatMap(Request $request)
    {
        foreach ($request->marketers as $marketerId) {
            $beatMarketer = new BeatMarketer();
            $beatMarketer->agent_id = $request->agent_id;
            $beatMarketer->marketer_id = $marketerId;
            $beatMarketer->beat_id = $request->beat_id;
            $beatMarketer->save();
        }
    }

    public function deleteBeatMap(Request $request)
    {
        $beatMarketer = BeatMarketer::where('agent_id', $request->agent_id)->where('id', $request->id)->first();
        if (empty($beatMarketer)) {
            throw new ServiceException("Entry doesn't exist!");
        }
        $beatMarketer->delete();
    }

    public function saveStatus(Request $request, Agent $agent)
    {
        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            $agent->status = $request->status;
            $agent->save();
        }
        return $agent;
    }

    public function sendOtp(Agent $agent)
    {
        if (empty($agent->otp_expiry) || (Carbon::now()->toDateTimeString() > Carbon::parse($agent->otp_expiry)->toDateTimeString())) {
            if ($agent->mobile == '+919999999990') {
                $otp = '123456';
            }else {
                $otp = generateOtp();
            }
            $agent->otp = $otp;
            $agent->otp_expiry = Carbon::now()->addHour()->toDateTimeString();
            $agent->otp_sent_count = 1;
            $agent->save();
        } elseif ($agent->otp_sent_count < 3) {
            if ($agent->mobile == '+919999999990') {
                $agent->otp_sent_count = 1;
            }else{
                $agent->otp_sent_count += 1;
            }

            $agent->save();
        } else {
            throw new ServiceException("You have reached maximum OTP limit. Please try after 1 hour !!");
        }
        dispatch(new SendOtpToAgent($agent));
    }

    public function resetOtpDetails(Agent $agent)
    {
        $agent->otp = null;
        $agent->otp_expiry = null;
        $agent->otp_sent_count = 0;
        $agent->save();
    }

    public function verifyMobile(Agent $agent)
    {
        if (!$agent->isVerified()) {
            $agent->verified = 1;
            $agent->save();
        }
    }

    public function storeDeviceParameters($request, $createdBy)
    {
        DB::transaction(function () use ($request, $createdBy) {
            $query = DeviceParameter::where('device_id', $request->device_id)->where('created_by_id', $createdBy->id);
            if ($createdBy instanceof Agent) {
                $query->where('created_by_type', 'agent');
            } else {
                $query->where('created_by_type', 'user');
            }
            $deviceParams = $query->first();
            if (empty($deviceParams)) {
                $deviceParams = new DeviceParameter();
            }
            $deviceParams = $this->processDeviceData($deviceParams, $request);
            $deviceParams->createdBy()->associate($createdBy);
            $deviceParams->save();
        });
    }

    public function processDeviceData($deviceParams, $request)
    {
        $deviceParams->device_id = $request->device_id;
        if ($request->filled('build_no')) {
            $deviceParams->build_no = $request->build_no;
        }
        return $deviceParams;
    }
}
