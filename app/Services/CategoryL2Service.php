<?php

namespace Niyotail\Services;

use Niyotail\Models\CategoryL2;
use Illuminate\Http\Request;

/**
 * Class CategoryL2Service
 *
 * @package Niyotail\Services
 *
 */
class CategoryL2Service extends Service
{
    public function store (Request $request)
    {
        $category = CategoryL2::create($request->all());
        return $category;
    }

    public function update(Request $request, $id)
    {
        $category = CategoryL2::find($id);
        abort_if(empty($category), 404);

        $category->update($request->all());
        return $category;
    }
}
