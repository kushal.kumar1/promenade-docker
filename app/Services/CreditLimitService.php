<?php

namespace Niyotail\Services;

use Niyotail\Models\Store;
use Niyotail\Models\Invoice;
use Carbon\Carbon;
use DateTimeZone;
use Niyotail\Models\CreditLimit;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Services\Service;
use Illuminate\Http\Request;

class CreditLimitService extends Service
{
    private $creditLimit = 0;
    private $store;

    public function init(Store $store)
    {
        $this->store = $store;
        $this->store->loadMissing(['invoices' => function ($query) {
            $query->where('invoices.status', Invoice::STATUS_GENERATED);
        },'creditLimit']);
    }

    private function isLimitChanged($limitType)
    {
        $oldLimit = $this->store->credit_limit_value;
        $oldLimitType = $this->store->credit_limit_type;
        if ($oldLimit != $this->creditLimit || $oldLimitType != $limitType) {
            return true;
        }

        return false;
    }

    private function isLimitApplicable()
    {
        $totalInvoices = $this->store->invoices->count();
        //Condition 1
        if ($totalInvoices < 5) {
            return false;
        }
        //Condition 2
        $firstInvoice = $this->store->invoices->first();
        $today =  Carbon::now(new DateTimeZone('Asia/Kolkata'));
        $firstInvoiceDate = Carbon::parse($firstInvoice->created_at);
        $diff = $firstInvoiceDate->diffInDays($today);
        if ($diff >= 15) {
            return true;
        }

        return false;
    }

    private function calculateCreditLimit($netSale)
    {
        if ($netSale < 5000) {
            $creditLimit = 0;
        } elseif ($netSale < 15000) {
            $creditLimit = .625 * $netSale;
        } elseif ($netSale < 25000) {
            $creditLimit =  0.75 * $netSale;
        } elseif ($netSale >= 25000) {
            $creditLimit = $netSale;
        }

        $this->creditLimit = round($creditLimit);
    }

    private function getNetSale()
    {
        //Net Sale for last month
        $startDate = (new Carbon('first day of last month', 'Asia/Kolkata'))->toDateString();
        $endDate = (new Carbon('last day of last month', 'Asia/Kolkata'))->toDateString();
        $storeId = $this->store->id;

        $invoices = Invoice::with('shipment.creditNotes')
        ->whereHas('order.store', function ($query) use ($storeId) {
            $query->where('stores.id', $storeId);
        })
        ->whereRaw("date(convert_tz(created_at, '+00:00', '+05:30')) between '$startDate' and '$endDate'")
        ->where('status', Invoice::STATUS_GENERATED)
        ->get();

        $sales = $invoices->sum('amount');
        $credit = $invoices->pluck('shipment.creditNotes')->collapse()->sum('amount');
        return $sales - $credit;
    }

    public function autoGenerate(Store $store)
    {
        if ($store->credit_allowed) {
            $this->init($store);

            if ($this->isLimitApplicable()) {
                $netSale = $this->getNetSale();
                $this->calculateCreditLimit($netSale);
            }

            if ($this->isLimitChanged("automatic")) {
                $creditLimit = new CreditLimit();
                $creditLimit->store_id = $this->store->id;
                $creditLimit->invoice_count = $this->store->invoices->count();
                $creditLimit->sale = !empty($netSale) ? $netSale : null;
                $creditLimit->value = $this->creditLimit;
                $creditLimit->type = "automatic";
                $creditLimit->save();
            }
        }
    }

    public function getAutoCreditLimit(Store $store)
    {
        $this->init($store);
        if ($this->isLimitApplicable()) {
            $netSale = $this->getNetSale();
            $this->calculateCreditLimit($netSale);
        }

        return $this->creditLimit;
    }

    private function createManualEntry(Store $store, $value)
    {
        $this->init($store);
        $this->creditLimit = $value;

        if ($this->isLimitChanged("manual")) {
            $creditLimit = new CreditLimit();
            $creditLimit->store_id = $this->store->id;
            $creditLimit->value = $this->creditLimit;
            $creditLimit->type = "manual";
            $creditLimit->save();
        }
    }

    public function override(Request $request)
    {
        $store = Store::find($request->id);
        if (empty($store)) {
            throw new ServiceException('Store not found!');
        }

        if (!$store->credit_allowed) {
            throw new ServiceException('Credit not allowed to the store. Cannot Override!');
        }

        if ($request->type == "manual") {
            $this->createManualEntry($store, $request->value);
        } elseif ($request->type == "automatic") {
            $this->autoGenerate($store);
        }
    }
}
