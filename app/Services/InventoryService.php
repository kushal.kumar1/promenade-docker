<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Inventory;
use Niyotail\Models\InventoryTransaction;
use Niyotail\Models\Product;
use Niyotail\Models\ProductVariant;
use Illuminate\Http\Request;
use Niyotail\Models\PurchaseInvoiceGrn;
use Niyotail\Models\PurchaseInvoiceItem;
use Illuminate\Support\Facades\DB;

/**
 * Class InventoryService
 *
 * @package Niyotail\Services
 *
 */
class InventoryService extends Service
{
    protected $purchaseInvoiceService;

    public function __construct()
    {
        $this->purchaseInvoiceService = new PurchaseInvoiceService();
    }

    public function createFromGrn(PurchaseInvoiceGrn $grn)
    {
        return DB::transaction(function() use($grn) {
            $grnItems = $grn->items;
            $purchaseInvoice = $grn->invoice;
            $pidCounter = [];

            foreach ($grnItems->where('is_requested', '!=', 1) as $grnItem) {
                $purchaseInvoiceItem = $grnItem->item;
                $isRequested = $grnItem->is_requested;

                if($isRequested) continue;

                $itemVariant = ProductVariant::sku($grnItem->sku)->withTrashed()->first();
                $product = $itemVariant->product;

                if(empty($itemVariant))
                {
                    throw new \Exception('Variant '.$grnItem->sku.' not found when importing inventory');
                }

                $inventoryToUpdate = $itemVariant->quantity * $grnItem->quantity;
                $unitCostPrice = $effectiveUnitCostPrice = 0;
                if (!empty($purchaseInvoiceItem->total_invoiced_cost)) {
                    $unitCostPrice = $purchaseInvoiceItem->total_invoiced_cost;
                }
                if (!empty($purchaseInvoiceItem->total_effective_cost)) {
                    $effectiveUnitCostPrice = $purchaseInvoiceItem->total_effective_cost;
                }

                $productId = $grnItem->product_id;

                if(empty($pidCounter[$productId])) {
                    $pidCounter[$productId] = 1;
                }
                else {
                    $pidCounter[$productId] = $pidCounter[$productId] + 1;
                }

                $pidCounter[$productId];
                $finaBatchNo = $this->getBatchNo($grn, $productId, $pidCounter[$productId]);

                $inventory = new Inventory([
                    'grn_item_id' => $grnItem->id,
                    'purchase_invoice_id' => $purchaseInvoiceItem->purchase_invoice_id,
                    'warehouse_id' => $purchaseInvoice->warehouse_id,
                    'vendor_batch_number' => $purchaseInvoiceItem->vendor_batch_number,
                    'expiry_date' => $purchaseInvoiceItem->expiry_date,
                    'manufacturing_date' => $purchaseInvoiceItem->manufacturing_date,
                    'unit_cost_price' => $unitCostPrice,
                    'effective_unit_cost_price' => $effectiveUnitCostPrice,
                    'product_id' => $grnItem->product_id,
                    'quantity' => $inventoryToUpdate,
                    'original_stock' => $inventoryToUpdate,
                    'batch_number' => $finaBatchNo,
                    'is_direct_store_purchase' => $purchaseInvoice->is_direct_store_purchase
                ]);
                $inventory->save();

                // Activate the product if inactive
                $product->status = Product::STATUS_ACTIVE;
                $product->save();
            }

            return $grn;
        });
    }

    public function getBatchNo($grn, $productId, $pidCounter) {
        $invoice = $grn->invoice;
        $product = Product::find($productId);
        $marketer = null;
        if($product)
        {
            $brand = $product->brand;
            if($brand) {
                $marketer = $brand->marketer;
            }
        }

        $batchNo = [];
        $batchNo[0] = Carbon::parse($grn->getDeliveryDate())->format('Y-m-d');
        $batchNo[1] = str_pad($invoice->vendor_id, '6', '0', STR_PAD_LEFT);
        $batchNo[2] = $invoice->vendor_ref_id;
        $batchNo[3] = $marketer ? $marketer->code : 0;
        $batchNo[4] = str_pad($productId, '9', '0', STR_PAD_LEFT);
        $batchNo[5] = str_pad($pidCounter, '3', '0', STR_PAD_LEFT);
        $batchNo[6] = $grn->id;

        return implode('-', $batchNo);
    }

    public function createOrUpdate(Request $request, ProductVariant $productVariant, $sourceType = null, $sourceId = null)
    {
        $batchId = empty($request->batch_number) ? uniqid($productVariant->product_id).rand(0, 9) : $request->batch_number;
        $expiryDate = empty($request->expiry_date) ? null:Carbon::parse($request->expiry_date)->toDateString();
        $mfgDate = empty($request->mfg_date) ? null:Carbon::parse($request->mfg_date)->toDateString();
        $inventoryToUpdate = $productVariant->quantity * $request->quantity;
        $unitCostPrice = 0;
        if (!empty($request->total_cost)) {
            $unitCostPrice = $request->total_cost / $inventoryToUpdate;
        }

        $inventory = Inventory::firstOrNew([
            'type' => strtolower($request->type),
            'warehouse_id' => $request->warehouse_id,
            'batch_number' => $batchId,
            'expiry_date' => $expiryDate,
            'manufacturing_date' => $mfgDate,
            'unit_cost_price' => $unitCostPrice,
            'product_id' => $productVariant->product_id]);

        $originalQuantity = $inventory->quantity ? $inventory->quantity : 0;

        $inventory->quantity += $inventoryToUpdate;
        $inventory->status = $request->has('status') ? $request->status : 1;

        // optional or 0 fields
        $inventory->base_cost = $request->input('base_cost', 0);
        $inventory->trade_discount = $request->input('trade_discount', 0);
        $inventory->scheme_discount = $request->input('scheme_discount', 0);
        $inventory->qps_discount = $request->input('qps_discount', 0);
        $inventory->cash_discount = $request->input('cash_discount', 0);
        $inventory->program_schemes = $request->input('program_schemes', 0);
        $inventory->other_discount = $request->input('other_discount', 0);
        $inventory->taxes = $request->input('taxes', '');
        $inventory->total_taxable =  $request->input('total_taxable', '');
        $inventory->post_invoice_schemes = $request->input('post_invoice_schemes', 0);
        $inventory->post_invoice_qps = $request->input('post_invoice_qps', 0);
        $inventory->post_invoice_claim = $request->input('post_invoice_claim', 0);
        $inventory->post_invoice_incentive = $request->input('post_invoice_incentive', 0);
        $inventory->post_invoice_other = $request->input('post_invoice_other', 0);
        $inventory->post_invoice_other = $request->input('post_invoice_other', 0);
        $inventory->post_invoice_tot = $request->input('post_invoice_tot', 0);
        $inventory->reward_points_cashback = $request->input('reward_points_cashback', 0);
        $inventory->tax = $request->input('tax', 0);
        $inventory->total_invoiced_cost =  $request->input('total_invoiced_cost', 0);;
        $inventory->total_effective_cost = $request->input('total_effective_cost', 0);;

        $inventory->save();

        return $inventory;
    }

    public function updateStatus(Request $request)
    {
        $inventory = Inventory::find($request->id);
        if (empty($inventory)) {
            throw new ServiceException("Inventory doesn't exist !");
        }

        $inventory->status = $request->status;
        $inventory->inactive_reason = $request->filled('inactive_reason') ? $request->inactive_reason : null;
        $inventory->save();
        return $inventory;
    }

    public function updateType(Request $request)
    {
        $inventory = Inventory::find($request->id);
        if (empty($inventory)) {
            throw new ServiceException("Inventory doesn't exist !");
        }

        if ($inventory->quantity == 0 || $request->quantity > $inventory->quantity) {
            throw new ServiceException("Inventory has either changed or not available anymore. Refresh & try again!");
        }

        if ($inventory->type == $request->type) {
            throw new ServiceException("Inventory type cannot be same!");
        }

        $row = Inventory::firstOrNew([
            'type' => $request->type,
            'batch_number' => $inventory->batch_number,
            'warehouse_id' => $inventory->warehouse_id,
            'product_id' => $inventory->product_id
        ]);

        $row->quantity = isset($row->quantity) ? $row->quantity : 0;
        $row->unit_cost_price = isset($row->unit_cost_price) ? $row->unit_cost_price : $inventory->unit_cost_price;
        $row->expiry_date = isset($row->expiry_date) ? $row->expiry_date : $inventory->expiry_date;
        $row->mfg_date = isset($row->mfg_date) ? $row->mfg_date : $inventory->mfg_date;
        $row->status = isset($row->status) ? $row->status : $inventory->status;
        $row->inactive_reason = isset($row->inactive_reason) ? $row->inactive_reason : $inventory->inactive_reason;

        DB::transaction(function () use ($row, $request, $inventory) {
            $row->quantity += $request->quantity;
            $row->save();

            $inventory->quantity -= $request->quantity;
            $inventory->save();
        });
    }

    public function updateWarehouse(Request $request)
    {
        $inventory = Inventory::find($request->id);
        if (empty($inventory)) {
            throw new ServiceException("Inventory doesn't exist !");
        }

        if ($inventory->quantity == 0 || $request->quantity > $inventory->quantity) {
            throw new ServiceException("Inventory has either changed or not available anymore. Refresh & try again!");
        }

        if ($inventory->warehouse_id == $request->to_warehouse_id) {
            throw new ServiceException("Destination warehouse cannot be same!");
        }

        $row = Inventory::firstOrNew([
            'type' => $inventory->type,
            'batch_number' => $inventory->batch_number,
            'warehouse_id' => $request->to_warehouse_id,
            'product_id' => $inventory->product_id
        ]);

        $row->quantity = isset($row->quantity) ? $row->quantity : 0;
        $row->unit_cost_price = isset($row->unit_cost_price) ? $row->unit_cost_price : $inventory->unit_cost_price;
        $row->expiry_date = isset($row->expiry_date) ? $row->expiry_date : $inventory->expiry_date;
        $row->mfg_date = isset($row->mfg_date) ? $row->mfg_date : $inventory->mfg_date;
        $row->status = isset($row->status) ? $row->status : $inventory->status;
        $row->inactive_reason = isset($row->inactive_reason) ? $row->inactive_reason : $inventory->inactive_reason;

        DB::transaction(function () use ($row, $request, $inventory) {
            $row->quantity += $request->quantity;
            $row->save();

            $inventory->quantity -= $request->quantity;
            $inventory->save();
        });
    }

    public function updateQuantity($product, Request $request)
    {
        if($product->total_inventory > $request->quantity) { //WH stock is more than audited so start decreasing from oldest
            $inventoryToDecrease = $product->total_inventory - $request->quantity;
            $inventories = $product->activeInventories->sortBy('created_at');
            foreach($inventories as $inventory) {
                if ($inventoryToDecrease <= 0) {
                    break;
                }
                $transaction = new InventoryTransaction();
                $transaction->inventory_id = $inventory->id;
                $transaction->type = 'audit';
                $transaction->user_id = $request->user_id;
                if ($inventory->quantity >= $inventoryToDecrease) {
                    $transaction->quantity = $inventory->quantity - $inventoryToDecrease;
                    $transaction->previous_quantity = $inventory->quantity;
                    $inventory->quantity = $inventory->quantity - $inventoryToDecrease;
                    $inventory->save();
                    $inventoryToDecrease = 0;
                } else {
                    $transaction->quantity = 0;
                    $transaction->previous_quantity = $inventory->quantity;
                    $inventoryToDecrease = $inventoryToDecrease - $inventory->quantity;
                    $inventory->quantity = 0;
                    $inventory->save();
                }
                $transaction->save();
            }
        } else if($product->total_inventory < $request->quantity) {//WH stock is less than audited so increase from latest
            $latestInventory = $product->inventories->where('status', 1)->sortByDesc('created_at')->first();

            $transaction = new InventoryTransaction();
            $transaction->inventory_id = $latestInventory->id;
            $transaction->type = 'audit';
            $transaction->user_id = $request->user_id;

            if (!empty($latestInventory)) {
                $transaction->quantity = $request->quantity;
                $transaction->previous_quantity = $latestInventory->quantity;

                $latestInventory->quantity = $request->quantity;
                $latestInventory->save();
            } else {//No inventory line found. Add New.
                $batchNumber = null;
                $batchNumber = $this->getlatestBatchFromGroup($product);
                $newInventory = new Inventory();
                $newInventory->type = 'regular';
                $newInventory->warehouse_id = 2;
                $newInventory->product_id = $product->id;
                $newInventory->quantity = $stock;
                $newInventory->batch_number = !empty($batchNumber) ? $batchNumber : date('Y-m-d').'-STK-AUDIT-'.$product->id;
                $newInventory->save();

                $transaction->quantity = $request->quantity;
                $transaction->previous_quantity = 0;
            }
            $transaction->save();
        }
    }

    private function getLatestBatchFromGroup($product)
    {
        $group = $product->group;
        $groupProducts = $group->products;
        $productIds = $groupProducts->pluck('id')->toArray();
        $latestInventory =  Inventory::whereIn('product_id', $productIds)->orderByRaw("date(SUBSTRING_INDEX(batch_number,'-',3)) desc")->first();

        return !empty($latestInventory) ? $latestInventory->batch_number : null;
    }
}
