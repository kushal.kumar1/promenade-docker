<?php


namespace Niyotail\Services;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\GrnHelper;
use Niyotail\Models\Product;
use Niyotail\Models\ProductRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceGrn;
use Niyotail\Models\PurchaseInvoiceGrnItem;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Models\RackProduct;

class PurchaseInvoiceGrnService
{
    protected $inventoryService;

    public function __construct()
    {
        $this->inventoryService = new InventoryService();
    }

    public function generateGrnForReceivedItems(PurchaseInvoice $purchaseInvoice, $deliveryDate)
    {
        DB::transaction(function () use ($purchaseInvoice, $deliveryDate) {
            $grn = $this->createGrn($purchaseInvoice->id, $deliveryDate);
            $grn->warehouse_id = $purchaseInvoice->warehouse_id;
            $grn->save();
            $grnId = $grn->getId();
            $invoiceItems = $purchaseInvoice->items;
            $receivedItems = $invoiceItems->where('received_quantity', '>', 0);

            foreach ($receivedItems as $receivedItem) {
                $grnItem = new PurchaseInvoiceGrnItem();
                $grnItem->setGrnId($grnId);
                $grnItem->setItemId($receivedItem->id);
                $grnItem->setQuantity($receivedItem->received_quantity);
                $grnItem->setSku($receivedItem->sku);
                $grnItem->is_requested = false;
                $grnItem->product_id = $receivedItem->product_id;
                $grnItem->save();
            }

//            $this->generateGrnPdf($grn);
//            $this->importInventoryByGrn($grn);
            return $grn;
        });
    }

    public function createGrn($invoiceId, $deliveryDate)
    {
        $grn = new PurchaseInvoiceGrn();
        $grn->setPurchaseInvoiceId($invoiceId);
        $grn->setDeliveryDate($deliveryDate);
        $grn->setStatus(PurchaseInvoiceGrn::STATUS_GENERATED);
        $grn->createdBy()->associate(Auth::guard('admin')->user());
        $grn->save();
        return $grn;
    }

    public function createGrnWithItems(PurchaseInvoice $invoice, Request $request)
    {
        return DB::transaction(function () use ($invoice, $request) {
            $deliveryDate = $request->input('delivery_date');
            $grn = $this->createGrn($invoice->id, $deliveryDate);
            $grn->warehouse_id = $invoice->warehouse_id;
            $grn->save();
            $grnId = $grn->getId();
            $this->addItemsToGrn($grnId, $request);

            // import inventory
//            $this->inventoryService->createFromGrn($grn);

            $this->markGrnAsCompleted($grn);
            // return $this->generateGrnPdf($grn);
        });
    }

    public function addItemsToGrn($grnId, Request $request)
    {
        $selectedItems = $request->input('items');

        if (!count($selectedItems)) {
            throw new \Exception('Please select items for GRN');
        }

        foreach ($selectedItems as $selectedItem) {
            $this->createGrnItem($grnId, $selectedItem);
        }
    }

    public function createGrnItem($grnId, $selectedItem)
    {
        if (empty($selectedItem['item_id']) || empty($selectedItem['quantity'])) {
            throw new \Exception('Invalid item data');
        }

        $itemId = $selectedItem['item_id'];
        $purchaseInvoiceItem = PurchaseInvoiceItem::find($itemId);
        if (!$purchaseInvoiceItem) {
            throw new \Exception('Purchase invoice item not found');
        }

        $invoiceVariant = null;
        $productId = $selectedItem['product_id'];
        $isRequested = $selectedItem['is_requested'];
        if ($isRequested) {
            $product = ProductRequest::findOrFail($productId);
        } else {
            $product = Product::findOrFail($productId);
        }

        $quantity = $selectedItem['quantity'];

        $totalUnitsToImport = $this->getGrnItemQtyInUnits($product, $isRequested, $quantity);
        $remainingUnitsToImport = $this->remainingUnitsToImport($purchaseInvoiceItem, $product);

        if ($totalUnitsToImport > $remainingUnitsToImport) {
            throw new \Exception('Units exceeds remaining units to import.');
        }

        foreach ($quantity as $qty) {
            $sku = $qty['sku'];
            $variant = ProductVariant::withTrashed()->sku($sku)->first();
            $variantProduct = $variant->product;
            if ($variantProduct->id != $product->id) {
                $variant = ProductVariant::where('value', $variant->value)->where('product_id', $product->id)->first();
                $sku = $variant->sku;
            }

            $grnItem = new PurchaseInvoiceGrnItem();
            $grnItem->setGrnId($grnId);
            $grnItem->setItemId($itemId);
            $grnItem->setQuantity($qty['quantity']);
            $grnItem->setSku($sku);
            $grnItem->is_requested = $isRequested;
            $isRequested ? $grnItem->requested_product_id = $selectedItem['product_id']
                : $grnItem->product_id = $selectedItem['product_id'];
            $grnItem->save();
        }
    }

    private function getGrnItemQtyInUnits($product, $isRequested, $quantities)
    {
        $totalUnits = 0;
        foreach ($quantities as $qty) {
            $sku = $qty['sku'];
            if (!$isRequested) {
                $variant = ProductVariant::withTrashed()->sku($sku)->first();
                $variantProduct = $variant->product;
                if ($variantProduct->id != $product->id) {
                    $variant = ProductVariant::where('value', $variant->value)->where('product_id', $product->id)->first();
                }
                if (!$variant) {
                    throw new \Exception('Variant not found');
                }
                $totalUnits += $variant->quantity * $qty['quantity'];
            } else {
                switch ($sku) {
                    case "outer":
                        $variantQty = $product->outer_units;
                        break;
                    case "case":
                        $variantQty = $product->case_units;
                        break;
                    default:
                        $variantQty = 1;
                        break;
                }
                $totalUnits += $qty['quantity'] * $variantQty;
            }
        }

        return $totalUnits;
    }

    public function remainingUnitsToImport($purchaseInvoiceItem, $product)
    {
        $totalInvoiceQty = $purchaseInvoiceItem->quantity;
        $invoicedVariant = $purchaseInvoiceItem->productVariant;
        $totalInvoicedUnits = $totalInvoiceQty * $invoicedVariant->quantity;

        $grnItems = $purchaseInvoiceItem->grnItems;
        $totalImportedUnits = 0;
        foreach ($grnItems as $grnItem) {
            $isRequested = $grnItem->is_requested;
            if (!$isRequested) {
                $variant = $grnItem->variant;
                $totalImportedUnits += $variant->quantity * $grnItem->quantity;
            } else {
                $sku = $grnItem->sku;
                switch ($sku) {
                    case "outer":
                        $variantQty = $product->outer_units;
                        break;
                    case "case":
                        $variantQty = $product->case_units;
                        break;
                    default:
                        $variantQty = 1;
                        break;
                }
                $totalImportedUnits = $grnItem->quantity * $variantQty;
            }
        }

        return $totalInvoicedUnits - $totalImportedUnits;
    }

    public function markGrnAsCompleted(PurchaseInvoiceGrn $grn)
    {
        $grn->setStatus(PurchaseInvoiceGrn::STATUS_COMPLETED);
        $grn->save();
        return $grn;
    }

    public function importInventoryByGrn(PurchaseInvoiceGrn $grn)
    {
        /* Assign rack when available for importing items */
        $grnItems = $grn->items;
        foreach ($grnItems as $grnItem) {
            /* @var PurchaseInvoiceItem $invoiceItem */
            $invoiceItem = $grnItem->item;
            $rackId = $invoiceItem->rack_id;
            if ($rackId) {
                $productId = $invoiceItem->product_id;
                $newProductRack = RackProduct::where('product_id', $productId)->where('rack_id', $rackId)->first();
                if (empty($newProductRack)) {
                    $newProductRack = new RackProduct();
                    $newProductRack->product_id = $productId;
                    $newProductRack->rack_id = $rackId;
                }
                $newProductRack->status = 1;
                $newProductRack->save();
            }
            $quantity = $grnItem->variant->quantity * $grnItem->quantity;
            (new FlatInventoryService())->create($grnItem, $grn->warehouse_id, $quantity);
        }


        $grn->inventory_imported = 1;
        $grn->save();
    }

    public function generateGrnPdf(PurchaseInvoiceGrn $grn)
    {
        $invoice = $grn->invoice;
        $items = $this->getGrnItemsForPdf($grn);
        $pdf = GrnHelper::generatePdf($grn, $invoice, $items);
        return $pdf->download('grn-' . $grn->id . '-' . $grn->delivery_date . '.pdf');
    }

    public function getGrnItemsForPdf($grn)
    {
        $grnItems = $grn->items;
        return $grnItems->map(function ($item) {
            $invoicedItem = $item->item;
            $isRequested = $item->is_requested;
            $variant = null;
            $sku = $item->sku;
            if (!$isRequested) {
                $product = $item->product;
                $variant = ProductVariant::withTrashed()->sku($sku)->product($product->id)->first();
                $variantQty = $variant->quantity;
            } else {
                $product = $item->requestedProduct;
                switch ($sku) {
                    case "outer":
                        $variantQty = $product->outer_units;
                        break;
                    case "case":
                        $variantQty = $product->case_units;
                        break;
                    default:
                        $variantQty = 1;
                        break;
                }
            }
            $productName = $product->name;
            if ($isRequested) $productName = 'Requested - ' . $productName;
            $barcode = $product->barcode;
            $sku = $item->sku;
            $unitCost = $invoicedItem->total_invoiced_cost;
            $unitEffectiveCost = $invoicedItem->total_effective_cost;
            $unitMrp = !$isRequested ? $variant->mrp : $product->unit_mrp;
            $totalUnits = $variantQty * $item->quantity;

            $quantity = $item->quantity;

            return [
                'name' => $productName,
                'barcode' => $barcode,
                'unit_mrp' => $unitMrp,
                'total_units' => $totalUnits,
                'sku' => $sku,
                'manufacturing_date' => $invoicedItem->manufacturing_date,
                'expiry_date' => $invoicedItem->expiry_date,
                'vendor_batch_number' => $invoicedItem->vendor_batch_number,
                'quantity' => $quantity,
                'unit_invoiced_cost' => $unitCost,
                'total_invoiced_cost' => $unitCost * $totalUnits,
                'total_effective_cost' => $unitEffectiveCost * $totalUnits
            ];
        });
    }

    public function getInvoiceItemsForGrn(PurchaseInvoice $invoice)
    {
        $invoiceItems = $invoice->items;
        return $invoiceItems->map(function ($item) use ($invoice) {
            $grnItems = $item->grnItems;
            $debitNoteItem = $item->debitNoteItems;
            $totalDebitNoteQuantity = $debitNoteItem ? $debitNoteItem->quantity : 0;
//            $totalReturnedQuantity = $debitNoteItem->quantity;
            $orderedVariant = null;
            $allVariants = null;
            $variants = null;
            $orderedQty = $item->quantity;
            $invoiceVariant = $item->productVariant;
            $product = $item->product;
            $allGroupProducts = $product->group->products;
            $allVariants = $allGroupProducts->map(function ($product) {
                return $product->allVariants;
            })->flatten(1);

            $orderedVariant = $item->productVariant;
            $pcsUom = $orderedVariant->quantity . " " . $orderedVariant->uom;
            $totalOrderedUnits = $orderedQty * $orderedVariant->quantity;
            //->where('quantity', '<=', $orderedVariant->quantity);

            $totalReceivedUnits = 0;
            foreach ($grnItems as $grnItem) {
                $sku = $grnItem->sku;
                $isGrnItemRequested = $grnItem->is_requested;
                if (!$isGrnItemRequested) {
                    $grnVariant = $allVariants->where('sku', $sku)->first();
                    if (!$grnVariant) {
                        throw new \Exception('Variant not found');
                    }
                    $grnVariantQty = $grnVariant->quantity;
                } else {
                    $requestedProduct = $grnItem->requestedProduct;
                    switch ($sku) {
                        case "outer":
                            $grnVariantQty = $requestedProduct->outer_units;
                            break;
                        case "case":
                            $grnVariantQty = $requestedProduct->case_units;
                            break;
                        default:
                            $grnVariantQty = 1;
                            break;
                    }
                }

                $qty = $grnItem->quantity;
                $totalReceivedUnits += $qty * $grnVariantQty;
            }

            $remainingUnits = $totalOrderedUnits - $totalReceivedUnits - $totalDebitNoteQuantity;

            $group = $item->product->group;
            $groupProducts = $group->products;
            $groupRequestedProducts = $group->requestedProducts->where('status', 'pending');
            $finalGroupProducts = array();
            foreach ($groupProducts as $groupProduct) {
                $variants = $groupProduct->allVariants;
                $finalGroupProducts[$groupProduct->id] = [
                    'invoice_item_id' => $item->id,
                    'product_id' => $groupProduct->id,
                    'is_requested' => false,
                    'name' => $groupProduct->name,
                    'barcode' => $groupProduct->barcode,
                    'variants' => $variants->map(function ($variant) use ($remainingUnits) {
                        return [
                            'sku' => $variant->sku,
                            'value' => $variant->value,
                            'quantity' => $variant->quantity,
                            'min' => 0,
                            'max' => floor($remainingUnits / $variant->quantity),
                            'mrp' => $variant->mrp
                        ];
                    })
                ];
            }
            foreach ($groupRequestedProducts as $groupRequestedProduct) {
                $variants = [
                    [
                        'sku' => 'unit',
                        'value' => 'unit',
                        'quantity' => 1,
                        'min' => 0,
                        'max' => floor($remainingUnits),
                        'mrp' => $groupRequestedProduct->mrp
                    ]
                ];

                if ($groupRequestedProduct->outer_units) {
                    $variants[] = [
                        'sku' => 'outer',
                        'value' => 'outer',
                        'quantity' => $groupRequestedProduct->outer_units,
                        'min' => 0,
                        'max' => floor($remainingUnits / $groupRequestedProduct->outer_units)
                    ];
                }

                if ($groupRequestedProduct->case_units) {
                    $variants[] = [
                        'sku' => 'case',
                        'value' => 'case',
                        'quantity' => $groupRequestedProduct->case_units,
                        'min' => 0,
                        'max' => floor($remainingUnits / $groupRequestedProduct->case_units)
                    ];
                }

                $finalGroupProducts[] = [
                    'product_id' => $groupRequestedProduct->id,
                    'is_requested' => true,
                    'name' => $groupRequestedProduct->name,
                    'barcode' => $groupRequestedProduct->barcode,
                    'variants' => $variants
                ];
            }

            $selectedProduct = null;
            $unitMrp = 0;
            if ($item->product_id) {
                $selectedProduct = $finalGroupProducts[$item->product_id];
                $spv = $selectedProduct['variants']->where('value', 'unit')->first();
                $unitMrp = $spv['mrp'];
            }

            return [
                'is_complete' => $invoice->status === 'completed' ? true : $remainingUnits == 0,
                'product_id' => $invoice->status === 'completed' ? $item->product_id : $item->product_id,
                'productVariant' => $item->productVariant,
                'taxes' => $item->taxes,
                'group_id' => $group->id,
                'group_name' => $group->name,
                'invoice_item_id' => $item->id,
                'ordered_sku' => $item->sku,
                'qty_per_variant' => $orderedVariant->quantity,
                'pcs_uom' => $pcsUom,
                'variant_quantity' => $item->quantity,
                'quantity' => [
                    'ordered' => $totalOrderedUnits,
                    'received' => $totalReceivedUnits,
                    'debit_note' => $totalDebitNoteQuantity,
                    'remaining' => $remainingUnits,
//                    'returned' => $totalReturnedQuantity,
                ],
                'products' => $finalGroupProducts,
                'invoiced_cost' => number_format($item->total_invoiced_cost * $invoiceVariant->quantity * $item->quantity, 2),
                'post_tax_discount' => number_format($item->post_tax_discount * $invoiceVariant->quantity * $item->quantity, 2),
                'effective_cost' => number_format($item->total_effective_cost * $invoiceVariant->quantity * $item->quantity, 2),
                'margin' => $unitMrp ? number_format(($unitMrp - $item->total_effective_cost) * 100 / $unitMrp, 2) : 0,
                'unit_invoiced_cost' => number_format(($item->total_invoiced_cost), 2),
            ];
        });
    }

    public function ifEligibleForDebitNote($items)
    {
        $isAllowed = false;
        foreach ($items as $item) {
            if ($item['quantity']['remaining'] > 0) {
                $isAllowed = true;
                break;
            }
        }

        return $isAllowed;
    }
}