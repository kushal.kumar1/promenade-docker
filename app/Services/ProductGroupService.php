<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Niyotail\Models\ProductGroup;

class ProductGroupService extends Service
{
    public function create ($request, $user) {
        return ProductGroup::create($request->all());
    }

    public function update ($id, $request, $user) {
        return ProductGroup::where('id', $id)->update($request->all());
    }
}
