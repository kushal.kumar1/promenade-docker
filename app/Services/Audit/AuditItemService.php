<?php

namespace Niyotail\Services\Audit;

use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Audit\AuditItemMeta;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Permission;
use Niyotail\Services\Service;

abstract class AuditItemService extends Service
{
    public static function canViewAuditItem(AuditItem $auditItem, Employee $employee): bool
    {
        static $viewAuditPermission = null;
        static $manageAuditPermission = null;
        if (empty($viewAuditItemPermission) || empty($manageAuditItemPermission)) {
            $viewAuditPermission = Permission::where('name', AuditService::PERMISSION_VIEW_AUDIT)->first();
            $manageAuditPermission = Permission::where('name', AuditService::PERMISSION_MANAGE_AUDIT)->first();
        }

        if ($employee->hasPermission($manageAuditPermission) && AuditService::canManageAudit($auditItem->audit, $employee))
            return true;

        if ($employee->hasPermission($viewAuditPermission) && $auditItem->assigned_to == $employee->id)
            return true;

        return false;
    }

    public static function canManageAuditItem(AuditItem $auditItem, Employee $employee): bool
    {
        return AuditService::canManageAudit($auditItem->audit, $employee);
    }

    public static function canChangeAuditItemStatus($auditItem, $newStatus): bool
    {
        switch ($auditItem->status) {
            case AuditItem::STATUS_GENERATED:
            {
                switch ($newStatus) {
                    case AuditItem::STATUS_ASSIGNED:
                        if ($auditItem->audit->status != Audit::STATUS_CONFIRMED)
                            return false;
                        return true;
                    case AuditItem::STATUS_CANCELLED:
                        return true;
                    default:
                        return false;
                }
            }
            case AuditItem::STATUS_ASSIGNED:
            {
                switch ($newStatus) {
                    case AuditItem::STATUS_PENDING_APPROVAL:
                        if ($auditItem->audit->status != Audit::STATUS_STARTED)
                            return false;
                        return true;
                    case AuditItem::STATUS_CANCELLED:
                        return true;
                    case AuditItem::STATUS_PENDING_ACTION:
                        return is_null($auditItem->storage_id);
                    default:
                        return false;
                }
            }
            case AuditItem::STATUS_PENDING_APPROVAL:
            {
                switch ($newStatus) {
                    case AuditItem::STATUS_PENDING_ACTION:
                        if ($auditItem->audit->status != Audit::STATUS_STARTED)
                            return false;
                        return true;
                    case AuditItem::STATUS_ASSIGNED:
                    case AuditItem::STATUS_CANCELLED:
                        return true;
                    case AuditItem::STATUS_RESOLVED:
                        return $auditItem->audit->type == Audit::TYPE_STORAGE;
                    default:
                        return false;
                }
            }
            case AuditItem::STATUS_PENDING_ACTION:
            {
                switch ($newStatus) {
                    case AuditItem::STATUS_RESOLVED:
                        if (AuditItemService::updatedSystemQuantity($auditItem) != AuditItemService::actualQuantity($auditItem))
                            return false;
                        return true;
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }

    public static function canPerformAction($auditItem, $action, $quantity = 0): bool
    {
        $audit = $auditItem->audit;
        if (!AuditService::canperformAction($audit, $action))
            return false;

        switch ($auditItem->status) {
            case AuditItem::STATUS_PENDING_ACTION:
            {
                $actualQuantity = self::actualQuantity($auditItem);
                $updatedSystemQuantity = self::updatedSystemQuantity($auditItem);

                if ($actualQuantity == $updatedSystemQuantity)
                    return false;
                if ($quantity > abs($actualQuantity - $updatedSystemQuantity))
                    return false;

                switch ($action) {
                    case AuditItemAction::ACTION_HOLD:
                        if ($auditItem->auditItemActions()->whereAction(AuditItemAction::ACTION_HOLD)->exists())
                            return false;
//                        if ($actualQuantity > $updatedSystemQuantity)
//                            return false;
                        return true;
                    case AuditItemAction::ACTION_LOSS:
                        if ($actualQuantity > $updatedSystemQuantity)
                            return false;
                        return true;
                    case AuditItemAction::ACTION_EXCESS:
                        if ($actualQuantity < $updatedSystemQuantity)
                            return false;
                        return true;
                    default:
                        return false;
                }
            }
            case AuditItem::STATUS_RESOLVED:
            {
                $holdQuantity = $auditItem->auditItemActions()
                    ->where('action', AuditItemAction::ACTION_HOLD)
                    ->get()
                    ->reduce(function ($holdQuantity, $auditItemAction) {
                        return $holdQuantity + $auditItemAction->quantity;
                    }, 0);

                if ($quantity > $holdQuantity)
                    return false;

                $systemQuantity = $auditItem->system_quantity;
                $actualQuantity = self::actualQuantity($auditItem);

                switch ($action) {
                    case AuditItemAction::ACTION_RELEASE:
                        return true;
                    case AuditItemAction::ACTION_LOSS:
                        if ($systemQuantity < $actualQuantity)
                            return false;
                        return true;
                    case AuditItemAction::ACTION_EXCESS:
                        if ($systemQuantity > $actualQuantity)
                            return false;
                        return true;
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }


    public static function createAuditItem($auditId, $productId, $storageId, $remarks = null): AuditItem
    {
        if (!AuditService::canCreateAuditItem(Audit::find($auditId))) {
            throw new ServiceException("Cannot add items to Audit ({$auditId})");
        }

        $auditItem = AuditItem::where('audit_id', $auditId)
            ->where('product_id', $productId)
            ->where('storage_id', $storageId)
            ->first();

        if (empty($auditItem)) {
            $auditItem = new AuditItem();
            $auditItem->audit_id = $auditId;
            $auditItem->product_id = $productId;
            $auditItem->storage_id = $storageId;
        }

        $auditItem->remarks = $remarks;
        $auditItem->status = AuditItem::STATUS_GENERATED;
        $auditItem->system_quantity = 0;

        $auditItem->saveOrFail();
        self::refreshSystemQuantity($auditItem);

        return $auditItem;
    }

    public static function refreshSystemQuantity($auditItem)
    {
        DB::transaction(function () use ($auditItem) {
            $auditItem->system_quantity = FlatInventory::where('warehouse_id', $auditItem->audit->warehouse_id)
                ->where('product_id', $auditItem->product_id)
                ->where('storage_id', $auditItem->storage_id)
                ->where('status', FlatInventory::STATUS_READY_FOR_SALE)
                ->count();

            $auditItem->saveOrFail();
        });
    }

    public static function attachInventories($auditItem)
    {
        $inventoryIds = FlatInventory::where('warehouse_id', $auditItem->audit->warehouse_id)
            ->where('storage_id', $auditItem->storage_id)
            ->where('product_id', $auditItem->product_id)
            ->whereIn('status', [
                FlatInventory::STATUS_READY_FOR_SALE,
                FlatInventory::STATUS_AUDIT_BLOCKED
            ])
            ->pluck('id')
            ->toArray();

//        DB::table('')->updateOrInsert([
//            ['' => '', '' => ''],
//            ['' => '', '' => '']
//        ]);

        $auditItem->inventories()->sync($inventoryIds);
    }

    public static function blockInventories($auditItem)
    {
        DB::statement('UPDATE flat_inventories SET status = ? WHERE status = ? and id in (select flat_inventory_id from audit_item_audit_item_action_flat_inventory where audit_item_id = ?)', [
            FlatInventory::STATUS_AUDIT_BLOCKED,
            FlatInventory::STATUS_READY_FOR_SALE,
            $auditItem->id
        ]);
//        DB::transaction(function () use ($auditItem) {
//            foreach ($auditItem->inventories as $flatInventory) {
//                if ($flatInventory->status == FlatInventory::STATUS_READY_FOR_SALE) {
//                    $flatInventory->status = FlatInventory::STATUS_AUDIT_BLOCKED;
//                    $flatInventory->saveOrFail();
//                }
//            }
//        });
    }

    public static function unblockInventories($auditItem)
    {
        DB::statement('UPDATE flat_inventories SET status = ? WHERE status = ? and id in (select flat_inventory_id from audit_item_audit_item_action_flat_inventory where audit_item_id = ?)', [
            FlatInventory::STATUS_READY_FOR_SALE,
            FlatInventory::STATUS_AUDIT_BLOCKED,
            $auditItem->id
        ]);
//        DB::transaction(function () use ($auditItem) {
//            foreach ($auditItem->inventories as $flatInventory) {
//                if ($flatInventory->status == FlatInventory::STATUS_AUDIT_BLOCKED) {
//                    $flatInventory->status = FlatInventory::STATUS_READY_FOR_SALE;
//                    $flatInventory->saveOrFail();
//                }
//            }
//        });
    }


    public static function assignAuditItem(AuditItem $auditItem, Employee $employee, Employee $assignedTo)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to assign AuditItem ({$auditItem->id})");

        if (!static::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_ASSIGNED))
            throw new ServiceException("Audit Item ({$auditItem->id}) cannot be assigned");

        $auditItem->assigned_to = $assignedTo->id;
        $auditItem->status = AuditItem::STATUS_ASSIGNED;
        $auditItem->saveOrFail();
    }


    public static function inputResult($auditItem, $employee, $actualQuantities)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to input result for AuditItem ({$auditItem->id})");

        DB::transaction(function () use ($auditItem, $actualQuantities) {
            if (!static::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED))
                throw new ServiceException("Cannot input AuditItem Result ({$auditItem->id})");

            foreach ($actualQuantities as $actualQuantity) {
                $auditItemMeta = new AuditItemMeta();

                $auditItemMeta->audit_item_id = $auditItem->id;
                $auditItemMeta->condition = $actualQuantity['condition'];
                $auditItemMeta->quantity = $actualQuantity['quantity'];
                $auditItemMeta->reason_id = $actualQuantity['reason_id'];
                $auditItemMeta->remarks = $actualQuantity['remarks'];

                $auditItemMeta->saveOrFail();
            }
        });
    }

    public static function submitResult(AuditItem $auditItem, $employee)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to submit result for AuditItem ({$auditItem->id})");

        if (!static::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_PENDING_APPROVAL))
            throw new ServiceException("Cannot submit AuditItem Result({$auditItem->id})");

        $auditItem->status = AuditItem::STATUS_PENDING_APPROVAL;
        $auditItem->saveOrFail();
    }

    public static function rejectAuditItem($auditItem, $employee)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to reject result for AuditItem ({$auditItem->id})");

        DB::transaction(function () use ($auditItem) {
            if (!static::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_ASSIGNED))
                throw new ServiceException("Cannot reject result for AuditItem ({$auditItem->id})");

            $auditItem->auditItemMetas()->delete();

            $auditItem->status = AuditItem::STATUS_ASSIGNED;
            $auditItem->save();
        });
    }


    public static function cancelAuditItem($auditItem, $employee)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to cancel AuditItem ({$auditItem->id})");

        DB::transaction(function () use ($auditItem) {
            if (!static::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED))
                throw new ServiceException("Cannot cancel AuditItem ({$auditItem->id})");

            $auditItem->auditItemMetas()->delete();

            self::unblockInventories($auditItem);
            $auditItem->inventories()->sync([]);

            $auditItem->status = AuditItem::STATUS_CANCELLED;
            $auditItem->save();
        });
    }


    public static function hold($auditItem, $employee, $quantity, $destinationStorageId = null, $reasonId = null)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to take action HOLD on AuditItem ({$auditItem->id})");

        DB::transaction(function () use ($employee, $destinationStorageId, $reasonId, $quantity, $auditItem) {
            if (!static::canPerformAction($auditItem, AuditItemAction::ACTION_HOLD, $quantity))
                throw new ServiceException("Cannot take action HOLD on AuditItem ({$auditItem->id})");

            $auditItemAction = new AuditItemAction();
            $auditItemAction->audit_item_id = $auditItem->id;
            $auditItemAction->action = AuditItemAction::ACTION_HOLD;
            $auditItemAction->quantity = $quantity;
            $auditItemAction->destination_storage_id = $destinationStorageId;
            $auditItemAction->reason_id = $reasonId;
            $auditItemAction->taken_by = $employee->id;

            $auditItemAction->saveOrFail();
        });
    }

    public static function markExcess($auditItem, $employee, $quantity, $destinationStorageId = null, $reasonId = null)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to take action MARK EXCESS on AuditItem ({$auditItem->id})");


        DB::transaction(function () use ($employee, $reasonId, $destinationStorageId, $quantity, $auditItem) {
            if (!static::canPerformAction($auditItem, AuditItemAction::ACTION_EXCESS, $quantity))
                throw new ServiceException("Cannot take action MARK EXCESS on AuditItem ({$auditItem->id})");

            $audit = $auditItem->audit;
            $process = $audit->status == Audit::STATUS_COMPLETED;

            $auditItemAction = new AuditItemAction();
            $auditItemAction->audit_item_id = $auditItem->id;
            $auditItemAction->action = AuditItemAction::ACTION_EXCESS;
            $auditItemAction->quantity = $quantity;
            $auditItemAction->destination_storage_id = $destinationStorageId;
            $auditItemAction->reason_id = $reasonId;
            $auditItemAction->taken_by = $employee->id;

            $auditItemAction->saveOrFail();

            if ($process) {
                $holdAuditItemAction = $auditItem->auditItemActions()->where('action', AuditItemAction::ACTION_HOLD)->firstOrFail();
                $holdAuditItemAction->quantity = $holdAuditItemAction->quantity - $quantity;
                $holdAuditItemAction->saveOrFail();

                for ($i = 0; $i < $auditItemAction->quantity; $i++) {
                    $inventory = new FlatInventory();
                    $inventory->product_id = $auditItem->product_id;
                    $inventory->storage_id = $auditItem->storage_id;
                    $inventory->warehouse_id = $audit->warehouse_id;

                    $inventory->source_type = 'audit_item_action';
                    $inventory->source_id = $auditItemAction->id;

                    $inventory->unit_cost = 0;
                    $inventory->status = FlatInventory::STATUS_READY_FOR_SALE;

                    $inventory->saveOrFail();

                    $auditItem->inventories()->sync([
                        $inventory->id => ['audit_item_action_id' => $auditItemAction->id]
                    ], false);
                }
            }
        });
    }

    public static function markLoss($auditItem, $employee, $quantity, $destinationStorageId = null, $reasonId = null)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to take action MARK LOSS on AuditItem ({$auditItem->id})");


        DB::transaction(function () use ($destinationStorageId, $employee, $reasonId, $quantity, $auditItem) {
            if (!static::canPerformAction($auditItem, AuditItemAction::ACTION_LOSS, $quantity))
                throw new ServiceException("Cannot take action MARK LOSS on AuditItem ({$auditItem->id})");

            $audit = $auditItem->audit;
            $process = $audit->status == Audit::STATUS_COMPLETED;

            $auditItemAction = new AuditItemAction();
            $auditItemAction->audit_item_id = $auditItem->id;
            $auditItemAction->action = AuditItemAction::ACTION_LOSS;
            $auditItemAction->quantity = $quantity;
            $auditItemAction->destination_storage_id = $destinationStorageId;
            $auditItemAction->reason_id = $reasonId;
            $auditItemAction->taken_by = $employee->id;

            $auditItemAction->saveOrFail();

            if ($process) {
                $holdAuditItemAction = $auditItem->auditItemActions()->where('action', AuditItemAction::ACTION_HOLD)->firstOrFail();
                $holdAuditItemAction->quantity = $holdAuditItemAction->quantity - $quantity;
                $holdAuditItemAction->saveOrFail();

                $actionInventories = $auditItem->inventories()
                    ->where('status', FlatInventory::STATUS_AUDIT_HOLD)
                    ->take($auditItemAction->quantity)
                    ->get();

                foreach ($actionInventories as $actionInventory) {
                    $actionInventory->status = FlatInventory::STATUS_AUDIT_LOSS;
                    $actionInventory->saveOrFail();

                    $auditItem->inventories()->sync([
                        $actionInventory->id => ['audit_item_action_id' => $auditItemAction->id]
                    ], false);
                }
            }
        });
    }

    public static function release($auditItem, $employee, $quantity, $destinationStorageId, $reasonId)
    {
        if (!static::canManageAuditItem($auditItem, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to take action RELEASE on AuditItem ({$auditItem->id})");


        DB::transaction(function () use ($auditItem, $quantity, $destinationStorageId, $reasonId, $employee) {
            if (!static::canPerformAction($auditItem, AuditItemAction::ACTION_RELEASE, $quantity))
                throw new ServiceException("Cannot take action RELEASE on AuditItem ({$auditItem->id})");

            $audit = $auditItem->audit;
            $process = $audit->status == Audit::STATUS_COMPLETED;

            $auditItemAction = new AuditItemAction();
            $auditItemAction->audit_item_id = $auditItem->id;
            $auditItemAction->action = AuditItemAction::ACTION_RELEASE;
            $auditItemAction->quantity = $quantity;
            $auditItemAction->destination_storage_id = $destinationStorageId;
            $auditItemAction->reason_id = $reasonId;
            $auditItemAction->taken_by = $employee->id;

            $auditItemAction->saveOrFail();

            if ($process) {
                $holdAuditItemAction = $auditItem->auditItemActions()->where('action', AuditItemAction::ACTION_HOLD)->firstOrFail();
                $holdAuditItemAction->quantity = $holdAuditItemAction->quantity - $quantity;
                $holdAuditItemAction->saveOrFail();

                $systemQuantity = $auditItem->system_quantity;
                $actualQuantity = self::actualQuantity($auditItem);

                if ($systemQuantity > $actualQuantity) {
                    $actionInventories = $auditItem->inventories()
                        ->where('status', FlatInventory::STATUS_AUDIT_HOLD)
                        ->take($auditItemAction->quantity)
                        ->get();

                    foreach ($actionInventories as $actionInventory) {
                        $actionInventory->status = FlatInventory::STATUS_READY_FOR_SALE;
                        $actionInventory->storage_id = $destinationStorageId;
                        $actionInventory->saveOrFail();

                        $auditItem->inventories()->sync([
                            $actionInventory->id => ['audit_item_action_id' => $auditItemAction->id]
                        ], false);
                    }
                }

                // TODO confirm logic for excess release
                if ($systemQuantity < $actualQuantity) {
//                    $badActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_BAD);
//                    $badInventoriesCount = max($badActualQuantity - $auditItem->system_quantity, 0);
                    // create new inventories
//                    for ($i = 0; $i < $auditItemAction->quantity; $i++) {
//                        $inventory = new FlatInventory();
//                        $inventory->product_id = $auditItem->product_id;
//                        $inventory->storage_id = $auditItem->storage_id;
//                        $inventory->warehouse_id = $auditItem->audit->warehouse_id;
//
//                        $inventory->source_type = 'audit_item_action';
//                        $inventory->source_id = $auditItemAction->id;
//
//                        $inventory->unit_cost = 0;
//                        $inventory->status = $i < $badInventoriesCount ? FlatInventory::STATUS_AUDIT_BAD : FlatInventory::STATUS_READY_FOR_SALE;
//
//                        $inventory->saveOrFail();
//
//                        $auditItem->inventories()->sync([
//                            $inventory->id => ['audit_item_action_id' => $auditItemAction->id]
//                        ], false);
//                    }

//                    $actionInventories = $auditItem->inventories()
//                        ->where('status', FlatInventory::STATUS_AUDIT_HOLD)
//                        ->take($auditItemAction->quantity)
//                        ->get();

//                    foreach ($actionInventories as $actionInventory) {
//                        $actionInventory->status = FlatInventory::STATUS_READY_FOR_SALE;
//                        $actionInventory->storage_id = $destinationStorageId;
//                        $actionInventory->saveOrFail();
//
//                        $auditItem->inventories()->sync($actionInventory->id, [
//                            'audit_item_action_id' => $auditItemAction->id
//                        ], false);
//                    }
                }
            }
        });

    }

    public static function actualQuantity($auditItem, $condition = null)
    {
        return $auditItem->auditItemMetas->reduce(function ($actualQuantity, $auditItemMeta) use ($condition) {
            return $actualQuantity + (!is_null($condition) && $auditItemMeta->condition != $condition ? 0 : $auditItemMeta->quantity);
        }, 0);
    }

    public static function updatedSystemQuantity($auditItem)
    {
        return $auditItem->auditItemActions->reduce(function ($updatedSystemQuantity, $auditItemAction) use ($auditItem) {
            switch ($auditItemAction->action) {
                case AuditItemAction::ACTION_HOLD:
                    if ($auditItem->system_quantity > AuditItemService::actualQuantity($auditItem))
                        return $updatedSystemQuantity - $auditItemAction->quantity;
                    else
                        return $updatedSystemQuantity + $auditItemAction->quantity;
                case AuditItemAction::ACTION_LOSS:
                    return $updatedSystemQuantity - $auditItemAction->quantity;
                case AuditItemAction::ACTION_EXCESS:
                    return $updatedSystemQuantity + $auditItemAction->quantity;
                default:
                    return $updatedSystemQuantity;
            }
        }, $auditItem->system_quantity);
    }
}