<?php

namespace Niyotail\Services\Audit;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Request;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Audit\AuditItemMeta;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Permission;
use Niyotail\Services\Service;

abstract class AuditService extends Service
{
    const PERMISSION_VIEW_AUDIT = "view-audit";
    const PERMISSION_MANAGE_AUDIT = "manage-audit";

    public static function canViewAudit(Audit $audit, Employee $employee): bool
    {
        static $viewAuditPermission = null;
        static $manageAuditPermission = null;
        if (empty($viewAuditPermission) || empty($manageAuditPermission)) {
            $viewAuditPermission = Permission::where('name', static::PERMISSION_VIEW_AUDIT)->first();
            $manageAuditPermission = Permission::where('name', static::PERMISSION_MANAGE_AUDIT)->first();
        }

        if ($employee->hasRole('Admin') || $employee->hasPermission($manageAuditPermission))
            return true;

        if ($employee->hasPermission($viewAuditPermission)) {
            foreach ($audit->auditItems as $auditItem) {
                if ($auditItem->assigned_to == $employee->id)
                    return true;
            }
        }

        return false;
    }

    public static function canManageAudit(Audit $audit, Employee $employee): bool
    {
        static $manageAuditPermission = null;
        if (empty($manageAuditPermission)) {
            $manageAuditPermission = Permission::where('name', static::PERMISSION_MANAGE_AUDIT)->first();
        }

        return $employee->hasRole('Admin') || $employee->hasPermission($manageAuditPermission) && $employee->id == $audit->created_by;
    }

    public static function canChangeAuditStatus($audit, $newStatus): bool
    {
        switch ($audit->status) {
            case Audit::STATUS_GENERATED:
                switch ($newStatus) {
                    case Audit::STATUS_CONFIRMED:
                    case Audit::STATUS_CANCELLED:
                        return true;
                    default:
                        return false;
                }
            case Audit::STATUS_CONFIRMED:
                switch ($newStatus) {
                    case Audit::STATUS_STARTED:
                        foreach ($audit->auditItems()->notCancelled()->get() as $auditItem)
                            if ($auditItem->status != AuditItem::STATUS_ASSIGNED)
                                return false;
                        return true;
                    case Audit::STATUS_CANCELLED:
                        return true;
                    default:
                        return false;
                }
            case Audit::STATUS_STARTED:
            {
                switch ($newStatus) {
                    case Audit::STATUS_APPROVED:
                        if ($audit->type == Audit::TYPE_STORAGE)
                            return false;
                        foreach ($audit->auditItems()->notCancelled()->get() as $auditItem)
                            if ($auditItem->status != AuditItem::STATUS_PENDING_APPROVAL && !is_null($auditItem->storage_id))
                                return false;
                        return true;
                    case Audit::STATUS_COMPLETED:
                        if ($audit->type == Audit::TYPE_PRODUCT)
                            return false;
                        foreach ($audit->auditItems()->notCancelled()->get() as $auditItem)
                            if ($auditItem->status != AuditItem::STATUS_PENDING_APPROVAL)
                                return false;
                        return true;
                    case Audit::STATUS_CANCELLED:
                        return true;
                    default:
                        return false;
                }
            }
            case Audit::STATUS_APPROVED:
            {
                switch ($newStatus) {
                    case Audit::STATUS_COMPLETED:
                        if ($audit->type == Audit::TYPE_PRODUCT) {
                            foreach (self::distinctProductIds($audit) as $productId) {
                                $actualQuantity = self::totalActualQuantity($audit, $productId);
                                $updatedSystemQuantity = self::totalUpdatedSystemQuantity($audit, $productId);

                                if ($actualQuantity != $updatedSystemQuantity)
                                    return false;
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }

    public static function canPerformAction($audit, $action): bool
    {
        switch ($audit->status) {
            case Audit::STATUS_APPROVED:
                switch ($action) {
                    case AuditItemAction::ACTION_LOSS:
                    case AuditItemAction::ACTION_EXCESS:
                    case AuditItemAction::ACTION_HOLD:
                        return true;
                    default:
                        return false;
                }
            case Audit::STATUS_COMPLETED:
                switch ($action) {
                    case AuditItemAction::ACTION_LOSS:
                    case AuditItemAction::ACTION_EXCESS:
                    case AuditItemAction::ACTION_RELEASE:
                        return true;
                    default:
                        return false;
                }
            default:
                return false;
        }
    }

    public static function canCreateAuditItem($audit): bool
    {
        switch ($audit->status) {
            case Audit::STATUS_GENERATED:
            case Audit::STATUS_STARTED:
                return true;
            default:
                return false;
        }
    }

    public static function createAudit($name, $type, $warehouseId, $createdById, $reasonId = null): Audit
    {
        $audit = new Audit();

        $audit->name = $name;
        $audit->type = $type;
        $audit->warehouse_id = $warehouseId;
        $audit->created_by = $createdById;
        $audit->reason_id = $reasonId;

        $audit->status = Audit::STATUS_GENERATED;

        $audit->saveOrFail();
        return $audit;
    }

    public static function generateAuditItems($audit, $item)
    {
        if ($audit->status != Audit::STATUS_GENERATED) {
            throw new ServiceException("Cannot generate Audit Items for Audit ({$audit->id})");
        }

        if ($audit->type == Audit::TYPE_PRODUCT) {
            DB::transaction(function () use ($item, $audit) {
                $productId = $item;
                $storageIds = FlatInventory::where('product_id', $productId)
//                    ->whereNotNull('storage_id')
                    ->where('warehouse_id', $audit->warehouse_id)
                    ->where('status', FlatInventory::STATUS_READY_FOR_SALE)
                    ->distinct()
                    ->pluck('storage_id');

                foreach ($storageIds as $storageId)
                    AuditItemService::createAuditItem($audit->id, $productId, $storageId);

                if ($storageIds->count() == 0)
                    AuditItemService::createAuditItem($audit->id, $productId, null);
            });
        } else {
            DB::transaction(function () use ($item, $audit) {
                $storageId = $item;
                $productIds = FlatInventory::where('storage_id', $storageId)
//                    ->whereNotNull('product_id')
                    ->where('warehouse_id', $audit->warehouse_id)
                    ->where('status', FlatInventory::STATUS_READY_FOR_SALE)
                    ->distinct()
                    ->pluck('product_id');

                foreach ($productIds as $productId) {
                    AuditItemService::createAuditItem($audit->id, $productId, $storageId);
                }
            });
        }
    }

    public static function confirmAudit($audit, $employee)
    {
        if (!static::canManageAudit($audit, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to confirm Audit ({$audit->id})");

        if (!static::canChangeAuditStatus($audit, Audit::STATUS_CONFIRMED))
            throw new ServiceException("Cannot confirm Audit ({$audit->id})");

        $audit->status = Audit::STATUS_CONFIRMED;
        $audit->saveOrFail();
    }

    public static function startAudit($audit, $employee)
    {
        if (!static::canManageAudit($audit, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to start Audit ({$audit->id})");

        DB::transaction(function () use ($audit) {
            if (!static::canChangeAuditStatus($audit, Audit::STATUS_STARTED))
                throw new ServiceException("Cannot start Audit ({$audit->id})");

            try {
                static::checkAuditInventoryConflict($audit);
            } catch (Exception $exception) {
                throw new ServiceException('Audit cannot start because inventory is in conflict', 0, $exception);
            }

            foreach ($audit->auditItems()->notCancelled()->cursor() as $auditItem) {
                AuditItemService::refreshSystemQuantity($auditItem);
                AuditItemService::attachInventories($auditItem);
                AuditItemService::blockInventories($auditItem);
            }

            $audit->status = Audit::STATUS_STARTED;
            $audit->saveOrFail();
        });
    }

    public static function approveAudit($audit, $employee)
    {
        if (!static::canManageAudit($audit, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to approve Audit ({$audit->id})");

        DB::transaction(function () use ($employee, $audit) {
            if (!static::canChangeAuditStatus($audit, Audit::STATUS_APPROVED))
                throw new ServiceException("Cannot approve Audit ({$audit->id})");

            foreach ($audit->auditItems()->notCancelled()->get() as $auditItem) {
                if (!AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_PENDING_ACTION))
                    throw new ServiceException();

                $systemQuantity = $auditItem->system_quantity;
                $actualQuantity = AuditItemService::actualQuantity($auditItem);
                $goodActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_GOOD);
                $badActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_BAD);

                $auditItem->status = $audit->type == Audit::TYPE_PRODUCT ? ($systemQuantity == $actualQuantity ? AuditItem::STATUS_RESOLVED : AuditItem::STATUS_PENDING_ACTION) : AuditItem::STATUS_RESOLVED;
                $auditItem->saveOrFail();

                if ($audit->type == Audit::TYPE_PRODUCT) {
                    if ($badActualQuantity > 0) {
                        $auditItemAction = new AuditItemAction();

                        $auditItemAction->audit_item_id = $auditItem->id;
                        $auditItemAction->action = AuditItemAction::ACTION_REPORT_BAD_STOCK;
                        $auditItemAction->quantity = $badActualQuantity;
                        $auditItemAction->taken_by = $employee->id;

                        $auditItemAction->saveOrFail();

                        $badInventories = $auditItem->inventories()
                            ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
                            ->take($badActualQuantity)
                            ->get();

                        foreach ($badInventories as $badInventory) {
                            $badInventory->status = FlatInventory::STATUS_AUDIT_BAD;
                            $badInventory->saveOrFail();

                            $auditItem->inventories()->sync([
                                $badInventory->id => ['audit_item_action_id' => $auditItemAction->id]
                            ], false);
                        }
                    }

                    $goodInventoryCount = min($systemQuantity, $goodActualQuantity);
                    $goodInventories = $auditItem->inventories()
                        ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
                        ->take($goodInventoryCount)
                        ->get();
                    foreach ($goodInventories as $goodInventory) {
                        $goodInventory->status = FlatInventory::STATUS_READY_FOR_SALE;
                        $goodInventory->saveOrFail();
                    }
                }
            }

            $audit->status = $audit->type == Audit::TYPE_PRODUCT ? Audit::STATUS_APPROVED : audit::STATUS_COMPLETED;
            $audit->saveOrFail();
        });
    }

    public static function completeAudit($audit, $employee)
    {
        if (!static::canManageAudit($audit, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to complete Audit ({$audit->id})");

        DB::transaction(function () use ($audit) {
            if (!static::canChangeAuditStatus($audit, Audit::STATUS_COMPLETED))
                throw new ServiceException("Cannot complete Audit ({$audit->id})");

            if ($audit->type == Audit::TYPE_PRODUCT) {
                foreach (self::distinctProductIds($audit) as $productId) {
                    foreach ($audit->auditItems()
                                 ->notCancelled()
                                 ->whereProduct($productId)
                                 ->get() as $auditItem) {

                        foreach ($auditItem->auditItemActions as $auditItemAction) {
                            switch ($auditItemAction->action) {
                                case AuditItemAction::ACTION_HOLD:
                                {
                                    $actualQuantity = AuditItemService::actualQuantity($auditItem);
                                    $systemQuantity = $auditItem->system_quantity;
                                    if ($systemQuantity > $actualQuantity) {
                                        $actionInventories = $auditItem->inventories()
                                            ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
                                            ->take($auditItemAction->quantity)
                                            ->get();

                                        foreach ($actionInventories as $actionInventory) {
                                            $actionInventory->status = FlatInventory::STATUS_AUDIT_HOLD;
                                            $actionInventory->saveOrFail();

                                            $auditItem->inventories()->sync([
                                                $actionInventory->id => ['audit_item_action_id' => $auditItemAction->id]
                                            ], false);
                                        }
                                    }

                                    break;
                                }
                                case AuditItemAction::ACTION_LOSS:
                                {
                                    $actionInventories = $auditItem->inventories()
                                        ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
                                        ->take($auditItemAction->quantity)
                                        ->get();

                                    foreach ($actionInventories as $actionInventory) {
                                        $actionInventory->status = FlatInventory::STATUS_AUDIT_LOSS;
                                        $actionInventory->saveOrFail();

                                        $auditItem->inventories()->sync([
                                            $actionInventory->id => ['audit_item_action_id' => $auditItemAction->id]
                                        ], false);
                                    }
                                    break;
                                }
//                                case AuditItemAction::ACTION_REPORT_BAD_STOCK:
//                                {
//                                    $badInventories = $auditItem->inventories()
//                                        ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
//                                        ->take($auditItemAction->quantity)
//                                        ->get();
//
//                                    foreach ($badInventories as $badInventory) {
//                                        $badInventory->status = FlatInventory::STATUS_AUDIT_BAD;
//                                        $badInventory->saveOrFail();
//
//                                        $auditItem->inventories()->sync([
//                                            $actionInventory->id => ['audit_item_action_id' => $auditItemAction->id]
//                                        ], false);
//                                    }
//                                    break;
//                                }
                                case AuditItemAction::ACTION_EXCESS:
                                {
                                    $badActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_BAD);
                                    $badInventoriesCount = max($badActualQuantity - $auditItem->system_quantity, 0);

                                    for ($i = 0; $i < $auditItemAction->quantity; $i++) {
                                        $inventory = new FlatInventory();
                                        $inventory->product_id = $auditItem->product_id;
                                        $inventory->storage_id = $auditItem->storage_id;
                                        $inventory->warehouse_id = $audit->warehouse_id;

                                        $inventory->source_type = 'audit_item_action';
                                        $inventory->source_id = $auditItemAction->id;

                                        $inventory->unit_cost = 0;
                                        $inventory->status = $i < $badInventoriesCount ? FlatInventory::STATUS_AUDIT_BAD : FlatInventory::STATUS_READY_FOR_SALE;

                                        $inventory->saveOrFail();

                                        $auditItem->inventories()->sync([
                                            $inventory->id => ['audit_item_action_id' => $auditItemAction->id]
                                        ], false);
                                    }

                                    break;
                                }
                            }
                        }
                    }

                    // auto balance
                    $blockedInventories = new Collection();
                    foreach ($audit->auditItems()
                                 ->notCancelled()
                                 ->whereProduct($productId)
                                 ->get() as $auditItem) {
                        $blockedInventories = $blockedInventories->merge(
                            $auditItem->inventories()
                                ->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)
                                ->get()
                        );
                    }

                    foreach ($audit->auditItems()
                                 ->notCancelled()
                                 ->whereProduct($productId)
                                 ->get() as $auditItem) {

                        $goodQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_GOOD);
                        $releasedGoodQuantity = $auditItem->inventories()->where('status', FlatInventory::STATUS_READY_FOR_SALE)->count();

                        foreach ($blockedInventories->take($goodQuantity - $releasedGoodQuantity) as $blockedInventory) {
                            $blockedInventory->storage_id = $auditItem->storage_id;
                            $blockedInventory->save();
                        }
                    }
                }
            }

            if ($audit->type == Audit::TYPE_STORAGE) {
                foreach (self::distinctStorageIds($audit) as $storageId) {
                    foreach ($audit->auditItems()
                                 ->notCancelled()
                                 ->whereStorage($storageId)
                                 ->get() as $auditItem) {

                        $systemQuantity = $auditItem->system_quantity;
                        $actualQuantity = AuditItemService::actualQuantity($auditItem);
                        $goodActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_GOOD);
                        $badActualQuantity = AuditItemService::actualQuantity($auditItem, AuditItemMeta::CONDITION_BAD);

                        if ($actualQuantity > $systemQuantity) {
                            if ($badActualQuantity > 0 && $goodActualQuantity == 0) {
                                foreach ($auditItem->inventories()->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)->take($badActualQuantity)->get() as $inventory) {
                                    $inventory->status = FlatInventory::STATUS_AUDIT_BAD;
                                    $inventory->saveOrFail();
                                }
                            }
                            if ($goodActualQuantity > 0 && $badActualQuantity == 0) {
                                foreach ($auditItem->inventories()->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)->take($goodActualQuantity)->get() as $inventory) {
                                    $inventory->status = FlatInventory::STATUS_READY_FOR_SALE;
                                    $inventory->saveOrFail();
                                }
                            }
                            continue;
                        }

                        foreach ($auditItem->inventories()->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)->take($goodActualQuantity)->get() as $inventory) {
                            $inventory->status = FlatInventory::STATUS_READY_FOR_SALE;
                            $inventory->saveOrFail();
                        }
                        foreach ($auditItem->inventories()->where('status', FlatInventory::STATUS_AUDIT_BLOCKED)->take($badActualQuantity)->get() as $inventory) {
                            $inventory->status = FlatInventory::STATUS_AUDIT_BAD;
                            $inventory->saveOrFail();
                        }
                    }
                }

            }

            foreach ($audit->auditItems()
                         ->notCancelled()
                         ->get() as $auditItem) {
                AuditItemService::unblockInventories($auditItem);
            }

            foreach ($audit->auditItems()->notCancelled()->get() as $auditItem) {
                $status = AuditItem::STATUS_RESOLVED;

                if ($audit->type == Audit::TYPE_STORAGE)
                    if ($auditItem->system_quantity != AuditItemService::actualQuantity($auditItem))
                        $status = AuditItem::STATUS_PENDING_ACTION;

                $auditItem->status = $status;
                $auditItem->save();
            }

            $audit->status = Audit::STATUS_COMPLETED;
            $audit->saveOrFail();
        });
    }

    public static function cancelAudit($audit, $employee)
    {
        if (!static::canManageAudit($audit, $employee))
            throw new ServiceException("Employee ({$employee->name}) does not have permission to cancel Audit ({$audit->id})");

        DB::transaction(function () use ($audit) {
            if (!static::canChangeAuditStatus($audit, Audit::STATUS_CANCELLED))
                throw new ServiceException("Cannot cancel Audit ({$audit->id})");

            foreach ($audit->auditItems as $auditItem) {
                AuditItemService::unblockInventories($auditItem);

                $auditItem->auditItemMetas()->delete();
                $auditItem->status = AuditItem::STATUS_CANCELLED;
                $auditItem->saveOrFail();
            }

            $audit->status = Audit::STATUS_CANCELLED;
            $audit->saveOrFail();
        });
    }

    public static function refreshSystemQuantities($audit)
    {
        DB::transaction(function () use ($audit) {
            foreach ($audit->auditItems()->notCancelled()->get() as $auditItem) {
                AuditItemService::refreshSystemQuantity($auditItem);
            }
        });
    }

    public static function listAuditItems($audit, $employee)
    {
        if (static::canManageAudit($audit, $employee)) {
            return $audit->items;
        }

        if (static::canViewAudit($audit, $employee)) {
            return $audit->items()
                ->where('assigned_to', $employee->id)
                ->get();
        }

        return null;
    }

    public static function distinctProductIds($audit)
    {
        return $audit->auditItems()
//            ->notCancelled()
            ->select(DB::raw('distinct product_id'))
            ->pluck('product_id')
            ->toArray();
    }

    public static function distinctStorageIds($audit)
    {
        return $audit->auditItems()
//            ->notCancelled()
            ->select(DB::raw('distinct storage_id'))
            ->pluck('storage_id')
            ->toArray();
    }

    public static function totalActualQuantity($audit, $item)
    {
        $auditItems = $audit->auditItems()->notCancelled();
        $audit->type == Audit::TYPE_PRODUCT
            ? $auditItems->whereProduct($item)
            : $auditItems->whereStorage($item);

        return $auditItems->get()->reduce(function ($totalActualQuantity, $auditItem) {
            return $totalActualQuantity + AuditItemService::actualQuantity($auditItem);
        }, 0);
    }

    public static function totalSystemQuantity($audit, $item)
    {
        $query = $audit->auditItems()->notCancelled();
        $query = $audit->type == Audit::TYPE_PRODUCT ? $query->whereProduct($item) : $query->whereStorage($item);
        $auditItems = $query->get();
        return $auditItems->reduce(function ($totalActualQuantity, $auditItem) {
            return $totalActualQuantity + AuditItemService::actualQuantity($auditItem);
        }, 0);
    }

    public static function totalUpdatedSystemQuantity($audit, $item)
    {
        $query = $audit->auditItems()->notCancelled();
        $query = $audit->type == Audit::TYPE_PRODUCT ? $query->whereProduct($item) : $query->whereStorage($item);
        $auditItems = $query->get();
        return $auditItems->reduce(function ($totalUpdatedSystemQuantity, $auditItem) {
            return $totalUpdatedSystemQuantity + AuditItemService::updatedSystemQuantity($auditItem);
        });
    }

    public static function checkAuditInventoryConflict($audit)
    {
        foreach ($audit->auditItems()->notCancelled()->get() as $auditItem) {
            $conflictedInventoriesCount = FlatInventory::where('warehouse_id', $audit->warehouse_id)
                ->where('product_id', $auditItem->product_id)
                ->where('storage_id', $auditItem->storage_id)
                ->whereIn('status', [
                    FlatInventory::STATUS_HOLD,
                    FlatInventory::STATUS_AUDIT_BLOCKED
                ])
                ->count();

            if ($conflictedInventoriesCount > 0)
                throw new ServiceException("Audit Inventory is in conflict.\nProductId: {$auditItem->product_id}\nStorageId: {$auditItem->storage_id}\nNo. of Items: {$conflictedInventoriesCount}");
        }
    }
}