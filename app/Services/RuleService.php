<?php

namespace Niyotail\Services;

use DateTime;
use Illuminate\Http\Request;
use Niyotail\Models\Rule;
use Niyotail\Models\Condition;
use Niyotail\Models\Expression;
use Niyotail\Models\RuleTarget;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Brand;

class RuleService extends Service
{
    public function create(Request $request)
    {
        return DB::transaction(function () use ($request) {
            $rule = new Rule();
            $rule->type = $request->type;
            $this->map($rule, $request);
            $rule->save();
            $this->saveConditions($rule, $request);
            $this->saveTargets($rule, $request);

            return $rule;
        });
    }

    public function update(Rule $rule, Request $request)
    {
        return DB::transaction(function () use ($rule, $request) {
            $this->map($rule, $request);
            $rule->save();
            $this->saveConditions($rule, $request);
            $this->saveTargets($rule, $request);

            return $rule;
        });
    }

    private function map(Rule $rule, Request $request)
    {
        $rule->name = $request->name;
        $rule->position = $request->position;
        $rule->should_stop = $request->should_stop;

        $rule->offer = $request->offer;
        $rule->source_quantity = $request->has('source_quantity') ?  $request->source_quantity : 1;
        $rule->target_quantity = $request->has('target_quantity') ? $request->target_quantity : 1;
        $rule->value = empty($request->offer_value) ? 0 : $request->offer_value;

        if ($rule->type != Rule::TYPE_PRICING) {
            $rule->max_offer = $request->max_offer;
            $rule->user_limit = $request->user_limit;
            $rule->total_limit = $request->total_limit;
        }
        $rule->description = null;
        $rule->status = $request->status;

        $rule->start_at = (DateTime::createFromFormat('d M, Y h:i A', $request->start_at))->format('Y-m-d H:i:s');
        $rule->end_at = empty($request->end_at) ? null : (DateTime::createFromFormat('d M, Y h:i A', $request->end_at))->format('Y-m-d H:i:s');
    }

    private function saveConditions(Rule $rule, Request $request)
    {
        $rule->conditions()->delete();
        foreach ($request->conditions as $conditionData) {
            $condition = new Condition();
            $rule->conditions()->save($condition);
            $this->saveExpressions($condition, $conditionData['expressions']);
        }
    }

    private function saveExpressions(Condition $condition, $expressionsData)
    {
        $expressions = [];
        foreach ($expressionsData as $expressionData) {
            if (isset($expressionData['values'])) {
                if ($expressionData['name'] == 'brand') {
                    $expressionData['values'] = $this->attachChildBrands($expressionData['values']);
                }
                foreach ($expressionData['values'] as $value) {
                    $expressions[] = $this->getExpression($expressionData['name'], $value, null);
                }
            } else {
                $expressions[] = $this->getExpression($expressionData['name'], $expressionData['min_value'], $expressionData['max_value']);
            }
        }

        $condition->expressions()->saveMany($expressions);
    }

    private function attachChildBrands($values)
    {
        $brands = $values;
        foreach ($values as $value) {
            $children = Brand::getChildren($value)->pluck('id')->toArray();
            $brands = array_merge($brands, $children);
        }
        $brands = array_unique($brands);
        return $brands;
    }

    private function getExpression($name, $value, $maxValue)
    {
        $expression = new Expression();
        $expression->name = $name;
        $expression->value = $value;
        $expression->max_value = $maxValue;

        return $expression;
    }

    private function saveTargets(Rule $rule, Request $request)
    {
        $targets = [];
        $rule->targets()->delete();
        if ($request->has('targets')) {
            foreach ($request->targets as $targetValue) {
                $ruleTarget = new RuleTarget();
                $ruleTarget->type = $request->target_type;
                $ruleTarget->value = $targetValue;
                $targets[] = $ruleTarget;
            }
        }
        $rule->targets()->saveMany($targets);
    }
}
