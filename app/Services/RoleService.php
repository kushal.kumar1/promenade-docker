<?php

namespace Niyotail\Services;

use Niyotail\Models\Role;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

class RoleService extends Service
{
    public function create(Request $request)
    {
        if ($this->isDuplicateEntry($request)) {
            return false;
        }
        $role=$this->processData($request, new Role());
        return $role->save();
    }

    public function update(Request $request)
    {
        $role=Role::find($request->id);
        if (empty($role)) {
            throw new ServiceException("Role doesn't exist!");
        }
        if ($this->isDuplicateEntry($request, $request->id)) {
            throw new ServiceException("Duplicate Entry!");
        }
        $role=$this->processData($request, $role);
        $permissions=$request->has('permissions')?$request->permissions:[];
        $role->permissions()->sync($permissions);
        return $role->save();
    }

    public function delete(Request $request)
    {
        $role=Role::find($request->id);
        if (empty($role)) {
            throw new ServiceException("Role doesn't exist !");
        }
        return $role->delete();
    }

    private function processData(Request $request, Role $role)
    {
        $role->name = $request->name;
        return $role;
    }

    public function isDuplicateEntry(Request $request, $id = null)
    {
        $employee = Role::where('name', $request->name)->where('id', '!=', $id)->first();
        if (!empty($employee)) {
            throw new ServiceException("Name Already Taken");
        }
        return false;
    }
}
