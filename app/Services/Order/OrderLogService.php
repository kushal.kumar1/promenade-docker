<?php

namespace Niyotail\Services\Order;


use Niyotail\Models\Order;
use Niyotail\Models\OrderLog;
use Illuminate\Support\Facades\Auth;

trait OrderLogService
{
    public function createLog(Order $order, $log)
    {
        $orderLog = new OrderLog();
        if(!is_array($log))
            $log=[$log];
        $orderLog->description = json_encode(['data' => $log]);
        $orderLog->order_id = $order->id;
        if($this->getAuthority() != self::AUTHORITY_SYSTEM) {
            $orderLog->employee_id = Auth::guard('admin')->check() ? Auth::guard('admin')->user()->id : null;
        }
        $orderLog->save();
    }

    public function createRemarks(Order $order, $request)
    {
        $this->createLog($order, $request->remarks);
    }
}