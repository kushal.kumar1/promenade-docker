<?php

namespace Niyotail\Services\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Niyotail\Events\Shipment\ShipmentCancelled;
use Niyotail\Events\Shipment\ShipmentDelivered;
use Niyotail\Events\Shipment\ShipmentDispatched;
use Niyotail\Events\Shipment\ShipmentGenerated;
use Niyotail\Events\Shipment\ShipmentMarkedRTO;
use Niyotail\Events\Shipment\ShipmentUpdated;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\File;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Models\Shipment;
use Niyotail\Services\InvoiceService;
use Niyotail\Services\PicklistService;
use Niyotail\Services\Service;
use Niyotail\Jobs\SendShipmentOtp;

class ShipmentService extends Service
{
    use OrderLogService;

    /**
     * @param Request $request
     * @return mixed
     * @throws ServiceException
     */
    public function updateBoxes(Request $request)
    {
        $this->checkAuthorisation(self::AUTHORITY_ADMIN);
        $shipment = Shipment::find($request->id);
        if (empty($shipment)) {
            throw new ServiceException("Shipment not found");
        }

        if ($shipment->status != Shipment::STATUS_READY_TO_SHIP) {
            throw new ServiceException("Shipment Cannot be Updated");
        }

        $shipment->boxes = $request->boxes;
        $shipment->save();
        return $shipment;
    }

    /**
     * Function to create shipments for picklist orders
     * Imp Point: Break orders into multiple shipments if shipments invoice is greater than 45K.
     * Performance Improvements: make the process async, right now lots is happening inside the DB transaction.
     * Shipment generated -> Order/Items status change -> Invoice generation -> Transaction inserted Store Ledger
     * @param Picklist $picklist
     * @param Order $order
     * @throws ServiceException
     */
    public function createFromPicklist(Picklist $picklist, Order $order)
    {
        $picklistItems = $order->picklistItems->where('status', PicklistItem::STATUS_PICKED);

        //Refer Imp Point in comments
        $picklistItemsChunks = $order->type == Order::TYPE_STOCK_TRANSFER ? [$picklistItems] : $this->partitionItems($picklistItems);
        $picklistService = new PicklistService();
        DB::transaction(function () use ($picklistItemsChunks, $picklist, $order, $picklistService) {
            foreach ($picklistItemsChunks as $picklistItemChunk) {
                $shipment = new Shipment();
                $shipment->status = Shipment::STATUS_GENERATED;
                $shipment->warehouse_id = $picklist->warehouse_id;
                $shipment->order()->associate($order);
                $shipment->picklist()->associate($picklist);
                $shipment->save();
                $orderItemIds = $picklistItemChunk->pluck('order_item_id')->toArray();
                $this->attachItems($shipment, $orderItemIds);
                $picklistItemIds = $picklistItemChunk->pluck('id')->toArray();
                $picklistService->updateItemsStatus($picklistItemIds, PicklistItem::STATUS_PROCESSED);
                event(new ShipmentGenerated($shipment));
            }
        });
    }

    /**
     * Function to partition items into chunks of specified breakpoints.
     * @param $items
     * @return array
     * @throws ServiceException
     */
    private function partitionItems($items)
    {
        $breakpoint = 45000;
        $sum = 0;
        $chunks = array();
        $chunk = collect([]);
        $items->each(function ($item, $key) use (&$sum, &$chunks, &$chunk, $breakpoint) {

            $itemTotal = $this->getItemTotal($item);
            $sum += $itemTotal;
            if ($sum < $breakpoint) $chunk->push($item);
            if ($sum == $breakpoint) {
                $chunk->push($item);
                $chunks[] = $chunk;
                $chunk = collect([]);
                $sum = 0;
            }
            if ($sum > $breakpoint) {
                $chunks[] = $chunk;
                $chunk = collect([]);
                $chunk->push($item);
                $sum = $this->getItemTotal($item);
            }
        });

        if ($chunk->isNotEmpty()) {
            $chunks[] = $chunk;
        }

        return $chunks;
    }

//    public function rejectRetryRto(Shipment $shipment)
//    {
//        return DB::transaction(function () use ($shipment) {
//            $this->updateStatus($shipment, Shipment::STATUS_DISPATCHED);
//        });
//    }

    /**
     * Function to get item total based on item object.
     * @param $item
     * @return mixed
     * @throws ServiceException
     */
    private function getItemTotal($item)
    {
        if ($item instanceof OrderItem) $total = $item->total;
        else if ($item instanceof PicklistItem) $total = $item->orderItem->total;
        else throw new ServiceException("Unidentified item passed to the partition function!");
        return $total;
    }

    /**
     * @param Shipment $shipment
     * @param array $orderItemIds
     */
    private function attachItems(Shipment $shipment, array $orderItemIds)
    {
        $shipmentItems = array();
        foreach ($orderItemIds as $orderItemId) {
            array_push($shipmentItems, [
                'order_item_id' => $orderItemId,
            ]);
        }
        $shipment->items()->createMany($shipmentItems);
    }

    /**
     * Function to create & deliver shipments for direct store purchases.
     * @param Order $order
     * @return mixed
     * @throws ServiceException
     */
    public function completeDirectPurchase(Order $order)
    {
        if ($order->warehouse_id != 1 && $order->status != Order::STATUS_CONFIRMED)
            throw new ServiceException("Cannot process order!");
        $order->loadMissing('items');
        $orderItems = $order->items->where('status', OrderItem::STATUS_PROCESSING);
        $itemsChunks = $this->partitionItems($orderItems);
        DB::transaction(function () use ($order, $orderItems, $itemsChunks) {
            foreach ($itemsChunks as $chunk) {
                //1. Create shipments
                $shipment = new Shipment();
                $shipment->status = Shipment::STATUS_GENERATED;
                $shipment->warehouse_id = $order->warehouse_id;
                $shipment->order()->associate($order);
                $shipment->save();
                $orderItemIds = $chunk->pluck('id')->toArray();
                $this->attachItems($shipment, $orderItemIds);
                //2. Generate Invoice
                (new InvoiceService())->setAuthority(OrderService::AUTHORITY_SYSTEM)->create($shipment);
                //3. Deliver Shipment
                $this->updateShipment($shipment, Shipment::STATUS_DELIVERED);
            }
        });
    }

    /**
     * Common function to update shipment status.
     * Events are classified into end status events like delivered, cancelled, rto & common update shipment status.
     * All order, order items related changes in event listeners.
     * @param Shipment $shipment
     * @param string $status
     * @param false $reason
     * @return mixed
     * @throws ServiceException
     */
    public function updateShipment(Shipment $shipment, string $status, $reason = false)
    {
        $shipment->load('orderItems');

        if (!$shipment->canUpdate() || !in_array($status, Shipment::getConstants("STATUS"))) {
            throw new ServiceException("Shipment cannot be updated!");
        }

        return DB::transaction(function () use ($shipment, $status, $reason) {
            switch ($status) {
                case Shipment::STATUS_DELIVERED:
                    $eventObject = new ShipmentDelivered($shipment);
                    break;
                case Shipment::STATUS_CANCELLED:
                    if (!$shipment->canCancel()) throw new ServiceException("Cannot cancel shipment!");
                    $eventObject = new ShipmentCancelled($shipment);
                    break;
                case Shipment::STATUS_RTO:
                    $eventObject = new ShipmentMarkedRTO($shipment);
                    break;
                case Shipment::STATUS_READY_TO_SHIP:
                    if (!in_array($shipment->status, [Shipment::STATUS_GENERATED, Shipment::STATUS_RETRY])){
                        throw new ServiceException("Shipment status change not allowed!");
                    }
                    $eventObject = new ShipmentUpdated($shipment);
                    break;
                case Shipment::STATUS_DISPATCHED:
//                    $shipment = $this->setOtp($shipment);
                    $eventObject = new ShipmentDispatched($shipment);
                    break;
                default:
                    $eventObject = new ShipmentUpdated($shipment);
                    break;
            }

            $oldShipment = clone $shipment;
            $shipment->status = $status;
            $shipment->save();
            $this->createShipmentLog($shipment, $oldShipment, $reason);
            event($eventObject);
            return $shipment;
        });
    }

    /**
     * Function to record changes in shipment and update in order logs
     * @param Shipment $shipment
     * @param Shipment $oldShipment
     * @param false $reason
     */
    private function createShipmentLog(Shipment $shipment, Shipment $oldShipment, $reason = false)
    {
        $data = [];
        $changed = array_diff($shipment->attributesToArray(), $oldShipment->attributesToArray());
        unset($changed['created_at']);
        unset($changed['updated_at']);
        foreach ($changed as $key => $value) {
            $data[] = "Shipment #$shipment->id : $key changed from $oldShipment[$key] to $value";
        }
        if (!empty($reason)) {
            $data[] = $reason;
        }
        $this->createLog($shipment->order, $data);
    }

    /**
     * Set Shipment OTP
     * @param Shipment $shipment
     * @return Shipment
     */
    public function setOtp(Shipment $shipment)
    {
        if (App::environment() != 'production') {
            $shipment->otp = "123456";
        } else {
            $shipment->otp = $this->generateUniqueShipmentOtp();
        }
        // $shipment->otp = "000000";
        $shipment->otp_sent_count = 1;
        $shipment->save();
        return $shipment;
    }

    /**
     * Generate Unique Shipment OTP
     * @return int
     */
    private function generateUniqueShipmentOtp(): int
    {
        $otp = generateOtp();
        if (!empty(Shipment::where('otp', $otp)->first())) {
            $this->generateUniqueShipmentOtp();
        }
        return $otp;
    }

    public function verifyOtp(Shipment $shipment, int $otp): bool
    {
        if ($shipment->otp != $otp) {
           throw new ServiceException('Invalid OTP');
        } else {
//            \Log::info('Shipment OTP for delivery submitted successfully. ID: '.$shipment->id.'  OTP: '.$otp);
            $shipment->otp = null;
            $shipment->otp_sent_count = null;
            $shipment->save();
        }
        return true;
    }

    public function resendOTP(Shipment $shipment): bool
    {
        if ($shipment->otp_sent_count < 5) {
            $shipment->otp_sent_count += 1;
            $shipment->updated_at = $shipment->updated_at;
            $shipment->save();
        } else {
            throw new ServiceException("You have reached maximum OTP limit. Please try after 1 hour !!");
        }
        $shipment->loadMissing('order.store', 'invoice');
        dispatch(new SendShipmentOtp($shipment));
        return true;
    }

    public function savePOD($file, Shipment $shipment)
    {
        $targetDirectory = public_path('media/shipments/' . $shipment->id);
        $pod = new File($file);
        $pod->setName(uniqid(). "." . $file->guessExtension());
        $pod->setDirectory($targetDirectory);
        $pod->save();

        $shipment->pod_name = $pod->getName();
        $shipment->save();

        $path = 'media/shipments/'.$shipment->id.'/'.$pod->getName();
        $s3Path = 'shipments/'.$shipment->id.'/'.$pod->getName();
        if(Storage::disk('public')->exists($path)) {
            $uploadedPOD = Storage::disk('public')->get($path);
            Storage::disk('s3')->put($s3Path, $uploadedPOD);
        }
    }
}
