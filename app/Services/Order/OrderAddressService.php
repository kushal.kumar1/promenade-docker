<?php

namespace Niyotail\Services\Order;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Order;
use Niyotail\Models\OrderAddress;
use Niyotail\Models\Pincode;
use Niyotail\Models\Store;

trait OrderAddressService
{
    public function addAddress(Order $order, Store $store)
    {
        $orderAddress = $this->updateAddressFromStore((new OrderAddress()), $store);
        $order->shippingAddress()->associate($orderAddress);
        $order->billingAddress()->associate($orderAddress);
        $order->save();
    }

    public function updateAddressFromStore(OrderAddress $address, Store $store)
    {
        $pincode = Pincode::where('pincode', $store->pincode)->first();
        if(empty($pincode)) throw new ServiceException("Pincode not available");
        $address->name = $store->name;
        $address->mobile = $store->contact_mobile;
        $address->address = $store->address;
        $address->landmark = $store->landmark;
        $address->pincode = $store->pincode;
        $address->lat = $store->lat;
        $address->lng = $store->lng;
        $address->city_id = $pincode->city_id;
        $address->save();
        return $address;
    }
}
