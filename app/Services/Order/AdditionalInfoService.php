<?php

namespace Niyotail\Services\Order;

use Niyotail\Models\AdditionalOrderInfo;
use Niyotail\Models\Order;
use Niyotail\Models\Employee;

trait AdditionalInfoService
{
    public function createAdditionalInfo(Order $order, $additionalInformation)
    {
        $data = (array)$additionalInformation->data;
        // if (array_key_exists('pos_order_id', $data) || array_key_exists('tsm_number', $data)) {
            $customerInfo = [];
            $additionalInfo = new AdditionalOrderInfo();
            $additionalInfo->pos_order_id = $data['pos_order_id'] ?? null;
            $additionalInfo->order_id = $order->id;
            if (!empty($data['tsm_number'])) {
                $mobile = $data['tsm_number'];
                $employee = Employee::where('mobile', $mobile)->first();
                $additionalInfo->tsm_employee_id = $employee->id ?? null;
                $additionalInfo->tsm_number = $mobile;
            }
            if(!empty($data['customer_name'])){
                $customerInfo['name'] = $data['customer_name'];
            }
            if(!empty($data['customer_mobile'])){
                $customerInfo['mobile'] = $data['customer_mobile'];
            }
            
            if(!empty($data['due_amount'])){
                $customerInfo['due_amount'] = $data['due_amount'];
            }

            if(array_key_exists('st_type', $data)){
                $additionalInfo->stock_transfer_type = $data['st_type'];
            }
            $additionalInfo->customer_info = (!empty($customerInfo)) ? json_encode($customerInfo) : null;
            $additionalInfo->save();
        // }
    }
}
