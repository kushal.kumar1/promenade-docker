<?php

namespace Niyotail\Services\Order;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Niyotail\Events\Order\OrderCancelled;
use Niyotail\Events\Order\OrderPlaced;
use Niyotail\Exceptions\CreditLimitExceededException;
use Niyotail\Exceptions\OrderItemsNotFoundException;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\PicklistItem;
use Niyotail\Models\Product;
use Niyotail\Models\Shipment;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\StoreOrderItem;
use Niyotail\Models\Tag;
use Niyotail\Services\Service;

class OrderService extends Service
{
    use OrderItemTaxService, OrderAddressService, OrderLogService, AdditionalInfoService;

    private OrderItemService $orderItemService;

    public function __construct(OrderItemService $orderItemService)
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * @throws CreditLimitExceededException
     * @throws \Exception
     */
    public function create(StoreOrder $storeOrder)
    {
        if ($storeOrder->status !== 'pending') {
//            throw new \Exception('Cannot convert to sale order. (' . $storeOrder->id . ')', 422);
            \Log::info('Cannot convert to sale order. (' . $storeOrder->id . ')');
            return $storeOrder->order;
        }

        $storeOrder->load('items.product', 'items.productVariant');

        $store = $storeOrder->store;
        if ($storeOrder->warehouse_id != 1 and $store->isCreditLimitExceeded($storeOrder->total)) {
            throw new CreditLimitExceededException("Credit limit exceeded!");
        }

        return DB::transaction(function () use ($storeOrder) {
            $data = $this->processData($storeOrder);
            $order = Order::create($data);
            if (!empty($storeOrder->tag)) {
                $this->setOrderTags($order, $storeOrder->tag);
            }
            $storeOrder->load('store');
            $this->addAddress($order, $storeOrder->store);
            $additionalInfo = null;
            if (!empty($storeOrder->additional_info)) {
                $additionalInfo = json_decode($storeOrder->additional_info);
                $this->createAdditionalInfo($order, $additionalInfo);
            }
            foreach ($storeOrder->items as $item) {
                $data = $this->processStoreOrderItem($item, $storeOrder->warehouse_id, $storeOrder->source, $storeOrder->is1KMallOrder($additionalInfo));
                $price = $item->price / $item->productVariant->quantity;
                $itemsToBeFullFilled = $data['products'];
                $inventories = $data['inventories'];
                if (!empty($inventories)) {
                    if ($order->source == Order::SOURCE_WHOLESALE_IMPORTER) {
                        $this->orderItemService->addItems($order, $itemsToBeFullFilled, $inventories, $price);
                    } else {
                        $this->orderItemService->addItems($order, $itemsToBeFullFilled, $inventories);
                    }
                }
            }
            $order->load('items');
            if ($order->items->isEmpty()) {
                throw new OrderItemsNotFoundException("Unable to convert store order $storeOrder->id. No Stock for items available");
            }
            $this->calculateItemsTotalAndTaxes($order);

            //associate sale order to purchase order
            $storeOrder->order()->associate($order);
            $storeOrder->status = StoreOrder::STATUS_PROCESSED;
            $storeOrder->save();

            $order->refresh();
            $this->createLog($order, 'Order Created by ' . $order->createdBy->name . ' (' . $order->created_by_type . ')');

            //Collect all totals
            $this->collectTotals($order);
            event(new OrderPlaced($order));
            return $order;
        });
    }

    private function processData(StoreOrder $storeOrder)
    {
        $data = $storeOrder->attributesToArray();
        unset($data['order_id']);
        unset($data['additional_info']);
        unset($data['tag']);
        //TODO::remove hardcoding of warehouse condition from complete codebase
        $prefix = $storeOrder->warehouse_id == 1 ? "SP-" : "NT-";
        if ($storeOrder->warehouse_id != 1 && $storeOrder->source == StoreOrder::SOURCE_WHOLESALE_IMPORTER) {
            $prefix = "WS-";
        }
        if ($storeOrder->type == StoreOrder::TYPE_STOCK_TRANSFER) {
            $prefix = "STO-";
        }
        $orderTypes = Order::getConstants('TYPE');
        $data['reference_id'] = $prefix . $storeOrder->store->id . "-" . round(microtime(true) * 1000);
        $data['payment_method'] = "cash";
        $data['currency'] = "INR";
        $data['status'] = Order::STATUS_CONFIRMED;
        $data['type'] = $orderTypes[$storeOrder->type];
        if ($storeOrder->source == StoreOrder::SOURCE_IMPORTER) {
            $data['source'] = Order::SOURCE_IMPORTER;
        }
        if ($storeOrder->source == StoreOrder::SOURCE_WHOLESALE_IMPORTER) {
            $data['source'] = Order::SOURCE_WHOLESALE_IMPORTER;
            $data['type'] = Order::TYPE_WHOLESALE;
        }
        return $data;
    }

    private function setOrderTags(Order $order, string $tag)
    {
        $tag = Tag::order()->where('name', $tag)->first();
        if (!empty($tag)) {
            $order->tags()->attach($tag);
        }
    }

    /**
     * Function to generate fulfillable items list basis order item inventory or group products inventory.'
     * Case 1: Item fulfilled by own stock
     * Case 2: Item partially fulfilled by own stock
     * Case 3: Item fulfilled by group stock partially or whole depending upon case 1 & case 2
     * @param StoreOrderItem $storeOrderItem
     * @param $warehouseId
     * @param $orderSource
     * @return Collection
     * @throws ServiceException
     */
    private function processStoreOrderItem(StoreOrderItem $storeOrderItem, $warehouseId, $orderSource, $is1KMallOrder = false): Collection
    {
        $storeOrderItem->loadMissing('storeOrder');
        $product = $storeOrderItem->product;
        $desiredStock = $storeOrderItem->productVariant->quantity * $storeOrderItem->quantity;
        $orderQuantity = $desiredStock;
        $unitVariant = $this->getUnitVariant($product);
        $availableStock = FlatInventory::getSaleableProductInventory($product, $warehouseId, $desiredStock);

        $toBeFulfilled = collect([]);

        //Case 1:
        if ($availableStock->count() >= $desiredStock) {
            $toBeFulfilled->push([
                'variant' => $unitVariant,
                'quantity' => $desiredStock,
            ]);
            return collect(['products' => $toBeFulfilled, 'inventories' => $availableStock]);
        } else if($is1KMallOrder) {
            $storeOrderId = $storeOrderItem->storeOrder->id;
            $availableQuantity = $availableStock->count();
            throw new OrderItemsNotFoundException("Unable to convert store order $storeOrderId. Not enough stock for Product: $product->id. Required ($desiredStock) Available($availableQuantity)");
        }

        // Case 2:
        if ($availableStock->count() < $desiredStock && $availableStock->count() != 0) {
            $toBeFulfilled->push([
                'variant' => $unitVariant,
                'quantity' => $availableStock->count(),

            ]);
            $desiredStock -= $availableStock->count();
        }
        // Case 3:
        if(!$product->is1KMall())
        {
            $groupProducts = Product::getGroupProductsWithStock($product, $warehouseId)->where('current_inventory', '>', 0);
            foreach ($groupProducts as $product) {
                $unitVariant = $this->getUnitVariant($product);
                $quantity = min($desiredStock, $product->current_inventory);
                $productGroupInventories = FlatInventory::getSaleableProductInventory($product, $warehouseId, $quantity);
                if ($productGroupInventories->count() != 0) {
                    $availableStock->push($productGroupInventories);
                    $availableQuantity = $productGroupInventories->count();
                    $toBeFulfilled->push([
                        'variant' => $unitVariant,
                        'quantity' => $availableQuantity
                    ]);
                    $desiredStock -= $availableQuantity;
                    if ($desiredStock == 0) break;
                }
            }
        }

        //If Order Source is importer and assigned stock percentage < 50
        if ($orderSource == StoreOrder::SOURCE_IMPORTER && $toBeFulfilled->isNotEmpty() && $orderQuantity >= 500) {
            $assignedPercentage = ($toBeFulfilled->sum('quantity') / $orderQuantity) * 100;
            if ($assignedPercentage < 50) {
                $toBeFulfilled = collect([]);
                $availableStock = collect([]);
            }
        }

        return collect(['products' => $toBeFulfilled, 'inventories' => $availableStock->flatten()]);
    }

    private function getUnitVariant(Product $product)
    {
        $unitVariant = $product->unitVariant;
        if (empty($unitVariant)) throw new ServiceException("Unit Variant doesn't exist for product " . $product->id);
        return $unitVariant;
    }

    private function calculateItemsTotalAndTaxes(Order $order)
    {
        foreach ($order->items as $item) {
            $this->createItemTaxes($item, $order->store->pincode);
            $this->orderItemService->collectTotals($item);
        }
    }


    //TODO:: to be removed, once agent app is out
//    public function assignAgent(Order $order, Agent $agent)
//    {
//        $order->created_by_type = 'agent';
//        $order->created_by_id = $agent->id;
//        $order->save();
//        $order->refresh();
//        $this->createLog($order, "Agent Assigned : " . $order->createdBy->name);
//        return $order;ee
//    }

    public function collectTotals(Order $order)
    {
        $order->loadMissing('items');
        $subTotal = $discount = $tax = 0;
        foreach ($order->items as $item) {
            $subTotal += $item->subtotal;
            $discount += $item->discount;
            $tax += $item->tax;
        }

        $order->subtotal = $subTotal;
        $order->discount = $discount;
        $order->tax = $tax;
        $order->total = $subTotal - $discount + $tax;
        $order->save();
    }

    //TODO:: Check if it is required. Discounting to be done at store order level.
//    public function updateItems(Order $order, Request $request)
//    {
//        $order->load('items');
//        $order->load('items.productVariant');
//        DB::transaction(function () use ($request, $order) {
//            foreach ($request->items as $sku => $item) {
//                $orderItems = $order->items->where('sku', $sku);
//                if ($orderItems->isEmpty()) {
//                    throw new ServiceException("Order Item Not Found!");
//                }
//                $oldDiscount = $orderItems->sum('discount');
//                $newDiscount = $item['discount'];
//
//                if ($newDiscount != $oldDiscount) {
//                    $discountSplits = splitAmount($newDiscount, $orderItems->sum('quantity'));
//                    $index = 1;
//                    foreach ($orderItems as $orderItem) {
//                        $orderItem->discount = $discountSplits[$index];
//                        $orderItem->save();
//                        $index++;
//                        //delete old taxes
//                        $orderItem->taxes()->delete();
//                        //calculate & save taxes
//                        $this->createItemTaxes($orderItem, $order->store->pincode);
//                        $this->orderItemService->collectTotals($orderItem);
//                    }
//                    $sku = $orderItems->first()->sku;
//                    $this->createLog($order, "Discount for SKU:$sku changed from $oldDiscount to $newDiscount");
//                }
//            }
//            $this->collectTotals($order);
//        });
//    }

    public function confirmOrder($order)
    {
        $this->updateStatus($order, Order::STATUS_CONFIRMED);
    }

    private function updateStatus($order, $status)
    {
        //check status update
        if (!in_array($status, Order::getConstants("STATUS"))) {
            throw new ServiceException("Invalid Status");
        }
        $order->status = $status;
        $order->save();
    }

    public function manifestOrder($order)
    {
        if ($order->status == Order::STATUS_MANIFESTING) {
            $this->updateStatus($order, Order::STATUS_MANIFESTED);
        }
    }

    public function orderInTransit(Order $order)
    {
        $this->updateStatus($order, Order::STATUS_TRANSIT);
    }

    public function afterShipmentUpdate(Order $order)
    {
        // cancelled state shipments == total shipments ? order = manifested
        $order->loadMissing('shipments');
        $totalShipmentsCount = $order->shipments->count();
        $cancelledShipmentsCount = $order->shipments->whereIn('status', SHIPMENT::STATE_CANCELLED)->count();
        $completeShipmentsCount = $order->shipments->whereIn('status', SHIPMENT::STATE_COMPLETE)->count();
        $noCancelledShipmentsCount = $totalShipmentsCount - $cancelledShipmentsCount;

        if ($totalShipmentsCount == $cancelledShipmentsCount) {
            $this->setAuthority(self::AUTHORITY_SYSTEM)->cancel($order->id, "All order shipments cancelled");
        }

        //complete state shipments == non cancelled shipments ? order = complete
        if ($completeShipmentsCount == $noCancelledShipmentsCount && $completeShipmentsCount != 0) {
            $this->orderCompleted($order);
        }
    }

    /**
     * @throws ServiceException
     */
    public function cancel($orderId, $reason)
    {
        $order = Order::with('items', 'store')->find($orderId);
        if ($this->getAuthority() != self::AUTHORITY_SYSTEM && !$order->canCancel()) {
            throw new ServiceException("Order can't be cancelled !");
        }

        DB::transaction(function () use ($reason, $order) {
            $order->status = Order::STATUS_CANCELLED;
            $order->cancellation_reason = $reason;
            $order->save();
            //If all the order items are not cancelled.
            $totalItemsCount = $order->items->count();
            $totalCancelledItems = $order->items->where('status', OrderItem::STATUS_CANCELLED)->count();
            if ($totalCancelledItems < $totalItemsCount) {
                $orderItems = $order->items->whereNotIn('status', [OrderItem::STATUS_CANCELLED, OrderItem::STATUS_UNFULFILLED]);
                $this->orderItemService->cancel($orderItems, $reason);
            }
            if ($order->picklistItems->isNotEmpty()) {
                if ($order->picklistItems->count() == $order->picklistItems->where('status', PicklistItem::STATUS_PENDING)->count()) {
                    foreach ($order->picklistItems as $picklistItem) {
                        $picklistItem->delete();
                    }
                    $picklist = $order->picklistItems->first()->picklist;
                    $totalOrders = $picklist->orderItems->pluck('order')->unique()->count();
                    //Delete Picklist if it has no more orders left!
                    if ($totalOrders == 0) {
                        $picklist->delete();
                    }
                }
            }
            event(new OrderCancelled($order));
        });
        return $order;
    }

    public function orderCompleted(Order $order)
    {
        $this->updateStatus($order, Order::STATUS_COMPLETED);
    }

    public function afterPicklistUpdate(Order $order)
    {
        $totalPicklistItems = $order->picklistItems->count();
        $totalPicklistItemsPicked = $order->picklistItems->where('status', PicklistItem::STATUS_PICKED)->count();
        $totalPicklistItemsSkipped = $order->picklistItems->where('status', PicklistItem::STATUS_SKIPPED)->count();
        if ($totalPicklistItems == $totalPicklistItemsSkipped) {
            $this->setAuthority(self::AUTHORITY_SYSTEM)->cancel($order->id, "Order cancelled by picklist!");
        }
        if (($totalPicklistItems == $totalPicklistItemsPicked + $totalPicklistItemsSkipped) && $totalPicklistItemsPicked != 0) {
            $this->orderBilled($order);
        }
    }

    public function orderBilled(Order $order)
    {
        $this->updateStatus($order, Order::STATUS_BILLED);
    }

    /**
     * ----------------------------------------------------
     * New functions added for bulk updates,
     * TODO: to be moved into a seperate file later
     * -----------------------------------------------------
     **/

    public function manifestOrders(array $orderIds)
    {
        $this->switchStatus($orderIds, Order::STATUS_MANIFESTING, Order::STATUS_MANIFESTED);
    }

    private function switchStatus(array $orderIds, $fromStatus, $toStatus)
    {
        Order::where('status', $fromStatus)
            ->whereIn('id', $orderIds)
            ->update([
                'status' => $toStatus
            ]);
    }
}
