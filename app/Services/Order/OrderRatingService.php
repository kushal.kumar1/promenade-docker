<?php

namespace Niyotail\Services\Order;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\OrderRating;
use Niyotail\Services\Service;

class OrderRatingService extends Service
{
    public function create(Request $request)
    {
        if (!$request->rating) {
            throw new ServiceException('Rating must be specified');
        } else if ($request->rating < 0 || $request->rating > 10) {
            throw new ServiceException('Rating must be between 0 and 10');
        } else if (!$request->order_id) {
            throw new ServiceException('Rating must have an associated order');
        }
        $orderRating = new OrderRating();
        $orderRating->order_id = $request->order_id;
        $orderRating->type = OrderRating::TYPE_DELIVERY;
        $orderRating->rating = $request->rating;
        $orderRating->save();
        return $orderRating;
    }
}
