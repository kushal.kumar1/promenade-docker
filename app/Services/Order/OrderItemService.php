<?php

namespace Niyotail\Services\Order;

use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\OrderItemsNotFoundException;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Inventory\Actions\BulkSaleCancellation;
use Niyotail\Helpers\Inventory\Actions\ReturnAction;
use Niyotail\Helpers\Inventory\Actions\SaleAction;
use Niyotail\Helpers\Inventory\Actions\SaleCancellationAction;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Inventory;
use Niyotail\Models\InventoryTransaction;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\OrderItemFlatInventory;
use Niyotail\Models\OrderItemInventory;
use Niyotail\Models\Product;
use Niyotail\Models\PurchaseInvoiceItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Service;


class OrderItemService extends Service
{
    use OrderItemTaxService;

    public function addItems($order, $items, $inventories, $price = null)
    {
        $orderItems = [];
        if($order->type == Order::TYPE_STOCK_TRANSFER){
            $price = $this->getItemBaseCost($inventories->first());
        }
        foreach ($items as $item) {
            for ($i = 1; $i <= $item['quantity']; $i++) {
                $orderItems [] = [
                    'product_id' => $item['variant']->product->id,
                    'sku' => $item['variant']->sku,
                    'variant' => $item['variant']->value,
                    'mrp' => $item['variant']->mrp,
                    'price' => empty($price) ? $item['variant']->price : $price,
                    'quantity' => 1,
                    'subtotal' => 0,
                    'discount' => 0,
                    'tax' => 0,
                    'total' => 0,
                    'source' => 'manual',
                    'status' => OrderItem::STATUS_PROCESSING,
                    'order_id' => $order->id,
                ];
            }
        }
        foreach (array_chunk($orderItems, 500) as $chunkedOrderItems) {
            OrderItem::insert($chunkedOrderItems);
        }
        $this->updateFlatInventories($inventories, $order);
    }

    private function updateFlatInventories($inventories, $order)
    {
        $orderItemFlatInventories = [];
        $inventoriesToBeMarkedAsSold = collect([]);
        $orderItems = $order->items()->whereDoesntHave('itemInventory')->get();
        foreach ($orderItems as $orderItem) {
            $inventory = $inventories->whereNotIn('id', $inventoriesToBeMarkedAsSold->toArray())
                ->where('status', FlatInventory::STATUS_READY_FOR_SALE)
                ->where('product_id', $orderItem['product_id'])->first();
//            if (empty($inventory)) {
//                throw new OrderItemsNotFoundException("Unable to convert store order. Required Stock for items not available for warehouse:: ".$order['warehouse_id']." ProductId::".$orderItem['product_id']);
//            }
            $inventoriesToBeMarkedAsSold->push($inventory->id);
            $orderItemFlatInventories [] = [
                'order_item_id' => $orderItem->id,
                'flat_inventory_id' => $inventory->id,
            ];
        }
        (new FlatInventoryService())->markInventoriesSold($inventoriesToBeMarkedAsSold->toArray());
        foreach (array_chunk($orderItemFlatInventories, 500) as $chunkedOrderItemFlatInventories) {
            OrderItemFlatInventory::insert($chunkedOrderItemFlatInventories);
        }
    }

    public function collectTotals($orderItem)
    {
        $totalTax = 0;
        foreach ($orderItem->taxes as $tax) {
            $totalTax += $tax->amount;
        }
        $subTotal = $orderItem->price * $orderItem->quantity - $totalTax;
        $orderItem->tax = $totalTax;
        $orderItem->subtotal = $subTotal;
        $orderItem->total = $subTotal - $orderItem->discount + $totalTax;
        $orderItem->save();
    }

    /**
     * @throws ServiceException
     */
    public function cancel($orderItems, $reason)
    {
        $orderItems->loadMissing('itemInventory');
        $billedInventories = $orderItems->where('status', OrderItem::STATUS_BILLED)->pluck('itemInventory.flat_inventory_id');
        $preBilledInventories = $orderItems->whereIn('status', [OrderItem::STATUS_PROCESSING, OrderItem::STATUS_MANIFESTED])->pluck('itemInventory.flat_inventory_id');
        $flatInventoryService = new FlatInventoryService();
        foreach ($orderItems->chunk(5000) as $items) {
            $this->massStatusUpdate($items->pluck('id')->toArray(), OrderItem::STATUS_CANCELLED, $reason);
        }
        if ($billedInventories->isNotEmpty()) {
            $flatInventoryService->markGoodInventories($billedInventories->toArray(), FlatInventory::STATUS_PUTAWAY);
        }

        if ($preBilledInventories->isNotEmpty()) {
            $flatInventoryService->markGoodInventories($preBilledInventories->toArray(), FlatInventory::STATUS_READY_FOR_SALE);
        }
    }

    /**
     * Function to bulk update order item status
     * TODO: Add status change validations for items as current status
     * @param array $orderItemIds
     * @param $status
     * @param null $cancellationReason
     * @throws ServiceException
     */
    public function massStatusUpdate(array $orderItemIds, $status, $cancellationReason = null)
    {
        if (!in_array($status, OrderItem::getConstants("STATUS"))) {
            throw new ServiceException("Invalid Status");
        }
        $updates = ['status' => $status];
        if (!empty($cancellationReason)) {
            $updates['cancellation_reason'] = $cancellationReason;
        }
        $chunkedOrderItemIds = array_chunk($orderItemIds, 100);
        foreach ($chunkedOrderItemIds as $ids) {
            DB::transaction(function () use ($ids, $updates) {
                OrderItem::whereIn('id', $ids)->update($updates);
            });
        }
    }

    public function rto(array $orderItemIds)
    {
        $orderItems = OrderItem::with('itemInventory')
            ->where('status', OrderItem::STATUS_RTO)
            ->whereIn('id', $orderItemIds)
            ->get();
        $rtoInventories = $orderItems->pluck('itemInventory.flat_inventory_id');
        if($rtoInventories->isEmpty()) throw new ServiceException("Flat Inventory doesn't exist");
        if ($rtoInventories->isNotEmpty()) {
            $flatInventoryService = new FlatInventoryService();
            $flatInventoryService->markGoodInventories($rtoInventories->toArray(), FlatInventory::STATUS_PUTAWAY);
        }
    }


    /**
     * @throws ServiceException
     */
    public function markReturn($returnItems)
    {
        $damagedReasons = ['Damaged product','Bad Quality.'];
        $expiredReasons = ['Near expiry product(s)','Expired product(s)'];
        $damagedReturns = $returnItems->whereIn('reason', $damagedReasons);
        $expiredReturns = $returnItems->whereIn('reason', $expiredReasons);
        $goodReturns = $returnItems->whereNotIn('reason', array_merge($damagedReasons, $expiredReasons));
        $flatInventoryService = new FlatInventoryService();
        if ($goodReturns->isNotEmpty()) {
            $inventoriesToBeMarkedGood = $goodReturns->pluck('orderItem.itemInventory.flat_inventory_id')->toArray();
            $flatInventoryService->markGoodInventories($inventoriesToBeMarkedGood, FlatInventory::STATUS_PUTAWAY);
        }
        if ($damagedReturns->isNotEmpty()) {
            $inventoriesToBeMarkedAsDamaged = $damagedReturns->pluck('orderItem.itemInventory.flat_inventory_id')->toArray();
            $flatInventoryService->markBadInventories($inventoriesToBeMarkedAsDamaged, FlatInventory::STATUS_DAMAGED);
        }
        if ($expiredReturns->isNotEmpty()) {
            $inventoriesToBeMarkedAsExpired = $expiredReturns->pluck('orderItem.itemInventory.flat_inventory_id')->toArray();
            $flatInventoryService->markBadInventories($inventoriesToBeMarkedAsExpired, FlatInventory::STATUS_EXPIRED);
        }
        $this->massStatusUpdate($returnItems->pluck('order_item_id')->toArray(), OrderItem::STATUS_RETURNED);
    }

    public function markDelivered(OrderItem $orderItem)
    {
        $this->updateStatus($orderItem, OrderItem::STATUS_DELIVERED);
    }

    public function updateStatus($orderItem, $status)
    {
        //check status update
        if (!in_array($status, OrderItem::getConstants("STATUS"))) {
            throw new ServiceException("Invalid Status");
        }
        $orderItem->status = $status;
        $orderItem->save();
    }

    public function markPendingReturn(OrderItem $orderItem)
    {
        $this->updateStatus($orderItem, OrderItem::STATUS_PENDING_RETURN);
    }


    /**
     * @param $orderItem
     * @throws ServiceException
     */
    private function deductInventory($orderItem)
    {
        //TODO:: support for other variants, remove once we are confirmed everything is moved to unit variant
        $orderItem->loadMissing('productVariant');
        $deductCount = $orderItem->quantity * $orderItem->productVariant->quantity;
        $inventories = Inventory::getAvailableInventories($orderItem);
        $totalInventory = $inventories->isNotEmpty() ? $inventories->sum('quantity') : 0;

        if ($totalInventory == 0 || $totalInventory < $deductCount) {
            throw new ServiceException($orderItem->productVariant->product->name . ' (' . $orderItem->productVariant->sku . ") Out of Stock");
        }

        $orderItemInventory = array();
        foreach ($inventories as $inventory) {
            if ($inventory->quantity >= $deductCount) {
                $orderItemInventory[] = $this->getOrderItemInventory($inventory, $deductCount);
                (new SaleAction($inventory, $deductCount, $orderItem))->commit();
                $deductCount = 0;
            } else {
                $orderItemInventory[] = $this->getOrderItemInventory($inventory, $inventory->quantity);
                (new SaleAction($inventory, $inventory->quantity, $orderItem))->commit();
                $deductCount -= $inventory->quantity;
            }

            if ($deductCount == 0) {
                break;
            }
        }
        //TODO: remove storing order item inventories as the relation is being stored in inventory transactions
        $orderItem->inventories()->saveMany($orderItemInventory);
    }

    private function getOrderItemInventory(Inventory $inventory, $quantity)
    {
        return new OrderItemInventory([
            'quantity' => $quantity,
            'batch_number' => $inventory->batch_number,
            'unit_cost_price' => $inventory->unit_cost_price
        ]);
    }

    private function cancelOldStock($orderItem)
    {
        $inventories = $orderItem->inventories;
        foreach ($inventories as $inventory) {
            $quantity = $inventory->quantity;
            $inventoryRow = Inventory::where('batch_number', $inventory->batch_number)->where('warehouse_id', 2)->first();
            if (empty($inventoryRow)) {
                \Log::error("inventory row not found");
                die;
            }
            (new SaleCancellationAction($orderItem, $quantity, $inventoryRow))->commit();
        }
    }

    private function increaseInventory($orderItem, $returnItem = null)
    {
        $orderItem->loadMissing('inventories', 'product', 'order');
        $product = $orderItem->product;

        foreach ($orderItem->inventories as $itemInventory) {
            $inventory = Inventory::where([
                'product_id' => $product->id,
                'warehouse_id' => $orderItem->order->warehouse_id,
                'batch_number' => $itemInventory->batch_number,
                'type' => 'regular'
            ])->first();

            if (empty($inventory))
                continue;

            // increase by number of exact returned units
            $quantity = !empty($returnItem) ? $returnItem->units : $itemInventory->quantity;
            (new ReturnAction($inventory, $quantity, $orderItem))->commit();

            //Temp:: make product active if it is inactive
            if ($product->status == 0) {
                $product->status = Product::STATUS_ACTIVE;
                $product->save();
            }
        }
    }

    private function getItemBaseCost(FlatInventory $flatInventory)
    {
        if(!empty($flatInventory->unit_cost)){
            return $flatInventory->unit_cost;
        }
        $purchaseInvoiceItem = PurchaseInvoiceItem::where('product_id', $flatInventory->product_id)
            ->orderBy('id', 'desc')->first();
        return $purchaseInvoiceItem->base_cost;
    }
}
