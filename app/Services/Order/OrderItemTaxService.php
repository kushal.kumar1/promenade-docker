<?php

namespace Niyotail\Services\Order;

use Niyotail\Helpers\TaxHelper;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Pincode;

trait OrderItemTaxService
{
    public function createItemTaxes(OrderItem $item, $storePincode)
    {
        $pincode = Pincode::with('city.state')->where('pincode', $storePincode)->first();
        $sourceStateId = $item->order->warehouse->city->state_id;
        $supplyState = $pincode->city->state;
        $taxes = TaxHelper::getTaxes($item->product->tax_class_id, $item->mrp, $sourceStateId, $supplyState->id, $supplyState->country_id);

        $orderItemTaxes = [];
        if (!empty($taxes)) {
            $totalTaxRate = $taxes->sum('percentage');
            $additionalTax = $taxes->sum('additional_charges_per_unit') * $item->quantity * $item->productVariant->quantity;
            $taxAmount = inclusiveTaxAmount($item->price * $item->quantity - $item->discount, $totalTaxRate, $additionalTax);
            $priceExcludingTax = $item->price * $item->quantity - $item->discount - $taxAmount;
            foreach ($taxes as $tax) {
                $additionalCharges = $tax->additional_charges_per_unit * $item->quantity * $item->productVariant->quantity;
                $orderItemTaxes[] = $this->getItemTax($tax, $priceExcludingTax, $additionalCharges);
            }
        }
        $item->taxes()->createMany($orderItemTaxes);
    }

    private function getItemTax($tax, $priceExcludingTax, $additionalCharges)
    {
        if ($tax->percentage) {
            return [
                'name' => $tax->name,
                'percentage' => $tax->percentage,
                'amount' => (($priceExcludingTax * $tax->percentage) / 100) + $additionalCharges
            ];
        }

    }
}
