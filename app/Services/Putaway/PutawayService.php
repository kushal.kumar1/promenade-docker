<?php

namespace Niyotail\Services\Putaway;

use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Employee;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Services\InventoryMigrationService;
use Niyotail\Services\Service;

class PutawayService extends Service
{
    use PutawayItemService;
    /**
     * @param int $warehouseId
     * @param Employee $employee
     * @return Putaway
     */
    public function create(int $warehouseId, Employee $employee, $migrationId = null): Putaway
    {
        $putaway = new Putaway();
        $putaway->warehouse_id = $warehouseId;
        $putaway->status = Putaway::STATUS_OPEN;
        $putaway->employee_id = $employee->id;
        $putaway->inventory_migration_id = $migrationId;
        $putaway->save();
        return $putaway;
    }

    /**
     * @param Putaway $putaway
     * @return Putaway
     * @throws ServiceException
     */
    public function start(Putaway $putaway): Putaway
    {
        if ($putaway->status != Putaway::STATUS_OPEN) throw new ServiceException("Putaway cannot be started! ");
        $putaway->status = Putaway::STATUS_STARTED;
        $putaway->save();
        return $putaway;
    }

    /**
     * @param Putaway $putaway
     * @return Putaway
     * @throws ServiceException
     */
    public function close(Putaway $putaway): Putaway
    {
        //TODO Can close logic to be revisited again
        if (!$putaway->canClose()) throw new ServiceException("Putaway cannot be closed! ");
        $putaway->status = Putaway::STATUS_CLOSED;
        $putaway->save();
        $putaway->loadMissing('migration');
        if (!empty($putaway->migration)) {
            (new InventoryMigrationService())->close($putaway->migration);
        }
        return $putaway;
    }
}
