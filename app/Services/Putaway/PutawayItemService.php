<?php

namespace Niyotail\Services\Putaway;

use Illuminate\Support\Facades\DB;
use Niyotail\Events\Putaway\PutawayItemPlaced;
use Niyotail\Events\Putaway\PutawayItemAdded;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;
use Niyotail\Models\Storage;
use Niyotail\Services\FlatInventoryService;

trait PutawayItemService
{
    /**
     * @param PutawayItem $item
     * @param $storageId
     * @return mixed
     * @throws ServiceException
     */
    public function assignStorageToItem(PutawayItem $item, $storageId)
    {
        if ($item->putaway->status != Putaway::STATUS_STARTED) {
            throw new ServiceException("Cannot assign storage, when putaway is {$item->putaway->status}!");
        }
        return DB::transaction(function () use ($item, $storageId) {
            $item->storage_id = $storageId;
            $item->placed_quantity = $item->quantity;
            $item->save();
            event(new PutawayItemPlaced($item));
            // (new FlatInventoryService())->updatePutawayItem($item);
            return $item;
        });
    }

    /**
     * @param PutawayItem $putawayItem
     * @return bool|null
     * @throws ServiceException
     */
    public function removeItem(PutawayItem $putawayItem): ?bool
    {
        if ($putawayItem->putaway->status != Putaway::STATUS_OPEN) {
            throw new ServiceException("Cannot delete item! Putaway has been {$putawayItem->putaway->status}!");
        }
        return $putawayItem->delete();
    }

    /**
     * @param Putaway $putaway
     * @param int $productId
     * @param int $quantity
     * @param InventoryMigrationItem|null $inventoryMigrationItem
     * @return PutawayItem
     */
    public function putItem(Putaway $putaway, int $productId, int $quantity): PutawayItem
    {
        return DB::transaction(function () use ($putaway, $productId, $quantity) {
            if ($putaway->status != Putaway::STATUS_OPEN) {
                throw new ServiceException("Cannot add the item! Putaway has been $putaway->status!");
            }
            if (!$this->canAddProductInPutaway($putaway, $productId, $quantity)) {
                throw new ServiceException('Quantity greater than available quantity');
            }

            $putawayItem = new PutawayItem();
            $putawayItem->product_id = $productId;
            $putawayItem->quantity = $quantity;
            $putaway->items()->save($putawayItem);
            event(new PutawayItemAdded($putawayItem));
            return $putawayItem;
        });
    }

    /**
     * @param Putaway $putaway
     * @param int $productId
     * @param int $quantity
     * @return bool
     */
    private function canAddProductInPutaway(Putaway $putaway, int $productId, int $quantity): bool
    {
        $whereCondition = ['status' => FlatInventory::STATUS_PUTAWAY, 'product_id' => $productId, 'warehouse_id' => $putaway->warehouse_id];
        $count = FlatInventory::where($whereCondition)->limit($quantity)->count();
        $itemsInOpenPutaway = PutawayItem::whereHas('putaway', function ($putawayQuery) use ($putaway) {
            $putawayQuery->where('status', '!=', Putaway::STATUS_CLOSED)->where('warehouse_id', $putaway->warehouse_id);
        })->where('product_id', $productId)->whereNull('placed_quantity')->get();

        if (($count - $itemsInOpenPutaway->sum('quantity')) < $quantity) {
            return false;
        }
        return true;
    }

    public function addItem(Putaway $putaway, $request): PutawayItem
    {
        if ($putaway->status != Putaway::STATUS_OPEN) {
            throw new ServiceException("Cannot add the item! Putaway has been $putaway->status!");
        }

        $putawayItem = new PutawayItem();
        $putawayItem->product_id = $request->product_id;
        $putawayItem->quantity = $request->quantity;
        $putawayItem->storage_id = $request->destination_storage_id;
        $putawayItem->inventory_migration_item_id = $request->migration_item_id;
        if (!empty($request->remarks)) {
          $putawayItem->from_remarks = $request->remarks;
        }
        $putaway->items()->save($putawayItem);
        event(new PutawayItemAdded($putawayItem));
        return $putawayItem;
    }

    /**
     * @param int $employeeId
     * @param int $putawayItemId
     * @param int $quantity
     * @return PutawayItem
     * @throws ServiceException
     */
    public function updatePutawayItem(int $employeeId, int $putawayItemId, int $quantity): PutawayItem
    {
        $putawayItem = PutawayItem::with('putaway', 'migrationItem')->find($putawayItemId);
        if (empty($putawayItem)) {
            throw new ServiceException('Item not found!');
        }

        if ($putawayItem->putaway->employee_id != $employeeId) {
            throw new ServiceException('Item not found!');
        }

        if ($putawayItem->putaway->status != Putaway::STATUS_OPEN) {
            throw new ServiceException('Item cannot be updated as status is not open!');
        }

        if ($quantity != 0) {
            $putawayItem->quantity = $quantity;
            $putawayItem->save();
        } else {
            $putawayItem = $this->removeItem($putawayItem);
        }
        event(new PutawayItemAdded($putawayItem));
        return $putawayItem;
    }

    public function placeItem(PutawayItem $putawayItem, int $placedQuantity, int $destinationStorageId)
    {
        if ($putawayItem->putaway->status != Putaway::STATUS_STARTED) {
            throw new ServiceException('Can not place item without starting the migration activity!');
        }
        if (!empty($putawayItem->placed_quantity)) {
            throw new ServiceException('Item already placed!');
        }
        if ($putawayItem->quantity < $placedQuantity) {
            throw  new ServiceException('Placed Quantity cannot be greater than assigned item quantity');
        }

        $newPutawayItem = null;
        if (($putawayItem->quantity > $placedQuantity)) {
            $newPutawayItem = $putawayItem->replicate();
            $newPutawayItem->storage_id = null;
            $newPutawayItem->placed_quantity = null;
            $newPutawayItem->quantity = $putawayItem->quantity - $placedQuantity;
            $newPutawayItem->save();
            //Free the quantity from putaway_item_inventories and place in this new putaway item
            event(new PutawayItemAdded($newPutawayItem, $putawayItem));
        }

        $putawayItem->placed_quantity = $placedQuantity;
        $putawayItem->storage_id = $destinationStorageId;
        $putawayItem->save();
        event(new PutawayItemPlaced($putawayItem));
        return $newPutawayItem;
    }
}
