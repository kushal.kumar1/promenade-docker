<?php

namespace Niyotail\Services;

use Niyotail\Models\Inventory;
use Niyotail\Models\Product;
use Niyotail\Models\ProductVariant;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

class ProductVariantService extends Service
{
    public function create($productId, Request $request)
    {
        if ($this->isDirtyData($request)) {
            throw new ServiceException("SKU: $request->sku already exist !");
        }
        if (!$this->isMiniumumPriceValid($request)) {
            throw new ServiceException("Difference between minimum price and price can not be less than 15% !");
        }
        $productVariant = new ProductVariant();
        $productVariant->product_id = $productId;
        return $this->processData($productVariant, $request);
    }

    public function update(Request $request)
    {
        $variant = ProductVariant::withTrashed()->find($request->id);
        if (empty($variant)) {
            throw new ServiceException("Product Variant doesn't exist !");
        }
        if ($this->isDirtyData($request, $variant)) {
            throw new ServiceException("SKU: $variant->sku already exist !");
        }
        if (!$this->isMiniumumPriceValid($request)) {
            throw new ServiceException("Difference between minimum price and price can not be less than 15% !");
        }
        return $this->processData($variant, $request);
    }

    public function delete($id)
    {
        $variant = ProductVariant::find($id);
        if (empty($variant)) {
            throw new ServiceException("Product Variant doesn't exist !");
        }
        return $variant->delete();
    }

    public function restore($id)
    {
        $variant = ProductVariant::onlyTrashed()->find($id);
        if (empty($variant)) {
            throw new ServiceException("Product Variant doesn't exist !");
        }
        return $variant->restore();
    }

    private function processData(ProductVariant $productVariant, $request)
    {
        $productVariant->sku = isset($request->sku) ? $request->sku : $productVariant->sku;
        $productVariant->value = isset($request->value) ? $request->value : $productVariant->value;
        // $productVariant->uom = isset($request->uom)?  $request->uom : $productVariant->uom;
        $productVariant->uom = 'pc';
        $productVariant->quantity = isset($request->quantity) ? $request->quantity : $productVariant->quantity;
        $productVariant->mrp = isset($request->mrp) ?  $request->mrp : $productVariant->mrp;
        $productVariant->price = isset($request->price) ?  $request->price : $productVariant->price;
        $productVariant->min_price = isset($request->min_price) ?  $request->min_price : ($productVariant->min_price == 0 ? $productVariant->price : $productVariant->min_price);
        $productVariant->max_price = isset($request->max_price) ?  $request->max_price : ($productVariant->max_price == 0 ? $productVariant->price : $productVariant->max_price);
        $productVariant->moq = isset($request->moq) ?  $request->moq : $productVariant->moq;
        $productVariant->save();
        return $productVariant;
    }

    private function isDirtyData($request, ProductVariant $productVariant=null)
    {
        $query = ProductVariant::where('sku', $request->sku);
        if (!empty($productVariant)) {
            $query->where('id', '!=', $productVariant->id);
        }

        $productVariant = $query->first();
        if (empty($productVariant)) {
            return false;
        }
        return true;
    }

    private function isMiniumumPriceValid($request) {
        if ($request->min_price < (float) $request->price * 0.85) {
            return false;
        }
        return true;
    }
}
