<?php

namespace Niyotail\Services;

use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorBankAccount;
use Illuminate\Http\Request;
use Niyotail\Models\VendorMarketerMap;
use Niyotail\Models\VendorsAddresses;

/**
 * Class VendorService
 *
 * @package Niyotail\Services
 *
 */
class VendorService extends Service
{
    public function create (Request $request)
    {
        $vendor = Vendor::create($request->all());
        
        return $vendor;
    }

    public function update(Request $request, $id)
    {
        $vendor = Vendor::find($id);
        $vendor->update($request->all());

        return $vendor;
    }

    public function createAddress ($request, $id)
    {
        $vendor = Vendor::find($id);
        $vendor->addresses()->create([
            'name' => $request->name,
            'address' => $request->address,
            'pincode' => $request->pincode,
            'city' => $request->city,
            'state' => $request->state,
            'gstin' => $request->gstin,
            'state_code' => $request->state_code,
            'country' => 'India'
        ]);
    }

    public function deleteAddress ($id, $addressId)
    {
        $vendor = Vendor::find($id);
        $vendor->addresses()->find($addressId)->delete();
    }

    public function updateAddress ($request, $id, $addressId)
    {
        $address = VendorsAddresses::find($addressId);
        $address->update($request->all());
    }

    public function createBankAccount ($request, $id)
    {
        $vendor = Vendor::find($id);
        $vendor->accounts()->create([
            'name' => $request->name,
            'status' => $request->status,
            'type' => $request->type,
            'bank' => $request->bank,
            'ifsc' => $request->ifsc,
            'number' => $request->number,
            'icici_beneficiary_id' => $request->icici_beneficiary_id,
            'icici_beneficiary_nickname' => $request->icici_beneficiary_nickname
        ]);
    }

    public function updateBankAccount ($id, $accountId, $request)
    {
        $vendorAccount = VendorBankAccount::find($accountId);
        $vendorAccount->update([
            'name' => $request->name,
            'status' => $request->status,
            'type' => $request->type,
            'bank' => $request->bank,
            'ifsc' => $request->ifsc,
            'number' => $request->number,
            'icici_beneficiary_id' => $request->icici_beneficiary_id,
            'icici_beneficiary_nickname' => $request->icici_beneficiary_nickname
        ]);
    }

    public function deleteBankAccount ($id, $accountId)
    {
        $vendor = Vendor::find($id);
        $vendor->accounts()->find($accountId)->delete();    
    }

    public function saveVendorMarketerMap(Request $request) {
        $params = $this->parseVendorMarketerMapRequest($request);

        $vendorMarketerMap = VendorMarketerMap::firstOrNew([
            'vendor_id' => $params['vendor_id'],
            'marketer_id' => $params['marketer_id']
        ]);

        $file = $request->file('tot_file');
        $fullName = null;
        if($file) {
            $ext = $file->getClientOriginalExtension();
            $name = $params['vendor_id'].'_'.$params['marketer_id'].'_'.strtotime('now');
            $fullName = $name.'.'.$ext;
            Storage::disk('s3')->putFileAs('signed_tot_file/', $file, $fullName);
        }

        $vendorMarketerMap->relation = $params['relation'];
        $vendorMarketerMap->channel = $params['channel'];
        $vendorMarketerMap->mov = $params['mov'];
        $vendorMarketerMap->lead_time = $params['lead_time'];
        $vendorMarketerMap->priority = $params['priority'];
        $vendorMarketerMap->credit_term = $params['credit_term'];
        $vendorMarketerMap->cash_discount = $params['cash_discount'];
        $vendorMarketerMap->tot_signed = $params['tot_signed'];
        $vendorMarketerMap->tot_file = $fullName;
        $vendorMarketerMap->comments = $params['comments'];
        $vendorMarketerMap->date_from = $params['date_from'];
        $vendorMarketerMap->date_to = $params['date_to'];
        $vendorMarketerMap->status = $params['status'];
        $vendorMarketerMap->save();
        return $vendorMarketerMap;
    }

    public function parseVendorMarketerMapRequest(Request $request) {
        return [
            'vendor_id' => $request->input('vendor_id'),
            'marketer_id' => $request->input('marketer_id'),
            'relation' => $request->input('relation'),
            'channel' => $request->input('channel'),
            'mov' => $request->input('mov'),
            'lead_time' => $request->input('lead_time'),

            'credit_term' => $request->input('credit_term'),
            'cash_discount' => $request->input('cash_discount'),
            'tot_signed' => $request->input('tot_signed'),
            'comments' => $request->input('comments'),

            'priority' => $request->input('priority'),
            'date_from' => $request->input('date_from'),
            'date_to' => $request->input('date_to'),
            'status' => $request->input('status'),
        ];
    }
}
