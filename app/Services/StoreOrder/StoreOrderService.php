<?php


namespace Niyotail\Services\StoreOrder;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Niyotail\Events\StoreOrder\StoreOrderCreated;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Niyoos\Client;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\StoreOrderItem;
use Niyotail\Services\Service;

class StoreOrderService extends Service
{
    public function create(Cart $cart, $purchaseInvoice = null): StoreOrder
    {
        $storeOrder = $this->geStoreOrder($cart);
        $storeOrder->setRelation('items', collect([]));
        foreach ($cart->items as $item) {
            $storeOrderItem = $this->getStoreOrderItem($item);
            $storeOrder->items->push($storeOrderItem);
        }

        $this->persist($storeOrder);
        $cart->storeOrder()->associate($storeOrder)->save();

        // Assign store order
        if (!empty($purchaseInvoice)) {
            $purchaseInvoice->assignedOrder()->associate($storeOrder);
            $purchaseInvoice->save();
        }

        event(new StoreOrderCreated($storeOrder));
        return $storeOrder;
    }

    private function geStoreOrder(Cart $cart): StoreOrder
    {
        $data = $cart->attributesToArray();
        unset($data['billing_address_id']);
        unset($data['shipping_address_id']);
        unset($data['currency']);
        unset($data['store_order_id']);
        $data['status'] = StoreOrder::STATUS_PENDING;

        if ($cart->source == Cart::SOURCE_WHOLESALE_CART_IMPORTER) {
            $data['source'] = StoreOrder::SOURCE_WHOLESALE_IMPORTER;
        }
        if (!empty($cart->additional_info)) {
            $data['additional_info'] = json_encode(['data' => json_decode($data['additional_info'])]);
            unset($cart->additional_info);
        }
        return new StoreOrder($data);
    }

    private function getStoreOrderItem(CartItem $cartItem): StoreOrderItem
    {
        $data = $cartItem->attributesToArray();
        unset($data['cart_id']);
        unset($data['parent_sku']);
        unset($data['source']);

        return new StoreOrderItem($data);
    }

    private function persist(StoreOrder $storeOrder)
    {
        //TODO:: validations for order creation goes here
        //$this->validate($storeOrder);
        DB::transaction(function () use ($storeOrder) {
            $storeOrder->save();
            $storeOrder->items()->createMany($storeOrder->items->toArray());
        });

        $storeOrder->unsetRelation('items');
        $storeOrder->refresh();
        $storeOrder->load('items');
    }

    public function updateStatus()
    {
    }

    public function scopeWithApiData($query)
    {
        return $query->with(['items.product' => function ($query) {
            $query->with('images', 'brand');
        }])->with('user', 'store', 'store', 'shippingAddress.city.state.country', 'billingAddress.city.state.country');
    }

    public function cancel(StoreOrder $storeOrder)
    {
        $storeOrder->status = StoreOrder::STATUS_CANCELLED;
        $storeOrder->save();

        return $storeOrder;
    }

    public function getMallOrderCustomerInfo($posOrderReferenceId)
    {
        $client = App::make(Client::class);
        $params['ref_id'] = $posOrderReferenceId;
        $response = $client->dispatch("client/v1/orders/mall-order-customer-info", 'GET', $params);
        $orderCustomerInfo = $response->getBody();

        $customerInfo = [];
        if (!empty($orderCustomerInfo['data'])) {
            $customerData = $orderCustomerInfo['data'];
            $customerInfo['customer_name'] = ($customerData['first_name']) ? $customerData['first_name'] . ' ' . $customerData['last_name'] : '';
            $customerInfo['customer_mobile'] = ($customerData['mobile']) ? $customerData['mobile'] : '';
            $customerInfo['due_amount'] = $customerData['due_amount'];
        }

        return $customerInfo;
    }

    /**
     * @throws ServiceException
     */
    public function retryUnFulfilledOrder(StoreOrder $order)
    {
        if ($order->status != StoreOrder::STATUS_UNFULFILLED) {
            throw new ServiceException("Only UnFulfilled Orders can be retried!");
        }
        DB::transaction(function () use ($order) {
            $order->status = StoreOrder::STATUS_PENDING;
            $order->save();
            event(new StoreOrderCreated($order));
        });
    }

    private function validate(StoreOrder $storeOrder)
    {
    }
}
