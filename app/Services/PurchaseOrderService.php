<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Niyotail\Models\AutoReplenishDemand;
use Niyotail\Models\Product;
use Niyotail\Models\ProductDemandItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\PurchaseInvoiceCartItems;
use Niyotail\Models\PurchaseOrder;
use Niyotail\Models\PurchaseOrderItem;
use Niyotail\Models\Vendor;

class PurchaseOrderService
{
    protected $purchaseOrderPdfService;

    public function __construct()
    {
        $this->purchaseOrderPdfService = new PurchaseOrderPdfService();
    }

    public function createPurchaseOrder($vendorId, $status) {
        return DB::transaction(function() use($vendorId, $status) {
            // create purchase order
            $createdBy = Auth::guard('admin')->user();
            $purchaseOrder = new PurchaseOrder();
            $purchaseOrder->vendor_id = $vendorId;
            $vendorAddress = Vendor::find($vendorId)->activeAddresses->first();
            if($vendorAddress) {
                $purchaseOrder->vendor_address_id = $vendorAddress->id;
            }
            $purchaseOrder->status = $status;
            $purchaseOrder->createdBy()->associate($createdBy);
            $purchaseOrder->save();

            return $purchaseOrder;
        });
    }

    public function createPurchaseOrderWithDemand($demand)
    {
        return DB::transaction(function() use($demand) {
            $vendorId = $demand['vendor_id'];
            $leadTime = $demand['mapping']['lead_time'];

            $purchaseOrder = $this->createPurchaseOrder($vendorId, PurchaseOrder::STATUS_PENDING);
            $purchaseOrder->estimated_delivery_date = Carbon::today()->addDays($leadTime)->format('Y-m-d');
            $purchaseOrder->save();

            $purchaseOrderId = $purchaseOrder->id;
            $demandItems = $demand['items'];
            Log::info($demandItems);
            /* @var ProductDemandItem $item */
            foreach ($demandItems as $item)
            {
                $purchaseOrderItem = new PurchaseOrderItem();
                $purchaseOrderItem->purchase_order_id = $purchaseOrderId;
                $purchaseOrderItem->quantity = $item['reorder_quantity'];
                $purchaseOrderItem->demand_item_id = $item['id'];
                $purchaseOrderItem->product_id = $item['product_id'];
                $purchaseOrderItem->price_calculation_type = strtolower($item['cost_price_by']);
                $purchaseOrderItem->price_calculation_factor = $item['cost_price'];

                if($item['product_id'] && $item['cost_price_by']) {
                    $product = Product::findOrFail($item['product_id']);
                    $unitVariant = $product->unitVariant;
                    if($unitVariant) {
                        $purchaseOrderItem->sku = $unitVariant->sku;
                        $cost = $this->getCostFromPriceCalculationType($product, $unitVariant->sku, $item['cost_price_by'], $item['cost_price']);
                        $purchaseOrderItem->cost_per_unit = $cost['unit'];
                        $purchaseOrderItem->total_cost = $cost['variant']*$item['reorder_quantity'];
                    }
                }

                $purchaseOrderItem->save();
            }

            return $purchaseOrder;
        });
    }

    public function getFullPurchaseOrderById($id)
    {
        return PurchaseOrder::with('createdBy', 'vendor', 'items', 'purchaseInvoices')->findOrFail($id);
    }

    public function updatePurchaseOrderItem(
        $poId,
        $poItemId, $productId, $sku,
        $quantity,
        $priceCalculationType, $priceCalculationFactor
    ) {
        $purchaseOrder = PurchaseOrder::findOrFail($poId);
        $poItems = $purchaseOrder->items;

        /* @var PurchaseOrderItem $poItem */
        $poItem = $poItems->find($poItemId);
        if(empty($poItem))
        {
            throw new \Exception('Purchase order item not found');
        }

        $poItem->product_id = $productId;
        $poItem->sku = $sku;
        $poItem->save();

        if(!empty($poItem->product))
        {
            try {
                $productVariant = ProductVariant::withTrashed()->sku($sku)->firstOrFail();
                $product = $productVariant->product;
                if(empty($product))
                {
                    throw new \Exception('Select product not found');
                }
//            $this->verifyDemandQtyAndPoQty($demand, $quantity);
//            $this->setDemandPendingQuantity($demand, $quantity);

                $cost = $this->getCostFromPriceCalculationType($product, $sku, $priceCalculationType, $priceCalculationFactor);

//            $this->validateMrp($product, $costPerUnit);
                $poItem->quantity = $quantity;
                $poItem->cost_per_unit = $cost['unit'];
                $poItem->price_calculation_type = $priceCalculationType;
                $poItem->price_calculation_factor = $priceCalculationFactor;
                $poItem->total_cost = $cost['variant']*$quantity;
                $poItem->save();

                return $poItem;
            } catch (\Exception $ex) {
                throw new \Exception($ex->getMessage());
            }
        }

    }

    private function getCostFromPriceCalculationType($product, $sku,
                                                     $priceCalculationType,
                                                     $priceCalculationFactor) {
        $variant = ProductVariant::withTrashed()->sku($sku)->firstOrFail();
        $variantUnits = $variant->quantity;
        $mrp = $variant->mrp/$variantUnits;
        switch (strtolower($priceCalculationType)) {
            case "margin":
                $cost = round($mrp * (1 - ($priceCalculationFactor / 100)), 2);
                break;
            case "markup":
                $cost = round($mrp / (1 + ($priceCalculationFactor / 100)), 2);
                break;
            case "latest_unit_cost":
                $inventories = $product->inventories;
                if($inventories->count())
                {
                    $cost = $inventories->first()->unit_cost_price;
                }
                else
                {
                    throw new \Exception('Inventory not found for latest unit cost');
                }
                break;
            case "manual":
                $cost = $priceCalculationFactor;
                break;
        }

        $this->validateMrp($product, $cost);

        return [
            'unit' => $cost,
            'variant' => $cost*$variantUnits
        ];
    }

    private function setDemandPendingQuantity(AutoReplenishDemand $demand, $quantity)
    {
        $pendingQty = $demand->reorder_qty - $quantity;
        $demand->pending_qty = $pendingQty;
        $demand->save();
    }

    private function verifyDemandQtyAndPoQty(AutoReplenishDemand $demand, $quantity)
    {
        if($quantity < 1 || $quantity > $demand->reorder_qty)
        {
            throw new \Exception('Invalid quantity. Max quantity = '.$demand->reorder_qty);
        }
    }

    private function validateMrp($product, $costPerUnit)
    {
        $variants = $product->allVariants;

        $variants = $variants->sortBy('quantity');

        $maxUnitMrp = $variants->first()->mrp;

        if($costPerUnit < 0.1 || $costPerUnit > $maxUnitMrp)
        {
            throw new \Exception('MRP should not exceed '.$maxUnitMrp);
        }
    }

    public function addNewGroupToPurchaseOrder($poId, $groupId, $quantity)
    {
        $purchaseOrder = PurchaseOrder::findOrFail($poId);

        $poItems = $purchaseOrder->items;

        foreach ($poItems as $poItem) {
            $demand = $poItem->demand;
            if($demand->group_id && $demand->group_id == $groupId)
            {
                // group already exists - increase quantity
                // 1. increase reorder quantity
                $demand->reorder_quantity += $quantity;
                // 2. Increase quantity in PO item
                $poItem->quantity = $quantity;
                $poItem->total_cost = $quantity*$poItem->cost_per_unit;
                $poItem->save();
                // 3. Set remaining quantity in demand
                $this->setDemandPendingQuantity($demand, $quantity);

                return [
                    'success' => true,
                    'message' => 'Updated quantity for already existing group',
                    'po_item' => $poItem
                ];
            }
        }

        // no existing demand with same group found
        // create new PO Item
        
        // 1. Create new demand
        $demand = new AutoReplenishDemand();
        $demand->group_id = $groupId;
        $demand->reorder_qty = $quantity;
        $demand->warehouse_id = 2; // TBD - update it later
        $demand->status = 'pending';
        $demand->save();
        // 2. Create PO Item from demand
        $poItem = new PurchaseOrderItem();
        $poItem->purchase_order_id = $poId;
        $poItem->demand_item_id = $demand->id;
        $poItem->quantity = $quantity;
        $poItem->total_cost = $quantity*$poItem->cost_per_unit;
        $poItem->save();
        
        $demand->status = 'po_generated';
        $this->setDemandPendingQuantity($demand, $quantity);

        return [
            'success' => true,
            'message' => 'New purchase order item added',
            'po_item' => $poItem
        ];
    }

    public function placePurchaseOrder($poId)
    {
        return DB::transaction(function() use($poId) {
            $purchaseOrder = PurchaseOrder::findOrFail($poId);
            $poItems = $purchaseOrder->items;
            foreach ($poItems as $poItem) {
                $demand = $poItem->demandItem;
                $this->validatePOItems($poItem, $demand);
//                $demand->status = 'po_placed';
//                $demand->save();
            }

            $purchaseOrder->status = PurchaseOrder::STATUS_PLACED;
            $purchaseOrder->save();
            return $purchaseOrder;
        });
    }

    public function generatePurchaseInvoice($poId)
    {
        return DB::transaction(function() use($poId) {
            $purchaseOrder = PurchaseOrder::findOrFail($poId);
            $this->validatePOVendorAndWarehouse($purchaseOrder);
            $poItems = $purchaseOrder->items;
            if(!$poItems->count())
            {
                throw new \Exception('No purchase order items found to be used in invoice');
            }

            foreach ($poItems as $poItem) {
                $demand = $poItem->demandItem;
                $this->validatePOItems($poItem, $demand);
            }

            $invoice = new PurchaseInvoiceCart();
            $invoice->purchase_order_id = $purchaseOrder->id;
            $invoice->vendor_id = $purchaseOrder->vendor_id;
            $invoice->vendor_address_id = $purchaseOrder->vendor_address_id;
            $invoice->warehouse_id = $purchaseOrder->warehouse_id;
            $invoice->save();
            $invoiceId = $invoice->id;

            // add items to PI
            /*foreach ($poItems as $poItem) {
                $demand = $poItem->demand;
                $demand->status = 'invoice_generated';
                $demand->save();
                $sku = null;
                if($demand->group_id)
                {
                    $group = $demand->group;
                    $product = $group->products->where('id', $poItem->product_id)->first();
                    if(!$product)
                    {
                        throw new \Exception('Selected product for group not found or no product selected');
                    }
                    $variants = $product->allVariants;
                    $unitVariant = $variants->where('value', 'unit')->first();
                    if(!$unitVariant)
                    {
                        $unitVariant = $variants->last();
                    }
                    $sku = $unitVariant->sku;
                }
                $invoiceItem = new PurchaseInvoiceItem();
                $invoiceItem->purchase_invoice_id = $invoiceId;
                $invoiceItem->purchase_order_item_id = $poItem->id;
                $invoiceItem->sku = $sku;
                $invoiceItem->product_id = $poItem->product_id;
                $invoiceItem->quantity = $poItem->quantity;
                $invoiceItem->save();
            }*/

            $purchaseOrder->status = PurchaseOrder::STATUS_INVOICE_CART_GENERATED;
            $purchaseOrder->save();

            return $invoice;
        });
    }

    private function validatePOVendorAndWarehouse($purchaseOrder)
    {
        if(!$purchaseOrder->vendor_address_id || !$purchaseOrder->warehouse_id)
        {
            throw new \Exception('Please select vendor address and niyotail warehouse/');
        }
    }

    private function validatePOItems(PurchaseOrderItem $poItem, ProductDemandItem $demand)
    {
        $quantity = $poItem->quantity;

        if($demand->group_id)
        {
            $group = $demand->group;
            $product = $group->products->where('id', $poItem->product_id)->first();
            if(!$product)
            {
                throw new \Exception('Selected product for group not found or no product selected');
            }

            $this->validateMrp($product, $poItem->cost_per_unit);
        }
    }

    public function createPurchaseOrderFromInvoiceCart(PurchaseInvoiceCart $cart)
    {
        $createdBy = Auth::guard('admin')->user();
        $order = new PurchaseOrder();
        $order->vendor_id = $cart->vendor_id;
        $order->vendor_address_id = $cart->vendor_address_id;
        $order->warehouse_id = $cart->warehouse_id;
        $order->createdBy()->associate($createdBy);
        $order->save();
        return $order;
    }

    public function createOrderItemFromInvoiceCart($poId, PurchaseInvoiceCartItems $cartItem)
    {
        $orderItem = new PurchaseOrderItem();
        $orderItem->purchase_order_id = $poId;
        $orderItem->product_id = $cartItem->product_id;
        $productVariant = ProductVariant::withTrashed()->sku($cartItem->sku)->first();
        if(!$productVariant)
        {
            throw new \Exception('Invalid variant  provided.');
        }
        $orderItem->quantity = $productVariant->quantity*$cartItem->quantity;
        $orderItem->cost_per_unit = $cartItem->total_invoiced_cost/$productVariant->quantity;
        $orderItem->total_cost = $orderItem->cost_per_unit*$orderItem->quantity;
        $orderItem->save();
        return $orderItem;
    }

    public function updatePurchaseOrder($poId, $addressId, $warehouseId, $deliveryDate)
    {
        /* @var PurchaseOrder $purchaseOrder */
        $purchaseOrder = PurchaseOrder::findOrFail($poId);
        $purchaseOrder->vendor_address_id = $addressId;
        $purchaseOrder->warehouse_id = $warehouseId;
        $purchaseOrder->estimated_delivery_date = $deliveryDate;
        $purchaseOrder->save();
        return $purchaseOrder;
    }

    public function generatePurchaseOrderFile($poId)
    {
        /* @var PurchaseOrder $purchaseOrder */
        $purchaseOrder = PurchaseOrder::findOrFail($poId);

        if($purchaseOrder->isEditable())
        {
            throw new \Exception('Purchase order is still in editable phase. Please complete it first.');
        }

        $pdf = $this->purchaseOrderPdfService->generatePdf($purchaseOrder)->output();

        $path = Str::uuid()->toString().".pdf";
        Storage::disk('po')->put($path, $pdf);
        $purchaseOrder->file = $path;
        $purchaseOrder->save();
        return $purchaseOrder;
    }
}
