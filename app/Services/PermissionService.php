<?php

namespace Niyotail\Services;

use Niyotail\Models\Permission;
use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;

class PermissionService extends Service
{
    public function create(Request $request)
    {
        if ($this->isDuplicateEntry($request)) {
            return false;
        }
        $permission=$this->processData($request, new Permission());
        return $permission->save();
    }

    public function update(Request $request)
    {
        $permission=Permission::find($request->id);
        if (empty($permission)) {
            throw new ServiceException("Permission doesn't exist !");
        }
        if ($this->isDuplicateEntry($request, $request->id)) {
            return false;
        }
        $permission=$this->processData($request, $permission);
        return $permission->save();
    }

    public function delete(Request $request)
    {
        $permission=Permission::find($request->id);
        if (empty($permission)) {
            throw new ServiceException("Permission doesn't exist !");
        }
        return $permission->delete();
    }
    private function processData(Request $request, Permission $permission)
    {
        $permission->name = $request->name;
        $permission->label = $request->label;
        return $permission;
    }

    public function isDuplicateEntry(Request $request, $id = null)
    {
        $employee = Permission::where('name', $request->name)->where('id', '!=', $id)->first();
        if (!empty($employee)) {
            throw new ServiceException("Name Already Taken");
        }
        return false;
    }
}
