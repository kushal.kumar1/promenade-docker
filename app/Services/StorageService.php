<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Storage;

class StorageService extends Service
{
    /**
     * @throws ServiceException
     */
    public function create(Request $request): Storage
    {
        $alreadyExists = Storage::where('warehouse_id', $request->warehouse_id)
            ->where('label', $request->label)->first();
        if (!empty($alreadyExists)) {
            throw new ServiceException('Label already exist with given warehouse');
        }
        $storage = new Storage();
        $storage->warehouse_id = $request->warehouse_id;
        $storage->zone = strtolower($request->zone);
        $storage->category = strtolower($request->category);
        $storage->label = $request->label;
        $storage->save();
        return $storage;
    }
}