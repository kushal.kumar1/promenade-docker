<?php


namespace Niyotail\Services;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\AutoReplenishDemand;
use Niyotail\Models\AutoReplenishParameter;
use Niyotail\Models\GroupProduct;
use Niyotail\Models\Niyoos\NiyoOsInventory;
use Niyotail\Models\Niyoos\NiyoOsOrderItems;
use Niyotail\Models\WhGroupAbc;

class AutoReplenishDemandService
{
    protected $procedure = 'update_wh_abc_groups';

    public function generateDemandByProcedure()
    {
        $this->executeProcedure();
        $openDemands = WhGroupAbc::openDemand()->get();

        foreach ($openDemands as $openDemand)
        {
            $demand = AutoReplenishDemand::firstOrNew(
                [
                    'group_id' => $openDemand->group_id,
                    "product_id" => null,
                    'status' => 'pending'
                ]
            );

            $demand->reorder_qty = $openDemand->demand_units_final;
            $demand->warehouse_id = 2;

            $demand->save();
        }
    }

    private function executeProcedure()
    {
        $query = 'call '.$this->procedure.'()';
        return DB::select($query);
    }

    public function generateDemandByDateRange()
    {
        $start = Carbon::parse('2020-07-05 00:00:00');
        $end = Carbon::parse('2020-07-18 23:59:59');

        $startDate = $start->copy()->format('Y-m-d H:i:s'); // a
        $endDate = $end->copy()->format('Y-m-d H:i:s'); // b
        $period = $end->diffInDays($start) + 1; // c

        /* get total ordered units for all items in the date range */
        $orderItemsByProductId = NiyoOsOrderItems::with('vendor')->where('created_at', '>=', $startDate)
            ->where('created_at', '<=', $endDate)
            ->get();

        /* format ordered units details with niyotail product id */
        $orderedProductUnits = $orderItemsByProductId
            ->groupBy('product_id')
            ->keyBy('product_id')
            ->map(function($items, $key){
                return [
                    'total_quantity' => $items->sum('quantity'),
                    'niyoos_product_id' => $key,
                    'niyotail_product_id' => $items->first()->vendor ? $items->first()->vendor->vendor_product_identifier : null
                ];
            })->keyBy('niyoos_product_id');

        $uniqueProductIds = $orderedProductUnits->pluck('niyoos_product_id');

        /* Get inventory info for all the product ids received above */
        $inventories = NiyoOsInventory::whereIn('product_id', $uniqueProductIds)
            ->where('status', 1)->get();

        /* Map current inventory for all the products received above */
        $productInventory = $inventories->groupBy('product_id')->map(
            function($rows, $productId) use($orderedProductUnits) {
                $inventory = $rows->sum('quantity');
                $productOrder = $orderedProductUnits[$productId];
                $productOrder['current_inventory'] = $inventory - $productOrder['total_quantity'];
                return $productOrder;
            });

        /* key by niyotail_product_id */
        $productInventory = $productInventory->keyBy('niyotail_product_id');

        $uniqueNiyotailProductIds = $productInventory->pluck('niyotail_product_id');

        /* TODO:: change as per new group product relationship */

        /* Get GID for all the product ids obtained above */
        $groupProducts = GroupProduct::whereIn('product_id', $uniqueNiyotailProductIds)->get();

        /* Group by GID */
        $groupWiseProductData = $groupProducts->groupBy('group_id')->map(
            function($rows, $groupId) use($productInventory) {
                $products = array();
                $totalGroupOrderQuantity = 0;
                $totalGroupInventory = 0;
                foreach ($rows as $row)
                {
                    $productId = $row->product_id;
                    if(isset($productInventory[$productId]))
                    {
                        $thisProductInventory = $productInventory[$productId];
                        $products[$productId] = $thisProductInventory;
                        $totalGroupOrderQuantity += $thisProductInventory['total_quantity'];
                        $totalGroupInventory += $thisProductInventory['current_inventory'];
                    }
                }

                return [
                    "group_id" => $groupId,
                    "order_units" => $totalGroupOrderQuantity,
                    "current_inventory" => $totalGroupInventory,
                    "products" => $products
                ];
            })->keyBy('group_id');

        /* identify class for all groups */
        $uniqueGroupIds = $groupWiseProductData->pluck('group_id');

        $groupClassParams = WhGroupAbc::whereIn('group_id', $uniqueGroupIds)->get();

        $groupClassData = $groupClassParams->map(function ($groupClass) use ($groupWiseProductData) {
            if(!empty($groupWiseProductData[$groupClass->group_id]))
            {
                $groupData = $groupWiseProductData[$groupClass->group_id];
                $groupData['qty_per_day'] = $groupClass->qty_per_day;
                $groupData['class_abc'] = $groupClass->class_abc;
                return $groupData;
            }
        });

        /* get group replenishment params for all classes */
        $replenishParams = AutoReplenishParameter::active()->warehouse()->get()->keyBy('class');

        /* Get groups eligible for demand generation where reorder qty >= current inventory */
        $eligibleDemandGroups = $groupClassData->filter(function($group) use ($replenishParams) {
            $class = $group['class_abc'];
            $classParams = $replenishParams[$class];
            if(!empty($classParams))
            {
                $reorderLevel = $classParams->reorder_level;
                $quantityPerDay = $group['qty_per_day'];
                $reorderLevelQty = $reorderLevel*$quantityPerDay;
                $currentInventory = $group['current_inventory'];

                return $reorderLevelQty >= $currentInventory;
            }
        });

        $groupDemandQty = $eligibleDemandGroups->map(function($group) use($replenishParams) {
            $groupClass = $group['class_abc'];
            $replenishParam = $replenishParams[$groupClass];
            $reorderQty = $group['qty_per_day']*$replenishParam->reorder_quantity;

            $demand = AutoReplenishDemand::firstOrNew(
                [
                    'group_id' => $group['group_id'],
                    "product_id" => null,
                    'status' => 'pending'
                ]
            );

            $demand->reorder_qty = ceil($reorderQty);
            $demand->warehouse_id = 2;
//            if($reorderQty >= 1) $demand->save();
        });
    }
}