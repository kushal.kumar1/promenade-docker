<?php

namespace Niyotail\Services;

use Illuminate\Support\Facades\App;
use Niyotail\Events\Invoice\InvoiceCancelled;
use Niyotail\Events\Invoice\InvoiceCreated;
use Niyotail\Models\Order;
use Niyotail\Models\Shipment;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;
use Niyotail\Models\WarehouseScope;
use Niyotail\Services\Service;
use Niyotail\Models\Invoice;
use Niyotail\Helpers\InvoiceHelper;
use Niyotail\Models\Transaction;
use Niyotail\Services\TransactionService;
use Niyotail\Exceptions\ServiceException;

class InvoiceService extends Service
{
    public function create(Shipment $shipment)
    {
        $shipment->loadMissing('order.store', 'orderItems');
        $order = $shipment->order;
        if($order->type != Order::TYPE_STOCK_TRANSFER){
            if ($shipment->orderItems->sum('total') > "45000") {
                throw new ServiceException("Cannot create invoice for amount greater than 45000");
            }
        }
        $invoice = new Invoice();
        $invoice->order()->associate($order);
        $invoice->shipment()->associate($shipment);
        $invoice->financial_year = $this->getFinancialYear($order);
        $invoice->number = 1;
        $lastInvoice = $this->getLastInvoice($invoice->financial_year);
        if (!empty($lastInvoice)) {
            $invoice->number = $lastInvoice->number + 1;
        }
        $invoice->reference_id = "NT/" . $invoice->financial_year . "/" . sprintf('%06d', $invoice->number);
        $invoice->status = Invoice::STATUS_GENERATED;
        $invoice->pdf = $invoice->reference_id . '.pdf';
        $invoice->warehouse_id = $shipment->warehouse_id;
        $invoice->amount = round($shipment->orderItems->sum('total'));
        $invoice->save();

        //add transaction
        $transactionService = (new TransactionService())->setAuthority(TransactionService::AUTHORITY_SYSTEM);
        $transactionService->createForInvoice($invoice, Transaction::TYPE_DEBIT);

        event(new InvoiceCreated($invoice));
        if (App::environment() == 'production') {
            InvoiceHelper::generatePdf($invoice);
        }
        return $invoice;
    }

    public function cancel(Invoice $invoice)
    {
        $invoiceMonth = Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at, 'Asia/Kolkata')->month;
        $currentMonth = (new DateTime())->setTimezone(new DateTimeZone('Asia/Kolkata'))->format('m');
//        if (($invoiceMonth < $currentMonth) && (date('d') >= 2)) {
 //           throw new ServiceException("Cannot cancel invoices for previous month.");
 //       }
        $invoice->cancelled_on = (new DateTime())->format('Y-m-d H:i:s');
        $invoice->status = Invoice::STATUS_CANCELLED;
        $invoice->save();
        event(new InvoiceCancelled($invoice));
    }

    private function getFinancialYear($order)
    {
        $creationDateTime = new DateTime($order->created_at);
        $year = $creationDateTime->format("y");
        $month = $creationDateTime->format("n");
        if ($month < 4) {
            return ($year - 1) . '-' . $year;
        }
        return $year . '-' . ($year + 1);
    }

    private function getLastInvoice($financialYear)
    {
        return Invoice::withoutGlobalScope(WarehouseScope::class)->where('financial_year', $financialYear)
            ->orderBy('number', 'desc')->first();
    }

    public function addElectronicWayBillNumber(Invoice $invoice, string $number)
    {
        $invoice->eway_bill_number = $number;
        $invoice->save();
        InvoiceHelper::generatePdf($invoice);
    }
}
