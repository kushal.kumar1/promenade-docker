<?php

namespace Niyotail\Services;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\CheckProductInventorySyncJob;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\OrderItemFlatInventory;
use Niyotail\Models\PurchaseInvoiceGrnItem;
use Niyotail\Models\Putaway\PutawayItem;
use Niyotail\Models\StockTransferItem;
use Niyotail\Models\Storage;

class FlatInventoryService extends Service
{

    private array $source = [
        PurchaseInvoiceGrnItem::class => 'grn_item',
        StockTransferItem::class => 'stock_transfer_item'
    ];

    /**
     * Function to create new inventory rows from GRN
     * @param $modelObject
     * @param int $warehouseId
     * @param int $quantity
     */
    public function create($modelObject, int $warehouseId, int $quantity, string $status = null): void
    {
        $data = collect([]);
        $status = empty($status) ? FlatInventory::STATUS_PUTAWAY : $status;
        $expiryDate = null;
        $manufacturingDate = null;
        $unitCost = null;
        //If grn source is external purchase set status to ready_for_sale.
        //TODO: Update the warehouse check from id warehouse type
        if ($this->source[get_class($modelObject)] == 'grn_item') {
            if ($modelObject->grn->warehouse_id == 1) {
                $status = FlatInventory::STATUS_READY_FOR_SALE;
            }
            $expiryDate = $modelObject->item->expiry_date;
            $manufacturingDate = $modelObject->item->manufacturing_date;
            if (!empty($modelObject->item->total_invoiced_cost)) {
                $unitCost = $modelObject->item->total_invoiced_cost;
            }
        }

        for ($i = 0; $i < $quantity; $i++) {
            $row = array();
            $row['source_type'] = $this->source[get_class($modelObject)];
            $row['source_id'] = $modelObject->id;
            $row['product_id'] = $modelObject->product_id;
            $row['warehouse_id'] = $warehouseId;
            $row['expiry_date'] = $expiryDate;
            $row['manufacturing_date'] = $manufacturingDate;
            $row['status'] = $status;
            $row['unit_cost'] = $unitCost;
            $data->push($row);
        }
        $newInventoriesData = array_chunk($data->toArray(), 500);
        foreach ($newInventoriesData as $flatInventories) {
            FlatInventory::insert($flatInventories);
        }
    }

    /**
     * Function to update storage & move items to ready for sale via putaway process
     * TODO: differentiate putaway for returns, rto and purchases
     * @param PutawayItem $putawayItem
     * @throws ServiceException
     */
    public function updatePutawayItem(PutawayItem $putawayItem)
    {
        DB::transaction(function () use ($putawayItem) {
            $warehouseId = $putawayItem->putaway->warehouse_id;
            $whereCondition = ['status' => FlatInventory::STATUS_PUTAWAY, 'product_id' => $putawayItem->product_id, 'warehouse_id' => $warehouseId];
            $inventories = FlatInventory::where($whereCondition)->limit($putawayItem->quantity)->lockForUpdate()->get();
            if ($inventories->count() < $putawayItem->quantity) throw new ServiceException("Putaway quantity not available! Try again!");
            FlatInventory::whereIn('id', $inventories->pluck('id')->toArray())
                ->update(['status' => FlatInventory::STATUS_READY_FOR_SALE, 'storage_id' => $putawayItem->storage_id]);
        });
    }

    /**
     * Function to transfer inventory from one storage to another
     * TODO: change function to handle damage type transfers
     * @param int $productId
     * @param int $quantity
     * @param Storage $originalLocation
     * @param Storage $targetLocation
     * @throws ServiceException
     */
    public function changeLocation(int $productId, int $quantity, Storage $originalLocation, Storage $targetLocation)
    {
        if ($originalLocation->warehouse->id != $targetLocation->warehouse->id) throw new ServiceException("Warehouse mismatch for storages!");
        $warehouseId = $originalLocation->warehouse->id;
        $whereCondition = ['status' => FlatInventory::STATUS_READY_FOR_SALE, 'product_id' => $productId, 'warehouse_id' => $warehouseId, 'storage_id' => $originalLocation->id];
        $inventories = FlatInventory::where($whereCondition)->limit($quantity)->lockForUpdate()->get();
        if ($inventories->count() < $quantity) throw new ServiceException("Quantity not available for transfer! Try again!");
        FlatInventory::whereIn('id', $inventories->pluck('id')->toArray())
            ->update(['storage_id' => $targetLocation->storage_id]);
    }


    /**
     * @param array $inventoryIds
     * @param string $status
     * @throws ServiceException
     */
    public function markBadInventories(array $inventoryIds, string $status)
    {
        if (!in_array($status, FlatInventory::getBadInventoryStatus())) {
            throw new ServiceException('Not valid bad inventory status');
        }
        $flatInventories = FlatInventory::lockInventoryIdsForUpdate($inventoryIds);
        foreach (array_chunk($flatInventories, 500) as $chunkedIds) {
            FlatInventory::whereIn('id', $chunkedIds)->update(['status' => $status]);
        }
        if (!empty($inventoryIds[0])) {
            $this->checkAndSyncMallProduct('outbound', $inventoryIds[0]);
        }
    }

    /**
     * @param array $inventoryIds
     * @param string $status
     * @throws ServiceException
     */
    public function markGoodInventories(array $inventoryIds, string $status)
    {
        if (!in_array($status, FlatInventory::getGoodInventoryStatus())) {
            throw new ServiceException("Not valid good inventory status!");
        }
        $flatInventories = FlatInventory::lockInventoryIdsForUpdate($inventoryIds);
        foreach (array_chunk($flatInventories, 500) as $chunkedIds) {
            FlatInventory::whereIn('id', $chunkedIds)->update(['status' => $status]);
        }

        if ($status == FlatInventory::STATUS_READY_FOR_SALE) {
            if (!empty($flatInventories[0])) {
                $this->checkAndSyncMallProduct('inbound', $flatInventories[0]);
            }
        }
    }

    public function markInventoriesSold(array $inventoryIds)
    {
        foreach (array_chunk($inventoryIds, 500) as $chunkedIds) {
            FlatInventory::whereIn('id', $chunkedIds)->update(['status' => FlatInventory::STATUS_SOLD]);
        }
        if (!empty($inventoryIds[0])) {
            $this->checkAndSyncMallProduct('outbound', $inventoryIds[0]);
        }
    }

    public function generateSoldInventories($orderItem)
    {
      $orderItem->loadMissing('inventories.inventory', 'order');

      $flatInventories = collect([]);
      foreach ($orderItem->inventories as $orderItemInventory) {
          if (!empty($orderItemInventory->inventory->grn_item_id)) {
              $expiryDate = $orderItemInventory->inventory->grnItem->item->expiry_date ?? null;
              $mfgDate = $orderItemInventory->inventory->grnItem->item->manufacturing_date ?? null;
              $unitCost = $orderItemInventory->unit_cost_price ?? null;
              $flatInventory = [
                  'source_type' => 'grn_item',
                  'source_id' => $orderItemInventory->inventory->grn_item_id,
                  'product_id' => $orderItem->product_id,
                  'warehouse_id' => $orderItem->order->warehouse_id,
                  'status' => FlatInventory::STATUS_SOLD,
                  'expiry_date' => $expiryDate,
                  'manufacturing_date' => $mfgDate,
                  'unit_cost' => $unitCost
              ];
              $soldInventory = FlatInventory::create($flatInventory);
              $flatInventories->push($soldInventory->id);
          }
      }

      $generatedFlatInventories = FlatInventory::where('source_type', 'grn_item')
          ->whereIn('id', $flatInventories->flatten()->toArray())->get();
      $orderItemFlatInventories = [];
      foreach ($generatedFlatInventories as $inventory) {
          $orderItemFlatInventories [] = [
              'order_item_id' => $orderItem->id,
              'flat_inventory_id' => $inventory->id,
          ];
      }
      //Generating Order Item Flat Inventories
      foreach (array_chunk($orderItemFlatInventories, 500) as $itemInventories) {
          OrderItemFlatInventory::insert($itemInventories);
      }
    }

    public function markInventoriesForTransfer(array $inventoriesId)
    {
        foreach (array_chunk($inventoriesId, 500) as $chunkedInventoryIds) {
            FlatInventory::whereIn('id', $chunkedInventoryIds)->update(['status' => FlatInventory::STATUS_STOCK_TRANSFER]);
        }
    }

    public function completeInventoryTransfer(array $inventoriesId, int $toWarehouseId)
    {
        $flatInventories = FlatInventory::lockInventoryIdsForUpdate($inventoriesId);

        foreach (array_chunk($flatInventories, 500) as $chunkedInventoryIds) {
            FlatInventory::whereIn('id', $chunkedInventoryIds)
                ->update(['status' => FlatInventory::STATUS_PUTAWAY, 'warehouse_id' => $toWarehouseId]);
        }
    }

    public function markGoodInventoriesWithStorage(array $inventoryIds, string $status, int $storageId)
    {
        if ($status != FlatInventory::STATUS_PUTAWAY && $status != FlatInventory::STATUS_READY_FOR_SALE && $status != FlatInventory::STATUS_HOLD) {
            throw new ServiceException("Not valid good inventory status!");
        }
        $flatInventories = FlatInventory::lockInventoryIdsForUpdate($inventoryIds);
        foreach (array_chunk($flatInventories, 500) as $chunkedIds) {
            FlatInventory::whereIn('id', $chunkedIds)->update(['status' => $status, 'storage_id' => $storageId]);
        }

        if ($status == FlatInventory::STATUS_READY_FOR_SALE) {
            if (!empty($flatInventories[0])) {
                $this->checkAndSyncMallProduct('inbound', $flatInventories[0]);
            }
        }
    }

    private function checkAndSyncMallProduct($type, $inventoryId)
    {
        if(App::environment() != 'local') {
            CheckProductInventorySyncJob::dispatch($type, $inventoryId)
                ->onConnection('sqs')
                ->onQueue(config('queue.wmsQueue'));
        } else {
            dispatch(new CheckProductInventorySyncJob($type, $inventoryId));
        }
    }
}
