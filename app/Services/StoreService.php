<?php

namespace Niyotail\Services;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Store;
use Niyotail\Models\User;

/**
 * Class StoreService
 * @package Niyotail\Services
 */
class StoreService extends Service
{
    /**
     * @param Request $request
     * @return Store
     * @throws ServiceException
     */
    public function create(Request $request)
    {
        $this->checkIfContactMobileAlreadyExists($request);
        $store = $this->processData($request, new Store());
        $store->save();
        return $store;
    }

    private function checkIfContactMobileAlreadyExists($request)
    {
        $store = Store::where('contact_mobile', $request->contact_mobile)->first();
        if (!empty($store)) {
            throw new ServiceException('Store with id: ' . $store->id . ' and name: ' . $store->name . ' already exists with the same contact number!');
        }
    }

    public function update(Request $request, Store $store)
    {
        $this->processData($request, $store);
        $store->save();
        return $store;
    }

    private function processData(Request $request, Store $store)
    {
        $store->name = $request->name;
        $store->legal_name = $request->legal_name;
        $store->gstin = $request->gstin;
        $store->lat = $request->lat;
        $store->lng = $request->lng;
        if ($request->has('location')) {
            $store->location = $request->location;
        }
        $store->address = $request->address;
        $store->landmark = $request->landmark;
        $store->pincode = $request->pincode;
        $store->contact_person = $request->contact_person;
        $store->contact_mobile = $request->contact_mobile;

        if ($request->has('tag_ids')) {
            $store->tags()->sync($request->get('tag_ids'));
        }

        if ($this->checkAuthority(self::AUTHORITY_ADMIN)) {
            if ($request->has('beat_id')) {
                $store->beat_id = $request->beat_id;
            }
            $store->verified = $request->has('verified') ? $request->verified : 0;
            $store->status = $request->has('status') ? $request->status : 1;
            $store->credit_allowed = $request->has('credit_allowed') ? $request->credit_allowed : 0;
        }
        if ($this->checkAuthority(self::AUTHORITY_AGENT)) {
            $store->beat_id = $request->beat_id;
            // $store->verified = 1;
            // $store->status = 1;
        }
        return $store;
    }

    public function addUser(Request $request)
    {
        $store = Store::find($request->id);
        if (empty($store)) {
            throw new ServiceException('Store not found!');
        }

        $store->users()->attach($request->user_id);
        return $store;
    }

    public function removeUser(Request $request)
    {
        $store = Store::find($request->id);
        if (empty($store)) {
            throw new ServiceException('Store not found!');
        }

        $store->users()->detach($request->user_id);
        return $store;
    }


    /**
     * @throws ServiceException
     */
    public function updateStatus(Request $request)
    {
        $store = Store::find($request->id);
        if (empty($store)) {
            throw new ServiceException('Store not found');
        }
        if ($request->status == 1 && !$store->canActivate()) {
            throw new ServiceException("Store cannot be activated!");
        }
        $store->status = $request->status;
        $store->disable_reason = $request->filled('disable_reason') ? $request->disable_reason : null;
        $store->save();
        return $store;
    }

    public function addDefaultUser($store, User $user)
    {
        $store->users()->attach($user->id);
        return $store;
    }


    public function createFromOnBoarding(Request $request, User $user)
    {
        $store = new Store();
        $store->name = $store->legal_name = $request->store_name;
        $store->pincode = $request->pincode;
        $store->contact_person = $user->name;
        $store->contact_mobile = $user->mobile;
        $store->save();
        return $store;
    }

    /**
     * @throws ServiceException
     */
    public function setIsCreditAllowed(int $storeId, bool $isCreditAllowed)
    {
        $store = Store::find($storeId);
        if(empty($store)){
            throw new ServiceException('Invalid Store Id');
        }
        $store->credit_allowed = $isCreditAllowed;
        $store->save();
    }
}
