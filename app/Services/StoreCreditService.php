<?php

namespace Niyotail\Services;

use Niyotail\Models\StoreCredit;
use Niyotail\Models\CreditRequest;


class StoreCreditService extends Service
{

    public function create($customerId,$orderId,$amount,$type,$description=null)
    {
        $storeCredit=new StoreCredit();
        $storeCredit->customer_id=$customerId;
        $storeCredit->order_id=$orderId;
        $storeCredit->amount=$amount;
        $storeCredit->type=$type;
        $storeCredit->description=$description;
        return $storeCredit->save();
    }

    public function createCreditRequest($storeID, $user, $request)
    {
        $request = CreditRequest::where('store_id', $storeID)->first();
        if (!empty($request)) {
            throw new ServiceException('Credit request already exists for the store');
        }
        $storeCredit = new CreditRequest();
        $storeCredit->store_id = $storeID;
        $storeCredit->user_id = $user->id;
        $storeCredit->partner = $request->partner;
        $storeCredit->status = 'pending';
        return $storeCredit->save();
    }
}
