<?php

namespace Niyotail\Services;

use Niyotail\Models\StoreAnswer;
use Niyotail\Models\ProfileQuestion;
use Niyotail\Exceptions\ServiceException;
use Illuminate\Http\Request;
use Niyotail\Models\User;
use Niyotail\Helpers\StoreProfilingHelper;

/**
 * Class StoreProfileService
 * @package Niyotail\Services
 */
class StoreProfilingService extends Service
{
    public function create(Request $request, $profileQuestion, $agent)
    {
        if (empty($profileQuestion->answerOptions) || $profileQuestion->answerOptions->isEmpty()) {
            throw new ServiceException("Invalid options !");
        }
        $storeAnswers = $profileQuestion->answerOptions->pluck('storeAnswers')->collapse()->where('question_id', $request->question_id);
        if ($storeAnswers->isNotEmpty()) {
            throw new ServiceException("Question already answered !");
        }

        $type = $profileQuestion->type;
        $answer = (!in_array($type, ['checkbox', 'dropdown'])) ? $request->answer: $request->answer_id;
        try {
          StoreProfilingHelper::validate($type, $answer);
        } catch (\Exception $e) {
             throw new ServiceException($e->getMessage());
        }

        foreach($profileQuestion->answerOptions as $option) {
          $storeProfile = $this->processData($request, $type, $option, $agent, new StoreAnswer());
          $storeProfile->save();
        }
        return true;
    }

    public function update(Request $request, $agent)
    {
        $profileQuestion = ProfileQuestion::getStoreAnswers($request->question_id, $request->store_id);
        if ($profileQuestion->isEmpty()) {
            throw new ServiceException("Answer was not answered previously!");
        }
        $storeAnswers = $profileQuestion->pluck('storeAnswers')->collapse();
        foreach ($storeAnswers as $storeAnswer) {
            $storeAnswer->delete();
        }
        $profileQuestion = ProfileQuestion::getQuestion($request->question_id, $request->store_id, $request->answer_id);
        if (empty($profileQuestion->answerOptions) || $profileQuestion->answerOptions->isEmpty()) {
            throw new ServiceException("Invalid options !");
        }

        $type = $profileQuestion->type;
        $answer = (!in_array($type, ['checkbox', 'dropdown'])) ? $request->answer: $request->answer_id;
        try {
          StoreProfilingHelper::validate($type, $answer);
        } catch (\Exception $e) {
             throw new ServiceException($e->getMessage());
        }

        foreach($profileQuestion->answerOptions as $option) {
          $type = $profileQuestion->type;
          $storeProfile = $this->processData($request, $type, $option, $agent, new StoreAnswer());
          $storeProfile->save();
        }
        return true;
    }

    private function processData(Request $request, $type, $option, $agent, StoreAnswer $storeProfile)
    {
        $storeProfile->store_id = $request->store_id;
        $storeProfile->question_id = $request->question_id;
        $storeProfile->answer_id = $option->id;
        if (!in_array($type, ['checkbox', 'dropdown'])) {
            $storeProfile->answer = $request->answer;
        } else {
            $storeProfile->answer = $option->option;
        }
        $storeProfile->updated_by_agent = $agent->id;
        return $storeProfile;
    }
}
