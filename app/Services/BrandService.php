<?php

namespace Niyotail\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Grids\BrandsDashboardGrid;
use Niyotail\Helpers\SearchHelper;
use Niyotail\Helpers\StatsHelper;
use Niyotail\Http\Resources\Brand\DashboardMetricsResource;
use Niyotail\Http\Resources\Brand\DashboardStatsResource;
use Niyotail\Models\Beat;
use Niyotail\Models\Brand;
use Illuminate\Http\Request;
use Niyotail\Helpers\File;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Collection;
use Niyotail\Models\Marketer;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Product;
use Niyotail\Models\Store;

/**
 * Class BrandService
 *
 * @package Niyotail\Services
 *
 */
class BrandService extends Service
{
    public function create(Request $request)
    {
        $brand = new Brand();
        $parentNode = Brand::find($request->parent_id);
        $leftNode = Brand::find($request->left_node_id);
        $brand = DB::transaction(function () use ($brand, $request, $parentNode, $leftNode) {
            if (!empty($leftNode) && !empty($parentNode)) {
                if (!$leftNode->isChildOf($parentNode)) {
                    $leftNode = null;
                }
            }
            if (empty($leftNode) && !empty($parentNode)) {
                $leftNode = Brand::where('rgt', $parentNode->rgt - 1)->first();
                if (empty($leftNode)) {
                    Brand::where('lft', '>', $parentNode->rgt)->increment('lft', 2);
                    Brand::where('rgt', '>', $parentNode->rgt)->increment('rgt', 2);
                    $parentNode->rgt += 2;
                    $parentNode->save();
                }
            } elseif (empty($leftNode) && empty($parentNode)) {
                $leftNode = Brand::orderBy('rgt', 'desc')->first();
            }
            if (!empty($leftNode)) {
                Brand::where('rgt', '>', $leftNode->rgt)->increment('rgt', 2);
                Brand::where('lft', '>', $leftNode->rgt)->increment('lft', 2);
            }
            $brand = $this->processData($brand, $request, $leftNode, $parentNode);
			$brand->marketer_id = $request->marketer_id;
			
			if(isset($request->status))
				$brand->status = $request->status;
			if(isset($request->visible))	
				$brand->is_visible = $request->visible;

            $brand->save();
            if ($request->has('logo')) {
                 $this->addLogo($brand, $request);
            }

            return $brand;
        });
        return $brand;
    }

    public function update(Request $request)
    {
        $brand = Brand::find($request->id);
        if (empty($brand)) {
            throw new ServiceException('Invalid Brand');
        }
        $brand->name = $request->name;
        $brand->status = $request->status;
        $brand->is_visible = $request->visible;
        $brand->marketer_id = $request->marketer_id;
        $brand->save();
        if ($request->has('logo')) {
            $this->addLogo($brand, $request);
        }
    }

    private function processData(Brand $brand, $request, $leftNode = null, $parentNode = null)
    {
        $brand->name = $request->name;
        $brand->slug = str_slug($request->name);
        if (!empty($leftNode)) {
            $brand->lft = $leftNode->rgt + 1;
            $brand->rgt = $leftNode->rgt + 2;
        } elseif (!empty($parentNode)) {
            $brand->lft = $parentNode->lft + 1;
            $brand->rgt = $parentNode->lft + 2;
        } else {
            $brand->lft = 1;
            $brand->rgt = 2;
        }
        $brand->status = $request->status;
        $brand->is_visible = $request->visible;
        return $brand;
    }

    public function delete($id)
    {
        $brand = Brand::find($id);
        if (empty($brand)) {
            throw new ServiceException('Invalid Brand');
        }
        if ($brand->isLeafNode()) {
            return DB::transaction(function () use ($brand) {
                Brand::where('lft', '>', $brand->rgt)->decrement('lft', 2);
                Brand::where('rgt', '>', $brand->rgt)->decrement('rgt', 2);
                return $brand->delete();
            });
        } else {
            return DB::transaction(function () use ($brand) {
                Brand::where('lft', '>', $brand->rgt)->decrement('lft', 2);
                Brand::where('rgt', '>', $brand->rgt)->decrement('rgt', 2);
                Brand::where('lft', '>', $brand->lft)->where('rgt', '<', $brand->rgt)->decrement('lft', 1);
                Brand::where('lft', '>=', $brand->lft)->where('rgt', '<', $brand->rgt)->decrement('rgt', 1);
                return $brand->delete();
            });
        }
    }

    private function addLogo(Brand $brand, Request $request)
    {
        $logo = $request->file('logo');
        $targetDirectory = public_path('media/brand/' . $brand->id);
        $file = new File($logo);
        $file->setName(uniqid() . "." . $logo->guessExtension());
        $file->setDirectory($targetDirectory);
        $file->save();
        $brand->logo = $file->getName();
        $brand->save();
    }

    public function sort($request)
    {
        $brand = Brand::find($request->id);
        if (empty($brand)) {
            throw new ServiceException('Invalid Brand');
        }
        $parentId = $request->parent_id;
        $leftNodeId = $request->left_node_id;
        DB::transaction(function () use ($brand, $parentId, $leftNodeId) {
            if (empty($parentId) && empty($leftNodeId)) {
                $startValue = 1;
            } elseif (!empty($parentId) && empty($leftNodeId)) {
                $parentBrand = Brand::find($parentId);
                $startValue = $parentBrand->lft + 1;
            } else {
                $leftBrand = Brand::find($leftNodeId);
                $startValue = $leftBrand->rgt + 1;
            }
            return $this->moveBrand($brand, $startValue);
        });
    }

    private function moveBrand($brand, $startValue)
    {
        $width = $brand->getWidth();
        $brandWithChildren = Brand::getChildren($brand->id)->pluck('id')->toArray();
        if ($startValue < $brand->lft) {
            Brand::where('lft', '>=', $startValue)->where('lft', '<', $brand->rgt)->whereNotIn('id', $brandWithChildren)->increment('lft', $width);
            Brand::where('rgt', '>=', $startValue)->where('rgt', '<', $brand->rgt)->whereNotIn('id', $brandWithChildren)->increment('rgt', $width);
            $decrementValue = $brand->lft - $startValue;
            Brand::whereIn('id', $brandWithChildren)->decrement('lft', $decrementValue);
            Brand::whereIn('id', $brandWithChildren)->decrement('rgt', $decrementValue);
        } else {
            Brand::where('lft', '>', $brand->rgt)->where('lft', '<', $startValue)->whereNotIn('id', $brandWithChildren)->decrement('lft', $width);
            Brand::where('rgt', '>', $brand->rgt)->where('rgt', '<', $startValue)->whereNotIn('id', $brandWithChildren)->decrement('rgt', $width);
            $incrementValue = $startValue - $brand->lft - $width;
            Brand::whereIn('id', $brandWithChildren)->increment('lft', $incrementValue);
            Brand::whereIn('id', $brandWithChildren)->increment('rgt', $incrementValue);
        }
    }

    private function getNotFulfilledStatuses()
    {
        return array(
            OrderItem::STATUS_CANCELLED,
            OrderItem::STATUS_MANIFESTED,
            OrderItem::STATUS_RETURNED,
            OrderItem::STATUS_RTO,
        );
    }

    public function getFormattedBrandStatsForMap(Request $request)
    {
        $filters = SearchHelper::getDashboardFiltersFromRequest($request)['filters'];
        $type = $request->types[0];
        $filters['type'] = $type;

        $filters['view-type'] = $request->input('view-type');

        switch ($type)
        {
            case "brand_counts":
            case "top_brands":
                $query = StatsHelper::getBrandsQuery($filters);
                break;
            case "gross_revenues":
                $query = StatsHelper::getGrossRevenueQuery($filters);
                break;
            case "order_counts":
            case "order_totals":
                $query = StatsHelper::getOrderQuery($filters);
                break;
            case "order_item_units":
                $query = StatsHelper::getOrderItemUnitsQuery($filters);
                break;
            case "rto_totals":
                $query = StatsHelper::getRtoTotalsQuery($filters);
                break;
            case "return_totals":
                $query = StatsHelper::getReturnTotalQuery($filters);
                break;
            case "top_marketers":
                $query = StatsHelper::getTopMarketersQuery($filters);
                break;
            case "top_stores":
                $query = StatsHelper::getTopStoresQuery($filters);
                break;
            case "net_revenues":
                $query = StatsHelper::getNetRevenueQuery($filters);
                break;
        }

        return $query->get();
    }

    public function getFormattedBrandMetrics(Request $request)
    {
        $filters = SearchHelper::getDashboardFiltersFromRequest($request);
        $storesQueryWithFilters = $this->getDashboardStatsQueryByDate($filters);
        $storeStatsData = $storesQueryWithFilters->get();
        return DashboardMetricsResource::collection($storeStatsData);
    }

    public function getDashboardStatsQueryByStores($filters)
    {
        $notAllowedStatuses = $this->getNotFulfilledStatuses();
        $query = Store::selectRaw('
            stores.id, stores.lat, stores.lng, stores.name, stores.beat_id,
            stores.location, stores.address,
            sum(oi.total) as total_amount,
            count(distinct orders.id) as total_orders,
            count(distinct oi.product_id) as total_products,
            count(distinct br.id) as total_brands
        ');

        $query->join("orders as orders", "orders.store_id", "=", "stores.id");
        $this->applyCommonQueryJoins($query);

        $query->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'");
        $query->whereNotIn("orders.status", $notAllowedStatuses);
        if(!empty($filters['store_ids']))
        {
            $query->whereIn("stores.id", $filters['store_ids']);
        }
        $this->applyCommonQueryFilters($query, $filters);

        $query->groupBy('stores.id');

        return $query;
    }

    public function getDashboardStatsQueryByDate($filters)
    {
        $notAllowedStatuses = $this->getNotFulfilledStatuses();

        $query = Order::selectRaw('
            date(convert_tz(orders.created_at, "+00:00", "+05:30")) as date, sum(oi.total) as total_amount,
            count(distinct orders.id) as total_orders,
            count(distinct oi.product_id) as total_products,
            count(distinct stores.id) as total_stores,
            count(distinct b.id) as total_beats,
            count(distinct br.id) as total_brands
        ');

        $query->join("stores as stores", "orders.store_id", "=", "stores.id");
        $this->applyCommonQueryJoins($query);

        $query->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'");
        $query->whereNotIn("orders.status", $notAllowedStatuses);

        $this->applyCommonQueryFilters($query, $filters);
        if(!empty($filters['store_ids']))
        {
            $query->whereIn("stores.id", $filters['store_ids']);
        }

        $query->groupBy(DB::raw('date(convert_tz(orders.created_at, "+00:00", "+05:30"))'));

        return $query;
    }

    private function applyCommonQueryJoins($query)
    {
        $query->join("order_items as oi", "oi.order_id", "=", "orders.id");
        $query->join("beats as b", "stores.beat_id", "=", "b.id");
        $query->join("products as p", "oi.product_id", "=", "p.id");
        $query->join("brands as br", "p.brand_id", "=", "br.id");
        $query->join(\DB::raw('(select product_id, collection_id from collection_product group by product_id) cp'), 'cp.product_id', '=', 'p.id');
        // $query->join("collection_product as cp", "p.id", "=", "cp.product_id");
    }

    private function applyCommonQueryFilters($query, $filters)
    {
        if(!empty($filters['product_ids']))
        {
            $query->whereIn("p.id", $filters['product_ids']);
        }

        if(!empty($filters['brand_ids']))
        {
            $query->whereIn("br.id", $filters['brand_ids']);
        }

        if(!empty($filters['beat_ids']))
        {
            $query->whereIn("b.id", $filters['beat_ids']);
        }

        if(!empty($filters['marketer_ids']))
        {
            $query->whereIn("br.marketer_id", $filters['marketer_ids']);
        }

        if(!empty($filters['collection_ids']))
        {
            $query->whereIn("cp.collection_id", $filters['collection_ids']);
        }
    }

    public function getBrandsDashboardGrid($request)
    {
        $filterData = $filters = SearchHelper::getDashboardFiltersFromRequest($request);
        $filters = $filterData['filters'];

        $filters['brands-dashboard-grid'] = $request->input('brands-dashboard-grid', []);
        if(!isset($filters['brands-dashboard-grid']['sort']))
        {
            $filters['brands-dashboard-grid']['sort'] = ["date" => "desc"];
        }

        $filters['types'] = $request->types;

        return BrandsDashboardGrid::get($filters);
    }
}
