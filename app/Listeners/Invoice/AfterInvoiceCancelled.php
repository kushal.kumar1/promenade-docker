<?php

namespace Niyotail\Listeners\Invoice;

use Niyotail\Events\Invoice\InvoiceCancelled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Niyotail\Models\Transaction;
use Niyotail\Services\TransactionService;

class AfterInvoiceCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InvoiceCancelled  $event
     * @return void
     */
    public function handle(InvoiceCancelled $event)
    {
        $invoice = $event->getInvoice();

        $transactionService=(new TransactionService())->setAuthority(TransactionService::AUTHORITY_SYSTEM);
        $transactionService->removeForCancelledInvoice($invoice);
    }
}
