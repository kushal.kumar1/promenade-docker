<?php

namespace Niyotail\Listeners\StoreOrder;

use Illuminate\Support\Facades\App;
use Niyotail\Events\StoreOrder\StoreOrderCreated;
use Niyotail\Exceptions\CreditLimitExceededException;
use Niyotail\Exceptions\OrderItemsNotFoundException;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\ConvertToSaleOrderJob;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;

class AfterStoreOrderCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param StoreOrderCreated $event
     * @return void
     * @throws ServiceException
     */
    public function handle(StoreOrderCreated $event)
    {
        $storeOrder = $event->storeOrder;
        if(App::environment() != 'local'){
            ConvertToSaleOrderJob::dispatch($storeOrder)
                ->onConnection('sqs')
                ->onQueue(config('queue.convertToSaleOrder'));
        }else{
            $orderService = App::make(OrderService::class);
            try {
                $order = $orderService->create($storeOrder);
                //TODO: Update the warehouse check from id warehouse type
                if ($order->warehouse_id == 1) {
                    (new ShipmentService())->completeDirectPurchase($order);
                }
            } catch (\Exception $ex) {
                if ($ex instanceof OrderItemsNotFoundException) {
                    $storeOrder->status = StoreOrder::STATUS_UNFULFILLED;
                    $storeOrder->save();
                } elseif ($ex instanceof CreditLimitExceededException) {
                    $storeOrder->status = StoreOrder::STATUS_UNFULFILLED;
                    $storeOrder->cancellation_reason = "Credit limit exceeded!";
                    $storeOrder->save();
                } else {
                    throw new \Exception($ex->getMessage());
                }
            }
        }
    }
}
