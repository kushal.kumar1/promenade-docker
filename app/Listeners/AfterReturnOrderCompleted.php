<?php

namespace Niyotail\Listeners;

use Illuminate\Support\Facades\App;
use Niyotail\Events\ReturnOrderCompleted;
use Niyotail\Helpers\IntegrationQueueHelper;
use Niyotail\Models\OrderItem;

class AfterReturnOrderCompleted
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(ReturnOrderCompleted $event)
    {
        $returnOrder = $event->getReturnOrder();

        $order = $returnOrder->order;
        $order->loadMissing('store.tags');
        // Store tag id 8 belongs to 1K
        $isOneKay = $order->store->tags->contains('id', 8);
        if (App::environment() == 'production' && $isOneKay) {
            $orderItems = OrderItem::selectRaw('order_items.product_id as product_id, sum(return_order_items.units) as units, sum(order_items.quantity * product_variants.quantity) as quantity')
                ->join('return_order_items', 'return_order_items.order_item_id', 'order_items.id')
                ->join('return_orders', 'return_order_items.return_order_id', 'return_orders.id')
                ->join('shipments', 'return_orders.shipment_id', 'shipments.id')
                ->join('product_variants', 'product_variants.sku', '=', 'order_items.sku')
                ->where('return_order_items.return_order_id', $returnOrder->id)
                ->where('shipments.id', $returnOrder->shipment->id)
                ->groupBy('product_id')
                ->get();

            $payload = null;
            $products = collect([]);
            foreach ($orderItems as $orderItem) {
                //NON POS Sale GID
                if ($orderItem->product->group_id != 21805) {
                    $products->push([
                        'product_id' => $orderItem->product_id,
                        'quantity' => $orderItem->units,
                    ]);
                }
            }

            if ($products->count() > 0) {
                $payload['store_id'] = $order->store_id;
                $payload['order_id'] = $order->id;
                $payload['shipment_id'] = $returnOrder->shipment->id;
                $payload['return_id'] = $returnOrder->id;
                $payload['products'] = $products;
                $payload['action'] = 'return';

                IntegrationQueueHelper::dispatch($payload)->onQueue(config('queue.integration.inventory-sync'));
            }
        }
    }
}
