<?php

namespace Niyotail\Listeners\Inventory;

use Niyotail\Events\Inventory\MallProductsInventorySync;
use Niyotail\Helpers\IntegrationQueueHelper;
use Niyotail\Models\Product;

class SyncMallProductsInventoryToNiyoos
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(MallProductsInventorySync $event)
    {
        $product = $event->getProduct();

        if (!isset($product->current_inventory)) {
            $warehouseId = 2;
            $product = Product::inventoryCount([$warehouseId])->find($product->id);
        }

        $payload = ['product_id' => $product->id, 'stock' => $product->current_inventory];
        IntegrationQueueHelper::dispatch($payload)->onQueue(config('queue.integration.mall-inventory-sync'));
     }
}
