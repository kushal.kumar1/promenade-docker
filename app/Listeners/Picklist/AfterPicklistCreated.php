<?php

namespace Niyotail\Listeners\Picklist;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;
use Niyotail\Events\Picklist\PicklistCreated;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\ProcessPicklistItemsJob;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\PicklistService;

class AfterPicklistCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     * @throws ServiceException
     */
    public function handle(PicklistCreated $event)
    {
        $picklist = $event->picklist;
        if(App::environment() == 'production'){
            ProcessPicklistItemsJob::dispatch($picklist)->onConnection('sqs')
                ->onQueue(config('queue.picklist'));
        }else{
            $picklistService = new PicklistService();
            $flatInventoryService = new FlatInventoryService();
            $orderItems = $picklist->orderItems;
            $orderService = app(OrderService::class);
            $orderItemService = app(OrderItemService::class);
            $orderService->manifestOrders($orderItems->pluck('order_id')->unique()->toArray());
            $orderItemService->massStatusUpdate($orderItems->pluck('id')->toArray(), OrderItem::STATUS_MANIFESTED);
            $flatInventoryService->markGoodInventories($orderItems->pluck('itemInventory.flat_inventory_id')->toArray(), FlatInventory::STATUS_MANIFESTED);
            $picklistService->updatePicklistStatus($picklist, Picklist::STATUS_GENERATED);
        }
    }
}
