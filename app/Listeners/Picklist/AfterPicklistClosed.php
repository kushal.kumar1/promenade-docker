<?php

namespace Niyotail\Listeners\Picklist;

use Illuminate\Support\Facades\App;
use Niyotail\Events\Picklist\PicklistClosed;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Jobs\ClosePicklistJob;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;
use Niyotail\Services\PicklistService;

class AfterPicklistClosed
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PicklistClosed $event
     * @return void
     * @throws ServiceException
     */
    public function handle(PicklistClosed $event)
    {
        $picklist = $event->picklist;
        $picklist->loadMissing('items.orderItemInventory');
        if(App::environment() == 'production'){
            ClosePicklistJob::dispatch($picklist)
                ->onConnection('sqs')
                ->onQueue(config('queue.picklist'));
        }else{
            $flatInventoryService = new FlatInventoryService();
            $picklistService = new PicklistService();
            $picklist->items->loadMissing('orderItemInventory');
            $skippedItems = $picklist->items->where('status', PicklistItem::STATUS_SKIPPED)->groupBy('skip_reason');
            $orderItemService = new OrderItemService();
            foreach ($skippedItems as $skipReason => $items) {
                $orderItemIds = $items->pluck('order_item_id')->toArray();
                $orderItemService->massStatusUpdate($orderItemIds, OrderItem::STATUS_UNFULFILLED, $skipReason);
                $inventoryIds = $items->pluck('orderItemInventory.flat_inventory_id')->toArray();
                if($skipReason == 'cancel_order_item'){
                    $flatInventoryService->markGoodInventories($inventoryIds, FlatInventory::STATUS_READY_FOR_SALE);
                }else{
                    $flatInventoryService->markBadInventories($inventoryIds, PicklistItem::INVENTORY_MAP_SKIP_REASONS[$skipReason]);
                }
            }
            $orderIds = $picklist->orderItems->unique('order_id')->pluck('order_id')->toArray();
            $orders = Order::with('picklistItems')->whereIn('id', $orderIds)->get();
            if ($orders->isNotEmpty()) {
                foreach ($orders as $order) {
                    $shipmentService = new ShipmentService();
                    $shipmentService->createFromPicklist($picklist, $order);

                    //Update Order Status
                    app(OrderService::class)->afterPicklistUpdate($order);
                }
            }

            $inventoryIds = $picklist->items->where('status', PicklistItem::STATUS_PICKED)->pluck('orderItemInventory.flat_inventory_id')->toArray();
            $flatInventoryService->markGoodInventories($inventoryIds, FlatInventory::STATUS_BILLED);
            $picklistService->updatePicklistStatus($picklist, Picklist::STATUS_CLOSED);
        }
    }
}
