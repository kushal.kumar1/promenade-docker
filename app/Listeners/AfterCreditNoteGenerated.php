<?php

namespace Niyotail\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Niyotail\Events\CreditNote\CreditNoteGenerated;
use Niyotail\Services\TransactionService;

class AfterCreditNoteGenerated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreditNoteGenerated $event)
    {
        $creditNote = $event->getCreditNote();
        //Create Transaction
        $transactionService=(new TransactionService())->setAuthority(TransactionService::AUTHORITY_SYSTEM);
        $transactionService->createForCreditNote($creditNote);
    }
}
