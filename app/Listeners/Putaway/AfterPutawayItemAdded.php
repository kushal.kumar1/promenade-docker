<?php

namespace Niyotail\Listeners\Putaway;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Niyotail\Events\Putaway\PutawayItemAdded;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Services\FlatInventoryService;

class AfterPutawayItemAdded
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PutawayItemAdded $event
     * @return void
     * @throws ServiceException
     */
    public function handle(PutawayItemAdded $event)
    {
        $putawayItem = $event->getPutawayItem();
        $originalPutawayItem = $event->getOriginalPutawayItem();
        if (empty($putawayItem->inventory_migration_item_id)) {
            $this->associateInventoryForDirectPutaway($putawayItem);
            return;
        }

        // First Move already assigned picklist (if it is an update case) inventories to hold.
        // This is not getting used as the update functionality is not yet implemented.
        $putawayItem->loadMissing('flatInventories', 'migrationItem.putawayItems');
        $putawayInventoryIds = $putawayItem->flatInventories->pluck('id')->toArray();
        if (!empty($putawayInventoryIds)) {
            (new FlatInventoryService())->markGoodInventories($putawayInventoryIds, FlatInventory::STATUS_HOLD);
        }

        //Case when excess area putaway is generated against old putaway item
        if (!empty($originalPutawayItem)) {
            $this->associateInventoryForExcessArea($putawayItem, $originalPutawayItem);
            return;
        }

        //Associate inventories with placed item
        $this->associateInventoryForMigrationItem($putawayItem);

    }

    private function associateInventoryForExcessArea($putawayItem, $originalPutawayItem)
    {
        $originalPutawayItem->loadMissing('flatInventories');
        $originalPutawayInventories = $originalPutawayItem->flatInventories->pluck('id')->toArray();
        //Event was triggered before updating quantity of original item. Calculate quantity again.
        $originalPutawayItemQuantity = $originalPutawayItem->quantity - $putawayItem->quantity;
        $originalInventories = array_slice($originalPutawayInventories, 0, $originalPutawayItemQuantity);
        $newInventories = array_slice($originalPutawayInventories, $originalPutawayItemQuantity, count($originalPutawayInventories) - 1);

        $putawayItem->flatInventories()->sync($newInventories);
        $originalPutawayItem->flatInventories()->sync($originalInventories);
    }

    private function associateInventoryForMigrationItem($putawayItem)
    {
        $putawayItem->loadMissing('migrationItem.inventories.inventory');
        $inventories = $putawayItem->migrationItem->inventories;
        $count = 0;
        $migrationItemInventories = [];
        $releaseItemInventories = [];
        foreach ($inventories as $migrationInventory) {
            if ($migrationInventory->inventory->status == FlatInventory::STATUS_HOLD) {
                if ($count >= $putawayItem->quantity) {
                    if(!empty($putawayItem->from_remarks == 'Product quantity not available for migration')) {
                      $releaseItemInventories[] = $migrationInventory->inventory->id;
                    } else
                        break;
                } else {
                    $migrationItemInventories[] = $migrationInventory->inventory->id;
                }
                $count++;
            }
        }
        $putawayItem->flatInventories()->sync($migrationItemInventories);
        //Update status of placed inventories to Putaway
        (new FlatInventoryService())->markGoodInventories($migrationItemInventories, FlatInventory::STATUS_PUTAWAY);
        if (!empty($releaseItemInventories)) {
            (new FlatInventoryService())->markGoodInventories($releaseItemInventories, FlatInventory::STATUS_READY_FOR_SALE);
        }
    }

    private function associateInventoryForDirectPutaway($putawayItem)
    {
        $inventories = FlatInventory::getInventoriesForUpdateForDirectPutaway($putawayItem->putaway->warehouse_id, $putawayItem->product_id, $putawayItem->quantity);
        $putawayItem->flatInventories()->sync($inventories->pluck('id')->toArray());
    }
}
