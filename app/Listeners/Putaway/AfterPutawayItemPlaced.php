<?php

namespace Niyotail\Listeners\Putaway;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Niyotail\Events\Putaway\PutawayItemPlaced;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\InventoryMigrationService;
use Niyotail\Services\Putaway\PutawayService;

class AfterPutawayItemPlaced
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param PutawayItemPlaced $event
     * @return void
     * @throws ServiceException
     */
    public function handle(PutawayItemPlaced $event)
    {
        $putawayItem = $event->getPutawayItem();

        // $whereCondition = ['status' => FlatInventory::STATUS_PUTAWAY, 'product_id' => $putawayItem->product_id, 'warehouse_id' => $putawayItem->putaway->warehouse_id];

        // $flatInventories = FlatInventory::where($whereCondition)->limit($putawayItem->placed_quantity)
        //    ->lockForUpdate()->get();
        // $putawayItem->flatInventories()->sync($flatInventories->pluck('id')->toArray());
        $putawayItem->loadMissing('flatInventories');
        $inventoryIds = $putawayItem->flatInventories->take($putawayItem->placed_quantity)->pluck('id')->toArray();

        (new FlatInventoryService())->markGoodInventoriesWithStorage($inventoryIds, FlatInventory::STATUS_READY_FOR_SALE, $putawayItem->storage_id);
        if (empty($putawayItem->inventory_migration_item_id)) {
            return;
        }
        $migrationItem = InventoryMigrationItem::where('id', $putawayItem->inventory_migration_item_id)
            ->with('putawayItems')->first();
        // if($migrationItem->quantity == $migrationItem->putawayItems->sum('placed_quantity')){
        (new InventoryMigrationService())->markItemStatusComplete($migrationItem);
        // }
    }
}
