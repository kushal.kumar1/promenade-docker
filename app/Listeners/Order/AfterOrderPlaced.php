<?php

namespace Niyotail\Listeners\Order;

use Niyotail\Events\Order\OrderPlaced;
use Niyotail\Services\InventoryService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Jobs\SplitBackOrder;
use Niyotail\Jobs\AssignAgentToItems;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AfterOrderPlaced
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPlaced  $event
     * @return void
     */
    public function handle(OrderPlaced $event)
    {
        $order = $event->getOrder();
    }
}
