<?php

namespace Niyotail\Listeners\Order;

use Niyotail\Events\Order\OrderCancelled;
use Niyotail\Services\InventoryService;
use Niyotail\Services\Order\OrderService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Niyotail\Services\Service;

class AfterOrderCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCancelled  $event
     * @return void
     */
    public function handle(OrderCancelled $event)
    {
        $order = $event->getOrder();
        $orderService = app(OrderService::class);
        $orderService->createLog($order, 'Order Cancelled ! Reason: '.$order->cancellation_reason);

    }
}
