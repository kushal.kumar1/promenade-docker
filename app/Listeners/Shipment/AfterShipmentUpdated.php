<?php

namespace Niyotail\Listeners\Shipment;

use Niyotail\Events\Shipment\ShipmentUpdated;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Shipment;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AfterShipmentUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShipmentUpdated $event
     * @return void
     */
    public function handle(ShipmentUpdated $event)
    {
        $shipment = $event->getShipment();
        $orderService = app(OrderService::class);
        $orderItemService = new OrderItemService();

        if($shipment->status == Shipment::STATUS_DISPATCHED) {
            $orderService->orderInTransit($shipment->order);
            $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_DISPATCHED);
        }

        if(in_array($shipment->status, [Shipment::STATUS_RETRY, Shipment::STATUS_READY_TO_SHIP])) {
            $orderItemService = new OrderItemService();
            $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_READY_TO_SHIP);
        }

        // case when shipment is detached from manifest and moved back to generated status
        if($shipment->status == Shipment::STATUS_GENERATED) {
            $orderItemService = new OrderItemService();
            $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_BILLED);
        }

        //TODO:: update order status basis shipments status
//        $orderService = app(OrderService::class);
//        $orderService->afterShipmentUpdate($order);
    }
}
