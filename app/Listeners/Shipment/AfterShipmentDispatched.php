<?php

namespace Niyotail\Listeners\Shipment;

use Niyotail\Events\Shipment\ShipmentDispatched;
use Niyotail\Jobs\SendShipmentOtp;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;

class AfterShipmentDispatched
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ShipmentDispatched $event
     * @return void
     * @throws \Niyotail\Exceptions\ServiceException
     */
    public function handle(ShipmentDispatched $event)
    {
        $shipment = $event->getShipment();
        $shipment->loadMissing('order.store', 'invoice', 'orderItems.itemInventory');
        $orderService = app(OrderService::class);
        $orderItemService = new OrderItemService();
        $orderService->orderInTransit($shipment->order);
        $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_DISPATCHED);
        if($shipment->order->type != Order::TYPE_STOCK_TRANSFER){
            (new ShipmentService())->setOtp($shipment);
            dispatch(new SendShipmentOtp($shipment));
        }
        $inventoriesToBeDispatched = $shipment->orderItems->pluck('itemInventory.flat_inventory_id')->flatten()->toArray();
        (new FlatInventoryService())->markGoodInventories($inventoriesToBeDispatched, FlatInventory::STATUS_DISPATCHED);
    }
}
