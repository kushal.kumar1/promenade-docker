<?php

namespace Niyotail\Listeners\Shipment;

use Niyotail\Events\Shipment\ShipmentGenerated;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Services\InvoiceService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AfterShipmentGenerated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShipmentGenerated  $event
     * @return void
     */
    public function handle(ShipmentGenerated $event)
    {
        $shipment = $event->getShipment();

        //1. Move all order items & order to billed status
        $shipment->load('orderItems');
        $orderItemIds = $shipment->orderItems->pluck('id')->toArray();
        (app(OrderItemService::class))->massStatusUpdate($orderItemIds, OrderItem::STATUS_BILLED);

        //2. Generate Invoice for the shipment
        (new InvoiceService())->setAuthority(OrderService::AUTHORITY_SYSTEM)->create($shipment);
    }
}
