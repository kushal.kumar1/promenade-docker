<?php

namespace Niyotail\Listeners\Shipment;

use Niyotail\Events\Shipment\ShipmentCancelled;
use Niyotail\Services\InvoiceService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;

class AfterShipmentCancelled
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ShipmentGenerated $event
     * @return void
     */
    public function handle(ShipmentCancelled $event)
    {
        $shipment = $event->getShipment();
        $orderItemService = new OrderItemService();
        $orderItemService->cancel($shipment->orderItems, "shipment cancellation");
        $invoiceService = new InvoiceService();
        $invoiceService->cancel($shipment->invoice);
//        $shipment->manifests()->detach($shipment->id);
        app(OrderService::class)->afterShipmentUpdate($shipment->order);
    }
}
