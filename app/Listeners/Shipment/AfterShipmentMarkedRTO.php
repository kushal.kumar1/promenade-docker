<?php

namespace Niyotail\Listeners\Shipment;

// use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Niyoos\Client as NiyoOSClient;
use Niyotail\Events\Shipment\ShipmentMarkedRTO;
use Niyotail\Models\OrderItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Niyotail\Services\InvoiceService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;

class AfterShipmentMarkedRTO
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(ShipmentMarkedRTO $event)
    {
        $shipment = $event->getShipment();

        $orderItemService = new OrderItemService();
        $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_RTO);
        $orderItemService->rto($shipment->orderItems->pluck('id')->toArray());
        app(OrderService::class)->afterShipmentUpdate($shipment->order);
        $invoiceService = new InvoiceService();
        $invoiceService->cancel($shipment->invoice);
    }
}
