<?php

namespace Niyotail\Listeners\Shipment;

use Illuminate\Support\Facades\App;
use Niyotail\Events\Shipment\ShipmentDelivered;
use Niyotail\Helpers\IntegrationQueueHelper;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\Store;
use Niyotail\Models\Vendor;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\PurchaseInvoiceCartService;

class AfterShipmentDelivered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(ShipmentDelivered $event)
    {
        $shipment = $event->getShipment();
        $order = $shipment->order;
        $shipment->loadMissing('orderItems.itemInventory');
        //TODO: need improvements
        app(OrderService::class)->afterShipmentUpdate($order);
        $orderItemService = new OrderItemService();
        $orderItemService->massStatusUpdate($shipment->orderItems->pluck('id')->toArray(), OrderItem::STATUS_DELIVERED);
        $inventoriesToBeDelivered = $shipment->orderItems->pluck('itemInventory.flat_inventory_id')->flatten()->toArray();
        (new FlatInventoryService())->markGoodInventories($inventoriesToBeDelivered, FlatInventory::STATUS_DELIVERED);

        //Push Inventory to queue for syncing into POS
        $order->loadMissing('store.tags');
        // Store tag id 8 belongs to 1K
        $isOneKay = $order->store->tags->contains('id', 8);
        if (App::environment() == 'production' && $isOneKay) {
            $orderItems = OrderItem::selectRaw('order_items.product_id as product_id, sum(order_items.quantity * product_variants.quantity) as quantity')
                ->join('shipment_items', 'shipment_items.order_item_id', 'order_items.id')
                ->join('shipments', 'shipment_items.shipment_id', 'shipments.id')
                ->join('product_variants', 'product_variants.sku', 'order_items.sku')
                ->where('order_items.order_id', $order->id)
                ->where('shipments.id', $shipment->id)
                ->groupBy('product_id')
                ->get();

            $payload = null;
            $products = collect([]);
            foreach ($orderItems as $orderItem) {
                //NON POS Sale GID
                if($orderItem->product->group_id != 21805) {
                    $products->push([
                        'product_id' => $orderItem->product_id,
                        'quantity' => $orderItem->quantity,
                    ]);
                }
            }

            if($products->count() > 0) {
                $payload['store_id'] = $order->store_id;
                $payload['order_id'] = $order->id;
                $payload['shipment_id'] = $shipment->id;
                $payload['products'] = $products;
                $payload['action'] = 'sale';

                IntegrationQueueHelper::dispatch($payload)->onQueue(config('queue.integration.inventory-sync'));
            }
        }

        if($order->type == Order::TYPE_STOCK_TRANSFER){
            (new PurchaseInvoiceCartService())->createCartFromShipment($shipment);
        }
    }
}
