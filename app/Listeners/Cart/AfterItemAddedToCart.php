<?php

namespace Niyotail\Listeners\Cart;

use Niyotail\Events\Cart\ItemAddedToCart;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Models\Cart;
use Niyotail\Services\Cart\CartService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;

class AfterItemAddedToCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemAddedToCart  $event
     * @return void
     */
    public function handle(ItemAddedToCart $event)
    {
        $cart=$event->getCart();
        event(new RecalculateCart($cart));

    }
}
