<?php

namespace Niyotail\Listeners\Cart;

use Niyotail\Events\Cart\ItemRemovedFromCart;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Models\CartItem;
use Niyotail\Services\Cart\CartService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;

class AfterItemRemoved
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemRemovedFromCart $event
     * @return void
     */
    public function handle(ItemRemovedFromCart $event)
    {
        $cartService = app(CartService::class);
        $cart = $event->getCart();
        $cartItemCount = CartItem::where('cart_id', $cart->id)->count();
        if ($cartItemCount > 0) {
            event(new RecalculateCart($cart));
        } else {
            $cartService->setAuthority(CartService::AUTHORITY_SYSTEM)->deleteCart($cart);
        }
    }
}
