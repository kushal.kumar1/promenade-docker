<?php

namespace Niyotail\Listeners\Cart;

use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Models\Cart;
use Niyotail\Services\Cart\CartService;

class CartToBeRecalculated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RecalculateCart  $event
     * @return void
     */
    public function handle(RecalculateCart $event)
    {
        $cartService = new CartService();
        $cart = $event->getCart();
        if($cart->source != Cart::SOURCE_WHOLESALE_CART_IMPORTER){
            $cartService->calculateItemsPrice($cart);
        }
        $cartService->calculateItemsTaxes($cart);
        $cartService->calculateItemsTotal($cart);
        $cartService->calculateShipping($cart);
        $cartService->calculateCartTotal($cart);
    }
}
