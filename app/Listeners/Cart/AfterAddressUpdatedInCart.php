<?php

namespace Niyotail\Listeners\Cart;

use Niyotail\Events\Cart\AddressUpdatedInCart;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Services\Cart\CartService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Event;

class AfterAddressUpdatedInCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AddressUpdatedInCart  $event
     * @return void
     */
    public function handle(AddressUpdatedInCart $event)
    {
        $cart=$event->getCart();
        event(new RecalculateCart($cart));
    }
}
