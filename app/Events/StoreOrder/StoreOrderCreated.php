<?php

namespace Niyotail\Events\StoreOrder;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Niyotail\Models\StoreOrder;

class StoreOrderCreated
{
    use Dispatchable, SerializesModels, InteractsWithSockets;

    public $storeOrder;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StoreOrder $order)
    {
        $this->storeOrder = $order;
    }
}