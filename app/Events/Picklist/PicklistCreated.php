<?php

namespace Niyotail\Events\Picklist;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Collection;
use Niyotail\Models\Picklist;

class PicklistCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $picklist;
    public $orderItems;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Picklist $picklist, Collection $orderItems)
    {
        $this->picklist = $picklist;
        $this->orderItems = $orderItems;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
//    public function broadcastOn()
//    {
//        return new PrivateChannel('channel-name');
//    }
}
