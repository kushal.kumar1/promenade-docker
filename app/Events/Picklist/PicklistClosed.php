<?php

namespace Niyotail\Events\Picklist;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Niyotail\Models\Picklist;

class PicklistClosed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $picklist;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Picklist $picklist)
    {
        $this->picklist = $picklist;
    }
}
