<?php

namespace Niyotail\Events\Putaway;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;

class PutawayItemPlaced
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private PutawayItem $putawayItem;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PutawayItem $putawayItem)
    {
        $this->putawayItem = $putawayItem;
    }

    public function getPutawayItem(): PutawayItem
    {
        return $this->putawayItem;
    }
}
