<?php

namespace Niyotail\Events\Putaway;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Niyotail\Models\Putaway\PutawayItem;

class PutawayItemAdded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private PutawayItem $putawayItem;

    private $originalPutawayItem;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(PutawayItem $putawayItem, $originalPutawayItem = null)
    {
        $this->putawayItem = $putawayItem;
        $this->originalPutawayItem = $originalPutawayItem;
    }

    public function getPutawayItem(): PutawayItem
    {
        return $this->putawayItem;
    }

    public function getOriginalPutawayItem()
    {
        return $this->originalPutawayItem;
    }
}
