<?php

namespace Niyotail\Events\Cart;

use Niyotail\Models\CartItem;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ItemQuantityUpdatedInCart
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $cartItem;

    public function __construct(CartItem $cartItem)
    {
        $this->cartItem = $cartItem;
    }

    public function getCartItem()
    {
        return $this->cartItem;
    }

    public function getCart()
    {
        return $this->cartItem->cart;
    }

}
