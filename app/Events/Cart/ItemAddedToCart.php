<?php

namespace Niyotail\Events\Cart;

use Niyotail\Models\Cart;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ItemAddedToCart
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function getCart()
    {
        return $this->cart;
    }
}
