<?php

namespace Niyotail\Events\Inventory;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class MallProductsInventorySync
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $product;

    public function __construct($product)
    {
        $this->product =  $product;
    }

    public function getProduct()
    {
        return $this->product;
    }
}
