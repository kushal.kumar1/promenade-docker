<?php

namespace Niyotail\Events\Shipment;

use Niyotail\Models\Shipment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ShipmentUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $shipment;

    public function __construct(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    public function getShipment(): Shipment
    {
        return $this->shipment;
    }
}
