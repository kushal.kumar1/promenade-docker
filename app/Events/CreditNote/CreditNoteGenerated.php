<?php

namespace Niyotail\Events\CreditNote;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Niyotail\Models\CreditNote;

class CreditNoteGenerated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $creditNote;

    public function __construct(CreditNote $creditNote)
    {
        $this->creditNote = $creditNote;
    }

    public function getCreditNote():CreditNote
    {
        return $this->creditNote;
    }
}
