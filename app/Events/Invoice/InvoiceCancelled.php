<?php

namespace Niyotail\Events\Invoice;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Niyotail\Models\Invoice;

class InvoiceCancelled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function getInvoice():Invoice
    {
        return $this->invoice;
    }
}
