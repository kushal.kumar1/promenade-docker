<?php

namespace Niyotail\Events;

use Niyotail\Models\ReturnOrder;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReturnOrderCompleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $returnOrder;

    public function __construct(ReturnOrder $returnOrder)
    {
        $this->returnOrder = $returnOrder;
    }

    public function getReturnOrder():ReturnOrder
    {
        return $this->returnOrder;
    }
}
