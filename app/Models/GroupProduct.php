<?php


namespace Niyotail\Models;


class GroupProduct extends Model
{
    protected $table = 'groups_products';

    public function group()
    {
        return $this->hasOne(ProductGroup::class, 'id', 'group_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
