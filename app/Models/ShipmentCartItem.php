<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class ShipmentCartItem extends Model
{
    public function shipmentCart()
    {
        return $this->belongsTo(ShipmentCart::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku');
    }

    public function originalProductVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'original_sku');
    }
}
