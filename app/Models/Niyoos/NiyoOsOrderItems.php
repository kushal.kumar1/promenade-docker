<?php


namespace Niyotail\Models\Niyoos;


use Niyotail\Models\Model;

class NiyoOsOrderItems extends Model
{
    protected $connection = 'mysql_niyoos';
    protected $table = 'order_items';

    public function vendor()
    {
        return $this->hasOne(NiyoOsProductVendor::class, 'product_id', 'product_id');
    }
}