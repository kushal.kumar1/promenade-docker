<?php


namespace Niyotail\Models\Niyoos;


use Niyotail\Models\Model;

class NiyoOsProductVendor extends Model
{
    protected $connection = 'mysql_niyoos';
    protected $table = 'product_vendor';
}