<?php


namespace Niyotail\Models\Niyoos;


use Niyotail\Models\Model;

class NiyoOsOrders extends Model
{
    protected $connection = 'mysql_niyoos';
    protected $table = 'orders';
}