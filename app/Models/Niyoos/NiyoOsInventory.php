<?php


namespace Niyotail\Models\Niyoos;


use Niyotail\Models\Model;

class NiyoOsInventory extends Model
{
    protected $connection = 'mysql_niyoos';
    protected $table = 'inventories';
}