<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Queries\ProfileQuestionOptionQueries;

class ProfileQuestionOption extends Model
{
    use ProfileQuestionOptionQueries;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeQuestionId($query, $qID)
    {
        return $query->where("question_id", $qID);
    }

    public function question()
    {
        return $this->belongsTo(ProfileQuestion::class, 'question_id');
    }

    public function storeAnswers()
    {
        return $this->hasMany(StoreAnswer::class, 'answer_id');
    }
}
