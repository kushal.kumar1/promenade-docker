<?php


namespace Niyotail\Models;


class ProductCategoryL2Map extends Model
{
    protected $table = 'product_category_l2_map';

    public function subCategory()
    {
        return $this->hasOne(CategoryL2::class, 'id', 'category_l2_id');
    }
}