<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class StoreAnswer extends Model
{
    public function question()
    {
        return $this->belongsTo(ProfileQuestion::class);
//        return $this->belongsToMany(ProfileQuestion::class);
    }

    public function answerOption()
    {
        return $this->belongsToMany(ProfileQuestionOption::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class, 'store_id');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'updated_by_agent');
    }

    public function scopeStoreId($query, $id)
    {
        return $query->where("store_id", $id);
    }
}
