<?php


namespace Niyotail\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class PosOrderItemTax extends Model
{
    use SoftDeletes;

    public function scopeItemId($q, $id) {
        return $q->where('pos_order_item_id', $id);
    }

    public function item() {
        return $this->belongsTo(PosOrderItem::class);
    }
}