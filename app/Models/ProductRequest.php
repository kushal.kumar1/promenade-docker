<?php


namespace Niyotail\Models;


use Niyotail\Queries\ProductRequestQueries;

class ProductRequest extends Model
{
    use ProductRequestQueries;

    public const STATUS_PENDING = 'pending';
    public const STATUS_CREATED = 'product_created';

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function taxes()
    {
        return $this->hasMany(Tax::class, 'tax_class_id', 'tax_class_id');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }

    public function group() {
        return $this->belongsTo(ProductGroup::class, 'group_id', 'id');
    }

    public function newCatL4() {
        return $this->belongsTo(NewCatL4::class, 'cl4_id', 'id');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param $group_id
     * @return $this
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsRequestedGroup()
    {
        return $this->is_requested_group;
    }

    /**
     * @param $is_requested_group
     * @return $this
     */
    public function setIsRequestedGroup($is_requested_group)
    {
        $this->is_requested_group = $is_requested_group;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBarcode()
    {
        return $this->barcode;
    }

    /**
     * @param $barcode
     * @return $this
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * @param $brand_id
     * @return $this
     */
    public function setBrandId($brand_id)
    {
        $this->brand_id = $brand_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCollectionId()
    {
        return $this->collection_id;
    }

    /**
     * @param $collection_id
     * @return $this
     */
    public function setCollectionId($collection_id)
    {
        $this->collection_id = $collection_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMrp()
    {
        return $this->unit_mrp;
    }

    /**
     * @param $mrp
     * @return $this
     */
    public function setMrp($mrp)
    {
        $this->unit_mrp = $mrp;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxClassId()
    {
        return $this->tax_class_id;
    }

    /**
     * @param $tax_class_id
     * @return $this
     */
    public function setTaxClassId($tax_class_id)
    {
        $this->tax_class_id = $tax_class_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOuterUnits()
    {
        return $this->outer_units;
    }

    /**
     * @param $outer_units
     * @return $this
     */
    public function setOuterUnits($outer_units)
    {
        $this->outer_units = $outer_units;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCaseUnits()
    {
        return $this->case_units;
    }

    /**
     * @param $case_units
     * @return $this
     */
    public function setCaseUnits($case_units)
    {
        $this->case_units = $case_units;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUom()
    {
        return $this->uom;
    }

    /**
     * @param $uom
     * @return $this
     */
    public function setUom($uom)
    {
        $this->uom = $uom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHsnCode()
    {
        return $this->hsn_code;
    }

    /**
     * @param $uom
     * @return $this
     */
    public function setHsnCode($hsn_code)
    {
        $this->hsn_code = $hsn_code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $uom
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAutoAssignBarcode()
    {
        return $this->auto_assign_barcode;
    }

    /**
     * @param $uom
     * @return $this
     */
    public function setAutoAssignBarcode($bool)
    {
        $this->auto_assign_barcode = $bool;
        return $this;
    }

    public function getCl4Id()
    {
        return $this->cl4_id;
    }

    public function setCl4Id($cl4_id)
    {
        $this->cl4_id = $cl4_id;
        return $this;
    }
}
