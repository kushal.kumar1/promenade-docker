<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as IlluminateModel;
use Niyotail\Helpers\MorphTo;

class Expression extends Model
{
    const MEDIUM_ADMIN = 'admin';
    const MEDIUM_AGENT = 'agent';
    const MEDIUM_RETAILER = 'retailer';

    protected function newMorphTo(Builder $query, IlluminateModel $parent, $foreignKey, $ownerKey, $type, $relation)
    {
        return new MorphTo($query, $parent, $foreignKey, $ownerKey, $type, $relation);
    }

    public function attribute()
    {
        return $this->morphTo('attribute', 'name', 'value')
            ->addTypeConstraints(['beat', 'variant', 'collection', 'brand', 'marketer']);
    }

    public function condition()
    {
        return $this->belongsTo(Condition::class);
    }

    public function scopeWhereNumber($query, $number)
    {
        $number = floatval($number);
        $query->where('value', '<=', $number)
            ->where(function ($subQuery) use ($number) {
                $subQuery->whereNull('max_value')->orWhere('max_value', '>=', $number);
            });
    }
}
