<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Queries\FilterQueries;
use Niyotail\Queries\ProductQueries;

class Product extends Model
{
    use ProductQueries, FilterQueries;

    const UOM_PC = "pc";
    const UOM_KILOGRAM = "kg";
    const UOM_LITRE = "l";
    const UOM_METRE = "m";
    const STATUS_ACTIVE = 1;

    const SYNC_TAGS = ['1K-Mall'];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeBarcode($query, $barcode)
    {
        return $query->where('barcode', $barcode);
    }

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now()->toDateTimeString());
    }

    public function scopeInStock($query, $warehouseId)
    {
        $query->whereHas('inventories', function ($query) use ($warehouseId) {
            $query->where('quantity', '>', '0')->where('warehouse_id', $warehouseId);
        });
    }

    public function scopeGroupId($q, $id) {
        return $q->where('group_id', $id);
    }

    public function variants()
    {
        return $this->hasMany(ProductVariant::class)->orderBy('value', 'desc');
    }

    public function largestVariant()
    {
        return $this->hasOne(ProductVariant::class)->orderBy('quantity', 'desc');
    }

    public function allVariants()
    {
        return $this->hasMany(ProductVariant::class)->orderBy('quantity', 'desc')->withTrashed();
    }

    public function unitVariant()
    {
        return $this->hasOne(ProductVariant::class)->where('value', 'unit')->withTrashed();
    }

    public function taxClass()
    {
        return $this->belongsTo(TaxClass::class, 'tax_class_id');
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'products_attributes')->withPivot('value');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class)->orderBy('position');
    }

    public function featuredImage()
    {
        return $this->hasOne(ProductImage::class);
    }

    public function getPrimaryImageAttribute()
    {
        if ($this->relationLoaded('images')) {
            return $this->images->first();
        }
        return null;
    }

    public function collections()
    {
        return $this->belongsToMany(Collection::class)->withPivot(['id', 'created_at']);
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    public function inventoriesV2()
    {
        return $this->hasMany(FlatInventory::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function marg()
    {
        return $this->hasOne(MargProduct::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function group()
    {
        return $this->belongsTo(ProductGroup::class);
    }

    public function newCatL4()
    {
        return $this->belongsTo(NewCatL4::class, 'cl4_id', 'id');
    }

    public function totalInventory()
    {
        return $this->hasOne(Inventory::class)
            ->active()
            ->selectRaw('product_id, sum(quantity) as total')
            ->groupBy('product_id');
    }


    public function scopeWithStockByWarehouse($query, $warehouseId)
    {
        return $query->with(['totalInventory' => function ($q) use ($warehouseId) {
            $q->where('warehouse_id', $warehouseId);
        }]);
    }

    public function getTotalInventoryAttribute()
    {
        if (!array_key_exists('totalInventory', $this->relations)) {
            throw new \InvalidArgumentException("totalInventory relation not loaded");
        }

        $related = $this->getRelation('totalInventory');
        return ($related) ? (int)$related->total : 0;
    }

//    public function isSalable($beatId)
//    {
//        //TODO: Switch to redis which supports tagging and tag it with beat_exceptions.
//        //Update required in Beat Service as well
//        return cache()->remember("$this->brand_id-$beatId", 30 * 24 * 60 * 60, function () use ($beatId) {
//            return !!!Brand::havingChildren($this->brand_id)
//                ->whereHas('beatExceptions', function ($query) use ($beatId) {
//                    $query->where('beat_id', $beatId);
//                })->count();
//        });
//    }
//
//    public function focussed()
//    {
//        return $this->hasOne(FocussedProduct::class)->where('valid_on', '=', date('Y-m-d'))->orWhereNull('valid_on');
//    }
//
//    public function isFocussed($date)
//    {
//        return (int)$this->hasOne(FocussedProduct::class)->where('valid_on', $date)->orWhereNull('valid_on')->get()->isNotEmpty();
//    }

//    public function totalInventoryForWareHouse($warehouseId)
//    {
//        if (empty($warehouseIds)) {
//            return $this->activeInventories()->where('warehouse_id', '!=', '2')->sum('quantity');
//        }
//        return $this->activeInventories()->whereIn('warehouse_id', $warehouseId)->sum('quantity');
//    }
//
//    public function activeInventories()
//    {
//        return $this->hasMany(Inventory::class)->where('status', 1)->where('quantity', '>', '0');
//    }

    public function warehouseRacks()
    {
        return $this->belongsToMany(WarehouseRack::class, 'rack_product', 'product_id', 'rack_id')->where('status', 1);
    }

    public function storages()
    {
        return $this->hasMany(Storage::class);
    }

    public function orderItemInventories()
    {
        return $this->hasManyThrough(OrderItemInventory::class, OrderItem::class);
    }

    public function taxes()
    {
        return $this->hasMany(Tax::class, 'tax_class_id', 'tax_class_id');
    }

    public function commissionTags()
    {
        return $this->belongsToMany(CommissionTag::class, 'commission_tag_product', 'product_id', 'commission_tag_id');
    }

    public function picklistItems()
    {
        return $this->hasManyThrough(PicklistItem::class, OrderItem::class);
    }

    public function totalInventoryByWarehouse($warehouseId)
    {
        return $this->hasOne(Inventory::class)
            ->selectRaw('product_id, sum(quantity) as total')
            ->where('status', 1)
            ->where('warehouse_id', $warehouseId)
            ->groupBy('product_id')
            ->get();
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'cl4_id', 'cl4_id');
    }

    public function priceMap()
    {
        return $this->hasOne(ProductPriceMap::class, 'nt_id', 'id');
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function scopeInventoriesCountByWarehouse($query, $warehouseId)
    {
        return $query->withCount(['inventories as inventories_count' => function ($inventoryQuery) use ($warehouseId) {
            $inventoryQuery->select(DB::raw('round(sum(quantity)) as inventories_count'))
                ->where('warehouse_id', $warehouseId);
        }]);
    }

    public function scopeBlockedInventoryCount($query)
    {
        $query->withCount(['orderItems as blockedInventory_count' => function ($orderItemQuery) {
            $orderItemQuery->whereIn('status', [OrderItem::STATUS_PROCESSING, OrderItem::STATUS_MANIFESTED])
                ->whereHas('order', function ($orderQuery) {
                    $orderQuery->whereIn('status', [Order::STATUS_CONFIRMED, Order::STATUS_MANIFESTED]);
                });
        }]);
    }

    public function canActivate(): bool
    {
        if (empty($this->group_id) || empty($this->cl4_id) || empty($this->barcode) || $this->variants->isEmpty() || empty($this->taxClass) || empty($this->brand_id) || empty($this->hsn_sac_code) || empty($this->uom)) {
            return false;
        }
        return true;
    }

    public function migrationItems()
    {
        return $this->hasMany(InventoryMigrationItem::class);
    }

    public function is1KMall()
    {
        if (!$this->relationLoaded('tags')) {
            $this->load('tags');
        }
        return $this->tags->where('name', self::SYNC_TAGS[0])->isNotEmpty();
    }
}
