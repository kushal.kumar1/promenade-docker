<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Niyotail\Helpers\MultiWarehouseHelper;

class WarehouseScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $multiWareHouseHelper = app(MultiWarehouseHelper::class);

        $warehouseIds = $multiWareHouseHelper->getCurrentWarehouses();

        if (!empty($warehouseIds)) {
            $builder->whereIn("{$model->getTable()}.warehouse_id", $warehouseIds);
        }
    }
}
