<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Product;
use Niyotail\Models\Inventory;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductVariant extends Model
{
    use SoftDeletes;

    const TYPE_UNIT = "unit";
    const TYPE_OUTER = "outer";
    const TYPE_CASE = "case";

    const VALID_VARIANT_TYPES = array(self::TYPE_CASE, self::TYPE_OUTER, self::TYPE_UNIT);

    const UOM_PC = "pc";
    const UOM_JAR = "jar";
    const UOM_BAG = "bag";
    const UOM_KILOGRAM = "kilogram";
    const UOM_LITRE = "litre";
    const UOM_METRE = "metre";

    protected $touches = ['product'];

    public function setSkuAttribute($value)
    {
        $this->attributes['sku'] = strtolower($value);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = strtolower($value);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function customers()
    {
        return $this->belongsToMany(User::class, 'customer_wish');
    }

    //final sale price of the sku
    public function getFinalPrice()
    {
        if (!empty($this->final_price)) {
            return $this->final_price;
        }

        return $this->price;
    }

    public function isPriceInRange($price)
    {
        return ($price >= $this->min_price) && ($price <= $this->max_price);
    }

    public function getStock(Product $product)
    {
        if ($this->product_id == $product->id) {
            return (int) ($product->total_inventory / $this->quantity);
        }

        throw new \InvalidArgumentException("Product doesnot belong to the variant");
    }

    public function isSalable(Product $product, $skipMoqCheck = false, $warehouseIds = null)
    {
        if ($this->product_id == $product->id) {
            if (empty($warehouseIds)) {
              $stock = (int) ($product->total_inventory / $this->quantity);
            } else {
              $stock = (int) ($product->regularInventoryForWarehouse($warehouseIds) / $this->quantity);
            }

            return $skipMoqCheck ? ((bool) ($stock > 0)) :  ((bool) ($stock >= $this->moq));
        }

        throw new \InvalidArgumentException("Product doesnot belong to the variant");
    }

    public function getPrettyPriceAttribute()
    {
        return number_format($this->price, 2);
    }

    public function getPrettyMaxPriceAttribute()
    {
        return number_format($this->max_price, 2);
    }

    public function getPrettyMinPriceAttribute()
    {
        return number_format($this->min_price, 2);
    }

    public function getPrettyMrpAttribute()
    {
        return number_format($this->mrp, 2);
    }

    public function isSalableForWarehouse(Product $product, $warehouseIds)
    {
        if ($this->product_id == $product->id) {
            $stock = (int) ($product->regularInventoryForWarehouse($warehouseIds) / $this->quantity);
            return (bool) ($stock >= $this->moq);
        }

        throw new \InvalidArgumentException("Product doesnot belong to the variant");
    }

    public function scopeSku($q, $sku)
    {
        return $q->where('sku', $sku);
    }

    public function scopeProduct($q, $productId)
    {
        return $q->where('product_id', $productId);
    }

    public function clubPrice()
    {
      return $this->hasOne(ClubPrice::class);
    }
}
