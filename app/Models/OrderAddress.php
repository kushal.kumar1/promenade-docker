<?php

namespace Niyotail\Models;

use Niyotail\Queries\LeadQueries;
use Niyotail\Models\Model;

class OrderAddress extends Model
{
    public function billingOrders()
    {
        return $this->hasMany(Order::class, 'billing_address_id') ;
    }

    public function shippingOrders()
    {
        return $this->hasMany(Order::class, 'shipping_address_id') ;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
