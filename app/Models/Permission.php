<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

/**
 * Class Permission
 *
 * @package Niyotail\Models
 * @author  Ankit <ankitjain0269@gmail.com>
 */
class Permission extends Model
{
    /**
     * @var string
     */
    protected $table = 'permissions';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * A permission can be applied to roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_permission');
    }
}
