<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class BeatException extends Model
{
    public function beat()
    {
        return $this->belongsTo(Beat::class);
    }                             

    public function brands()
    {
        return $this->hasMany(Brand::class);
    }
}
