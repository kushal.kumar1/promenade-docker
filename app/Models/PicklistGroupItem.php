<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class PicklistGroupItem extends Model
{
    public function picklist()
    {
        return $this->belongsTo(Picklist::class);
    }

    public function group()
    {
        return $this->belongsTo(ProductGroup::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }
}
