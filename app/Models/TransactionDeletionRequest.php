<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Gate;

class TransactionDeletionRequest extends ModificationRequest
{
    use SoftDeletes;

    protected $table = 'transaction_deletion_requests';

    public function __construct()
    {
        parent::__construct(ModificationRequest::TYPE_TRANSACTION);
        Gate::allows('transaction-modification');
    }

    /**
     * @throws \Exception
     */
    public function validateModificationRequest()
    {
        if (empty($this->delete_reason) || strlen($this->delete_reason) <= 10) {
            throw new \Exception("Failed validation for TransactionDeletionRequest! delete_reason should be at least 10 characters in length.");
        }

        if (TransactionDeletionRequest::query()
                ->whereIn('status', [
                    ModificationRequest::mapStatusToEnum(ModificationRequest::STATUS_PENDING),
                    ModificationRequest::mapStatusToEnum(ModificationRequest::STATUS_PROCESSING),
                ])
                ->where('transaction_id', $this->transaction_id)
                ->count() > 0
        ) {
            throw new \Exception("Cannot create multiple Transaction Deletion Requests for the same Transaction");
        }


    }
}
