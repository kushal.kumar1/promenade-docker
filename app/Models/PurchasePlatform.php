<?php


namespace Niyotail\Models;


class PurchasePlatform extends Model
{
    public function scopePurchase($q) {
        return $q->where('type', 'purchase');
    }

    public function scopeDiscount($q) {
        return $q->where('type', 'discount');
    }
}