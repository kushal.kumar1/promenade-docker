<?php


namespace Niyotail\Models;


class PurchaseInvoiceDebitNoteItem extends Model
{
    public function debitNote()
    {
        return $this->belongsTo(PurchaseInvoiceDebitNote::class);
    }

    public function item()
    {
        return $this->belongsTo(PurchaseInvoiceItem::class);
    }

    public function source()
    {
        return $this->morphTo();
    }
}