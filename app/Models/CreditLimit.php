<?php

namespace Niyotail\Models;

use Illuminate\Support\Facades\DB;

class CreditLimit extends Model
{
    const PARTNER_NIYOTAIL = "niyotail";
    const PARTNER_AU_BANK = "au_bank";

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
