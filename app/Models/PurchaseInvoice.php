<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;
use Niyotail\Queries\PurchaseInvoiceQueries;
use Niyotail\Queries\WarehouseFilter;

class PurchaseInvoice extends Model
{
    use PurchaseInvoiceQueries;
    use WarehouseFilter;

//    use SoftDeletes;

    const STATUS_GENERATED = 'generated';
    const STATUS_COMPLETED = 'completed';
    const STATUS_APPROVED = 'approved';

    public const TYPE_DEFAULT = 'default';
    public const TYPE_STOCK_TRANSFER = 'stock_transfer';

    public function isComplete()
    {
        return $this->status == self::STATUS_COMPLETED;
    }

    public function canBeUpdated()
    {
        return $this->status == self::STATUS_GENERATED;
    }

    public function canImportInventory()
    {
        return $this->items->where('grn_id', '==', '')->count() == 0;
    }

    public function createdBy()
    {
        return $this->belongsTo(Employee::class, 'created_by');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class);
    }

    public function purchaseCart()
    {
        return $this->belongsTo(PurchaseInvoiceCart::class, 'cart_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendorAddress()
    {
        return $this->belongsTo(VendorsAddresses::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    public function items()
    {
        return $this->hasMany(PurchaseInvoiceItem::class);
    }

    public function shortItems()
    {
        return $this->hasMany(PurchaseInvoiceShortItem::class);
    }

    public function all_grn()
    {
        return $this->hasMany(PurchaseInvoiceGrn::class);
    }

    public function debit_note()
    {
        return $this->hasOne(PurchaseInvoiceDebitNote::class);
    }

    public function platform()
    {
        return $this->belongsTo(PurchasePlatform::class);
    }

    public function discountPlatform()
    {
        return $this->belongsTo(PurchasePlatform::class, 'discount_platform_id', 'id');
    }

    public function storePurchaseInvoice()
    {
        return $this->hasOne(StorePurchaseInvoice::class);
    }

    public function scopeWherePurchaseOrder($q, $poId)
    {
        return $q->where("purchase_order_id", $poId);
    }

    public function getPrettyTaxableAttribute()
    {
        $items = $this->items;
        $shortItems = $this->shortItems;

        $total = 0;
        foreach ($items as $item) {
            $variant = $item->productVariant;
            $total += $item->total_taxable * $item->quantity * $variant->quantity;
        }

        foreach ($shortItems as $shortItem) {
            $taxInfo = $shortItem->getPrettyRates();
            $total += $taxInfo['base_rate'];
        }

        return number_format($total, 2);
    }

    public function getPrettyPostTaxDiscountAttribute()
    {
        $items = $this->items;
        $total = 0;
        foreach ($items as $item) {
            $variant = $item->productVariant;
            $total += $item->post_tax_discount * $item->quantity * $variant->quantity;
        }

        return number_format($total, 2);
    }

    public function getPrettyTotalAttribute()
    {
        return number_format($this->total, 2);
    }

    public function getPrettyRoundAttribute()
    {
        return number_format($this->round, 2);
    }

    public function getPrettyInvoiceDateAttribute()
    {
        return Carbon::parse($this->invoice_date)->format('d/M/Y');
    }

    public function canGenerateSaleOrder(): bool
    {
        return (Gate::allows('process-direct-purchase-order') && $this->status == PurchaseInvoice::STATUS_COMPLETED) && ($this->is_direct_store_purchase && empty($this->storePurchaseInvoice->assigned_id));
    }

    public function transactions(): BelongsToMany
    {
        return $this->belongsToMany(Transaction::class);
    }
}
