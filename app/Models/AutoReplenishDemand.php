<?php


namespace Niyotail\Models;


class AutoReplenishDemand extends Model
{
    protected $table = 'auto_replenish_demand';

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function group()
    {
        return $this->belongsTo(ProductGroup::class);
    }

    public function scopeIds($q, $ids)
    {
        return $q->whereIn('id', $ids);
    }

    public function isValidDemand()
    {
        return $this->status == 'pending' &&
            (!$this->pending_qty || $this->pending_qty > 0);
    }
}