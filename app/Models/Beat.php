<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Agent\AgentBeatDay;
use Niyotail\Queries\BeatQueries;

class Beat extends Model
{
    use BeatQueries;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    public function liveStores()
    {
        return $this->hasMany(Store::class)->active()->verified();
    }

    public function exceptions()
    {
        return $this->belongsToMany(Brand::class, 'beat_exceptions', 'beat_id', 'brand_id');
    }

    public function warehouses()
    {
        return $this->belongsToMany(Warehouse::class, 'beat_warehouse');
    }

    public function agentBeatDays()
    {
        return $this->hasMany(AgentBeatDay::class);
    }
}
