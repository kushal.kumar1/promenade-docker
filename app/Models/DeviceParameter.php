<?php

namespace Niyotail\Models;

class DeviceParameter extends Model
{
    protected $with = ['createdBy'];

    public function createdBy()
    {
        return $this->morphTo();
    }
}
