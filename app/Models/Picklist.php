<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Queries\WarehouseFilter;

class Picklist extends Model
{
    use WarehouseFilter;

    const STATUS_PROCESSING = 'processing';
    const STATUS_GENERATED = "generated";
    const STATUS_PICKING = "picking";
    const STATUS_PICKED = 'picked';
    const STATUS_CLOSED = "closed";
    const STATUS_CLOSING = 'closing';

    public function canClose(): bool
    {
        return ($this->items->where('status', PicklistItem::STATUS_PENDING)->count() == 0 && ($this->status != self::STATUS_CLOSED && $this->status != self::STATUS_CLOSING));
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'created_by');
    }

    /*TODO:To be removed once old picklists are assigned to picker_id (F.K relation with employees table)
    * agent_id will be replaced with picker_id.
    */
    public function agent(): BelongsTo
    {
        return $this->belongsTo(Agent::class);
    }

    public function picker(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'picker_id');
    }

    public function orderItems(): BelongsToMany
    {
        return $this->belongsToMany(OrderItem::class, 'picklist_items');
    }

    public function items(): HasMany
    {
        return $this->hasMany(PicklistItem::class);
    }

    public function shipments(): HasMany
    {
        return $this->hasMany(Shipment::class);
    }

    public function getPdfUrlAttribute(): string
    {
        $timestamp = Carbon::parse($this->created_at)->timestamp;
        return storage_path('app/picklist') . "/$this->id-$timestamp.pdf";
    }

    public function allowScan(): bool
    {
        return ($this->status != self::STATUS_CLOSED) && $this->status != self::STATUS_CLOSING;
    }

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function canMarkPicked(): bool
    {
        return ($this->items->where('status', PicklistItem::STATUS_PENDING)->count() == 0 && $this->status == self::STATUS_PICKING);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withTimestamps();
    }
}
