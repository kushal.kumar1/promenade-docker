<?php

namespace Niyotail\Models;


class TransactionsSettlement extends Model
{
    protected $table='transactions_settlement';

    public function debitTransaction()
    {
        return $this->belongsTo(Transaction::class,'debit_transaction_id');
    }

    public function creditTransaction()
    {
        return $this->belongsTo(Transaction::class,'credit_transaction_id');
    }
}
