<?php


namespace Niyotail\Models;


class CommissionTagStore extends Model
{
    public function commissionTag() {
        return $this->belongsTo(CommissionTag::class);
    }

    public function store() {
        return $this->belongsTo(Store::class);
    }
}