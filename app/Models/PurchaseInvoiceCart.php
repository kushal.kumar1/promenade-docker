<?php


namespace Niyotail\Models;


use Illuminate\Support\Carbon;
use Niyotail\Queries\WarehouseFilter;

class PurchaseInvoiceCart extends Model
{
    use WarehouseFilter;
    protected $table = "purchase_invoice_cart";

    const STATUS_DRAFT = 'pending';
    const STATUS_INVOICE_GENERATED = 'completed';

    const SOURCE_SYSTEM = 'system';
    const SOURCE_IMPORTER = 'importer';

    public const TYPE_DEFAULT = 'default';
    public const TYPE_STOCK_TRANSFER = 'stock_transfer';

    public function createdBy()
    {
        return $this->belongsTo(Employee::class,'created_by');
    }

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendorAddress()
    {
        return $this->belongsTo(VendorsAddresses::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function items()
    {
        return $this->hasMany(PurchaseInvoiceCartItems::class);
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'id', 'store_id');
    }

    public function shortItems()
    {
        return $this->hasMany(PurchaseInvoiceCartShortItem::class, 'cart_id', 'id');
    }

    public function scopeWherePurchaseOrder($q, $poId)
    {
        return $q->where("purchase_order_id", $poId);
    }

    public function scopeVendorId($q, $vendorId)
    {
        return $q->where("vendor_id", $vendorId);
    }

    public function scopePending($q)
    {
        return $q->where("status", self::STATUS_DRAFT);
    }

    public function getCartTotal() {
        $total = 0;
        $items = $this->items;
        foreach ($items as $item) {
            $total += $item->cost;
        }

        $shortItems = $this->shortItems;
        foreach ($shortItems as $item) {
            $total += $item->effective_cost;
        }

        return $total;
    }

    public function getPrettyTotalAttribute()
    {
        $total = $this->getCartTotal();
        return number_format($total, 2);
    }

    public function getPrettyRoundAttribute()
    {
        return number_format($this->round, 2);
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }
}