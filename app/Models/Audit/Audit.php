<?php

namespace Niyotail\Models\Audit;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Employee;
use Niyotail\Models\Reason;
use Niyotail\Models\Warehouse;

class Audit extends Model
{
    const STATUS_GENERATED = 'generated';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_STARTED = 'started';
    const STATUS_APPROVED = 'approved';
    const STATUS_COMPLETED = 'completed';
    const STATUS_CANCELLED = 'cancelled';

    const TYPE_PRODUCT = 'product';
    const TYPE_STORAGE = 'storage';

    private static array $statusMap = [
        Audit::STATUS_GENERATED => 0,
        Audit::STATUS_CONFIRMED => 1,
        Audit::STATUS_STARTED => 2,
        Audit::STATUS_APPROVED => 3,
        Audit::STATUS_COMPLETED => 4,
        Audit::STATUS_CANCELLED => 5
    ];

    private static array $typeMap = [
        Audit::TYPE_PRODUCT => 0,
        Audit::TYPE_STORAGE => 1
    ];

    public static function auditStatuses(): array
    {
        return array_keys(static::$statusMap);
    }

    public static function auditTypes(): array
    {
        return array_keys(static::$typeMap);
    }

    public function getStatusAttribute(int $value)
    {
        $key = array_search($value, static::$statusMap);
        if (!empty($key)) {
            return $key;
        }
        throw new Exception();
    }

    public function setStatusAttribute(string $value): Audit
    {
        if (isset(static::$statusMap[$value])) {
            $this->attributes['status'] = static::$statusMap[$value];
            return $this;
        }
        throw new Exception();
    }

    public function getTypeAttribute(int $value)
    {
        $key = array_search($value, static::$typeMap);
        if (!empty($key)) {
            return $key;
        }
        throw new Exception();
    }

    public function setTypeAttribute(string $value): Audit
    {
        if (isset(static::$typeMap[$value])) {
            $this->attributes['type'] = static::$typeMap[$value];
            return $this;
        }
        throw new Exception();
    }


    public function reason(): BelongsTo
    {
        return $this->belongsTo(Reason::class);
    }

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(
            Employee::class,
            'created_by',
            'id'
        );
    }

    public function auditItems(): HasMany
    {
        return $this->hasMany(AuditItem::class);
    }
}
