<?php

namespace Niyotail\Models\Audit;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Niyotail\Models\Reason;

class AuditItemMeta extends Model
{
    const CONDITION_GOOD = 'good';
    const CONDITION_BAD = 'bad';

    private static $conditionMap = [
        AuditItemMeta::CONDITION_GOOD => 0,
        AuditItemMeta::CONDITION_BAD => 1
    ];

    public static function auditItemMetaConditions(): array
    {
        return array_keys(static::$conditionMap);
    }

    public static function scopeWhereCondition($query, $condition)
    {
        if (isset(static::$conditionMap[$condition])) {
            return $query->where('condition', static::$conditionMap[$condition]);
        }
        throw new Exception();
    }

    public function getConditionAttribute(int $value)
    {
        $key = array_search($value, static::$conditionMap);
        if (!empty($key)) {
            return $key;
        }
        throw new Exception();
    }

    public function setConditionAttribute(string $value): AuditItemMeta
    {
        if (isset(static::$conditionMap[$value])) {
            $this->attributes['condition'] = static::$conditionMap[$value];
            return $this;
        }
        throw new Exception();
    }

    public function auditItem(): BelongsTo
    {
        return $this->belongsTo(AuditItem::class);
    }

    public function reason(): BelongsTo
    {
        return $this->belongsTo(Reason::class);
    }
}
