<?php

namespace Niyotail\Models\Audit;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Reason;
use Niyotail\Models\Storage;

class AuditItemAction extends Model
{
    const ACTION_HOLD = 'hold';
    const ACTION_EXCESS = 'excess';
    const ACTION_LOSS = 'loss';
    const ACTION_RELEASE = 'release';
    const ACTION_REPORT_BAD_STOCK = 'report_bad_stock';

    private static $actionMap = [
        AuditItemAction::ACTION_HOLD => 0,
        AuditItemAction::ACTION_EXCESS => 1,
        AuditItemAction::ACTION_LOSS => 2,
        AuditItemAction::ACTION_RELEASE => 3,
        AuditItemAction::ACTION_REPORT_BAD_STOCK => 4,
    ];

    public static function auditItemActionActions(): array
    {
        return array_keys(static::$actionMap);
    }

    public static function scopeWhereAction($query, $action)
    {
        if (isset(static::$actionMap[$action]))
            return $query->where('action', static::$actionMap[$action]);
        throw new Exception();
    }

    public function getActionAttribute(int $value): string
    {
        $key = array_search($value, static::$actionMap);
        if (!empty($key)) {
            return $key;
        }
        throw new Exception();
    }

    public function setActionAttribute(string $value): AuditItemAction
    {
        if (isset(static::$actionMap[$value])) {
            $this->attributes['action'] = static::$actionMap[$value];
            return $this;
        }
        throw new Exception();
    }


    public function auditItem(): BelongsTo
    {
        return $this->belongsTo(AuditItem::class);
    }

    public function destinationStorage(): BelongsTo
    {
        return $this->belongsTo(Storage::class, 'destination_storage_id');
    }

    public function takenBy(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function reason(): BelongsTo
    {
        return $this->belongsTo(Reason::class);
    }

    public function inventories()
    {
        return $this->belongsToMany(FlatInventory::class, AuditItem::INVENTORY_PIVOT_TABLE)
            ->withPivot(['audit_item_id']);
    }
}
