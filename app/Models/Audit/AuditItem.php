<?php

namespace Niyotail\Models\Audit;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Employee;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;

class AuditItem extends Model
{
    const INVENTORY_PIVOT_TABLE = 'audit_item_audit_item_action_flat_inventory';

    const STATUS_GENERATED = 'generated';
    const STATUS_ASSIGNED = 'assigned';
    const STATUS_PENDING_APPROVAL = 'pending_approval';
    const STATUS_PENDING_ACTION = 'pending_action';
    const STATUS_RESOLVED = 'resolved';
    const STATUS_CANCELLED = 'cancelled';

    private static $statusMap = [
        AuditItem::STATUS_GENERATED => 0,
        AuditItem::STATUS_ASSIGNED => 1,
        AuditItem::STATUS_PENDING_APPROVAL => 2,
        AuditItem::STATUS_PENDING_ACTION => 3,
        AuditItem::STATUS_RESOLVED => 4,
        AuditItem::STATUS_CANCELLED => 5
    ];

    public static function auditItemStatuses(): array
    {
        return array_keys(static::$statusMap);
    }

    public static function scopeNotCancelled($query)
    {
        return $query->where('status', '!=', static::$statusMap[static::STATUS_CANCELLED]);
    }

    public static function scopeWhereProduct($query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

    public static function scopeWhereStorage($query, $storage_id)
    {
        return $query->where('storage_id', $storage_id);
    }

    public static function scopeAssignedTo($query, $employee)
    {
        return $query->where('assigned_to', $employee->id);
    }


    public function getStatusAttribute(int $value)
    {
        $key = array_search($value, static::$statusMap);
        if (!empty($key)) {
            return $key;
        }
        throw new Exception();
    }

    public function setStatusAttribute(string $value): AuditItem
    {
        if (isset(static::$statusMap[$value])) {
            $this->attributes['status'] = static::$statusMap[$value];
            return $this;
        }
        throw new Exception();
    }

    public function audit(): BelongsTo
    {
        return $this->belongsTo(Audit::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function storage(): BelongsTo
    {
        return $this->belongsTo(Storage::class);
    }

    public function auditItemMetas(): HasMany
    {
        return $this->HasMany(AuditItemMeta::class);
    }

    public function auditItemActions(): HasMany
    {
        return $this->HasMany(AuditItemAction::class);
    }

    public function assignedTo(): BelongsTo
    {
        return $this->belongsTo(
            Employee::class,
            'assigned_to',
            'id'
        );
    }

    public function inventories(): BelongsToMany
    {
        return $this->belongsToMany(FlatInventory::class, static::INVENTORY_PIVOT_TABLE)
            ->withPivot(['audit_item_action_id']);
    }

}
