<?php


namespace Niyotail\Models;


class RateCreditNoteOrder extends Model
{
    public function order() {
        return $this->belongsTo(PosOrder::class);
    }

    public function creditNote() {
        return $this->belongsTo(RateCreditNote::class);
    }
}