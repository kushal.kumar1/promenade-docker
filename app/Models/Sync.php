<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Product;
use Niyotail\Queries\AttributeQueries;

class Sync extends Model
{
    public $timestamps = false;

    const TYPE_PRODUCTS = "products";
    const TYPE_PRICES = "prices";
}
