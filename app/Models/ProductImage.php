<?php

namespace Niyotail\Models;

class ProductImage extends Model
{

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
