<?php

namespace Niyotail\Models;

use Niyotail\Queries\PincodeQueries;

class Pincode extends Model
{
    use PincodeQueries;

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function warehouses()
    {
        return $this->belongsToMany(Warehouse::class, 'pincode_warehouse');
    }
}
