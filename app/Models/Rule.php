<?php

namespace Niyotail\Models;

use DateTime;
use DateTimeZone;
use Niyotail\Models\RuleTarget;
use Niyotail\Models\Model;

class Rule extends Model
{
    const TYPE_ORDER = "order";
    const TYPE_PRODUCT = "product";
    const TYPE_PRICING = "pricing";

    const OFFER_PERCENTAGE = "percentage";
    const OFFER_FLAT = "flat";
    const OFFER_FREE = "free";
    const OFFER_NEW_PRICE = "new_price";
    const OFFER_MARKUP = "markup";
    const OFFER_MARGIN = "margin";

    public function scopeIsActive($query)
    {
        $currentDateTime = (new DateTime("now", new DateTimeZone('Asia/Kolkata')))->format('Y-m-d H:i:s');
        $query->where('rules.status', 1)->where('rules.start_at', '<=', $currentDateTime)
            ->where(function ($subQuery) use ($currentDateTime) {
                $subQuery->whereNull('rules.end_at')->orWhere('rules.end_at', '>=', $currentDateTime);
            });
    }

    public function scopeIsUnderLimit($query, $storeId)
    {
        $query->whereHas('orderItems', function ($orderItemQuery) {
            $orderItemQuery->selectRaw('count(distinct order_items.order_id) as orders_count')
                ->whereDoesntHave('order', function ($orderQuery) {
                    $orderQuery->where('status', Order::STATUS_CANCELLED);
                })
                ->havingRaw('rules.total_limit is null or rules.total_limit > orders_count');
        })->whereHas('orderItems', function ($orderItemQuery) use ($storeId) {
            $orderItemQuery->selectRaw('count(distinct order_items.order_id) as store_orders_count')
                ->whereDoesntHave('order', function ($orderQuery) use ($storeId) {
                    $orderQuery->where('store_id', '!=', $storeId)
                        ->orWhere('status', Order::STATUS_CANCELLED);
                })
                ->havingRaw('rules.user_limit is null or rules.user_limit > store_orders_count');
        });
    }

    public function conditions()
    {
        return $this->hasMany(Condition::class);
    }

    public function targets()
    {
        return $this->hasMany(RuleTarget::class);
    }

    public function orderItems()
    {
        return $this->belongsToMany(OrderItem::class, 'order_item_rule');
    }
}
