<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AdditionalOrderInfo extends Model
{
    public const TYPE_INTERSTATE = 'inter-state';
    public const TYPE_INTRASTATE = 'intra-state';

    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

    public function tsmEmployee(): BelongsTo
    {
        return $this->belongsTo(Employee::class, 'tsm_employee_id');
    }
}
