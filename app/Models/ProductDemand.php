<?php

namespace Niyotail\Models;

use Niyotail\Queries\WarehouseFilter;

class ProductDemand extends Model
{
    use WarehouseFilter;

    const STATUS_INITIATED = 'initiated';
    const STATUS_PROCESSING = 'processing';
    const STATUS_GENERATED = 'generated';
    const STATUS_FINALIZED = 'finalized';
    const STATUS_PO_GENERATED = 'po_generated';
    const VENDOR_ASSIGNED = 'vendor_assigned';

    const CHANNEL_PRIMARY = 'primary';
    const CHANNEL_SECONDARY = 'secondary';

    const SOURCE_SYSTEM = 'system';
    const SOURCE_IMPORTER = 'importer';

    public function canBeUpdated()
    {
        return $this->status == self::STATUS_PROCESSING;
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function purchaseOrders()
    {
        return $this->hasOne(PurchaseOrder::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function calL1()
    {
        return $this->hasOne(NewCatL1::class, 'id', 'cl1_id');
    }

    public function calL2()
    {
        return $this->hasOne(NewCatL2::class, 'id', 'cl2_id');
    }

    public function calL3()
    {
        return $this->hasOne(NewCatL3::class, 'id', 'cl3_id');
    }

    public function calL4()
    {
        return $this->hasOne(NewCatL4::class, 'id', 'cl4_id');
    }

    public function scopeId($q, $val)
    {
        return $q->where('id', $val);
    }

    public function scopeInitiated($q)
    {
        return $q->where('status', self::STATUS_INITIATED);
    }

    public function scopeProcessing($q)
    {
        return $q->where('status', self::STATUS_PROCESSING);
    }

    public function items()
    {
        return $this->hasMany(ProductDemandItem::class);
    }
}
