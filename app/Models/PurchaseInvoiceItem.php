<?php

namespace Niyotail\Models;

use Niyotail\Queries\PurchaseInvoiceItemQueries;

class PurchaseInvoiceItem extends Model
{
    use PurchaseInvoiceItemQueries;

    public function itemTaxes()
    {
        return $this->hasMany(PurchaseInvoiceItemTax::class);
    }

    public function purchaseOrderItem()
    {
        return $this->hasOne(PurchaseOrderItem::class, 'purchase_order_item_id', 'id');
    }

    public function purchaseInvoice()
    {
        return $this->belongsTo(PurchaseInvoice::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function grnItems()
    {
        return $this->hasMany(PurchaseInvoiceGrnItem::class, 'item_id', 'id');
    }

    public function debitNoteItems() {
        return $this->morphOne(PurchaseInvoiceDebitNoteItem::class, 'source');
    }

    public function productRequest()
    {
        return $this->belongsTo(ProductRequest::class, 'product_id', 'id');
    }

    public function getPrettyInvoiceCostAttribute()
    {
        return number_format($this->total_invoiced_cost, 2);
    }

    public function scopeId($q, $id)
    {
        return $q->where("id", $id);
    }

    public function scopeInvoice($q, $id)
    {
        return $q->where("purchase_invoice_id", $id);
    }

    public function scopeNotRequested($q)
    {
        return $q->where("is_requested", 0);
    }

    public function getMaxGrnQty()
    {
        $items = $this->grnItems;
        $total = 0;
        if($items->count())
        {
            $total = $items->sum('quantity');
        }
        return $this->quantity - $total;
    }
}
