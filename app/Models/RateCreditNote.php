<?php


namespace Niyotail\Models;

class RateCreditNote extends Model
{
    const STATUS_GENERATED = "generated";
    const STATUS_CANCELLED = "cancelled";

    const TYPE_DISCOUNT = 'discount';
    const TYPE_PROMOTION = 'promotion';
    const TYPE_COMMISSION = 'commission';

    public function store() {
        return $this->belongsTo(Store::class);
    }

    public function orders() {
        return $this->belongsToMany(PosOrder::class)->withPivot('rate_credit_note_orders');
    }

    public function transaction()
    {
        return $this->morphOne(Transaction::class, 'source');
    }
}