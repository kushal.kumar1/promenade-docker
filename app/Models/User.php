<?php

namespace Niyotail\Models;

use Niyotail\Auth\OtpAuthenticateContract;
use Niyotail\Queries\UserQueries;
use Niyotail\Models\AuthUser;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends AuthUser implements OtpAuthenticateContract, JWTSubject
{
    use UserQueries;

    protected $guarded = ['id'];

    protected $hidden = ['otp', 'otp_expiry', 'otp_sent_count'];

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucwords($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucwords($value);
    }

    // public function orders()
    // {
    //     return $this->hasMany(Order::class);
    // }

    public function stores()
    {
        return $this->belongsToMany(Store::class, 'store_user');
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function badges()
    {
        return $this->belongsToMany(Badge::class, 'badge_user');
    }

    public function wishlist()
    {
        return $this->belongsToMany(ProductVariant::class, 'user_wish');
    }

    public function storeCredits()
    {
        return $this->hasMany(StoreCredit::class);
    }

    public function storeCreditSummary()
    {
        return $this->hasOne(StoreCredit::class)
            ->selectRaw("user_id, SUM(CASE WHEN type='credit' THEN amount ELSE 0 END) as credit, SUM(CASE WHEN type='debit' THEN amount ELSE 0 END) as debit")
            ->groupBy('customer_id');
    }

    public function getStoreCreditBalance()
    {
        return (StoreCredit::credit()->where('user_id', $this->id)->sum('amount') - StoreCredit::debit()->where('user_id', $this->id)->sum('amount'));
    }

    public function getStoreCreditBalanceAttribute()
    {
        if ($this->relationLoaded('storeCreditSummary') && !empty($this->storeCreditSummary)) {
            return $this->storeCreditSummary->credit - $this->storeCreditSummary->debit;
        }
        return 0;
    }

    public function getOtp()
    {
        return $this->otp;
    }

    public function getOtpExpiry()
    {
        return $this->otp_expiry;
    }

    public function isVerified()
    {
        return $this->verified;
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isNew()
    {
        return !!empty($this->first_name);
    }

    public function carts()
    {
        return $this->morphMany(Cart::class, 'created_by');
    }

    public function orders()
    {
        return $this->morphMany(Order::class, 'created_by');
    }

    public function deviceParams()
    {
        return $this->morphMany(DeviceParameter::class, 'created_by');
    }
}
