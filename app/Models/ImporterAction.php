<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Employee;

class ImporterAction extends Model
{
    const STATUS_PENDING = "pending";
    const STATUS_COMPLETED = "completed";
//    const STATUS_FAILED = "failed";

    const TYPE_PRODUCT = 'product';
    const TYPE_VARIANT = 'variant';
    const TYPE_CART = 'cart';
    const TYPE_VENDOR_CART = 'vendor-cart';
    const TYPE_VENDOR_MARKETER_MAP = 'vendor-marketer-map';
    const TYPE_PRODUCT_GROUP = 'product-group';
    const TYPE_MARKETER = 'marketer';
    const TYPE_BRAND = 'brand';
    const TYPE_NEW_CAT_L1 = 'cl_1';
    const TYPE_NEW_CAT_L2 = 'cl_2';
    const TYPE_NEW_CAT_L3 = 'cl_3';
    const TYPE_NEW_CAT_L4 = 'cl_4';
    const TYPE_CL4_COST_MAP = 'cl4-cost-map';

    const TYPE_WHOLESALE_CART_IMPORTER = 'wholesale-cart-importer';
    const TYPE_STOCK_TRANSFER_OUTBOUND = 'stock-transfer-outbound';
    const TYPE_STOCK_TRANSFER_SENT_IMPORTER = 'stock-transfer-sent-importer';
    const TYPE_STOCK_TRANSFER_INBOUND = 'stock-transfer-inbound';
    const TYPE_INVENTORY_MIGRATION = 'inventory-migration';
    const TYPE_STORE_CREDIT_LIMIT = 'store-credit-limit';
    const TYPE_WAREHOUSE_STORAGE = 'warehouse-storage';
    const TYPE_INVOICE_ZIP = 'invoice-zip';
    const TYPE_PRODUCT_DEMAND = 'product-demand';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function resource()
    {
        return $this->morphTo();
    }

    public function getOutPutFile()
    {

    }
}
