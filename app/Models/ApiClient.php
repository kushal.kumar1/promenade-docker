<?php

namespace Niyotail\Models;

class ApiClient extends Model
{
      protected $table = 'api_clients';

      protected $primaryKey = 'id';

      protected $guarded = ['id'];

}
