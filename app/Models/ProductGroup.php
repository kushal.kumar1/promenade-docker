<?php

namespace Niyotail\Models;

class ProductGroup extends Model
{
    protected $table = 'product_groups';

    public function warehouseAbc()
    {
        return $this->hasOne(WhGroupAbc::class, 'group_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'group_id', 'id');
    }

    public function brand() {
        return $this->belongsTo(Brand::class);
    }

    public function collection() {
        return $this->belongsTo(Collection::class);
    }

    public function requestedProducts() {
        return $this->hasMany(ProductRequest::class, 'group_id', 'id');
    }
}
