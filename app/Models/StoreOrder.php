<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Str;
use Niyotail\Models\Model;
use Niyotail\Queries\WarehouseFilter;

class StoreOrder extends Model
{
    use WarehouseFilter;

    const STATUS_PENDING = 'pending';
    const STATUS_PROCESSING = 'processing';
    const STATUS_PROCESSED = 'processed';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_UNFULFILLED = 'unfulfilled';

    const SOURCE_APP = "app";
    const SOURCE_AGENT_APP = "agent_app";
    const SOURCE_SYSTEM = "system";
    const SOURCE_IMPORTER = "importer";
    const SOURCE_STORE_PURCHASE = "store_purchase";
    const SOURCE_POS = "pos";
    const SOURCE_ARS = "ars";
    const SOURCE_WHOLESALE_IMPORTER = 'wholesale_importer';

    const TAG_1K_MALL = "1k_mall";
    const TAG_ARS = "ars";
    const TAG_NEW_STORE = "new_store";


    const TYPE_RETAIL = 'retail';
    const TYPE_STOCK_TRANSFER = 'stock_transfer';
    const TYPE_WHOLESALE = 'wholesale';


    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function items()
    {
        return $this->hasMany(StoreOrderItem::class);
    }

    public function canCancel()
    {
        return $this->status == self::STATUS_PENDING;
    }

    public function getPrettyTotalAttribute()
    {
        return number_format($this->total, 2);
    }

    public function getPrettySubtotalAttribute()
    {
        return number_format($this->subtotal, 2);
    }

    public function getPrettyShippingChargesAttribute()
    {
        return number_format($this->shipping_charges, 2);
    }

    public function getPrettyDiscountAttribute()
    {
        return number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute()
    {
        return number_format($this->tax, 2);
    }

    public function scopeCreatedByUser($query, $createdBy)
    {
        $query->where('created_by_type', array_search(get_class($createdBy), self::$map))
            ->where('created_by_id', $createdBy->id);
    }

    public function storePurchaseInvoice(): MorphOne
    {
        return $this->morphOne(StorePurchaseInvoice::class, 'assignedOrder');
    }

    public function is1KMallOrder($additionalInfo)
    {
        if (!empty($additionalInfo)) {
            $data = (array)$additionalInfo->data;
            if (!empty($data['pos_order_id'])) {
                return Str::contains($data['pos_order_id'], 'O1KM-');
            }
        }
        return false;
    }
}
