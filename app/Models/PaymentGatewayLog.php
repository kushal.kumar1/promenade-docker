<?php

namespace Niyotail\Models;

class PaymentGatewayLog extends Model
{
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
