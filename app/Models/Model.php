<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use DB;
use DateTime;
use DateTimeZone;
use ReflectionClass;
use Niyotail\Observer\ModelObserver;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use MongoDB\BSON\UTCDateTime;

class Model extends EloquentModel
{
    public static $map = [
        'order' => Order::class,
        'cart' => Cart::class,
        'invoice' => Invoice::class,
        'credit_note' => CreditNote::class,
        'rate_credit_note' => RateCreditNote::class,
        'return_order' => ReturnOrder::class,
        'employee' => Employee::class,
        'user' => User::class,
        'beat' => Beat::class,
        'variant' => ProductVariant::class,
        'agent' => Agent::class,
        'product' => Product::class,
        'inventory' => Inventory::class,
        'brand' => Brand::class,
        'marketer' => Marketer::class,
        'collection' => Collection::class,
        'grn_item' => PurchaseInvoiceGrnItem::class,
        'stock_transfer_item' => StockTransferItem::class,
        'store_order' => StoreOrder::class,
    ];

    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    public static function getClass()
    {
        return get_called_class();
    }

    public static function getConstants($key)
    {
        $key = strtoupper($key);
        $class = new ReflectionClass(static::getClass());
        $constants = array_filter($class->getConstants(), function ($constant) use ($key) {
            return substr($constant, 0, strlen($key)) == $key;
        }, ARRAY_FILTER_USE_KEY);
        return array_combine($constants, $constants);
    }

    public static function getEnum($column)
    {
        $table = (new static)->getTable();
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }

    public function getCreatedAtAttribute($value)
    {
        return (new DateTime($value))
            ->setTimezone(new DateTimeZone('Asia/Kolkata'))
            ->format("Y-m-d H:i:s");
    }

    public function getUpdatedAtAttribute($value)
    {
        return (new DateTime($value))
            ->setTimezone(new DateTimeZone('Asia/Kolkata'))
            ->format("Y-m-d H:i:s");
    }

    public function getPrettyCreatedAtAttribute()
    {
        return (new DateTime($this->created_at))->format('F d, Y \a\t h:i A');
    }

    public function unixTimestampToDate($timestamp, $format = 'd M, Y \a\t h:i A')
    {
        return Carbon::createFromTimestamp($timestamp, 'Asia/Kolkata')->format($format);
    }
}
