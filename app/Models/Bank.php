<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    use SoftDeletes;

    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }

    public function cheques()
    {
        return $this->hasMany(ChequeMeta::class);
    }

}