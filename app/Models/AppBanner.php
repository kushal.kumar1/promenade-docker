<?php

namespace Niyotail\Models;

class AppBanner extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
