<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Storage;

class City extends Model
{
    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
