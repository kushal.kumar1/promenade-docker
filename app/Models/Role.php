<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

/**
 * Class Role
 *
 * @package Niyotail\Models
 * @author  Ankit <ankitjain0269@gmail.com>
 */
class Role extends Model
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * A role may be given various permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission');
    }

    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'employee_role');
    }
}
