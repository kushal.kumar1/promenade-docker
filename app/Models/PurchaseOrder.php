<?php

namespace Niyotail\Models;


class PurchaseOrder extends Model
{
    const STATUS_PENDING='pending';
    const STATUS_PLACED='placed';
    const STATUS_INVOICE_CART_GENERATED='invoice_cart_generated';
    const STATUS_INVOICE_GENERATED='invoice_generated';

    public function isEditable()
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function createdBy()
    {
        return $this->belongsTo(Employee::class, 'created_by');
    }

    public function items()
    {
        return $this->hasMany(PurchaseOrderItem::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function vendorAddress()
    {
        return $this->hasOne(VendorsAddresses::class, 'id', 'vendor_address_id');
    }

    public function warehouse()
    {
        return $this->hasOne(Warehouse::class, 'id', 'warehouse_id');
    }

    public function purchaseInvoices()
    {
        return $this->hasMany(PurchaseInvoice::class, 'purchase_order_id', 'id');
    }
}
