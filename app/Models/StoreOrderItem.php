<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class StoreOrderItem extends Model
{
    public function storeOrder()
    {
        return $this->belongsTo(StoreOrder::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function getPrettyTotalAttribute()
    {
        return number_format($this->total, 2);
    }

    public function getPrettySubtotalAttribute()
    {
        return number_format($this->subtotal, 2);
    }

    public function getPrettyDiscountAttribute()
    {
        return number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute()
    {
        return number_format($this->tax, 2);
    }

    public function getPrettyPriceAttribute()
    {
        return number_format($this->price, 2);
    }

    public function getPrettyMrpAttribute()
    {
        return number_format($this->mrp, 2);
    }
}
