<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Model;
use Niyotail\Models\Vendor;

class VendorBankAccount extends Model
{
    protected $fillable = [
        'name', 'status', 'bank', 'vendor_id', 'ifsc', 'number', 'type', 'icici_beneficiary_id',
        'icici_beneficiary_nickname', 'valid_from', 'valid_to'
    ];

    public function vendor ()
    {
        return $this->belongsTo(Vendor::class);
    }
}
