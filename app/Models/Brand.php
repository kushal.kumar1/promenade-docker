<?php

namespace Niyotail\Models;

use Illuminate\Support\Facades\DB;
use Niyotail\Queries\BrandQueries;

class Brand extends Model
{
    use BrandQueries;

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function beatExceptions()
    {
        return $this->hasMany(BeatException::class);
    }

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function scopeWithRootParent($query)
    {
        $query->selectRaw("*, (SELECT id FROM {$this->getTable()} parent_brands WHERE parent_brands.lft < {$this->getTable()}.lft AND parent_brands.rgt > {$this->getTable()}.rgt ORDER BY parent_brands.lft ASC limit 1) AS parent_id");
    }

    public function scopeHavingParents($query,$parentIds)
    {
        $query->whereExists(function ($query) use ($parentIds) {
            $query->select(DB::raw(1))
                ->from("{$this->getTable()} as parent")
                ->whereRaw("parent.lft < {$this->getTable()}.lft")
                ->whereRaw("parent.rgt > {$this->getTable()}.rgt")
                ->whereIn('parent.id', $parentIds)
                ->orderBy('parent.lft')
                ->limit(1);
        })->orWhereIn('id', $parentIds);
    }

    public function scopeHavingChildren($query, $childId)
    {
        $query->whereExists(function ($query) use ($childId) {
            $query->select(DB::raw(1))
                ->from("{$this->getTable()} as child")
                ->whereRaw("child.lft BETWEEN {$this->getTable()}.lft AND {$this->getTable()}.rgt")
                ->where('child.id', $childId);
        })->orWhere('id', $childId);
    }

    public function isLeafNode()
    {
        return !!($this->lft + 1 == $this->rgt);
    }

    public function isChildOf($parentBrand)
    {
        return !!($this->lft > $parentBrand->lft && $this->rgt < $parentBrand->rgt);
    }

    public function getWidth()
    {
        return $this->rgt - $this->lft + 1;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeVisible($query)
    {
        return $query->where('is_visible', 1);
    }
}
