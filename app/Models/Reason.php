<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model
{
    const SOURCE_AUDIT = 'audit';
    const REASON_CREATE = 'create';

    public static function scopeWhereSource($query, $sourceType)
    {
        return $query->where('source_type', $sourceType);
    }

    public static function scopeWhereReason($query, $reasonType)
    {
        return $query->where('reason_type', $reasonType);
    }
}
