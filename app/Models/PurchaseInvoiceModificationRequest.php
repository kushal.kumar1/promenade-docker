<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use Niyotail\Helpers\File;

class PurchaseInvoiceModificationRequest extends ModificationRequest
{
    use SoftDeletes;

    protected static $storagePath = 'app/modification_request/purchase_invoice';
    protected $table = 'purchase_invoice_modification_requests';

    public function __construct()
    {
        parent::__construct(self::TYPE_PURCHASE_INVOICE);

        Gate::allows('purchase-invoice-modification');
    }

    /**
     * @throws Exception
     */
    public static function uploadPurchaseInvoiceImage(UploadedFile $purchaseInvoiceImage)
    {
        $uploadedImage = new File($purchaseInvoiceImage);
        $targetDirectory = storage_path(static::$storagePath);
        if (!is_dir($targetDirectory)) {
            if (!mkdir($targetDirectory)) {
                throw new Exception ("Unable to create directory: {$targetDirectory}");
            }
        }

        $uploadedImage->setName(uniqid() . "." . $purchaseInvoiceImage->guessExtension())
            ->setDirectory($targetDirectory)
            ->save();

        return $uploadedImage->getName();
    }

    public function getPurchaseInvoiceImagePath()
    {
        if (empty($this->image)) {
            return null;
        }

        $purchaseInvoiceImagePath = storage_path(static::$storagePath . DIRECTORY_SEPARATOR . $this->image);

        // TODO check if file exists

        return $purchaseInvoiceImagePath;
    }

    /**
     * @throws \Exception
     */
    public function validateModificationRequest()
    {
        PurchaseInvoice::query()->findOrFail($this->purchase_invoice_id);

        switch ($this->action) {
            case ModificationRequest::ACTION_EDIT:
            {
                Vendor::query()->findOrFail($this->vendor_id);
                VendorsAddresses::query()
                    ->where('vendor_id', $this->vendor_id)
                    ->findOrFail($this->vendor_address_id);

                if (empty($this->date)) {
                    throw new \Exception("Failed validation for PurchaseInvoiceModificationRequest! date is required.");
                }

                if (empty(PurchasePlatform::query()->find($this->platform_id))) {
                    throw new \Exception("Failed validation for PurchaseInvoiceModificationRequest! platform is required.");
                }

                return;
            }
            case ModificationRequest::ACTION_DELETE:
            {
                if (empty($this->delete_reason) || strlen($this->delete_reason) <= 10) {
                    throw new \Exception("Failed validation for PurchaseInvoiceModificationRequest! delete_reason should be atleast 10 characters in length.");
                }
                return;
            }
            default:
            {
                throw new Exception("Invalid value for PurchaseInvoiceModificationRequest->action: {{$$this->action}}");
            }
        }
    }

    /**
     * /**
     * @throws Exception
     * @throws \Throwable
     */
    public function setPurchaseInvoiceImage($uploadedImageName)
    {
        $targetDirectory = storage_path(static::$storagePath);
        if (!file_exists($targetDirectory . DIRECTORY_SEPARATOR . $uploadedImageName)) {
            throw new Exception ("Invoice Image Upload failed, try again");
        }

        $this->image = $uploadedImageName;
        $this->saveOrFail();
    }

}