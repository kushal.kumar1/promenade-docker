<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Model;

class ChequeMeta extends Model
{
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }
}
