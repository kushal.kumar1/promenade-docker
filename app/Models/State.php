<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Storage;

class State extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
