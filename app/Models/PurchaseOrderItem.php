<?php

namespace Niyotail\Models;


class PurchaseOrderItem extends Model
{
    public function demandItem() {
        return $this->belongsTo(ProductDemandItem::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function getPrettyVariants()
    {
        $remainingQty = $orderQuantity = $this->quantity;
        $product = $this->product;
        $allVariants = $product->allVariants;
        $skuDivision = array();

        foreach ($allVariants as $variant) {
            $variantUnits = $variant->quantity;
            $remainingQty = fmod($orderQuantity, $variantUnits);
            $qtyFulfilledByVariant = $orderQuantity - $remainingQty;
            $orderQuantity = $orderQuantity - ($orderQuantity - $remainingQty);
            $thisQty = ($qtyFulfilledByVariant)/$variantUnits;
            if($thisQty) {
                $skuDivision[$variant->value]['product'] = $product->name;
                $skuDivision[$variant->value]['sku'] = $variant->sku;
                $skuDivision[$variant->value]['qty'] = $thisQty;
                $skuDivision[$variant->value]['units'] = $qtyFulfilledByVariant;
                $skuDivision[$variant->value]['cost'] = $this->cost_per_unit;
                $skuDivision[$variant->value]['total'] = $this->cost_per_unit*$qtyFulfilledByVariant;
            }
            if(!$remainingQty) break;
        }

        return $skuDivision;
    }
}
