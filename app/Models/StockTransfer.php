<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class StockTransfer extends Model
{
    public const STATUS_INITIATED = 'initiated';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUS_TRANSFERRING = 'transferring';

    public function items(): HasMany
    {
        return $this->hasMany(StockTransferItem::class);
    }

    public function fromWarehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'from_warehouse_id');
    }

    public function toWarehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class, 'to_warehouse_id');
    }

    public function canConfirm(): bool
    {
        return $this->items->isNotEmpty() && (($this->status != self::STATUS_CONFIRMED && $this->status != self::STATUS_COMPLETED) && $this->status != self::STATUS_CANCELLED);
    }

    public function canComplete(): bool
    {
        return $this->status == self::STATUS_CONFIRMED;
    }

    public function transferNote(): HasOne
    {
        return $this->hasOne(StockTransferNote::class);
    }
}
