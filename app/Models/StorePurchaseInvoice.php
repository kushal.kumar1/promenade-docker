<?php


namespace Niyotail\Models;


class StorePurchaseInvoice extends Model
{
    public function purchaseInvoice()
    {
        return $this->belongsTo(PurchaseInvoice::class);
    }

    public function storeOrder()
    {
        return $this->belongsTo(StoreOrder::class);
    }

    public function assignedOrder()
    {
        return $this->morphTo('assigned', 'assigned_type');
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}