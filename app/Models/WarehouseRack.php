<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Storage;

class WarehouseRack extends Model
{
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function products()
    {
      return $this->belongsToMany(Product::class, 'rack_product', 'rack_id', 'product_id');
    }
}
