<?php

namespace Niyotail\Models;

use Niyotail\Queries\CartQueries;

class Cart extends Model
{
    use CartQueries;

    const SOURCE_SYSTEM = "system";
    const SOURCE_APP = "app";
    const SOURCE_AGENT_APP = "agent_app";
    const SOURCE_POS = "pos";
    const SOURCE_ARS = "ars";
    const SOURCE_IMPORTER = "importer";
    const SOURCE_WHOLESALE_CART_IMPORTER = "wholesale_cart_importer";
    const SOURCE_STORE_PURCHASE = "store_purchase";

    const TAG_1K_MALL = "1k_mall";
    const TAG_ARS = "ars";
    const TAG_NEW_STORE = "new_store";

    const TYPE_RETAIL = 'retail';
    const TYPE_STOCK_TRANSFER = 'stock_transfer';

    protected $with = ['createdBy'];

    public function scopeWithoutStoreOrder($query)
    {
        $query->whereNull('store_order_id');
    }

    public function scopeByEmployee($query, $employee = null)
    {
        $query->withoutStoreOrder()
            ->where('created_by_type', array_search(Employee::class, self::$map))
            ->where('created_by_id', $employee->id);
    }

    public function scopeByUser($query, User $user)
    {
        $query->withoutStoreOrder()
              ->where('created_by_type', array_search(User::class, self::$map))
              ->where('created_by_id', $user->id);
    }

    public function scopeByAgent($query, Agent $agent)
    {
        $query->withoutStoreOrder()
            ->where('created_by_type', array_search(Agent::class, self::$map))
            ->where('created_by_id', $agent->id);
    }

    public function isCreatedBy($class)
    {
        return $this->created_by_type == array_search($class, self::$map);
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    // Transform items to include free items in a particular structure to use in api and backend
    public function getModifiedItemsAttribute()
    {
        if (! array_key_exists('items', $this->relations)) {
            throw new \InvalidArgumentException("Items relation not loaded");
        }

        $items = $this->items;
            //->whereIn('source', [CartItem::SOURCE_MANUAL]);
//        foreach ($items as $item) {
//            $freeItems = $this->items->where('source', CartItem::SOURCE_OFFER)->where('sku', $item->sku);
//            $item->free_items = $freeItems;
//        }
        return $items;
    }

    // Function to get free items from order level rule engine
    public function getFreeItemsAttribute()
    {
        if (! array_key_exists('items', $this->relations)) {
            throw new \InvalidArgumentException("Items relation not loaded");
        }
        $items = $this->items->where('source', CartItem::SOURCE_OFFER)->where('parent_sku', null);
        return $items;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function shippingAddress()
    {
        return $this->belongsTo(CartAddress::class, 'shipping_address_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo(CartAddress::class, 'billing_address_id');
    }

    public function storeOrder()
    {
        return $this->belongsTo(StoreOrder::class);
    }

    public function discountRule()
    {
        return $this->belongsTo(DiscountRule::class);
    }
    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function getPrettyTotalAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . number_format($this->total, 2);
    }

    public function getPrettyShippingChargesAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . number_format($this->shipping_charges, 2);
    }

    public function getPrettySubtotalAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . number_format($this->subtotal, 2);
    }

    public function getPrettyDiscountAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . number_format($this->tax, 2);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }
}
