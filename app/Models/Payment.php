<?php

namespace Niyotail\Models;

class Payment extends Model
{
    const STATUS_PENDING = "pending";
    const STATUS_PAID = "paid";
    const STATUS_FAILED = "failed";
    const STATUS_VOID = "void";

    const TYPE_CREDIT = "credit";
    const TYPE_DEBIT = "debit";

    const METHOD_CASH = "cash";
    const METHOD_CARD = "card";
    const METHOD_PAYU = "payu";
    const METHOD_PAYPAL = "paypal";
    const METHOD_STORE_CREDIT = "store_credit";
    const METHOD_NEFT = "neft";
    const METHOD_CHEQUE = "cheque";
    const METHOD_ONLINE_PAYMENT = "online_payment";

    protected $guarded=['id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function returnOrder()
    {
        return $this->belongsTo(ReturnOrder::class);
    }

    public function paymentGatewayLogs()
    {
        return $this->hasMany(PaymentGatewayLog::class);
    }

    public function getPrettyAmountAttribute()
    {
        return number_format($this->amount, 2);
    }

    public function scopeCredit($query)
    {
        return $query->where('type', self::TYPE_CREDIT);
    }

    public function scopeDebit($query)
    {
        return $query->where('type', self::TYPE_DEBIT);
    }
}
