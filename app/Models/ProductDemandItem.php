<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDemandItem extends Model
{
    use SoftDeletes;

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function productDemand()
    {
        return $this->belongsTo(ProductDemand::class);
    }

    public function group() {
        return $this->belongsTo(ProductGroup::class);
    }

    public function scopeId($q, $id) {
        return $q->where('id', $id);
    }

    public function scopeDemand($q, $id) {
        return $q->where('product_demand_id', $id);
    }
}
