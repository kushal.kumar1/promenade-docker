<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class BeatMarketer extends Model
{
    // protected $with = ['beat','marketer'];

    public function beat()
    {
        return $this->belongsTo(Beat::class);
    }

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public static function getAgentId($beatId, $marketerId, $date)
    {
        $row = static::where('beat_id', $beatId)
            ->where('marketer_id', $marketerId)
            ->where('valid_from', '<=', $date)
            ->where(function ($query) use ($date) {
                $query->where('valid_to', '>=', $date)
                ->orWhereNull('valid_to');
            })
            ->first();

        return !empty($row) ? $row->agent_id : null;
    }
}
