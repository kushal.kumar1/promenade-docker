<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Order;
use Niyotail\Models\User;

class StoreCredit extends Model
{
    const TYPE_CREDIT = "credit";
    const TYPE_DEBIT = "debit";

    public function scopeCredit($query)
    {
        return $query->where('type',self::TYPE_CREDIT);
    }

    public function scopeDebit($query)
    {
        return $query->where('type',self::TYPE_DEBIT);
    }

    public function order()
    {
        return $this->hasOne(Order::class);
    }

    public function customer()
    {
        return $this->hasOne(User::class);
    }
}
