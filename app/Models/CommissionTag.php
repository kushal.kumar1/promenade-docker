<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class CommissionTag extends Model
{
    public function products()
    {
        return $this->hasManyThrough(Product::class,
            'commission_tag_product',
            'commission_tag_id', 'id', 'id', 'product_id');
    }
}
