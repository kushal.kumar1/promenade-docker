<?php

namespace Niyotail\Models;

class TaxClass extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function variants()
    {
        return $this->hasManyThrough(ProductVariant::class, Product::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function taxes()
    {
        return $this->hasMany(Tax::class);
    }
}
