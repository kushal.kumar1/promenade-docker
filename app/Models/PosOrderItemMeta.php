<?php


namespace Niyotail\Models;


class PosOrderItemMeta extends Model
{
    public function item()
    {
        $this->belongsTo(PosOrderItem::class);
    }

    public function shipments()
    {
        $this->hasMany(PosOrderItemMeta::class, 'pos_order_item_id','pos_order_item_id');
    }
}