<?php

namespace Niyotail\Models;

class ReturnCart extends Model
{
    protected $with = ['createdBy'];

    public function scopeActive($query)
    {
        $query->where('status', 'pending');
    }

    public function scopeByEmployee($query, $employee = null)
    {
        $query->active()
            ->where('created_by_type', array_search(Employee::class, self::$map))
            ->where('created_by_id', $employee->id);
    }

    public function scopeByUser($query, User $user)
    {
        $query->active()
              ->where('created_by_type', array_search(User::class, self::$map))
              ->where('created_by_id', $user->id);
    }

    public function scopeByAgent($query, Agent $agent)
    {
        $query->active()
            ->where('created_by_type', array_search(Agent::class, self::$map))
            ->where('created_by_id', $agent->id);
    }

    public function isCreatedBy($class)
    {
        return $this->created_by_type == array_search($class, self::$map);
    }

    public function getItemsByShipmentAttribute()
    {
        $this->relationLoaded('items') ? $this->items() : $this->load('items');
        return $this->getRelation('items')->groupBy('shipment_id');
    }

    public function items()
    {
        return $this->hasMany(ReturnCartItem::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }
}
