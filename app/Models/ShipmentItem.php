<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Shipment;
use Niyotail\Models\OrderItem;

class ShipmentItem extends Model
{
    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }
}
