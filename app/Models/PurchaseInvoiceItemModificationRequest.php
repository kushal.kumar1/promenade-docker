<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use \Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Gate;

class PurchaseInvoiceItemModificationRequest extends ModificationRequest
{
    use SoftDeletes;
    protected $table = 'purchase_invoice_item_modification_requests';

    public function __construct()
    {
        parent::__construct(ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM);
        Gate::allows('purchase-invoice-modification');
    }

    /**
     * @throws Exception
     */
    public function validateModificationRequest()
    {
        $purchaseInvoiceItem = PurchaseInvoiceItem::query()->findOrFail($this->purchase_invoice_item_id);

        switch ($this->action) {
            case ModificationRequest::ACTION_EDIT:
            {
                Product::query()->findOrFail($this->product_id);

                if(empty($this->quantity)) {
                    throw new \Exception("Failed validation for PurchaseInvoiceItemModificationRequest! quantity is required.");
                }
                if(empty($this->base_cost)) {
                    throw new \Exception("Failed validation for PurchaseInvoiceItemModificationRequest! base_cost is required.");
                }

                return;
            }
            case ModificationRequest::ACTION_DELETE:
            {
                if (empty($this->delete_reason) || strlen($this->delete_reason) <= 10) {
                    throw new \Exception("Failed validation for PurchaseInvoiceItemModificationRequest! delete_reason should be atleast 10 characters in length.");
                }
                return;
            }
            default:
            {
                throw new Exception("Invalid value for PurchaseInvoiceItemModificationRequest->action: {{$$this->action}}");
            }
        }
    }
}