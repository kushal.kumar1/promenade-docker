<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Model;

class WarehouseRackType extends Model
{
    public function warehouseRacks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Warehouse::class);
    }
}
