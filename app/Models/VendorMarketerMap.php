<?php


namespace Niyotail\Models;


class VendorMarketerMap extends Model
{
    public const VALID_CHANNELS = ['MT', 'GT', 'E-Comm'];
    public const VALID_RELATIONS = ['CFA', 'Brand', 'Super Stockist', 'Sub Stockist', 'Distributor', 'Wholesaler'];

    protected $table = 'vendor_marketer_map';

    public function scopeVendorID($q, $val) {
        return $q->where('vendor_id', $val);
    }

    public function scopeMarketerId($q, $val) {
        return $q->where('marketer_id', $val);
    }

    public function vendor() {
        return $this->belongsTo(Vendor::class);
    }

    public function marketer() {
        return $this->belongsTo(Marketer::class);
    }
}