<?php

namespace Niyotail\Models\InventoryMigration;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Model;
use Niyotail\Models\Employee;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;
use Niyotail\Models\Putaway\PutawayItem;

class InventoryMigrationItem extends Model
{
    const STATUS_PENDING = "pending";
    const STATUS_PROCESSING = "processing";
    const STATUS_PARTIAL = "partial";
    const STATUS_COMPLETED = "completed";

    public function inventoryMigration(): BelongsTo
    {
        return $this->belongsTo(InventoryMigration::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function fromStorage(): BelongsTo
    {
        return $this->belongsTo(Storage::class, 'from_storage_id');
    }

    public function toStorage(): BelongsTo
    {
        return $this->belongsTo(Storage::class, 'to_storage_id');
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function putawayItems(): HasMany
    {
        return $this->hasMany(PutawayItem::class);
    }

    public function inventories()
    {
        return $this->hasMany(InventoryMigrationItemInventory::class, 'inventory_migration_item_id');
    }
}
