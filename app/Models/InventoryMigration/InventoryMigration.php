<?php

namespace Niyotail\Models\InventoryMigration;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Models\Employee;
use Niyotail\Models\Model;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Warehouse;
use Niyotail\Queries\InventoryMigrationQueries;
use Niyotail\Queries\WarehouseFilter;

class InventoryMigration extends Model
{
    use WarehouseFilter, InventoryMigrationQueries;

    const STATUS_OPEN = "open";
    const STATUS_CLOSED = "closed";

    public function items(): HasMany
    {
        return $this->hasMany(InventoryMigrationItem::class);
    }

    public function employee(): BelongsTo
    {
        return $this->belongsTo(Employee::class);
    }

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function canClose(): bool
    {
//        return $this->items->whereIn('status', [InventoryMigrationItem::STATUS_PENDING, InventoryMigrationItem::STATUS_PROCESSING])->isEmpty();
        return ($this->items->whereIn('status', [InventoryMigrationItem::STATUS_PENDING, InventoryMigrationItem::STATUS_PROCESSING])->isEmpty()
                && $this->putaways->where('status', '!=', Putaway::STATUS_CLOSED)->isEmpty());
    }

    public function putaways(): HasMany
    {
        return $this->hasMany(Putaway::class);
    }
}
