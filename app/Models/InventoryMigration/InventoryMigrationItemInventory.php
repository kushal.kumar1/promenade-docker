<?php

namespace Niyotail\Models\InventoryMigration;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

use Niyotail\Models\Model;
use Niyotail\Models\FlatInventory;

class InventoryMigrationItemInventory extends Model
{
    public function migrationItem(): BelongsTo
    {
        return $this->belongsTo(InventoryMigrationItem::class);
    }

    public function inventory(): BelongsTo
    {
        return $this->belongsTo(FlatInventory::class, 'flat_inventory_id');
    }
}
