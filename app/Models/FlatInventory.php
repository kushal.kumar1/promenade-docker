<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Queries\FlatInventoryQueries;

class FlatInventory extends Model
{
    use FlatInventoryQueries;

    const STATUS_HOLD = "hold";
//    const STATUS_QC = "qc";
    const STATUS_PUTAWAY = "putaway";
    const STATUS_READY_FOR_SALE = "ready_for_sale";
    const STATUS_SOLD = "sold";
    const STATUS_MANIFESTED = "manifested";
    const STATUS_PICKED = "picked";
    const STATUS_BILLED = "billed";
    const STATUS_DISPATCHED = "dispatched";
    const STATUS_DELIVERED = "delivered";
    const STATUS_LOST = "lost";
    const STATUS_EXPIRED = "expired";
    const STATUS_DAMAGED = "damaged";
//    const STATUS_DUPLICATE = "duplicate";
    const STATUS_STOCK_TRANSFER= "stock_transfer";


    const STATUS_AUDIT_BLOCKED = 'audit_blocked';
    const STATUS_AUDIT_HOLD = 'audit_hold';
    const STATUS_AUDIT_BAD = 'audit_bad';
    const STATUS_AUDIT_LOSS = 'audit_loss';

    const AUDIT_STATUS = [
        self::STATUS_AUDIT_BLOCKED,
        self:: STATUS_AUDIT_HOLD,
        self:: STATUS_AUDIT_BAD,
        self::STATUS_AUDIT_LOSS
    ];

    const BAD_INVENTORY_STATUS = [
        self::STATUS_DAMAGED,
        self::STATUS_LOST,
//        self::STATUS_DUPLICATE,
        self::STATUS_EXPIRED
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function grnItem()
    {
        return $this->belongsTo(PurchaseInvoiceGrnItem::class, 'grn_item_id', 'id');
    }

    public function source()
    {
        return $this->morphTo();
    }

    public function scopeForSale($query)
    {
        return $query->where('status', self::STATUS_READY_FOR_SALE);
    }

    public function orderItem(): HasMany
    {
        return $this->hasMany(OrderItemFlatInventory::class);
    }

    /**
     * Bad Inventory status
     * @return array
     */
    public static function getBadInventoryStatus(): array
    {
        return self::BAD_INVENTORY_STATUS;
    }

    /**
     * Good Inventory Status
     * @return array
     */
    public static function getGoodInventoryStatus(): array
    {
        return array_values(array_diff(array_values(self::getConstants('STATUS')),self::getBadInventoryStatus()));
    }

}
