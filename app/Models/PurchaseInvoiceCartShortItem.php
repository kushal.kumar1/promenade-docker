<?php


namespace Niyotail\Models;


class PurchaseInvoiceCartShortItem extends Model
{
    public function taxClass() {
        return $this->hasOne(TaxClass::class, 'id', 'tax_class');
    }

    public function scopeId($q, $id) {
        return $q->where('id', $id);
    }

    public function scopeCart($q, $id) {
        return $q->where('cart_id', $id);
    }
}