<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderItemFlatInventory extends Model
{
    public function orderItem(): BelongsTo
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function inventory(): BelongsTo
    {
        return $this->belongsTo(FlatInventory::class, 'flat_inventory_id');
    }
}
