<?php

namespace Niyotail\Models;

use Illuminate\Support\Facades\DB;
use Niyotail\Models\Model;
use Niyotail\Models\ShipmentItem;
use Niyotail\Queries\ShipmentQueries;
use Niyotail\Queries\WarehouseFilter;

class Shipment extends Model
{
    use ShipmentQueries;
    use WarehouseFilter;

    const STATUS_GENERATED = "generated";
    const STATUS_READY_TO_SHIP = "ready_to_ship";
    const STATUS_DISPATCHED = "dispatched";
    const STATUS_RETRY = "retry";
    const STATUS_PENDING_RETRY = "pending_retry";
    const STATUS_RTO = "rto";
    const STATUS_PENDING_RTO = "pending_rto";
    const STATUS_DELIVERED = "delivered";
    const STATUS_CANCELLED = "cancelled";

    const STATE_TRANSIT = [
      self::STATUS_DISPATCHED,
      self::STATUS_PENDING_RETRY,
      self::STATUS_PENDING_RTO,
    ];

    const STATE_RETRY = [
      self::STATUS_RETRY
    ];

    const STATE_COMPLETE = [
      self::STATUS_RTO,
      self::STATUS_DELIVERED
    ];

    const STATE_CANCELLED = [
      self::STATUS_CANCELLED,
    ];

    public function canCancel()
    {
        return in_array($this->status, [self::STATUS_GENERATED, self::STATUS_READY_TO_SHIP]);
    }

    public function canDeliver()
    {
        return in_array($this->status, self::STATE_TRANSIT);
    }

    public function canUpdate()
    {
        return !in_array($this->status, [ self::STATUS_CANCELLED,self::STATUS_DELIVERED,self::STATUS_RTO]);
    }

    public function canRto()
    {
        return in_array($this->status, self::STATE_TRANSIT);
    }

    public function canRetry()
    {
        return in_array($this->status, self::STATE_TRANSIT);
    }

    public function canReturn()
    {
        return (bool) (in_array($this->status, [Shipment::STATUS_DELIVERED]) && ($this->orderItems->count() > $this->returnItems->where('returnOrder', function($q) { $q->where('status', '!=', ReturnOrder::STATUS_CANCELLED); })->count()));
    }

    public function canResendOTP()
    {
        return ($this->status == self::STATUS_DISPATCHED && $this->order->type != Order::TYPE_STOCK_TRANSFER);
    }

    public function items()
    {
        return $this->hasMany(ShipmentItem::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function picklist()
    {
        return $this->belongsTo(Picklist::class);
    }

    public function orderItems()
    {
        return $this->belongsToMany(OrderItem::class, 'shipment_items');
    }

    public function returnableOrderItems()
    {
        return $this->belongsToMany(OrderItem::class, 'shipment_items')->canReturn();
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }

    public function isCancelled()
    {
        return in_array($this->status, [self::STATUS_CANCELLED, self::STATUS_RTO]);
    }

    public function returns()
    {
        return $this->hasMany(ReturnOrder::class);
    }

    public function returnItems()
    {
        return $this->hasManyThrough(ReturnOrderItem::class, ReturnOrder::class);
    }

    public function getOrderItemsBySkuAttribute()
    {
        $this->relationLoaded('orderItems') ? $this->orderItems() : $this->load('orderItems');
        return $this->getRelation('orderItems')->groupBy('sku');
    }

    public function creditNotes()
    {
        return $this->hasManyThrough(CreditNote::class, ReturnOrder::class);
    }

    public function scopeCanBeShipped($query)
    {
        return $query->whereIn('status', [self::STATUS_GENERATED,self::STATUS_RETRY]);
    }

    public function manifests()
    {
        return $this->belongsToMany(Manifest::class, 'manifest_shipment');
    }

     //For agent delivery order by distance
     public function scopeOrderByDistance($query, $lat, $lng)
     {
         $query->select(
             'shipments.*',
             DB::raw("
             (6371 * acos(
                cos( radians($lat) )
              * cos( radians( stores.lat ) )
              * cos( radians( stores.lng ) - radians($lng) )
              + sin( radians($lat) )
              * sin( radians( stores.lat ) )
                ) )
              AS distance ")
         )->join('orders', 'orders.id', 'shipments.order_id')
         ->join('stores', 'stores.id', 'orders.store_id')
         ->orderByRaw('distance is null, distance asc');
     }
}
