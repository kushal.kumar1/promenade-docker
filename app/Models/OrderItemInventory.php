<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class OrderItemInventory extends Model
{
    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function getTotalCostPriceAttribute()
    {
        return $this->unit_cost_price * $this->quantity;
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'batch_number', 'batch_number');
    }
}
