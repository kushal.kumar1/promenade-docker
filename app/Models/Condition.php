<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class Condition extends Model
{
    public function rule()
    {
        return $this->belongsTo(Rule::class);
    }

    public function expressions()
    {
        return $this->hasMany(Expression::class);
    }

    public function scopeWhereValid($query, $name, $value, $isNumber = false)
    {
        $query->where(function ($groupQuery) use ($name, $value, $isNumber) {
            $groupQuery->whereDoesntHave('expressions', function ($subQuery) use ($name) {
                $subQuery->where('name', $name);
            })->orWhereHas('expressions', function ($subQuery) use ($name, $value, $isNumber) {
                $subQuery->where('name', $name);
                if ($isNumber) {
                    $subQuery->whereNumber($value);
                } else {
                    $subQuery->whereIn('value', is_array($value) ? $value : [$value]);
                }
            });
        });
    }

    public function scopeIsValidForVariant($query, $variant, $quantity = null)
    {
        $collections = $variant->product->collections->pluck('id')->toArray();
        $query->whereValid('variant', $variant->id)
            ->whereValid('price', $variant->getFinalPrice(), true)
            ->whereValid('collection', $collections)
            ->whereValid('brand', $variant->product->brand_id)
            ->whereValid('marketer', $variant->product->brand->marketer_id);
        if (!isset($quantity)) {
            $query->whereDoesntHave('expressions', function ($expressionQuery) {
                $expressionQuery->where('name', 'quantity');
            });
        } elseif ($quantity != 0) {
            $query->whereValid('quantity', $quantity, true);
        }
    }

    public function scopeIsValidForCartItems($query, $cartItems)
    {
        $query->where(function ($groupQuery) use ($cartItems) {
            foreach ($cartItems as $cartItem) {
                $groupQuery->orWhere(function ($variantQuery) use ($cartItem) {
                    $collections = $cartItem->product->collections->pluck('id')->toArray();
                    $variantQuery->whereValid('variant', $cartItem->productVariant->id)
                        ->whereValid('price', $cartItem->price, true)
                        ->whereValid('collection', $collections)
                        ->whereValid('brand', $cartItem->product->brand_id)
                        ->whereValid('marketer', $cartItem->product->brand->marketer_id)
                        ->whereValid('quantity', $cartItem->quantity, true);
                });
            }
        });
    }
}
