<?php

namespace Niyotail\Models\Category;


use Niyotail\Models\Model;
use Niyotail\Models\Product;

class Cl4 extends Model
{
    protected $table = "newcat_l4";

    public function cl3()
    {
        return $this->belongsTo(Cl3::class, 'newcat_l3_id', 'id');
    }
}
