<?php

namespace Niyotail\Models\Category;


use Niyotail\Models\Model;

class Cl3 extends Model
{
    protected $table = "newcat_l3";

    public function cl2()
    {
        return $this->belongsTo(Cl2::class, 'newcat_l2_id', 'id');
    }
}
