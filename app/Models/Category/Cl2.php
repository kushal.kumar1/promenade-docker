<?php

namespace Niyotail\Models\Category;


use Niyotail\Models\Model;

class Cl2 extends Model
{
    protected $table = "newcat_l2";

    public function cl1()
    {
        return $this->belongsTo(Cl1::class, 'newcat_l1_id', 'id');
    }
}
