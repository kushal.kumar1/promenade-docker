<?php

namespace Niyotail\Models\Category;


use Niyotail\Models\CommissionTag;
use Niyotail\Models\Model;
use Niyotail\Models\Product;

class Cl4CostMap extends Model
{
    protected $table = "category_l4_cost_map_rt";

    public function scopeMarketerLevelId($q, $id) {
        return $q->where('marketer_level_id', $id);
    }

    public function scopeCl4Id($q, $id) {
        return $q->where('category_l4_id', $id);
    }

    public function commissionTag()
    {
        return $this->belongsTo(CommissionTag::class, 'commission_tag_id', 'id');
    }
}
