<?php


namespace Niyotail\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Niyotail\Queries\VendorAddressQueries;

class VendorsAddresses extends Model
{
    use VendorAddressQueries;
    use SoftDeletes;

    protected $table = 'vendor_addresses';

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function pin() {
        return $this->belongsTo(Pincode::class, 'pincode', 'pincode');
    }

    public function vendor ()
    {
        return $this->belongsTo(Vendor::class);
    }
}