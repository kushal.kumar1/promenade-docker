<?php


namespace Niyotail\Models;


use Niyotail\Queries\WarehouseFilter;

class PurchaseInvoiceDebitNote extends Model
{
    use WarehouseFilter;

    public function createdBy()
    {
        return $this->belongsTo(Employee::class,'created_by');
    }

    public function invoice()
    {
        return $this->hasOne(PurchaseInvoice::class, 'id', 'purchase_invoice_id');
    }

    public function items()
    {
        return $this->hasMany(PurchaseInvoiceDebitNoteItem::class, 'debit_note_id', 'id');
    }
}