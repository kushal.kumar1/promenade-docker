<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Queries\InventoryQueries;
use Niyotail\Queries\WarehouseFilter;

class Inventory extends Model
{
    use InventoryQueries;
//    use WarehouseFilter;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function grnItem()
    {
        return $this->belongsTo(PurchaseInvoiceGrnItem::class, 'grn_item_id', 'id');
    }

    public function getPrettyBatchDate()
    {
        $batchNumber = $this->batch_number;

        $batchDetails = explode("-", $batchNumber);
        return implode('-', [$batchDetails[0], $batchDetails[1], $batchDetails[2]]);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(InventoryTransaction::class);
    }

    public function scopeInternalPurchase($q) {
        return $q->where('warehouse_id', 2);
    }

}
