<?php

namespace Niyotail\Models;

use Niyotail\Auth\OtpAuthenticateContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Niyotail\Queries\AgentQueries;
use Niyotail\Models\Agent\AgentBeatDay;
use Niyotail\Models\Agent\AgentTarget;
use Niyotail\Models\AuthUser;

class Agent extends AuthUser implements OtpAuthenticateContract, JWTSubject
{
    use AgentQueries;

    const TYPE_PICKER='picker';
    const TYPE_SALES='sales';
    const TYPE_DELIVERY='delivery';
    const TYPE_COLLECTION='collection';

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function isVerified()
    {
        return $this->verified;
    }

    public function carts()
    {
        return $this->morphMany(Cart::class, 'created_by');
    }

    public function orders()
    {
        return $this->morphMany(Order::class, 'created_by');
    }

    public function deviceParams()
    {
        return $this->morphMany(DeviceParameter::class, 'created_by');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getOtp()
    {
        return $this->otp;
    }

    public function getOtpExpiry()
    {
        return $this->otp_expiry;
    }

    public function beatMarketers()
    {
        return $this->hasMany(BeatMarketer::class)->with('beeat', 'marketer');
    }

    public function targets()
    {
        return $this->hasMany(AgentTarget::class);
    }

    public function stats()
    {
        return $this->hasMany(AgentStat::class);
    }

    public function targetBeats()
    {
        return $this->hasMany(AgentBeatDay::class);
    }
}
