<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Storage extends Model
{
    const ZONE_DEFAULT = "default";
    const ZONE_UNIT = "unit";
    const ZONE_BULK = "bulk";
    const ZONE_EXCESS = "excess";
    const ZONE_DAMAGED = "damaged";
    const ZONE_REFRIGERATED = "refrigerated";
    const ZONE_DISPATCH = "dispatch";
    const ZONE_VIRTUAL = "virtual";
    const ZONE_LIQUIDATION = 'liquidation';
    const ZONE_NEAR_EXPIRY = 'near_expiry';
    const ZONE_1K_MALL = '1k_mall';

    const CATEGORY_FLOOR = "floor";
    const CATEGORY_RACK = "rack";
    const CATEGORY_PALLET = "pallet";
    const CATEGORY_BOX = "box";
    const CATEGORY_OTHER = "other";

    public function warehouse(): BelongsTo
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function inventories(): HasMany
    {
        return $this->hasMany(FlatInventory::class);
    }
}
