<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class StockTransferItem extends Model
{
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function stockTransfer(): BelongsTo
    {
        return $this->belongsTo(StockTransfer::class);
    }

    public function inventories(): MorphMany
    {
        return $this->morphMany(FlatInventory::class, 'source');
    }

    public function flatInventories(): BelongsToMany
    {
        return $this->belongsToMany(FlatInventory::class,'stock_transfer_item_flat_inventories');
    }
}
