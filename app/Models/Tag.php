<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class Tag extends Model
{
    public const TYPE_PRODUCT = 'product';
    public const TYPE_STORE = 'store';
    public const TYPE_ORDER = 'order';
    public const TYPE_PAYMENT_TAG = 'payment_tag';


    public static function paymentTags()
    {
        return Tag::paymentTag()->distinct('name')->pluck('name')->toArray();
    }

    public function scopeStore($query)
    {
        return $query->where('type', 'store');
    }

    public function scopeOrder($query)
    {
        return $query->where('type', 'order');
    }

    public function scopePaymentTag($query)
    {
        return $query->where('type', self::TYPE_PAYMENT_TAG);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
