<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Employee;

class OrderLog extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
