<?php

namespace Niyotail\Models;

class UserFeedback extends Model
{
    const TYPE_FEEDBACK = "feedback";
    const TYPE_PRODUCT_REQUEST = "product_request";

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
