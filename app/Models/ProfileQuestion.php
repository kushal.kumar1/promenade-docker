<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Queries\ProfileQuestionQueries;

class ProfileQuestion extends Model
{
    use ProfileQuestionQueries;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeMandatory($query)
    {
        return $query->where('is_mandatory', 1);
    }

    public function answerOptions()
    {
        return $this->hasMany(ProfileQuestionOption::class, 'question_id');
    }

    public function storeAnswers()
    {
        return $this->hasMany(StoreAnswer::class, 'question_id');
    }
}
