<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class OrderItemMeta extends Model
{
    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function orderItem()
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
