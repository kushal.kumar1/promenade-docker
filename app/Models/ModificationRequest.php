<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

abstract class ModificationRequest extends Model
{
    use SoftDeletes;

    const TYPE_TRANSACTION = "transaction";
    const TYPE_PURCHASE_INVOICE = "purchase_invoice";
    const TYPE_PURCHASE_INVOICE_ITEM = "purchase_invoice_item";

    const ACTION_EDIT = "edit";
    const ACTION_DELETE = "delete";

    const STATUS_PENDING = "pending";
    const STATUS_PROCESSING = "processing";
    const STATUS_FAILED = "failed";
    const STATUS_COMPLETED = "completed";

    protected string $modificationRequestType;
    protected int $modificationRequestTTL = 7200;   // 2 hours before the request is retried

    /**
     * @throws Exception
     */
    public function __construct($modificationRequestType)
    {
        parent::__construct();

        if (!self::isValidType($modificationRequestType)) {
            throw new Exception("Invalid ModificationRequestType");
        }
        $this->modificationRequestType = $modificationRequestType;
    }

    public static function isValidType($modificationRequestType): bool
    {
        return in_array($modificationRequestType, self::getTypes());
    }

    public static function isValidStatus($modificationRequestStatus): bool
    {
        return in_array($modificationRequestStatus, self::getStatuses());
    }

    public static function isValidAction($modificationRequestAction): bool
    {
        return in_array($modificationRequestAction, self::getActions());
    }

    public static function getTypes(): array
    {
        return array(
            self::TYPE_TRANSACTION,
            self::TYPE_PURCHASE_INVOICE,
            self::TYPE_PURCHASE_INVOICE_ITEM
        );
    }

    public static function getStatuses(): array
    {
        return array(
            self::STATUS_PENDING,
            self::STATUS_PROCESSING,
            self::STATUS_FAILED,
            self::STATUS_COMPLETED
        );
    }

    public static function getActions(): array
    {
        return array(
            self::ACTION_EDIT,
            self::ACTION_DELETE
        );
    }

    public function getModificationRequestType() {
        return $this->modificationRequestType;
    }

    /**
     * @throws Exception
     */
    public function getStatusAttribute(int $value): string
    {
        return $this->mapEnumToStatus($value);
    }

    /**
     * @throws Exception
     */
    public function setStatusAttribute(string $value): ModificationRequest
    {
        $this->attributes['status'] = $this->mapStatusToEnum($value);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function setActionAttribute(string $value): ModificationRequest
    {
        $this->attributes['action'] = $this->mapActionToEnum($value);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function getActionAttribute(int $value): string
    {
        return $this->mapEnumToAction($value);
    }

    /**
     * @throws Exception
     */
    public static function mapEnumToStatus(int $enum): string
    {
        switch ($enum) {
            case 0:
                return self::STATUS_PENDING;
            case 1:
                return self::STATUS_PROCESSING;
            case 2:
                return self::STATUS_FAILED;
            case 3:
                return self::STATUS_COMPLETED;
            default:
                throw new Exception("No ModificationRequest::Status mapping found for value:$enum");
        }
    }

    /**
     * @throws Exception
     */
    public static function mapStatusToEnum($status): int
    {
        switch ($status) {
            case self::STATUS_PENDING:
                return 0;
            case self::STATUS_PROCESSING:
                return 1;
            case self::STATUS_FAILED:
                return 2;
            case self::STATUS_COMPLETED:
                return 3;
            default:
                throw new Exception("Invalid ModificationRequest::status value provided: $status");
        }
    }

    /**
     * @throws Exception
     */
    public static function mapEnumToAction(int $enum): string
    {
        switch ($enum) {
            case 0:
                return self::ACTION_EDIT;
            case 1:
                return self::ACTION_DELETE;
            default:
                throw new Exception("No ModificationRequest::action mapping found for value:$enum");
        }
    }

    /**
     * @throws Exception
     */
    public static function mapActionToEnum($action): int
    {
        switch ($action) {
            case self::ACTION_EDIT:
                return 0;
            case self::ACTION_DELETE:
                return 1;
            default:
                throw new Exception("Invalid ModificationRequest::action value provided: $action");
        }
    }

    /**
     * @throws Exception
     */
    public static function scopeStatusPending($query)
    {
        return $query->where('status', self::STATUS_PENDING);
    }

    /**
     * @throws Exception
     */
    public static function scopeStatusProcessing($query)
    {
        return $query->where('status', self::STATUS_PROCESSING);
    }

    /**
     * @throws Exception
     */
    public static function scopeStatusCompleted($query)
    {
        return $query->where('status', self::STATUS_COMPLETED);
    }

    /**
     * @throws Exception
     */
    public static function scopeStatusFailed($query)
    {
        return $query->where('status', self::STATUS_FAILED);
    }

    /**
     * @throws Exception
     */
    public static function scopeWithStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * @throws Exception
     */
    public static function scopeActionEdit($query)
    {
        return $query->where('action', self::ACTION_EDIT);
    }

    /**
     * @throws Exception
     */
    public static function scopeActionDelete($query)
    {
        return $query->where('action', self::ACTION_DELETE);
    }

    /**
     * @throws Exception
     */
    public function scopeTimedOut($query)
    {
        return $query->processing()->filter(
            function ($request) {
                if (self::mapStatusToEnum($request->status) != self::STATUS_PROCESSING) {
                    return false;
                }
                if (Carbon::now()->diffInSeconds($request->updated, false) > $this->modificationRequestTTL) {
                    return true;
                }
                return false;
            });
    }

    public abstract function validateModificationRequest();

//    public function scopeExpandEmployee(Builder $query)
//    {
//        return $query->join('employees', 'id', '=', 'employees.id')
//
//
//        return $this->belongsTo(Employee::class, "employee_id", "id")->first();
//    }
}