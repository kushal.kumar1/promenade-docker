<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Niyotail\Queries\OrderQueries;
use Niyotail\Queries\WarehouseFilter;

class Order extends Model
{
    use OrderQueries;
    use WarehouseFilter;

    const STATUS_CONFIRMED = "confirmed";
    const STATUS_MANIFESTING = "manifesting";
    const STATUS_MANIFESTED = "manifested";
    const STATUS_BILLED = "billed";
    const STATUS_TRANSIT = "transit";
    const STATUS_COMPLETED = "completed";
    const STATUS_CANCELLED = "cancelled";

    const SOURCE_APP = "app";
    const SOURCE_AGENT_APP = "agent_app";
    const SOURCE_SYSTEM = "system";
    const SOURCE_IMPORTER = "importer";
    const SOURCE_STORE_PURCHASE = "store_purchase";
    const SOURCE_POS = "pos";
    const SOURCE_ARS = "ars";
    const SOURCE_WHOLESALE_IMPORTER = 'wholesale_importer';

    const TAG_1K_MALL = "1k_mall";
    const TAG_ARS = "ars";
    const TAG_NEW_STORE = "new_store";

    const TYPE_RETAIL = "retail";
    const TYPE_WHOLESALE = "wholesale";
    const TYPE_STOCK_TRANSFER = "stock_transfer";

    protected $with = ['createdBy'];
    protected $primaryKey = 'id';
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at', 'billing_address_id', 'shipping_address_id'];

    public function getTotalQtyAttribute()
    {
        $this->relationLoaded('items') ? $this->items() : $this->load('items');
        $items = $this->getRelation('items');
        return $items->sum('quantity');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function getItemsBySkuAttribute()
    {
        $this->relationLoaded('items') ? $this->items() : $this->load('items');
        return $this->items->groupBy('sku');
    }

    public function getItemsByStatusAndSkuAttribute()
    {
        $this->relationLoaded('items') ? $this->items() : $this->load('items');
        return $this->items->groupBy(['status', 'sku']);
    }

    public function scopeActiveOrCompleted($query)
    {
        return $query->whereNotIn('status', [self::STATUS_CANCELLED]);
    }

    public function canCancel()
    {
        return ($this->status == self::STATUS_CONFIRMED || ($this->status == self::STATUS_MANIFESTED && $this->picklistItems->count() == $this->picklistItems->where('status', PicklistItem::STATUS_PENDING)->count()));
    }

    public function canUpdate()
    {
        return ($this->status == self::STATUS_CONFIRMED || $this->status == self::STATUS_MANIFESTED);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function shipments()
    {
        return $this->hasMany(Shipment::class);
    }

    public function shippingAddress()
    {
        return $this->belongsTo(OrderAddress::class, 'shipping_address_id');
    }

    public function billingAddress()
    {
        return $this->belongsTo(OrderAddress::class, 'billing_address_id');
    }

    public function discountRule()
    {
        return $this->belongsTo(DiscountRule::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function logs()
    {
        return $this->hasMany(OrderLog::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function returns()
    {
        return $this->hasMany(ReturnOrder::class);
    }

    public function rating()
    {
        return $this->hasMany(OrderRating::class);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function purchaseInvoice()
    {
        return $this->hasMany(StorePurchaseInvoice::class);
    }

    public function isCreatedBy($class)
    {
        return !!$this->created_by_type == array_search($class, self::$map);
    }

    public function scopeCreatedByUser($query, $createdBy)
    {
        $query->where('created_by_type', array_search(get_class($createdBy), self::$map))
            ->where('created_by_id', $createdBy->id);
    }

    public function getPrettyTotalAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . " " . number_format($this->total, 2);
    }

    public function getPrettySubtotalAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . " " . number_format($this->subtotal, 2);
    }

    public function getPrettyShippingChargesAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . " " . number_format($this->shipping_charges, 2);
    }

    public function getPrettyDiscountAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . " " . number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute()
    {
        return html_entity_decode(trans("currency.$this->currency")) . " " . number_format($this->tax, 2);
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function picklistItems()
    {
        return $this->hasManyThrough(PicklistItem::class, OrderItem::class);
    }

    public function additionalInfo(): HasOne
    {
        return $this->hasOne(AdditionalOrderInfo::class);
    }

    public function storePurchaseInvoice(): MorphOne
    {
        return $this->morphOne(StorePurchaseInvoice::class, 'assignedOrder');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function picklists()
    {
        return $this->belongsToMany(Picklist::class, 'order_picklist');
    }
}
