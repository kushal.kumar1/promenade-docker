<?php

namespace Niyotail\Models;

class CartItem extends Model
{
    const SOURCE_MANUAL = "manual";
    const SOURCE_OFFER = "offer";
    const SOURCE_IMPORTER = "importer";
    const SOURCE_REPLENISH_BOT = "replenish_bot";
    const SOURCE_CLONE = "clone";
    const SOURCE_1k_MALL = "1k_mall";

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getPrettyTotalAttribute(): string
    {
        return number_format($this->total, 2);
    }

    public function getPrettySubtotalAttribute(): string
    {
        return number_format($this->subtotal, 2);
    }

    public function getPrettyDiscountAttribute(): string
    {
        return number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute(): string
    {
        return number_format($this->tax, 2);
    }

    public function getPrettyPriceAttribute(): string
    {
        return number_format($this->price, 0);
    }

    public function getPrettyMrpAttribute(): string
    {
        return number_format($this->mrp, 0);
    }
}
