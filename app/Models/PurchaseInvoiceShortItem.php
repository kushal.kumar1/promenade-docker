<?php


namespace Niyotail\Models;


class PurchaseInvoiceShortItem extends Model
{
    public function taxClass() {
        return $this->hasOne(TaxClass::class, 'id', 'tax_class');
    }

    public function getPrettyRates() {
        $taxClass = $this->taxClass;
        $taxes = $taxClass->taxes;
        $taxPercent = $taxes->where('name', 'IGST')->sum('percentage') + $taxes->where('name', 'CESS')->sum('percentage');
        $splCess = $taxes->where('name', 'SPL CESS');
        $additionalPerUnit = 0;
        if($splCess->count()) {
            $additionalPerUnit = $splCess->first()->additional_charges_per_unit;
        }
        $totalAdditionalCharge = $additionalPerUnit*$this->quantity;

        $invoiceCost = $this->invoice_cost;
        $baseRate  = ($invoiceCost - $totalAdditionalCharge) / (1 + ($taxPercent/100));

        $itemDetails['base_rate'] = $baseRate;
        $itemDetails['tax_amount'] = $invoiceCost - $baseRate;
        $itemDetails['invoice_cost'] = $invoiceCost;
        $itemDetails['effective_cost'] = $this->effective_cost;

        return $itemDetails;
    }
}