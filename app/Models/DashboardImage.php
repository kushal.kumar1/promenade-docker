<?php

namespace Niyotail\Models;

class DashboardImage extends Model
{
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
}
