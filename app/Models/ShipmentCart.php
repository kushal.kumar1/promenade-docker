<?php

namespace Niyotail\Models;

class ShipmentCart extends Model
{
    protected $with = ['createdBy'];

    public function scopeWithoutShipment($query)
    {
        $query->whereNull('shipment_id');
    }

    public function scopeByEmployee($query, $employee = null)
    {
        $query->withoutShipment()
            ->where('created_by_type', array_search(Employee::class, self::$map))
            ->where('created_by_id', $employee->id);
    }

    public function scopeOrder($query, $orderID)
    {
        $query->where('order_id', $orderID);
    }

    public function isCreatedBy($class)
    {
        return $this->created_by_type == array_search($class, self::$map);
    }

    public function items()
    {
        return $this->hasMany(ShipmentCartItem::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function getPrettyTotalAttribute()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item->productVariant->price * $item->quantity;
        }
        return html_entity_decode('&#8377;') . number_format($total, 2);
        // return html_entity_decode(trans("currency.$this->currency")) . number_format($this->total, 2);
    }
}
