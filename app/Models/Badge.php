<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Product;

class Badge extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class,'badge_user');
    }
}
