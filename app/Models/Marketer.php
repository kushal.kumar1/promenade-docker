<?php

namespace Niyotail\Models;

use Niyotail\Queries\MarketerQueries;

class Marketer extends Model
{
    use MarketerQueries;

    public function brands()
    {
        return $this->hasMany(Brand::class);
    }

    public function vendorMarketerMaps() {
        return $this->hasMany(VendorMarketerMap::class);
    }

    public function level() {
        return $this->hasOne(MarketerLevel::class, 'id', 'level_id');
    }
}
