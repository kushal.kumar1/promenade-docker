<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class AppSectionItem extends Model
{
    const TYPE_PRODUCT_LISTING = "product_listing";
    const TYPE_PRODUCT_DETAIL = "product_detail";

    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }

    public function section()
    {
        return $this->belongsTo(AppSection::class,'app_section_id');
    }
}
