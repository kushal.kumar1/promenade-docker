<?php

namespace Niyotail\Models;

use Niyotail\Queries\ReturnOrderQueries;
use Niyotail\Queries\WarehouseFilter;

class ReturnOrder extends Model
{
    use ReturnOrderQueries;
    use WarehouseFilter;

    const STATUS_INITIATED = "initiated";
    const STATUS_COMPLETED = "completed";
    const STATUS_CANCELLED = "cancelled";

    protected $with = ['createdBy'];

    public function getOrderItemsBySkuAttribute()
    {
        $this->relationLoaded('orderItems') ? $this->orderItems() : $this->load('orderItems');
        return $this->getRelation('orderItems')->groupBy('sku');
    }

    public function scopeCompleted($query)
    {
        $query->whereIn('status', [self::STATUS_COMPLETED]);
    }

    public function items()
    {
        return $this->hasMany(ReturnOrderItem::class);
    }

    public function orderItems()
    {
        return $this->belongsToMany(OrderItem::class, 'return_order_items');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'source');
    }

    public function creditNote()
    {
        return $this->hasOne(CreditNote::class);
    }

    public function canCancel()
    {
        return $this->status == self::STATUS_INITIATED;
    }

    public function canComplete()
    {
        return $this->status == self::STATUS_INITIATED;
    }

    public function scopeByEmployee($query, $employee = null)
    {
        $query
            ->where('created_by_type', array_search(Employee::class, self::$map))
            ->where('created_by_id', $employee->id);
    }

    public function scopeByAgent($query, Agent $agent)
    {
        $query
            ->where('created_by_type', array_search(Agent::class, self::$map))
            ->where('created_by_id', $agent->id);
    }

    public function createdBy()
    {
        return $this->morphTo();
    }

    public function createdByUser() {
        return $this->belongsTo(User::class, 'created_by_id', 'id');
    }

}
