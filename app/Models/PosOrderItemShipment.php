<?php


namespace Niyotail\Models;


class PosOrderItemShipment extends Model
{
    public function scopeOrderItem($q, $id)
    {
        return $q->where('pos_order_item_id', $id);
    }

    public function niyotailInvoice()
    {
        return $this->hasOne(Invoice::class, 'shipment_id', 'shipment_id');
    }
}