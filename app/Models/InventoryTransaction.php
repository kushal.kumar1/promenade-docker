<?php

namespace Niyotail\Models;

class InventoryTransaction extends Model
{
    const TYPE_INIT = "init";
    const TYPE_WRITE_OFF = "write-off";
    const TYPE_SALE = "sale";
    const TYPE_SALE_CANCELLATION = "sale_cancellation";
    const TYPE_RETURN = "return";
    const TYPE_AUDIT_INCREMENT = "audit_increment";
    const TYPE_AUDIT_WRITE_OFF = "audit-write-off";
    const TYPE_STOCK_TRANSFER = "stock-transfer";

    public function inventory()
    {
        return $this->belongsTo(Inventory::class);
    }

    public function source()
    {
        return $this->morphTo();
    }
}
