<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class CreditNote extends Model
{
    const STATUS_GENERATED = "generated";
    const STATUS_CANCELLED = "cancelled";

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function returnOrder()
    {
        return $this->belongsTo(ReturnOrder::class);
    }

    public function transaction()
    {
        return $this->morphOne(Transaction::class, 'source');
    }

}
