<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Niyotail\Queries\VendorQueries;

class Vendor extends Model
{
    use VendorQueries;
    use SoftDeletes;

    public const TYPE_DEFAULT = 'vendor';
    public const TYPE_WAREHOUSE = 'warehouse';

    protected $table = "vendors";

    protected $fillable = [
        "name", "trade_name", "status", "legal_name", "nickname", "incorporation_type", "type",
        "GSTIN", "PAN", "website", "email", "contact_person", "contact_phone", "credit_days", "credit_limit",
        "bank_name", "ifsc_code", "account_type", "account_number"
    ];

    public function addresses()
    {
        return $this->hasMany(VendorsAddresses::class);
    }

    public function activeAddresses()
    {
        return $this->hasMany(VendorsAddresses::class)->where('status', '=', 1);
    }

    public function accounts ()
    {
        return $this->hasMany(VendorBankAccount::class);
    }
}
