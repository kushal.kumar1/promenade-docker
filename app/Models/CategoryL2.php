<?php


namespace Niyotail\Models;


class CategoryL2 extends Model
{
    protected $table = 'category_l2';

    public function collection ()
    {
        return $this->hasOne(CategoryL1::class, 'id', 'category_l1_id');
    }
}