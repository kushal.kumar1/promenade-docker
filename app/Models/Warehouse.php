<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Models\Storage;
use Niyotail\Queries\WarehouseQueries;

class Warehouse extends Model
{
    use WarehouseQueries;

    public const TYPE_PHYSICAL = 'physical';
    public const TYPE_VIRTUAL = 'virtual';

    protected $attributes = [
        'type' => self::TYPE_PHYSICAL,
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function pincodes()
    {
        return $this->belongsToMany(Pincode::class, 'pincode_warehouse');
    }

    public function inventories()
    {
        return $this->hasMany(Inventory::class);
    }

    public function inventory()
    {
        return $this->hasOne(Inventory::class);
    }

    public function beats()
    {
        return $this->belongsToMany(Beat::class, 'beat_warehouse');
    }

    public function scopeActive($q)
    {
        return $q->where("status", 1);
    }

    public function isParent(): bool
    {
        return empty($this->parent_id);
    }

    public function scopePhysical($query)
    {
        return $query->where('type', self::TYPE_PHYSICAL);
    }

    public function scopeVirtual($query)
    {
        return $query->where('type', self::TYPE_VIRTUAL);
    }

    public function picklists(): HasMany
    {
        return $this->hasMany(Picklist::class);
    }

    public function parentWarehouse(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function childWarehouses(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function getFullNameAttribute()
    {
        if(!empty($this->parent_id)) {
            return $this->parentWarehouse->name . " : " . $this->name;
        }

        return $this->name;
    }

    public function vendor(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Vendor::class);
    }
}
