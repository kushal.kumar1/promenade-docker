<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class ClubPrice extends Model
{
    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class);
    }
}
