<?php


namespace Niyotail\Models;


class AutoReplenishParameter extends Model
{
    protected $table = 'auto_replenish_parameters';

    public function scopeActive($query)
    {
        return $query->where('status', '=', '1');
    }

    public function scopeWarehouse($query)
    {
        return $query->where('type', '=', 'warehouse');
    }
}