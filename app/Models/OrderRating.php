<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Invoice;
use Niyotail\Models\Shipment;
use Niyotail\Queries\OrderQueries;

class OrderRating extends Model
{
    protected $guarded = ['id', 'order_id'];

    const TYPE_DELIVERY = 'delivery';

    public function order()
    {
        return $this->hasOne(Order::class);
    }
}
