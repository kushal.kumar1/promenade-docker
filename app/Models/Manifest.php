<?php

namespace Niyotail\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Niyotail\Queries\WarehouseFilter;

class Manifest extends Model
{
    use WarehouseFilter;
    
    const STATUS_GENERATED = "generated";
    const STATUS_DISPATCHED = "dispatched";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_CLOSED = "closed";

    const ACTION_RTO = "rto";
    const ACTION_RETRY = "retry";
    const ACTION_RETURN_ITEMS = "return_items";
    const ACTION_REMOVE_SHIPMENT = "remove_shipment";
    const ACTION_RESEND_OTP = "resend_otp";
    const ACTION_VIEW_POD = "view_pod";
    const ACTION_ADD_EWAY_BILL_NUMBER = "add_eway_bill_number";

//    const ACTION_ADD_TRANSACTION = "add_transaction";

    public function canCancel()
    {
        return $this->status == self::STATUS_GENERATED;
    }

    public function canClose()
    {
        return $this->status == self::STATUS_DISPATCHED && $this->shipments->whereIn('status', Shipment::STATE_TRANSIT)->count() == 0;
    }

    public function canDispatch()
    {
        return $this->status == self::STATUS_GENERATED;
    }

    public function shipments()
    {
        return $this->belongsToMany(Shipment::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(Employee::class, 'created_by');
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }


    public function getPdfUrlAttribute()
    {
        return storage_path('app/manifest') . "/$this->id.pdf";
    }


    public function getAvailableActions(Shipment $shipment)
    {
        $actions = array();

        if ($this->status != self::STATUS_CANCELLED) {
            if ($shipment->canCancel()) {
                $actions[] = self::ACTION_REMOVE_SHIPMENT;
            }

            if ($shipment->canRetry()) {
                $actions[] = self::ACTION_RETRY;
            }

            if ($shipment->canRto()) {
                $actions[] = self::ACTION_RTO;
            }

            if ($shipment->canReturn()) {
                $actions[] = self::ACTION_RETURN_ITEMS;
            }

            if ($shipment->canResendOTP()) {
                $actions[] = self::ACTION_RESEND_OTP;
            }

            $actions[] = self::ACTION_VIEW_POD;

            if($shipment->order->type == Order::TYPE_STOCK_TRANSFER){
                $actions[] = self::ACTION_ADD_EWAY_BILL_NUMBER;
            }

        }
//        $actions[] = self::ACTION_ADD_TRANSACTION;
        return $actions;
    }

    public static function canCreate()
    {
        $openManifestCount = static::where('created_at', '<', Carbon::now()->subDays(37))->whereNotIn('status', [Manifest::STATUS_CLOSED, Manifest::STATUS_CANCELLED])->count();

        return $openManifestCount == 0 ? true : false;
    }

    public function locations(): BelongsToMany
    {
        return $this->belongsToMany(WarehouseRack::class, 'manifest_warehouse_rack', 'manifest_id','rack_id');
    }
}
