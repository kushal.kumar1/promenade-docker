<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDemandItems extends Model
{
    use SoftDeletes;

    protected $fillable = ['*'];

    public function scopeDemand($q, $val) {
        return $q->where('product_demand_id', $val);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }
}