<?php

namespace Niyotail\Models;

use Illuminate\Support\Facades\DB;
use Niyotail\Queries\WarehouseFilter;

class Invoice extends Model
{
    use WarehouseFilter;

    const STATUS_GENERATED = "generated";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_SETTLED = "settled";
    const STATUS_PARTIALLY_SETTLED = "partially_settled";

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function scopeToBeSettled($query)
    {
        return $query->whereIn('status', [self::STATUS_GENERATED,self::STATUS_PARTIALLY_SETTLED]);
    }

    public function canBeSettled()
    {
        return in_array($this->status, [self::STATUS_GENERATED,self::STATUS_PARTIALLY_SETTLED, self::STATUS_SETTLED]);
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'source');
    }

    public function invoiceTransactions()
    {
        return $this->belongsToMany(Transaction::class, 'invoices_transactions')->withPivot('amount');
    }

    public function settledAmount()
    {
        return $this->hasOne(InvoiceTransaction::class)
            ->select('invoice_id', DB::raw("SUM(amount) as amount"))->groupBy('invoice_id');
    }

    public function getPrettyAmountAttribute()
    {
        return number_format($this->amount, 2);
    }
}
