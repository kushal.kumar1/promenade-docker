<?php


namespace Niyotail\Models;


class WhGroupAbc extends Model
{
    protected $table = 'wh_group_abc';

    public function scopeOpenDemand($query)
    {
        return $query->where('demand_generated', 1);
    }
}