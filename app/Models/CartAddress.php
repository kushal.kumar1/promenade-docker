<?php

namespace Niyotail\Models;

class CartAddress extends Model
{
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
