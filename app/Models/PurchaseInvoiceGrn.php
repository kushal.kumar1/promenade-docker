<?php


namespace Niyotail\Models;


use Illuminate\Database\Eloquent\Builder;
use Niyotail\Queries\WarehouseFilter;

class PurchaseInvoiceGrn extends Model
{
    use WarehouseFilter;

    const STATUS_GENERATED = 'generated';
    const STATUS_COMPLETED = 'completed';
    protected $table = 'purchase_invoice_grn';

    public function createdBy()
    {
        return $this->belongsTo(Employee::class, 'created_by');
    }

    public function invoice()
    {
        return $this->hasOne(PurchaseInvoice::class, 'id', 'purchase_invoice_id');
    }

    public function items()
    {
        return $this->hasMany(PurchaseInvoiceGrnItem::class, 'grn_id', 'id');
    }

//    protected $id;
//    protected $purchase_invoice_id;
//    protected $grn_path;
//    protected $delivery_date;
//    protected $status;

    /**
     * @return mixed
     */
    public function getPurchaseInvoiceId()
    {
        return $this->purchase_invoice_id;
    }

    /**
     * @param $purchase_invoice_id
     * @return $this
     */
    public function setPurchaseInvoiceId($purchase_invoice_id)
    {
        $this->purchase_invoice_id = $purchase_invoice_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGrnPath()
    {
        return $this->grn_path;
    }

    /**
     * @param $grn_path
     * @return $this
     */
    public function setGrnPath($grn_path)
    {
        $this->grn_path = $grn_path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

    /**
     * @param $delivery_date
     * @return $this
     */
    public function setDeliveryDate($delivery_date)
    {
        $this->delivery_date = $delivery_date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getPrettyGrnReferenceId()
    {
        $invRef = $this->invoice->reference_id;
        return "GRN-" . $invRef . "-" . $this->getId();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPrettyGrnTotal()
    {
        $total = 0;
        $grnItems = $this->items;

        foreach ($grnItems as $item) {
            $invoiceItem = $item->item;
            $variant = $item->variant;
            if ($variant) $total += $invoiceItem->total_invoiced_cost * $item->quantity * $variant->quantity;
        }

        return number_format($total);
    }
}