<?php


namespace Niyotail\Models;


class PurchaseInvoiceCartItems extends Model
{
    public function getTableColumns() {
        return $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
    }

    public function purchaseInvoice()
    {
        return $this->belongsTo(PurchaseInvoiceCart::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productRequest()
    {
        return $this->belongsTo(ProductRequest::class, 'product_id', 'id');
    }

    public function getPrettyInvoiceCostAttribute()
    {
        return number_format($this->total_invoiced_cost, 2);
    }

    public function scopeCart($q, $cartId)
    {
        return $q->where('purchase_invoice_cart_Id', $cartId);
    }

    public function scopeSku($q, $sku)
    {
        return $q->where('sku', $sku);
    }

    public function scopeProduct($q, $id)
    {
        return $q->where('product_id', $id);
    }

    public function scopeVendorBatchNo($q, $batchNo)
    {
        return $q->where('vendor_batch_number', $batchNo);
    }

    public function getPrettyTotalAttribute()
    {
        $unitFactor = 1;
        if($this->value_type == "per_unit") {
            $unitFactor = $this->quantity;
        }
        return number_format($this->cost*$unitFactor, 2);
    }

    public function getSortedFields()
    {
        $baseFields = [
            'quantity',
            'sku',
            'manufacturing_date',
            'expiry_date',
            'vendor_batch_number',
            'product_rack'
        ];

        $baseCost = ['cost'];

        $discounts = [
//            'trade_discount',
//            'scheme_discount',
//            'qps_discount',
            'cash_discount',
            'discount',
        ];

        $preInvoiceFields = [
            'total_taxable',
            'taxes',
            'tax',
        ];

        $postTaxDiscount = [
//            'post_tax_discount_type',
            'post_tax_discount',
//            'reward_points_cashback',
//            'program_schemes',
        ];

        $totalInvoicedCost = [
            'total_invoiced_cost',
        ];

        $postInvoiceDiscount = [
            'post_invoice_schemes',
            'post_invoice_tot',
            'post_invoice_qps',
            'post_invoice_claim',
            'post_invoice_incentive',
            'post_invoice_other',
        ];

        $effectiveCost = [
            'total_effective_cost',
        ];

        return [
            "cost_info" => [
                'label' => 'Basic details',
                'fields' => $baseFields
            ],
            "base_cost" => [
                'label' => 'Cost',
                'fields' => $baseCost
            ],
//            "total_invoiced_cost" => [
//                'label' => 'Invoiced Cost',
//                'fields'=> $totalInvoicedCost
//            ],
//            "effective_cost" => [
//                'label' => 'Effective cost',
//                'fields' => $effectiveCost
//            ],
            "discounts" => [
                'label' => 'Discount details',
                'fields' => $discounts
            ],
//            "pre_invoice_fields" => [
//                'label' => 'Tax details',
//                'fields' => $preInvoiceFields
//            ],
            "post_tax_discount" => [
                'label' => 'Post tax discount',
                'fields' => $postTaxDiscount
            ],
//            "post_invoice_discount" => [
//                'label' => 'Post invoice discount',
//                'fields' => $postInvoiceDiscount
//            ]
        ];
    }
}