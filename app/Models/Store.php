<?php

namespace Niyotail\Models;

use Illuminate\Support\Facades\DB;
use Niyotail\Helpers\CreditLimitHelper;
use Niyotail\Models\Agent\AgentTarget;
use Niyotail\Queries\StoreQueries;
use ReflectionClass;

class Store extends Model
{
    use StoreQueries;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 2;

    public const TYPE_RETAIL = 'retail';
    public const TYPE_WAREHOUSE = 'warehouse';

    public function setSkuAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function getStatusText()
    {
        $class = new ReflectionClass(static::getClass());
        $constants = $class->getConstants();
        $status = array_search($this->status, $constants);
        return explode('_', $status)[1];
    }

    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }

    public function scopeDisabled($query)
    {
        return $query->where('status', '2');
    }

    public function scopeVerified($query)
    {
        return $query->where('verified', '1');
    }

    public function canCreateOrder()
    {
        return $this->verified && $this->status;
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'store_user');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function pincode()
    {
        return $this->belongsTo(Pincode::class, 'pincode', 'pincode');
    }

    public function pincodeRel()
    {
        return $this->belongsTo(Pincode::class, 'pincode', 'pincode');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function beat()
    {
        return $this->belongsTo(Beat::class);
    }

    public function creditLimits()
    {
        return $this->hasMany(CreditLimit::class);
    }

    public function creditLimit()
    {
        return $this->hasOne(CreditLimit::class)->orderBy('id', 'desc');
    }

    public function creditRequest()
    {
        return $this->hasMany(CreditRequest::class)->orderBy('id', 'desc');
    }

    public function commissionTags()
    {
        return $this->hasMany(CommissionTagStore::class);
    }

    public function getCreditLimitValueAttribute()
    {
        if ($this->credit_allowed) {
            if (!array_key_exists('creditLimit', $this->relations)) {
                throw new \Exception("CreditLimit relation not loaded");
            }

            $relation = $this->getRelation('creditLimit');
            return !empty($relation) ? $relation->value : 0;
        }

        return 0;
    }

    public function getCreditLimitTypeAttribute()
    {
        if ($this->credit_allowed) {
            if (!array_key_exists('creditLimit', $this->relations)) {
                throw new \Exception("CreditLimit relation not loaded");
            }

            $relation = $this->getRelation('creditLimit');
            return !empty($relation) ? $relation->type : "automatic";
        }

        return null;
    }

    public function totalBalance()
    {
        return $this->hasOne(Transaction::class)
            ->select(
                'store_id',
                DB::raw(" SUM(CASE WHEN type = 'credit' THEN amount ELSE 0 END) as 'credit_amount',SUM(CASE WHEN type = 'debit' THEN amount ELSE 0 END) as 'debit_amount'
                 ,SUM(CASE WHEN type = 'credit' THEN - amount ELSE amount END) as balance_amount ")
            )
            ->where('status', Transaction::STATUS_COMPLETED)->groupBy('store_id');
    }

    public function invoices()
    {
        return $this->hasManyThrough(Invoice::class, Order::class)->where('invoices.status', '!=', 'cancelled');
    }

    public function purchaseInvoice()
    {
        return $this->hasMany(StorePurchaseInvoice::class);
    }

    public function scopeOrderByDistance($query, $lat, $lng)
    {
        $query->select(
            '*',
            DB::raw("
            (6371 * acos(
               cos( radians($lat) )
             * cos( radians( stores.lat ) )
             * cos( radians( stores.lng ) - radians($lng) )
             + sin( radians($lat) )
             * sin( radians( stores.lat ) )
               ) )
             AS distance ")
        )
            ->orderByRaw('distance');
    }

    public function scopeWhereDistance($query, $lat, $lng, $radius)
    {
        $haversine = "(6371 * acos(cos(radians(" . $lat . "))
                    * cos(radians(lat))
                    * cos(radians(lng)
                    - radians(" . $lng . "))
                    + sin(radians(" . $lat . "))
                    * sin(radians(lat))))";
        $query->selectRaw("*, {$haversine} as distance")
            ->whereRaw("{$haversine} < ?", [$radius])
            ->orderByRaw('distance');
        // $query->select('*',
        //     DB::raw(" (6371 * acos (cos ( radians($lat) ))* cos( radians( lat ) ) * cos( radians( lng ) - radians($lng) )+ sin ( radians($lat) )* sin( radians( lat ) )) AS distance "))
        //     ->whereRaw("distance <= ?",[$distance])->orderByRaw('distance');
    }

    public function totalPendingBalance()
    {
        return $this->hasOne(Transaction::class)
            ->select(
                'store_id',
                DB::raw(" SUM(CASE WHEN type = 'credit' THEN amount ELSE 0 END) as balance_amount ")
            )
            ->where('status', Transaction::STATUS_PENDING)->groupBy('store_id');
    }

    public function orderItemMetas()
    {
        return $this->hasMany(OrderItemMeta::class);
    }

    public function is1KStore()
    {
        return DB::table('store_tag')->where('tag_id', 8)->where('store_id', $this->id)->exists();
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

//    public function isClub1K()
//    {
//        return DB::table('store_tag')->where('tag_id', 9)->where('store_id', $this->id)->exists();
//    }

    public function canActivate(): bool
    {
        if (($this->verified == 0) || empty($this->beat_id) || empty($this->contact_person) || empty($this->contact_mobile) || empty($this->landmark) || empty($this->pincode) || empty($this->address)) {
            return false;
        }
        return true;
    }

    public function isCreditLimitExceeded(float $currentOrderValue): bool
    {
        if (!$this->credit_allowed) {
            return false;
        }
        if(!empty($this->creditLimit)){
            $creditLimit = $this->creditLimit->value;
            $totalOutStanding = !empty($this->totalBalance) ? $this->totalBalance->balance_amount : 0;

            $confirmedOrdersValue = $this->orders->whereIn('status', [Order::STATUS_MANIFESTED, Order::STATUS_CONFIRMED])->sum('total');
            if (($totalOutStanding + $currentOrderValue + $confirmedOrdersValue) > $creditLimit) {
                return true;
            }
            return false;
        }
        return false;
    }
}
