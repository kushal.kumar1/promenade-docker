<?php


namespace Niyotail\Models;


class PosOrderItem extends Model
{
    public function scopeOrderId($q, $val) {
        return $q->where('pos_order_id', $val);
    }

    public function order()
    {
        return $this->belongsTo(PosOrder::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function itemMeta()
    {
        return $this->hasOne(PosOrderItemMeta::class);
    }

    public function taxes()
    {
        return $this->hasMany(PosOrderItemTax::class);
    }

    public function shipments()
    {
        return $this->hasMany(PosOrderItemShipment::class);
    }

    public function returnItem() {
        return $this->hasMany(PosOrderItem::class, 'parent_id', 'id');
    }
}