<?php

namespace Niyotail\Models;

use Niyotail\Queries\CollectionQueries;

class Collection extends Model
{
    use CollectionQueries;

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeVisible($query)
    {
        return $query->where('is_visible', 1);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function isLeafNode()
    {
        return !!($this->lft+1 == $this->rgt);
    }

    public function isChildOf($parentCollection)
    {
        return !!($this->lft > $parentCollection->lft && $this->rgt < $parentCollection->rgt);
    }

    public function getWidth()
    {
        return $this->rgt - $this->lft+1;
    }
}
