<?php

namespace Niyotail\Models;

use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Niyotail\Models\Model;
use MongoDB\BSON\UTCDateTime;

class Log extends Model
{
//    protected $connection = 'mongodb';

    public function subject()
    {
        return $this->morphTo();
    }

    public function causer()
    {
        return $this->morphTo();
    }

    public function scopeCausedBy($query, $causer)
    {
        return $query
           ->where('causer_type', $causer->getMorphClass())
           ->where('causer_id', $causer->getKey());
    }

    public function scopeForSubject($query, $subject)
    {
        return $query
           ->where('subject_type', $subject->getMorphClass())
           ->where('subject_id', $subject->getKey());
    }

    public function scopeForSubjectRelation($query, $subject)
    {
        return $query
           ->where('subject_type', $subject->getMorphClass())
           ->where('subject_id', $subject->getKey());
    }
}
