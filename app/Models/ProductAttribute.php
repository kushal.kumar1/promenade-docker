<?php

namespace Niyotail\Models;

class ProductAttribute extends Model
{
    protected $table='products_attributes';
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }
}
