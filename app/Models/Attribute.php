<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;
use Niyotail\Models\Product;
use Niyotail\Queries\AttributeQueries;

class Attribute extends Model
{
    use AttributeQueries;
    public function products()
    {
        return $this->belongsToMany(Product::class,'products_attributes');
    }
}
