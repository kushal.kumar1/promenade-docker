<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Niyotail\Models\Model;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItemTax;
use Niyotail\Models\ShipmentItem;
use Niyotail\Models\Shipment;
use Niyotail\Models\ProductVariant;
use Niyotail\Queries\OrderItemQueries;
use ReflectionClass;

class OrderItem extends Model
{
    use OrderItemQueries;

    const STATUS_PROCESSING = "processing";
    const STATUS_MANIFESTED = "manifested";
    const STATUS_BILLED = "billed";
    const STATUS_READY_TO_SHIP = "ready_to_ship";
    const STATUS_DISPATCHED = "dispatched";
    const STATUS_PENDING_RETURN = "pending_return";
    const STATUS_DELIVERED = "delivered";
    const STATUS_RETURNED = "returned";
    const STATUS_PARTIAL_RETURNED = "partial_return";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_RTO = "rto";
    const STATUS_UNFULFILLED = "unfulfilled";


    const ALLOWED_STATUSES_FOR_RETURN = [self::STATUS_DISPATCHED, self::STATUS_DELIVERED, self::STATUS_PARTIAL_RETURNED];

    public function canCancel()
    {
        return ($this->status==self::STATUS_PROCESSING  || $this->status == self::STATUS_MANIFESTED);
    }

    public function scopeOrderId($query, $orderId)
    {
        return $query->where("order_id", $orderId);
    }

    public function scopeProduct($query, $sku)
    {
        return $query->where("sku", $sku);
    }

    public function scopeCancellable($query)
    {
        return $query->whereIn("status", [self::STATUS_PROCESSING, self::STATUS_MANIFESTED]);
    }

    public function scopeCanShip($query)
    {
        return $query->whereIn('status', [self::STATUS_MANIFESTED]);
    }

    public function scopeCanReturn($query)
    {
        return $query->whereIn('status', self::ALLOWED_STATUSES_FOR_RETURN);
    }

    public function canShip()
    {
        return (in_array($this->status, [self::STATUS_MANIFESTED]));
    }

    public function canManifest()
    {
        return (in_array($this->status, [self::STATUS_PROCESSING]));
    }

    public function taxes()
    {
        return $this->hasMany(OrderItemTax::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function rules()
    {
        return $this->belongsToMany(Rule::class, 'order_item_rule')->withPivot('discount');
    }

    public function productAttributes()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_id', 'product_id');
    }

    public function returns()
    {
        return $this->belongsToMany(ReturnOrder::class, 'return_order_items');
    }

    public function shipmentItems()
    {
        return $this->hasMany(ShipmentItem::class);
    }

    public function shipments()
    {
        return $this->hasManyThrough(Shipment::class, ShipmentItem::class, 'order_item_id', 'id', 'id', 'shipment_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function returnOrderItems()
    {
        return $this->hasMany(ReturnOrderItem::class, 'order_item_id', 'id')->latest();
    }

    public function completedReturnOrderItems()
    {
        return $this->hasMany(ReturnOrderItem::class, 'order_item_id', 'id')
            ->with(['returnOrder' => function($q) {
               return $q->where('status', 'completed');
            }])
            ->whereHas('returnOrder', function ($q) {
                return $q->where('status', 'completed');
            })
            ->latest();
    }

    public function getPrettyTotalAttribute()
    {
        return number_format($this->total, 2);
    }

    public function getPrettySubtotalAttribute()
    {
        return number_format($this->subtotal, 2);
    }

    public function getPrettyShippingChargesAttribute()
    {
        return number_format($this->shipping_charges, 2);
    }

    public function getPrettyDiscountAttribute()
    {
        return number_format($this->discount, 2);
    }

    public function getPrettyTaxAttribute()
    {
        return number_format($this->tax, 2);
    }

    public function getPrettyPriceAttribute()
    {
        return number_format($this->price, 2);
    }

    public function getPrettyMrpAttribute()
    {
        return number_format($this->mrp, 2);
    }

    public function inventories()
    {
        return $this->hasMany(OrderItemInventory::class);
    }

    public function itemInventory(): HasOne
    {
        return $this->hasOne(OrderItemFlatInventory::class);
    }

    public function costPrice()
    {
        return $this->hasOne(OrderItemInventory::class)
            ->selectRaw('order_item_id, sum(quantity * unit_cost_price) as cost_price')
            ->groupBy('order_item_id');
    }

    public function getMarginAttribute()
    {
        if (! array_key_exists('costPrice', $this->relations)) {
            throw new \InvalidArgumentException("Cost Price relation not loaded");
        }

        $related = $this->getRelation('costPrice');
        return ($related) ? ($this->total - $related->cost_price) : 0;
    }

    public function getInventoryType()
    {
        return Inventory::TYPE_REGULAR;
    }

    public function meta()
    {
        return $this->hasOne(OrderItemMeta::class);
    }

    public function childItems()
    {
        return $this->hasMany(OrderItem::class, 'parent_id');
    }

    public function parentItem()
    {
        return $this->belongsTo(OrderItem::class, 'parent_id', 'id');
    }

    public function picklistItems()
    {
        return $this->hasMany(PicklistItem::class);
    }

    public function inventoryTransactions()
    {
        return $this->morphMany(InventoryTransaction::class, 'source');
    }
}
