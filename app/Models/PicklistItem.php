<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Niyotail\Queries\PicklistItemQueries;

class PicklistItem extends Model
{
    use PicklistItemQueries;

    const STATUS_PENDING = "pending";
    const STATUS_PICKED = "picked";
    const STATUS_PROCESSED = "processed";
    const STATUS_SKIPPED = "skipped";

    const SKIP_REASON_DAMAGED = 'damaged';
    const SKIP_REASON_EXPIRED = 'expired';
    const SKIP_REASON_NOT_FOUND = 'not_found';

    const INVENTORY_MAP_SKIP_REASONS = [
        self::SKIP_REASON_DAMAGED => FlatInventory::STATUS_DAMAGED,
        self::SKIP_REASON_EXPIRED => FlatInventory::STATUS_EXPIRED,
        self::SKIP_REASON_NOT_FOUND => FlatInventory::STATUS_LOST
    ];

    public function orderItem(): BelongsTo
    {
        return $this->belongsTo(OrderItem::class);
    }

    public function picklist(): BelongsTo
    {
        return $this->belongsTo(Picklist::class);
    }

    public function orderItemInventory(): BelongsTo
    {
        return $this->belongsTo(OrderItemFlatInventory::class, 'order_item_id', 'order_item_id');
    }
}
