<?php

namespace Niyotail\Models;

class ReturnCartItem extends Model
{
    public function returnCart()
    {
        return $this->belongsTo(ReturnCart::class);
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class);
    }
}
