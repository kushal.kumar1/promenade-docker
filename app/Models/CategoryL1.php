<?php


namespace Niyotail\Models;


class CategoryL1 extends Model
{
    protected $table = 'category_l1';

    public function childrens ()
    {
        return $this->hasMany(CategoryL2::class);
    }
}