<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class AppSection extends Model
{
    const MEDIUM_RETAILER = "retail";
    const MEDIUM_AGENT = "agent";

    const TYPE_PRODUCT_LISTING = "product_listing";
    const TYPE_PRODUCT_DETAIL = "product_detail";

    //const SOURCE_PERSONALIZE = "personalize";
    const SOURCE_PRODUCT = "products";

    const LAYOUT_SLIDER = "slider";
    const LAYOUT_TEXT = "text";
    const LAYOUT_IMAGE = "image";
    const LAYOUT_LISTING = "listing";
    const LAYOUT_GRID_2 = "grid_2";
    const LAYOUT_GRID_3 = "grid_3";
    const LAYOUT_GRID_4 = "grid_4";

    public function scopeActive($query)
    {
        return $query->where('status', '1');
    }

    public function sectionItems()
    {
        return $this->hasMany(AppSectionItem::class, 'app_section_id','id');
    }
}
