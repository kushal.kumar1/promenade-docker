<?php

namespace Niyotail\Models;

use Niyotail\Queries\TaxQueries;
use Niyotail\Models\Model;

class Tax extends Model
{
    use TaxQueries;

    public function taxClass()
    {
        return $this->belongsTo(TaxClass::class);
    }
    public function supplyCountry()
    {
        return $this->belongsTo(Country::class);
    }
    public function sourceState()
    {
        return $this->belongsTo(State::class);
    }

    public function supplyState()
    {
        return $this->belongsTo(State::class);
    }

}
