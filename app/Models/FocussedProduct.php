<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class FocussedProduct extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
