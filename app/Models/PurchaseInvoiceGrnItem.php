<?php


namespace Niyotail\Models;


class PurchaseInvoiceGrnItem extends Model
{
    protected $table = 'purchase_invoice_grn_items';

    public function grn()
    {
        return $this->belongsTo(PurchaseInvoiceGrn::class);
    }

    public function item()
    {
        return $this->belongsTo(PurchaseInvoiceItem::class);
    }

    public function variant()
    {
        return $this->hasOne(ProductVariant::class, 'sku', 'sku')->withTrashed();
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function requestedProduct()
    {
        return $this->hasOne(ProductRequest::class, 'id', 'requested_product_id');
    }

    public function inventoryItem() {
        return $this->hasOne(Inventory::class, 'grn_item_id', 'id');
    }

    public function scopeItemId($query, $itemId)
    {
        return $query->where('item_id', $itemId);
    }

    public function inventories()
    {
        return $this->morphMany(FlatInventory::class, 'source');
    }


//    protected $id;
//    protected $grn_id;
//    protected $item_id;
//    protected $quantity;
//    protected $sku;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getGrnId()
    {
        return $this->grn_id;
    }

    /**
     * @param $grn_id
     * @return $this
     */
    public function setGrnId($grn_id)
    {
        $this->grn_id = $grn_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @param $item_id
     * @return $this
     */
    public function setItemId($item_id)
    {
        $this->item_id = $item_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param $sku
     * @return $this
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
        return $this;
    }

    public function getQtyInUnits()
    {
        $qty = $this->getQtyInUnits();
        $totalUnits = $qty;
        $invoiceItem = $this->item;
        $isRequested = $invoiceItem->is_requested;
        if(!$isRequested)
        {
            $orderedVariant = $invoiceItem->productVariant;
            $variantUnits = $orderedVariant->quantity;
            $totalUnits = $variantUnits*$qty;
        }
        return $totalUnits;
    }
}