<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Niyotail\Auth\OtpAuthenticateContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Employee
 *
 * @package Niyotail\Models
 */
class Employee extends AuthUser implements OtpAuthenticateContract, JWTSubject
{
    /**
     * @var string
     */
    protected $table = 'employees';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

//    protected $with = ['warehouses','childWarehouses', 'parentWarehouses'];

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'employee_role');
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param \Niyotail\Models\Permission $permission
     *
     * @return bool
     */
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }

    /**
     * Determine if the user has the given role.
     *
     * @param $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !!$role->intersect($this->roles)->count();
    }

    public function transactions()
    {
        return $this->morphMany(Transaction::class, 'created_by');
    }

    public function logs()
    {
        return $this->morphMany(Log::class, 'created_by');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getOtp()
    {
        return $this->otp;
    }

    public function getOtpExpiry()
    {
        return $this->otp_expiry;
    }

    public function warehouses(): BelongsToMany
    {
        return $this->belongsToMany(Warehouse::class, 'employee_warehouse');
    }

    public function parentWarehouses(): BelongsToMany
    {
        return $this->belongsToMany(Warehouse::class, 'employee_warehouse')->whereNull('parent_id');
    }

    public function childWarehouses(): BelongsToMany
    {
        return $this->belongsToMany(Warehouse::class, 'employee_warehouse')->whereNotNull('parent_id');
    }

    public function picklistsCreated(): HasMany
    {
        return $this->hasMany(Picklist::class, 'created_by');
    }

    public function picklists(): HasMany
    {
        return $this->hasMany(Picklist::class, 'picker_id');
    }
}
