<?php

namespace Niyotail\Models;

use Niyotail\Models\Model;

class RuleTarget extends Model
{ 
    public function attribute()
    {
        return $this->morphTo('attribute', 'type', 'value');
    }
}
