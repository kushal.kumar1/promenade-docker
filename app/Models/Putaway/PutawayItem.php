<?php

namespace Niyotail\Models\Putaway;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Model;
use Niyotail\Models\Storage;
use Niyotail\Models\Product;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;

class PutawayItem extends Model
{
    public function putaway(): BelongsTo
    {
        return $this->belongsTo(Putaway::class);
    }

    public function storage(): BelongsTo
    {
        return $this->belongsTo(Storage::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function migrationItem()
    {
        return $this->belongsTo(InventoryMigrationItem::class, 'inventory_migration_item_id');
    }

    public function flatInventories(): BelongsToMany
    {
        return $this->belongsToMany(FlatInventory::class, 'putaway_item_inventories');
    }
}
