<?php

namespace Niyotail\Models\Putaway;

use Niyotail\Models\Employee;
use Niyotail\Models\Model;
use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Queries\WarehouseFilter;

class Putaway extends Model
{
    use WarehouseFilter;

    const STATUS_OPEN = "open";
    const STATUS_STARTED = 'started';
    const STATUS_CLOSED = "closed";

    public function items()
    {
        return $this->hasMany(PutawayItem::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function migration()
    {
        return $this->belongsTo(InventoryMigration::class);
    }

    public function canClose()
    {
        return $this->items()->where('placed_quantity', null)->get()->isEmpty();
    }

    public function scopeByWarehouseEmployee($query, $warehouseId, $employeeId)
    {
        $query->where(['employee_id' => $employeeId, 'warehouse_id' => $warehouseId]);
    }
}
