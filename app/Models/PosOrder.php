<?php


namespace Niyotail\Models;


use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class PosOrder extends Model
{
    const TYPE_RETAIL = "retail";
    const TYPE_WHOLESALE = "wholesale";

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function items()
    {
        return $this->hasMany(PosOrderItem::class);
    }

    public function creditNote()
    {
        return $this->hasOne(RateCreditNoteOrder::class);
    }

    public function scopeStoreId($q, $id)
    {
        return $q->where('store_id', $id);
    }

    public function scopeOrderId($q, $id) {
        return $q->where('order_id', $id);
    }

    public function shipments(): HasManyThrough
    {
        return $this->hasManyThrough(PosOrderItemShipment::class, PosOrderItem::class);
    }

    public function itemMetas(): HasManyThrough
    {
        return $this->hasManyThrough(PosOrderItemMeta::class, PosOrderItem::class);
    }

    public function isMallOrder() {
        return strpos($this->ref_id, '1KM');
    }
}
