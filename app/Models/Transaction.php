<?php

namespace Niyotail\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Niyotail\Queries\TransactionQueries;

//use Niyotail\Helpers\Log\Traits\LogChanges;
use Niyotail\Models\WarehouseScope;

class Transaction extends Model
{
    use TransactionQueries;
//    /use LogChanges;

    const STATUS_PENDING = "pending";
    const STATUS_FAILED = "failed";
    const STATUS_CANCELLED = "cancelled";
    const STATUS_COMPLETED = "completed";

    const MODE_MANUAL = "manual";
    const MODE_SYSTEM = "system";

    //Debit methods
    const PAYMENT_METHOD_CHEQUE_BOUNCE = "cheque_bounce";
    const PAYMENT_METHOD_CHEQUE_BOUNCE_CHARGES = "cheque_bounce_charges";
    const PAYMENT_METHOD_REVERSE_CASH_DISCOUNT = "reverse_cash_discount";
    const PAYMENT_METHOD_REVERSE_PAYMENT = "reverse_payment";
    const PAYMENT_METHOD_BALANCE_TRANSFERRED_MARG = "balance_transferred_marg";
    const PAYMENT_METHOD_BALANCE_TRANSFERRED_WINOMKAR = "balance_transferred_winomkar";
    const PAYMENT_METHOD_1K_COMMISSION_PAID = "1k_comm_paid";
    const PAYMENT_METHOD_1K_INFRASTRUCTURE_PAID = "1k_infra_paid";
    const PAYMENT_METHOD_1K_DIRECT_SUPPLY_PAID = "paid_1k_direct_supply";
    const PAYMENT_METHOD_1K_RENT_PAID = "1k_rent_reimbursement_paid";
    const PAYMENT_METHOD_1K_ELECTRICITY_PAID = "1k_elec_reimbursement_paid";
    const PAYMENT_METHOD_SUPPLY_NIYOTAIL_PAID = "supply_to_niyotail_paid";
    const PAYMENT_METHOD_1K_FESTIVE_SEASON_SCHEME_PAID = "1k_festive_season_scheme_paid";
    const PAYMENT_METHOD_SALES_APP_PROMO_INCENTIVE_PAID = 'Sales_App_Promo_Incentive_Paid';

    //Credit methods
    const PAYMENT_METHOD_CASH = "cash";
    const PAYMENT_METHOD_CHEQUE = "cheque";
    const PAYMENT_METHOD_NET_BANKING = "net_banking";
    // const PAYMENT_METHOD_PAYTM = "paytm";
    const PAYMENT_METHOD_PAYTM_ABHISHEK = "paytm_abhishek";
    const PAYMENT_METHOD_PAYTM_SANGEETESH = "paytm_sangeetesh";
    const PAYMENT_METHOD_PAYTM_SACHIN = "paytm_sachin";
    const PAYMENT_METHOD_UPI = "UPI";
    const PAYMENT_METHOD_CASH_DISCOUNT = "cash_discount";
    const PAYMENT_METHOD_EXPIRY_DISCOUNT = "expiry_discount";
    const PAYMENT_METHOD_1K_COMMISSION = "1k_comm";
    const PAYMENT_METHOD_1K_INFRASTRUCTURE = "1k_infra";
    const PAYMENT_METHOD_1K_CONSUMER_PROMO = "1k_cons_promo";
    const PAYMENT_METHOD_1K_DISCOUNT = "1k_disc";
    const PAYMENT_METHOD_1K_DIRECT_SUPPLY = "1k_direct_supply";
    const PAYMENT_METHOD_1K_RENT = "1k_rent_reimbursement";
    const PAYMENT_METHOD_1K_ELECTRICITY = "1k_elec_reimbursement";
    const PAYMENT_METHOD_1K_SECURITY = "1k_security";
    const PAYMENT_METHOD_SUPPLY_TO_NIYOTAIL = "supply_to_niyotail";
    const PAYMENT_METHOD_BAD_DEBT_WRITTEN_OFF = "bad_debt_written_off";
    const PAYMENT_METHOD_INTERNAL_CONSUMPTION = "internal_consumption";
    const PAYMENT_METHOD_1K_FESTIVE_SEASON_SCHEME = "1k_festive_season_scheme";
    const PAYMENT_METHOD_STORE_UPI_SCAN = "store_upi_scan";
    CONST PAYMENT_METHOD_STORE_CARD_SWIPE = 'store_card_swipe';
    const PAYMENT_METHOD_CUSTOMER_LOYALTY_REIMBURSEMENT = 'customer_loyalty_reimbursement';
    const PAYMENT_METHOD_SALES_APP_PROMO_INCENTIVE = 'sales_app_promo_incentive';
    const PAYMENT_METHOD_STORE_INTERNET_REIMBURSEMENT = 'store_internet_reimbursement';
    const PAYMENT_METHOD_STORE_REFERRAL_SCHEME = 'store_referral_scheme';
    const PAYMENT_METHOD_ONLINE = "online";

    /* For manual payments */
    const PAYMENT_METHOD_TRADE_IN_CREDITS_D = 'trade_in_credits_D';
    const PAYMENT_METHOD_TRADE_IN_CREDITS_C = 'trade_in_credits_C';


    const TYPE_CREDIT = "credit";
    const TYPE_DEBIT = "debit";

    const MAP_PAYMENT_METHODS = [
    self::TYPE_CREDIT => [
        self::PAYMENT_METHOD_CASH, self::PAYMENT_METHOD_CHEQUE,
        self::PAYMENT_METHOD_NET_BANKING,
        self::PAYMENT_METHOD_PAYTM_ABHISHEK,
        self::PAYMENT_METHOD_PAYTM_SANGEETESH,
        self::PAYMENT_METHOD_PAYTM_SACHIN,
        self::PAYMENT_METHOD_UPI,
        self::PAYMENT_METHOD_CASH_DISCOUNT,
        self::PAYMENT_METHOD_1K_COMMISSION,
        self::PAYMENT_METHOD_1K_INFRASTRUCTURE,
        self::PAYMENT_METHOD_1K_CONSUMER_PROMO,
        self::PAYMENT_METHOD_1K_DISCOUNT,
        self::PAYMENT_METHOD_1K_DIRECT_SUPPLY,
        self::PAYMENT_METHOD_1K_RENT,
        self::PAYMENT_METHOD_1K_ELECTRICITY,
        self::PAYMENT_METHOD_SUPPLY_TO_NIYOTAIL,
        self::PAYMENT_METHOD_INTERNAL_CONSUMPTION,
        self::PAYMENT_METHOD_BAD_DEBT_WRITTEN_OFF,
        self::PAYMENT_METHOD_EXPIRY_DISCOUNT,
        self::PAYMENT_METHOD_1K_FESTIVE_SEASON_SCHEME,
        self::PAYMENT_METHOD_STORE_UPI_SCAN,
        self::PAYMENT_METHOD_1K_SECURITY,
        self::PAYMENT_METHOD_STORE_CARD_SWIPE,
        self::PAYMENT_METHOD_CUSTOMER_LOYALTY_REIMBURSEMENT,
        self::PAYMENT_METHOD_SALES_APP_PROMO_INCENTIVE,
        self::PAYMENT_METHOD_STORE_REFERRAL_SCHEME,
        self::PAYMENT_METHOD_STORE_INTERNET_REIMBURSEMENT,
        self::PAYMENT_METHOD_TRADE_IN_CREDITS_C,
        self::PAYMENT_METHOD_TRADE_IN_CREDITS_D,
        self::PAYMENT_METHOD_ONLINE
    ],

    self::TYPE_DEBIT => [
        self::PAYMENT_METHOD_CHEQUE_BOUNCE,
        self::PAYMENT_METHOD_CHEQUE_BOUNCE_CHARGES,
        self::PAYMENT_METHOD_REVERSE_CASH_DISCOUNT,
        self::PAYMENT_METHOD_BALANCE_TRANSFERRED_MARG,
        self::PAYMENT_METHOD_BALANCE_TRANSFERRED_WINOMKAR,
        self::PAYMENT_METHOD_CASH, self::PAYMENT_METHOD_NET_BANKING, self::PAYMENT_METHOD_1K_COMMISSION_PAID,
        self::PAYMENT_METHOD_1K_DIRECT_SUPPLY_PAID,
        self::PAYMENT_METHOD_1K_INFRASTRUCTURE_PAID,
        self::PAYMENT_METHOD_1K_RENT_PAID,
        self::PAYMENT_METHOD_1K_ELECTRICITY_PAID,
        self::PAYMENT_METHOD_SUPPLY_NIYOTAIL_PAID,
        self::PAYMENT_METHOD_REVERSE_PAYMENT,
        self::PAYMENT_METHOD_1K_FESTIVE_SEASON_SCHEME_PAID,
        self::PAYMENT_METHOD_SALES_APP_PROMO_INCENTIVE_PAID
    ]
];

const MAP_AGENT_PAYMENT_METHODS = [
    self::TYPE_CREDIT => [
        self::PAYMENT_METHOD_CASH,
        self::PAYMENT_METHOD_CHEQUE,
        self::PAYMENT_METHOD_NET_BANKING,
        self::PAYMENT_METHOD_PAYTM_ABHISHEK,
        self::PAYMENT_METHOD_PAYTM_SANGEETESH,
        self::PAYMENT_METHOD_PAYTM_SACHIN,
        self::PAYMENT_METHOD_UPI,
        self::PAYMENT_METHOD_CASH_DISCOUNT
    ],
    self::TYPE_DEBIT => [
        self::PAYMENT_METHOD_CHEQUE_BOUNCE,
        self::PAYMENT_METHOD_CHEQUE_BOUNCE_CHARGES,
        self::PAYMENT_METHOD_REVERSE_CASH_DISCOUNT,
        self::PAYMENT_METHOD_BALANCE_TRANSFERRED_MARG,
        self::PAYMENT_METHOD_BALANCE_TRANSFERRED_WINOMKAR,
        self::PAYMENT_METHOD_CASH,
        self::PAYMENT_METHOD_NET_BANKING
        ]
    ];

    use SoftDeletes;

    public function source()
    {
        return $this->morphTo()->withoutGlobalScope(WarehouseScope::class);
    }

    public function createdBy()
    {
        return $this->morphTo()->withoutGlobalScope(WarehouseScope::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class, 'invoices_transactions')->withPivot('amount');
    }

    public function chequeMetas() {
        return $this->hasOne(ChequeMeta::class);
    }

    public function paymentTag()
    {
        return $this->belongsTo(Tag::class, 'payment_tag', 'id');
    }

    public function settledByCreditTransaction()
    {
        return $this->belongsToMany(self::class, 'transactions_settlement', 'debit_transaction_id', 'credit_transaction_id')->withPivot('amount');
    }

    public function settledDebitTransactions()
    {
        return $this->belongsToMany(self::class, 'transactions_settlement', 'credit_transaction_id', 'debit_transaction_id')->withPivot('amount');
    }

    public function settledAmount()
    {
        return $this->hasOne(TransactionsSettlement::class, 'debit_transaction_id')
            ->select('debit_transaction_id', DB::raw("SUM(amount) as amount"))->groupBy('debit_transaction_id');
    }

    public function scopeToBeSettled($query)
    {
        return $query->where('type', 'debit')
                      ->where(function ($q) {
                          $q->where('payment_method', '!=', self::PAYMENT_METHOD_CHEQUE_BOUNCE)->orWhereNull('payment_method');
                      })
                      ->whereDoesntHave('settledAmount')
                      ->orWhereHas('settledAmount', function ($q) {
                          $q->havingRaw('sum(amount) != transactions.amount');
                      });
    }
}
