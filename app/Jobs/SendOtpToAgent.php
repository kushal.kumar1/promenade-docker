<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Niyotail\Helpers\Sms\Sms;
use Niyotail\Models\Agent;
use Niyotail\Models\User;

class SendOtpToAgent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var User
     */
    private $agent;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->agent->mobile == '+919999999990'){
            return;
        }
        $sms = new Sms(SMS::TYPE_OTP);
        $sms->send($this->agent->mobile, 'sms.otp', ['otp'=>$this->agent->otp]);
        // $sms->driver('sns')->send($this->agent->mobile, 'sms.otp', ['otp'=>$this->agent->otp]);
    }
}
