<?php

namespace Niyotail\Jobs;

use Aws\Sqs\Exception\SqsException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Niyotail\Exceptions\CreditLimitExceededException;
use Niyotail\Exceptions\OrderItemsNotFoundException;
use Niyotail\Models\StoreOrder;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class ConvertToSaleOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var StoreOrderService
     */
    protected $storeOrderService;
    /**
     * @var
     */
    protected $orderService;
    /**
     * @var StoreOrder
     */
    private $storeOrder;


    /**
     * @param StoreOrder $order
     */
    public function __construct(StoreOrder $order)
    {
        $this->storeOrder = $order;
        $this->storeOrderService = new StoreOrderService();
    }

    /**
     *
     */
    public function handle()
    {
        $jobLock = Cache::lock("convert-store-order-{$this->storeOrder->id}")->get(function () {
            $orderService = App::make(OrderService::class);
            $storeOrder = $this->storeOrder;

            try {
                $order = $orderService->create($storeOrder);
                //TODO: Update the warehouse check from id warehouse type
                if ($order->warehouse_id == 1) {
                    (new ShipmentService())->completeDirectPurchase($order);
                }
            } catch (\Exception $ex) {
                if ($ex instanceof OrderItemsNotFoundException) {
                    $storeOrder->status = StoreOrder::STATUS_UNFULFILLED;
                    if (!Str::contains($ex->getMessage(), 'No Stock for items available')) {
                        $storeOrder->cancellation_reason = $ex->getMessage();
                    }
                    $storeOrder->save();
                    \Log::info($ex->getMessage());
                } elseif ($ex instanceof CreditLimitExceededException) {
                    $storeOrder->status = StoreOrder::STATUS_UNFULFILLED;
                    $storeOrder->cancellation_reason = "Credit limit exceeded!";
                    $storeOrder->save();
                    \Log::info($ex->getMessage());
                } else {
                    throw new \Exception($ex->getMessage());
                }
            }
        });
        if (!$jobLock) {
            $this->releaseDuplicateMessage(60);
        }
    }

    private function releaseDuplicateMessage($delay)
    {
        try {
            $this->release($delay);
        } catch (\Exception $ex) {
            if (!$this->causedByMessageNoLongerAvailable($ex)) {
                throw $ex;
            }
        }
    }

    private function causedByMessageNoLongerAvailable(\Exception $ex): bool
    {
        return $ex instanceof SqsException &&
            Str::contains(
                $ex->getAwsErrorMessage(),
                "Message does not exist or is not available for visibility timeout change"
            );
    }
}
