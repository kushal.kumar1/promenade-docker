<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Sms\Sms;
use Niyotail\Models\Employee;

class SendOtpToEmployee implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }


    public function handle()
    {
        if (App::environment() != 'production') {
            return true;
        }
        $sms = new Sms(SMS::TYPE_OTP);
        $sms->send($this->employee->mobile, 'sms.otp', ['otp' => $this->employee->otp]);
        return true;
    }
}