<?php

namespace Niyotail\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendContactUsMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var
     */
    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $toEmail=env('CONTACT_MAIL_ADDRESS');
        $toEmailName=env('CONTACT_MAIL_NAME');
        $data=$this->data;
        Mail::send('emails.contactUs', compact('data'), function ($message) use ($toEmail,$toEmailName,$data) {
            $message->to($toEmail,$toEmailName)
                ->subject('Contact Us Query on - '.Carbon::now()->toDateTimeString());
        });

    }
}
