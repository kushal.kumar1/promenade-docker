<?php

namespace Niyotail\Jobs\StockTransfer;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\StockTransfer;
use Niyotail\Services\FlatInventoryService;

class CompleteStockTransferJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private  $stockTransfer;
    private FlatInventoryService $flatInventoryService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(StockTransfer $stockTransfer)
    {
        $this->stockTransfer = $stockTransfer;
        $this->flatInventoryService = new FlatInventoryService();
    }


    public function handle()
    {
        $transfer = $this->stockTransfer;
        $transfer->status = StockTransfer::STATUS_COMPLETED;
        // TODO:Check to be removed once all the stock transfers less than id 57 are marked as complete.
        if ($transfer->id > 57) {
            foreach ($transfer->items as $item) {
                if (is_null($item->received_quantity)) {
                    throw new ServiceException('Please update the received qty of all items!');
                }
                $inventoriesToBeTransferred = $item->flatInventories->take($item->received_quantity)->pluck('id')->toArray();
                $this->flatInventoryService->completeInventoryTransfer($inventoriesToBeTransferred, $transfer->to_warehouse_id);
            }
        } else {
            foreach ($transfer->items as $item) {
                if (is_null($item->received_quantity)) {
                    throw new ServiceException('Please update the received qty of all items!');
                }
                $this->flatInventoryService->create($item, $transfer->to_warehouse_id, $item->received_quantity);
            }
        }
        $transfer->save();
    }
}
