<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Niyotail\Importers\Excel\AgentTargetImporter;
use Niyotail\Importers\Excel\BadgeImporter;
use Niyotail\Importers\Excel\BrandImporter;
use Niyotail\Importers\Excel\CartImporter;
use Niyotail\Importers\Excel\Cl4CostMapImporter;
use Niyotail\Importers\Excel\InventoryImporter;
use Niyotail\Importers\Excel\InventoryMigrationImporter;
use Niyotail\Importers\Excel\InvoiceZipImporter;
use Niyotail\Importers\Excel\MarketerImporter;
use Niyotail\Importers\Excel\NewCatL1Importer;
use Niyotail\Importers\Excel\NewCatL2Importer;
use Niyotail\Importers\Excel\NewCatL3Importer;
use Niyotail\Importers\Excel\NewCatL4Importer;
use Niyotail\Importers\Excel\ProductDemandImporter;
use Niyotail\Importers\Excel\ProductGroupImporter;
use Niyotail\Importers\Excel\ProductImporter;
use Niyotail\Importers\Excel\ProductVariantImporter;
use Niyotail\Importers\Excel\PurchaseInvoiceCartImporter;
use Niyotail\Importers\Excel\StockTransferInboundImporter;
use Niyotail\Importers\Excel\WarehouseStorageImporter;
use Niyotail\Importers\Excel\StockTransferOutboundImporter;
use Niyotail\Importers\Excel\StockTransferSentImporter;
use Niyotail\Importers\Excel\StoreCreditLimitImporter;
use Niyotail\Importers\Excel\VendorMarketerMappingImporter;
use Niyotail\Importers\Excel\WholesaleCartImporter;
use Niyotail\Models\ImporterAction;

class DataImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $importerAction;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ImporterAction $importerAction)
    {
        $this->importerAction = $importerAction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $importer = null;
        switch ($this->importerAction->type) {
            case ImporterAction::TYPE_PRODUCT:
                $importer = new ProductImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_VARIANT:
                $importer = new ProductVariantImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_CART:
                $importer = new CartImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_VENDOR_CART:
                $importer = new PurchaseInvoiceCartImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_VENDOR_MARKETER_MAP:
                $importer = new VendorMarketerMappingImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_PRODUCT_GROUP:
                $importer = new ProductGroupImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_MARKETER:
                $importer = new MarketerImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_BRAND:
                $importer = new BrandImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_NEW_CAT_L1:
                $importer = new NewCatL1Importer($this->importerAction);
                break;

            case ImporterAction::TYPE_NEW_CAT_L2:
                $importer = new NewCatL2Importer($this->importerAction);
                break;

            case ImporterAction::TYPE_NEW_CAT_L3:
                $importer = new NewCatL3Importer($this->importerAction);
                break;

            case ImporterAction::TYPE_NEW_CAT_L4:
                $importer = new NewCatL4Importer($this->importerAction);
                break;

            case ImporterAction::TYPE_CL4_COST_MAP:
                $importer = new Cl4CostMapImporter($this->importerAction);
                break;


            case ImporterAction::TYPE_WHOLESALE_CART_IMPORTER:
                $importer = new WholesaleCartImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_STOCK_TRANSFER_OUTBOUND:
                $importer = new StockTransferOutboundImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_STOCK_TRANSFER_INBOUND:
                $importer = new StockTransferInboundImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_STOCK_TRANSFER_SENT_IMPORTER:
                $importer = new StockTransferSentImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_INVENTORY_MIGRATION:
                $importer = new InventoryMigrationImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_STORE_CREDIT_LIMIT:
                $importer = new StoreCreditLimitImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_WAREHOUSE_STORAGE:
                $importer = new WarehouseStorageImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_INVOICE_ZIP:
                $importer = new InvoiceZipImporter($this->importerAction);
                break;

            case ImporterAction::TYPE_PRODUCT_DEMAND:
                $importer = new ProductDemandImporter($this->importerAction);

                break;

            default:
                # code...
                break;
        }

        if (!empty($importer)) {
            $importer->start();
        }
    }
}
