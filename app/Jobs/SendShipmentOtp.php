<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Sms\Sms;
use Niyotail\Models\Shipment;
use Niyotail\Models\Order;

class SendShipmentOtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $shipment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Shipment $shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (App::environment() != 'production') {
            return true;
        }
        $shipment = $this->shipment;
        $sms = new Sms();
        if ($shipment->order->type == Order::TYPE_WHOLESALE) {
            $number = $shipment->order->additionalInfo->tsm_number;
            $sms->driver('karix')->send($number, 'sms.shipment_otp', ['otp' => $shipment->otp, 'invoice'=>"Invoice {$shipment->invoice->reference_id}"]);
        } else {
            $users = $shipment->order->store->users->where('status', 1);
            foreach($users as $user) {
                $sms->driver('karix')->send($user->mobile, 'sms.shipment_otp', ['otp' => $shipment->otp, 'invoice'=>"Invoice {$shipment->invoice->reference_id}"]);
            }
        }
    }
}
