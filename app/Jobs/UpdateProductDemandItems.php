<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Niyotail\Models\ProductDemand;

class UpdateProductDemandItems implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $demand;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ProductDemand $demand)
    {
        $this->demand = $demand;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Niyotail\Exceptions\ServiceException
     */
    public function handle()
    {
        Artisan::call("generate:product-demand-items {$this->demand->id}");
    }
}