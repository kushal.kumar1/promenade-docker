<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Niyotail\Services\PosOrder\OneKMallOrderService;
use Niyotail\Services\PosOrder\SyncPosOrderService;


class HandlerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Job $job, array $data)
    {
        $entity = $data['entity'];
        $method = $data['method'];

        /*
            This will decide which service to call based on entity and method combination
        */
        if ($entity == "pos_order") {

            if ($method == "sync_order") {
                $syncOrderService = new SyncPosOrderService();
                $syncOrderService->syncOrder($data);
                return true;
            }

            if ($method == "sync_1kmall_order") {
                $oneKMallService = new OneKMallOrderService();
                $oneKMallService->createOrder($data);
                return true;
            }
        }
    }
}
