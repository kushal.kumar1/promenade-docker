<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Niyotail\Models\Log;

class CreateLog implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $log;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Log $log)
    {
        $this->log = $log;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->log->save();
    }
}
