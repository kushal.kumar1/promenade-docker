<?php


namespace Niyotail\Jobs;


use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Niyotail\Services\RateCreditNoteService;

class StoreRateCreditNote implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $storeId;
    protected $date;
    protected $endDate;
    protected $registerNumber;
    protected $rateCreditNoteService;

    /**
     * Create a new job instance.
     *
     * @param $storeId
     * @param $date
     * @param $endDate
     */
    public function __construct($storeId, $date, $endDate, $registerNumber = null)
    {
        $this->storeId = $storeId;
        $this->date = Carbon::parse($date);
        $this->endDate = $endDate ? Carbon::parse($endDate) : null;
        $this->rateCreditNoteService = new RateCreditNoteService();
        $this->registerNumber = $registerNumber;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $storeId = $this->storeId;
        $date = $this->date;
        $endDate = $this->endDate;
        $registerNumber = $this->registerNumber;
        $this->rateCreditNoteService->createRateCreditNotesByStoreAndDate($storeId, $date, $endDate, $registerNumber);
    }
}