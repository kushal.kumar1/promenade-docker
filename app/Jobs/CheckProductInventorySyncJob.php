<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Niyotail\Events\Inventory\MallProductsInventorySync;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Product;

class CheckProductInventorySyncJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $type;

    protected $inventoryId;

    /**
     * @param StoreOrder $order
     */
    public function __construct(string $type, int $inventoryId)
    {
        $this->type = $type;
        $this->inventoryId = $inventoryId;
    }

    /**
     *
     */
    public function handle()
    {
        $flatInventory = FlatInventory::with('product.tags')->find($this->inventoryId);
        try {
            if (($flatInventory->warehouse_id == 2) && !empty($flatInventory->product->tags->where('id', 14))) {
                $product = Product::inventoryCount([$flatInventory->warehouse_id])->find($flatInventory->product_id);
                switch ($this->type) {
                    case 'outbound':
                        if ($product->current_inventory == 0) {
                            event(new MallProductsInventorySync($product));
                        }
                        break;
                    case 'inbound':
                        event(new MallProductsInventorySync($product));
                }
            }
        } catch(\Exception $e) {
            \Log::info('Inventory_id: '.$this->inventoryId.' '.$e->getMessage());
        }
    }
}
