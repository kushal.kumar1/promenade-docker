<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Niyotail\Helpers\Sms\Sms;
use Niyotail\Models\User;

class SendOtpToCustomer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var User
     */
    private $customer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sms = new Sms(SMS::TYPE_OTP);
        $sms->send($this->customer->mobile, 'sms.otp', ['otp'=>$this->customer->otp]);
        // $sms->driver('sns')->send($this->customer->mobile, 'sms.otp', ['otp'=>$this->customer->otp]);
    }
}
