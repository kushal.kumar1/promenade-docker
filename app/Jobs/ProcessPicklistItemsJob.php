<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\PicklistService;

class ProcessPicklistItemsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;//, SerializesModels;

    private Picklist $picklist;
    protected PicklistService $picklistService;
    protected FlatInventoryService $flatInventoryService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Picklist $picklist)
    {
        $this->picklist = $picklist;
        $this->picklistService = new PicklistService();
        $this->flatInventoryService = new FlatInventoryService();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws ServiceException
     */
    public function handle()
    {
        $orderItems = $this->picklist->orderItems;
        $orderItems->loadMissing('itemInventory');
        $orderService = app(OrderService::class);
        $orderItemService = app(OrderItemService::class);
        $orderService->manifestOrders($orderItems->pluck('order_id')->unique()->toArray());
        $orderItemService->massStatusUpdate($orderItems->pluck('id')->toArray(), OrderItem::STATUS_MANIFESTED);
        $this->flatInventoryService->markGoodInventories($orderItems->pluck('itemInventory.flat_inventory_id')->toArray(), FlatInventory::STATUS_MANIFESTED);
        $this->picklistService->updatePicklistStatus($this->picklist, Picklist::STATUS_GENERATED);
    }
}
