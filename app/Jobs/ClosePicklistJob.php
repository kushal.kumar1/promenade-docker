<?php

namespace Niyotail\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Services\FlatInventoryService;
use Niyotail\Services\Order\OrderItemService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\Order\ShipmentService;
use Niyotail\Services\PicklistService;

class ClosePicklistJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected PicklistService $picklistService;
    private  $picklist;
    protected FlatInventoryService $flatInventoryService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Picklist $picklist)
    {
        $this->picklist = $picklist;
        $this->picklistService = new PicklistService();
        $this->flatInventoryService = new FlatInventoryService();
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws ServiceException
     */
    public function handle()
    {
        if($this->picklist->status == Picklist::STATUS_CLOSED){
            return;
        }
        $this->picklist->items->loadMissing('orderItemInventory');
        $skippedItems = $this->picklist->items->where('status', PicklistItem::STATUS_SKIPPED)->groupBy('skip_reason');
        $orderItemService = new OrderItemService();
        foreach ($skippedItems as $skipReason => $items) {
            $orderItemIds = $items->pluck('order_item_id')->toArray();
            $orderItemService->massStatusUpdate($orderItemIds, OrderItem::STATUS_UNFULFILLED, $skipReason);
            $inventoryIds = $items->pluck('orderItemInventory.flat_inventory_id')->toArray();
            //To Be Removed. Check for old picklists
            if($skipReason == 'cancel_order_item'){
                $this->flatInventoryService->markGoodInventories($inventoryIds, FlatInventory::STATUS_READY_FOR_SALE);
            }else{
                $this->flatInventoryService->markBadInventories($inventoryIds, PicklistItem::INVENTORY_MAP_SKIP_REASONS[$skipReason]);
            }
        }
        $orderIds = $this->picklist->orderItems->unique('order_id')->pluck('order_id')->toArray();
        $orders = Order::with('picklistItems')->whereIn('id', $orderIds)->get();
        if ($orders->isNotEmpty()) {
            foreach ($orders as $order) {
                $shipmentService = new ShipmentService();
                $shipmentService->createFromPicklist($this->picklist, $order);

                //Update Order Status
                app(OrderService::class)->afterPicklistUpdate($order);
            }
        }
        $inventoryIds = $this->picklist->items->where('status', PicklistItem::STATUS_PICKED)
            ->pluck('orderItemInventory.flat_inventory_id')->toArray();
        $this->flatInventoryService->markGoodInventories($inventoryIds, FlatInventory::STATUS_BILLED);
        $this->picklistService->updatePicklistStatus($this->picklist, Picklist::STATUS_CLOSED);
    }
}
