<?php

namespace Niyotail\Helpers\Niyoos;

use Illuminate\Support\Facades\Request;
use Psr\Http\Message\StreamInterface;

class NiyoOSResponse
{

    protected $statusCode;

    /**
     * @var
     */
    protected $body;

    /**
     * @return mixed
     * @author Ankit
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }


    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function setBody(StreamInterface $body)
    {
        $contents = json_decode($body->getContents(), true);
        // if(!empty($contents['links'])){
        //     $contents['links'] = $this->updateLinks($contents['links']);
        // }
        $this->body = $contents;
        return $this;
    }

    private function updateLinks($links)
    {
        $currentPage = $links['meta']['pagination']['current_page'];
        $links['self'] = $this->makeUrl($currentPage);
        $links['first'] = $this->makeUrl(1);
        $links['prev'] = (!empty($links['prev']))?$this->makeUrl($currentPage-1):null;
        $links['next'] = (!empty($links['next']))?$this->makeUrl($currentPage+1):null;
        $links['last'] = $this->makeUrl($links['meta']['pagination']['last_page']);
        return $links;
    }

    private function makeUrl($page)
    {
        $requestAll = Request::all();
        $requestAll['page']=$page;
        return Request::url().'?'.http_build_query($requestAll);
    }

}
