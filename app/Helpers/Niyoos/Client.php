<?php

namespace Niyotail\Helpers\Niyoos;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\HttpFoundation\File\File;


class Client
{
    /**
     * @var
     */
    private $url;

    /**
     * @var
     */
    private $secret;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->setAuthenticationParameters();
    }

    private function setAuthenticationParameters()
    {
        $this->secret = config('services.niyoos.secret');
        $this->url = config('services.niyoos.api_base');
    }

    public function dispatch($api, $method, array $parameters = [])
    {
        $httpClient = new HttpClient(['base_uri' => $this->url]);
        $option = $this->getOptionName(strtolower($method));
        $parameters['secret'] = $this->secret;
        try {
            if ($option == 'multipart')
                $parameters = $this->processParameters($parameters);
            $response = $httpClient->$method($api, [$option => $parameters]);
        } catch (RequestException $e) {
            $response = $e->getResponse();
        }
        return $this->mapClientResponseToNiyoOSResponse($response);
    }


    private function mapClientResponseToNiyoOSResponse(Response $response)
    {
        return (new NiyoOSResponse)->setStatusCode($response->getStatusCode())->setBody($response->getBody());
    }

    /**
     * @param $method
     *
     *
     * @return string
     * @author Ankit
     */
    private function getOptionName($method)
    {
        if ($method == 'post') {
            return 'multipart';
        } else
            return 'query';
    }

    private function processParameters(array $parameters)
    {
        $processedParameters = [];
        foreach ($parameters as $key => $parameter) {
            if (!$parameter instanceof File) {
                if (is_array($parameter)) {
                    foreach ($parameter as $value) {
                        $processedParameters[] = ['name' => $key . "[]", 'contents' => $value];
                    }
                } else {
                    $processedParameters[] = ['name' => $key, 'contents' => $parameter];
                }
            } else {
                $processedParameters[] = ['name' => $key, 'contents' => fopen($parameter->path(), 'r'), 'filename' => $parameter->getClientOriginalName()];
            }
        }
        return $processedParameters;
    }

}
