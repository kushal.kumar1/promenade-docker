<?php

namespace Niyotail\Helpers\RuleEngine;

use Niyotail\Helpers\RuleEngine\DataProviders\DataProvider;
use Niyotail\Models\Rule;

class Action
{
    public function getValue(Rule $rule, DataProvider $dataProvider)
    {
        switch ($rule->type) {
            case Rule::TYPE_PRODUCT:
                return $this->getDiscount($rule, $dataProvider->get('price'), $dataProvider->get('quantity'), $dataProvider->get('discount'));

            case Rule::TYPE_ORDER:
                return $this->getDiscount($rule, $dataProvider->get('total_amount'), 1);

            case Rule::TYPE_PRICING:
                return $this->getPrice($rule, $dataProvider);
        }

        throw new Exception("Invalid Rule Type", 1);
    }

    private function getPrice(Rule $rule, DataProvider $dataProvider)
    {
        $newPrice = $dataProvider->get('price');
        switch ($rule->offer) {
            case Rule::OFFER_NEW_PRICE:
                $newPrice = $rule->value;
                break;

            case Rule::OFFER_MARKUP:
                $markup = $rule->value;
                $mrp = $dataProvider->get('mrp');
                $newPrice = $mrp / (1 + $markup/100);
                break;

            case Rule::OFFER_MARGIN:
                $margin = $rule->value;
                $mrp = $dataProvider->get('mrp');
                $newPrice = (1 - $margin/100) * $mrp;
                break;
        }

        return $newPrice;
    }

    public function getDiscount(Rule $rule, $price, $quantity, $usedDiscount = 0)
    {
        $discount = 0;

        switch ($rule->offer) {
            case Rule::OFFER_PERCENTAGE:
                $discount = $rule->value * $price / 100;
                break;

            case Rule::OFFER_FLAT:
                $discount = $rule->value;
                break;

            case Rule::OFFER_NEW_PRICE:
                $discount = $price - $rule->value;
                break;
        }

        $discount = round($discount * $quantity, 2);
        $discount = min($discount, $price * $quantity - $usedDiscount);
        $discount = max(0, $discount);

        return $discount;
    }
}
