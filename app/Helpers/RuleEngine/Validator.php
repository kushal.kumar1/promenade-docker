<?php

namespace Niyotail\Helpers\RuleEngine;

use Niyotail\Models\Rule;
use Niyotail\Helpers\RuleEngine\DataProviders\DataProvider;
use Niyotail\Models\Condition;

class Validator
{
    private $attributes;

    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    public static function isValid(Rule $rule, DataProvider $dataProvider, $attributes = [])
    {
        $validator = new Validator($attributes);
        foreach ($rule->conditions as $condition) {
            if ($validator->isValidCondition($condition, $dataProvider)) {
                return true;
            }
        }

        return false;
    }

    public function isValidCondition(Condition $condition, DataProvider $dataProvider)
    {
        $groupedExpressions = $condition->expressions->groupBy('name');
        foreach ($groupedExpressions as $name => $expressions) {
            $value = null;
            if (in_array($name, ['brand_quantity', 'brand_amount'])) {
                $brandIds = $groupedExpressions->has('brand') ? $groupedExpressions->get('brand')->pluck('value')->toArray() : [];
                $marketerIds = $groupedExpressions->has('marketer') ? $groupedExpressions->get('marketer')->pluck('value')->toArray() : [];
                if ($name == 'brand_quantity') {
                    $value = $dataProvider->getBrandQuantity($brandIds, $marketerIds);
                } elseif ($name == 'brand_amount') {
                    $value = $dataProvider->getBrandAmount($brandIds, $marketerIds);
                }
            } else {
                $value = $dataProvider->get($name);
            }
            $isNumber = $dataProvider->isNumber($name);
            if (!$this->isValidExpression($name, $value, $expressions, $isNumber)) {
                return false;
            }
        }

        return true;
    }

    protected function isValidExpression($name, $value, $expressions, $isNumber)
    {
        if (!empty($this->attributes) && !in_array($name, $this->attributes)) {
            return true;
        }

        if ($isNumber) {
            $expressions = $expressions->filter(function ($expression) use ($value) {
                return $expression->value <= $value && (empty($expression->max_value) || $value <= $expression->max_value);
            });
        } elseif (is_array($value)) {
            $expressions = $expressions->whereIn('value', $value);
        } else {
            $expressions = $expressions->where('value', $value);
        }

        if ($expressions->isNotEmpty()) {
            return true;
        }

        return false;
    }
}
