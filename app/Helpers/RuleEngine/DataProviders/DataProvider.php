<?php

namespace Niyotail\Helpers\RuleEngine\DataProviders;

abstract class DataProvider
{
    protected $medium;

    public function get($name)
    {
        $name = str_replace("_", " ", $name);
        $name = 'get' . ucwords($name);
        $name = str_replace(" ", "", $name);
        return $this->$name();
    }

    public function isNumber($name)
    {
        $numbers = ['price', 'quantity', 'brand_quantity', 'brand_amount', 'total_quantity', 'total_amount'];
        if (in_array($name, $numbers)) {
            return true;
        }

        return false;
    }

    public function setMedium($medium)
    {
        $this->medium = $medium;
        return $this;
    }

    protected function getMedium()
    {
        return $this->medium;
    }

    protected function getStore()
    {
        return null;
    }

    protected function getBeat()
    {
        return null;
    }

    protected function getVariant()
    {
        return null;
    }

    protected function getCollection()
    {
        return [];
    }

    protected function getBrand()
    {
        return null;
    }

    protected function getMarketer()
    {
        return null;
    }

    protected function getMrp()
    {
        return 0;
    }

    protected function getPrice()
    {
        return 0;
    }

    protected function getQuantity()
    {
        return 1;
    }

    protected function getTotalQuantity()
    {
        return 1;
    }

    public function getBrandQuantity(array $brandIds, array $marketerIds)
    {
        return 0;
    }

    public function getBrandAmount(array $brandIds, array $marketerIds)
    {
        return 0;
    }

    protected function getTotalAmount()
    {
        return 0;
    }

    protected function getDiscount()
    {
        return 0;
    }
}
