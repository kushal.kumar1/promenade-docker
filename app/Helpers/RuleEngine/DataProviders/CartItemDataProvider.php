<?php

namespace Niyotail\Helpers\RuleEngine\DataProviders;

use Niyotail\Models\CartItem;
use Niyotail\Helpers\RuleEngine\DataProviders\DataProvider;

class CartItemDataProvider extends DataProvider
{
    private $cartItem;

    public function __construct(CartItem $cartItem)
    {
        $cartItem->loadMissing('cart.store', 'productVariant', 'product.collections');
        $this->cartItem = $cartItem;
    }

    protected function getStore()
    {
        return $this->cartItem->cart->store_id;
    }

    protected function getBeat()
    {
        return $this->cartItem->cart->store->beat_id;
    }

    protected function getVariant()
    {
        return $this->cartItem->productVariant->id;
    }

    protected function getCollection(): array
    {
        $collections = $this->cartItem->product->collections->pluck('id')->toArray();
        return $collections;
    }

    protected function getBrand()
    {
        return $this->cartItem->product->brand_id;
    }

    protected function getMarketer()
    {
        return $this->cartItem->product->brand->marketer_id;
    }

    protected function getPrice()
    {
        return $this->cartItem->price;
    }

    protected function getQuantity()
    {
        return $this->cartItem->quantity;
    }

    protected function getDiscount()
    {
        return $this->cartItem->discount;
    }
}
