<?php

namespace Niyotail\Helpers\RuleEngine\DataProviders;

use Niyotail\Helpers\RuleEngine\DataProviders\DataProvider;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Brand;

class CartDataProvider extends DataProvider
{
    private $cart;

    public function __construct(Cart $cart)
    {
        $cart->loadMissing('store', 'items');
        $this->cart = $cart;
    }

    protected function getStore()
    {
        return $this->cart->store_id;
    }

    protected function getBeat()
    {
        return $this->cart->store->beat_id;
    }

    protected function getTotalQuantity()
    {
        $totalQuantity = $this->cart->items->where('source', CartItem::SOURCE_MANUAL)->sum('quantity');
        return $totalQuantity;
    }

    protected function getTotalAmount()
    {
        return $this->cart->total;
    }

    protected function getBrand()
    {
        $products = $this->cart->items->pluck('product')->flatten();
        return $products->pluck('brand_id')->toArray();
    }

    protected function getMarketer()
    {
        $products = $this->cart->items->pluck('product')->flatten();
        return $products->pluck('brand')->flatten()->pluck('marketer_id')->toArray();
    }

    public function getBrandQuantity(array $brandIds, array $marketerIds)
    {
        return $this->getCartItems($brandIds, $marketerIds)
            ->where('source', CartItem::SOURCE_MANUAL)
            ->sum('quantity');
    }

    public function getBrandAmount(array $brandIds, array $marketerIds)
    {
        return $this->getCartItems($brandIds, $marketerIds)
            ->where('source', CartItem::SOURCE_MANUAL)
            ->sum('total');
    }

    public function getCartItems(array $brandIds, array $marketerIds)
    {
        return $this->cart->items->where('source', CartItem::SOURCE_MANUAL)
            ->filter(function ($cartItem) use ($brandIds, $marketerIds) {
                if (!empty($brandIds) && !in_array($cartItem->product->brand_id, $brandIds)) {
                    return false;
                }

                if (!empty($marketerIds) && !in_array($cartItem->product->brand->marketer_id, $marketerIds)) {
                    return false;
                }

                return true;
            });
    }
}
