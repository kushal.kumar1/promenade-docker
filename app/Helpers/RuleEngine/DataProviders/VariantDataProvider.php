<?php

namespace Niyotail\Helpers\RuleEngine\DataProviders;

use Niyotail\Models\Store;
use Niyotail\Models\ProductVariant;
use Niyotail\Helpers\RuleEngine\DataProviders\DataProvider;

class VariantDataProvider extends DataProvider
{
    private $store;
    private $variant;

    public function __construct(ProductVariant $variant, ?Store $store)
    {
        $variant->loadMissing('product.collections');
        $this->variant = $variant;
        $this->store = $store;
    }

    protected function getStore()
    {
        return empty($this->store) ? null : $this->store->id;
    }

    protected function getBeat()
    {
        return $this->store->beat_id;
    }

    protected function getVariant()
    {
        return $this->variant->id;
    }

    protected function getCollection(): array
    {
        $collections = $this->variant->product->collections->pluck('id')->toArray();
        return $collections;
    }

    protected function getBrand()
    {
        return $this->variant->product->brand_id;
    }

    protected function getMarketer()
    {
        return $this->variant->product->brand->marketer_id;
    }

    protected function getMrp()
    {
        return $this->variant->mrp;
    }

    protected function getPrice()
    {
        $store = null;
        if(request()->filled('store_id')) {
          $store = Store::find(request()->store_id);
        }

        return $this->variant->getFinalPrice($store);
    }

    protected function getDiscount()
    {
        return $this->variant->discount;
    }
}
