<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Niyotail\Helpers\RuleEngine;
use Niyotail\Helpers\RuleEngine\Action;
use Niyotail\Models\Cart;
use Niyotail\Models\Rule;
use Niyotail\Helpers\RuleEngine\Validator;
use Niyotail\Helpers\RuleEngine\DataProviders\CartItemDataProvider;
use Niyotail\Models\CartItem;
use Niyotail\Services\Cart\CartService;

class CartItemsRuleProcessor extends RuleProcessor
{
    private $cart;
    private $medium;

    public function __construct(Cart $cart, $medium = null)
    {
        $this->cart = $cart;
        $this->medium = $medium;
    }

    protected function getType()
    {
        return Rule::TYPE_PRODUCT;
    }

    protected function setQuery($baseRulesQuery)
    {
        $baseRulesQuery->isUnderLimit($this->cart->store_id);
    }

    protected function setCondition($conditionQuery)
    {
        $cart = $this->cart;
        $cart->loadMissing('items.productVariant');
        $cart->loadMissing('items.product.collections', 'items.product.brand');
        $conditionQuery->whereValid('beat', $cart->store->beat_id)
            ->isValidForCartItems($cart->items->where('source', CartItem::SOURCE_MANUAL));
        if (!empty($this->medium)) {
            $conditionQuery->whereValid('medium', $this->medium);
        }
    }

    protected function onStart()
    {
        $cart = $this->cart;
        $cart->items->where('source', CartItem::SOURCE_MANUAL)->each(function ($item) {
            $item->discount = 0;
        });
    }

    protected function apply($rules)
    {
        $cart = $this->cart;
        foreach ($cart->items->where('source', CartItem::SOURCE_MANUAL) as $cartItem) {
            $dataProvider = new CartItemDataProvider($cartItem);
            $dataProvider->setMedium($this->medium);
            foreach ($rules as $rule) {
                if (Validator::isValid($rule, $dataProvider)) {
                    $this->process($rule, $cartItem);
                    if ($rule->should_stop == 1) {
                        break;
                    }
                }
            }
        }
    }

    protected function process(Rule $rule, CartItem $cartItem)
    {
        $offered = 0;
        if ($rule->offer == Rule::OFFER_FREE) {
            $offered = $this->addFreeItem($rule, $cartItem, $rule->balance_offer);
        } else {
            $offered = $this->updateDiscount($rule, $cartItem, $rule->balance_offer);
        }

        if (!empty($rule->max_offer)) {
            $rule->balance_offer = isset($rule->balance_offer) ? $rule->balance_offer : $rule->max_offer;
            $rule->balance_offer = $rule->balance_offer - $offered;
        }
    }

    protected function addFreeItem(Rule $rule, CartItem $cartItem, $limit = null)
    {
        $freeQuantity = floor($cartItem->quantity / $rule->source_quantity) * $rule->target_quantity;
        if (isset($limit)) {
            $freeQuantity = min($freeQuantity, $limit);
        }

        if ($freeQuantity > 0) {
            $productVariant = null;
            if ($rule->targets->isNotEmpty()) {
                $productVariant = $rule->targets->first()->attribute;
                RuleEngine::loadPricing($productVariant, $this->cart->store, $this->medium);
            } else {
                $productVariant = $cartItem->productVariant;
            }

            $cartService = app(CartService::class);
            $cartService->add($this->cart, [
                'rule_id' => $rule->id,
                'variant' => $productVariant,
                'quantity' => $freeQuantity,
                'discount' => $freeQuantity * $productVariant->getFinalPrice($cartItem->cart->store),
                'parent_sku' => $cartItem->sku
            ]);
        }

        return $freeQuantity;
    }

    protected function updateDiscount(Rule $rule, CartItem $cartItem, $limit = null)
    {
        $action = new Action();
        $dataProvider = new CartItemDataProvider($cartItem);
        $discount = $action->getValue($rule, $dataProvider);
        if (isset($limit)) {
            $discount = min($discount, $limit);
        }
        $cartItem->discount += $discount;
        $cartItem->appliedRules = empty($cartItem->appliedRules) ? collect([]) : $cartItem->appliedRules;
        $cartItem->appliedRules->put($rule->id, ['discount' => $discount]);
        return $discount;
    }

    protected function onComplete()
    {
        $this->persist($this->cart, true);
    }
}
