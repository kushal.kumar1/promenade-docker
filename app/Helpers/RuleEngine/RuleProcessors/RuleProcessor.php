<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Closure;
use Niyotail\Models\Rule;
use Niyotail\Models\CartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Services\Cart\CartService;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Models\Cart;

abstract class RuleProcessor
{
    public function execute()
    {
        $rules = $this->getRules();
        $this->onStart();
        $this->apply($rules);
        $this->onComplete();
    }

    abstract protected function getType();

    protected function getRules()
    {
        $rulesQuery = Rule::isActive()
            ->where('type', $this->getType())
            ->with('conditions.expressions', 'targets.attribute')
            ->whereHas('conditions', Closure::fromCallable([$this, 'setCondition']))
            ->orderBy('position', 'asc');

        $this->setQuery($rulesQuery);

        $rules = $rulesQuery->get();

        return $rules;
    }

    protected function setQuery($baseRulesQuery)
    {
    }

    abstract protected function setCondition($conditionQuery);

    protected function onStart()
    {
    }

    abstract protected function apply($rules);

    protected function onComplete()
    {
    }

    protected function persist(Cart $cart, $overrideRules = false)
    {
        $cartItems = $cart->items->where('source', CartItem::SOURCE_MANUAL);
        $cartService = app(CartService::class);
        foreach ($cartItems as $item) {
            $appliedRules = $item->appliedRules;
            unset($item->appliedRules);
            $cartService->update($item, $item->quantity, $item->discount, [
                'rules' => empty($appliedRules) ? [] : $appliedRules->toArray(),
                'rules_override' => $overrideRules
            ]);
        }
        event(new RecalculateCart($cart));
        $cart->refresh();
    }
}
