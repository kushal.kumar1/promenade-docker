<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Niyotail\Helpers\RuleEngine;
use Niyotail\Helpers\RuleEngine\Action;
use Niyotail\Helpers\RuleEngine\DataProviders\CartDataProvider;
use Niyotail\Helpers\RuleEngine\Validator;
use Niyotail\Models\Cart;
use Niyotail\Models\Rule;
use Niyotail\Models\CartItem;
use Niyotail\Services\Cart\CartService;

class CartRuleProcessor extends RuleProcessor
{
    private $cart;
    private $medium;

    public function __construct(Cart $cart, $medium = null)
    {
        $this->cart = $cart;
        $this->medium = $medium;
    }

    protected function getType()
    {
        return Rule::TYPE_ORDER;
    }

    protected function setQuery($baseRulesQuery)
    {
        $baseRulesQuery->isUnderLimit($this->cart->store_id);
    }

    protected function setCondition($conditionQuery)
    {
        $cart = $this->cart;
        $products = $cart->items->where('source', CartItem::SOURCE_MANUAL)->pluck('product')->flatten();
        $brandIds = $products->pluck('brand_id')->toArray();
        $marketerIds = $products->pluck('brand')->flatten()->pluck('marketer_id')->toArray();
        $totalQuantity = $cart->items->where('source', CartItem::SOURCE_MANUAL)->sum('quantity');
        $conditionQuery->whereValid('beat', $cart->store->beat_id)
            ->whereValid('brand', $brandIds)
            ->whereValid('marketer', $marketerIds)
            ->whereValid('total_quantity', $totalQuantity, true)
            ->whereValid('total_amount', $cart->total, true);

        if (!empty($this->medium)) {
            $conditionQuery->whereValid('medium', $this->medium);
        }
    }

    protected function apply($rules)
    {
        $cart = $this->cart;
        $dataProvider = new CartDataProvider($cart);
        $dataProvider->setMedium($this->medium);
        foreach ($rules as $rule) {
            if (Validator::isValid($rule, $dataProvider)) {
                $this->process($rule);
                if ($rule->should_stop == 1) {
                    break;
                }
            }
        }
    }

    protected function process(Rule $rule)
    {
        $cartDataProvider = new CartDataProvider($this->cart);
        $cartItems = $this->cart->items->where('source', CartItem::SOURCE_MANUAL);
        $validator = new Validator();
        foreach ($rule->conditions as $condition) {
            if ($validator->isValidCondition($condition, $cartDataProvider)) {
                $groupedExpressions = $condition->expressions->groupBy('name');
                $brandIds = $groupedExpressions->has('brand') ? $groupedExpressions->get('brand')->pluck('value')->toArray() : [];
                $marketerIds = $groupedExpressions->has('marketer') ? $groupedExpressions->get('marketer')->pluck('value')->toArray() : [];
                $cartItems = $cartDataProvider->getCartItems($brandIds, $marketerIds);
                break;
            }
        }

        $offered = 0;
        if ($rule->offer == Rule::OFFER_FREE) {
            $offered = $this->addFreeItem($rule, $cartItems, $rule->balance_offer);
        } else {
            $offered = $this->updateDiscount($rule, $cartItems, $rule->balance_offer);
        }

        if (!empty($rule->max_offer)) {
            $rule->balance_offer = isset($rule->balance_offer) ? $rule->balance_offer : $rule->max_offer;
            $rule->balance_offer = $rule->balance_offer - $offered;
        }
    }

    protected function addFreeItem(Rule $rule, $cartItems, $limit)
    {
        $totalQuantity = $cartItems->sum('quantity');
        $freeQuantity = floor($totalQuantity / $rule->source_quantity) * $rule->target_quantity;
        if (isset($limit)) {
            $freeQuantity = min($freeQuantity, $limit);
        }

        if ($freeQuantity > 0) {
            $productVariant = $rule->targets->first()->attribute;
            RuleEngine::loadPricing($productVariant, $this->cart->store, $this->medium);

            $cartService = app(CartService::class);
            $cartService->add($this->cart, [
                'rule_id' => $rule->id,
                'variant' => $productVariant,
                'quantity' => $freeQuantity,
                'discount' => $freeQuantity * $productVariant->getFinalPrice($cartItems->first()->cart->store)
            ]);
        }

        return $freeQuantity;
    }

    protected function updateDiscount(Rule $rule, $cartItems, $limit = null)
    {
        $totalAmount = $cartItems->sum('total');
        $action = new Action();
        $discount = $action->getDiscount($rule, $totalAmount, 1);
        if (isset($limit)) {
            $discount = min($discount, $limit);
        }
        foreach ($cartItems as $cartItem) {
            $itemDiscount = ($cartItem->total / $totalAmount) * $discount;
            $cartItem->discount += $itemDiscount;
            $cartItem->appliedRules = empty($cartItem->appliedRules) ? collect([]) : $cartItem->appliedRules;
            $cartItem->appliedRules->put($rule->id, ['discount' => $discount]);
        }
        return $discount;
    }

    protected function onComplete()
    {
        $this->persist($this->cart);
    }
}
