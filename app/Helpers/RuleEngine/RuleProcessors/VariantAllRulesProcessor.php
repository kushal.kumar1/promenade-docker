<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Illuminate\Support\Collection;
use Niyotail\Helpers\RuleEngine\Action;
use Niyotail\Helpers\RuleEngine\DataProviders\VariantDataProvider;
use Niyotail\Helpers\RuleEngine\RuleProcessors\RuleProcessor;
use Niyotail\Helpers\RuleEngine\Validator;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Rule;
use Niyotail\Models\Store;

class VariantAllRulesProcessor extends RuleProcessor
{
    private $store;
    private $variants;
    private $medium;

    public function __construct(Collection $variants, Store $store = null, $medium = null)
    {
        $this->store = $store;
        $this->variants = $variants;
        $this->medium = $medium;
    }

    protected function getType()
    {
        return Rule::TYPE_PRODUCT;
    }

    protected function setQuery($baseRulesQuery)
    {
        if (!empty($this->store)) {
            $baseRulesQuery->isUnderLimit($this->store->id);
        }
    }

    protected function setCondition($conditionQuery)
    {
        $variants = $this->variants;
        $conditionQuery->where(function ($variantsQuery) use ($variants) {
            foreach ($variants as $variant) {
                $variantsQuery->orWhere(function ($variantQuery) use ($variant) {
                    $variantQuery->isValidForVariant($variant, 0);
                });
            }
        });
        if (!empty($this->store)) {
            $conditionQuery->whereValid('beat', $this->store->beat_id);
        }

        if (!empty($this->medium)) {
            $conditionQuery->whereValid('medium', $this->medium);
        }
    }

    protected function apply($rules)
    {
        $attributes = ['variant', 'collection', 'price'];
        if (!empty($this->store)) {
            $attributes[] = 'beat';
        }
        foreach ($this->variants as $variant) {
            $variant->rules = collect([]);
            $dataProvider = new VariantDataProvider($variant, $this->store);
            $dataProvider->setMedium($this->medium);
            foreach ($rules as $rule) {
                if (Validator::isValid($rule, $dataProvider, $attributes)) {
                    $this->process($rule, $variant);
                    // if ($rule->should_stop == 1) {
                    //     break;
                    // }
                }
            }
        }
    }

    protected function process(Rule $rule, ProductVariant $variant)
    {
        $variant->rules->push($rule);
    }
}
