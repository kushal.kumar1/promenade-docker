<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Illuminate\Support\Collection;
use Niyotail\Helpers\RuleEngine\Action;
use Niyotail\Helpers\RuleEngine\DataProviders\VariantDataProvider;
use Niyotail\Helpers\RuleEngine\RuleProcessors\RuleProcessor;
use Niyotail\Helpers\RuleEngine\Validator;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Rule;
use Niyotail\Models\Store;

class VariantPricingRuleProcessor extends RuleProcessor
{
    protected $store;
    protected $variants;
    private $medium;

    public function __construct(Collection $variants, Store $store, $medium = null)
    {
        $this->store = $store;
        $this->variants = $variants;
        $this->medium = $medium;
    }

    protected function getType()
    {
        return Rule::TYPE_PRICING;
    }

    protected function setCondition($conditionQuery)
    {
        $variants = $this->variants;
        $conditionQuery->whereValid('beat', $this->store->beat_id);
        $conditionQuery->where(function ($subQuery) use ($variants) {
            foreach ($variants as $variant) {
                $subQuery->orWhere(function ($variantQuery) use ($variant) {
                    $variantQuery->whereValid('variant', $variant->id)
                        ->whereValid('brand', $variant->product->brand_id)
                        ->whereValid('marketer', $variant->product->brand->marketer_id);
                });
            }
        });

        if (!empty($this->medium)) {
            $conditionQuery->whereValid('medium', $this->medium);
        }
    }

    protected function apply($rules)
    {
        foreach ($this->variants as $variant) {
            $dataProvider = new VariantDataProvider($variant, $this->store);
            $dataProvider->setMedium($this->medium);
            foreach ($rules as $rule) {
                if (Validator::isValid($rule, $dataProvider)) {
                    $this->process($rule, $variant);
                    break;
                }
            }
        }
    }

    protected function process(Rule $rule, ProductVariant $variant)
    {
        $action = new Action();
        $dataProvider = new VariantDataProvider($variant, $this->store);
        $variant->final_price = $action->getValue($rule, $dataProvider);
    }
}
