<?php

namespace Niyotail\Helpers\RuleEngine\RuleProcessors;

use Illuminate\Support\Collection;
use Niyotail\Helpers\RuleEngine\Action;
use Niyotail\Helpers\RuleEngine\DataProviders\VariantDataProvider;
use Niyotail\Helpers\RuleEngine\RuleProcessors\RuleProcessor;
use Niyotail\Helpers\RuleEngine\Validator;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Rule;
use Niyotail\Models\Store;

class VariantDiscountRuleProcessor extends RuleProcessor
{
    private $store;
    private $variants;
    private $medium;

    public function __construct(Collection $variants, Store $store, $medium = null)
    {
        $this->store = $store;
        $this->variants = $variants;
        $this->medium = $medium;
    }

    protected function getType()
    {
        return Rule::TYPE_PRODUCT;
    }

    protected function setQuery($baseRulesQuery)
    {
        $baseRulesQuery->isUnderLimit($this->store->id)
            ->where('offer', '!=', Rule::OFFER_FREE)
            ->whereNull('max_offer');
    }

    protected function setCondition($conditionQuery)
    {
        $variants = $this->variants;
        $conditionQuery->whereValid('beat', $this->store->beat_id)
            ->where(function ($variantsQuery) use ($variants) {
                foreach ($variants as $variant) {
                    $variantsQuery->orWhere(function ($variantQuery) use ($variant) {
                        $variantQuery->isValidForVariant($variant);
                    });
                }
            });

        if (!empty($this->medium)) {
            $conditionQuery->whereValid('medium', $this->medium);
        }
    }

    protected function apply($rules)
    {
        foreach ($this->variants as $variant) {
            $variant->appliedRules = collect([]);
            $dataProvider = new VariantDataProvider($variant, $this->store);
            $dataProvider->setMedium($this->medium);
            $variant->discount = 0;
            foreach ($rules as $rule) {
                if (Validator::isValid($rule, $dataProvider)) {
                    $this->process($rule, $variant);
                    if ($rule->should_stop == 1) {
                        break;
                    }
                }
            }
        }
    }

    protected function process(Rule $rule, ProductVariant $variant)
    {
        $action = new Action();
        $dataProvider = new VariantDataProvider($variant, $this->store);
        $discount = $action->getValue($rule, $dataProvider);
        $variant->discount += $discount;
        $variant->appliedRules->push($rule);
    }
}
