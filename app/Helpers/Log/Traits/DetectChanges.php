<?php

namespace Niyotail\Helpers\Log\Traits;

use Niyotail\Models\Model;

trait DetectChanges
{
    protected $oldAttributes = [];

    protected static function bootDetectChanges()
    {
        if (static::eventsToBeRecorded()->contains('updated')) {
            static::updating(function (Model $model) {
                $changes = $model->getAttributeChanges();
                $oldModel = (new static)->setRawAttributes($model->getOriginal());
                $model->oldAttributes =  array_intersect_key($oldModel->toArray(), $changes);
            });
        }
    }

    public function attributeValuesToBeLogged($processingEvent): array
    {
        $properties['attributes'] = $this->getAttributeChanges();

        if (static::eventsToBeRecorded()->contains('updated') && $processingEvent == 'updated') {
            $properties['old'] = $this->oldAttributes;
            $this->oldAttributes = [];
        }

        if (in_array($processingEvent, ['deleted','restored'])) {
            $properties['attributes'] = $this->getAttributes();
        }

        return $properties;
    }

    public function getAttributeChanges(): array
    {
        $changed = $this->getDirty();

        //remove timestamps
        unset($changed['created_at']);
        unset($changed['updated_at']);

        //TODO:: remove guarded attributes
        return $changed;
    }
}
