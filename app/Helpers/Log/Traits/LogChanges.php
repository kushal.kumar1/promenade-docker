<?php

namespace Niyotail\Helpers\Log\Traits;

use Niyotail\Helpers\Log\Traits\DetectChanges;
use Niyotail\Helpers\Log\Logger;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Niyotail\Models\Log;
use Illuminate\Support\Collection;
use Niyotail\Models\Model;
use Illuminate\Support\Arr;

trait LogChanges
{
    use DetectChanges;

    public function logs(): MorphMany
    {
        return $this->morphMany(Log::class, 'subject')->orderBy('_id', 'desc');
    }

    protected static function bootLogChanges()
    {
        static::eventsToBeRecorded()->each(function ($eventName) {
            return static::$eventName(function (Model $model) use ($eventName) {
                if (!$model->shouldLogEvent($eventName)) {
                    return;
                }

                $attrs = $model->attributeValuesToBeLogged($eventName);
                $logger = app(Logger::class)
                    ->performedOn($model)
                    ->withProperties($attrs);

                if (method_exists($model, 'tapActivity')) {
                    $logger->tap([$model, 'tapActivity'], $eventName);
                }

                $logger->log($eventName);
            });
        });
    }

    protected static function eventsToBeRecorded(): Collection
    {
        if (isset(static::$recordEvents)) {
            return collect(static::$recordEvents);
        }

        $events = collect([
            'created',
            'updated',
            'deleted',
        ]);

        if (collect(class_uses_recursive(static::class))->contains(SoftDeletes::class)) {
            $events->push('restored');
        }

        return $events;
    }

    protected function shouldLogEvent(string $eventName): bool
    {
        //Do not log update event triggered before restored event
        if (Arr::has($this->getDirty(), 'deleted_at') && $eventName="updated") {
            if ($this->getDirty()['deleted_at'] === null) {
                return false;
            }
        }

        return true;
    }
}
