<?php

namespace Niyotail\Helpers\Log;

use Illuminate\Auth\AuthManager;
use Niyotail\Models\Log;
use Niyotail\Models\Model;
use Niyotail\Models\AuthUser;
use Niyotail\Jobs\CreateLog;

class Logger
{
    protected $auth;
    protected $log;

    public function __construct(AuthManager $auth)
    {
        $this->auth = $auth;
    }

    public function performedOn(Model $model)
    {
        $this->getLog()->subject()->associate($model);
        return $this;
    }

    public function on(Model $model)
    {
        return $this->performedOn($model);
    }

    public function causedBy($user)
    {
        if (! $user instanceof AuthUser) {
            return $this;
        }

        $this->getLog()->causer()->associate($user);
        return $this;
    }

    private function getActiveUser()
    {
        foreach (array_keys(config('auth.guards')) as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->user();
            }
        }
        return null;
    }

    public function withProperties($properties)
    {
        $this->getLog()->properties = $properties;
        return $this;
    }

    public function log($message)
    {
        //log message only for production env for now
        if (app()->environment('production')) {
            $log = $this->getLog();
            $log->message = $message;
            dispatch(new CreateLog($log));
            $this->log = null;
            return $log;
        }
    }

    protected function getLog()
    {
        if (!isset($this->log)) {
            $this->log = new Log();
            $user = $this->getActiveUser();
            $this->withProperties([])->causedBy($user);
        }
        return $this->log;
    }
}
