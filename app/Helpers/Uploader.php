<?php

namespace Niyotail\Helpers;

use Niyotail\Helpers\File;

class Uploader
{
    private $directory;
    private $files = [];

    public function addFile(File $file)
    {
        $this->files[] = $file;
    }

    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    public function upload()
    {
        foreach ($this->files as $file) {
            $directory = $file->getDirectory();
            if (empty($directory)) {
                $directory = $this->directory;
            }
            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            try {
                $file->get()->move($directory, $file->getName());
            } catch (\FileException $e) {
                //log exception
                continue;
            }
        }
    }

    public function uploadToS3()
    {
    }
}
