<?php

namespace Niyotail\Helpers\Cart;

use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Store;
use Niyotail\Models\Order;
use Niyotail\Services\Cart\CartChecks;
use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Utils;

class CartHelper
{
    use CartChecks;
    private $errors = [];

    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
        $cart->loadMissing('items.product.totalInventory');
        $cart->loadMissing('items.productVariant');
        $cart->loadMissing('store.beat.warehouses');
    }

    public function validateCart()
    {
        $store = Store::active()->verified()->where('id', $this->cart->store_id)->first();
        if (empty($store)) {
            throw new \Exception("You can not place order for this store as it is inactive or unverified.");
        }

        // $warehouseIds = $this->cart->store->beat->warehouses->pluck('id')->toArray();
        $warehouseIds = Utils::getAvailableWarehouseForStore($this->cart->store);

        $skipHelperValidations = (!empty($warehouseIds) && in_array(2, $warehouseIds)) ? true : false;

        foreach ($this->cart->items as $item) {
            try {
                 $this->checkCreditLimit($store);
                if (($this->cart->source == CartItem::SOURCE_IMPORTER) || $skipHelperValidations) {
                    $this->validateProduct($item->productVariant, $this->cart->store, $this->cart->source);
                }
                else if ($item->source != "group_buy") {
                    ProductHelper::validate($item->productVariant, $this->cart->store->beat_id, $warehouseIds);
                    CartItemHelper::checkMOQ($item, $item->quantity);
                    CartItemHelper::checkExistingItemQty($this->cart->id, $item, $warehouseIds);
                }
            } catch (\Exception $e) {
                $this->errors[$item->id] = $e->getMessage();
            }
        }

        return $this->errors;
    }

     private function checkCreditLimit(Store $store)
     {
         $store->load('totalBalance', 'creditLimit');
         $outstandingOrders = Order::whereIn('status', ['confirmed','manifested'])->where('store_id', $store->id)->get();
         $outstandingOrderAmount = $outstandingOrders->sum('total');
         $totalInCart = $this->cart->total;

         $creditLimit = $store->credit_limit_value;
         if ($creditLimit != 0) {
             $actualLimit = $store->totalBalance->balance_amount + $outstandingOrderAmount + $totalInCart;
             if ($actualLimit > $creditLimit) {
                 throw new \Exception("You have reached your Credit Limit. Please clear your dues or remove some items from your cart");
             }
         }
     }
}
