<?php

namespace Niyotail\Helpers;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Invoice;

class InvoiceHelper
{
    /**
     * @param Invoice $invoice
     * @return mixed
     */
    public static function streamPdf(Invoice $invoice)
    {
        $pdf = self::getPdf($invoice);
        return $pdf->stream();
    }

    /**
     * @param Invoice $invoice
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private static function getPdf(Invoice $invoice)
    {
        $invoice->load('order.shippingAddress', 'order.billingAddress', 'order.billingAddress.city.state', 'shipment.orderItems.taxes', 'shipment.orderItems.product.taxClass','order.store', 'shipment.orderItems.productVariant', 'order.store.totalBalance', 'order.store.invoices', 'order.additionalInfo', 'order.warehouse');
        $balance = empty($invoice->order->store->totalBalance) ? "0" : $invoice->order->store->totalBalance->balance_amount;
        $header = view()->make('pdf.invoice.header', compact('invoice', 'balance'))->render();
        $footer = view()->make('pdf.invoice.footer', compact('invoice'))->render();

        return SnappyPdf::loadView('pdf.invoice.sales', ['invoice' => $invoice])
            ->setOption('margin-top', '75mm')
            ->setOption('margin-bottom', '35mm')
            ->setOption('header-html', $header)
            ->setOption('footer-html', $footer)
            // ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }

    /**
     * @param Invoice $invoice
     * @return mixed
     */
    public static function downloadPdf(Invoice $invoice)
    {
        $path = "invoices/$invoice->financial_year/$invoice->number.pdf";
        $disk = config('filesystems.invoice_file_driver');
        /* Generate PDF if it does not exist */
        if (!Storage::disk($disk)->exists($path)) {
            self::generatePdf($invoice);
        }
        return Storage::disk($disk)->download($path);
    }

    /**
     * @param Invoice $invoice
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public static function generatePdf(Invoice $invoice)
    {
        $pdf = self::getPdf($invoice)->output();
        $path = "invoices/$invoice->financial_year/$invoice->number.pdf";
        $disk = config('filesystems.invoice_file_driver');
        Storage::disk($disk)->put($path, $pdf);
    }
}
