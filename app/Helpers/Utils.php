<?php

namespace Niyotail\Helpers;

use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\Product;
use Niyotail\Models\Warehouse;

class Utils
{
    public static $hyphen = '-';
    public static $conjunction = ' and ';
    public static $separator = ', ';
    public static $negative = 'negative ';
    public static $decimal = ' point ';
    public static $dictionary = [
        0 => 'Zero',
        1 => 'One',
        2 => 'Two',
        3 => 'Three',
        4 => 'Four',
        5 => 'Five',
        6 => 'Six',
        7 => 'Seven',
        8 => 'Eight',
        9 => 'Nine',
        10 => 'Ten',
        11 => 'Eleven',
        12 => 'Twelve',
        13 => 'Thirteen',
        14 => 'Fourteen',
        15 => 'Fifteen',
        16 => 'Sixteen',
        17 => 'Seventeen',
        18 => 'Eighteen',
        19 => 'Nineteen',
        20 => 'Twenty',
        30 => 'Thirty',
        40 => 'Fourty',
        50 => 'Fifty',
        60 => 'Sixty',
        70 => 'Seventy',
        80 => 'Eighty',
        90 => 'Ninety',
        100 => 'Hundred',
        1000 => 'Thousand',
        1000000 => 'Million',
        1000000000 => 'Billion',
        1000000000000 => 'Trillion',
        1000000000000000 => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    ];

    public static function getAmountInWords($amount)
    {
        $string = '';
        switch (true) {
            case $amount < 21:
                $string = self::$dictionary[$amount];
                break;
            case $amount < 100:
                $tens = ((int)($amount / 10)) * 10;
                $units = $amount % 10;
                $string = self::$dictionary[$tens];
                if ($units) {
                    $string .= self::$hyphen . self::$dictionary[$units];
                }
                break;
            case $amount < 1000:
                $hundreds = $amount / 100;
                $remainder = $amount % 100;
                $string = self::$dictionary[$hundreds] . ' ' . self::$dictionary[100];
                if ($remainder) {
                    $string .= self::$conjunction . self::getAmountInWords($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($amount, 1000)));
                $numBaseUnits = (int)($amount / $baseUnit);
                $remainder = $amount % $baseUnit;
                $string = self::getAmountInWords($numBaseUnits) . ' ' . self::$dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? self::$conjunction : self::$separator;
                    $string .= self::getAmountInWords($remainder);
                }
                break;
        }
        return $string;
    }

    /**
     * Apply pagination on an array
     *
     * @param array|Collection $items
     * @param int $perPage
     * @param int $page
     *
     * @return LengthAwarePaginator
     */
    public static function paginate($items, $perPage = 15, $page = null)
    {
        $page = $page ?: Paginator::resolveCurrentPage();
        $perPage = $perPage ?: 15;
        $items = $items instanceof Collection ? $items : Collection::make($items);

        return Utils::paginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page'
        ]);
    }

    /**
     * Create a new length-aware paginator instance.
     *
     * @param Collection $items
     * @param int $total
     * @param int $perPage
     * @param int $currentPage
     * @param array $options
     * @return LengthAwarePaginator
     */
    private static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        // if some keys are missing, PHP treats array as associative array,
        // meaning data will be "0": {...}, "1": {...}, etc.
        // We obviously need data to be an array, so we pluck array values
        $items = array_values($items->all());
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
    }

    public static function getAvailableWarehouseForStore($store)
    {
        return [2];
    }

    public static function convertToVariantQuantity($orderQuantity, $productId): array
    {
        $product = Product::find($productId);
        $product->loadMissing('allVariants');
        if ($product->variants->isEmpty()) {
            return [];
        }
        $variants = $product->allVariants;
        $converted = [];
        foreach ($variants as $variant) {
            if ($orderQuantity >= $variant->quantity) {
                if($variant->quantity != 0){
                    $result = intdiv($orderQuantity, $variant->quantity);
                    $orderQuantity = $orderQuantity % $variant->quantity;
                    $converted [] = "$result $variant->value";
                }
            }
        }
        return $converted;
    }

    public static function getCurrentFinancialYear(): string
    {
        $currentDateTime = new \DateTime();
        $year = $currentDateTime->format("y");
        $month = $currentDateTime->format("n");
        if ($month < 4) {
            return ($year - 1) . '-' . $year;
        }
        return $year . '-' . ($year + 1);
    }
}
