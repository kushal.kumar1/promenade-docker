<?php

namespace Niyotail\Helpers;

use Niyotail\Models\Warehouse;

class MultiWarehouseHelper
{

    private array $currentWarehouses = array();
    private $selectedWarehouse;

    public function setWarehouses($currentWarehouses, $selectedWarehouse)
    {
        $this->selectedWarehouse = $selectedWarehouse;
        $this->currentWarehouses = $currentWarehouses;
    }

    public function getCurrentWarehouses()
    {
        return $this->currentWarehouses;
    }

    public static function isValidWarehouse($model, int $id): bool
    {
        $multiWarehouseHelper = app(self::class);
        $object = $model::whereIn('warehouse_id' , $multiWarehouseHelper->getCurrentWarehouses())->where('id', $id)->first();
        if (empty($object)) {
            return false;
        }
        return true;
    }

    public function getSelectedWarehouse()
    {
        return $this->selectedWarehouse;
    }
}