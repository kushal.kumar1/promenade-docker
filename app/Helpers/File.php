<?php

namespace Niyotail\Helpers;

use Niyotail\Helpers\Uploader;
use Illuminate\Http\UploadedFile;

class File
{
    private $file;
    private $name;
    private $mimeType;
    private $directory;

    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
        $this->setName($file->getClientOriginalName().".".$file->guessExtension());
        $this->setMimeType($file->getMimeType());
    }

    public function get()
    {
        return $this->file;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }

    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function setDirectory($directory)
    {
        $this->directory = $directory;
        return $this;
    }

    public function save()
    {
        $uploader = new Uploader;
        $uploader->addFile($this);
        $uploader->upload();
    }
}
