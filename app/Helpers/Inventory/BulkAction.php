<?php


namespace Niyotail\Helpers\Inventory;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Niyotail\Models\InventoryTransaction;

abstract class BulkAction
{
    const TYPE_NEW = "new";
    const TYPE_INCREMENT = "increment";
    const TYPE_DECREMENT = "decrement";
    private $inventoryTransactions;

    /**
     * BulkAction constructor.
     * @param InventoryTransaction [] $inventoryTransactions
     */
    public function __construct(iterable $inventoryTransactions)
    {
        $this->inventoryTransactions = $inventoryTransactions;
        $this->inventoryTransactions->loadMissing('inventory');
    }


    public function commit()
    {
        DB::transaction(function () {
            $this->saveTransactions();
            $this->saveInventories();
        });
    }


    protected function saveTransactions()
    {
        $newTransactions = [];
        $groupedInventoryTransactions = $this->inventoryTransactions->groupBy('inventory_id');
        foreach ($groupedInventoryTransactions as $inventoryTransactions) {
            $currentQuantity = $inventoryTransactions->first()->inventory->quantity;
            foreach ($inventoryTransactions as $inventoryTransaction) {
                $transaction = [
                    'inventory_id' => $inventoryTransaction->inventory_id,
                    'type' => $this->getTransactionType(),
                    'quantity' => $inventoryTransaction->quantity / -1,
                    'previous_quantity' => $currentQuantity,
                    'source_type' => $inventoryTransaction->source_type,
                    'source_id' => $inventoryTransaction->source_id,
                ];
                $currentQuantity = $this->getActionType() == self::TYPE_INCREMENT ? $currentQuantity + 1 : $currentQuantity - 1;
                $newTransactions[] = $transaction;
            }
        }
        try {
            foreach (array_chunk($newTransactions, 5000) as $inventoryTransactions) {
                InventoryTransaction::insert($inventoryTransactions);
            }
        } catch (\Exception $e) {
            Log::error("Duplicate inventory transaction entry for order item {$this->inventoryTransactions->first()->source_id}");
        }
    }

    abstract protected function getTransactionType();

    abstract protected function getActionType();

    protected function saveInventories()
    {
        $groupedInventoryTransactions = $this->inventoryTransactions->groupBy('inventory_id');
        foreach ($groupedInventoryTransactions as $transactions) {
            $inventory = $transactions->first()->inventory;
            $inventory->quantity += -($transactions->sum('quantity'));
            $inventory->save();
        }
    }
}