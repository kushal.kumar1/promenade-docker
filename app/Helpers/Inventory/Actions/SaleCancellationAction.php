<?php
namespace Niyotail\Helpers\Inventory\Actions;

use Niyotail\Models\InventoryTransaction;
use Niyotail\Helpers\Inventory\Action;

class SaleCancellationAction extends Action
{
    private $orderItem;

    //remove null inventory once hack is removed
    public function __construct($orderItem, $quantity, $inventory = null)
    {
        $this->orderItem = $orderItem;
        // Hack to support cancellation of old orders
        if(empty($inventory)) {
            $this->orderItem->load('inventoryTransactions.inventory');
            $inventory = $this->orderItem->inventoryTransactions->where('type',InventoryTransaction::TYPE_SALE)->first()->inventory;
        }
        parent::__construct($inventory, $quantity);
    }

    protected function getSource()
    {
        return $this->orderItem;
    }

    protected function getActionType()
    {
        return Action::TYPE_INCREMENT;
    }

    protected function getTransactionType()
    {
        return InventoryTransaction::TYPE_SALE_CANCELLATION;
    }
}
