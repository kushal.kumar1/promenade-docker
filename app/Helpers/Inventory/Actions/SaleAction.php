<?php
namespace Niyotail\Helpers\Inventory\Actions;

use Niyotail\Models\InventoryTransaction;
use Niyotail\Helpers\Inventory\Action;

class SaleAction extends Action
{
    private $orderItem;

    public function __construct($inventory, $quantity, $orderItem)
    {
        $this->orderItem = $orderItem;
        parent::__construct($inventory, $quantity);
    }

    protected function getSource()
    {
        return $this->orderItem;
    }

    protected function getActionType()
    {
        return Action::TYPE_DECREMENT;
    }

    protected function getTransactionType()
    {
        return InventoryTransaction::TYPE_SALE;
    }
}
