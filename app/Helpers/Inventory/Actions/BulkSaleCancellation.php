<?php


namespace Niyotail\Helpers\Inventory\Actions;


use Niyotail\Helpers\Inventory\Action;
use Niyotail\Helpers\Inventory\BulkAction;
use Niyotail\Models\InventoryTransaction;

class BulkSaleCancellation extends BulkAction
{

    protected function getActionType(): string
    {
        return Action::TYPE_INCREMENT;
    }

    protected function getTransactionType(): string
    {
        return InventoryTransaction::TYPE_SALE_CANCELLATION;
    }
}