<?php
namespace Niyotail\Helpers\Inventory\Actions;

use Niyotail\Models\InventoryTransaction;
use Niyotail\Helpers\Inventory\Action;

class InitAction extends Action
{
    private $orderItem;

    public function __construct($inventory, $quantity)
    {
        parent::__construct($inventory, $quantity);
    }

    protected function getActionType()
    {
        return Action::TYPE_NEW;
    }

    protected function getTransactionType()
    {
        return InventoryTransaction::TYPE_INIT;
    }
}
