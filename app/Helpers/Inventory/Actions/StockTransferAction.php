<?php

namespace Niyotail\Helpers\Inventory\Actions;

use Illuminate\Support\Facades\Log;
use Niyotail\Helpers\Inventory\Action;
use Niyotail\Models\Inventory;
use Niyotail\Models\InventoryTransaction;
use Niyotail\Models\StockTransferItem;

class StockTransferAction extends Action
{
    private $stockTransfer;

    public function __construct(StockTransferItem $item, Inventory $inventory, $quantity)
    {
        $this->stockTransfer = $item->stockTransfer;
        parent::__construct($inventory, $quantity);
    }

    protected function getSource()
    {
        return $this->stockTransfer;
    }

    protected function getActionType()
    {
        return Action::TYPE_DECREMENT;
    }

    protected function getTransactionType(): string
    {
       return InventoryTransaction::TYPE_STOCK_TRANSFER;
    }
}