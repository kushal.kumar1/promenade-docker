<?php
namespace Niyotail\Helpers\Inventory;

use Niyotail\Models\Inventory;
use Niyotail\Models\InventoryTransaction;
use Illuminate\Support\Facades\DB;

abstract class Action
{
    protected $inventory;
    protected $quantity;
    protected $remarks;
    const TYPE_NEW = "new";
    const TYPE_INCREMENT = "increment";
    const TYPE_DECREMENT = "decrement";

    public function __construct(Inventory $inventory, $quantity, $remarks = null)
    {
        $this->inventory = $inventory;
        $this->quantity = $quantity;
        $this->remarks = $remarks;
    }

    protected function incrementInventory()
    {
        $this->inventory->quantity += $this->quantity;
        $this->inventory->save();
    }

    protected function decrementInventory()
    {
        $this->inventory->quantity -= $this->quantity;
        $this->inventory->save();
    }

    protected function getPreviousQuantity()
    {
        return ($this->getActionType() ==  self::TYPE_NEW) ? 0 : $this->inventory->quantity;
    }

    public function commit()
    {
        $prevQuantity = $this->getPreviousQuantity();
        DB::transaction(function() use($prevQuantity){
            $this->modifyInventory();
            $this->saveTransaction($prevQuantity);
        });
    }

    protected function modifyInventory()
    {
        switch($this->getActionType()) {
            case self::TYPE_INCREMENT:
                $this->incrementInventory();
                break;

            case self::TYPE_DECREMENT:
                $this->decrementInventory();
                break;

            case self::TYPE_NEW:
                break;
        }
    }

    protected function saveTransaction($prevQuantity)
    {
        $transaction = new InventoryTransaction();
        $transaction->type = $this->getTransactionType();
        $transaction->quantity = ($this->getActionType() == self::TYPE_DECREMENT) ? -$this->quantity : $this->quantity;
        $transaction->previous_quantity = $prevQuantity;
        $transaction->remarks = $this->getRemarks();
        $transaction->inventory()->associate($this->inventory);
        $transaction->source()->associate($this->getSource());
        $transaction->save();
    }

    protected function getRemarks(){}

    protected function getSource() {}

    abstract protected function getActionType();

    abstract protected function getTransactionType();
}
