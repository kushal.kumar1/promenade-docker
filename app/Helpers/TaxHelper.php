<?php

namespace Niyotail\Helpers;


use Carbon\Carbon;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\State;
use Niyotail\Models\Store;
use Niyotail\Models\Tax;

class TaxHelper
{

    public static function getTaxes($taxClassId, $productMrp, $sourceStateId, $supplyStateId, $supplyCountryId)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $taxes= Tax::getTaxes($taxClassId,$productMrp,$now, $sourceStateId,$supplyCountryId,$supplyStateId);
        if($taxes->isEmpty())
            $taxes = Tax::getTaxes($taxClassId, $productMrp, $now, $sourceStateId, $supplyCountryId);
        return $taxes;
    }

}