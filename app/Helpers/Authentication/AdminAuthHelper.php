<?php

namespace Niyotail\Helpers\Authentication;

use Illuminate\Support\Facades\Auth;
use Niyotail\Contracts\Authentication\AdminAuthContract;
use Niyotail\Models\Employee;

/**
 * Class AdminAuthHelper
 *
 * @package Niyotail\Helpers\Authentication
 * @author  Ankit <ankitjain0269@gmail.com>
 */
class AdminAuthHelper
{
    /**
     * @var mixed
     */
    private $auth;

    /**
     * AdminAuthHelper constructor.
     */
    public function __construct()
    {
        $this->auth = Auth::guard('admin');
    }

    /**
     * @param                                                        $request
     * @param \Niyotail\Contracts\Authentication\AdminAuthContract $listener
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function execute($request, AdminAuthContract $listener)
    {
        return $this->attemptLogin($request, $listener);
    }

    /**
     * @param                                                        $request
     * @param \Niyotail\Contracts\Authentication\AdminAuthContract $listener
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    private function attemptLogin($request, AdminAuthContract $listener)
    {
        $employee = Employee::where(['email' => $request->email])->first();
        if (!empty($employee)) {
            if (!$employee->status) {
                return $listener->userIsBlocked();
            }
            if ($this->auth->attempt(['email' => $request->email, 'password' => $request->password])) {
                return $listener->userHasLoggedIn($this->auth->User());
            }
        }
        return $listener->userLogInFailed('Invalid credentials');
    }

    /**
     * @param                                                        $request
     * @param \Niyotail\Contracts\Authentication\AdminAuthContract $listener
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function logout($request, AdminAuthContract $listener)
    {
        $this->auth->logout();
        return $listener->userHasLoggedOut();
    }
}