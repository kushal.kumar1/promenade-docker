<?php

namespace Niyotail\Helpers\Authentication;


use Illuminate\Support\Facades\Auth;
use Niyotail\Contracts\Authentication\UserAuthContract;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\User;
use Niyotail\Services\UserService;

class UserAuthHelper
{
    public function sendOtp($request, UserAuthContract $listener)
    {
        try {
            $userService = new UserService();
            $user = $userService->firstOrCreate($request->mobile);
            $userService->sendOtp($user);
        } catch (ServiceException $exception) {
            return $listener->otpFailed($exception->getMessage());
        }
        return $listener->otpSuccessfullySent();
    }

    public function execute($request, UserAuthContract $listener)
    {
        $user = User::where(['mobile' => $request->mobile])->first();
        if (!empty($user)) {
            if (!$user->status)
                return $listener->userIsBlocked();
            if ($token = Auth::guard('api')->attempt(['mobile' => $request->mobile, 'otp' => $request->otp])) {
                try {
                    $userService = new UserService();
                    $userService->verifyMobile($user);
                    $userService->resetOtpDetails($user);
                } catch (ServiceException $exception) {
                    return $listener->userLogInFailed($exception->getMessage());
                }
                return $listener->userLoggedIn($user, $token);
            } else {
                return $listener->userLoginFailed('Invalid OTP !!');
            }
        }
        return $listener->userLoginFailed('Mobile is not registered !!');
    }
}