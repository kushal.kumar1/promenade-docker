<?php

namespace Niyotail\Helpers\Authentication;

use Carbon\Carbon;
use Niyotail\Contracts\Authentication\WebAuthContract;
use Niyotail\Models\User;
use Niyotail\Models\CustomerPasswordReset;
use Niyotail\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Contracts\Factory as Socialite;

class WebAuthHelper
{
    private $auth;

    public function __construct()
    {
        $this->auth = Auth::guard('web');
    }

    public function execute($request, WebAuthContract $listener)
    {
        $customer = User::where(['email' => $request->email])->first();
        if (!empty($customer)) {
            if (!$customer->status) {
                return $listener->userIsBlocked();
            }
            if ($this->auth->validate(['email' => $request->email, 'password' => $request->password])) {
                return $listener->userCanLogIn($customer);
            }
        }
        return $listener->userLogInFailed('Invalid credentials');
    }

    public function resetPassword(User $customer, WebAuthContract $listener, $request)
    {
            if ($this->auth->validateCredentials($customer,['password' => $request->old_password])) {
                $customerService = UserService::newInstance();
                $customerService->resetPassword($customer,$request);
                if ($customerService->hasError())
                    return $listener->userPasswordResetFailed($customerService->getError());
                return $listener->userPasswordResetDone();
            }
        return $listener->userPasswordResetFailed('Invalid Password');
    }

    public function resetPasswordUsingToken(WebAuthContract $listener,$request)
    {
        $resetPassword=CustomerPasswordReset::where('token',$request->token)->with('customer')
            ->where('expires_on','>',Carbon::now()->toDateTimeString())->first();
        if(!empty($resetPassword)) {
            $customerService = UserService::newInstance();
            $customerService->resetPassword($resetPassword->customer, $request);
            if ($customerService->hasError())
                return $listener->userPasswordResetFailed($customerService->getError());
            $resetPassword->delete();
            return $listener->userPasswordResetDone();
        }
        return $listener->userPasswordResetFailed('Invalid Token');

    }

    public function logout($request, WebAuthContract $listener)
    {
        $this->auth->logout();
        return $listener->userHasLoggedOut();
    }
}