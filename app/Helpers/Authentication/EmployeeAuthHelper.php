<?php

namespace Niyotail\Helpers\Authentication;

use Illuminate\Support\Facades\Auth;
use Niyotail\Contracts\Authentication\EmployeeAuthContract;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Employee;
use Niyotail\Services\EmployeeService;

class EmployeeAuthHelper
{
    public function sendOtp($request, EmployeeAuthContract $listener)
    {
        try {
            $employee = Employee::where(['mobile' => $request->mobile])->first();
            if(empty($employee)){
                return $listener->otpFailed('Invalid phone number!');
            }
            $employeeService = new EmployeeService();
            $employeeService->sendOtp($employee);
        } catch (ServiceException $exception) {
            return $listener->otpFailed($exception->getMessage());
        }
        return $listener->otpSuccessfullySent();
    }

    public function execute($request, EmployeeAuthContract $listener)
    {
        $employee = Employee::where(['mobile' => $request->mobile])->first();
        if (!empty($employee)) {
            if (!$employee->status)
                return $listener->employeeIsBlocked();
            if ($token = Auth::guard('employee_api')->attempt(['mobile' => $request->mobile, 'otp' => $request->otp])) {
                try {
                    $employeeService = new EmployeeService();
                    $employeeService->resetOtpDetails($employee);
                } catch (ServiceException $exception) {
                    return $listener->employeeLogInFailed($exception->getMessage());
                }
                return $listener->employeeLoggedIn($employee, $token);
            } else {
                return $listener->employeeLoginFailed('Invalid OTP !!');
            }
        }
        return $listener->employeeLoginFailed('Mobile is not registered !!');
    }

    /**
     * @param $request
     * @param \Niyotail\Contracts\Authentication\EmployeeAuthContract $listener
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function logout($request, EmployeeAuthContract $listener)
    {
        auth()->logout();
        $request->session()->forget('selected-warehouse');
        return $listener->userHasLoggedOut();
    }
}