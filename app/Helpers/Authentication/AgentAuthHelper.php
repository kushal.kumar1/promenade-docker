<?php

namespace Niyotail\Helpers\Authentication;


use Illuminate\Support\Facades\Auth;
use Niyotail\Contracts\Authentication\AgentAuthContract;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Agent;
use Niyotail\Services\AgentService;

class AgentAuthHelper
{
    public function sendOtp($request, AgentAuthContract $listener)
    {
        try {
            $agent=Agent::where(['mobile' => $request->mobile])->first();
            $agentService=new AgentService();
            $agentService->sendOtp($agent);
        } catch (ServiceException $exception) {
            return $listener->otpFailed($exception->getMessage());
        }
        return $listener->otpSuccessfullySent();
    }

    public function execute($request, AgentAuthContract $listener)
    {
        $agent = Agent::where(['mobile' => $request->mobile])->first();
        if (!empty($agent)) {
            if (!$agent->status)
                return $listener->agentIsBlocked();
            if ($token = Auth::guard('agent_api')->attempt(['mobile' => $request->mobile, 'otp' => $request->otp])) {
                try {
                    $agentService = new AgentService();
                    $agentService->verifyMobile($agent);
                    $agentService->resetOtpDetails($agent);
                } catch (ServiceException $exception) {
                    return $listener->agentLogInFailed($exception->getMessage());
                }
                return $listener->agentLoggedIn($agent, $token);
            } else {
                return $listener->agentLoginFailed('Invalid OTP !!');
            }
        }
        return $listener->agentLoginFailed('Mobile is not registered !!');
    }
}