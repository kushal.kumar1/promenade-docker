<?php

namespace Niyotail\Helpers\Filters;
use Solarium\QueryType\Select\Query\Query;
use Niyotail\Http\Requests\Api\V1\Product\ListingRequest;

class QueryBuilder
{
    private static $variants = ['case', 'unit', 'outer', 'moq50'];

    public static function setQuery(Query $query, $request): Query {
        // Default Queries
        $queries[] = '(table_type:product)';
        $queries[] = '(product_status:true)';
        $queries[] = '(product_published_at:[* TO NOW])';
        $queries[] = '(product_total_inventory:[0.1 TO *])';
        //$queries[] = '-(product_warehouse_ids:2)';

        foreach (self::$variants as $variant) {
            $variants_queries[] = 'product_'.$variant.'_value:*';
        }
        $queries[] = '('.implode(' OR ', $variants_queries).')';

        if($request->filled('new')) {
            $queries[] = '(product_tag_ids:3 OR product_published_at:[NOW-7DAY/DAY TO NOW])';
        }
        if($request->filled('jit')) {
            $queries[] = '(allow_back_orders:1)';
        }
        if($request->filled('ufml')) {
            $queries[] = '(product_tag_ids:1)';
        }
        if($request->filled('collections') && !empty($request->collections)) {
            $queries[] = self::collectionsQuery($request->collections);
        }
        if($request->filled('brandIds') && !empty($request->brandIds)) {
            $queries[] = self::brandsQuery($request->brandIds);
        }
        if($request->filled('margin')) {
            $queries[] = self::variantMarginQuery($request->margin);
        }

        if($request->filled('warehouse_id') && is_array($request->warehouse_id)) {
            $queries[] = self::storeBeatWarehouseQuery($request);
        }
        if(!$request->has('warehouse_id')) {
            $queries[] = '(product_warehouse_ids:1 OR product_warehouse_ids:[3 TO *])';
        }
        if($request->filled('forAgentApi')) {
            // $queries[] = '-(product_tag_ids:1 OR product_tag_ids:2)';
        }

        return $query->setQuery(implode(' AND ', $queries));
    }

    private static function brandsQuery(array $brandIds): string {
        $brandIdsWithKey = array_map(function ($brandId) {
            return 'brand_id:'.$brandId;
        }, $brandIds);
        $BrandParentIdsWithKey = array_map(function ($brandId) {
            return 'brand_parent_id:'.$brandId;
        }, $brandIds);
        return '('.implode(' OR ', array_merge($brandIdsWithKey, $BrandParentIdsWithKey)).')';
    }

    private static function collectionsQuery(array $collectionIds): string {
        $collectionIdsWithKey = array_map(function ($collectionId) {
            return 'collection_ids:'.$collectionId;
        }, $collectionIds);
        return '('.implode(' OR ', $collectionIdsWithKey).')';
    }

    private static function variantMarginQuery(string $margin): string {
        $margin = explode('-', $margin);
        abort_if(count($margin) !== 2, 400, 'incorrect margin format');
        foreach (self::$variants as $variant) {
            $variant_queries[] = 'product_'.$variant.'_margin:['.$margin[0].' TO '.$margin[1].']';
        }
        return '('.implode(' OR ', $variant_queries ?? []).')';
    }

    private static function storeBeatWarehouseQuery($request): string {
        foreach ($request->warehouse_id as $warehouse_id) {
            $wh_queries[] = 'product_warehouse_ids:'.$warehouse_id;
        }
        return '('.implode(' OR ', $wh_queries ?? []).')';
    }
}
