<?php


namespace Niyotail\Helpers\Filters;


use Niyotail\Models\RateCreditNote;

class TransactionFilterHelper
{
    public function getFilterQuery($types, $query)
    {
        if (count($types) == 1) {
            $type = $types[0];
            if (in_array($type, ['rcn_promotion', 'rcn_discount', 'rcn_commission'])) {
                $query->whereHasMorph('source',[RateCreditNote::class], function ($q) use($type) {
                    $rcnType = $this->getRCNType($type);
                    $q->where('type', $rcnType);
                });
            } else {
                $query->whereRaw($this->getQuery($type));
            }
        } else {
            $query = $query->where(function($q) use($types) {
                foreach($types as $type) {
                    if (in_array($type, ['rcn_promotion', 'rcn_discount', 'rcn_commission'])) {
                        $q->orWhereHasMorph('source', [RateCreditNote::class], function ($q) use($type) {
                            $rcnType = $this->getRCNType($type);
                            $q->where('type', $rcnType);
                        });
                    } else {
                        $q->orWhereRaw($this->getQuery($type));
                    }
                }
            });
        }
        return $query;
    }

    private function getQuery($type) {
        $rawQuery = '';
        switch($type) {
            case 'return':
                $rawQuery = " source_type='credit_note'";
                break;
            case 'purchase':
                $rawQuery = " source_type='invoice'";
                break;
            case '1k_rent_reimbursement':
            case 'store_internet_reimbursement':
            case 'customer_loyalty_reimbursement':
            case 'upi':
            case 'cash':
            case 'cheque':
            case 'net_banking':
                $rawQuery = " payment_method='".$type."'";
                break;
            case 'others':
                $rawQuery = " source_type is null and payment_method not in ('1k_rent_reimbursement', 'store_internet_reimbursement', 'customer_loyalty_reimbursement', 'upi', 'cheque', 'net_banking', 'cash')";
        }
        return $rawQuery;
    }

    private function getRCNType($type): string
    {
        switch($type) {
            case 'rcn_promotion':
                return RateCreditNote::TYPE_PROMOTION;
            case 'rcn_discount':
                return RateCreditNote::TYPE_DISCOUNT;
            case 'rcn_commission':
                return RateCreditNote::TYPE_COMMISSION;
        }
        return '';
    }
}