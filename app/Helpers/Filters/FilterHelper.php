<?php

namespace Niyotail\Helpers\Filters;

use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Search\Solr;
use Niyotail\Http\Requests\Api\V1\Product\ListingRequest;
use Solarium\QueryType\Select\Query\Query;
use Niyotail\Helpers\Filters\FacetParsers\BrandFacetParser;
use Niyotail\Helpers\Filters\FacetParsers\CollectionFacetParser;

class FilterHelper
{
    private $solr;

    public function __construct()
    {
        $this->solr = App::make(Solr::class);
    }

    public function getFilteredProducts($request, int $rows=15): array {
        $rows = $request->per_page ?? $rows;
        $client = $this->solr->getClient();

        $query = $client->createSelect();

        $query = $query->setRows($rows);

        if(isset($request->page) && $request->page > 1)
            $query->setStart(($request->page-1)*$rows);

        // ADD FACETS
        $query = $this->addFacetsToQuery($query);

        // FILTERING
        $query = QueryBuilder::setQuery($query, $request);

        // SORTING
        $query = $this->applySortingQuery($query, $request);

        // FETCH RESULT
        $result = $client->select($query);

        foreach ($result->getDocuments() as $product) {
            $productIds[] = (int) $product->product_id;
        }

        if(empty($request->filters_for) && empty($request->filters_for_id)) {
            $filters = [
                'brands' => BrandFacetParser::parse($result, $request),
                'collections' => CollectionFacetParser::parse($result, $request)
            ];
        }

        // TODO: To be replaced by single query later
        if(($request->has('filters_for') && $request->has('filters_for_id')) || $request->has('ufml') || $request->has('new') || $request->has('margin') || $request->has('group_buy') || $request->has('jit')) {
            if($request->filters_for === 'brands') {
                $request->merge(['brandIds' => [$request->filters_for_id], 'brands' => [$request->filters_for_id], 'collectionsUnused' => $request->collections, 'collections' => [], 'brandsUnused' => $request->brands, 'brandIdsUnused' => $request->brandIds]);
            }
            elseif($request->filters_for === 'collections') {
                $request->merge(['collections' => [$request->filters_for_id], 'brandIdsUnused' => $request->brandIds, 'brandsUnused' => $request->brands, 'brandIds' => [], 'brands' => [], 'collectionsUnused' => $request->collections]);
            }
            if(!empty($request->new) || !empty($request->ufml) || !empty($request->margin) || !empty($request->group_buy)) {
                $request->merge(['brandIds' => [], 'brands' => [], 'collections' => [], 'collectionsUnused' => $request->collections, 'brandsUnused' => $request->brands, 'brandIdsUnused' => $request->brandIds]);
            }
            $queryForFilters = QueryBuilder::setQuery($query, $request);
            $queryForFilters = $queryForFilters->setRows(0);
            $resultForFilters = $client->select($queryForFilters);
            $filters = [
                'brands' => BrandFacetParser::parse($resultForFilters, $request),
                'collections' => CollectionFacetParser::parse($resultForFilters, $request)
            ];
        }

        return [
            'count' => (int) $result->count(),
            'rows' => (int) $rows > $result->count() ? $result->count() : $rows,
            'currentPage' => (int) $request->page ?? 1,
            'numFound' => (int) $result->getNumFound(),
            'lastPage' => (int) number_format($result->getNumFound()/$rows),
            'productIds' => $productIds ?? [],
            //'products' => $result->getDocuments(),
            'filters' => $filters
        ];
    }

    private function addFacetsToQuery(Query $query): Query {
        $query->getFacetSet()->createFacetPivot('collections')->addFields('collection_facets')->setMinCount(1);
        $query->getFacetSet()->createFacetPivot('brands')->addFields('brand_parent_name,brand_parent_id,brand_parent_logo,brand_name,brand_id,brand_logo')->setMinCount(1);

        return $query;
    }

    private function applySortingQuery(Query $query, $request): Query {
        if($request->filled('new')) {
            $query = $query->addSort('product_published_at', $query::SORT_DESC);
        }

        $query = $query->addSort('product_total_inventory', $query::SORT_DESC);
        if($request->filled('margin')) {
            $query = $query->addSort('product_case_margin', $query::SORT_DESC);
            $query = $query->addSort('product_unit_margin', $query::SORT_DESC);
            $query = $query->addSort('product_outer_margin', $query::SORT_DESC);
        }

        if($request->filled('forAgentApi')) {
            $query = $query->addSort('product_focussed_count', $query::SORT_DESC);
        }
        if (!$request->filled('new')) {
              $query = $query->addSort('product_published_at', $query::SORT_DESC);
        }
        return $query;
    }
}
