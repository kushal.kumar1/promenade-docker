<?php

namespace Niyotail\Helpers\Filters\FacetParsers;

use Niyotail\Helpers\Image;
use Niyotail\Http\Requests\Api\V1\Product\ListingRequest;
use Niyotail\Models\Brand;
use Solarium\QueryType\Select\Result\Result;

class BrandFacetParser {

    public static function parse(Result $result, $request): array {
        $facet = $result->getFacetSet()->getFacet('brands');

        $brands = [];
        $auto_selected_parent_brands = [];

        foreach ($facet as $key => $brand) {
            $subBrandsArray = [];
            $brandId = $brand->getPivot()[0]->getValue();

            if(isset($brand->getPivot()[0]->getPivot()[0])) {
                $subBrands = $brand->getPivot()[0]->getPivot()[0]->getPivot();

                foreach ($subBrands as $subBrand) {
                    $subBrandId = $subBrand->getPivot()[0]->getValue();

                    $subBrandSelected = (!empty($request->brands) && in_array($subBrandId, $request->brands)) || (!empty($request->brandsUnused) && in_array($subBrandId, $request->brandsUnused));

                    if($subBrandSelected)
                        $auto_selected_parent_brands[] = $brandId;

                    $subBrandsArray[] = self::getBrandResource($subBrand, $subBrandSelected);
                }

                usort($subBrandsArray, function ($a, $b) {
                    return $a['active'] < $b['active'];
                });
            }

            $brandSelected = (!empty($request->brands) && (in_array($brandId, array_merge($request->brands, $auto_selected_parent_brands)))) || (!empty($request->brandsUnused) && (in_array($brandId, array_merge($request->brandsUnused, $auto_selected_parent_brands)))) || (!empty($request->brandIdsUnused) && (in_array($brandId, array_merge($request->brandIdsUnused, $auto_selected_parent_brands))));

            if($brandSelected && count($subBrandsArray) === 1)
                $subBrandsArray[0]['active'] = true;

            $brands[] = self::getBrandResource($brand, $brandSelected, $subBrandsArray);
        }

        usort($brands, function($a, $b) {
            return $a['active'] < $b['active'];
        });

        return $brands;
    }

    private static function getBrandLogo(int $brand_id, string $brand_logo): string {
        $brand = new Brand();
        $brand->id = $brand_id;
        $brand->logo = $brand_logo;
        return Image::getSrc($brand, null, 100);
    }

    private static function getBrandResource($brand, $selected, $subBrands=null): array {
        $resource = [
            'id' => $brand->getPivot()[0]->getValue(),
            'name' => $brand->getValue(),
            'active' => $selected,
            'logo' => isset($brand->getPivot()[0]->getPivot()[0]) ? self::getBrandLogo($brand->getPivot()[0]->getValue(), $brand->getPivot()[0]->getPivot()[0]->getValue()) : null,
            'count' => $brand->getCount()
        ];
        if(is_null($subBrands))
            return $resource;
        return array_merge($resource, ['sub_brands' => $subBrands]);
    }
}