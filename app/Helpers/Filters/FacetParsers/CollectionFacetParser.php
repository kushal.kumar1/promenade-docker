<?php

namespace Niyotail\Helpers\Filters\FacetParsers;

use Niyotail\Http\Requests\Api\V1\Product\ListingRequest;
use Solarium\QueryType\Select\Result\Result;

class CollectionFacetParser {

    public static function parse(Result $result, $request): array {
        $facet = $result->getFacetSet()->getFacet('collections');

        $collections = [];

        foreach ($facet as $pivot) {
            if(!empty($request->filters_for) && $request->filters_for === 'collections')
                return [];

            $collectionParts = explode('#', $pivot->getValue());

            $selected = ($request->filled('collections') && in_array($collectionParts[0], $request->collections)) || ($request->filled('collectionsUnused') && in_array($collectionParts[0], $request->collectionsUnused));

            $collections[] = [
                'id' => (int) $collectionParts[0],
                'name' => $collectionParts[1],
                'active' => $selected,
                'slug' => str_slug($collectionParts[1]),
                'icon' => null,
                'count' => $pivot->getCount(),
            ];
        }

        usort($collections, function($a, $b) {
            return $a['active'] < $b['active'];
        });

        return $collections;
    }
}