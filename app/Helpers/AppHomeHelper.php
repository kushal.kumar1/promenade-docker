<?php

namespace Niyotail\Helpers;

use Niyotail\Models\AppBanner;
use Niyotail\Models\AppSection;
use Niyotail\Models\AppSectionItem;
use Niyotail\Models\Collection;
use Niyotail\Models\Brand;
use Niyotail\Models\Product;
use Niyotail\Models\Store;
use Niyotail\Models\Warehouse;
use Niyotail\Helpers\RuleEngine;
use Niyotail\Models\Expression;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppHomeHelper
{
    public function getAppSections($request, $medium)
    {
        $appSections = AppSection::active()
                        ->with(['sectionItems' => function ($q) {
                            $q->active()->orderBy('sort');
                        }])
                        ->where('medium', $medium)
                        ->orderBy('sort')->paginate(2);

        $collection = $appSections->getCollection();

        $collection->transform(function ($section) use($request) {
            $section->items = $this->getItems($section, $request);
            return $section;
        });

        $appSections->setCollection($collection);
        return $appSections;
    }

    private function getItems($section, $request)
    {
        $itemCollection = collect([]);
        $source = $section->source;
        $layout = $section->layout;
        $sectionItems = $section->sectionItems;
        $itemCount = !empty($section->item_count) ? $section->item_count : 10;
        $code = $section->code;

        if($source == 'banner') {
            if ($layout == 'slider') {
                $itemCollection = $this->getSliderBannerData();
            } else {
                $itemCollection = $sectionItems;
            }
        }

        if($source == 'image') {
            $itemCollection = $sectionItems;
        }

        if($source == 'products') {
            $itemCollection = $this->getProducts($sectionItems, $itemCount, $code, $request);
        }

        if($source == 'categories') {
            $itemCollection = $this->getCategories($itemCount);
        }

        if($source == 'brands') {
            $itemCollection = $this->getBrands($itemCount, $code);
        }

        if($source == 'generic') {
            $itemCollection = $sectionItems;
        }

        return $itemCollection;
    }

    private function getSliderBannerData()
    {
        $appBanners = AppBanner::active()
                      ->where(function ($q) {
                          $q->where('valid_from', '<=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
                            ->where('valid_to', '>=', Carbon::now()->setTimezone('Asia/Kolkata')->toDateTimeString())
                            ->orWhere('valid_from', null);
                      })
                      ->orderBy('position')
                      ->get();
        return $appBanners;
    }

    private function getProducts($sectionItems, $itemCount, $code, $request)
    {
        $store = Store::find($request->store_id);
//        $beatId = $store->beat_id;
//
//        $warehouseIds = Utils::getAvailableWarehouseForStore($store);
//
//        $params['warehouse_id'] = $warehouseIds;

        if (!empty($code)) {
            $params[$code] = 1;
            $products = $this->getProductsForParams($params, $itemCount);
        } else {
            $products = $sectionItems->take($itemCount);
        }

//        $products->map(function ($product) use ($beatId) {
//            $product['beatId'] = $beatId;
//            return $product;
//        });

//        RuleEngine::processProducts($products, $store, Expression::MEDIUM_RETAILER);
//        RuleEngine::processProductsForRules($products, $store, Expression::MEDIUM_RETAILER);

        return $products;
    }

    private function getProductsForParams($params, $itemCount)
    {
        $query = Product::published()
            ->with('brand', 'images', 'variants')->has('variants')
            ->where('products.status', 1)
            ->selectRaw('products.*, sum(quantity) as total_inventory')
            ->join('inventories', 'inventories.product_id', '=', 'products.id')
            ->where('inventories.quantity', '>', 0)
            ->where('inventories.status', 1)
            ->groupBy('product_id');

        if (!empty($params['warehouse_id'])) {
            $query = $query->whereIn('inventories.warehouse_id', $params['warehouse_id']);
        } else {
            $query = $query->whereNotIn('inventories.warehouse_id', ['2']);
        }
        if (!empty($params['new'])) {
            $query->whereHas('tags', function ($query) {
                $query->where('tags.name', 'New');
            });
            $query->orWhereDoesntHave('tags', function ($query) {
                $query->whereRaw('published_at >= NOW() - INTERVAL 1 WEEK');
            });
            $query->whereHas('variants');
            $query->orderBy('published_at', 'desc');
        }

        // if (!empty($params['focus'])) {
        //     $query->whereHas('focussed');
        // }

        if (!empty($params['ufml'])) {
            $query->whereHas('tags', function ($query) {
                $query->where('tags.id', 1);
            });
        }

        // if (!empty($params['offer'])) {
        //     $query->whereHas('focussed');
        // }

        if (!empty($params['jit'])) {
            $query->where('allow_back_orders', 1);
        }

        if (empty($params['focus'])) {
            $query = $query->orderBy('total_inventory', 'desc');
        }
        $result = $query->limit($itemCount)->get();

        return $result;
    }

    private function getCategories($itemCount)
    {
        $categories = Collection::getAllVisibleRoots();
        $categories = $categories->take($itemCount);
        return $categories;
    }

    private function getBrands($itemCount, $code)
    {
        if (empty($code)) {
          $brands = Brand::getAllVisible();
          $brands = $brands->take($itemCount);

        } else {
          $brands = Brand::active()->with('marketer')->where('is_visible', 1)->groupBy('marketer_id')->orderBy('id')->get();
          $brands = $brands->take($itemCount);
          $brands->map(function ($brand) {
              $brand['name'] = $brand->marketer->name;
              return $brand;
          });
        }

        return $brands;
    }
}
