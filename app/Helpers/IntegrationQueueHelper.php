<?php

namespace Niyotail\Helpers;

use Dusterio\PlainSqs\Jobs\DispatcherJob;
use Illuminate\Foundation\Bus\Dispatchable;

class IntegrationQueueHelper extends DispatcherJob
{
    use Dispatchable;

    public $connection = 'integration';

    public function __construct($data)
    {
        parent::__construct($data);
        $this->setPlain();
    }
}