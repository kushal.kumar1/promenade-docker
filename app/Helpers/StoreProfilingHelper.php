<?php

namespace Niyotail\Helpers;

use Niyotail\Models\Cart;

class StoreProfilingHelper
{
    public static function validate($type, $answers)
    {
        switch($type) {
            case 'dropdown' :
                if (!is_array($answers) || count($answers) != 1) {
                    throw new \Exception("Only one answer allowed.");
                }
                break;
            case 'checkbox':
                if (!is_array($answers) || count($answers) == 0) {
                    throw new \Exception("At least one answer is must.");
                }
                break;
            case 'textedit':
                if (strlen($answers) < 1) {
                    throw new \Exception("Answer must be more than 1 character.");
                }
                break;
            case 'numberedit':
                if (strlen($answers) < 1 || !is_numeric($answers)) {
                    throw new \Exception("Answer must be more than 1 character and only numeric.");
                }
                break;
        }
    }
}
