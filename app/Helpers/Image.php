<?php

namespace Niyotail\Helpers;

use Niyotail\Models\Brand;
use Niyotail\Models\Product;
use Niyotail\Models\Collection;
use Niyotail\Models\ProductImage;
use Niyotail\Models\AppBanner;
use Niyotail\Models\DashboardImage;
use Niyotail\Models\AppSectionItem;
use Niyotail\Models\Shipment;
use Niyotail\Models\Transaction;

class Image
{
    public static function getProductImageSrc($path, $width, $height)
    {
        if ($width || $height) {
            return config('app.product_image_url')."/media/".$width."X".$height."/".$path;
        }
        return config('app.product_image_url')."/media/".$path;
    }

    public static function getImageSrc($path, $width, $height)
    {
        if ($width || $height) {
            return config('app.image_url')."/media/cache/".$width."X".$height."/".$path;
        }
        return config('app.image_url')."/media/".$path;
    }

    public static function getSrc($object, $width = null, $height = null)
    {
        $path = "";
        if ($object instanceof ProductImage) {
            $path="$object->product_id/$object->name";
        }
        if ($object instanceof Collection) {
            $path="collection/$object->id/$object->icon";
        }
        if ($object instanceof Brand && !empty($object->logo)) {
            $path="brand/$object->id/$object->logo";
        }
        if($object instanceof AppBanner){
            $path="app_banner/$object->id/$object->file";
        }
        if($object instanceof DashboardImage){
            $path="dashboard_image/$object->id/$object->image";
        }
        if($object instanceof AppSectionItem){
            $path="app_section/".$object->section->id."/$object->image";
        }
        if($object instanceof Transaction){
            $path="transactions/".$object->id."/$object->image";
        }
        if($object instanceof Shipment){
            $path="shipments/".$object->id."/$object->pod_name";
        }
        if(empty($object)){
            $path = "default.png";
        }
        if (!empty($object) && $object instanceof ProductImage) {
          return self::getProductImageSrc($path, $width, $height);
        }
        return self::getImageSrc($path, $width, $height);
    }
}
