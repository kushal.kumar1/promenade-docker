<?php

namespace Niyotail\Helpers\Payments;


class PaymentResponse
{
    private $reason = '';

    private $order=null;

    private $redirection=null;


    public function getReason()
    {
        return $this->reason;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
        return $this;
    }

    public function hasRedirection()
    {
        return !empty($this->getRedirection());
    }

    public function getRedirection()
    {
        return $this->redirection;
    }

    public function setRedirection($redirection)
    {
        $this->redirection = $redirection;
        return $this;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
        return $this;

    }
}