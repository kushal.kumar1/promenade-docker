<?php

namespace Niyotail\Helpers\Payments;

use Niyotail\Helpers\Payments\Contracts\PaymentMethodContract;
use Niyotail\Models\Order;
use Niyotail\Models\Payment;

class Card implements PaymentMethodContract
{
    use PaymentHelper;

    protected $name = Payment::METHOD_CARD;

    public function processPayment(Order $order, $amount, $processOrder = false)
    {
        $orderPayment = $this->createOrderPayment($order, $amount, Payment::TYPE_CREDIT,Payment::STATUS_PAID);
        if ($processOrder) {
            return $this->orderPaymentSuccessful($order);
        }
        return $orderPayment;
    }

    public function refund($order,$amount,$reason)
    {
        $this->createOrderPayment($order, $amount, Payment::TYPE_DEBIT,Payment::STATUS_PAID,$reason);
    }
}