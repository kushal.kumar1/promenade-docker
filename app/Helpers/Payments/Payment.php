<?php

namespace Niyotail\Helpers\Payments;

use Illuminate\Support\Str;
use Niyotail\Helpers\Payments\Contracts\PaymentMethodContract;
use Niyotail\Helpers\Payments\PaymentGateways\Payu\Payu;
use InvalidArgumentException;

class Payment
{
    private $paymentRequest;

    public function __construct(PaymentRequest $paymentRequest=null)
    {
        $this->paymentRequest = $paymentRequest;
    }

    /**
     * @param null $name
     * @return PaymentMethodContract
     */
    public function driver($name = null)
    {
        return $this->resolve($name);
    }

    private function resolve($name)
    {
        $driverMethod = 'create' . ucfirst(Str::camel($name)) . 'Driver';
        $config = config('payments.' . $name);
        if (method_exists($this, $driverMethod)) {
            return $this->{$driverMethod}($config);
        } else {
            throw new InvalidArgumentException("Driver [{$name}] is not supported.");
        }
    }

    protected function createPayuDriver($config)
    {
        return new Payu($config['merchant_key'], $config['salt'], $config['url'], $config['info_url']);
    }

    protected function createCashDriver($config)
    {
        return new Cash();
    }


    protected function createCardDriver($config)
    {
        return new Card();
    }

    protected function createNeftDriver($config)
    {
        return new Neft();
    }

    protected function createChequeDriver($config)
    {
        return new Cheque();
    }

    protected function createOnlinePaymentDriver($config)
    {
        return new OnlinePayment();
    }

    protected function createStoreCreditDriver($config)
    {
        return new StoreCredit();
    }

    public function __call($name, $parameter)
    {
        if(empty($this->paymentRequest))
            throw new PaymentException('Set Payment Request first');
        $paymentMethods=$this->paymentRequest->getPayments();
        $methods = array_keys($paymentMethods);
        $lastPaymentMethod = end($methods);
        foreach ($paymentMethods as $paymentMethod => $amount) {
            $driver = $this->resolve($paymentMethod);
            if ($paymentMethod === $lastPaymentMethod) {
                $response = call_user_func_array([$driver, $name], [$this->paymentRequest->getOrder(), $amount, true]);
            } else {
                $response = call_user_func_array([$driver, $name], [$this->paymentRequest->getOrder(), $amount]);
            }
        }
        return $response;
    }

}