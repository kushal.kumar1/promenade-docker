<?php

namespace Niyotail\Helpers\Payments\PaymentGateways\Payu;

use Niyotail\Helpers\Payments\Contracts\PaymentMethodContract;
use Niyotail\Helpers\Payments\PaymentException;
use Niyotail\Helpers\Payments\PaymentGateways\BasePaymentGateway;
use Niyotail\Helpers\Payments\PaymentHelper;
use Niyotail\Helpers\Payments\PaymentResponse;
use Niyotail\Models\Order;
use Niyotail\Models\Payment;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class Payu extends BasePaymentGateway implements PaymentMethodContract
{
    use PaymentHelper;

    private $merchantKey;

    private $salt;

    private $url;

    private $fields = [];

    protected $name=Payment::METHOD_PAYU;

    private $infoUrl;

    public function __construct($merchantKey, $salt, $url, $infoUrl)
    {
        $this->merchantKey = $merchantKey;
        $this->salt = $salt;
        $this->url = $url;
        $this->infoUrl = $infoUrl;
    }

    public function processPayment(Order $order, $amount, $processOrder = false)
    {
        $this->setDefaultPaymentFields();
        $this->setUserFields($order->id, $amount);
        $payment = $this->createOrderPayment($order, $amount, Payment::TYPE_CREDIT, Payment::STATUS_PENDING);
        $this->setField('txnid', $payment->transaction_id);
        $this->setField('hash', $this->generateHashForPayment($this->fields));
        $this->createLog($payment, $this->fields, 'request');
        $redirection= view('payment.payu.request', ['data' => $this->fields, 'url' => $this->url.'/_payment']);
        return (new PaymentResponse())->setOrder($order)->setRedirection($redirection);
    }


    public function verifyPayment(Request $request)
    {
        $payment=Payment::where('transaction_id', $request->txnid)->with('order')->first();
        if (!empty($payment)) {
            $this->createLog($payment, $request->all(), 'response');
            $requestHashSequence = $this->salt . '|' . $request->status . '|||||||||||' . $request->email . '|' . $request->firstname . '|' . $request->productinfo . '|' . $request->amount . '|' . $request->txnid . '|' . $request->key;
            if ($request->has('additionalCharges')) {
                $requestHashSequence= $request->additionalCharges.'|'.$requestHashSequence;
            }
            $hash = hash("sha512", $requestHashSequence);
            if ($hash == $request->hash) {
                if ($request->status == 'success') {
                    $this->updatePaymentStatus($payment, Payment::STATUS_PAID);
                    return $this->orderPaymentSuccessful($payment->order);
                }
                $this->updatePaymentStatus($payment, Payment::STATUS_FAILED);
                $this->orderPaymentFailed($payment->order);
            }
            throw new PaymentException('Inavlid Hash : unauthorised access');
        }
        throw new PaymentException('Invalid Transaction Id');
    }


    public function generateChecksum(Request $request)
    {
        $fields=['key'=>$this->merchantKey,'salt'=>$this->salt];
        $fields=array_merge($fields, $request->only(['txnid','amount','productinfo','firstname','email']));
        $this->createLog($fields['txnid'], $fields, 'request');
        $hash['payment_hash']= $this->generateHashForPayment($fields);

        $detailsForMobileSdk_str1 = $this->merchantKey  . '|payment_related_details_for_mobile_sdk|default|' . $this->salt ;
        $hash['payment_related_details_for_mobile_sdk_hash'] = $detailsForMobileSdk1 = strtolower(hash('sha512', $detailsForMobileSdk_str1));

        $mobileSdk_str = $this->merchantKey . '|vas_for_mobile_sdk|default|' . $this->salt;
        $hash['vas_for_mobile_sdk_hash'] = strtolower(hash('sha512', $mobileSdk_str));

        return $hash;
    }

    public function reconcileTransactions(array $transactions)
    {
        $reconcileUrl=$this->infoUrl.'/merchant/postservice.php?form=2';
        $this->setFields(['key'=>$this->merchantKey,'command'=>'verify_payment','var1'=>implode('|', $transactions)]);
        $hash=$this->generateHash($this->fields);
        $this->setField('hash', $hash);
        $client=new Client();
        $response = $client->request('POST', $reconcileUrl, [
            'form_params' => $this->fields
        ]);
        $response= $response->getBody();
        $transactionsResponse=json_decode($response, true);
        if (!empty($transactionsResponse)) {
            foreach ($transactionsResponse['transaction_details'] as $transactionId => $transactionResponse) {
                $this->processReconciledTransaction($transactionId, $transactionResponse);
            }
        }
    }

    private function processReconciledTransaction($id, $response)
    {
        $payment = Payment::where('transaction_id', $id)->with('order')->first();
        if (!empty($payment)) {
            if (empty($response['txnid'])) {
                $this->updatePaymentStatus($payment, Payment::STATUS_FAILED);
                $this->orderPaymentFailed($payment->order);
            }

            $this->createLog($payment, $response, 'response');
            if ($response['status'] == 'success') {
                $this->updatePaymentStatus($payment, Payment::STATUS_PAID);
                return $this->orderPaymentSuccessful($payment->order);
            } elseif ($response['status'] == 'failure') {
                $this->updatePaymentStatus($payment, Payment::STATUS_FAILED);
                $this->orderPaymentFailed($payment->order);
            }
        }
    }

    private function setField($key, $value)
    {
        $this->fields[$key] = $value;
        return $this;
    }

    private function setFields(array $fields)
    {
        foreach ($fields as $key=>$value) {
            $this->setField($key, $value);
        }
        return $this;
    }

    private function setDefaultPaymentFields()
    {
        $this->setField('key', $this->merchantKey)->setField('productinfo', 'Ecomm')
            ->setField('surl', route('web::verifyPayment', ['driver' => 'payu']))
            ->setField('curl', route('web::verifyPayment', ['driver' => 'payu']));
    }

    private function setUserFields($orderId, $amount)
    {
        $order=Order::with('customer', 'billingAddress')->where('id', $orderId)->first();
        $email=$order->email;
        $mobile='9999999999';
        $firstName='dummy';
        if (!empty($order->customer)) {
            $email=!empty($order->customer->email) ? $order->customer->email : $email;
            $mobile=!empty($order->customer->mobile) ? $order->customer->mobile : $mobile;
            $firstName=str_replace(' ', '', $order->customer->first_name);
        }
        if (!empty($order->billingAddress) && $mobile == '9999999999') {
            $mobile=!empty($order->billingAddress->mobile)?$order->billingAddress->mobile:$mobile;
        }

        $this->setField('amount', $amount)->setField('email', $email)
            ->setField('firstname', $firstName)->setField('phone', str_replace('+', '', $mobile));
    }

    private function generateHashForPayment(array $fields)
    {
        $hashString=$fields['key'].'|'.$fields['txnid'].'|'.$fields['amount'].'|'.$fields['productinfo'].'|'.$fields['firstname'].'|'.$fields['email'].'|||||||||||'.$this->salt;
        return strtolower(hash('sha512', $hashString));
    }

    private function generateHash(array $fields)
    {
        $hashString=implode('|', $fields);
        $hashString.='|'.$this->salt;
        return strtolower(hash('sha512', $hashString));
    }

    public function refund($order, $amount, $reason)
    {
    }
}
