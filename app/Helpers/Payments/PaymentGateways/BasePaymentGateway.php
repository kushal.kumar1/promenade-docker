<?php

namespace Niyotail\Helpers\Payments\PaymentGateways;

use Niyotail\Models\OrderPayment;
use Niyotail\Models\Payment;
use Niyotail\Models\PaymentGatewayLog;
use Illuminate\Http\Request;

abstract class BasePaymentGateway
{
    abstract function verifyPayment(Request $request);

//    abstract function generateChecksum(Request $request);
//
//    abstract function reconcileTransactions(array $transactions);

    public function createLog(Payment $payment, $fields, $type)
    {
        if ($type == 'request') {
            $paymentGatewayLog = new PaymentGatewayLog();
            $paymentGatewayLog->payment_id = $payment->id;
            $paymentGatewayLog->request = json_encode($fields);
            $paymentGatewayLog->gateway_name = class_basename($this);
        }
        else {
            $paymentGatewayLog = PaymentGatewayLog::where('payment_id',$payment->id)->first();
            $paymentGatewayLog->response = json_encode($fields);
        }
        $paymentGatewayLog->save();
    }
}