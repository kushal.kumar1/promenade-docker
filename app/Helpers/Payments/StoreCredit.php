<?php

namespace Niyotail\Helpers\Payments;

use Niyotail\Helpers\Payments\Contracts\PaymentMethodContract;
use Niyotail\Models\Payment;
use Niyotail\Models\Order;
use Niyotail\Models\ReturnOrder;
use Niyotail\Services\StoreCreditService;
use Niyotail\Models\StoreCredit as StoreCreditModel;

class StoreCredit implements PaymentMethodContract
{
    use PaymentHelper;

    protected $name = Payment::METHOD_STORE_CREDIT;

    public function processPayment(Order $order, $amount, $processOrder = false)
    {
        $customer = $order->customer;
        if (empty($customer))
            throw new PaymentException('No customer associated for store credit');
        $storeCredit = $customer->getStoreCreditBalance();
        if ($storeCredit < $amount)
            throw new PaymentException('Insufficient store credit');

        $this->debitStoreCredit($customer->id, $order->id, $amount);
        $orderPayment = $this->createOrderPayment($order, $amount, Payment::TYPE_CREDIT,Payment::STATUS_PAID);
        if ($processOrder) {
            return $this->orderPaymentSuccessful($order);
        }
        return $orderPayment;
    }

    private function debitStoreCredit($customerId, $orderId, $amount)
    {
        $orderService = (StoreCreditService::newInstance())->setAuthority(StoreCreditService::AUTHORITY_SYSTEM);
        return $orderService->create($customerId, $orderId, $amount, \Niyotail\Models\StoreCredit::TYPE_DEBIT, 'Order Placed using credits');
    }

    public function refund($order, $amount,$reason)
    {
        $storeCreditService = (new StoreCreditService())->setAuthority(StoreCreditService::AUTHORITY_SYSTEM);
        if($order instanceof ReturnOrder){
            $storeCreditService->create($order->order->customer_id, $order->order_id, $amount, StoreCreditModel::TYPE_CREDIT, 'Order Cancelled');
        }else{
            $storeCreditService->create($order->customer_id, $order->id, $amount, StoreCreditModel::TYPE_CREDIT, 'Order Cancelled');
        }
        $this->createOrderPayment($order, $amount, Payment::TYPE_DEBIT,Payment::STATUS_PAID,$reason);
    }
}