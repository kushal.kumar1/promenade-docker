<?php

namespace Niyotail\Helpers\Payments;

use Illuminate\Support\Str;
use Niyotail\Models\Order;

class PaymentRequest
{

    private $methodOrder = [ 'cash'];
    private $cash = null;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function setMethod($name,$value)
    {
        $functionName='set'.ucfirst(Str::camel($name));
        if(method_exists($this,$functionName)){
            return $this->$functionName($value);
        }
        throw new PaymentException("Invalid method $name called");
    }

    public function setCash($cash)
    {
        $this->cash = $cash;
        return $this;
    }

    public function getPayments()
    {
        $payments = [];
        foreach ($this->methodOrder as $method) {
            $methodValue = Str::camel($method);
            if (empty($this->$methodValue))
                continue;
            $payments[$method] = $this->$methodValue;
        }
        $this->checkPaymentMethodsAmount($payments, $this->order->total);
        return $payments;
    }

    private function checkPaymentMethodsAmount($paymentMethods, $orderTotal)
    {
        $totalPaymentAmount = 0;
        if (empty($paymentMethods)) {
           throw new PaymentException('Please add at least one payment method!');
        }
        foreach ($paymentMethods as $paymentMethod => $amount) {
            if (empty($amount) || $amount < 0)
                throw new PaymentException("$paymentMethod amount is incorrect !");
            $totalPaymentAmount += $amount;
        }
        if ($orderTotal != $totalPaymentAmount)
            throw new PaymentException("Invalid amount!");
    }

    public function getOrder()
    {
        return $this->order;
    }

}