<?php

namespace Niyotail\Helpers\Payments;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\Order;
use Niyotail\Models\OrderPayment;
use Niyotail\Models\Payment;
use Niyotail\Models\ReturnOrder;
use Niyotail\Services\OldOrderService;
use Niyotail\Services\Order\OrderService;
use Niyotail\Services\PaymentService;

trait PaymentHelper{

    protected function orderPaymentSuccessful(Order $order)
    {
        try {
            $orderService = (new OrderService())->setAuthority(OrderService::AUTHORITY_SYSTEM);
            $orderService->orderPaymentSuccessful($order);
        }catch (ServiceException $exception) {
           throw new PaymentException($exception->getMessage());
        }
        return (new PaymentResponse())->setOrder($order)->setReason('Payment Successful');
    }

    protected function updatePaymentStatus(Payment $payment,$status)
    {
        try {
            $paymentService=(new PaymentService())->setAuthority(OrderService::AUTHORITY_SYSTEM);
            $paymentService->updateStatus($payment,$status);
        }catch (ServiceException $exception) {
            throw new PaymentException($exception->getMessage());
        }
    }

    protected function orderPaymentFailed(Order $order)
    {
        try {
            $orderService = (new OrderService())->setAuthority(OrderService::AUTHORITY_SYSTEM);
            $orderService->orderPaymentFailed($order);
        }catch (ServiceException $exception) {
            throw new PaymentException($exception->getMessage());
        }
        throw new PaymentException("payment Failed");

    }

    protected function createOrderPayment($order, $amount, $type,$status,$remarks=null,$transactionId=null)
    {
        $returnOrderId=null;
        if($order instanceof ReturnOrder){
            $returnOrderId=$order->id;
            $order=$order->order;
        }
        try {
            $paymentService=(new PaymentService())->setAuthority(OrderService::AUTHORITY_SYSTEM);
            return $paymentService->createOrderPayment($order,$this->name,$transactionId,$amount,$type,$status,$returnOrderId,$remarks);
        }catch (ServiceException $exception) {
            throw new PaymentException($exception->getMessage());
        }
    }

}