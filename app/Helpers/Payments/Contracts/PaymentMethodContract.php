<?php
namespace Niyotail\Helpers\Payments\Contracts;

use Niyotail\Models\Order;

interface PaymentMethodContract
{
    public function processPayment(Order $order, $amount,$processOrder=false);

    public function refund($order,$amount,$reason);
}