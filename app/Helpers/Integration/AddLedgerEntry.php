<?php

namespace Niyotail\Helpers\Integration;

use Illuminate\Contracts\Queue\Job;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Niyotail\Models\Employee;
use Niyotail\Models\Transaction;
use Niyotail\Services\TransactionService;

class AddLedgerEntry
{
    use InteractsWithQueue;

    private string $error;
    private int $storeId;
    private string $amount;

    public function handle(Job $job, $data)
    {
        if (!$this->validateAndMap($data)) {
            return $job->fail("Validation error| $this->error");
        }

        //Employee ID : 75 - 1K Mall
        $user = Employee::find(75);
        $requestData['type'] = Transaction::TYPE_CREDIT;
        $requestData['store_id'] = $this->storeId;
        $requestData['amount'] = $this->amount;
        $requestData['payment_details'] = json_encode($data);
        $requestData['payment_method'] = 'online';
        $requestData['auto_settle'] = 1;
        $requestData['description'] = 'POS online payment';
        $request = new Request();
        $request->query->add($requestData);

        $service = new TransactionService();
        $service->storeTransaction($request, $user);
    }

    private function validateAndMap(array $data): bool
    {
        $validator = Validator::make($data, $this->rules());
        if ($validator->fails()) {
            $errorString = implode(' , ', Arr::flatten($validator->errors()->all()));
            $this->error = $errorString;
            return false;
        }

        //Mapping class variables for clean access
        $this->storeId = $data['store_id'];
        $this->amount = $data['amount'];

        $paymentId = $data['payment_id'];
        /* Check if a primary order already exists */
        $existingTransaction = Transaction::where('payment_details', 'like', "%$paymentId%")->first();
        if(!empty($existingTransaction)) {
            $this->error = 'This transaction already exists - '.$existingTransaction->id;
            return false;
        }
        return true;
    }

    private function rules()
    {
        return [
            'payment_id' => 'required',
            'store_id' => 'required|exists:stores,id',
            'amount' => 'required',
        ];
    }
}
