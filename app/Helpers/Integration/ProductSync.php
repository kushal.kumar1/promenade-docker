<?php

namespace Niyotail\Helpers\Integration;


use Illuminate\Support\Facades\Log;
use Niyotail\Helpers\IntegrationQueueHelper;
use Niyotail\Models\Category\Cl4CostMap;
use Niyotail\Models\Product;
use Niyotail\Models\ProductPriceMap;

class  ProductSync
{
    public static function push(Product $product)
    {
        if ($product->group_id == "21805") {
            throw new \Exception("GID 21805 is blocked for pos sync");
        }

        $product->loadMissing('brand.marketer.level', 'unitVariant', 'group', 'category');

        $product->is_mall = false;

        if($product->tags->where('name', '1K-Mall')->isNotEmpty()){
            $product->is_mall = true;
        }
        $product->unsetRelation('tags');

        $marketerLevelId = $product->brand->marketer->level_id;
        $cl4Id = $product->cl4_id;
        $cl4CostMap = Cl4CostMap::with('commissionTag')->where(['marketer_level_id' => $marketerLevelId, 'category_l4_id' => $cl4Id])->first();
        if (!empty($cl4CostMap)) {
            $commissionTag = $cl4CostMap->commissionTag->name;
            $product->commission_tag = $commissionTag;
        }

        IntegrationQueueHelper::dispatch($product)->onQueue(config('queue.integration.product-sync'));
    }

    public static function pushPrice(Product $product)
    {
        $product->loadMissing('priceMap');
        $productPrice = $product->priceMap ?? null;
        if (!empty($productPrice)) {
            $payload = array();
            $payload['product_id'] = $product->id;
            $payload['prices'] = [
                'mrp' => $productPrice['new_mrp'],
                'retail' => $productPrice['new_rt_sp'],
//                'wholesale' => $productPrice['new_ws_sp']
            ];
            IntegrationQueueHelper::dispatch($payload)->onQueue(config('queue.integration.price-sync'));
        }
    }
}
