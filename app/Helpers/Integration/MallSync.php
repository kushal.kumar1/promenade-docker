<?php

namespace Niyotail\Helpers\Integration;

use Illuminate\Contracts\Queue\Job;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Niyotail\Models\AdditionalOrderInfo;
use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Niyotail\Models\Employee;
use Niyotail\Models\ProductVariant;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\StoreOrder\StoreOrderService;
use Exception;

class MallSync
{
    use InteractsWithQueue;

    private string $error;
    private int $storeId;
    private string $mallOrderId;
    private string $mallOrderStatus;
    private array $items;
    private $cart = null;

    public function handle(Job $job, $data)
    {
        if (!$this->validateAndMap($data)) {
            return $job->fail("Validation error| $this->error");
        }

        //Employee ID : 75 - 1K Mall
        $user = Employee::find(75);
        foreach ($this->items as $item) {
            $itemRow = array();
            $sku = $item['product_id'] . "-unit";
            $productVariant = ProductVariant::where('sku', $sku)->withTrashed()->first();
            $itemRow['id'] = $productVariant->id;
            $itemRow['store_id'] = $this->storeId;
            $itemRow['quantity'] = $item['quantity'];
            $itemRow['source'] = Cart::SOURCE_POS;
            $itemRow['tag'] = Cart::TAG_1K_MALL;
            $storeOrderService = new StoreOrderService();
            $customerInfo = $storeOrderService->getMallOrderCustomerInfo($this->mallOrderId);
            $additionalInfo = ['pos_order_id' => $this->mallOrderId];
            $additionalInfo = array_merge($additionalInfo, $customerInfo);
            $itemRow['additional_info'] = json_encode($additionalInfo);
            //TODO:: Refactor code with cart service
            $request = new Request();
            $request->query->add($itemRow);
            $cartService = new CartService();
            try {
                if (empty($this->cart)) {
                    $this->cart = $cartService->addToCart($request, $user);
                } else {
                    $cartService->setCart($this->cart);
                    $cartService->addToCart($request, $user);
                }
            }
            catch(Exception $e) {
                return $job->fail($e->getMessage());
            }
        }

        //Set Additional Info in cart
        // $additionalInfo['pos_order_id'] = $this->mallOrderId;
        // $this->cart->additional_info = $additionalInfo;

        //Create Store Order
        try {
            $storeOrderService = new StoreOrderService();
            $storeOrderService->create($this->cart);
        } catch (Exception $e) {
            return $job->fail($e->getMessage());
        }

    }

    private function validateAndMap(array $data): bool
    {
        $validator = Validator::make($data, $this->rules());
        if ($validator->fails()) {
            $errorString = implode(' , ', Arr::flatten($validator->errors()->all()));
            $this->error = $errorString;
            return false;
        }

        /* Check if a primary order already exists */
        $existingPrimaryOrder = AdditionalOrderInfo::where('pos_order_id', $data['ref_id'])->first();
        if(!!$existingPrimaryOrder) {
            $this->error = 'Primary order already exists for this order. Order ID - '.$existingPrimaryOrder->order_id;
            return false;
        }

        //Mapping class variables for clean access
        $this->storeId = $data['store_id'];
        $this->mallOrderId = $data['ref_id'];
        $this->mallOrderStatus = $data['status'];
        $this->items = $data['items'];
        return true;
    }

    private function rules()
    {
        return [
            'ref_id' => 'required',
            'store_id' => 'required|exists:stores,id',
            'items' => 'required|array',
            'items.*.product_id' => 'required|exists:products,id',
            'items.*.quantity' => 'required|numeric',
        ];
    }
}
