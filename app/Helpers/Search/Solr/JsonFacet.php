<?php

namespace Niyotail\Helpers\Search\Solr;

class JsonFacet
{
    private $json = [];

    public function __construct($field)
    {
        $this->setField($field);
    }

    public function setType($type)
    {
        $this->json['type'] = $type;

        return $this;
    }

    public function setField($field)
    {
        $this->json['field'] = $field;

        return $this;
    }

    public function setMinCount($minCount)
    {
        $this->json['mincount'] = $minCount;

        return $this;
    }

    public function setQuery($query)
    {
        $this->json['q'] = $query;

        return $this;
    }

    public function addFacet($name, $value)
    {
        $this->json['facet'][$name] = $value;

        return $this;
    }

    public function excludeTag($tag)
    {
        $this->json['domain'] = [
            'excludeTags' => $tag,
        ];

        return $this;
    }

    public function getValues()
    {
        return $this->json;
    }
}
