<?php

namespace Niyotail\Helpers\Search\Transformer;

class ProductVariant
{
    public function transform($results)
    {
        switch ($results['table_type']) {
            case 'product':
                return $this->transformProduct($results);
                break;
        }
    }

    private function transformProduct($product)
    {
        // 'id'=> $this->id,
        // 'name'=> ucwords($this->name),
        // 'brand'=> !empty($this->brand)?(new BrandResource($this->brand)):null,
        // 'mrp'=>$cheapestMrp,
        // 'price'=>$cheapestPrice,
        // 'margin'=>$variantMargin,
        // 'images'=>ProductImageResource::collection($this->images),
        // 'salable'=>$this->isSalable($this->beatId),
        // 'variants' => ProductVariantResource::collection($this->variants),
        return [
            'id' => (int)$product['id'],
            'name' => $product['product_name'],
            'brand' => !empty($product['brand_name']) ? ($this->transformBrand($product)) : [],
            'mrp' => 0,
            'price' => 0,
            'margin' => 0,
            'images' => !empty($product['product_images']) ? ($this->transformProductImages($product)) : [],
            'salable' => true,
            'variants' => ProductVariantResource::collection($this->variants),
        ];
    }

    private function transformBrand($product)
    {
        return [
          'id' => $product['brand_id'],
          'name' => $product['brand_name'],
          'logo' => $product['brand_logo'],
        ];
    }

    private function transformProductImages($product)
    {
        $images = [];
        foreach ($product['product_images'] as $productImage) {
            $productId = $product['id'];
            $image = [
            'id '=> 1,
            'position' => 1,
            'image' => Image::getImageSrc($product['id']."/$productImage", null, 400),
          ];
            $images[] = $image;
        }
        return $images;
    }
}
