<?php

namespace Niyotail\Helpers\Search;

use Closure;
use Niyotail\Helpers\Search\Solr\JsonFacet;
use Solarium\Client;
use Illuminate\Support\Facades\Config;

class Solr
{
    private $client;
    private $query;
    private $spellcheck = null;
    private $filterQueries = [];
    private $locationQuery = [];
    private $sorts = [];
    private $groups = [];
    private $page = 1;
    private $start = 0;
    private $rows = 18;
    private $facets = [];
    private $jsonFacets = [];
    private $returnFields = [];

    public function __construct()
    {
        $config = array(
            'endpoint' => array(
                'solr' => array(
                    'host' => Config::get('search.SOLR_HOST'),
                    'port' => Config::get('search.SOLR_PORT'),
                    'path' => Config::get('search.SOLR_PATH'),
                    'core' => Config::get('search.SOLR_CORE'),
                    'username' => Config::get('search.SOLR_USERNAME'),
                    'password' => Config::get('search.SOLR_PASSWORD'),
                    'timeout' => Config::get('search.SOLR_REQUEST_TIMEOUT'),
                ),
            ),
        );
        $this->client = new Client($config);
    }

    public function spellcheck($term)
    {
        $this->spellcheck = $term;
        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getClient()
    {
        return $this->client;
    }

    private function appendBoolean($boolean)
    {
        if (!empty($this->query)) {
            $this->query = $this->query.' '.$boolean.' ';
        }
    }

    private function appendNestedQuery($function)
    {
        $query = $this->query;
        $query = $query.'(';
        $subSolr = new self();
        call_user_func($function, $subSolr);
        $query = $query.$subSolr->getQuery();
        $query = $query.')';
        $this->query = $query;
    }

    public function select($fields)
    {
        $this->returnFields = array_merge($this->returnFields, $fields);

        return $this;
    }

    public function boost($power)
    {
        $this->query = $this->query.'^'.$power;

        return $this;
    }

    public function whereFilterQuery($function, $name, $shouldTag = false)
    {
        $query = '(';
        $subSolr = new self();
        call_user_func($function, $subSolr);
        $query = $query.$subSolr->getQuery();
        $query = $query.')';

        $filterQuery['query'] = $query;
        $filterQuery['name'] = $name;
        $filterQuery['shouldTag'] = $shouldTag;

        $this->filterQueries[] = $filterQuery;

        return $this;
    }

    public function wherePage($page)
    {
        $this->page = $page;

        return $this;
    }

    public function where($key, $value = null, $boolean = 'AND')
    {
        $this->appendBoolean($boolean);
        if ($key instanceof Closure) {
            $this->appendNestedQuery($key);
        } else {
            $this->query = $this->query.$key.':'.$value;
        }

        return $this;
    }

    public function whereIn($key, $values = null, $boolean = 'AND')
    {
        $this->appendBoolean($boolean);
        if ($key instanceof Closure) {
            $this->appendNestedQuery($key);
        } elseif (!empty($values)) {
            $valueString = implode(' ', $values);
            $this->query = $this->query.$key.':('.$valueString.')';
        }

        return $this;
    }

    public function whereBetween($key, $from, $to, $boolean = 'AND')
    {
        $this->appendBoolean($boolean);
        $this->query = $this->query.$key.':['.$from.' TO '.$to.']';

        return $this;
    }

    public function whereDistance($key, $latitude, $longitude, $distance, $boolean = 'AND')
    {
        //$this->appendBoolean($boolean);
        //$this->query = $this->query.'{!geofilt sfield='.$key.' pt='.$latitude.','.$longitude.' d='.$distance.'}';
        $query = "{!geofilt}&sfield=$key&pt=$latitude,$longitude&d=$distance";
        $filterQuery['query'] = $query;
        $filterQuery['name'] = '_distance_';
        $filterQuery['shouldTag'] = false;

        $this->locationQuery['field'] = $key;
        $this->locationQuery['latitude'] = $latitude;
        $this->locationQuery['longitude'] = $longitude;
        $this->locationQuery['distance'] = $distance;
        $this->locationQuery['sort'] = false;
        return $this;
    }

    public function orWhere($key, $value = null)
    {
        return $this->where($key, $value, 'OR');
    }

    public function orWhereIn($key, $values)
    {
        return $this->whereIn($key, $values, 'OR');
    }

    public function orWhereBetween($key, $from, $to)
    {
        return $this->whereBetween($key, $from, $to, 'OR');
    }

    public function orWhereDistance($key, $latitude, $longitude, $distance)
    {
        return $this->whereDistance($key, $latitude, $longitude, $distance, 'OR');
    }

    public function orderBy($key, $order = 'asc')
    {
        $this->sorts[$key] = $order;

        return $this;
    }

    public function groupBy($fields)
    {
        $this->groups = array_merge($this->groups, $fields);

        return $this;
    }

    public function skip($number)
    {
        $this->start = $number;

        return $this;
    }

    public function take($number)
    {
        $this->rows = $number;

        return $this;
    }

    public function withFacet($name, $min = false, $exclude = false)
    {
        $facet['name'] = $name;
        if ($min) {
            $facet['min'] = $min;
        }

        if ($exclude) {
            $facet['exclude'] = $exclude;
        }
        $this->facets[] = $facet;

        return $this;
    }

    public function withJsonFacet($name, $function)
    {
        $values = $function;
        if ($function instanceof Closure) {
            $jsonFacet = new JsonFacet($name);
            call_user_func($function, $jsonFacet);
            $values = $jsonFacet->getValues();
        }
        $this->jsonFacets[$name] = $values;

        return $this;
    }

    public function get()
    {
        $select = $this->client->createSelect();
        $start = (($this->page - 1) * $this->rows) + $this->start;
        $select->setQuery($this->query)
            ->setFields($this->returnFields)
            ->setStart($start)
            ->setRows($this->rows);

        if (!empty($this->spellcheck)) {
            $spellcheck = $select->getSpellcheck();
            $spellcheck->setQuery($this->spellcheck);
            $spellcheck->setCount(10);
            $spellcheck->setBuild(true);
            $spellcheck->setCollate(true);
            $spellcheck->setExtendedResults(true);
            $spellcheck->setCollateExtendedResults(true);
        }

        if (!empty($this->filterQueries)) {
            foreach ($this->filterQueries as $filterQuery) {
                $fq = $select->createFilterQuery($filterQuery['name'])
                    ->setQuery($filterQuery['query']);
                if ($filterQuery['shouldTag']) {
                    $fq->addTag($filterQuery['name']);
                }
            }
        }

        if (!empty($this->locationQuery)) {
            $spatial = $select->getSpatial();
            $spatial->setPoint("'".$this->locationQuery['latitude'].",".$this->locationQuery['longitude']."'");
            $spatial->setField($this->locationQuery['field']);
            $spatial->setDistance(doubleval($this->locationQuery['distance']));
            if (!empty($this->locationQuery['sort'])) {
              $select->addSort('geodist('.$this->locationQuery['field'].','.$this->locationQuery['latitude'].','.$this->locationQuery['longitude'].')', 'asc');
            }
        }

        if (!empty($this->jsonFacets)) {
            $customizer = $this->client->getPlugin('customizerequest');
            $customizer->createCustomization('jsonFacets')
                ->setType('param')
                ->setName('json.facet')
                ->setValue(json_encode($this->jsonFacets));
        }

        if (!empty($this->groups)) {
            $groupComponent = $select->getGrouping();
            $groupComponent->addFields($this->groups);
            $groupComponent->setMainResult(true);
        }

        if (!empty($this->facets)) {
            $facetSet = $select->getFacetSet()
                ->setLimit(-1)
                ->setSort('count');
            foreach ($this->facets as $facet) {
                $field = $facetSet->createFacetField($facet['name'])->setField($facet['name']);
                if (!empty($facet['min'])) {
                    $field->setMinCount($facet['min']);
                }
                if (!empty($facet['exclude'])) {
                    $field->addExclude($facet['exclude']);
                }
            }
        }

        if (!empty($this->sorts)) {
            $select->addSorts($this->sorts);
        }

        return $this->client->select($select);
    }

    public function ping()
    {
        $ping = $this->client->createPing();
        $result = $this->client->ping($ping);
        print_r($result);
    }

    public function displayResult($results)
    {
        echo $results->getNumFound();
        foreach ($results as $result) {
            print_r($result);
        }
    }
}
