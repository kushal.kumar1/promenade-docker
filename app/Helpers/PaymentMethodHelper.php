<?php

namespace Niyotail\Helpers;

use Niyotail\Models\Cart;

class PaymentMethodHelper
{
    public static function getPaymentMethods(Cart $cart):array
    {
        $methods = config('payments.online_methods');
        $storePaymentMethods = null;
        foreach($methods as $key => $paymentMethod) {
            $storePaymentMethods[$key] = $paymentMethod;
        }
        return $storePaymentMethods;
    }
}
