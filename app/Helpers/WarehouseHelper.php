<?php


namespace Niyotail\Helpers;


class WarehouseHelper
{
    public static function getWarehouseForPurchase($isDirectStorePurchase) {
        return $isDirectStorePurchase ? 1 : 2;
    }
}