<?php

namespace Niyotail\Helpers;

use Niyotail\Models\Cart;
use Niyotail\Models\CartItem;
use Illuminate\Database\Eloquent\Collection;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Helpers\RuleEngine\RuleProcessors\CartRuleProcessor;
use Niyotail\Helpers\RuleEngine\RuleProcessors\CartItemsRuleProcessor;
use Niyotail\Helpers\RuleEngine\RuleProcessors\VariantAllRulesProcessor;
use Niyotail\Helpers\RuleEngine\RuleProcessors\VariantDiscountRuleProcessor;
use Niyotail\Helpers\RuleEngine\RuleProcessors\VariantPricingRuleProcessor;
use Niyotail\Models\Expression;
use Niyotail\Models\Product;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Rule;
use Niyotail\Models\Store;

class RuleEngine
{
    public static function processCart(?Cart $cart)
    {
        if (!empty($cart)) {
            $mediumMap = [
                'employee' => Expression::MEDIUM_ADMIN,
                'user' => Expression::MEDIUM_RETAILER,
                'agent' => Expression::MEDIUM_AGENT
            ];
            $medium = $mediumMap[$cart->created_by_type];
            $cart->items()->where('source', CartItem::SOURCE_OFFER)->delete();
            event(new RecalculateCart($cart));
            $cartItemsRuleProcessor = new CartItemsRuleProcessor($cart, $medium);
            $cartItemsRuleProcessor->execute();
            $cartRuleProcessor = new CartRuleProcessor($cart, $medium);
            $cartRuleProcessor->execute();
        }
    }

    public static function processProduct(Product $product, Store $store, $medium)
    {
        $products = new Collection([$product]);
        self::processProducts($products, $store, $medium);
    }

    public static function processProducts($products, Store $store, $medium)
    {
        $products->loadMissing('variants');
        $variants = $products->pluck('variants')->flatten();
        $variants = new Collection($variants);
        self::processVariants($variants, $store, $medium);
    }

    public static function processVariant(ProductVariant $variant, Store $store, $medium)
    {
        $variants = new Collection([$variant]);
        self::processVariants($variants, $store, $medium);
    }

    public static function processVariants(Collection $variants, Store $store, $medium)
    {
        $variants->loadMissing('product.collections', 'product.brand');
        $variantPricingRuleProcessor = new VariantPricingRuleProcessor($variants, $store, $medium);
        $variantPricingRuleProcessor->execute();
        $variantDiscountRuleProcessor = new VariantDiscountRuleProcessor($variants, $store, $medium);
        $variantDiscountRuleProcessor->execute();
    }

    public static function loadPricing(ProductVariant $variant, Store $store, $medium)
    {
        $variants = new Collection([$variant]);
        $variants->loadMissing('product.collections');
        $variantPricingRuleProcessor = new VariantPricingRuleProcessor($variants, $store, $medium);
        $variantPricingRuleProcessor->execute();
    }

    public static function processProductForRules(Product $product, Store $store = null, $medium)
    {
        $products = new Collection([$product]);
        self::processProductsForRules($products, $store, $medium);
    }

    public static function processProductsForRules($products, Store $store = null, $medium)
    {
        $products->loadMissing('variants');
        $variants = $products->pluck('variants')->flatten();
        $variants = new Collection($variants);
        self::processVariantsForRules($variants, $store, $medium);
    }

    public static function processVariantsForRules(Collection $variants, Store $store = null, $medium)
    {
        $variants->loadMissing('product.collections', 'product.brand');
        $variantAllRulesProcessor = new VariantAllRulesProcessor($variants, $store, $medium);
        $variantAllRulesProcessor->execute();
    }
}
