<?php

namespace Niyotail\Helpers\Sms;

use InvalidArgumentException;
use Niyotail\Helpers\Sms\Drivers\Gupshup;
use Niyotail\Helpers\Sms\Drivers\Karix;
use Niyotail\Helpers\Sms\Drivers\Sns;

class Sms
{
    private $defaultDriver;

    private $type;

    const TYPE_DEFAULT = "default";
    const TYPE_OTP = "otp";

    public function __construct($type = self::TYPE_DEFAULT)
    {
        $this->defaultDriver = config('sms.default');
        $this->type = $type;
    }

    public function driver($name = null)
    {
        return $this->resolve($name);
    }

    private function resolve($name)
    {
        $driverMethod = 'create' . ucfirst($name) . 'Driver';
        if (method_exists($this, $driverMethod)) {
            return $this->{$driverMethod}(config('sms.' . $name));
        } else {
            throw new InvalidArgumentException("Driver [{$name}] is not supported.");
        }
    }

    public function __call($name, $parameter)
    {
        $driver = $this->getDefaultDriver();
        return call_user_func_array([$driver, $name], $parameter);
    }

    private function getDefaultDriver()
    {
        return $this->resolve($this->defaultDriver);
    }

    protected function createSnsDriver($config)
    {
        return new Sns($config['region'], $config['key'], $config['secret'], $config['version']);
    }

    protected function createKarixDriver($config)
    {
        return new Karix($config['url'], $config['key'][$this->type]);
    }

    protected function createGupshupDriver($config)
    {
        return new Gupshup($config['user'], $config['url'], $config['password']);
    }

}
