<?php

namespace Niyotail\Helpers\Sms\Drivers;

use Aws\Sns\SnsClient;

class Sns extends Base
{
    private $client;

    public function __construct($region, $key, $secret, $version)
    {
        $this->client = new SnsClient([
            'region' => $region,
            'version' => $version,
            'credentials' => ['key' => $key, 'secret' => $secret]
        ]);
    }

    protected function dispatchMessage($to, $message)
    {
        try {
            $result = $this->client->publish([
                'Message' => $message,
                'PhoneNumber' => $to,
                'MessageAttributes' => ['AWS.SNS.SMS.SMSType' =>
                    ['DataType' => 'String', 'StringValue' => 'Transactional']
                ]]);
        } catch (\Exception $e) {
            return false;
        }
        return $messageID = $result->get('MessageId');
    }
}
