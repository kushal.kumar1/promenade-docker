<?php

namespace Niyotail\Helpers\Sms\Drivers;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;

class Gupshup extends Base
{

    private $user;
    private $url;
    private $password;
    private $httpClient;
    private $mask;

    public function __construct($user, $url, $password)
    {
        $this->httpClient = new HttpClient();
        $this->mask = 'NIYOTL';
        $this->user = $user;
        $this->url = $url;
        $this->password = $password;
    }

    protected function dispatchMessage($to, $message)
    {
        try {
            $response = $this->httpClient->get($this->url, ['query' => [
                'userid' => $this->user,
                'password' => $this->password,
                'auth_scheme' => 'Plain',
                'method' => 'SendMessage',
                'send_to' => $to,
                'msg' => $message,
                'msg_type' => 'Text',
                'format' => 'json',
                'mask' => $this->mask
            ]]);
        } catch (RequestException $e) {
            return false;
        }
        return true;
    }
}