<?php


namespace Niyotail\Helpers\Sms\Drivers;


use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;

class Karix extends Base
{
    private $key;

    private $httpClient;

    private $url;

    public function __construct($url, $key)
    {
        $this->httpClient = new HttpClient();
        $this->key = $key;
        $this->url = $url;
    }

    protected function dispatchMessage($to, $message)
    {
        try {
            $repsonse = $this->httpClient->get($this->url, ['query' => [
                'ver' => '1.0',
                'key' => $this->key,
                'dest' => $to,
                'send' => 'ONEKAY',
                'text' => $message
            ]]);
        } catch (RequestException $e) {

        }
    }
}