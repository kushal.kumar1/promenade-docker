<?php

namespace Niyotail\Helpers\Sms\Drivers;
use Closure;
use Illuminate\Support\Facades\View;


abstract class Base
{

    /**
     * @param $view
     * @param array $data
     * @return string
     */
    protected function setMessage($view, array $data): string
    {
        return View::make($view, $data)->render();
    }

    /**
     * @param $to
     * @param $view
     * @param array $data
     * @return bool
     */
    public function send($to, $view, array $data = [])
    {
        if (getenv('APP_ENV') == 'local' && empty($to)) {
            return true;
        } elseif (getenv('APP_ENV') == 'local') {
            $to = env('SMS_TEST_NUMBER', 9999);
        }
        $message = $this->setMessage($view, $data);
        $this->dispatchMessage($to, $message);
    }

    abstract protected function dispatchMessage($to, $message);
}