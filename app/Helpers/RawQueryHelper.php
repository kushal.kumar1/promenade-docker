<?php

namespace Niyotail\Helpers;
use Illuminate\Support\Facades\DB;

class RawQueryHelper
{
    public static function getPurchaseReportQuery($startDate, $endDate, $reportAs) 
        {
            $reportBy = null;
            switch ($reportAs) {
            case "created_at":
                $reportBy = 'created_at';
                break;
            case "invoice_at":
                $reportBy = 'invoice_date';
                break;
            default:
                
            }
            $query = DB::select (DB::raw( "select 

	pi.reference_id as Voucher_No,
    date(convert_tz(pi.created_at , '+00:00', '+05:30')) as Voucher_Date,
    date(convert_tz(pi.invoice_date , '+00:00', '+05:30')) as Supplier_Invoice_Date,
    pi.vendor_ref_id as Supplier_Invoice_Number,
	CONCAT(CAST(REPEAT(0, (6 - LENGTH(v.id))) as CHAR(6)), v.id) as Vendor_Id,
    v.name as Vendor_Name,
    va.address as Add1, 
    va.name as Add2,
    '' as Add3,
    va.state as State,
    va.gstin as GSTIN_No,
    '' as GSTIN_Type,
    v.contact_phone as Mobile,
    v.email as Email,
    CONCAT(CAST(REPEAT(0, (6 - LENGTH(p.id))) as CHAR(6)), p.id) as PID,
    p.name as Item_Description, 
    p.hsn_sac_code as HSNCode,
    pii.sku as SKU,
    pii.quantity as Invoice_Quantity,
    pv.quantity as Units_In_SKU,
    pii.quantity*pv.quantity as Total_Units,
    pii.base_cost as Unit_Rate,
    round(pii.quantity*pv.quantity*pii.base_cost, 4) as Gross_Amount,
    0 as Freight,
	pii.trade_discount + pii.scheme_discount + pii.qps_discount + pii.cash_discount + pii.program_schemes + 	pii.other_discount as Pre_Tax_Discount,
    ROUND(pii.total_taxable*(pii.quantity*pv.quantity), 4) as Taxable, 
    IFNULL(pt_gst.gst_perc, 0) as GST_Perc,
    IFNULL(pt_cess.cess_perc, 0) as CESS_Perc,
    IFNULL(round(pt_sp_cess.sp_cess_charge, 4), 0) as SPL_CESS_Per_Unit,
    round(pii.quantity*pv.quantity*pii.sgst, 4) as SGST,
    round(pii.quantity*pv.quantity*pii.cgst, 4) as CGST,
    round(pii.quantity*pv.quantity*pii.igst, 4) as IGST,
    round(pii.quantity*pv.quantity*pii.cess, 4) as CESS,
    round(pii.quantity*pv.quantity*pii.spl_cess, 4) as SPL_CESS,
    0 as TCS,
    ROUND(pii.total_invoiced_cost*(pii.quantity*pv.quantity), 4) as Total_Amount,
    ROUND(pii.post_tax_discount*(pii.quantity*pv.quantity), 4) as Post_Tax_Discount,
    0 as Round_Off,
    ROUND(pii.total_effective_cost*(pii.quantity*pv.quantity), 4) as Invoice_Total,
    '' as Narration,
    '' as Vehicle_No,
    pi.eway_number as Ewaybill_No, 
    grn_items.quantity as a,
    grn_items.quantity*pv.quantity as GRN_Qty_In_Units



from purchase_invoices as pi 
left join purchase_invoice_items pii on pi.id = pii.purchase_invoice_id
left join purchase_invoice_grn_items pigi on pii.id = pigi.item_id 
left join vendors as v on pi.vendor_id = v.id
left join vendor_addresses as va on pi.vendor_address_id  = va.id
left join products as p on pii.product_id = p.id
left join product_variants as pv on pii.sku = pv.sku
left join tax_classes as tc on p.tax_class_id = tc.id
left join purchase_invoice_grn_items as grn_items on pii.id = grn_items.item_id

left join (select sum(t.percentage) as gst_perc, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%GST%' GROUP by t.tax_class_id) as pt_gst on pt_gst.tax_class_id = p.tax_class_id
            
left join (select sum(t.percentage) as cess_perc, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%CESS%' GROUP by t.tax_class_id) as pt_cess on pt_cess.tax_class_id = p.tax_class_id
            
left join (select sum(t.additional_charges_per_unit) as sp_cess_charge, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%SPL%' GROUP by t.tax_class_id) as pt_sp_cess on pt_sp_cess.tax_class_id = p.tax_class_id

where (date(convert_tz(pi.".$reportBy.", '+00:00', '+05:30')) >= '".$startDate."' and date(convert_tz(pi.".$reportBy.", '+00:00', '+05:30')) < '".$endDate."')

union all

select 

	pi.reference_id as Voucher_No,
    date(convert_tz(pi.created_at , '+00:00', '+05:30')) as Voucher_Date,
    date(convert_tz(pi.invoice_date , '+00:00', '+05:30')) as Supplier_Invoice_Date,
    pi.vendor_ref_id as Supplier_Invoice_Number,
    CONCAT(CAST(REPEAT(0, (6 - LENGTH(v.id))) as CHAR(6)), v.id) as Vendor_Id,
    v.name as Vendor_Name,
    va.address as Add1, 
    va.name as Add2,
    '' as Add3,
    va.state as State,
    va.gstin as GSTIN_No,
    '' as GSTIN_Type,
    v.contact_phone as Mobile,
    v.email as Email,
    '' as PID,
    pisi.name as Item_Description, 
    pisi.hsn as HSNCode,
    '' as SKU,
    pisi.quantity as Invoice_Quantity,
    '' as Units_In_SKU,
    '' as Total_Units,
    round(pisi.invoice_cost/pisi.quantity, 4) as Unit_Rate,
    pisi.invoice_cost as Gross_Amount,
    0 as Freight,
	pisi.discount as Pre_Tax_Discount,
    '' as Taxable, 
    IFNULL(pt_gst.gst_perc, 0) as GST_Perc,
    IFNULL(pt_cess.cess_perc, 0) as CESS_Perc,
    IFNULL(round(pt_sp_cess.sp_cess_charge, 4), 0) as SPL_CESS_Per_Unit,
    0 as SGST,
    0 as CGST,
    0 as IGST,
    0 as CESS,
    0 as SPL_CESS,
    0 as TCS,
    ROUND(pisi.effective_cost, 4) as Total_Amount,
    ROUND(pisi.post_tax_discount, 4) as Post_Tax_Discount,
    0 as Round_Off,
    ROUND(pisi.invoice_cost, 4) as Invoice_Total,
    '' as Narration,
    '' as Vehicle_No,
    pi.eway_number as Ewaybill_No, 
    0 as a,
    0 as GRN_Qty_In_Units


from purchase_invoice_short_items as pisi 
left join purchase_invoices as pi on pisi.purchase_invoice_id = pi.id 
left join vendors as v on pi.vendor_id = v.id
left join vendor_addresses as va on pi.vendor_address_id  = va.id
left join tax_classes as tc on pisi.tax_class = tc.id

left join (select sum(t.percentage) as gst_perc, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%GST%' GROUP by t.tax_class_id) as pt_gst on pt_gst.tax_class_id = pisi.tax_class
            
left join (select sum(t.percentage) as cess_perc, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%CESS%' GROUP by t.tax_class_id) as pt_cess on pt_cess.tax_class_id = pisi.tax_class
            
left join (select sum(t.additional_charges_per_unit) as sp_cess_charge, t.tax_class_id from taxes t where t.source_state_id = t.supply_state_id and t.name like '%SPL%' GROUP by t.tax_class_id) as pt_sp_cess on pt_sp_cess.tax_class_id = pisi.tax_class
                        
            where (date(convert_tz(pi.".$reportBy.", '+00:00', '+05:30')) >= '".$startDate."' and date(convert_tz(pi.".$reportBy.", '+00:00', '+05:30')) < '".$endDate."')"));
            
            return $query;
        } 
}
