<?php

namespace Niyotail\Helpers;

use Illuminate\Database\Eloquent\Relations\MorphTo as EloquentMorphTo;
use Illuminate\Database\Eloquent\Collection;

class MorphTo extends EloquentMorphTo
{
    protected $types = ['*'];

    public function addTypeConstraints(array $types)
    {
        $this->types = $types;
        return $this;
    }

    protected function buildDictionary(Collection $models)
    {
        foreach ($models as $model) {
            $type = $model->{$this->morphType};
            if ($type && ($this->types == ['*'] || in_array($type, $this->types))) {
                $this->dictionary[$model->{$this->morphType}][$model->{$this->foreignKey}][] = $model;
            }
        }
    }
}
