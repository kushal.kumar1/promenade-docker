<?php

namespace Niyotail\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Niyotail\Helpers\Search\Solr;
use Niyotail\Models\Beat;
use Niyotail\Models\Brand;
use Niyotail\Models\Collection;
use Niyotail\Models\Marketer;
use Niyotail\Models\Product;
use Niyotail\Models\ProductGroup;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Models\Shipment;
use Niyotail\Models\Store;
use Niyotail\Models\WarehouseScope;

class SearchHelper
{
    // public function searchProduct($term)
    // {
    //     $term = $this->sanitize($term);
    //     $term = $this->escapeTerm($term);
    //     $products = Product::searchWithKeyword($term);
    //     return $products;
    // }

    public function searchStores($request)
    {
        $term = $request->keyword;
        $term = $this->sanitize($term);
        $term = $this->escapeTerm($term);
        $lat = $request->has('latitude') ? $request->latitude : false;
        $lng = $request->has('longitude') ? $request->longitude : false;
        $isOutstandingRequired = true;
        $results = $this->searchStoreWithKeyword($term, $lat, $lng, $isOutstandingRequired);
        //dd($results->getDebug());
        $storeResults = empty($results->getData()['response']['docs']) ? [] : $results->getData()['response']['docs'];
        $suggestions = [];
        if (!empty($results->getSpellCheck()) && !empty($results->getSpellCheck()->getCollations())) {
            $collations = $results->getSpellCheck()->getCollations();
            $keyword = $term;
            foreach ($collations as $collation) {
                foreach ($collation->getCorrections() as $input => $correction) {
                    $suggestions[] = $correction;
                    $keyword = str_replace($input, $correction, $keyword);
                }
            }
            if (strcmp($keyword, $term) != 0) {
                $newResults = $this->searchStoreWithKeyword($keyword, $lat, $lng, $isOutstandingRequired);
                $newStores = empty($newResults->getData()['response']['docs']) ? [] : $newResults->getData()['response']['docs'];
                if (!empty($newStores)) {
                    $storeResults = array_merge($storeResults, $newStores);
                }
            }
        }

        $stores = collect();
        $storeIDs = array_column($storeResults, "store_id");
        if (!empty($storeIDs)) {
            $stores = Store::whereIn('id', $storeIDs)
                ->with('users', 'beat', 'creditLimit')
                ->get();
        }

        return $stores;
    }

    public function searchProduct($term, $request)
    {
        $term = $this->sanitize($term);
        $term = $this->escapeTerm($term);
        $results = $this->searchProductWithKeyword($term);
        $productResults = empty($results->getData()['response']['docs']) ? [] : $results->getData()['response']['docs'];
        $suggestions = [];
        if (!empty($results->getSpellCheck()) && !empty($results->getSpellCheck()->getCollations())) {
            $collations = $results->getSpellCheck()->getCollations();
            $keyword = $term;
            foreach ($collations as $collation) {
                foreach ($collation->getCorrections() as $input => $correction) {
                    $suggestions[] = $correction;
                    try {
                        $keyword = str_replace($input, $correction, $keyword);
                    } catch (\Exception $e) {
                        $keyword = $term;
                    }
                }
            }
            if (strcmp($keyword, $term) != 0) {
                $newResults = $this->searchProductWithKeyword($keyword);
                $newProducts = empty($newResults->getData()['response']['docs']) ? [] : $newResults->getData()['response']['docs'];
                if (!empty($newProducts)) {
                    $productResults = array_merge($productResults, $newProducts);
                }
            }
        }

        $products = collect();
        $productIDs = array_column($productResults, "product_id");
        if (!empty($productIDs)) {
            $productQuery = Product::whereIn('products.id', $productIDs)
                ->with('images', 'variants', 'activeInventories')
                ->selectRaw('products.*, sum(quantity) as total_inventory')
                ->join('inventories', 'inventories.product_id', '=', 'products.id')
                ->withStockByWarehouse(2)
                ->groupBy('product_id');

//          if (!empty($request->warehouse_id)) {
//              $productQuery = $productQuery->whereIn('inventories.warehouse_id', $request->warehouse_id);
//          } else {
//              $productQuery = $productQuery->whereNotIn('inventories.warehouse_id', ['2']);
//          }
            $products = $productQuery
                // ->withCount('focussed')
                // ->orderBy('total_inventory', 'desc')
                // ->orderBy('published_at', 'desc')
                // ->orderBy('focussed_count', 'desc')
                ->get();
        }
        return $products;
    }

    private function sanitize($term)
    {
        return trim($term);
    }

    public function escapeTerm($input)
    {
        $pattern = '/(\+|-|&&|\|\||!|\(|\)|\{|}|\[|]|\^|"|~|\*|\?|:|\/|\\\)/';
        return preg_replace($pattern, '\\\$1', $input);
    }

    private function searchProductWithKeyword($q = false, $page = 1, $rows = 100)
    {
        $solr = App::make(Solr::class);
        $solr->
        select(['product_id'])
            // select(['id', 'product_name', 'product_status', 'product_code',  'product_hsn_code', 'product_slug', 'product_description', 'product_meta_title', 'product_meta_description',
            // 'brand_id', 'brand_name', 'marketer_id', 'marketer_name', 'marketer_code', 'marketer_alias' ])
            ->where('table_type', 'product')
            ->where('product_total_inventory', '[0.1 TO *]')
            ->where('product_status', true);

        if (!empty($q)) {
            $solr->where(function ($subSolr) use ($q) {
                $subSolr->where('ft_product_suggest', '"' . $q . '"')->boost(15)
                    ->orwhere('ft_product_suggest_ws', '"' . $q . '"')->boost(30)
                    ->orWhere('product_name', '"' . $q . '"')->boost(100)
                    ->orWhere('product_barcode', '"' . $q . '"')->boost(100)
                    ->orWhere('product_barcode', '*' . $q . '*')->boost(50)
                    ->orWhere('ft_product_left_suggest', '"' . $q . '"')->boost(70)
                    ->orWhere('ft_product_name_en', '"' . $q . '"')->boost(75);
                // ->orderBy('product_published_at', 'desc')
                // ->orderBy('product_total_inventory', 'desc');
            });
        }

        if (empty($page)) {
            $page = 1;
        }

        //Moved ordering to subquery
        // $solr->orderBy('product_published_at', 'desc');
        // $solr->orderBy('product_total_inventory', 'desc');

        $solr->spellcheck($q);

        return $solr->wherePage($page)
            ->take($rows)
            ->get();
    }

    private function searchStoreWithKeyword($q = false, $lat = false, $long = false, $outstanding = false, $page = 1, $rows = 20)
    {
        $solr = App::make(Solr::class);
        $solr->select(['store_id'])
            ->where('table_type', 'store')
            ->where('store_verified', true);
        if ($outstanding) {
            //$solr->where('-store_outstanding', 0);
            $solr->whereIn('store_status', [1, 2]);
        } else {
            $solr->where('store_status', 1);
        }

        if (!empty($q)) {
            $solr->where(function ($subSolr) use ($q) {
                $subSolr->where('ft_store_suggest', '"' . $q . '"')->boost(20)
                    ->orwhere('ft_store_suggest_ws', '"' . $q . '"')->boost(50)
                    ->orWhere('store_name', '"' . $q . '"')->boost(100)
                    ->orWhere('ft_store_left_suggest', '"' . $q . '"')->boost(60)
                    ->orWhere('ft_store_name_en', '"' . $q . '"')->boost(70);
                if (is_numeric($q) && strlen($q) >= 1) {
                    $subSolr->orWhere('store_id', '"' . $q . '"')->boost(100);
                }
                if (is_numeric($q) && strlen($q) >= 4) {
                    $subSolr->orWhere('ft_store_user_mobile', '*' . $q . '*')->boost(100);
                }
            });
        }

        if (!empty($lat) && !empty($long)) {
            $solr->whereDistance('store_latlong', $lat, $long, 20);
            //->orderBy('geodist(store_latlong,'.$lat.','.$long.')', 'asc');
        }

        if (empty($page)) {
            $page = 1;
        }

        $solr->spellcheck($q);

        return $solr->wherePage($page)
            ->take($rows)
            ->get();
    }

    public static function getDashboardFiltersFromRequest(\Illuminate\Http\Request $request, $withData = false)
    {
        $now = Carbon::now()->format("Y-m-d");
        $lastMonthDate = Carbon::now()->subMonth()->format("Y-m-d");

        $startDate = $request->input('start_date', $lastMonthDate);
        $endDate = $request->input('end_date', $now);

        $storeIds = $request->input('store_ids', []);
        $beatIds = $request->input('beat_ids', []);
        $productIds = $request->input('product_ids', []);
        $brandIds = $request->input('brand_ids', []);
        $collectionIds = $request->input("collection_ids", []);
        $marketerIds = $request->input("marketer_ids", []);
        $storeType = $request->input("store_type", "all");

        $filters = array(
            "start_date" => $startDate,
            "end_date" => $endDate,
            "store_ids" => $storeIds,
            "beat_ids" => $beatIds,
            "product_ids" => $productIds,
            "brand_ids" => $brandIds,
            "collection_ids" => $collectionIds,
            "marketer_ids" => $marketerIds,
            "store_type" => $storeType
        );

        $response = array(
            "filters" => $filters,
        );

        if ($withData) {
            $data = array();

            $data['store_type'] = $filters['store_type'];

            if (!empty($filters['store_ids'])) {
                $stores = Store::getByIds($filters['store_ids'])->toArray();
                $data['stores'] = $stores;
            }

            if (!empty($filters['beat_ids'])) {
                $beats = Beat::getByIds($filters['beat_ids'])->toArray();
                $data['beats'] = $beats;
            }

            if (!empty($filters['brand_ids'])) {
                $brands = Brand::getByIds($filters['brand_ids'])->toArray();
                $data['brands'] = $brands;
            }

            if (!empty($filters['product_ids'])) {
                $products = Product::getByIds($filters['product_ids'])->toArray();
                $data['products'] = $products;
            }

            if (!empty($filters['collection_ids'])) {
                $collections = Collection::getByIds($filters['collection_ids'])->toArray();
                $data['collections'] = $collections;
            }

            if (!empty($filters['marketer_ids'])) {
                $collections = Marketer::getByIds($filters['marketer_ids'])->toArray();
                $data['marketers'] = $collections;
            }

            $response['data'] = $data;
        }

        return $response;
    }

    public static function getChartTypes()
    {
        return collect([
            'order_counts' => 'Order Counts',
            'order_totals' => 'Order Total',
            'gross_revenues' => 'Gross Revenues',
            'return_totals' => 'Return Totals',
            'rto_totals' => 'RTO Totals',
            'brand_counts' => 'Brand Counts',
            'net_revenues' => 'Net Revenues',
            'top_brands' => 'Top Brands',
            'top_marketers' => 'Top Marketers',
            'top_stores' => 'Top Stores',
            'order_item_units' => 'Order Item Units',
            //'top_products' => 'Top Products'
        ])->sort();
    }

    public function suggestItemsInPurchaseInvoiceCart($term, PurchaseInvoiceCart $cart)
    {

        $purchaseOrder = $cart->purchaseOrder;
        if ($purchaseOrder) {
            $poItems = $purchaseOrder->items;
            $uniqueGroupIds = $poItems->pluck('demand.group_id')->unique()->filter()->toArray();
        }

        $productGroups = ProductGroup::with(['products' => function ($query) use ($term) {
            $query->where("name", "like", "%$term%");
            $query->orWhere("barcode", "like", "%$term%");
        }])
            ->whereHas('products', function ($query) use ($term) {
                $query->where("name", "like", "%$term%");
                $query->orWhere("barcode", "like", "%$term%");
            });

        if (!empty($uniqueGroupIds)) {
            $productGroups = $productGroups->whereIn('id', $uniqueGroupIds);
        }

        $productGroups = $productGroups->limit(5)->get();

        $products = $productGroups->map(function ($group) {
            return $group->products;
        })->flatten(1);

        if (!empty($poItems)) {
            $products = $products->map(function ($product) use ($poItems) {
                $poItem = $poItems->where('product_id', $product->id)->first();
                $productDetails = $product;
                $productDetails->po_id = $poItem ? $poItem->purchase_order_id : '';
                $productDetails->po_sku = $poItem ? $poItem->sku : '';
                $productDetails->po_qty = $poItem ? $poItem->quantity : '';
                $productDetails->po_cost = $poItem ? $poItem->cost_per_unit : '';
                return $productDetails;
            });
        }

        return view('admin.purchase_invoice.cart._products', compact('products'));
    }

    public function searchFromSourceShipment($term, PurchaseInvoiceCart $cart)
    {
        $shipment = Shipment::withoutGlobalScope(WarehouseScope::class)->with(['order' => function ($orderQuery) {
            $orderQuery->withoutGlobalScope(WarehouseScope::class)->with('items.productVariant', 'items.product');
        }])->find($cart->shipment_id);

        $orderItems = $shipment->order->items->flatten();

        return view('admin.purchase_invoice.cart._products', compact('orderItems',));
    }
}
