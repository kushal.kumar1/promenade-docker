<?php

namespace Niyotail\Helpers;

use Barryvdh\Snappy\Facades\SnappyPdf;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Picklist;
use Niyotail\Models\PicklistItem;
use Niyotail\Models\Product;

class PicklistHelper
{
    public static function getProcessedProducts(Picklist $picklist)
    {
        $processedProductIds = PicklistItem::with('orderItem')
            ->whereIn('status', [PicklistItem::STATUS_PICKED, PicklistItem::STATUS_SKIPPED])
            ->where('picklist_id', $picklist->id)
            ->get()
            ->pluck('orderItem.product_id')->unique()->toArray();

        $processedProducts = Product::with('featuredImage')
            ->with(['picklistItems' => function ($query) use ($picklist) {
                $query->where('picklist_id', $picklist->id);
            }])
            ->whereIn('id', $processedProductIds)
            ->get();

        $processedProducts = $processedProducts->sortByDesc(function ($product, $key) {
            return $product->picklistItems->max('updated_at');
        });

        return $processedProducts;
    }

    public static function getPdf(Picklist $picklist, $items)
    {
        $timestamp = Carbon::parse($picklist->created_at)->timestamp;
        $path = "$picklist->id-$timestamp.pdf";
        if (!Storage::disk('picklist')->exists($path)) {
            self::generatePdf($picklist, $items);
        }
        $url = storage_path('app/picklist') . "/$path";
        return response()->file($url);
    }

    /**
     * Generated pdf for the picklist and store it in the storage.
     * @param Picklist $picklist
     */
    public static function generatePdf(Picklist $picklist, $items)
    {
        $picklist->loadMissing('agent', 'orderItems.order');

        $pdf = SnappyPdf::loadView('pdf.picklist', ['picklist' => $picklist, 'items' => $items])
            ->setOption('footer-center', utf8_decode('Page [page] of [topage] (Picklist ID: ' . $picklist->id . ')'))
            ->setPaper('A4')
            ->setOption('image-quality', 100)->output();

        $timestamp = Carbon::parse($picklist->created_at)->timestamp;
        $path = "$picklist->id-$timestamp.pdf";
        Storage::disk('picklist')->put($path, $pdf);
    }
}
