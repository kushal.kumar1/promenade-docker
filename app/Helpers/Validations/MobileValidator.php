<?php

namespace Niyotail\Helpers\Validations;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Routing\Exception\InvalidParameterException;



class MobileValidator
{
    private $lib;

    private $types = [];

    private $countries = [];

    public function __construct(PhoneNumberUtil $lib)
    {
        $this->lib = $lib;
    }

    public function validateMobile($attribute, $value, array $parameters, $validator)
    {
        $this->assignParameters($parameters);

        if ($this->isValidNumber($value) && $this->isCorrectFormat($value)) {
            return true;
        }
        $validator->setCustomMessages(['The :attribute is invalid number']);
        return false;
    }

    protected function isValidNumber($number, $country = null)
    {
        try {
            $phoneNumber = $this->lib->parse($number, $country);
            if (empty($this->types) || in_array($this->lib->getNumberType($phoneNumber), $this->types)) {
                return $this->lib->isPossibleNumber($phoneNumber);
            }
        } catch (NumberParseException $e) {
        }
        return false;
    }

    private function isCorrectFormat($number)
    {
        try {
            $phoneNumber = $this->lib->parse($number, null);
            if ($this->lib->format($phoneNumber, PhoneNumberFormat::E164) == $number) {
                return true;
            }
        } catch (NumberParseException $e) {
        }
        return false;
    }


    private function assignParameters($parameters)
    {
        $types = [];
        foreach ($parameters as $parameter) {
            if ($this->isPhoneType($parameter)) {
                $types[] = strtoupper($parameter);
            } elseif ($this->isPhoneCountry($parameter)) {
                $this->countries[] = strtoupper($parameter);
            } else {
                throw new InvalidParameterException($parameter);
            }
        }
        $this->types = $this->parseTypes($types);
    }


    private function isPhoneType($type)
    {
        return defined('\libphonenumber\PhoneNumberType::' . strtoupper($type));
    }


    private function isPhoneCountry($code)
    {
        return array_key_exists($code, CountryCodes::$countries);
    }


    protected function parseTypes(array $types)
    {
        array_walk($types, function (&$type) {
            $type = constant('\libphonenumber\PhoneNumberType::' . $type);
        });
        if (array_intersect([PhoneNumberType::FIXED_LINE, PhoneNumberType::MOBILE], $types)) {
            $types[] = PhoneNumberType::FIXED_LINE_OR_MOBILE;
            $types[] = PhoneNumberType::UNKNOWN;
        }
        return $types;
    }
}