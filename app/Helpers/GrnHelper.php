<?php


namespace Niyotail\Helpers;


use Barryvdh\Snappy\Facades\SnappyPdf;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceGrn;

class GrnHelper
{
    private static function getPdf(PurchaseInvoiceGrn $grn, PurchaseInvoice $invoice, $items = null)
    {
        if(empty($items))
        {
            throw new \Exception('No items assigned to GRN');
        }
        $invoice->load('createdBy', 'purchaseOrder.vendor','purchaseOrder.createdBy');

        $headerMargin = "90mm";

        $header = view()->make('pdf.goods_receipt_note.header', compact('grn', 'invoice'))->render();

        return SnappyPdf::loadView('pdf.goods_receipt_note.invoice_items',
                [
                    'grn' => $grn,
                    'invoice' => $invoice,
                    'items' => $items
                ]
            )
            ->setOption('margin-top', $headerMargin)
            ->setOption('margin-bottom', '65mm')
            ->setOption('header-html', $header)
            ->setOption('footer-center', utf8_decode('Page [page] of [topage]'))
            ->setPaper('A4')
            ->setOption('image-quality', 100);
    }

    public static function generatePdf(PurchaseInvoiceGrn $grn, PurchaseInvoice $invoice, $items)
    {
        $invoice->load('purchaseOrder');
        $pdf = self::getPdf($grn, $invoice, $items);
        return $pdf;
    }
}
