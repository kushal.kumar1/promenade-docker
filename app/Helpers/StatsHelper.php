<?php


namespace Niyotail\Helpers;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\CreditNote;
use Niyotail\Models\Invoice;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Shipment;
use function foo\func;

class StatsHelper
{
    public static function getOrderQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        $groupBySelectCol = "DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date";
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        $query = Order::selectRaw($groupBySelectCol.", count(distinct orders.id) as count, ceiling(sum(order_items.total)) as total");

        $query->whereNotIn("orders.status", $notAllowedStatuses)
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->when($isFiltered, function ($query) use ($filters) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id');
                return self::applyFilterQueries($query, $filters);
            })
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'");

        $query->groupBy($groupByStatement);

        return $query;
    }

    public static function getBrandsQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        $groupBySelectCol = "DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date";
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        $query = Order::selectRaw(
            $groupBySelectCol.", 
            count(distinct orders.id) as order_count,
            count(distinct brands.id) as brands_count,
            ceiling(sum(order_items.total)) as order_total
        ");

        $query->join('order_items', 'order_items.order_id', '=', 'orders.id')
        ->join('stores', 'stores.id', '=', 'orders.store_id')
        ->when($isFiltered, function ($query) use ($filters) {
            $query = $query
                ->join('products', 'products.id', '=', 'order_items.product_id')
                ->join('brands', 'brands.id', '=', 'products.brand_id');
            return self::applyFilterQueries($query, $filters);
        })
        ->whereNotIn("orders.status", $notAllowedStatuses)
        ->whereNotIn("order_items.status", $notAllowedStatuses)
        ->whereRaw("convert_tz(orders.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'");

        $query->groupBy($groupByStatement);

        return $query;
    }

    public static function getGrossRevenueQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        $groupBySelectCol = "DATE_FORMAT(date(convert_tz(invoices.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date";
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(invoices.created_at, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        $query = OrderItem::selectRaw(
            $groupBySelectCol.",
            count(distinct orders.id) as order_count, 
            ceiling(sum(order_items.total)) as revenue
        ");

        $query->join('shipment_items', 'shipment_items.order_item_id', '=', 'order_items.id')
        ->join('shipments', function($query) {
            $query->on('shipments.id', '=', 'shipment_items.shipment_id')->where('shipments.status', '!=', 'cancelled');
        })
        ->join('orders', 'orders.id', '=', 'shipments.order_id')
        ->join('stores', 'stores.id', '=', 'orders.store_id')
        ->join('invoices', function($query) use ($filters) {
            $query->on('invoices.shipment_id', '=', 'shipments.id')
                ->where('invoices.status', '!=', Invoice::STATUS_CANCELLED)
                ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'");
        })
        ->when($isFiltered, function ($query) use ($filters) {
            $query = $query
                ->join('products', 'products.id', '=', 'order_items.product_id')
                ->join('brands', 'brands.id', '=', 'products.brand_id');
            return self::applyFilterQueries($query, $filters);
        })
        ->whereNotIn("orders.status", $notAllowedStatuses)
        ->whereNotIn("order_items.status", $notAllowedStatuses);

        $query->groupBy($groupByStatement);

        return $query;
    }

    public static function getOrderItemUnitsQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        $groupBySelectCol = "DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date";
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(orders.created_at, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        return OrderItem::selectRaw($groupBySelectCol.",
                count(distinct orders.id) as order_count, 
                count(order_items.id) as units_count,
                count(distinct order_items.product_id) as unique_items
            ")->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('product_variants', 'product_variants.sku', '=', 'order_items.sku')
            ->when($isFiltered, function ($query) use ($filters) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id');
                return self::applyFilterQueries($query, $filters);
            })
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->whereNotIn("orders.status", $notAllowedStatuses)
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'")
            ->groupBy($groupByStatement);
    }

    public static function getRtoTotalsQuery($filters)
    {
        $notAllowedStatuses = self::getNotFulfilledStatuses();
        $isFiltered = self::checkIfFiltersApplied($filters);

        $groupBySelectCol = 'date(convert_tz(invoices.cancelled_on, "+00:00", "+05:30")) as date';
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(invoices.cancelled_on, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        return Shipment::selectRaw($groupBySelectCol.', 
            sum(invoices.amount) as rto_total,
            count(distinct orders.id) as order_count
        ')
            ->join('invoices', 'shipments.id', '=', 'invoices.shipment_id')
            ->join('orders', 'orders.id', '=', 'shipments.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->whereNotIn("orders.status", $notAllowedStatuses)
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->where('shipments.status', 'rto')
            ->whereRaw("convert_tz(invoices.cancelled_on, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'")
            ->when($isFiltered, function ($query) use ($filters) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id');
                return self::applyFilterQueries($query, $filters);
            })
            ->groupBy($groupByStatement);
    }

    public static function getReturnTotalQuery($filters)
    {
        $notAllowedStatuses = self::getNotFulfilledStatuses();
        $isFiltered = self::checkIfFiltersApplied($filters);

        $groupBySelectCol = "DATE_FORMAT(date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')), '%Y-%m-%d') as date";
        $groupByStatement = DB::raw("DATE_FORMAT(date(convert_tz(credit_notes.created_at, '+00:00', '+05:30')), '%Y-%m-%d')");
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        return CreditNote::selectRaw($groupBySelectCol.",
            count(distinct orders.id) as order_count,
            ceiling(sum(credit_notes.amount)) as return_total
        ")
            ->whereRaw("convert_tz(credit_notes.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and '".$filters['end_date']."'")
            ->join('orders', 'orders.id', '=', 'credit_notes.order_id')
            ->join('order_items', 'order_items.order_id', '=', 'orders.id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->whereNotIn("orders.status", $notAllowedStatuses)
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->when($isFiltered, function ($query) use ($filters) {
                $query = $query
                    ->join('products', 'products.id', '=', 'order_items.product_id')
                    ->join('brands', 'brands.id', '=', 'products.brand_id');
                return self::applyFilterQueries($query, $filters);
            })
            ->groupBy($groupByStatement);
    }

    public static function getTopBrandsQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        return OrderItem::selectRaw('
            brands.name as name, 
            count(distinct orders.id) as order_count,
            count(distinct order_items.product_id) as unique_items,
            sum(order_items.quantity) as total_items,
            ceiling(sum(order_items.total)) as order_total
        ')
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and  '".$filters['end_date']."'")
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->when($isFiltered, function ($query) use ($filters) {
                $query = $query
                    ->join('stores', 'stores.id', '=', 'orders.store_id');
                return self::applyFilterQueries($query, $filters);
            })
            ->groupBy('brand_id');
    }

    public static function getTopMarketersQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        $groupBySelectCol = "marketers.id as marketer_id, marketers.name as name";
        $groupByStatement = "marketer_id";
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.name as name, stores.lat as lat, stores.lng as lng, count(distinct marketers.id) as marketers_count";
            $groupByStatement = "stores.id";
        }

        return OrderItem::selectRaw($groupBySelectCol.',
            count(distinct order_items.order_id) as order_count,
            count(distinct order_items.product_id) as unique_items,
            sum(order_items.quantity) as total_items,
            ceiling(sum(order_items.total)) as order_total
        ')
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('brands', 'brands.id', '=', 'products.brand_id')
            ->join('marketers', 'marketers.id', '=', 'brands.marketer_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and  '".$filters['end_date']."'")
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->whereNotIn("orders.status", $notAllowedStatuses)
            ->when($isFiltered, function ($query) use ($filters) {
                return self::applyFilterQueries($query, $filters);
            })
            ->groupBy($groupByStatement);
    }

    public static function getTopStoresQuery($filters)
    {
        $isFiltered = self::checkIfFiltersApplied($filters);
        $notAllowedStatuses = self::getNotFulfilledStatuses();

        return OrderItem::selectRaw('
            stores.id as store_id,
            stores.name as name,
            stores.lat as lat, 
            stores.lng as lng,
            count(distinct order_items.order_id) as order_count,
            count(distinct order_items.product_id) as unique_items,
            sum(order_items.quantity) as total_items,
            ceiling(sum(order_items.total)) as order_total
        ')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->whereNotIn("orders.status", $notAllowedStatuses)
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '".$filters['start_date']."' and  '".$filters['end_date']."'")
            ->whereNotIn("order_items.status", $notAllowedStatuses)
            ->when($isFiltered, function ($query) use ($filters) {
                return self::applyFilterQueries($query, $filters);
            })
            ->groupBy('store_id');
    }

    public static function getNetRevenueQuery($filters)
    {
        $groupBySelectCol = "date(shipments.created_at) as shipment_date";
        $groupByStatement = DB::raw('date(shipments.created_at)');
        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $groupBySelectCol = "stores.id as store_id, stores.name as name, stores.lat as lat, stores.lng as lng";
            $groupByStatement = "stores.id";
        }

        $query = Shipment::selectRaw($groupBySelectCol.',
        	count(distinct orders.store_id) as unique_stores,
            count(distinct stores.beat_id) as unique_beats,
            count(distinct shipments.order_id) as total_orders,
            count(distinct order_items.product_id) as unique_products,
            sum(order_items.quantity) as total_item_units,
            COALESCE(sum(order_items.total),0) as item_total,
            COALESCE(sum(distinct total_credit_notes.total_cn),0) as total_credit_notes,
            COALESCE((COALESCE(sum(order_items.total),0) - COALESCE(sum(distinct total_credit_notes.total_cn),0)),0) as net_revenue
        ');

        $query->join('invoices', 'invoices.shipment_id', '=', 'shipments.id');;
        $query->join('orders', 'shipments.order_id', '=', 'orders.id');
        $query->join("stores", "orders.store_id", "=", "stores.id");
        $query->join('shipment_items', 'shipments.id', '=', 'shipment_items.shipment_id');
        $query->join('order_items', 'shipment_items.order_item_id', '=',  'order_items.id');

        if(!empty($filters['view-type']) && $filters['view-type'] == "map-view")
        {
            $query->join(DB::raw('(select orders.store_id as store_id, COALESCE(sum(cn.amount),0) as total_cn from credit_notes as cn left join orders as orders on cn.order_id = orders.id group by store_id) total_credit_notes'), 'total_credit_notes.store_id', '=', 'stores.id');
        }
        else
        {
            $query->join(DB::raw('(select date(cn.created_at) as date, COALESCE(sum(cn.amount),0) as total_cn from credit_notes as cn group by date) total_credit_notes'), 'total_credit_notes.date', '=', DB::raw('(date(shipments.created_at))'));
        }

        // store_id || beat_id
        if(!empty($filters['store_ids']))
        {
            $query->whereIn("stores.id", $filters['store_ids']);
        }
        if(!empty($filters['beat_ids']))
        {
            $query->whereIn("stores.beat_ids", $filters['beat_ids']);
        }

        // product_id || marketer_id || brand_id || collection_id
        if(!empty($filters['product_ids']))
        {
            $query->whereIn("order_items.product_id", $filters['product_ids']);
        }

        if(!empty($filters['brand_ids']) || !empty($filters['collection_ids']) || !empty($filters['marketer_ids']))
        {
            $query->join('products', 'order_items.product_id', '=', 'order_items.product_id');

            if(!empty($filters['brand_ids']))
            {
                $query->whereIn("products.brand_id", $filters['brand_ids']);
            }

            if(!empty($filters['marketer_ids']))
            {
                $query->join('brands', 'products.brand_id', '=', 'brands.id');
                $query->whereIn("brands.marketer_id", $filters['marketer_ids']);
            }

            if(!empty($filters['collection_ids']))
            {
                $query->join("collection_product", "products.id", '=', 'collection_product.product_id');
                $query->whereIn("collection_product.collection_id", $filters['collection_ids']);
            }
        }

        $query->where("shipments.status", '!=', 'cancelled');
        $query->whereRaw('1 = case when date(invoices.cancelled_on) > "'.$filters['end_date'].'" then 1 when date(invoices.cancelled_on) < "'.$filters['start_date'].'" then 1 when invoices.cancelled_on is null then 1 else 0 end');
        $query->whereRaw("date(shipments.created_at) >= '".$filters['start_date']."' and date(shipments.created_at) <= '".$filters['end_date']."'");
        $query->groupBy($groupByStatement);

        return $query;
    }

    private static function applyFilterQueries(Builder $query, $filters): Builder {

        return $query->when( $filters['store_type'] === '1k', function ($query) {
                return $query->whereIn('orders.store_id', self::getoneKStores());
            })
            ->when($filters['store_type'] === 'retail', function ($query) {
                return $query->whereNotIn('orders.store_id', self::getoneKStores());
            })
            ->when(!empty($filters['beat_ids']), function ($query) use ($filters) {
                return $query->whereIn('stores.beat_id', $filters['beat_ids']);
            })
            ->when(!empty($filters['brand_ids']), function ($query) use ($filters) {
                return $query->whereIn('products.brand_id', $filters['brand_ids']);
            })
            ->when(!empty($filters['store_ids']), function ($query) use ($filters) {
                return $query->whereIn('orders.store_id', $filters['store_ids']);
            })
            ->when(!empty($filters['product_ids']), function ($query) use ($filters) {
                return $query->whereIn('order_items.product_id', $filters['product_ids']);
            })
            ->when(!empty($filters['collection_ids']), function ($query) use ($filters) {
                return $query->join('collection_product', function($q) {
                    $q->on('collection_product.product_id', '=', 'products.id')->limit(1);
                })->whereIn('collection_product.collection_id', $filters['collection_ids']);
            })
            ->when(!empty($filters['marketer_ids']), function ($query) use ($filters) {
                return $query->whereIn('brands.marketer_id', $filters['marketer_ids']);
            });
    }

    private static function getoneKBeats(): array {
        return cache()->remember('onek.beat.ids', 30, function() {
            return DB::table('beat_warehouse')->select('beat_id')->distinct()->get()->pluck('beat_id')->toArray();
        });
    }

    private static function getoneKStores(): array {
        return cache()->remember('onek.store.ids', 30, function() {
            return DB::table('stores')->select('id')->whereIn('beat_id', self::getoneKBeats())->get()->pluck('id')->toArray();
        });
    }

    private static function getNotFulfilledStatuses()
    {
        return array(
            OrderItem::STATUS_CANCELLED,
            OrderItem::STATUS_MANIFESTED,
            OrderItem::STATUS_RETURNED,
            OrderItem::STATUS_RTO,
        );
    }

    private static function checkIfFiltersApplied($filters)
    {
        return
            !empty($filters['store_ids']) ||
            !empty($filters['marketer_ids']) ||
            !empty($filters['collection_ids']) ||
            !empty($filters['product_ids']) ||
            !empty($filters['brand_ids']) ||
            !empty($filters['beat_ids']) ||
            !empty($filters['store_type']);
    }
}