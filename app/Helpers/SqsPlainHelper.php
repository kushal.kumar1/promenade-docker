<?php

namespace Niyotail\Helpers;

use Illuminate\Support\Facades\Queue;

class SqsPlainHelper
{
    public static function dispatchJobWithData($data, $method) {
        $reqBody = json_encode(self::getRequestBody($data, $method));
        Queue::connection('sqs-plain')->pushRaw($reqBody);
    }

    private static function getRequestBody($data, $method) {
        return [
            "method" => $method,
            "payload" => $data,
        ];
    }
}