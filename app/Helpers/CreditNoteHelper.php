<?php

namespace Niyotail\Helpers;

use Niyotail\Models\CreditNote;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Illuminate\Support\Facades\Storage;

class CreditNoteHelper
{
    private static function getPdf(CreditNote $creditNote)
    {
        $creditNote->load('order.shippingAddress', 'order.billingAddress', 'returnOrder.orderItems.taxes', 'order.store.beat', 'returnOrder.orderItems.productVariant', 'returnOrder.shipment.invoice');
        $header = view()->make('pdf.credit_note.header', compact('creditNote'))->render();
        $footer = view()->make('pdf.credit_note.footer', compact('creditNote'))->render();

        $cnData = self::getFormattedData($creditNote);

        return SnappyPdf::loadView('pdf.credit_note.sales', [
            'creditNote' => $creditNote,
            'rows' => $cnData['rows'],
            'taxes' => $cnData['taxes']
        ])
          ->setOption('margin-top', '70mm')
          ->setOption('margin-bottom', '60mm')
          ->setOption('header-html', $header)
          // ->setOption("footer-center", "Page [page] from [topage]")
          ->setOption('footer-html', $footer)
          ->setPaper('A4')
          ->setOption('image-quality', 100);
    }

    private static function getFormattedData(CreditNote $creditNote)
    {
        $returnOrder = $creditNote->returnOrder;
        $returnOrderItems = $returnOrder->items;
        $orderItems = $returnOrder->orderItems;

        $taxes = [];
        $allTaxes=$orderItems->pluck('taxes')->flatten();
        if($allTaxes->isNotEmpty())
        {
            $taxes = $allTaxes->groupBy('name')->keys()->all();
        }

        $formattedItems = [];

        foreach ($returnOrderItems as $returnItem)
        {
            $orderItem = $returnItem->orderItem;
            $variant = $orderItem->productVariant;
            $product = $variant->product;

            $refund = ($returnItem->units/$variant->quantity)*$orderItem->total;
            $subtotal = ($returnItem->units/$variant->quantity)*$orderItem->subtotal;
            $discount = ($returnItem->units/$variant->quantity)*$orderItem->discount;
            $returnRatio = $returnItem->units/$variant->quantity;
            $isPartialReturn = $returnRatio != 1;
            $mrp = $isPartialReturn ? ($orderItem->total/$variant->quantity) : $orderItem->mrp;
            $tax = $isPartialReturn ? ($orderItem->tax/$variant->quantity) : $orderItem->tax;
            $returnType = $isPartialReturn ? 'partial' : 'full';
            $qty = $returnRatio == 1 ? 1 : $returnItem->units;

            $formattedItem = [
                "key" => $product->id."-".$variant->sku."-".$returnType.'-'.$qty,
                "name" => $product->name,
                "hsn_sac_code" => $product->hsn_sac_code,
                "sku" => $variant->sku,
                "type" => $variant->value,
                "uom" => $product->uom,
                "return_ratio" => $returnRatio,
                "is_partial_return" => $returnRatio != 1,
                "quantity" => $qty,
                "total" => $refund,
                "mrp" => $mrp,
                "price" => $mrp - $tax,
                "subtotal" => $subtotal,
                "discount" => $discount,
            ];

            $orderItemTaxes = $orderItem->taxes;
            foreach ($orderItemTaxes as $orderItemTax) {
                $taxName = $orderItemTax->name;
                $taxArr = [
                    "percentage" => $orderItemTax->percentage,
                    "amount" => $returnRatio*$orderItemTax->amount,
                ];
                $formattedItem[$taxName] = $taxArr;
            }

            $formattedItems[] = $formattedItem;
        }

        $formattedItems = collect($formattedItems)->groupBy('key');

        $formattedItems = $formattedItems->map(function($items, $key) use ($taxes) {
            $firstItem = $items->first();

            $arr = [
                "name" => $firstItem['name'],
                "hsn_sac_code" => $firstItem['hsn_sac_code'],
                "sku" => $firstItem['sku'],
                "type" => $firstItem['type'],
                "uom" => $firstItem['uom'],
                "return_ratio" => $firstItem['return_ratio'],
                "is_partial_return" => $firstItem['is_partial_return'],
                "quantity" => $items->sum('quantity'),
                "total" => $items->sum('total'),
                "mrp" => $items->sum('mrp'),
                "price" => $items->sum('price'),
                "subtotal" => $items->sum('subtotal'),
                "discount" => $items->sum('discount'),
            ];

            $totalTax = 0;
            foreach ($taxes as $tax)
            {
                $percentage = '';
                $amount = 0;

                foreach ($items as $item)
                {
                    if(isset($item[$tax]))
                    {
                        $percentage = $item[$tax]['percentage'];
                        $amount += $item[$tax]['amount'];
                    }
                }

                $arr[$tax] = [
                    "percentage" => $percentage,
                    "amount" => $amount
                ];

                $totalTax += $amount;
            }

            $arr['tax'] = $totalTax;

            return $arr;
        });

        return [
            "rows" => $formattedItems,
            "taxes" => $taxes
        ];
    }

    public static function generatePdf(CreditNote $creditNote)
    {
        $pdf = self::getPdf($creditNote)->output();
        $path = "credit/$creditNote->financial_year/$creditNote->number.pdf";
        $disk = config('filesystems.credit_note_file_driver');
        Storage::disk($disk)->put($path, $pdf);
    }

    public static function streamPdf(CreditNote $creditNote)
    {
        $path = "credit/$creditNote->financial_year/$creditNote->number.pdf";
        $disk = config('filesystems.credit_note_file_driver');
        if(!Storage::disk($disk)->exists($path)){
            self::generatePdf($creditNote);
        }
        return Storage::disk($disk)->temporaryUrl($path, now()->addMinutes(15));
    }

    public static function getFinancialYear($date)
    {
        $creationDateTime = new \DateTime($date);
        $year = $creationDateTime->format("y");
        $month = $creationDateTime->format("n");
        if ($month < 4) {
            return ($year - 1) . '-' . $year;
        }
        return $year . '-' . ($year + 1);
    }

    public static function downloadCreditNote(CreditNote $creditNote)
    {
        $path = "credit/$creditNote->financial_year/$creditNote->number.pdf";
        $disk = config('filesystems.credit_note_file_driver');
        if(!Storage::disk($disk)->exists($path)){
            self::generatePdf($creditNote);
        }
        return Storage::disk($disk)->download($path);
    }
}
