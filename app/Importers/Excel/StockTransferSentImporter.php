<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Models\StockTransferItem;
use Niyotail\Services\StockTransferService;

class StockTransferSentImporter extends Importer
{

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $transferService = new StockTransferService();
        foreach ($rows as $row) {
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            $stockTransferItem = StockTransferItem::find($row['item_id']);

            if (empty($stockTransferItem)) {
                $data = $row;
                $data['error'] = "Invalid Item id";
                $this->addOutputRow($data);
                continue;
            }
            try {
                $transferService->addSentQuantity($stockTransferItem, $request);
            } catch (ServiceException $exception) {
                $data = $row;
                $data['error'] = $exception->getMessage();
                $this->addOutputRow($data);
            }
        }
    }
}