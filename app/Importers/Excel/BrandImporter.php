<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\BrandRequest;
use Niyotail\Services\BrandService;

class BrandImporter extends Importer
{

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $brandService = new BrandService();
        foreach ($rows as $row) {
            $row = $row->toArray();
            if ($row['visible']) {
                $row['visible'] = strtolower($row['visible']) == 'yes';
            }
            if ($row['status']) {
                $row['status'] = strtolower($row['status']) == 'yes';
            }
            $request = new Request();
            $request->replace($row);
            if (!$this->validate($request->all())) {
                continue;
            }
            try {
                if ($request->filled('id')) {
                    $brandService->update($request);
                } else {
                    $brandService->create($request);
                }
            } catch (ServiceException $exception) {
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
        }
    }

    protected function rules(): array
    {
        return (new BrandRequest())->rules();
    }
}