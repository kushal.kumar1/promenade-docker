<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Models\Product;
use Niyotail\Models\StockTransfer;
use Niyotail\Services\StockTransferService;

class StockTransferOutboundImporter extends Importer
{

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $transferService = new StockTransferService();
        foreach ($rows as $row){
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            $stockTransfer = StockTransfer::find($row['transfer_id']);

            if(empty($stockTransfer)){
                $data = $row;
                $data['error'] = "Invalid transfer id";
                $this->addOutputRow($data);
                continue;
            }
            $product = Product::find($row['product_id']);
            if(empty($product)){
                $data = $row;
                $data['error'] = "Invalid product id";
                $this->addOutputRow($data);
                continue;
            }

            $transferService->addItem($stockTransfer, $request);
        }
    }
}