<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\ProductGroupRequest;
use Niyotail\Services\ProductGroupService;

class ProductGroupImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $productGroupService = new ProductGroupService();
        foreach ($rows as $row) {
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if (!$this->validate($request->all())) {
                continue;
            }
            try {
                if ($request->filled('id')) {
                    $productGroupService->update($request->id, $request, $this->importer->employee);
                } else {
                    $productGroupService->create($request, $this->importer->employee);
                }
            } catch (ServiceException $exception) {
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
        }
    }

    protected function rules(): array
    {
        return (new ProductGroupRequest())->rules();
    }
}