<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Http\Requests\Importer\NewCatL3Request;
use Niyotail\Services\NewCatService;

class NewCatL3Importer extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $newCatService = new NewCatService();
        foreach ($rows as $row){
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if(!$this->validate($request->all())){
                continue;
            }
            $newCatService->createCl3($request);
        }
    }

    protected function rules(): array
    {
        return (new NewCatL3Request())->rules();
    }
}