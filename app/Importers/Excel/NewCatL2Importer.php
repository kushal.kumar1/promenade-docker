<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Http\Requests\Importer\NewCatL2Request;
use Niyotail\Services\NewCatService;

class NewCatL2Importer extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $newCatService = new NewCatService();
        foreach ($rows as $row){
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if(!$this->validate($request->all())){
                continue;
            }
            $newCatService->createCl2($request);
        }
    }

    protected function rules(): array
    {
        return (new NewCatL2Request())->rules();
    }
}