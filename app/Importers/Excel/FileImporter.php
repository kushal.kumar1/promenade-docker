<?php
namespace Niyotail\Importers\Excel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class FileImporter implements ToCollection, WithHeadingRow
{
    public function collection(Collection $excel)
    {
    }

    public function chunkSize(): int
    {
        return 100;
    }
}
