<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Http\Requests\Importer\ProductDemandRequest;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\ProductGroup;
use Niyotail\Services\ProductDemandService;

class ProductDemandImporter extends Importer
{

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $demandService = new ProductDemandService();
        $groupedRows = $rows->groupBy('channel');
        $hasError = false;
        foreach ($groupedRows as $rows) {
            $demand = null;
            foreach ($rows as $row) {
                $row = $row->toArray();
                $hasError = $this->validate($row);
            }
            if(!$hasError) {
                foreach ($rows as $row) {
                    $row = $row->toArray();
                    $row['source'] = ProductDemand::SOURCE_IMPORTER;
                    $row['created_by'] = $this->importer->employee;
                    $row['status'] = ProductDemand::STATUS_FINALIZED;
                    $request = new Request();
                    $request->replace($row);
                    ProductGroup::findOrFail($request->group_id);
                    if (empty($demand)) {
                        $demand = $demandService->saveDemand($request);
                    } else {
                        $demand = $demandService->saveDemand($request, $demand->id);
                    }
                }
            }
        }
    }

    protected function rules(): array
    {
        return (new ProductDemandRequest())->rules();
    }
}