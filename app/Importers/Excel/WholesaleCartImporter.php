<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\Cart\WholesaleCartRequest;
use Niyotail\Models\Cart;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\Order;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class WholesaleCartImporter extends Importer
{

    private $cart = null;

    protected function onLoaded()
    {
        $data = $this->excel->first();
        $rowsGroupedByStoreId = $data->groupBy('store_id');
        foreach ($rowsGroupedByStoreId as $storeId => $rows) {
            foreach ($rows as $originalRow) {
                $row = $originalRow->toArray();
                if (!empty($row['sku'])) {
                    $sku = $row['sku'];
                    $row['sku'] = "$sku";
                }
                if (!$this->validate($row)) {
                    continue;
                }
                $productVariant = ProductVariant::where('sku', $row['sku'])->with('product')->withTrashed()->first();

                if(!$this->isValidPriceAndActiveProduct($productVariant,$row)){
                    continue;
                }
                $user = auth()->user();
                $hasAccess = !empty($user->warehouses->where('id', $row['warehouse_id'])->first());
                if(!$hasAccess){
                    $data = $row;
                    $data['error'] = "You don't have the access to selected warehouse";
                    $this->addOutputRow($data);
                    continue;
                }

                $row['id'] = $productVariant->id;
                $row['source'] = Cart::SOURCE_WHOLESALE_CART_IMPORTER;
                $additionalInfo = null;
                if (!empty($row['tsm_number'])) {
                    $mobile = $row['tsm_number'];
                    $mobile = Str::startsWith($mobile, '+91') ? $mobile : '+91'.$mobile;
                    $additionalInfo['tsm_number'] = $mobile;
                    if(isset($row['pos_order_id'])){
                        $additionalInfo['pos_order_id'] = $row['pos_order_id'];
                        $storeOrderService = new StoreOrderService();
                        $customerInfo = $storeOrderService->getMallOrderCustomerInfo($additionalInfo['pos_order_id']);
                        $additionalInfo = array_merge($additionalInfo, $customerInfo);
                    }
                }
                $row['additional_info'] = $additionalInfo;

                $request = new Request();
                $request->replace($row);
                $cartService = new CartService();
                try {
                    if (empty($this->cart)) {
                        $this->cart = $cartService->addToCart($request, $this->importer->employee);
                    } else {
                        $cartService->setCart($this->cart);
                        $cartService->addToCart($request, $this->importer->employee);
                    }

                } catch (ServiceException $serviceException) {
                    $data = $row;
                    $data['error'] = $serviceException->getMessage();
                    $this->addOutputRow($data);
                }
            }
            if (!empty($this->cart)) {
                $storeOrderService = new StoreOrderService();
                $storeOrderService->create($this->cart);
                $this->cart = null;
            }
        }
    }

    protected function rules(): array
    {
        return (new WholesaleCartRequest())->rules();
    }

    private function isValidPriceAndActiveProduct(ProductVariant $productVariant, array $row): bool
    {
        if (empty($productVariant->product->status)) {
            $data = $row;
            $data['error'] = 'Product not active';
            $this->addOutputRow($data);
            return false;
        }
        if ($productVariant->mrp < $row['price']) {
            $data = $row;
            $data['error'] = 'Price cannot be greater than MRP';
            $this->addOutputRow($data);
            return false;
        }

        if ($productVariant->trashed()) {
            $data = $row;
            $data['error'] = "Product variant with SKU " . $row['sku'] . " not found";
            $this->addOutputRow($data);
            return false;
        }
        return true;
    }
}
