<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Admin\PurchaseInvoice\CreateRequest;
use Niyotail\Services\PurchaseInvoiceService;

class PurchaseInvoiceImporterImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $inventoryService = new PurchaseInvoiceService();
        foreach ($rows as $originalRow) {
            $row = $originalRow->toArray();
            if (!empty($sku = $row['sku']))
                $row['sku'] = "$sku";
            if (!$this->validate($row)) {
                continue;
            }
            $request = new Request();
            $request->replace($row);
            try {
                $inventoryService->createOrUpdate($request);
            } catch (ServiceException $serviceException) {
                $data = $originalRow;
                $data['error'] = $serviceException->getMessage();
                $this->addOutputRow($data);
            }
        }
    }

    protected function rules()
    {
        return (new CreateRequest())->rules();
    }

    protected function startRow()
    {
        return 2;
    }

}
