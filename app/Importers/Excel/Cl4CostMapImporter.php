<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\Cl4CostMapRequest;
use Niyotail\Services\Cl4CostMapService;

class Cl4CostMapImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $cl4costMapService = new Cl4CostMapService();
        foreach ($rows as $row){
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if(!$this->validate($request->all())){
                continue;
            }
            try{
                $cl4costMapService->create($request);
            }catch (ServiceException $exception){
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
        }
    }

    protected function rules(): array
    {
        return (new Cl4CostMapRequest())->rules();
    }
}