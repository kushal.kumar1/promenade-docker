<?php

namespace Niyotail\Importers\Excel;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Niyotail\Http\Requests\Importer\PurchaseInvoiceCart\PurchaseInvoiceCartRequest;
use Niyotail\Models\ProductVariant;
use Niyotail\Models\PurchaseInvoiceCart;
use Niyotail\Services\PurchaseInvoiceCartItemService;
use Niyotail\Services\PurchaseInvoiceCartService;

class PurchaseInvoiceCartImporter extends Importer
{
    private $vendorId;
    private $vendorAddressId;
    private $invoiceDate;
    private $vendorRefId;
    private $cart;

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $cartItemService = new PurchaseInvoiceCartItemService();
        foreach ($rows as $row) {
            if (!empty($row['is_direct_store_purchase'])) {
                $row['is_direct_store_purchase'] = $row['is_direct_store_purchase'] == 'yes';
            }

            if (!empty($row['is_local_purchase'])) {
                $row['is_local_purchase'] = $row['is_local_purchase'] == 'yes';
            }

            if ($row['is_local_purchase']) {
                $row['is_direct_store_purchase'] = 1;
            }
            if(!empty($this->cart)){
                $this->cart->refresh();
            }
            $row['source'] = PurchaseInvoiceCart::SOURCE_IMPORTER;
            $request = new Request();
            $request->replace($row->toArray());
            if($this->isNewCart($request)){
                $this->cart = $this->getPurchaseInvoiceCart($request);
            }
            if (!empty($this->cart) && ($this->cart->status != PurchaseInvoiceCart::STATUS_DRAFT)) {
                $data = $row->toArray();
                $data['error'] = "Purchase Invoice already exists #{$this->cart->vendor_ref_id}";
                $this->addOutputRow($data);
                continue;
            }
            if (!$this->validate($request->all())) {
                continue;
            }
            $productVariant = ProductVariant::where('sku', $request->sku)->withTrashed()->first();
            if (empty($productVariant)) {
//            if ($productVariant->trashed()) {
                $data = $row->toArray();
                $data['error'] = "Product variant with SKU " . $request->sku . " not found";
                $this->addOutputRow($data);
                continue;
            }
            $request->merge(['product_id' => $productVariant->product_id, 'value_type' => 'total_units', 'price_type' => 'all_inclusive', 'discount_type' => 'flat', 'platform_id' => 16]);

            if (empty($this->cart)) {
                $this->cart = $this->createPurchaseInvoiceCart($request);
            }
            $cartItemService->addItem($request, $this->cart);
        }
    }

    private function getPurchaseInvoiceCart(Request $request)
    {
        $findBy = [
            'vendor_id' => $request->vendor_id,
            'vendor_address_id' => $request->vendor_address_id,
            'invoice_date' => Carbon::parse($request->invoice_date)->toDateString(),
            'vendor_ref_id' => $request->vendor_ref_id
        ];

        return PurchaseInvoiceCart::where($findBy)->first();
    }

    private function createPurchaseInvoiceCart(Request $request): PurchaseInvoiceCart
    {
        $purchaseInvoiceCartService = new PurchaseInvoiceCartService();
        $cart = $purchaseInvoiceCartService->createVendorCart($request->vendor_id);
        return $purchaseInvoiceCartService->updatePurchaseInvoiceCart($request, $cart);
    }

    private function isNewCart(Request $request): bool
    {
        if (empty($this->vendorId)) {
            $this->vendorId = $request->vendor_id;
            $this->vendorAddressId = $request->vendor_address_id;
            $this->vendorRefId = $request->vendor_ref_id;
            $this->invoiceDate = $request->invoice_date;
            return true;
        }
        if (($this->vendorRefId != $request->vendor_id && $this->vendorAddressId != $request->vendor_address_id) && ($this->vendorRefId != $request->vendor_ref_id && $this->invoiceDate != $request->invoice_date)) {
            return true;
        }
        return false;
    }

    protected function rules(): array
    {
        return (new PurchaseInvoiceCartRequest())->rules();
    }
}