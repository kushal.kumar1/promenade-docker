<?php

namespace Niyotail\Importers\Excel;

use Carbon\Carbon;
use Carbon\Exceptions\InvalidDateException;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Request;
use Niyotail\Http\Requests\Importer\VendorMarketerMapRequest;
use Niyotail\Services\VendorService;

class VendorMarketerMappingImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $vendorService = new VendorService();
        foreach ($rows as $originalRow) {
            $row = $originalRow->toArray();
            $row = $this->sanitizeInputs($row);
            try {
                if (array_key_exists('date_from', $row)) {
                    $row['date_from'] = Carbon::parse($row['date_from'])->toDateString();
                }
                if (array_key_exists('date_to', $row)) {
                    $row['date_to'] = Carbon::parse($row['date_to'])->toDateString();
                }
            } catch (\Exception | InvalidFormatException $e) {
                $row['error'] = 'Invalid date format';
                $this->addOutputRow($row);
                continue;
            }
            $request = new Request();
            $request->replace($row);
            if (!$this->validate($request->all())) {
                continue;
            }
            $vendorService->saveVendorMarketerMap($request);
        }
    }

    public function rules(): array
    {
        return (new VendorMarketerMapRequest())->rules();
    }

    private function sanitizeInputs(array &$row)
    {
        if (empty($row['mov'])) {
            $row['mov'] = 0;
        }

        if (empty($row['lead_time'])) {
            $row['lead_time'] = 0;
        }

        if (empty($row['credit_term'])) {
            $row['credit_term'] = 0;
        }

        if (empty($row['cash_discount'])) {
            $row['cash_discount'] = 0;
        }

        if (!empty($row['tot_signed'])) {
            $row['tot_signed'] = $row['tot_signed'] == 1;
        } else {
            $row['tot_signed'] = 0;
        }

        if (!empty($row['status'])) {
            $row['status'] = $row['status'] == 1;
        } else {
            $row['status'] = 1;
        }

        if (empty($row['priority'])) {
            $row['priority'] = 1;
        }

        return $row;
    }
}