<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Niyotail\Models\ImporterAction;
use Niyotail\Services\ImporterActionService;

abstract class Importer
{
    protected $excel;
    protected $importer;
    private $file;
    private $outputRows = [];
    private $resource = null;

    public function __construct(ImporterAction $importerAction)
    {
        $this->importer = $importerAction;
        $this->file = $importerAction->input;
    }

    public function chunk($size, $function)
    {
        return $this->excel->chunk($size, $function, false);
    }

    public function start()
    {
        $this->load();
        $this->onLoaded();
        $this->onCompleted();
    }

    private function load()
    {
        $filePath = public_path("importer") . "/" . $this->getFile();
        $this->excel = Excel::toCollection(new FileImporter, $filePath);
        return $this;
    }

    private function getFile()
    {
        return $this->file;
    }

    abstract protected function onLoaded();

    protected function onCompleted()
    {
        $fileName = $this->generateOutput();
        (new ImporterActionService())->setCompleted($this->importer, $fileName, $this->resource);
    }

    private function generateOutput()
    {
        $fileName = null;
        $rows = $this->outputRows;
        if (!empty($rows)) {
            $headings = array_keys($rows[0]);
            $fileName = "output-" . $this->getFile();
            $export = new FileExporter($rows, $headings);
            Excel::store($export, $fileName, 'importer');
        }

        return $fileName;
    }

    protected function setResource(Model $resource)
    {
        $this->resource = $resource;
    }

    protected function validate($data)
    {
        $validator = Validator::make($data, $this->rules());
        if ($validator->fails()) {
            $errors = implode(' , ', Arr::flatten($validator->errors()->all()));
            $data['error'] = $errors;
            $this->addOutputRow($data);
            return false;
        }
        return true;
    }

    protected function rules()
    {
        return [];
    }

    protected function addOutputRow($row)
    {
        $this->outputRows[] = $row;
        return $this;
    }
}
