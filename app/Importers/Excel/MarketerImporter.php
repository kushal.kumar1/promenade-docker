<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\MarketerRequest;
use Niyotail\Services\MarketerService;

class MarketerImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $marketerService = new MarketerService();
        foreach ($rows as $row) {
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if (!$this->validate($request->all())) {
                continue;
            }
            try {
                if ($request->filled('id')) {
                    $marketerService->update($request);
                } else {
                    $marketerService->create($request);
                }
            } catch (ServiceException $exception) {
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
        }
    }

    protected function rules(): array
    {
        return (new MarketerRequest())->rules();
    }
}