<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Support\Facades\Storage;
use Niyotail\Helpers\InvoiceHelper;
use Niyotail\Models\Invoice;
use Niyotail\Models\WarehouseScope;
use ZipStream\Option\Archive;
use ZipStream\ZipStream;

class InvoiceZipImporter extends Importer
{

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $options = new Archive();
        $fileName = "invoice-" . time() . ".zip";
        $tempStream = fopen('php://memory', 'w+');
        $options->setOutputStream($tempStream);
        $zip = new ZipStream("invoice-" . time() . ".zip", $options);
        $disk = config('filesystems.invoice_file_driver');
        foreach ($rows as $row) {
            $row = $row->toArray();
            $invoice = Invoice::withoutGlobalScope(WarehouseScope::class)
                ->where('reference_id', $row['ref_id'])->first();
            if (empty($invoice)) {
                $row['error'] = 'Invoice not found!';
                $this->addOutputRow($row);
                continue;
            }
            $path = "invoices/$invoice->financial_year/$invoice->number.pdf";
            if (!Storage::disk($disk)->exists($path)) {
                InvoiceHelper::generatePdf($invoice);
            }
            $file = Storage::disk($disk)->get($path);
            $zip->addFile(str_replace('/', '-', $invoice->reference_id).".pdf", $file);
        }
        $zip->finish();
        Storage::disk($disk)->put("invoices/zips/$fileName", $tempStream);
        fclose($tempStream);
        $row['download_path'] = Storage::disk($disk)->temporaryUrl("invoices/zips/$fileName", now()->addMinutes(15));
        $this->addOutputRow($row);
    }
}
