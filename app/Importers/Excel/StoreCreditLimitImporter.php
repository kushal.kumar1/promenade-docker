<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\StoreCreditLimitRequest;
use Niyotail\Services\CreditLimitService;
use Niyotail\Services\StoreService;

class StoreCreditLimitImporter extends Importer
{

    /**
     * @throws ServiceException
     */
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $creditLimitService = new CreditLimitService();
        $storeService = new StoreService();
        foreach ($rows as $row) {
            $row = $row->toArray();
            if (!$this->validate($row)) {
                continue;
            }
            $data['id'] = $row['store_id'];
            $data['value'] = $row['credit_limit'];
            $data['type'] = "manual";
            $request = new Request();
            $request->replace($data);
            $storeService->setIsCreditAllowed($request->id,true);
            $creditLimitService->override($request);
        }
    }

    protected function rules(): array
    {
        return (new StoreCreditLimitRequest())->rules();
    }
}