<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Helpers\Integration\ProductSync;
use Niyotail\Http\Requests\Admin\Product\CreateRequest;
use Niyotail\Services\ProductService;

class ProductImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        foreach ($rows as $row) {
            $row = $row->toArray();
            if (!empty($row['tag_ids'])) {
                $row['tag_ids'] = explode(',', $row['tag_ids']);
            }
            $request = new Request();
            $request->replace($row);
            $product = null;
            $data = $row;
            if (!$this->validate($data)) {
                continue;
            }
            try {
                if (empty($row->id)) {
                    $product = (new ProductService)->createImportData($request);
                } else {
                    $product = (new ProductService)->updateImportData($request);
                }
            } catch (ServiceException $exception) {
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
            if (!empty($product->status)) {
                ProductSync::push($product);
            }
        }
    }

    protected function rules()
    {
        return (new CreateRequest())->rules();
    }
}
