<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\Cart\AddItemRequest;
use Niyotail\Models\CartItem;
use Niyotail\Models\ProductVariant;
use Niyotail\Services\Cart\CartService;
use Niyotail\Services\StoreOrder\StoreOrderService;

class CartImporter extends Importer
{
    private $cart = null;

    protected function onLoaded()
    {
        $data = collect($this->excel->first());
        $rowsGroupedByStoreId = $data->groupBy('store_id');
        foreach ($rowsGroupedByStoreId as $storeId => $rows) {
            foreach ($rows as $originalRow) {
                $row = $originalRow->toArray();
                if (!empty($row['sku'])) {
                    $sku = $row['sku'];
                    $row['sku'] = "$sku";
                }
                if (!$this->validate($row)) {
                    continue;
                }
                $productVariant = ProductVariant::where('sku', $row['sku'])->with('product')->withTrashed()->first();

                if($productVariant->trashed()){
                    $data = $row;
                    $data['error'] = "Product variant with SKU " . $row['sku'] . " not found";
                    $this->addOutputRow($data);
                    continue;
                }

                $user = auth()->user();
                $hasAccess = !empty($user->warehouses->where('id', $row['warehouse_id'])->first());
                if(!$hasAccess){
                    $data = $row;
                    $data['error'] = "You don't have the access to selected warehouse";
                    $this->addOutputRow($data);
                    continue;
                }
                if (empty($productVariant->product->status)) {
                    $data = $row;
                    $data['error'] = 'Product not active';
                    $this->addOutputRow($data);
                    continue;
                }
                $row['id'] = $productVariant->id;
                $row['source'] = CartItem::SOURCE_IMPORTER;
                $request = new Request();
                $request->replace($row);
                $cartService = new CartService();
                try {
                    if (empty($this->cart)) {
                        $this->cart = $cartService->addToCart($request, $this->importer->employee);
                    } else {
                        $cartService->setCart($this->cart);
                        $cartService->addToCart($request, $this->importer->employee);
                    }

                } catch (ServiceException $serviceException) {
                    $data = $row;
                    $data['error'] = $serviceException->getMessage();
                    $this->addOutputRow($data);
                }
            }
            if (!empty($this->cart)) {
                $storeOrderService = new StoreOrderService();
                $storeOrderService->create($this->cart);
                $this->cart = null;
            }
        }
    }

    protected function rules()
    {
        return (new AddItemRequest())->rules();
    }
}
