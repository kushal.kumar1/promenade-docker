<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Exceptions\ServiceException;
use Niyotail\Http\Requests\Importer\WarehouseStorageRequest;
use Niyotail\Services\StorageService;

class WarehouseStorageImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        $storageService = new StorageService();
        foreach ($rows as $row){
            $row = $row->toArray();
            $request = new Request();
            $request->replace($row);
            if(!$this->validate($request->all())){
                continue;
            }
            try{
                $storageService->create($request);
            }catch (ServiceException $exception){
                $row['error'] = $exception->getMessage();
                $this->addOutputRow($row);
            }
        }
    }

    protected function rules(): array
    {
        return (new WarehouseStorageRequest())->rules();
    }
}