<?php

namespace Niyotail\Importers\Excel;

use Illuminate\Http\Request;
use Niyotail\Services\ProductService;
use Niyotail\Importers\Excel\Importer;
use Niyotail\Services\ProductVariantService;
use Niyotail\Models\ProductVariant;

class ProductVariantImporter extends Importer
{
    protected function onLoaded()
    {
        $rows = $this->excel->first();
        foreach ($rows as $row) {
            $request = new Request();
            $data = $this->getVariantData($row);
            $request->replace($data);
            $productVariantService = new ProductVariantService();
            try {
                if (empty($data['id'])) {
                    $productVariantService->create($row->product_id, $request);
                } else {
                    $productVariantService->update($request);
                }
            } catch (ServiceException $exception) {
                $this->addOutputRow($row->toArray());
            }
        }
    }

    protected function getVariantData($row)
    {
        $data = $row->toArray();
        $variant = ProductVariant::withTrashed()->where('sku', $row->sku);
        if (!empty($row->product_id)) {
            $variant = $variant->where('product_id', $row->product_id);
        }

        $variant = $variant->first();
        if (!empty($variant)) {
            $data['id'] = $variant->id;
        }

        return $data;
    }
}
