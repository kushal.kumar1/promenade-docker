<?php

namespace Niyotail\Importers\Excel;

use Niyotail\Services\InventoryMigrationService;
use Niyotail\Exceptions\ServiceException;
use Illuminate\Support\Facades\DB;
use Niyotail\Http\Requests\Importer\InventoryMigrationRequest;

class InventoryMigrationImporter extends Importer
{
    private $migration;

    protected function onLoaded()
    {
        $rows = $this->excel->first();
        DB::transaction(function () use ($rows) {
            $migrationService = new InventoryMigrationService();
            $user = auth()->user();
            foreach ($rows as $row) {
                    $row = $row->toArray();
                    $hasAccess = !empty($user->warehouses->where('id', $row['warehouse_id'])->first());
                    if(!$hasAccess) {
                        $data = $row;
                        $data['error'] = "You don't have the access to selected warehouse";
                        $this->addOutputRow($data);
                        return;
                    }

                    if (!$this->validate($row)) {
                        continue;
                    }

                    try {
                        if (empty($this->migration)) {
                            $this->migration = $migrationService->create($row['warehouse_id'], $this->importer->employee);
                        }
                        $migrationItem = $migrationService->addItem($this->migration, $row['product_id'], $row['quantity'], $row['from_storage_id'], $row['employee_id'], $row['to_storage_id']);
                        // $migrationService->getUpdatedInventories($migrationItem['item'], $migrationItem['inventories']);
                    } catch (ServiceException $serviceException) {
                        $data = $row;
                        $data['error'] = $serviceException->getMessage();
                        $this->addOutputRow($data);
                    }
            }
        });
    }

    protected function rules(): array
    {
        return (new InventoryMigrationRequest())->rules();
    }
}
