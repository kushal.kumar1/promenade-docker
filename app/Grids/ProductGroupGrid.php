<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProductGroup;

class ProductGroupGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("product-group-grid");
        $grid->addColumn("id")->setSearchFilter();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn('brand.name')->setSearchFilter();
        $grid->addColumn('edit', 'Action')->setCallback(function ($val, $row) {
            $brand = $row->getSrc();
            return '
                <a href="/product-group/edit/'.$brand->id.'" class="btn btn-raw"><i class="fa fa-edit"></i></a>
            ';
        });
        return $grid;
    }

    protected function getQuery()
    {
        return ProductGroup::with('products', 'brand', 'collection', 'requestedProducts', 'warehouseAbc');
    }
}
