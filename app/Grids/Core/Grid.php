<?php

namespace Niyotail\Grids\Core;

use Nayjest\Grids\GridConfig;
use Nayjest\Grids\EloquentDataProvider;
use Niyotail\Grids\Core\Header;
use Niyotail\Grids\Core\Column;
use Niyotail\Grids\Core\Footer;
use Nayjest\Grids\Grid as NayGrid;
use Nayjest\Grids\Components\TFoot;
use Nayjest\Grids\Components\THead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Grid
{
    const OPERATOR_LIKE = 'like';
    const OPERATOR_EQ = '=';
    const OPERATOR_NOT_EQ = '<>';
    const OPERATOR_GT = '>';
    const OPERATOR_LS = '<';
    const OPERATOR_LSE = '<=';
    const OPERATOR_GTE = '>=';
    private $config;
    private $header;
    private $footer;
    private $hiddenColumns = [];
    private $sort = [];
    private $actions = [];
    private $buttons = [];

    public function __construct($dataSource)
    {
        $this->config = new GridConfig();
        $this->setDefaultPageSize(10);
        $this->setDefaultSort(['id' => 'desc']);
        $this->setDataSource($dataSource);
    }

    private function setDataSource($source)
    {
        $this->config->setDataProvider(
            new EloquentDataProvider($source)
        );
        return $this;
    }

    public function setName($name)
    {
        $this->config->setName($name);
        return $this;
    }

    public function setDefaultPageSize($size)
    {
        $this->config->setPageSize($size);
        return $this;
    }

    public function setDefaultSort($sortArray)
    {
        $this->sort = $sortArray;
        return $this;
    }

    public function getHeader()
    {
        return $this->header;
    }

    public function getFooter()
    {
        return $this->footer;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function addColumn($name, $label = null)
    {
        $column = new Column($this, $name, $label);
        $this->config->addColumn($column->getConfig());
        return $column;
    }

    public function addHiddenColumn($name)
    {
        $this->hiddenColumns[] = $name;
        return $this;
    }

    public function addAction($name, $href, $destination, $attributes = [])
    {
        $this->actions[] = array_merge($attributes, [
            'name' => $name,
            'href' => $href,
            'data-destination' => $destination
        ]);
    }

    public function addButton($name, $href, $destination, $attributes = [])
    {
        $this->buttons[] = array_merge($attributes, [
            'name' => $name,
            'href' => $href,
            'data-destination' => $destination
        ]);
    }

    public function render()
    {
        $this->sort();
        $header = new Header($this->config->getComponentByName(THead::NAME));
        $header->setBulkActions($this->actions);
        $header->setButtons($this->buttons);
        $header->setDefaultComponents();
        new Footer($this->config->getComponentByName(TFoot::NAME));
        $nayGrid = new NayGrid($this->config);
        return $nayGrid->render();
    }

    private function sort()
    {
        $name = $this->config->getName();
        $sorts = is_array(Input::get($name)) && isset(Input::get($name)['sort']) ? Input::get($name)['sort'] : null;
        if (empty($sorts) || empty(array_filter($sorts))) {
            Input::merge([
                $name => [
                    'sort' => $this->sort
                ]
            ]);
        }
    }
}
