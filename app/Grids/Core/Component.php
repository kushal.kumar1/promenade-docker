<?php

namespace Niyotail\Grids\Core;

use Nayjest\Grids\Components\HtmlTag as NayTag;
use Nayjest\Grids\Components\CsvExport;
use Nayjest\Grids\Components\ExcelExport;
use Nayjest\Grids\Components\ColumnsHider;
use Nayjest\Grids\Components\RecordsPerPage;
use Nayjest\Grids\Components\ShowingRecords;
use Niyotail\Grids\Core\HtmlTag;

class Component
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function createHtmlTag()
    {
        $htmlTag = new HtmlTag();
        return $htmlTag;
    }

    public function addExcelExport($fileName)
    {
        $excelExport = (new ExcelExport())->setFileName($fileName);
        $this->config->addComponent($excelExport);
        return $this;
    }

    public function addCsvExport($fileName)
    {
        $excelExport = (new CsvExport())->setFileName($fileName)->setRowsLimit(50000);
        $this->config->addComponent($excelExport);
        return $this;
    }

    public function addColumnsHider()
    {
        $columnsHider = new ColumnsHider;
        $this->config->addComponent($columnsHider);
        return $this;
    }

    public function addRecordsPerPage($variants = [])
    {
        $recordsPerPage = new RecordsPerPage;
        if ($variants) {
            $recordsPerPage->setVariants($variants);
        }
        $this->config->addComponent($recordsPerPage);
        return $this;
    }

    public function addShowingRecords()
    {
        $showingRecords = new ShowingRecords;
        $this->config->addComponent($showingRecords);
        return $this;
    }

    public function addResetButton()
    {
        $resetButton = (new NayTag())
            ->setContent('<i class="fa fa-refresh" aria-hidden="true"></i> Reset')
            ->setTagName('button')
            ->setAttributes([
                'type' => 'button',
                'class' => 'btn btn-success btn-sm grid-reset',
            ]);
        $this->config->addComponent($resetButton);
        return $this;
    }

    public function addButton($button)
    {

        if (!empty($button)) {
            $button = (new NayTag())
            ->setContent('<a href="'.$button['href'].'">'.$button['name'].'</a>')
            ->setTagName('button')
            ->setAttributes([
                'type' => 'button',
                'class' => 'btn btn-sm btn-primary btn-custom margin-r-5',
                'data-destination' => $button['data-destination']
            ]);
            $this->config->addComponent($button);
        }
        return $this;
    }

    public function addButtons($buttons)
    {
        if (!empty($buttons)) {
            foreach ($buttons as $button) {
                $this->addButton($button);
            }
        }
        return $this;
    }



    public function addAction($action)
    {
        $action['class'] = empty($action['class']) ? 'action' : $action['class'].' action';
        $actionButtonConfig = $this->createHtmlTag()->getConfig();
        $actionButtonConfig->setContent('<a href="'.$action['href'].'">'.$action['name'].'</a>')
            ->setTagName('li')
            ->setAttributes($action);
        $this->config->addComponent($actionButtonConfig);
    }

    public function addActions($actions)
    {
        if (!empty($actions)) {
            $dropdownWrapper = $this->createHtmlTag()->addClass('margin-r-5 inline-block');
            $dropdownWrapperConfig = $dropdownWrapper->getConfig();
            $dropdownButtonConfig = $this->createHtmlTag()->getConfig();
            $dropdownButtonConfig->setContent('Actions <span class="fa fa-caret-down"></span>')
                ->setTagName('button')
                ->setAttributes([
                    'type' => 'button',
                    'class' => 'btn btn-sm btn-default dropdown-toggle',
                    'data-toggle' => 'popover',
                ]);

            $dropdown =  $this->createHtmlTag()->addClass("dropdown-menu bulk-actions");
            foreach ($actions as $action) {
                $dropdown->addAction($action);
            }
            $dropdownConfig = $dropdown->getConfig();
            $dropdownConfig->setTagName('ul');

            $dropdownWrapperConfig->addComponent($dropdownButtonConfig);
            $dropdownWrapperConfig->addComponent($dropdownConfig);
            $this->config->addComponent($dropdownWrapperConfig);
        }
        return $this;
    }
}
