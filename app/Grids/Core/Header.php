<?php

namespace Niyotail\Grids\Core;

use Nayjest\Grids\Components\THead;
use Niyotail\Grids\Core\Component;

class Header extends Component
{
    public function __construct($config = null)
    {
        if (empty($config)) {
            $config = new THead();
        }
        parent::__construct($config);
        // $this->setDefaultComponents();
    }

    public function setBulkActions($actions)
    {
        $this->actions = $actions;
    }

    public function setButtons($buttons)
    {
        $this->buttons = $buttons;
    }

    public function setDefaultComponents()
    {
        $headerTag = $this->createHtmlTag()->addClass("flex gutter-between");
        $leftTag = $this->createHtmlTag()
            ->addRecordsPerPage([10, 20, 50, 100, 200, 500])
            ->addShowingRecords();

        $rightTag = $this->createHtmlTag()
            ->addActions($this->actions)
            ->addButtons($this->buttons)
            ->addResetButton()
            // ->addExcelExport('excel-data-'.date('d-m-Y-h-i-s'))
            ->addCsvExport('csv-data-'.date('d-m-Y-h-i-s'));
        // ->addColumnsHider();

        $headerTag->getConfig()->addComponent($leftTag->getConfig());
        $headerTag->getConfig()->addComponent($rightTag->getConfig());

        $this->config->addComponent($headerTag->getConfig());
    }
}
