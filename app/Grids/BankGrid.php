<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Bank;

class BankGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('bank-grid');
        $grid->addColumn("id");
        $grid->addColumn('name',"Bank")->setSearchFilter();
        $grid->addColumn('is_active',"Status")->setCallback(function ($val,$dp){
            $row = $dp->getSrc();
            return $row->is_active ? 'Active' : 'Inactive';
        })->setBooleanFilter('Active', 'Inactive');

        $grid->addColumn("edit", "Action")->setCallback(function ($val,$dp){
           $row = $dp->getSrc();
           return "<button type='button' class='btn btn-primary' data-modal='#modal-bank-edit' data-id='{$row->id}' data-name='{$row->name}' data-is_active='{$row->is_active}'>Edit</button>";

        });

        return $grid;
    }

    protected function getQuery()
    {
        return Bank::orderBy('id');
    }
}
