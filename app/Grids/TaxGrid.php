<?php
namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Tax;

class TaxGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("tax-grid");
        $grid->addColumn("id");
        $grid->addColumn('sourceState.name', 'Source State');
        $grid->addColumn('supplyState.name', 'Supply State');
        $grid->addColumn('supplyCountry.name', 'Supply Country');
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("percentage");
        $grid->addColumn('min_price');
        $grid->addColumn('max_price');
        $grid->addColumn("start_date", "Start Date")->setCallback(function ($val, $row) {
            $startDate = $row->getSrc()->start_date;
            return Carbon::parse($startDate)->toFormattedDateString();
        });
        $grid->addColumn("end_date", "End Date")->setCallback(function ($val, $row) {
            $endDate = $row->getSrc()->end_date;
            if (!empty($endDate)) {
                return Carbon::parse($endDate)->toFormattedDateString();
            }
        });
        $grid->addColumn("edit", "Action")->setLink(route('admin::taxes.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        $taxClassId=$this->data['tax_class_id'];
        return Tax::where('tax_class_id', $taxClassId)->with(['sourceState','supplyCountry','supplyState']);
    }
}
