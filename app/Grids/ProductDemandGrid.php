<?php


namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProductDemand;

class ProductDemandGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-product-demand");
        $grid->setDefaultSort(['created_at'=>'desc']);
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("date_from")->setSortable();
        $grid->addColumn("date_from")->setSortable();
        $grid->addColumn("channel")->setSortable();
        $grid->addColumn("sale_days")->setSortable();
//        $grid->addColumn("marketer_id")->setSortable();
//        $grid->addColumn("vendor_id")->setSortable();
//        $grid->addColumn("brand_id")->setSortable();
//        $grid->addColumn("cl1_id")->setSortable();
//        $grid->addColumn("cl2_id")->setSortable();
//        $grid->addColumn("cl3_id")->setSortable();
//        $grid->addColumn("cl4_id")->setSortable();
        $grid->addColumn("inventory_days")->setSortable();
        $grid->addColumn("reorder_days")->setSortable();
//        $grid->addColumn("items_count",'total items')->setSortable();
        $grid->addColumn("status")->setSelectFilter(ProductDemand::getConstants('status'));
        $grid->addColumn('source')->setSelectFilter(ProductDemand::getConstants('SOURCE'));
        $grid->addColumn("createdBy.name",'created by')->setSearchFilter();
        $grid->addColumn("created_at",'created at')->setCallback(function ($val, $d) {
            return Carbon::createFromDate($val)->timezone('Asia/Kolkata')->toDateString();
        })->setSearchFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::product-demands.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return ProductDemand::with('createdBy'); //->withCount('items');
    }
}