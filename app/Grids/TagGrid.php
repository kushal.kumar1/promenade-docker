<?php

namespace Niyotail\Grids;

use Illuminate\Database\Eloquent\Builder;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Tag;

class TagGrid extends BaseGrid
{

    protected function setGrid(Grid $grid): Grid
    {
       $grid->setName('tags-grid');
       $grid->addColumn('id')->setSortable();
       $grid->addColumn('name')->setSearchFilter();
       $grid->addColumn('type')->setSelectFilter(Tag::getConstants('TYPE'));
       $grid->addColumn('status')->setBooleanFilter('Active', 'Inactive');
       return $grid;
    }

    protected function getQuery(): Builder
    {
        return (new Tag())->newQuery();
    }
}