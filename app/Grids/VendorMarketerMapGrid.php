<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Models\VendorMarketerMap;

class VendorMarketerMapGrid extends BaseGrid
{
    public function setGrid(Grid $grid)
    {
        $grid->setName('Vendor-Marketer-Map-Grid');
        $grid->addColumn('vendor.name', 'Vendor')->setSearchFilter();
        $grid->addColumn('marketer.name', 'Marketer')->setSearchFilter();
        $grid->addColumn('relation');
        $grid->addColumn('channel');
        $grid->addColumn('mov', 'Min. Order Value');
        $grid->addColumn('lead_time');
        $grid->addColumn('priority');
        $grid->addColumn('credit_term')->setCallback(function($val, $d) {
            $creditTerm = '';
            switch ($val) {
                case "0":
                    $creditTerm = 'Advance';
                break;
                case "999":
                    $creditTerm = 'Bill to bill';
                    break;
                case "default":
                    $creditTerm = $val.' days(s)';
                    break;
            }
            return $creditTerm;
        });
        $grid->addColumn('cash_discount', 'Cash Discount (%)');
        $grid->addColumn('tot_signed')->setCallback(function($val, $d) {
            return $val == 0 ? 'No' : 'Yes';
        });
        $grid->addColumn('date_from');
        $grid->addColumn('date_to');
        $grid->addColumn('status')->setCallback(function($val, $d) {
            return $val == 0 ? 'Inactive' : 'Active';
        });
        $grid->addColumn("edit", "Action")
            ->setLink(route('admin::vendors.marketers.edit', ['vendor_id' => '%s', 'marketer_id' => '%s']),
                ['vendor_id', 'marketer_id'], 'btn btn-sm btn-primary', 'Edit');

        return $grid;
    }

    public function getQuery()
    {
        return VendorMarketerMap::with('vendor', 'marketer')
            ->VendorId($this->data['vendor_id']);
    }
}