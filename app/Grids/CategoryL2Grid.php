<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\CategoryL2;

class CategoryL2Grid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("category-l2-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn('collection.name', 'collection');
        $grid->addColumn("view", "Action")->setCallback(function($val, $dp) {
            return '<a href="'.route("admin::category_l2.edit", [$dp->getSrc()->id]).'" class="btn btn-primary">Edit</a>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        return CategoryL2::with('collection');
    }
}
