<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\PurchaseOrder;
use Niyotail\Models\PurchaseOrderItem;

class PurchaseOrderItemGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        //['HSN','Barcode','Tax','Collection','Current Stock(Unit)', 'Current Stock', 'Days of Average Sale', 'Average Sale', 'Lead Time', 'Days of Demand', 'Wholesale Boost', 'Safety Stock', 'Po Quanity','sku_quantities','sku_quantity','Cost','Total','variants','MRP','Margin','Po Quanity (Unit)','Cost (Unit)','MRP (Unit)','Margin (Unit)'],

        $grid->setName("grid-product-demand-item");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("sku")->setSearchFilter();
        $grid->addColumn("productVariant.product.name","Product")->setSearchFilter();
        $grid->addColumn("productVariant.product.brand.marketer.name","Marketer")->setSearchFilter();
        $grid->addColumn("productVariant.product.brand.name","Brand")->setSearchFilter();
        $grid->addColumn("purchaseOrder.vendor.name","Brand")->setSearchFilter();
        $grid->addColumn("productVariant.product.hsn_sac_code","HSN")->setSearchFilter();
        $grid->addColumn("productVariant.product.barcode",'barcode')->setSearchFilter();
        $grid->addColumn("productVariant.product.taxClass.name",'Tax')->setSearchFilter();
//        $grid->addColumn("productDemandItem.days_for_average_sales",'Days of Avg sale')->setSearchFilter();
//        $grid->addColumn("productDemandItem.average_sale",'Average sale')->setSearchFilter();
//        $grid->addColumn("productDemandItem.lead_time",'Lead Time')->setSearchFilter();
//        $grid->addColumn("productDemandItem.days_of_demand",'Days of Demand')->setSearchFilter();
//        $grid->addColumn("productDemandItem.demand_booster",'Booster')->setSearchFilter();
//        $grid->addColumn("productDemandItem.safety_stock_days",'safety stock')->setSearchFilter();
        $grid->addColumn("productDemandItem.quantity",'quantity')->setSearchFilter();
        $grid->addColumn("productDemandItem.cost",'cost')->setSearchFilter();
        $grid->addColumn("productDemandItem.total",'total')->setSearchFilter();
        $grid->addColumn("productVariant.mrp",'MRP')->setSearchFilter();
        $grid->addColumn("Margin",'Margin')->setCallback(function ($var, $row) {
            $mrp = $row->getSrc()->productVariant->mrp;
            $cost = $row->getSrc()->productDemandItem->cost;
            $margin=0;
            if($cost > 0){
                $margin= (($mrp-$cost)/$mrp)*100;
            }
            return number_format($margin,2);
        });
        $grid->addColumn("productDemandItem.quantity",'quantity (unit)')->setCallback(function ($var, $row) {
            return $row->getSrc()->productVariant->quantity * $row->getSrc()->productDemandItem->quantity;
        });
        $grid->addColumn("productDemandItem.cost",'cost (unit)')->setCallback(function ($var, $row) {
            $quantity = $row->getSrc()->productVariant->quantity;
            $cost = $row->getSrc()->productDemandItem->cost;
            if($cost > 0){
                $cost= ($cost/$quantity);
            }
            return number_format($cost,2);
        });
        $grid->addColumn("productVariant.mrp",'MRP (unit)')->setCallback(function ($var, $row) {
            $variants = $row->getSrc()->productVariant->product->variants;
            $variants=$variants->sortBy('quantity');
            return $variants->first()->mrp;
        });
        $grid->addColumn("Margin",'Margin (unit)')->setCallback(function ($var, $row) {
            $variants = $row->getSrc()->productVariant->product->variants;
            $variants=$variants->sortBy('quantity');
            $mrp= $variants->first()->mrp;
            $quantity = $row->getSrc()->productVariant->quantity;
            $cost = $row->getSrc()->productDemandItem->cost;
            $margin=0;
            if($cost > 0){
                $cost= ($cost/$quantity);
                $margin= (($mrp-$cost)/$mrp)*100;
            }
            return number_format($margin,2);
        });



        return $grid;
    }

    protected function getQuery()
    {
        return PurchaseOrderItem::where('purchase_order_id',$this->data['purchase_order_id'])
            ->with('productDemandItem','productVariant.product.brand.marketer','purchaseOrder.vendor','productVariant.product.taxClass','productVariant.product.variants');
    }
}