<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Warehouse;

class WarehouseGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("warehouse-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("type")->setSelectFilter(Warehouse::getConstants('TYPE'));
        $grid->addColumn("parentWarehouse.name","Parent")->setSearchFilter();
        $grid->addColumn("pincode")->setSearchFilter();
        $grid->addColumn("gstin")->setSearchFilter();
        $grid->addColumn("invoice_code")->setSearchFilter();
        $grid->addColumn("city.name")->setSearchFilter();
        $grid->addColumn("status")->setBooleanFilter("Active", "Inactive");
        $grid->addColumn("edit", "Action")->setLink(route('admin::warehouses.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return Warehouse::with('city', 'parentWarehouse');
    }
}
