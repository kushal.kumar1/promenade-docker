<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProfileQuestion;

class ProfileQuestionsGrid extends BaseGrid
{
    public function setGrid(Grid $grid)
    {
        $grid->addColumn('id', 'ID');
        $grid->addColumn('name', 'Name');
        $grid->addColumn('type', 'Type');
        $grid->addColumn('status', 'Status')->setCallback(function ($val, $dp) {
            return $val == 0 ? "In-active" : "Active";
        });
        $grid->addColumn('priority', 'Priority');
        $grid->addColumn('is_mandatory', 'Mandatory')->setCallback(function ($val, $dp) {
            return $val == 0 ? "No" : "Yes";
        });
        $grid->addColumn("view", "Options")->setLink(route('admin::profile-questions.options', ['id' => '%s']), ['id'], 'btn btn-sm', 'Options');
        $grid->addColumn("action", "Action")->setLink(route('admin::profile-questions.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');

        return $grid;
    }

    public function getQuery()
    {
        return ProfileQuestion::query();
    }
}