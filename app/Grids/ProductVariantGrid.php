<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProductVariant;

class ProductVariantGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-variant");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("product.id", "PId")->setSortable();
        $grid->addColumn("product.name", "Name")->setSearchFilter();
        // $grid->addColumn("product.code", "Article Code")->setSearchFilter();
        $grid->addColumn("value", "Type")->setSearchFilter(Grid::OPERATOR_EQ);
        $grid->addColumn("sku")->setSearchFilter();
        $grid->addColumn("mrp")->setSortable()->setSearchFilter();
        $grid->addColumn("price")->setSortable()->setSearchFilter();
        $grid->addColumn("quantity", "qty")->setSortable()->setSearchFilter()->setCallback(function ($val, $row) {
            $variant = $row->getSrc();
            return $variant->quantity." ".$variant->uom;
        });
        $grid->addColumn("moq");
        // $grid->addColumn("product.status", "Status")->setBooleanFilter("Active", "Inactive");
        $grid->addColumn("edit", "Action")->setLink(route('admin::products.variants.edit', ['productId' => '%s', 'variantId' => '%s']), ['product_id','id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return ProductVariant::with('product');
    }
}
