<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Badge;

class BadgeGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("badge-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("users_count", "users");
        $grid->addColumn("description");
        return $grid;
    }

    protected function getQuery()
    {
        return Badge::withCount('users');
    }
}
