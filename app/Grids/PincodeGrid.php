<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\City;
use Niyotail\Models\State;
use Niyotail\Models\Pincode;

class PincodeGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('pincode-grid');
        $grid->addColumn("id");
        $grid->addColumn('pincode',"Pincode")->setSearchFilter()->setWidth("250px");
        $grid->addColumn("city.name","City");
        $grid->addColumn("city.state.name","State");
        $grid->addColumn("edit", "Action")->setLink(route('admin::pincode.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'edit');
        return $grid;
    }

    protected function getQuery()
    {
        return Pincode::with('city', 'city.state');
    }
}
