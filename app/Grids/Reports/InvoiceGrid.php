<?php

namespace Niyotail\Grids\Reports;

use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Niyotail\Models\Invoice;

class InvoiceGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-invoice-report");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("reference_id", "Invoice Number")->setSearchFilter()->setSortable()->setCallback(function ($val, $dp) {
            $invoice = $dp->getSrc();
            $link = route('admin::invoices.index', $invoice->id);
            return '<a href="'.$link.'" target="_blank" class="link">'.$invoice->reference_id.'</a>';
        });
        ;
        // $grid->addColumn("financial_year", )->setSearchFilter();
        $grid->addColumn("order.reference_id", "Order Id")->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("order.store_id", "Store ID")->setSearchFilter();
        $grid->addColumn("order.store.name", "Store")->setSearchFilter()->setLink(route('admin::stores.profile', ['id' => '%s']), ['order.store_id'], 'link');
        $grid->addColumn("amount")->setSearchFilter()->setSortable();
        $grid->addColumn("status")->setSelectFilter(Invoice::getConstants('status'));
        $grid->addColumn('created_at')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn('action')->setCallback(function ($val, $dp) {
            $invoice = $dp->getSrc();
            return '<button class="btn btn-sm regenerate-invoice" data-id="'.$invoice->id.'"> <i class="fa fa-spinner fa-spin hidden"></i> Regenerate</button>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        return Invoice::with('order.store');
    }
}
