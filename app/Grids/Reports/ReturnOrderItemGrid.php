<?php

namespace Niyotail\Grids\Reports;

use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\ReturnOrderItem;
use Niyotail\Models\Store;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Order;

class ReturnOrderItemGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-return-order-item-report");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("return_id", "R Id");
        $grid->addColumn("order.reference_id", "Order")->setSearchFilter()
            ->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("product.name", "name")->setSearchFilter();
        $grid->addColumn("product.barcode", "Barcode")->setSearchFilter();
        $grid->addColumn("order.store.name", "Store")->setSearchFilter();
        $grid->addColumn("order.store.beat.name", "Beat")->setSearchFilter();
        $grid->addColumn("sku")->setSearchFilter();
        $grid->addColumn("price", 'price');
        $grid->addColumn("quantity", 'Qty');
        $grid->addColumn("subtotal");
        $grid->addColumn("discount");
        $grid->addColumn('tax');
        $grid->addColumn('total');
        $grid->addColumn('reason')->setSearchFilter();
        $grid->addColumn('created_at');
        return $grid;
    }

    private function getOrderStatusOptions()
    {
        return ReturnOrder::getConstants("STATUS");
    }

    protected function getQuery()
    {
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return OrderItem::join('return_order_items', 'return_order_items.order_item_id', '=', 'order_items.id')
        ->join('return_orders', 'return_orders.id', '=', 'return_order_items.return_order_id')
        ->selectRaw('return_orders.created_at as created_at, order_items.id as id, return_orders.id as return_id, order_items.order_id, order_items.product_id, order_items.price, order_items.mrp, order_items.sku, order_items.variant, sum(quantity) as quantity, sum(subtotal) as subtotal, sum(discount) as discount, sum(tax) as tax, sum(order_items.total) as total, return_order_items.reason as reason')
        ->whereHas('order', function ($orderQuery) use ($selectedWarehouse){
            $orderQuery->where('warehouse_id', $selectedWarehouse);
        })
        ->with('order.store.beat', 'product')
        ->where('return_orders.status', '!=', 'cancelled')
        ->groupBy('order_items.sku', 'return_orders.id')
        ->orderBy('return_orders.id', 'desc');
    }
}
