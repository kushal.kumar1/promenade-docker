<?php

namespace Niyotail\Grids\Reports;

use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Illuminate\Support\Facades\Storage;
use Niyotail\Models\Attribute;

class ProductInventorySnapshotGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("product-inventory-snapshot-report");
        $grid->addColumn("File Name", 'files')
            //->setSearchFilter()
            ->setCallback(function ($val, $dp) {
                $row = $dp->getSrc();
                return $row->files;
            });
        $grid->addColumn('action')->setCallback(function ($val, $dp) {
            $result = $dp->getSrc();
            $link = route('admin::reports.product-inventory-snapshot-file', $result->files);
            return '<a href="'.$link.'" target="_blank" class="btn btn-sm">Download</a>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        $files = Storage::files('product_inventory');
        $query = '';
        foreach ($files as $file) {
          $fileName = explode('/', $file)[1];
          if (empty($query)) {
              $query = "select '$fileName' as files ";
          } else {
              $query .= "union select '$fileName' ";
          }
        }
        if (!empty($query)) {
            return Attribute::selectRaw('result.*')
                  ->join(\DB::raw("($query) result"), 'result.files', '!=', 'attributes.name')
                  ->groupBy('result.files')->orderBy('result.files', 'desc');
        }
    }
}
