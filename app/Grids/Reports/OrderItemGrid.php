<?php

namespace Niyotail\Grids\Reports;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Shipment;
use Niyotail\Models\Store;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Order;

class OrderItemGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-order-item-report");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("order.reference_id", "Order Id")->setSearchFilter()
            ->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("order.store.name", "Store")->setSearchFilter();
        $grid->addColumn("product.id", "P Id")->setSearchFilter();
        $grid->addColumn("product.name", "name")->setSearchFilter();
        $grid->addColumn("product.brand.name", "Brand")->setSearchFilter();
        $grid->addColumn("product.brand.marketer.code", "marketer")->setSearchFilter();
        $grid->addColumn("product.barcode", "Barcode")->setSearchFilter();
        $grid->addColumn("sku", "Sku")->setSearchFilter();
        $grid->addColumn("pretty_price", "price");
        $grid->addColumn("quantity", 'Qty');
        $grid->addColumn("pretty_discount", "discount");
        $grid->addColumn('pretty_tax', 'tax');
        $grid->addColumn("pretty_total", 'total');
        $grid->addColumn("status")->setSelectFilter(OrderItem::getConstants('status'));
        $grid->addColumn('order.created_at', 'Date')->setSortable()->isDate()->setDateTimeRangeFilter();
        return $grid;
    }

    protected function getQuery()
    {
        $monthStartDate = Carbon::now()->subMonth()->toDateString();
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return OrderItem::selectRaw('order_id, product_id, price, mrp, sku, variant, sum(quantity) as quantity, sum(subtotal) as subtotal, sum(discount) as discount, sum(tax) as tax, sum(total) as total, status')
//          ->whereHas('inventories')
          ->where('created_at', '>=', $monthStartDate)
            ->whereHas('order', function ($orderQuery) use ($selectedWarehouse){
                $orderQuery->where('warehouse_id', $selectedWarehouse);
            })
          ->with('order.store.beat', 'product.brand.marketer')
          ->groupBy('sku', 'order_id', 'status');
    }
}
