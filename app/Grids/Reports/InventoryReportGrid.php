<?php
namespace Niyotail\Grids\Reports;

use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Inventory;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;

class InventoryReportGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-inventory-report");
        $grid->addColumn('id',"PID")->setSearchFilter();
        $grid->addColumn('group_id');
        $grid->addColumn('category.cl4_id',"Cl4 Id");
        $grid->addColumn('category.cl3_id',"Cl3 Id");
        $grid->addColumn('category.cl2_id',"Cl2 Id");
        $grid->addColumn('category.cl1_id',"Cl1 Id");
        $grid->addColumn('brand.name');
        $grid->addColumn("name", "Name")->setSearchFilter()->setLink(route('admin::products.edit', ['productId' => '%s']), ['id'], 'link');
        $grid->addColumn('unitVariant.mrp', "mrp");
        $grid->addColumn('barcode', "Barcode")->setSearchFilter();
        $grid->addColumn("stock", "Stock")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return $row->total_inventory;
        });
        $grid->addColumn("racks", "Racks")->setCallback(function ($val, $dp) {
            $racks = $dp->getSrc()->warehouseRacks->pluck('reference')->toArray();
            return implode(",",$racks);
        });
        return $grid;
    }


    protected function getQuery()
    {
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return Product::active()->with('warehouseRacks','category','brand','unitVariant')
            ->withStockByWarehouse($selectedWarehouse);
    }
}
