<?php


namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProductDemand;
use Niyotail\Models\ProductDemandItem;

class ProductDemandItemsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-product-demand-items");
        $grid->setDefaultSort(['daily_avg_unit_sale'=>'desc']);
        $grid->addColumn("group_id", 'GID')->setSearchFilter();
        $grid->addColumn("group.name", 'Group')->setSearchFilter();
        $grid->addColumn("total_quantity_sold", 'Total Quantity Sold')->setSortable();
        $grid->addColumn("daily_avg_unit_sale", 'Daily Avg. Unit Sale')->setSortable();
        $grid->addColumn("available_stock", 'Stock Days')->setSortable();
        $grid->addColumn("reorder_quantity", 'Reorder Quantity')->setSortable();
        return $grid;
    }

    protected function getQuery()
    {
        return ProductDemandItem::Demand($this->data['demand_id'])->with('group');
    }
}