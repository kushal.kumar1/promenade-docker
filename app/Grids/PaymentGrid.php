<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Payment;

class PaymentGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-payments");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("order.reference_id", "Order")->setSearchFilter()
            ->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("returnOrder.reference_id", "Return Order")->setSearchFilter();
        $grid->addColumn("transaction_id")->setSearchFilter();
        $grid->addColumn("method")->setSelectFilter(Payment::getConstants("METHOD"));
        $grid->addColumn("type")->setSelectFilter(Payment::getConstants("TYPE"));
        $grid->addColumn("amount")->setSortable();
        $grid->addColumn("status")->setSelectFilter(Payment::getConstants("STATUS"));
        return $grid;
    }

    protected function getQuery()
    {
        return Payment::with('order','returnOrder');
    }
}
