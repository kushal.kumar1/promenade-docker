<?php

namespace Niyotail\Grids\Putaway;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Putaway\Putaway;
use Niyotail\Models\Putaway\PutawayItem;

class PutawayItemGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("putaway-item-grid");
        $grid->addColumn("product.name", "Product")->setSearchFilter()->setCallback(function($val, $dp) {
            $product = $dp->getSrc()->product;
            $html = "<div>$product->name</div>
                    <div class='small'><span class='bold'>Barcode:</span> $product->barcode</div>
                     <div class='small'><span class='bold'>PID:</span> $product->id</div>";
            return $html;
        });
        $grid->addColumn("quantity");
        $grid->addColumn("storage.label","Storage" )->setCallback(function($val, $dp) {
            $storage = $dp->getSrc()->storage;
            $html = "<div><span class='bold'>Label:</span> $storage->label</div>
                    <div class='small'><span class='bold'>Zone:</span> $storage->zone</div>";
            return $html;
        });
        return $grid;
    }

    protected function getQuery()
    {
        $id = $this->data['id'];
        return PutawayItem::with('storage','product')->where('putaway_id', $id);
    }
}
