<?php

namespace Niyotail\Grids\Putaway;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Putaway\Putaway;

class PutawayGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("putaway-grid");
        $grid->addColumn("id", "Id")->setSearchFilter();
        $grid->addColumn("employee.name", "Created By")->setSearchFilter();
        $grid->addColumn("status", )->setSelectFilter(Putaway::getConstants('STATUS'));
        $grid->addColumn("edit", "Action")->setLink(route('admin::putaway.detail', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return Putaway::with('employee');
    }
}
