<?php


namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Tag;
use Niyotail\Models\Transaction;
use Niyotail\Models\WarehouseScope;

class TransactionGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("transaction-grid");
        $grid->setDefaultSort(['transaction_date' => 'desc']);
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("transaction_id")->setSearchFilter();
        $grid->addColumn("source_type")->setSearchFilter()->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            $link = "#";
            $type = '';
            $linkClass = !empty($transaction->source_type) ? "class='link'" : "";
            if ($transaction->source_type == "invoice") {
                $link = route("admin::invoices.index", $transaction->source->id);
            }
            if ($transaction->source_type == "credit_note") {
                $link = route("admin::returns.credit-note", $transaction->source->id);
            }
            if ($transaction->source_type == 'rate_credit_note') {
                $link = route("admin::rate-credit-note.get-file", $transaction->source->id);
                $type = $transaction->source->type;
            }

            return "<a href='$link' target='_blank' $linkClass>$transaction->source_type $type</a>";
        });
        $grid->addColumn("description")->setCallback(function ($val, $row) {
            $data = $row->getSrc();
            if ($data->source_type == 'rate_credit_note') {
                return (
                    "
                        <small>Ref ID {$data->source->reference_id}</small>
                        <br>
                        <small>Date from " . Carbon::parse($data->source->start_date)->format('d-M-Y') . "</small>
                        <br>
                        <small>Date To " . Carbon::parse($data->source->end_date)->format('d-M-Y') . "</small>
                        <br>
                        <small>Generated on " . Carbon::parse($data->source->generated_on)->format('d-M-Y') . "</small>
                    "
                );
            } else return $data->description;
        });
        $grid->addColumn("payment_method")->setSelectFilter(Transaction::getConstants('payment_method'));
        $grid->addColumn("paymentTag.name", "Payment Tag")->setSelectFilter(self::getPaymentTags());
        $grid->addColumn('credit')->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            if ($transaction->type == Transaction::TYPE_CREDIT) {
                return $transaction->amount;
            }
        });
        $grid->addColumn('debit')->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            if ($transaction->type == Transaction::TYPE_DEBIT) {
                return $transaction->amount;
            }
        });
        $grid->addColumn("balance", "Ledger Balance")->setCallback(function ($val, $dp) {
            if ($val == 0) {
                return $val;
            }
            return -($val);
        });
//        $grid->addColumn("settled invoices")->setCallback(function ($val, $row) {
//            $transaction = $row->getSrc();
//            $invoiceText = '';
//            if ($transaction->settledDebitTransactions->isNotEmpty()) {
//                foreach ($transaction->settledDebitTransactions as $debitTransaction) {
//                    if (!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice') {
//                      $invoice = $debitTransaction->source;
//                      if($invoice->amount == $debitTransaction->pivot->amount) {
//                        $invoiceText .= "[$invoice->reference_id - Full]";
//                      } else {
//                        $invoiceText .= "[$invoice->reference_id - Partial]";
//                      }
//                    }
//                }
//            }
//            return "<span>$invoiceText</span>";
//        });
        $grid->addColumn("transaction_date")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("createdBy.name", "created by");
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::transactions.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    private static function getPaymentTags(){
        $options = [];
        foreach(Tag::paymentTags() as $paymentTag){
            $options[$paymentTag] = $paymentTag;
        }

        return $options;
    }

    protected function getQuery()
    {
        return Transaction::where('status', Transaction::STATUS_COMPLETED)
            ->where('store_id', $this->data['store_id'])
            ->with(['source', 'createdBy', 'paymentTag', 'invoices'=>function($invoiceQuery){
                $invoiceQuery->withoutGlobalScope(WarehouseScope::class);
            }]);
    }
}
