<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceGrn;

class PurchaseInvoiceGrnGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice-grn");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("invoice.vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("invoice.reference_id",'invoice id')->setSearchFilter();
        $grid->addColumn("invoice.vendor_ref_id")->setSearchFilter();
        $grid->addColumn("invoice.invoice_date", "Invoice date")->setDateTimeRangeFilter()->setSortable();
        $grid->addColumn("delivery_date")->setDateTimeRangeFilter()->setSortable();
        $grid->addColumn("ref_id", 'GRN Ref.')->setCallback(function ($var, $row) {;
            return $row->getSrc()->getPrettyGrnReferenceId();
        });
        $grid->addColumn("total", 'Amount')->setCallback(function ($var, $row) {;
            return $row->getSrc()->getPrettyGrnTotal();
        });
        $grid->addColumn("view", "Download")->setCallback(function($val, $dp) {
            return '<a href="'.route("admin::purchase-invoice.grn", [$dp->getSrc()->id]).'" class="btn btn-primary">View</a>';
        });
        return $grid;

        //
    }

    protected function getQuery()
    {
        return PurchaseInvoiceGrn::with('invoice');
    }
}
