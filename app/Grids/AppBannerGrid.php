<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\AppBanner;
use Image;

class AppBannerGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('app-banner-grid');
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("IMG")->setCallback(function ($val, $row) {
            $banner = $row->getSrc();
            $image = Image::getSrc($banner, 200);
            return '<img class="bd" src="'.$image.'">';
        });
        $grid->addColumn("position")->setSortable();
        $grid->addColumn('valid_from')->setSortable();
        $grid->addColumn("valid_to")->setSortable();
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::app-banners.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return (new AppBanner())->newQuery();
    }
}
