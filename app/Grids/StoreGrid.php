<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\CreditLimitHelper;
use Niyotail\Models\Store;

class StoreGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("stores-grid");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('80px');
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("tags", "Tags")->setCallback(function ($val, $dp) {
            $storeTags = [];
            $row = $dp->getSrc();
            $tags = $row->tags;
            foreach ($tags as $tag) {
                $storeTags[] = $tag->name;
            }

            return implode(", ", $storeTags);
        })->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHas('tags', function ($tagQuery) use ($val) {
                $tagQuery->where('name', 'like', "%$val%");
            });
        });
        $grid->addColumn("address")->setSearchFilter();
        $grid->addColumn("landmark")->setSearchFilter();
        $grid->addColumn("gstin")->setSearchFilter();
        $grid->addColumn('pincode', 'Pin Code')->setSearchFilter();
        $grid->addColumn('creditLimit.value', 'Credit Limit')
            ->setCallback(function ($val, $dp) {
                if ($val) {
                    return "&#8377;" . number_format($val);
                } else {
                    return "";
                }
            });
        $grid->addColumn('contact_person', 'Contact Person')->setSearchFilter();
        $grid->addColumn('contact_mobile', 'Contact Mobile')->setSearchFilter();
        $grid->addColumn('beat.name', 'Beat')->setSearchFilter();
        $grid->addColumn('users_count', 'Users')->setSortable();
        $grid->addColumn("totalBalance.balance_amount", "Outstanding")
            ->setCallback(function ($val, $dp) {
                $row = $dp->getSrc();
                if (empty($row->totalBalance)) {
                    return "0.00";
                }
                return $val;
            });
        $grid->addColumn('credit_allowed')->setBooleanFilter('Yes', 'No');
        $grid->addColumn("verified")->setBooleanFilter('Verified', 'Unverified');
        $grid->addColumn("status")->setSelectFilter($this->getStoreStatusOptions())->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return $row->getStatusText();
        });
        $grid->addColumn("edit", "Action")->setLink(route('admin::stores.profile', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    private function getStoreStatusOptions()
    {
        return ['0' => 'Inactive', '1' => 'Active', '2' => 'Disabled'];
    }

    protected function getQuery()
    {
        return Store::with('totalBalance', 'beat', 'tags', 'creditLimit')->withCount('users');
    }
}
