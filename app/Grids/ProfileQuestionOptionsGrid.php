<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProfileQuestionOption;

class ProfileQuestionOptionsGrid extends  BaseGrid
{
    public function setGrid(Grid $grid)
    {
        $grid->addColumn('id', 'ID');
        $grid->addColumn('option', 'Option');
        $grid->addColumn('priority', 'Priority');
        $grid->addColumn('status', 'Status')->setCallback(function ($val, $dp) {
            return $val == 0 ? "In-active" : "Active";
        });
        $grid->addColumn('view', 'Action')->setCallback(function ($val, $dp) {

            if($dp->getSrc() instanceof ProfileQuestionOption){
                return '<a class="btn btn-sm btn-primary" href="'.route("admin::profile-questions.options.edit",[$dp->getSrc()->question_id, $dp->getSrc()->id]).'">View</a>';
            }
            return $dp->getSrc();
        });

        return $grid;
    }

    public function getQuery()
    {
        return ProfileQuestionOption::QuestionId($this->data['q_id']);
    }
}