<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Attribute;

class AttributeGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('attribute-grid');
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("slug")->setSearchFilter();
        $grid->addColumn("filter");
        return $grid;
    }

    protected function getQuery()
    {
        return (new Attribute())->newQuery();
    }
}
