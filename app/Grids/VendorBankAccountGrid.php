<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\VendorBankAccount;

class VendorBankAccountGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-vendors-bank-account");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("vendor.name")->setSearchFilter();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("status")->setBooleanFilter('Active', 'Disabled');
        $grid->addColumn("bank")->setSearchFilter();
        $grid->addColumn("ifsc");
        $grid->addColumn("number")->setSearchFilter();
        $grid->addColumn("type")->setBooleanFilter('savings', 'current');
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("profile", "Action")->setLink(route('admin::vendors.bank_account.edit', ['id' => '%s']), ['vendor.id'], 'btn btn-sm', 'Manage');
        return $grid;
    }

    protected function getQuery()
    {
        return VendorBankAccount::with('vendor');
    }
}
