<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Image;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Shipment;
use Niyotail\Models\WarehouseScope;

class ShipmentGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $picklistId = $this->data['picklist_id'] ?? 'main';
        $grid->setName("shipment-grid-$picklistId");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id")->setWidth('80px')->setSortable()->setSearchFilter();
        $grid->addColumn("boxes");
        $grid->addColumn("order.reference_id", "order")->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("order.store.name", "Store")->setSearchFilter()->setLink(route('admin::stores.profile', ['id' => '%s']), ['order.store.id'], 'link');
//        $grid->addColumn("order.store.beat.name", "Beat")->setSearchFilter();
        $grid->addColumn("items_count", "Items")->setCallback(function ($var, $row) {
            $items = $row->getSrc()->orderItems;
            return $items->unique('sku')->count();
        });
        $grid->addColumn("items_count", "Qty");
        $grid->addColumn("invoice.reference_id", "Invoice")->setWidth("170px")->setSearchFilter();
//            ->setLink(route('admin::invoices.index', ['id' => '%s']), ['invoice.id'], 'link');
        $grid->addColumn("invoice.amount", "Amount");
        $grid->addColumn("status")->setSelectFilter(Shipment::getConstants('status'))->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-' . $status . '" >' . $status . '</div>';
        });
        $grid->addColumn("pod_name", "POD")->setCallback(function ($var, $row) {
            $podName = $row->getSrc()->pod_name;
            if (!empty($podName)) {
                $id = $row->getSrc()->id;
                return '<a href='.Image::getSrc($row->getSrc()).' class="link">'.$podName.'</a>';
            }
            return '';
        });//->setLink(asset("shipments/%s/%s"), ['id', 'pod_name'], 'link');
        $grid->addColumn('created_at')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn('updated_at')->setSortable()->isDate()->setDateTimeRangeFilter();
        return $grid;
    }

    protected function getQuery()
    {
        $picklistId = $this->data['picklist_id'] ?? null;
        $shipmentQuery = Shipment::withCount('items')->with(['order.store', 'invoice']);

        if (!empty($picklistId)) {
            $shipmentQuery = $shipmentQuery->where('picklist_id', $picklistId);
        }

        return $shipmentQuery;
    }
}
