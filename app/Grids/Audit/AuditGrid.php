<?php


namespace Niyotail\Grids\Audit;

use Illuminate\Database\Eloquent\Builder;
use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Reason;
use Niyotail\Services\Audit\AuditService;

class AuditGrid extends BaseGrid
{
    public static function getCancelButton($audit)
    {
        return "<div><a type='button' class='btn btn-danger cancel-audit' data-action='"
            . route("admin::audits.cancel", ["audit" => $audit->id])
            . "' data-modal='#modal-audit-cancel'>Cancel</a></div>";
    }

    public static function getViewButton($audit)
    {
        return "<div><a href='/audits/$audit->id' type='button' class='btn btn-success'>View</a></div>";
    }

    protected function setGrid(Grid $grid): Grid
    {
        $grid->setName("grid-audits");
        $grid->setDefaultSort(['id' => 'desc']);

        $grid->addColumn("id")
            ->setSortable()
            ->setSearchFilter();
        $grid->addColumn("name", "Audit Name")
            ->setSearchFilter();
        $grid->addColumn("created_at", "Created At")
            ->setSortable();
        $grid->addColumn("type", "Type")
            ->setSelectFilter(Audit::auditTypes())
            ->setCallback(function ($var, $row) {
                $type = $row->getSrc()->type;
                return '<span class="tag tag-' . $type . '" >' . $type . '</span>';
            });
        $grid->addColumn("status")
            ->setSelectFilter(Audit::auditStatuses())
            ->setCallback(function ($var, $row) {
                $status = $row->getSrc()->status;
                return '<span class="tag tag-' . $status . '" >' . $status . '</span>';
            });
        $grid->addColumn("", "No. of Items")
            ->setCallback(function ($var, $row) {
                $audit = $row->getSrc();
                return count($audit->type == Audit::TYPE_PRODUCT ? AuditService::distinctProductIds($audit) : AuditService::distinctStorageIds($audit));
            });
        $grid->addColumn("createdBy.name", "Created By")
            ->setSearchFilter();
        $grid->addColumn("reason.reason_text", "Audit Reason");
        $grid->addColumn("", 'Action')
            ->setCallback(function ($var, $row) {
                $audit = $row->getSrc();
                $auditId = $audit->id;
                $status = $row->getSrc()->status;
                switch ($status) {
                    case Audit::STATUS_GENERATED:
                    case Audit::STATUS_CONFIRMED:
                    case Audit::STATUS_STARTED:
                        return self::getViewButton($audit) . "<br>" . self::getCancelButton($audit);
                    case Audit::STATUS_APPROVED:
                    case Audit::STATUS_COMPLETED:
                    case Audit::STATUS_CANCELLED:
                        return self::getViewButton($audit);
                }
            });

        return $grid;
    }


    protected function getQuery(): Builder
    {
        $warehouseId = $this->data['warehouseId'];
        return Audit::where('warehouse_id', $warehouseId);
    }

}
