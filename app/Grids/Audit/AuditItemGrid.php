<?php


namespace Niyotail\Grids\Audit;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Audit\AuditItemMeta;
use Niyotail\Models\Employee;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;
use Niyotail\Services\Audit\AuditItemService;
use Niyotail\Services\Audit\AuditService;

class AuditItemGrid extends BaseGrid
{
    private static function setAssignColumn($grid, $audit, $selectAssignedTo)
    {
        $grid->addColumn("assignedTo.name", "Assigned To")
            ->setSelectFilter($selectAssignedTo)
            ->setCallback(function ($var, $row) {
                $auditItem = $row->getSrc();
                if (!empty($auditItem->assigned_to))
                    return $auditItem->assignedTo->name;
                return '';
            });
    }

    private static function setResultColumn($grid, $audit)
    {
        $grid->addColumn("Audit Result")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();

            $total = AuditItemService::actualQuantity($auditItem);
            if ($auditItem->status == AuditItem::STATUS_PENDING_APPROVAL || $auditItem->status == AuditItem::STATUS_PENDING_ACTION || $auditItem->status == AuditItem::STATUS_RESOLVED) {
                $badResult = '';
                foreach ($auditItem->auditItemMetas as $auditItemMeta) {
                    if ($auditItemMeta->condition == AuditItemMeta::CONDITION_BAD) {
                        $badResult = $badResult . '<br>' . $auditItemMeta->quantity . ' items in bad condition : ' . $auditItemMeta->reason->reason_text;
                    }
                }

                $totalResult = $total . " total units found during audit";

                return $totalResult . '<br>' . $badResult;
            }
        });
    }

    private static function setActionTakenColumn($grid, $audit)
    {
        $grid->addColumn("Action Taken")->setCallback(function ($var, $row) use ($audit) {
            $auditItem = $row->getSrc();

            $actions = "";

            foreach ($auditItem->auditItemActions as $auditItemAction) {
                $actions .= $auditItemAction->quantity . " items " . $auditItemAction->action . '<br>';
            }

            return $actions;
        });
    }

    private static function setActionColumn($grid, $audit)
    {
        $grid->addColumn("Action", 'Action')->setCallback(function ($var, $row) use ($grid, $audit) {
            $auditItem = $row->getSrc();
            $actions = [];

            switch ($audit->status) {
                case Audit::STATUS_GENERATED:
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED))
                        array_push($actions, static::getCancelButton($audit, $auditItem));
                    break;
                case Audit::STATUS_CONFIRMED:
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_ASSIGNED))
                        array_push($actions, self::getAssignButton($audit, $auditItem));
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED))
                        array_push($actions, static::getCancelButton($audit, $auditItem));
                    break;
                case Audit::STATUS_STARTED:
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_ASSIGNED))
                        array_push($actions, static::getRejectButton($audit, $auditItem));
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED))
                        array_push($actions, static::getCancelButton($audit, $auditItem));
                    break;
                case Audit::STATUS_APPROVED:
                    if (AuditItemService::canPerformAction($auditItem, AuditItemAction::ACTION_HOLD))
                        array_push($actions, static::getHoldButton($audit, $auditItem));
                    if (AuditItemService::canPerformAction($auditItem, AuditItemAction::ACTION_EXCESS))
                        if ($auditItem->system_quantity < AuditItemService::actualQuantity($auditItem))
                            array_push($actions, static::getMarkExcessButton($audit, $auditItem));
                    if (AuditItemService::canPerformAction($auditItem, AuditItemAction::ACTION_LOSS))
                        if ($auditItem->system_quantity > AuditItemService::actualQuantity($auditItem))
                            array_push($actions, static::getMarkLossButton($audit, $auditItem));
                    break;
            }

            return array_reduce($actions, function ($v1, $v2) {
                return $v1 . "<br>" . $v2;
            }, "");
        });
    }

    private static function getAssignButton($audit, $auditItem)
    {
        return "<div><a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-assign' 
            data-action='" . route('admin::audits.items.assign', ['audit' => $audit->id, 'auditItem' => $auditItem->id]) . "' 
            data-form='#assign-form' 
            >Assign</a></div>";
    }

    private static function getCancelButton($audit, $auditItem)
    {
        return "<div><a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-item-cancel' 
            data-action = '" . route('admin::audits.items.cancel', ['audit' => $audit->id, 'auditItem' => $auditItem->id]). "' 
            data-form='#cancel-form' 
            >Cancel</a></div>";
    }

    private static function getRejectButton($audit, $auditItem)
    {
        return "<div><a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-reject' 
            data-action = '" . route('admin::audits.items.reject', ['audit' => $audit->id, 'auditItem' => $auditItem->id]) . "'
            data-form='#reject-form' 
            >Reject</a></div>";
    }

    private static function getHoldButton($audit, $auditItem)
    {
        return "<div><a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-hold' 
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_HOLD
            ]) . "' 
            data-form='#hold-form' 
            >Hold</a></div>";
    }

    private static function getMarkExcessButton($audit, $auditItem)
    {
        return "<a type='button'
            class='btn btn-primary audit-item-action'
            data-modal='#modal-audit-excess'
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_EXCESS
            ]) . "' 
            data-form='#excess-form' 
            >Mark Excess</a>";
    }

    private static function getMarkLossButton($audit, $auditItem)
    {
        return "<a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-loss' 
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_LOSS
            ]) . "' 
            data-form='#loss-form' 
            >Mark Loss</a>";
    }

    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-audit-items");
        $grid->setDefaultSort(['id' => 'asc']);
        $grid->setDefaultPageSize(100);

        $audit = $this->data['audit'];
        $productIds = AuditService::distinctProductIds($audit);
        $selectProducts = [];
        foreach ($productIds as $productId) {
            $product = Product::select(['id', 'name'])->find($productId);
            if (is_null($product)) continue;
            $selectProducts[$product->name] = "({$product->id}) {$product->name}";
        }
        $storageIds = AuditService::distinctStorageIds($audit);
        $selectStorages = [];
        foreach ($storageIds as $storageId) {
            $storage = Storage::select(['id', 'label'])->find($storageId);
            if (is_null($storage)) continue;
            $selectStorages[$storage->label] = "({$storage->id}) {$storage->label}";
        }
        $assignedToIds = $audit->auditItems()->pluck('assigned_to')->unique();
        $selectAssignedTo = [];
        foreach ($assignedToIds as $assignedToId) {
            $employee = Employee::select(['id', 'name'])->find($assignedToId);
            if (empty($employee))
                continue;
            $selectAssignedTo[$employee->name] = $employee->name;
        }

        switch ($audit->status) {
            case Audit::STATUS_GENERATED:
                $grid->addButton("Bulk Cancel", "#", route('admin::audits.detail', ['audit' => $audit->id]));
                break;
            case Audit::STATUS_CONFIRMED:
                $grid->addButton("Bulk Assign", "#", route('admin::audits.detail', ['audit' => $audit->id]));
                $grid->addButton("Bulk Cancel", "#", route('admin::audits.detail', ['audit' => $audit->id]));
                break;
            case Audit::STATUS_STARTED:
                $grid->addButton("Bulk Cancel", "#", route('admin::audits.detail', ['audit' => $audit->id]));
                $grid->addButton("Bulk Reject", "#", route('admin::audits.detail', ['audit' => $audit->id]));
                break;
        }


        $grid->addColumn('')->isSelect()->setCallback(function ($var, $row) use ($audit) {
            $auditItem = $row->getSrc();
            switch ($audit->status) {
                case Audit::STATUS_GENERATED:
                case Audit::STATUS_CONFIRMED:
                case Audit::STATUS_STARTED:
                case Audit::STATUS_APPROVED:
                    if (AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_CANCELLED) ||
                        AuditItemService::canChangeAuditItemStatus($auditItem, AuditItem::STATUS_ASSIGNED)
                    )
                        return '<input class="select-checkbox" type="checkbox" name="selected[]" value="' . $auditItem->id . '">';
            }
            return '';
        });
        $grid->addColumn('id', 'Audit Item Id')->setSearchFilter();
        $grid->addColumn("product.name", "Product")->setSelectFilter($selectProducts)
            ->setCallback(function ($var, $row) {
                $product = $row->getSrc()->product;
                if (is_null($product)) return '';
                return "PID: " . $product->id . '<br>' . $product->name;
            });
        $grid->addColumn("storage.label", "Storage")->setSelectFilter($selectStorages)
            ->setCallback(function ($var, $row) {
                $storage = $row->getSrc()->storage;
                if (is_null($storage)) return '';
                return "Storage ID: " . $storage->id . '<br>' . $storage->label;
            });
        $grid->addColumn("system_quantity", "SQ");
        $grid->addColumn("status")->setSelectFilter(AuditItem::auditItemStatuses())
            ->setCallback(function ($var, $row) {
                $status = $row->getSrc()->status;
                return '<span class="tag tag-' . $status . '" >' . $status . '</span>';
            });

        switch ($audit->status) {
            case Audit::STATUS_GENERATED:
                static::setActionColumn($grid, $audit);
                break;
            case Audit::STATUS_CONFIRMED:
                static::setAssignColumn($grid, $audit, $selectAssignedTo);
                static::setActionColumn($grid, $audit);
                break;
            case Audit::STATUS_STARTED:
                static::setAssignColumn($grid, $audit, $selectAssignedTo);
                static::setResultColumn($grid, $audit);
                static::setActionColumn($grid, $audit);
                break;
            case Audit::STATUS_APPROVED:
                static::setAssignColumn($grid, $audit, $selectAssignedTo);
                static::setResultColumn($grid, $audit);
                if ($audit->type == Audit::TYPE_PRODUCT)
                    static::setActionTakenColumn($grid, $audit);
                static::setActionColumn($grid, $audit);
                break;
            case Audit::STATUS_COMPLETED:
            case Audit::STATUS_CANCELLED:
                static::setAssignColumn($grid, $audit, $selectAssignedTo);
                static::setResultColumn($grid, $audit);
                if ($audit->type == Audit::TYPE_PRODUCT)
                    static::setActionTakenColumn($grid, $audit);
                break;
        }

        return $grid;
    }

    protected function getQuery()
    {
        $audit = $this->data['audit'];
        return AuditItem::with(['product', 'assignedTo', 'storage', 'auditItemMetas'])
            ->where('audit_id', $audit->id);
    }

}
