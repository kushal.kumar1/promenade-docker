<?php


namespace Niyotail\Grids\Audit;

use Illuminate\Support\Facades\DB;
use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Audit\AuditItemMeta;
use Niyotail\Models\Employee;
use Niyotail\Models\Reason;
use Niyotail\Services\Audit\AuditItemService;

class StorageReportGrid extends BaseGrid
{
    private static function getDismissButton($audit, $auditItem)
    {
        return "<a type='button'
            class='btn btn-primary dismiss-audit-item'
            data-modal='#modal-audit-dismiss'
            data-action = '" . route('admin::audits.items.action', ['audit' => $audit->id, 'auditItem' => $auditItem->id, 'action' => 'dismiss'])
            . "'>Dismiss</a>";
    }

    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-storage-report-list");
        $grid->addColumn("audit_id", "Audit ID");
        $grid->addColumn('id', "Audit Item ID");
        $grid->addColumn("product.name", "Product Name");
        $grid->addColumn("storage.label", "Storage");
        $grid->addColumn("good_quantity", "Good Quantity")->setCallback(function ($var, $row) {
            return AuditItemService::actualQuantity($row->getSrc(), AuditItemMeta::CONDITION_GOOD);
        });
        $grid->addColumn("bad_quantity", "Bad Quantity")->setCallback(function ($var, $row) {
            return AuditItemService::actualQuantity($row->getSrc(), AuditItemMeta::CONDITION_BAD);
        });
        $grid->addColumn("actual_quantity", "Actual Quantity");
        $grid->addColumn("system_quantity", "System Quantity");
        $grid->addColumn("Audit Result")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();

            $total = AuditItemService::actualQuantity($auditItem);
            if ($auditItem->status == AuditItem::STATUS_PENDING_APPROVAL || $auditItem->status == AuditItem::STATUS_PENDING_ACTION || $auditItem->status == AuditItem::STATUS_RESOLVED) {
                $badResult = '';
                foreach ($auditItem->auditItemMetas as $auditItemMeta) {
                    if ($auditItemMeta->condition == AuditItemMeta::CONDITION_BAD) {
                        $badResult = $badResult . '<br>' . $auditItemMeta->quantity . ' items in bad condition : ' . $auditItemMeta->reason->reason_text;
                    }
                }

                $totalResult = $total . " total units found during audit";

                return $totalResult . '<br>' . $badResult;
            }
        });
        $grid->addColumn("Action")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            return static::getDismissButton($auditItem->audit, $auditItem);
        });


        return $grid;
    }

    protected function getQuery()
    {
        $warehouseId = $this->data['warehouseId'];

        return AuditItem::query()
            ->join(DB::raw(
                '(
                    SELECT audit_item_id, SUM(quantity) AS actual_quantity
                    FROM audit_item_metas
                    GROUP BY audit_item_id
                ) foo'
            ), function ($join) {
                $join->on('audit_items.id', '=', 'foo.audit_item_id');
            })
            ->where('audit_items.system_quantity', '!=', 'foo.actual_quantity')
            ->where('audit_items.status', 3)
            ->whereHas('audit', function ($q) use ($warehouseId) {
                $q->where('warehouse_id', $warehouseId);
            })
            ->orderByDesc('audit_item_id');
    }
}