<?php


namespace Niyotail\Grids\Audit;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Audit\Audit;
use Niyotail\Models\Audit\AuditItem;
use Niyotail\Models\Audit\AuditItemAction;
use Niyotail\Models\Reason;
use Niyotail\Services\Audit\AuditItemService;

class HoldListGrid extends BaseGrid
{
    private static function getMarkExcessButton($audit, $auditItem)
    {
        return "<a type='button'
            class='btn btn-primary audit-item-action'
            data-modal='#modal-audit-excess'
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_EXCESS
            ]) . "' 
            data-form='#excess-form' 
            >Mark Excess</a>";
    }

    private static function getMarkLossButton($audit, $auditItem)
    {
        return "<a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-loss' 
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_LOSS
            ]) . "' 
            data-form='#loss-form' 
            >Mark Loss</a>";
    }

    private static function getReleaseButton($audit, $auditItem)
    {
        return "<a type='button' 
            class='btn btn-primary audit-item-action' 
            data-modal='#modal-audit-loss' 
            data-action = '" . route('admin::audits.items.action', [
                'audit' => $audit->id,
                'auditItem' => $auditItem->id,
                'action' => AuditItemAction::ACTION_RELEASE
            ]) . "' 
            data-form='#release-form' 
            >Mark Loss</a>";
    }

    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-hold-list");
        $grid->addColumn("audit_id", "Audit ID");
        $grid->addColumn('id', "Audit Item ID");
        $grid->addColumn("product.name", "Product Name");
        $grid->addColumn("storage.label", "Storage");
        $grid->addColumn("SQ", "SQ")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            return $auditItem->system_quantity;
        });
        $grid->addColumn("AQ", "AQ")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            return AuditItemService::actualQuantity($auditItem);
        });
        $grid->addColumn("", "Hold Quantity")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            $auditItemHoldAction = $auditItem->auditItemActions()->whereAction(AuditItemAction::ACTION_HOLD)->first();
            return $auditItemHoldAction->quantity ?? 0;
        });
        $grid->addColumn("", "Hold Reason")->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            $auditItemHoldAction = $auditItem->auditItemActions()->whereAction(AuditItemAction::ACTION_HOLD)->first();
            return $auditItemHoldAction->reason->reason_text ?? "";
        });
        $grid->addColumn("created_at", 'Created At');
        $grid->addColumn("Held By");
        $grid->addColumn("Actions", 'Action')->setCallback(function ($var, $row) {
            $auditItem = $row->getSrc();
            $audit = $auditItem->audit;
            if($audit->status == Audit::STATUS_COMPLETED){
                if ($auditItem->system_quantity > AuditItemService::actualQuantity($auditItem)) {
                    return static::getMarkLossButton($audit, $auditItem) . '<br>' . static::getReleaseButton($audit, $auditItem);
                }
                if ($auditItem->system_quantity < AuditItemService::actualQuantity($auditItem)) {
                    return static::getMarkExcessButton($audit, $auditItem) . '<br>' . static::getReleaseButton($audit, $auditItem);
                }
                return '';
            } else {
                return 'Complete Audit First!';
            }
        });
        return $grid;
    }

    protected function getQuery()
    {
        $warehouseId = $this->data['warehouseId'];
        return AuditItem::with(['audit', 'auditItemActions'])
            ->whereHas('audit', function ($q) use ($warehouseId) {
                $q->where('warehouse_id', $warehouseId)->where('status', '!=', 5);
            })
            ->whereHas('auditItemActions', function ($q) {
                $q->where('action', 0)->where('quantity', '>', 0);
            });
    }

}
