<?php
namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\TaxClass;

class TaxClassGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("tax-class-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn('taxes_count', 'Total Taxes');
        $grid->addColumn('status')->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::tax-class.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return TaxClass::withCount('taxes');
    }
}
