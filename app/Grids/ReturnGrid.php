<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\ReturnOrder;

class ReturnGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("return-grid");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id")->setWidth('80px')->setSortable()->setSearchFilter();
        $grid->addColumn("createdBy.name", 'Created By');
        $grid->addColumn("order.store.id", "Store Id")->setWidth('80px')->setSortable()->setSearchFilter();
        $grid->addColumn("order.store.name", "Store Name")->setSearchFilter()->setLink(route('admin::stores.profile', ['id' => '%s']), ['order.store_id'], 'link');
        $grid->addColumn("order.reference_id", "order Id")->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("shipment.id", "Shipment Id")->setSearchFilter();
        $grid->addColumn("shipment.invoice.reference_id", "Invoice No.")->setSearchFilter()->setLink(route('admin::invoices.index', ['id' => '%s']), ['shipment.invoice.id'], 'link');
        $grid->addColumn("items_count", "Unique Items")->setCallback(function ($var, $row) {
            $items = $row->getSrc()->orderItems;
            return $items->unique('sku')->count();
        });
        $grid->addColumn("items_count", "Total Units")->setCallback(function ($var, $row) {
//            if($row->getSrc()->items->sum('units') > 0)
//            {
//                return $var."<br><span class='tag tag-pending'>".$row->getSrc()->items->sum('units')." Unit(s)</span>";
//            }
//            else
//            {
//                return $var;
//            }
            return $row->getSrc()->items->sum('units');
        });;
        $grid->addColumn("creditNote.reference_id", "Credit Note")->setWidth("170px")->setSearchFilter()->setCallback(function ($val, $dp) {
            $creditNote = $dp->getSrc()->creditNote;
            if (!empty($creditNote)) {
                $link = route('admin::returns.credit-note', $creditNote->id);
                return '<a href="'.$link.'" target="_blank" class="link">'.$creditNote->reference_id.'</a>';
            }
        });
        $grid->addColumn("creditNote.amount", "Amount");
        $grid->addColumn("status")->setSelectFilter(ReturnOrder::getConstants('status'))->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-'.$status.'" >'.$status.'</div>';
        });
        $grid->addColumn('created_at')->setSortable()->isDate()->setDateTimeRangeFilter();
        return $grid;
    }

    protected function getQuery()
    {
        return ReturnOrder::withCount('items')->with('createdBy', 'order.store', 'orderItems', 'shipment.invoice', 'creditNote');
    }
}
