<?php

namespace Niyotail\Grids\Orders;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Order;
use Niyotail\Models\Tag;
use Niyotail\Models\User;

class OrderGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-orders");
        $grid->setDefaultSort(['created_at' => 'desc']);
        if ($this->data['picklist']) {
            $grid->addColumn('')->isSelect(false);
            $grid->addButton("Create Picklist", route('admin::picklists.store'), route('admin::picklists.index'));
        }
        $grid->addColumn("reference_id", "Order Id")->setSearchFilter();
        $grid->addColumn('picklists', 'Picklist ID')
            ->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
                $query = $dp->getBuilder();
                $query->whereHas('picklists', function ($picklistQuery) use ($val) {
                    $picklistQuery->where('picklist_id', $val);
                });
            })
            ->setCallback(function ($val, $dp) {
                if ($val->isNotEmpty()) {
                    $picklistId = $val->first()->id;
                    $href = route('admin::picklists.items', ['id' => $picklistId]);
                    return "<a class='link' href='$href' target='_blank'>$picklistId</a>";
                }
                return "";

            });

        $grid->addColumn("store.name", "Store")->setSearchFilter();
//        $grid->addColumn("store.beat.name", "Beat")->setSearchFilter();
        $grid->addColumn("items", "Items")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return $row->items->unique('sku')->count();
        });
        $grid->addColumn("items_count", "Qty");
        $grid->addColumn("pretty_discount", 'discount')->setMinWidth('120px');
        $grid->addColumn("pretty_total", 'total')->setMinWidth('120px');
        $grid->addColumn("status")
            ->setSelectFilter($this->getOrderStatusOptions())
            ->setCallback(function ($var, $row) {
                $status = $row->getSrc()->status;
                return '<span class="tag tag-' . $status . '" >' . $status . '</div>';
            });
        $grid->addColumn("type", "Type")->setSelectFilter(Order::getConstants('TYPE'));
        $grid->addColumn("tags", "Tags")
            ->setSelectFilter($this->getTagOptions())
            ->setFilteringFunc(function ($val, $dp) {
                $query = $dp->getBuilder();
                $query->whereHas('tags', function ($query) use ($val) {
                    $query->where('type', Tag::TYPE_ORDER)->where('name', $val);
                });
            })
            ->setCallback(function ($val, $dp) {
                $row = $dp->getSrc();
                $retVal = '';
                foreach ($row->tags->pluck('name')->toArray() as $tag) {
                    $retVal .= '<span class="tag tag-' . $tag . '" >' . $tag . '</span> <br><br>';
                }
                return $retVal;
            });
        $grid->addColumn('createdBy.name', 'created By')->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHasMorph('createdBy', '*', function ($query, $type) use ($val) {
                if ($type == User::class) {
                    $query->where('first_name', 'like', "$val%")
                        ->orWhere('last_name', 'like', "$val%");
                } else {
                    $query->where('name', 'like', "%$val%");
                }
            });
        });
        $grid->addColumn('created_by_type', "created Type")->setSelectFilter($this->getCreatedTypeOptions());
        $grid->addColumn("source")->setSelectFilter(Order::getConstants('source'));
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::orders.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');

        return $grid;
    }

    private function getOrderStatusOptions()
    {
        return Order::getConstants("STATUS");
    }

    private function getTagOptions()
    {
        $options = [];
        $orderTags = Tag::order()->pluck('name')->toArray();
        foreach ($orderTags as $orderTag) {
            $options[$orderTag] = $orderTag;
        }

        return $options;
    }

    private function getCreatedTypeOptions()
    {
        return [
            'agent' => 'agent',
            'user' => 'user',
            'employee' => 'employee'
        ];
    }

    protected function getQuery()
    {
        $picklist = $this->data['picklist'];
        $orderQuery = Order::with('store', 'store', 'createdBy', 'tags', 'picklists')
            ->withCount("items");

        if ($picklist) $orderQuery = $orderQuery->where('status', Order::STATUS_CONFIRMED)->where('warehouse_id', '!=', 1);
        return $orderQuery;
    }
}
