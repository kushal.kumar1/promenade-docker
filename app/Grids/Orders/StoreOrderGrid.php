<?php

namespace Niyotail\Grids\Orders;

use Illuminate\Support\Facades\Gate;
use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\User;
use Niyotail\Models\Warehouse;

class StoreOrderGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-store-orders");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id", "Id")->setSearchFilter();
        $grid->addColumn("store.name", "Store")->setSearchFilter();
        $grid->addColumn("warehouse.name", "Warehouse")->setSelectFilter(Warehouse::all()->pluck('name', 'id')->toArray(), 'id');
        $grid->addColumn("items", "Qty")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return $row->items->sum('quantity');
        });
        $grid->addColumn("items_count", "Items");
        $grid->addColumn("total", 'total')->setMinWidth('120px');
        $grid->addColumn("status")->setSelectFilter($this->getOrderStatusOptions())->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-' . $status . '" >' . $status . '</div>';
        });
        $grid->addColumn("Unfulfillment Reason")->setCallback(function ($var, $row) {
            $storeOrder = $row->getSrc();
            if ($storeOrder->status == StoreOrder::STATUS_UNFULFILLED) {
                return $storeOrder->cancellation_reason;
            }
        });
        $grid->addColumn("order.reference_id", "order")->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn('type')->setSelectFilter(StoreOrder::getConstants('type'));
        $grid->addColumn('createdBy.name', 'created By')->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHasMorph('createdBy', '*', function ($query, $type) use ($val) {
                if ($type == User::class) {
                    $query->where('first_name', 'like', "$val%")
                        ->orWhere('last_name', 'like', "$val%");
                } else {
                    $query->where('name', 'like', "%$val%");
                }
            });
        });
        $grid->addColumn("source")->setSelectFilter(StoreOrder::getConstants('source'));
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::store.orders.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
//        if (Gate::allows('retry-store-order')) {
//            $grid->addColumn('retry')->setCallback(function ($val, $dp) {
//               $row = $dp->getSrc();
//               if($row->source == StoreOrder::SOURCE_STORE_PURCHASE && $row->status == StoreOrder::STATUS_UNFULFILLED){
//                   return "<button class='btn btn-sm btn-success' data-id='$row->id' data-modal='#retry-modal'><i class='fa fa-recycle'></i>Retry</button>";
//               }
//            });
//        }
        return $grid;
    }

    private function getCreatedTypeOptions()
    {
        return [
            'agent' => 'agent',
            'user' => 'user',
            'employee' => 'employee'
        ];
    }

    private function getOrderStatusOptions()
    {
        return StoreOrder::getConstants("STATUS");
    }

    protected function getQuery()
    {
        return StoreOrder::with('store', 'createdBy', 'order', 'warehouse')
            ->withCount("items");
    }
}
