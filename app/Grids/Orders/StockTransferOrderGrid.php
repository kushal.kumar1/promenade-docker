<?php
namespace Niyotail\Grids\Orders;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Order;
use Niyotail\Models\User;
use Niyotail\Grids\BaseGrid;

class StockTransferOrderGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-stock-transfer-orders");
        $grid->setDefaultSort(['created_at' => 'desc']);
        if($this->data['picklist']) {
            $grid->addColumn('')->isSelect();
            $grid->addButton("Create Picklist", route('admin::picklists.store'), route('admin::picklists.index'));
        }
        $grid->addColumn("reference_id", "Order Id")->setSearchFilter();
        $grid->addColumn("store.name", "Destination Warehouse")->setSearchFilter();
//        $grid->addColumn("store.beat.name", "Beat")->setSearchFilter();
        $grid->addColumn("items", "Items")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return $row->items->unique('sku')->count();
        });
        $grid->addColumn("items_count", "Qty");
        $grid->addColumn("pretty_discount", 'discount')->setMinWidth('120px');
        $grid->addColumn("pretty_total", 'total')->setMinWidth('120px');
        $grid->addColumn("status")->setSelectFilter($this->getOrderStatusOptions())->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-'.$status.'" >'.$status.'</div>';
        });
        $grid->addColumn("type", "Type")->setSelectFilter(Order::getConstants('TYPE'));
//        $grid->addColumn("Tags")->setCallback(function ($val, $dp) {
//            $row = $dp->getSrc();
//            $tags = $row->tags->pluck('name')->toArray();
//            return !empty($tags) ? '<span class="tag" >'.implode(',',$tags).'</div>' : '';
//        });
        $grid->addColumn('createdBy.name', 'created By')->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHasMorph('createdBy', '*', function ($query, $type) use ($val) {
                if ($type == User::class) {
                    $query->where('first_name', 'like', "$val%")
                        ->orWhere('last_name', 'like', "$val%");
                } else {
                    $query->where('name', 'like', "%$val%");
                }
            });
        });
        $grid->addColumn('created_by_type', "created Type")->setSelectFilter($this->getCreatedTypeOptions());
        $grid->addColumn("source")->setSelectFilter(Order::getConstants('source'));
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::orders.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');

        return $grid;
    }

    private function getCreatedTypeOptions()
    {
        return [
            'agent' => 'agent',
            'user' => 'user',
            'employee' => 'employee'
        ];
    }

    private function getOrderStatusOptions()
    {
        return Order::getConstants("STATUS");
    }

    protected function getQuery()
    {
        $picklist = $this->data['picklist'];
        $orderQuery =  Order::where('type', Order::TYPE_STOCK_TRANSFER)->with('store', 'store','createdBy','tags')
            ->withCount("items");

        if($picklist) $orderQuery = $orderQuery->where('status', Order::STATUS_CONFIRMED)->where('warehouse_id', '!=', 1);
        return $orderQuery;
    }
}
