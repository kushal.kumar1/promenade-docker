<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\AutoReplenishDemand;

class DemandGenerationGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('demand-generation-grid');
        $grid->addColumn("id", "")
            ->setCallback(function ($id) {
                return "<input type='checkbox' class='demands_select' name='demands[]' value='$id' id='demand_$id' />";
        });
        $grid->addColumn("status", "Status")->setSearchFilter();
        $grid->addColumn("reorder_qty", "Reorder Quantity")->setSortable();
        $grid->addColumn("pending_qty", "Pending Quantity");
        $grid->addColumn("group.name", "Group")->setSearchFilter();
        $grid->addColumn("group.id", "Group Id")->setSearchFilter();
        $grid->addColumn("group.brand.name", "Brand Name")->setSearchFilter();
        $grid->addColumn("group.brand.marketer.name", "Marketer Name")->setSearchFilter();
        $grid->addColumn("group.collection.name", "C1")->setSearchFilter();
        $grid->addColumn("warehouse.name", "Warehouse")->setSearchFilter();
        $grid->addColumn("created_at", "Created At")->setSortable();
        return $grid;
    }

    protected function getQuery()
    {
        return (new AutoReplenishDemand())->newQuery()->with('warehouse', 'group.brand', 'group.brand.marketer', 'group.collection');
    }
}
