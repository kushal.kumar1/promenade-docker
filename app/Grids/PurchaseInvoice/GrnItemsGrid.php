<?php


namespace Niyotail\Grids\PurchaseInvoice;


use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\PurchaseInvoiceGrnItem;

class GrnItemsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-grn-items");
        $grid->addColumn('product_id', 'pid')->setSearchFilter();
        $grid->addColumn('sku', 'SKU')->setSearchFilter();
        $grid->addColumn('quantity', 'Received Quantity')->setSortable();
        $grid->addColumn('item.quantity', 'Invoiced Quantity')->setSortable();
        $grid->addColumn('item.purchaseInvoice.invoice_date', 'Invoice Date')->setSortable();
        $grid->addColumn('item.purchaseInvoice.vendor_ref_id', 'Vendor Ref ID')->setSearchFilter();
        $grid->addColumn('item.purchaseInvoice.is_direct_store_purchase','Is Store Purchase')->setBooleanFilter('Yes', 'No');
        return $grid;
    }

    protected function getQuery()
    {
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return PurchaseInvoiceGrnItem::whereHas('grn', function ($grnQuery) use($selectedWarehouse){
            $grnQuery->where('warehouse_id', $selectedWarehouse);
        })
            ->with('item', 'item.purchaseInvoice', 'grn');
    }
}