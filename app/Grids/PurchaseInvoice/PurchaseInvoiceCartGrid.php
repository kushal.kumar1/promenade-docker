<?php


namespace Niyotail\Grids\PurchaseInvoice;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\PurchaseInvoiceCart;

class PurchaseInvoiceCartGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice-cart");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("purchaseOrder.id", "Purchase Order")->setCallback(
            function ($val, $dp) {
                if ($dp->getSrc()->purchase_order_id) {
                    return '<a href="' . route("admin::purchase-order.single", [$dp->getSrc()->purchase_order_id]) . '" class="btn btn-primary">View</a>';
                } else {
                    return '';
                }
            }
        );
        $grid->addColumn("vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("vendor_ref_id", 'Reference id')->setSearchFilter();
        $grid->addColumn("invoice_date")->setDateTimeRangeFilter();
        $grid->addColumn("total")->setCallback(function ($var, $row) {
            $total = $row->getSrc()->total;
            return number_format($total, 2, '.', '');
        })->setSortable();
        $grid->addColumn("status");
        $grid->addColumn("view", "Action")->setCallback(function ($val, $dp) {
            return '<a href="' . route("admin::purchase-invoices.cart", [$dp->getSrc()->vendor_id]) . '" class="btn btn-primary">View</a>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        return PurchaseInvoiceCart::with('purchaseOrder.vendor')
            ->where(['status' => PurchaseInvoiceCart::STATUS_DRAFT]);
    }
}
