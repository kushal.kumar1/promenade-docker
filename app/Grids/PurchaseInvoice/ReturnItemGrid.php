<?php


namespace Niyotail\Grids\PurchaseInvoice;


use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\PurchaseInvoiceItem;

class ReturnItemGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice-return-item");
        $grid->addColumn('')->isSelect();
        $grid->addColumn('product_id')->setSearchFilter();
        $grid->addColumn('product.name')->setSearchFilter();
        $grid->addColumn('total_invoiced_cost', 'cost');
        $grid->addColumn('sku', 'sku');
        $grid->addColumn('quantity', 'quantity');
        $grid->addColumn('received_quantity', 'Return (units)')->setCallback(function($val, $row) {
            $data = $row->getSrc();
            $totalReceivedQty = $data->grnItems->sum('quantity');
            return '<input type="number" name="return_qty['.$data->id.']" max="'.$totalReceivedQty.'">';
        });
        $grid->addButton("Create debit note",
            route('admin::purchase-invoices.returnItems.save'), route('admin::purchase-invoices.index'));
        return $grid;
    }

    protected function getQuery()
    {
        $q = PurchaseInvoiceItem::with('product', 'grnItems');
        if(!empty($this->data['invoice_id']))
        {
            $q->Invoice($this->data['invoice_id']);
        }
        return $q;
    }
}