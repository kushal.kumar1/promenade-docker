<?php


namespace Niyotail\Grids\PurchaseInvoice;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\PurchaseInvoice;

class PurchaseInvoiceGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("reference_id",'invoice id')->setSearchFilter();
        $grid->addColumn("vendor_ref_id")->setSearchFilter();
        $grid->addColumn('type')->setSelectFilter(PurchaseInvoice::getConstants('TYPE'));
        $grid->addColumn("invoice_date")->setDateTimeRangeFilter();
        $grid->addColumn("total")->setCallback(function ($var, $row) {
            $total = $row->getSrc()->total;
            return number_format($total,2,'.','');
        })->setSortable();
        $grid->addColumn("status")->setSortable()->setSearchFilter();
        $grid->addColumn("view", "Action")->setCallback(function($val, $dp) {
            return '<a href="'.route("admin::purchase-invoices.editInvoice", [$dp->getSrc()->id]).'" class="btn btn-primary">View</a>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        $baseQuery = PurchaseInvoice::with('purchaseOrder.vendor');
        if(!empty($this->data['po_id']))
        {
            $baseQuery->WherePurchaseOrder($this->data['po_id']);
        }
        return $baseQuery;
    }
}
