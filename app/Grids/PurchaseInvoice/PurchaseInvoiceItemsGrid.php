<?php


namespace Niyotail\Grids\PurchaseInvoice;


use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceItem;

class PurchaseInvoiceItemsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice-item");
        $grid->addColumn("purchaseInvoice.vendor.name", "Vendor")->setSearchFilter();
        /*$grid->addColumn("purchaseInvoice.reference_id",'invoice id')->setSearchFilter();*/
        $grid->addColumn("purchaseInvoice.vendor_ref_id", 'Vendor ref ID')->setSearchFilter();
        $grid->addColumn("purchaseInvoice.status", 'Invoice status')->setSearchFilter();
        $grid->addColumn("purchaseInvoice.invoice_date", 'Invoice date')->setMinWidth('120px')->setDateTimeRangeFilter();
        $grid->addColumn("product.id", 'Product')->setSearchFilter();
        $grid->addColumn("product.name", 'PID')->setSearchFilter();
        $grid->addColumn("product.group.id", 'GID')->setSearchFilter();
        $grid->addColumn("product.brand.name", 'Brand')->setSearchFilter();
        $grid->addColumn("product.brand.marketer.name", 'Marketer')->setSearchFilter();
        $grid->addColumn("productVariant.mrp", 'MRP');
        $grid->addColumn("sku")->setSearchFilter();
        $grid->addColumn("base_cost")->setSearchFilter();
        $grid->addColumn("scheme_discount")->setSearchFilter();
        $grid->addColumn("total_taxable")->setSearchFilter();
        $grid->addColumn("taxes", 'Tax Percent')->setSearchFilter();
        $grid->addColumn("tax", 'Tax')->setSearchFilter();
        $grid->addColumn("quantity")->setSearchFilter();
        $grid->addColumn("total_invoiced_cost", 'Invoiced cost')->setSearchFilter();

        return $grid;
    }

    protected function getQuery()
    {
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return PurchaseInvoiceItem::whereHas('purchaseInvoice', function ($pIQuery) use ($selectedWarehouse){
            $pIQuery->where('warehouse_id', $selectedWarehouse);
        })
            ->with('purchaseInvoice', 'product');
    }
}