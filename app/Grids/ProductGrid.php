<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Product;
use Image;

class ProductGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-products");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("IMG")->setWidth('80px')->setCallback(function ($val, $row) {
            $product = $row->getSrc();
            $image = Image::getSrc($product->primaryImage, 80);
            return '<img class="bd" src="'.$image.'" width="80px"/>';
        });
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("brand.name", "Brand")->setSearchFilter();
        $grid->addColumn("group_id", "GId")->setSearchFilter();
        $grid->addColumn("newCatL4.id", " CL4 Id")->setSearchFilter();
        $grid->addColumn("newCatL4.name", "CL4 Name")->setSearchFilter();
        $grid->addColumn("group.warehouseAbc.class_abc", "Class")->setSearchFilter();
        $grid->addColumn("unitVariant.mrp", "Unit MRP");
        $grid->addColumn("code", "Marketer Code")->setSearchFilter();
        $grid->addColumn("hsn_sac_code", "HSN");
        $grid->addColumn("barcode")->setSearchFilter();
        $grid->addColumn("taxClass.name", "tax class");
        $grid->addColumn("current_inventory", "Stock");
        $grid->addColumn("tags")->setSearchFilter()->setCallback(function ($val, $row) {
            $tags = $row->getSrc()->tags;
            $html="";
            foreach ($tags as $tag) {
                $html .= "<span class='tag tag-product'>".$tag->name."</span>";
            }
            return $html;
        })->setFilteringFunc(function ($val, $dp) {
            $builder = $dp->getBuilder();
            $builder->whereHas('tags', function ($query) use ($val) {
                $query->where('name', 'like', "%$val%");
            });
        });
        $grid->addColumn("placement")->setSearchFilter()->setCallback(function ($val, $row) {
            $racks = $row->getSrc()->warehouseRacks->pluck('reference')->toArray();
            return implode(",",$racks);
        })->setFilteringFunc(function ($val, $dp) {
            $builder = $dp->getBuilder();
            $builder->whereHas('warehouseRacks', function ($query) use ($val) {
                $query->where('reference', 'like', "%$val%");
            });
        });
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::products.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        $selectedWarehouse = app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return Product::inventoryCount([$selectedWarehouse])->with(
            'images',
            'taxClass',
            'brand.marketer',
            'tags',
            'group.warehouseAbc',
            'newCatL4',
            'unitVariant',
            'warehouseRacks');
    }
}
