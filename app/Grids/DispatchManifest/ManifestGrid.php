<?php


namespace Niyotail\Grids\DispatchManifest;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Manifest;

class ManifestGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("manifest-grid");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("agent.name", "delivery agent")->setSearchFilter();
        $grid->addColumn('vehicle_number')->setSearchFilter();
        $grid->addColumn("shipments_count", "Shipments");
        $grid->addColumn("boxes")->setCallback(function ($var, $row) {
            $boxes = $row->getSrc()->shipments->sum('boxes');
            return $boxes;
        });
        $grid->addColumn('locations', 'Warehouse Locations')->setCallback(function ($val, $dp){
            return implode(', ', $val->pluck('reference')->toArray());
        });

        $grid->addColumn("status")->setSelectFilter(Manifest::getConstants('status'))->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-'.$status.'" >'.$status.'</div>';
        });
        $grid->addColumn("createdBy.name", "created By");
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        // $grid->addColumn('updated_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::manifests.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return Manifest::withCount('shipments')->with('createdBy', 'agent', 'shipments', 'locations');
    }
}
