<?php


namespace Niyotail\Grids\DispatchManifest;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\Shipment;

class CreateManifestGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("create-manifest-grid");
        $grid->addColumn('')->isSelect();
        $grid->addColumn("order.reference_id", "Order")->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order.id'], 'link');
        $grid->addColumn('order.created_at', 'Order Date')->setSortable()->isDate();
        $grid->addColumn("id", "shipment")->setSearchFilter()->setCallback(function ($var, $row) {
            $shipment = $row->getSrc();
            return '#' . $shipment->id . ' <span class="tag tag-' . $shipment->status . '" >' . $shipment->status . '</span>';
        });
        $grid->addColumn("order.store.name", "Store")->setSearchFilter()->setLink(route('admin::stores.profile', ['id' => '%s']), ['order.store_id'], 'link');
        $grid->addColumn("invoice.reference_id", "Invoice")->setSearchFilter();
//            ->setLink(route('admin::invoices.index', ['id' => '%s']), ['invoice.id'], 'link');
        $grid->addColumn('invoice.created_at', 'Invoice Date')->setSortable()->isDate();
        $grid->addColumn("invoice.amount", "Amount")->setSortable();
        $grid->addButton("Create New Manifest", route('admin::manifests.store'), route('admin::manifests.index'));

        return $grid;
    }

    protected function getQuery()
    {
        return Shipment::canBeShipped()->with('order.store', 'invoice');
    }
}
