<?php

namespace Niyotail\Grids\ModificationRequests;

use Exception;
use Illuminate\Support\Facades\Storage;
use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Agent;
use Niyotail\Models\Employee;
use Niyotail\Models\ModificationRequest;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\PurchaseInvoiceItemModificationRequest;
use Niyotail\Models\PurchaseInvoiceModificationRequest;
use Niyotail\Models\PurchasePlatform;
use Niyotail\Models\ReturnOrder;
use Niyotail\Models\Transaction;
use Niyotail\Models\TransactionDeletionRequest;
use Niyotail\Models\Vendor;
use Niyotail\Models\VendorsAddresses;

class ModificationRequestGrid extends BaseGrid
{
    protected string $modificationRequestType;

    /**
     * @throws Exception
     */
    public function __construct($modificationRequestType)
    {
        parent::__construct();

        if (!ModificationRequest::isValidType($modificationRequestType)) {
            throw new Exception("Invalid ModificationRequestType");
        }
        $this->modificationRequestType = $modificationRequestType;
    }

    /**
     * @throws Exception
     */
    protected function setGrid(Grid $grid): Grid
    {
        switch ($this->modificationRequestType) {
            case ModificationRequest::TYPE_TRANSACTION:
            {
                $grid->setName("grid-modification-request-purchase-invoice");
                $grid->addColumn("id")->setSortable();
                $grid->addColumn("transaction_id")
                    ->setCallback(function ($var, $row) {
                        try {
                            if ($row->getSrc()->status != "completed") {
                                $transaction = Transaction::where(
                                    "transaction_id", $row->getSrc()->transaction_id
                                )->firstOrFail();
                                return $transaction->transaction_id . '  <a class="link-primary" href="'
                                    . route("admin::transactions.edit", [
                                        'id' => $transaction->id
                                    ]) . '" style="color:blue;text-decoration:underline;">(view)</a>';
                            }
                            return $row->getSrc()->transaction_id . " (deleted)";
                        } catch (Exception $ignored) {
                            return $row->getSrc()->transaction_id;
                        }
                    })
                    ->setSearchFilter();
                $grid->addColumn("delete_reason");

                break;
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE:
            {
                $grid->setName("grid-modification-request-purchase-invoice");
                $grid->addColumn("id")
                    ->setSortable();
                $grid->addColumn("purchase_invoice_id")
                    ->setSearchFilter();
                $grid->addColumn("vendor_id")
//                    ->setSearchFilter()
                    ->setCallback(function ($var, $row) {
                        try {
                            $vendor = Vendor::findorfail($row->getSrc()->vendor_id);
                            return "{$vendor->id} ({$vendor->name} | {$vendor->type})";
                        } catch (Exception $ignored) {
                            return $row->getSrc()->vendor_id;
                        }
                    });
                $grid->addColumn("vendor_address_id")
//                    ->setSearchFilter()
                    ->setCallback(function ($var, $row) {
                        try {
                            $vendorAddress = VendorsAddresses::findorfail($row->getSrc()->vendor_address_id);
                            return "{$vendorAddress->id} ({$vendorAddress->address})";
                        } catch (Exception $ignored) {
                            return $row->getSrc()->vendor_id;
                        }
                    });
                $grid->addColumn("date", "invoice date")
                    ->setSortable()
                    ->isDate()
                    ->setDateTimeRangeFilter();
                $grid->addColumn("image")
                    ->setCallback(function ($var, $row) {
                        $invoice_src = route('admin::modification-requests.purchase-invoice.download-invoice', [
                            'modificationRequestType' => ModificationRequest::TYPE_PURCHASE_INVOICE,
                            'modificationRequestId' => $row->getSrc()->id
                        ]);
                        if (empty($row->getSrc()->image) || empty($invoice_src)) {
                            return "";
                        }
                        return '<a href="' . $invoice_src . '" download class = "btn btn-primary"><i class="fa fa-download"></i>Download</a>';
                    });
                $grid->addColumn("platform_id")
//                    ->setSearchFilter()
                    ->setCallback(function ($var, $row) {
                        try {
                            $platform = PurchasePlatform::findorfail($row->getSrc()->platform_id);
                            return "{$platform->id} ({$platform->platform})";
                        } catch (Exception $ignored) {
                            return $row->getSrc()->platform_id;
                        }
                    });
                $grid->addColumn("eway_number")
                    ->setSearchFilter();
                $grid->addColumn("forecast_payment_date")
                    ->setSortable()
                    ->isDate()
                    ->setDateTimeRangeFilter();
                $grid->addColumn("action", "Type")
                    ->setSelectFilter(ModificationRequest::getActions())
                    ->setCallback(function ($var, $row) {
                        $type = $row->getSrc()->action;
                        return '<span class="tag tag-' . $type . '" >' . $type . '</div>';
                    });

                break;
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
            {
                $grid->setName("grid-modification-request-purchase-invoice-item");
                $grid->addColumn("id")->setSortable();
                $grid->addColumn("purchase_invoice_item_id")->setSearchFilter();
                $grid->addColumn("product_id")->setSearchFilter();
                $grid->addColumn("quantity");
                $grid->addColumn("base_cost");
                $grid->addColumn("scheme_discount");
                $grid->addColumn("other_discount");
                $grid->addColumn("post_tax_discount");
                $grid->addColumn("action", "Type")
                    ->setSelectFilter(ModificationRequest::getActions())
                    ->setCallback(function ($var, $row) {
                        $type = $row->getSrc()->action;
                        return '<span class="tag tag-' . $type . '" >' . $type . '</div>';
                    });
                break;
            }
            default :
                throw new Exception();
        }


        $grid->addColumn("employee_id")
            ->setSearchFilter()
            ->setCallback(function ($var, $row) {
                $employee = Employee::query()->findOrFail($row->getSrc()->employee_id);
                return "{$employee->id} ({$employee->name})";
            });
        $grid->addColumn("status")->setSelectFilter(ModificationRequest::getStatuses())
            ->setCallback(function ($var, $row) {
                $status = $row->getSrc()->status;
                return '<span class="tag tag-' . $status . '" >' . $status . '</div>';
            });
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("updated_at")->setSortable()->isDate()->setDateTimeRangeFilter();


        $grid->addColumn("delete", "Action")
            ->setCallback(function ($var, $row) {

                if ($row->getSrc()->status === ModificationRequest::STATUS_PENDING) {


//                return '<a href="'.route("admin::purchase-invoices.editInvoice", [$dp->getSrc()->id]).'" class="btn btn-primary">View</a>';
                    $modificationRequestId = $row->getSrc()->id;

                    $buttonHtml = "";
                    $buttonHtml = $buttonHtml . '<button class="btn btn-primary"';
                    $buttonHtml = $buttonHtml . 'onclick="(function(){
                    if (confirm(\'Are you sure you want to delete the Modification Request>\')) {
                        $(\'#delete-request-id\').val(' . $modificationRequestId . ');
                        $(\'#delete-modification-request-form\').submit();
                     }
                })(); return false;"';
                    $buttonHtml = $buttonHtml . '>';
                    $buttonHtml = $buttonHtml . 'Delete!';
                    $buttonHtml = $buttonHtml . '</button>';

                    return $buttonHtml;
                }
            });

        return $grid;
    }

    protected function getQuery()
    {
        switch ($this->modificationRequestType) {
            case ModificationRequest::TYPE_TRANSACTION:
            {
                return TransactionDeletionRequest::query();
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE:
            {
                return PurchaseInvoiceModificationRequest::query();
            }
            case ModificationRequest::TYPE_PURCHASE_INVOICE_ITEM:
            {
                return PurchaseInvoiceItemModificationRequest::query();
            }
        }
    }
}