<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\Utils;
use Niyotail\Models\PurchaseInvoiceDebitNote;

class DebitNoteGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice-debit-notes");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("invoice.vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("invoice.reference_id",'invoice id')->setSearchFilter();
        $grid->addColumn("invoice.vendor_ref_id")->setSearchFilter();
        $grid->addColumn("invoice.invoice_date", "Invoice date")->setDateTimeRangeFilter()->setSortable();
        $grid->addColumn("view", "Download")->setCallback(function($val, $dp) {
            return '<a href="'.route("admin::purchase-invoices.debit-note-pdf", [$dp->getSrc()->id]).'" class="btn btn-primary">View</a>';
        });
        return $grid;

        //
    }

    protected function getQuery()
    {
        return PurchaseInvoiceDebitNote::with('invoice');
    }
}