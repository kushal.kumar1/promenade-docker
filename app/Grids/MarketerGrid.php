<?php


namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Marketer;
use Niyotail\Models\MarketerLevel;
use Niyotail\Models\Order;

class MarketerGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("marketer-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("alias")->setSearchFilter();
        $grid->addColumn('code')->setSearchFilter();
        $grid->addColumn('level_id')->setSearchFilter();
        $grid->addColumn('level.name')->setSearchFilter();
        $grid->addColumn('edit', 'Action')->setLink(route('admin::marketers.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return Marketer::with('level');
    }
}
