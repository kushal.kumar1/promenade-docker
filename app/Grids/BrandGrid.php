<?php


namespace Niyotail\Grids;

use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Brand;
use Niyotail\Helpers\Image;

class BrandGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("badge-grid");
        $grid->addColumn("id");
        $grid->addColumn('logo', 'Logo')->setCallback(function ($val, $row) {
            $brand = $row->getSrc();
            return '<img class="bd" src='. Image::getSrc($brand, 60, 60).'/>';
        });
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn('products_count', 'products');
        $grid->addColumn('edit', 'Action')->setCallback(function ($val, $row) {
            $brand = $row->getSrc();
            return '<button class="btn btn-raw" data-modal="#modal-badge-add" data-id="' . $brand->id . '" data-name="' . $brand->name . '" id="edit-brand"><i class="fa fa-edit"></i></button>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        return Brand::withCount('products');
    }
}
