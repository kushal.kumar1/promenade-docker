<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Agent;

class AgentGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-users");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("code");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("email")->setSearchFilter();
        $grid->addColumn("mobile")->setSearchFilter();
        $grid->addColumn("type")->setSelectFilter($this->getAgentTypeOptions());
        $grid->addColumn("verified")->setBooleanFilter('Yes', 'No');
        $grid->addColumn("status")->setBooleanFilter('Active', 'Disabled');
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("profile", "Action")->setLink(route('admin::agents.profile', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return (new Agent())->newQuery();
    }

    private function getAgentTypeOptions()
    {
        return Agent::getEnum("type");
    }
}
