<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Role;

class RoleGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("role-grid");
        $grid->addColumn("id");
        $grid->addColumn("name", "Role")->setSearchFilter();
        $grid->addColumn("employees_count", "No. of Users");
        $grid->addColumn("edit", "Action")->setLink(route('admin::roles.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return Role::withCount('employees');
    }
}
