<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Models\RateCreditNote;

class RateCreditNoteGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("rate-credit-note-grid");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("reference_id", "Reference Id")->setSearchFilter();
        $grid->addColumn("store_id", "Store ID")->setSearchFilter();
        $grid->addColumn("reference_id", 'File')->setCallback(function ($val, $row) {
            $rateCreditNote = $row->getSrc();
            $link = route("admin::rate-credit-note.get-file", $rateCreditNote->id);
            return "<a class='link' href='$link' target='_blank'>Download</a>";
        });
        $grid->addColumn("store.name", "Store")->setSearchFilter();
        $grid->addColumn("type", "Type")->setSearchFilter();
        $grid->addColumn("financial_year", "Financial Year");
        $grid->addColumn("number", "Number");
        $grid->addColumn("subtotal");
        $grid->addColumn("tax");
        $grid->addColumn("total")->setSearchFilter();
        $grid->addColumn("status");
        $grid->addColumn('created_at')->setSortable()->isDate()->setDateTimeRangeFilter();
        return $grid;
    }

    protected function getQuery()
    {
        return RateCreditNote::with('store');
    }
}