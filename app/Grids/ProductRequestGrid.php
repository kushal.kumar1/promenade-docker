<?php


namespace Niyotail\Grids;


use Carbon\Carbon;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\ProductRequest;

class ProductRequestGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-product-request");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn('created_at')->setSortable()->setCallback(function($val, $dp) {
            return Carbon::parse($dp->getSrc()->created_at)->format('jS M Y');
        });
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("brand.name", "Brand")->setSearchFilter();
        $grid->addColumn("barcode")->setSearchFilter();;
        $grid->addColumn("unit_mrp", "MRP");
        $grid->addColumn("collection.name", "Category");
        $grid->addColumn("status", "Status")->setSelectFilter(ProductRequest::getConstants('STATUS'));
        $grid->addColumn('edit', 'Action')->setCallback(function ($dp, $val) {
            $data = $val->getSrc();
            $status = $data->status;
            if($status == "product_created") {
                $link = route('admin::products.edit', $data->product_id);
                return '<a href="'.$link.'" class="btn btn-sm">View Product</a>';
            }
            else if($status == "pending") {
                $link = route('admin::product_request.edit', $data->id);
                return '<a href="'.$link.'" class="btn btn-sm">View</a>';
            }
        });
        return $grid;
    }

    protected function getQuery()
    {
        return ProductRequest::with('brand', 'collection');
    }
}
