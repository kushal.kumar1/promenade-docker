<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Inventory;
use Niyotail\Models\Warehouse;

class InventoryGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-inventory");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
//        $grid->addColumn("type")->setSelectFilter(Inventory::getConstants('type'));
        $grid->addColumn("warehouse.name", "warehouse")->setSelectFilter(Warehouse::all()->pluck('name', 'id')->toArray(), 'id');
        $grid->addColumn("quantity")->setSearchFilter(Grid::OPERATOR_EQ);
        $grid->addColumn("batch_number")->setSearchFilter(Grid::OPERATOR_EQ);
        $grid->addColumn("mfg_date")->setDateTimeRangeFilter();
        $grid->addColumn("expiry_date", "Expiry")->setDateTimeRangeFilter();
        $grid->addColumn("unit_cost_price");
        $grid->addColumn('status')->setBooleanFilter("Active", "Inactive");
        $grid->addColumn('inactive_reason');
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
//             route('admin::inventory.transactions', ['id'=>$row->id]);
            return "<button class='btn btn-sm btn-status' data-id='$row->id' data-status='$row->status' data-inactive_reason='$row->inactive_reason' type='button'>Status</button>";
////            $html .= "<button class=\"btn btn-sm btn-type\" data-id=\"".$row->id."\" type=\"button\">Edit Type</button>";
////            $html .= "<button class=\"btn btn-sm btn-warehouse\" data-id=\"".$row->id."\" type=\"button\">Transfer Stock</button>";
//            return $statusButton. $viewTransactions;
        });
        $grid->addColumn('Details')->setLink(route('admin::inventory.transactions', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        $productId = $this->data['productId'];
        return Inventory::with('warehouse')->where('product_id', $productId);
    }
}
