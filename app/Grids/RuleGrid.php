<?php

namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Rule;

class RuleGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("rules-grid");
        $grid->addColumn("id");
        $grid->addColumn("type")->setSelectFilter(Rule::getConstants('type'));
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("offer")->setSelectFilter(Rule::getConstants('offer'));
        $grid->addColumn("value", "Offer Value");
        $grid->addColumn("position")->setSortable();
        $grid->addColumn("should_stop", "Last Offer")->setBooleanFilter();
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::rules.detail', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return Rule::query();
    }
}
