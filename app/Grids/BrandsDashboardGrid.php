<?php


namespace Niyotail\Grids;


use Illuminate\Support\Facades\Cookie;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\StatsHelper;
use Niyotail\Services\BrandService;

class BrandsDashboardGrid extends BaseGrid
{
    protected $data;
    protected $brandsService;
    protected $gridName;

    public function __construct($data)
    {
        $this->data = $data;
        $this->brandsService = new BrandService();
        $this->gridName = "brands-dashboard-grid";
    }

    protected function setGrid(Grid $grid)
    {
        $grid->setName($this->gridName);
        $grid->setDefaultPageSize(100);

        $type = !empty($this->data['types']) ? $this->data['types'][0] : unserialize(Cookie::get("table-types"))[0] ;

        if( in_array($type, ["top_brands", "top_marketers", "top_stores"]))
        {
            $grid->setDefaultSort(['name' => 'asc']);
        }
        else if(isset($this->data[$this->gridName]) && isset($this->data[$this->gridName]['sort']))
        {
            $sort = $this->data[$this->gridName]['sort'];
            $grid->setDefaultSort($sort);
        }
        else
        {
            $grid->setDefaultSort(['date' => 'desc']);
        }

        switch ($type)
        {
            case "order_counts":
            case "order_totals":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('count', 'Total Orders')->setSortable();
                $grid->addColumn('total', 'Total Amount')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
            case "brand_counts":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('order_count', 'Total Orders')->setSortable();
                $grid->addColumn('brands_count', 'Total Brands')->setSortable();
                break;
            case "gross_revenues":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('order_count', 'Total Orders')->setSortable();
                $grid->addColumn('revenue', 'Gross Revenue')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
            case "order_item_units":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('order_count', 'Total Orders')->setSortable();
                $grid->addColumn('units_count', 'Total Units')->setSortable();
                $grid->addColumn('unique_items', 'Unique Items')->setSortable();
                break;
            case "rto_totals":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('order_count', 'Order Count')->setSortable();
                $grid->addColumn('rto_total', 'RTO Total')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
            case "return_totals":
                $grid->addColumn('date', 'Date')->setSortable();
                $grid->addColumn('order_count', 'Order Count')->setSortable();
                $grid->addColumn('return_total', 'Return Total')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
            case "top_marketers":
            case "top_stores":
            case "top_brands":
                if($type == "top_marketers") $name = "Marketers";
                else if ($type == "top_stores") $name = "Stores";
                else if ($type == "top_brands") $name = "Brands";
                $grid->addColumn('name', $name)->setSortable();
                $grid->addColumn('order_count', 'Order Count')->setSortable();
                $grid->addColumn('unique_items', 'Unique Items')->setSortable();
                $grid->addColumn('total_items', 'Total Items')->setSortable();
                $grid->addColumn('order_total', 'Total Amount')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
            case "net_revenues":
                $grid->addColumn('shipment_date', "Date")->setSortable();
                $grid->addColumn('unique_stores', "Unique Stores")->setSortable();
                $grid->addColumn('unique_beats', "Unique Beats")->setSortable();
                $grid->addColumn('total_orders', "Total Orders")->setSortable();
                $grid->addColumn('unique_products', "Unique Products")->setSortable();
                $grid->addColumn('total_item_units', "Total Units")->setSortable();
                $grid->addColumn('item_total', 'Total Amount')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                $grid->addColumn('total_credit_notes', 'Total Credit Notes')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                $grid->addColumn('net_revenue', 'Net Revenue')->setCallback(function($val, $dp){
                    return "&#8377; ".number_format($val);
                })->setSortable();
                break;
        }

        return $grid;
    }

    protected function getQuery()
    {
        $type = !empty($this->data['types']) ? $this->data['types'][0] : unserialize(Cookie::get("table-types"))[0] ;

//        dd($this->data, unserialize(Cookie::get("table-types")));

        switch ($type)
        {
            case "order_counts":
            case "order_totals":
                return StatsHelper::getOrderQuery($this->data);
                break;
            case "brand_counts":
                return StatsHelper::getBrandsQuery($this->data);
                break;
            case "gross_revenues":
                return StatsHelper::getGrossRevenueQuery($this->data);
                break;
            case "order_item_units":
                return StatsHelper::getOrderItemUnitsQuery($this->data);
                break;
            case "rto_totals":
                return StatsHelper::getRtoTotalsQuery($this->data);
                break;
            case "return_totals":
                return StatsHelper::getReturnTotalQuery($this->data);
                break;
            case "top_brands":
                return StatsHelper::getTopBrandsQuery($this->data);
                break;
            case "top_marketers":
                return StatsHelper::getTopMarketersQuery($this->data);
                break;
            case "top_stores":
                return StatsHelper::getTopStoresQuery($this->data);
                break;
            case "net_revenues":
                return StatsHelper::getNetRevenueQuery($this->data);
                break;
        }
    }
}
