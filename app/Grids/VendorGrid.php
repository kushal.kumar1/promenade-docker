<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Vendor;

class VendorGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-vendors");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("trade_name")->setSearchFilter();
        $grid->addColumn("legal_name")->setSearchFilter();
        $grid->addColumn("status")->setBooleanFilter('Active', 'Disabled');
        $grid->addColumn("incorporation_type");
        $grid->addColumn("type");
        $grid->addColumn("PAN")->setSearchFilter();
        $grid->addColumn("GSTIN")->setSearchFilter();
        // $grid->addColumn("stores_count", "Stores")->setSortable();
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("profile", "Bank Account")->setLink(route('admin::vendors.bank_account.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Manage');
        $grid->addColumn("profile", "Address")->setLink(route('admin::vendors.address.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Manage');
        $grid->addColumn("mMarketers", "Marketers")->setLink(route('admin::vendors.marketers', ['id' => '%s']), ['id'], 'btn btn-sm', 'Marketers');
        $grid->addColumn("profile", "Action")->setLink(route('admin::vendors.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return Vendor::with('addresses');
    }
}
