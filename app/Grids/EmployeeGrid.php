<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Employee;

class EmployeeGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("emp-grid");
        $grid->addColumn("id");
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("email")->setSearchFilter();
        $grid->addColumn('mobile')->setSearchFilter();
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::employees.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return (new Employee())->newQuery();
    }
}
