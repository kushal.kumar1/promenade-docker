<?php

namespace Niyotail\Grids\StockTransfer;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\StockTransfer;

class StockTransferInboundGrid extends \Niyotail\Grids\BaseGrid
{

    protected function setGrid(Grid $grid): Grid
    {
        $grid->setName('stock-transfer-inbound-grid');
        $grid->addColumn('id');
        $grid->addColumn('fromWarehouse.name', 'From Warehouse');
        $grid->addColumn('toWarehouse.name', 'To Warehouse');
        $grid->addColumn('items_count');
        $grid->addColumn('status')->setSelectFilter(StockTransfer::getConstants('STATUS'));
        $grid->addColumn('created_at')->isDate()->setDateTimeRangeFilter();
        $grid->addColumn('View Items')
            ->setLink(route('admin::stock-transfers.items', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        $selectedWarehouse =  app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return StockTransfer::where('to_warehouse_id', $selectedWarehouse)
            ->withCount(['items'])
            ->with('fromWarehouse', 'toWarehouse');
    }
}