<?php

namespace Niyotail\Grids\StockTransfer;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\StockTransferItem;

class StockTransferItemGrid extends \Niyotail\Grids\BaseGrid
{

    protected function setGrid(Grid $grid): Grid
    {
        $grid->setName('stock-transfer-items-grid');
        $grid->addColumn('id', 'item_id');
        $grid->addColumn('product_id')->setSearchFilter();
        $grid->addColumn('product.name', 'Product');
        $grid->addColumn('requested_quantity');
        $grid->addColumn('confirmed_quantity');
        $grid->addColumn('sent_quantity', 'sent_quantity');
        $grid->addColumn('received_quantity', 'Received Quantity');
        $grid->addColumn('deficiency_reason', 'deficiency_reason');
        return $grid;
    }

    protected function getQuery()
    {
        return StockTransferItem::where('stock_transfer_id', $this->data['id'])
            ->with('product');
    }
}