<?php

namespace Niyotail\Grids\StockTransfer;

use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\StockTransfer;

class StockTransferOutboundGrid extends \Niyotail\Grids\BaseGrid
{

    protected function setGrid(Grid $grid): Grid
    {
        $grid->setName('stock-transfer-outbound-grid');
        $grid->addColumn('id');
        $grid->addColumn('fromWarehouse.name', 'From Warehouse');
        $grid->addColumn('toWarehouse.name', 'To Warehouse');
        $grid->addColumn('vehicle_number')->setSearchFilter();
        $grid->addColumn('eway_bill_no')->setSearchFilter();
        $grid->addColumn('items_count');
        $grid->addColumn('status')->setSelectFilter(StockTransfer::getConstants('STATUS'));
        $grid->addColumn('created_at')->isDate()->setDateTimeRangeFilter();
        $grid->addColumn('Update')->setCallback(function ($val,$dp){
            $row = $dp->getSrc();
            if($row->status == StockTransfer::STATUS_INITIATED){
                return "<button type='button' class='btn btn-primary' data-modal='#modal-stock-transfer-update' data-id='{$row->id}' data-destination='{$row->toWarehouse->name}' data-vehicle_number='{$row->vehicle_number}' data-eway_bill_no='{$row->eway_bill_no}'>Update</button>";
            }
            return "<button type='button' class='btn btn-primary' disabled='disabled'>Update</button>";
        });
        $grid->addColumn('View Items')
            ->setLink(route('admin::stock-transfers.items', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        $selectedWarehouse =  app(MultiWarehouseHelper::class)->getSelectedWarehouse();
        return StockTransfer::where('from_warehouse_id', $selectedWarehouse)
            ->withCount(['items'])
            ->with('fromWarehouse', 'toWarehouse');
    }
}