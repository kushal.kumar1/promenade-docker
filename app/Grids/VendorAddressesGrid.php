<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\VendorsAddresses;

class VendorAddressesGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-vendors-addresses");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("vendor.name")->setSearchFilter();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("status")->setBooleanFilter("Active", "Inactive");;
        $grid->addColumn("gstin")->setSearchFilter();
       
        $grid->addColumn("address");
        $grid->addColumn("pincode")->setSearchFilter();
        $grid->addColumn("city")->setSearchFilter();
        $grid->addColumn("state")->setSearchFilter();
        $grid->addColumn("state_code")->setSearchFilter();
        $grid->addColumn("country")->setSearchFilter();

        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("profile", "Action")->setLink(route('admin::vendors.address.edit', ['id' => '%s']), ['vendor.id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return VendorsAddresses::with('vendor');
    }
}
