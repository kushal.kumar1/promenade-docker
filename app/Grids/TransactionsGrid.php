<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Tag;
use Niyotail\Models\Transaction;
use Niyotail\Models\WarehouseScope;

class TransactionsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("transaction-grid");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id")->setSortable()->setSearchFilter();
        $grid->addColumn("transaction_id")->setSearchFilter();
        $grid->addColumn("store.id", 'store id')->setSearchFilter();
        $grid->addColumn("store.name", 'store name')->setSearchFilter()->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            $link="#";
            $linkClass = !empty($transaction->source_type) ? "class='link'" :"";
            $link = route("admin::stores.profile", $transaction->store->id);
            return "<a href='$link' target='_blank' $linkClass>".$transaction->store->name."</a>";
        });
        $grid->addColumn("description");
        $grid->addColumn("payment_method")->setSelectFilter(Transaction::getConstants('payment_method'));
        $grid->addColumn("paymentTag.name", "Payment Tag")->setSelectFilter(self::getPaymentTags());
        $grid->addColumn('amount', 'credit')->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            if ($transaction->type == Transaction::TYPE_CREDIT) {
                return $transaction->amount;
            }
        })->setSearchFilter();
        $type = $this->data['type'];
        if (!empty($type) && $type == 'cheque_approval') {
            $grid->addColumn('Cheque Date')->setCallback(function ($val, $row) {
                $transaction = $row->getSrc();
                if (!empty($transaction->payment_details)) {
                    $chequeDetails = json_decode($transaction->payment_details);
                    return $chequeDetails->cheque_date;
                }
            });
            $grid->addColumn('Cheque Number')->setCallback(function ($val, $row) {
                $transaction = $row->getSrc();
                if (!empty($transaction->payment_details)) {
                    $chequeDetails = json_decode($transaction->payment_details);
                    return $chequeDetails->cheque_number;
                }
            });
        }
        $grid->addColumn("settled invoices")->setCallback(function ($val, $row) {
            $transaction = $row->getSrc();
            $invoiceText = '';
            if ($transaction->settledDebitTransactions->isNotEmpty()) {
                foreach ($transaction->settledDebitTransactions as $debitTransaction) {
                    if (!empty($debitTransaction->source_type) && $debitTransaction->source_type == 'invoice') {
                      $invoice = $debitTransaction->source;
                      if($invoice->amount == $debitTransaction->pivot->amount) {
                        $invoiceText .= "[$invoice->reference_id - Full]";
                      } else {
                        $invoiceText .= "[$invoice->reference_id - Partial]";
                      }
                    }
                }
            }
            return "<span>$invoiceText</span>";
        });
        $grid->addColumn('createdBy.name', 'created By');
        // ->setSearchFilter()->setFilteringFunc(function ($val, $row) {
        //     $query = $row->getBuilder();
        //     $query->whereHasMorph('createdBy', '*', function ($query) use ($val) {
        //       $query->where('name', 'like', "%$val%");
        //     });
        // });
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("status");
        $grid->addColumn("edit", "Edit")->setLink(route('admin::transactions.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        //$grid->addColumn("approve", "Approve")->setLink(route('admin::transactions.update', ['id' => '%s']), ['id'], 'btn btn-sm', 'Approve');
        return $grid;
    }

    private static function getPaymentTags(){
        $options = [];
        foreach(Tag::paymentTags() as $paymentTag){
            $options[$paymentTag] = $paymentTag;
        }

        return $options;
    }

    protected function getQuery()
    {
        $query = Transaction::with('source', 'store', 'paymentTag');
        $type = $this->data['type'];
        switch ($type) {
          case 'cheque_approval':
            $query->where('status', Transaction::STATUS_PENDING)->where('payment_method', 'cheque')
                  ->where(function($subquery) {
                      $period = new \DatePeriod(
                        new \DateTime(date('Y-m-d', strtotime('-15 days'))),
                        new \DateInterval('P1D'),
                        new \DateTime('tomorrow')
                      );
                      foreach($period as $key=>$value) {
                        $date = $value->format('d M, Y');
                        $subquery->orWhere('payment_details', 'like', "%cheque_date%$date%");
                      }
                  });
            break;
          case 'pending':
            $query->where('status', Transaction::STATUS_PENDING);
            break;
          default:
            $query->where('status', Transaction::STATUS_PENDING);
            break;
        }

        if (!empty($this->data['store_id'])) {
            $query->where('store_id', $this->data['store_id']);
        }
        return $query;
    }
}
