<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Order;
use Niyotail\Models\PurchaseInvoice;
use Niyotail\Models\StoreOrder;
use Niyotail\Models\Warehouse;

class PurchaseInvoiceGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-purchase-invoice");
        $grid->addColumn("id")->setSortable()->setSearchFilter(Grid::OPERATOR_EQ)->setWidth('20px');
        $grid->addColumn("warehouse.name", "Warehouse")->setSelectFilter(Warehouse::all()->pluck('name', 'id')->toArray(), 'id');
        $grid->addColumn("vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("reference_id", 'invoice id')->setSearchFilter();
        $grid->addColumn("vendor_ref_id")->setSearchFilter();
        $grid->addColumn("invoice_date")->setDateTimeRangeFilter();
        $grid->addColumn("total")->setCallback(function ($var, $row) {
            $total = $row->getSrc()->total;
            return number_format($total, 2, '.', '');
        })->setSortable();
        $grid->addColumn("status")->setSelectFilter($this->getPurchaseInvoiceStatuses());
        $grid->addColumn('type')->setSelectFilter(PurchaseInvoice::getConstants('TYPE'));
        $grid->addColumn("is_direct_store_purchase", 'Store')->setCallback(function ($var, $row) {
            $data = $row->getSrc();
            if ($data->is_direct_store_purchase) {
                if ($data->storePurchaseInvoice && $data->storePurchaseInvoice->store) {
                    return '#' . $data->storePurchaseInvoice->store->id . ' - ' . $data->storePurchaseInvoice->store->name;
                }
            }
        })->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHas('storePurchaseInvoice.store', function ($query) use ($val) {
                $query->where('name', 'like', "$val%")->orWhere('id', 'like', "$val%");
            });
        });
        $grid->addColumn('all_grn_count', 'Imported GRN Count')->setSortable();
        $grid->addColumn("view", "Action")->setCallback(function ($val, $dp) {
            return '<a href="' . route("admin::purchase-invoices.editInvoice", [$dp->getSrc()->id]) . '" class="btn btn-primary">View</a>';
        });
        $grid->addColumn('Generate Sale Order')->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            $genUrl = route('admin::purchase-invoices.generate-sale-order');
            if ($row->canGenerateSaleOrder()) {
                return "<button class='btn btn-secondary gen-sale-order' data-url='$genUrl' data-id='$row->id'>Generate</button>";
            } elseif (!empty($row->storePurchaseInvoice) && !empty($row->storePurchaseInvoice->assigned_id)) {
                if ($row->storePurchaseInvoice->assignedOrder instanceof Order) {
                    return " ";
//                    $orderUrl = route('admin::orders.edit', ['id' => $row->storePurchaseInvoice->assignedOrder->id]);
//                    return "<a class='link' href='$orderUrl'>{$row->storePurchaseInvoice->assignedOrder->reference_id}</a>";
                }
                $storeOrder = StoreOrder::with('order')->find($row->storePurchaseInvoice->assigned_id);
                if (!empty($storeOrder)) {
                    if (empty($storeOrder->order)) {
                        return "generating order (Store Order Id : {$storeOrder->id})";
                    } else {
                        $orderUrl = route('admin::orders.edit', ['id' => $storeOrder->order->id]);
                        return "<a class='link' href='$orderUrl'>{$storeOrder->order->reference_id}</a>";
                    }
                } else {
                    return "{$row->storePurchaseInvoice->assigned_id}";
                }
            }
            return "";
        });
        return $grid;
    }

    private function getPurchaseInvoiceStatuses()
    {
        return PurchaseInvoice::getConstants('STATUS');
    }

    protected function getQuery()
    {
        $baseQuery = PurchaseInvoice::with('warehouse', 'purchaseOrder.vendor', 'storePurchaseInvoice.assignedOrder')
            ->withCount(['all_grn' => function ($query) {
                $query->where('inventory_imported', 1);
            }]);
        if (!empty($this->data['po_id'])) {
            $baseQuery->WherePurchaseOrder($this->data['po_id']);
        }
        return $baseQuery->orderBy('status', 'DESC')
            ->orderBy('created_at', 'DESC');
    }
}
