<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Beat;

class BeatGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("beat-grid");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("name")->setSearchFilter();
        $grid->addColumn("long_name");
        $grid->addColumn('live_stores_count', 'Stores No.');
        $grid->addColumn('exceptions_count', 'No. of Brands Restricted');
        $grid->addColumn('outstanding')->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            return number_format($row->liveStores->sum('totalBalance.balance_amount'));
        });
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn("edit", "Action")->setLink(route('admin::beats.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return Beat::withCount('liveStores', 'exceptions')->with('liveStores.totalBalance');
    }
}
