<?php


namespace Niyotail\Grids;


use Niyotail\Grids\Core\Grid;
use Niyotail\Models\InventoryTransaction;
use Niyotail\Models\OrderItem;

class InventoryTransactionGrid extends BaseGrid
{

    protected function setGrid(Grid $grid)
    {
        $grid->addColumn('date');
        $grid->addColumn('type')->setSelectFilter(InventoryTransaction::getConstants('TYPE'))
            ->setFilteringFunc(function ($val,$dp){
                $query = $dp->getBuilder();
                $query->where('inventory_transactions.type', $val);
            });
        $grid->addColumn('quantity', 'Transacted Qty');
        $grid->addColumn('prev_quantity', 'Previous Qty');
        $grid->addColumn('reference_id')->setCallback(function ($val,$dp){
            $row = $dp->getSrc();
            if(!empty($val)){
                $href = route('admin::orders.edit', ['id'=>$row->order_id]);
                return "<a class='link' href='$href' target='_blank'>$val</a>";
            }elseif (!empty($row->audit_id)){
                $href = route('admin::audits.edit', ['id'=>$row->audit_id]);
                return "<a class='link' href='$href' target='_blank'>Audit-$row->audit_id</a>";
            }
        });
        return $grid;
    }

    protected function getQuery()
    {
        return InventoryTransaction::selectRaw('sum(inventory_transactions.quantity) as quantity, sum(inventory_transactions.previous_quantity) as prev_quantity, date(convert_tz(inventory_transactions.created_at, "+00:00", "+05:30")) as date, inventory_transactions.type as type, orders.reference_id, orders.id as order_id, audits.id as audit_id')
            ->leftjoin('order_items', function ($join) {
                $join->on('inventory_transactions.source_id', '=', 'order_items.id')
                ->where('inventory_transactions.source_type', OrderItem::class);
            })->leftjoin('orders', 'orders.id', '=', 'order_items.order_id')
            ->leftJoin('audits', function ($join){
                $join->on('inventory_transactions.source_id', '=', 'audits.id')
                    ->where('inventory_transactions.source_type', 'audit');
            })
            ->where('inventory_transactions.inventory_id', $this->data['id'])
            ->groupBy('inventory_transactions.type', 'orders.id', 'audits.id')
            ->orderBy('date', 'desc');
    }
}