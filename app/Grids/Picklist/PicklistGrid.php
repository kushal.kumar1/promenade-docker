<?php

namespace Niyotail\Grids\Picklist;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Picklist;


class PicklistGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("picklist-grid");
        $grid->setDefaultSort(['created_at' => 'desc']);
        $grid->addColumn("id")->setSortable()->setSearchFilter();
        $grid->addColumn("picker.name", "Picker")->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            if (!empty($row->picker)) {
                return $val;
            } elseif (!empty($row->agent)) {
                return $row->agent->name;
            }
            return "";
        })->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->whereHas('picker', function ($pickerQuery) use ($val) {
                $pickerQuery->where('name', 'like', "%$val");
            })->orWhereHas('agent', function ($agentQuery) use ($val) {
                $agentQuery->where('name', 'like', "%$val");
            });
        });
        $grid->addColumn("status")->setSelectFilter(Picklist::getConstants('status'))->setCallback(function ($var, $row) {
            $status = $row->getSrc()->status;
            return '<span class="tag tag-' . $status . '" >' . $status . '</div>';
        });
        $grid->addColumn("createdBy.name", "created By");
        $grid->addColumn('created_at')->setMinWidth('120px')->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("edit", "Action")->setLink(route('admin::picklists.items', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return Picklist::with('createdBy', 'picker', 'agent');
    }
}
