<?php

namespace Niyotail\Grids\Picklist;

use Illuminate\Support\Facades\DB;
use Niyotail\Grids\Core\Grid;
use Niyotail\Grids\BaseGrid;
use Niyotail\Models\OrderItem;
use Niyotail\Models\PicklistItem;

class PicklistItemsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("picklist-items-grid");
        $grid->setDefaultSort(['picklist_items.created_at' => 'desc']);
        $grid->addColumn("reference_id", 'order_id')->setSearchFilter()->setLink(route('admin::orders.edit', ['id' => '%s']), ['order_id'], 'link');
        $grid->addColumn("store","store")->setSearchFilter()->setFilteringFunc(function($val, $dp) {
            $builder = $dp->getBuilder();
            return $builder->where('s.name','like', "%$val%");
        });
        $grid->addColumn("id","pid")->setLink(route('admin::products.edit', ['id' => '%s']), ['id'], 'link');
        $grid->addColumn("name","product")->setSearchFilter()->setFilteringFunc(function($val, $dp) {
            $builder = $dp->getBuilder();
            return $builder->where('p.name','like', "%$val%");
        });
        $grid->addColumn("sku")->setSearchFilter();
        $grid->addColumn("barcode")->setSearchFilter();
        $grid->addColumn("qty");
        $grid->addColumn('status')->setSelectFilter(PicklistItem::getConstants('STATUS'))->setFilteringFunc(function($val, $dp) {
            $builder = $dp->getBuilder();
            return $builder->where('picklist_items.status','=', $val);
        });
        $grid->addColumn('skip_reason');

        return $grid;
    }

    protected function getQuery()
    {
        $id = $this->data['id'];

        return PicklistItem::join('order_items as oi','oi.id','=','picklist_items.order_item_id')
            ->join('orders as o','o.id','=','oi.order_id')
            ->join('stores as s','o.store_id','=','s.id')
            ->join('products as p','p.id','=','oi.product_id')
            ->with('orderItem.product')
            ->select(DB::raw('count(*) as qty, oi.order_id, oi.product_id, oi.sku, o.reference_id, s.name as store, p.name,p.id, picklist_items.status, p.barcode, skip_reason'))
            ->where('picklist_id', $id)
            ->groupBy('product_id','order_id','picklist_items.status','picklist_items.skip_reason')
            ->orderBy('oi.id', 'desc');
    }
}
