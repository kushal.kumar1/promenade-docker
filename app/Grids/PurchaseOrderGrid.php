<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\PurchaseOrder;

class PurchaseOrderGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('purchase-order-grid');
        $grid->addColumn("id", "Id");
        $grid->addColumn("status", "Status");
        $grid->addColumn("vendor.name", "Vendor")->setSearchFilter();
        $grid->addColumn("createdBy.name", "Created By")->setSearchFilter();
        $grid->addColumn("created_at", "Created At")->setSortable();
        $grid->addColumn("view", "View")->setLink(route('admin::purchase-order.single', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return PurchaseOrder::with('createdBy','vendor')->withCount('items');
    }
}
