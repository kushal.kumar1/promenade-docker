<?php

namespace Niyotail\Grids\FlatInventory;

use Illuminate\Support\Facades\DB;
use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\FlatInventory;

class ProductLocationsGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('location-grid');
        $grid->addColumn('id','Storage Id')->setSearchFilter()->setSortable();
        $grid->addColumn('label')->setSearchFilter()->setSortable();
        $grid->addColumn('status')->setSortable();
        $grid->addColumn('qty')->setSortable();
        $grid->addColumn('expiry_date')->isDate('d-m-Y')->setSortable();
        return $grid;
    }

    protected function getQuery()
    {
        $productId = $this->data['product_id'];
        return FlatInventory::join('storages','storages.id','=','flat_inventories.storage_id')
            ->select(DB::raw('flat_inventories.storage_id as id, count(*) as qty, storages.label as label, flat_inventories.status as status, flat_inventories.expiry_date as expiry_date'))
            ->where('product_id', $productId)
            ->where('flat_inventories.warehouse_id', app(MultiWarehouseHelper::class)->getSelectedWarehouse())
            ->groupBy('flat_inventories.storage_id', 'flat_inventories.status', 'flat_inventories.expiry_date')
            ->orderBy('flat_inventories.storage_id', 'desc');
    }
}