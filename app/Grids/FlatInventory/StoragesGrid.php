<?php

namespace Niyotail\Grids\FlatInventory;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\Storage;

class StoragesGrid extends BaseGrid
{

    protected function setGrid(Grid $grid)
    {
        $grid->setName('storages-grid');
        $grid->setDefaultSort(['id' => 'desc']);
        $grid->addColumn('id')->setSearchFilter()->setSortable();
        $grid->addColumn('label')->setSearchFilter();
        $grid->addColumn('inventories_count', 'Total Items');
        $grid->addColumn('View Products')->setCallback(function ($val, $dp) {
            $id = $dp->getSrc()->id;
            $url = route('admin::flat-inventory.storages.details',['id'=>$id]);
            return "<a class='btn btn-raw' href='$url'>View</a>";
        });
        return $grid;
    }

    protected function getQuery()
    {
        return Storage::where('id', '!=', 6497)->where(['warehouse_id' => app(MultiWarehouseHelper::class)->getSelectedWarehouse()])->withCount('inventories');
    }
}