<?php

namespace Niyotail\Grids\FlatInventory;

use Niyotail\Grids\BaseGrid;
use Niyotail\Grids\Core\Grid;
use Niyotail\Helpers\MultiWarehouseHelper;
use Niyotail\Models\FlatInventory;

class ProductWiseInventoryGrid extends BaseGrid
{

    protected function setGrid(Grid $grid)
    {
        $grid->setName('product-wise-inventory-count-grid');
        $grid->setDefaultSort(['product_id' => 'asc']);
        $grid->addColumn('product_id')->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->where('products.id', 'like', "%$val%");
        })->setSortable();
        $grid->addColumn('group_id')->setSearchFilter()->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->where('products.group_id', $val);
        });
        $grid->addColumn('barcode')->setSearchFilter()
            ->setFilteringFunc(function ($val, $dp) {
                $query = $dp->getBuilder();
                $query->where('products.barcode', 'like', "%$val%");
            });
        $grid->addColumn('product')->setSearchFilter()
            ->setFilteringFunc(function ($val, $dp) {
            $query = $dp->getBuilder();
            $query->where('products.name', 'like', "%$val%");
        });
        $grid->addColumn('mrp');
        foreach (FlatInventory::getConstants('STATUS') as $status) {
            $grid->addColumn("$status");
        }
        $grid->addColumn("details", "Action")->setCallback(function ($val, $dp) {
            $data = $dp->getSrc();
            $link = route('admin::flat-inventory.product-locations', $data->product_id);
            return '<a href="'.$link.'" class="btn btn-sm">View</a>';
        });
        return $grid;
    }

    protected function getQuery()
    {
        $selectString = ['products.name as product, products.id as product_id, products.barcode as barcode, products.group_id as group_id, product_variants.mrp as mrp'];
        foreach (FlatInventory::getConstants('STATUS') as $status) {
            $selectString [] = "COUNT(case when flat_inventories.status='{$status}' then flat_inventories.id end) as {$status}";
        }
        return FlatInventory::join('products', 'flat_inventories.product_id', '=', 'products.id')
            ->join('product_variants', 'products.id', '=', 'product_variants.product_id')
            ->selectRaw(implode(', ', $selectString))
            ->where('flat_inventories.warehouse_id', app(MultiWarehouseHelper::class)->getSelectedWarehouse())
            ->where('product_variants.value', 'unit')
            ->groupBy('products.id');
    }
}