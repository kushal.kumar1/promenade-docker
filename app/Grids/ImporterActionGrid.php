<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\Cart;
use Niyotail\Models\ImporterAction;

class ImporterActionGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("importer_actions");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("type")->setSelectFilter(ImporterAction::getConstants('TYPE'));
        $grid->addColumn("input")->setLink(asset("importer/%s"), ["input"]);
        $grid->addColumn("output")->setLink(asset("importer-output/%s"), ["output"]);
        $grid->addColumn("employee.name", "Employee")->setSearchFilter();
        $grid->addColumn("status")->setSelectFilter(ImporterAction::getConstants('STATUS'));
        $grid->addColumn("created_at", "Date")->isDate()->setDateTimeRangeFilter();
        $grid->addColumn('resource_type', 'Resource')->setCallback(function ($val, $dp) {
            $row = $dp->getSrc();
            if($row->resource instanceof Cart){
                return '<a class="btn btn-sm btn-primary" href="'.route("admin::stores.cart",$row->resource->store_id).'">view</a>';
            }
            return $row->resource_type;
        });
        return $grid;
    }

    protected function getQuery()
    {
        return ImporterAction::with('employee','resource');
    }
}
