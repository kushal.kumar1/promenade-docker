<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\DashboardImage;
use Image;

class DashboardImageGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName('dashboard-banner-grid');
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("IMG")->setCallback(function ($val, $row) {
            $banner = $row->getSrc();
            $image = Image::getSrc($banner, 200);
            return '<img class="bd" src="'.$image.'">';
        });
        $grid->addColumn("status")->setBooleanFilter('Active', 'Inactive');
        $grid->addColumn('valid_from')->setSortable();
        $grid->addColumn("edit", "Action")->setLink(route('admin::dashboard-image.edit', ['id' => '%s']), ['id'], 'btn btn-sm', 'View');
        return $grid;
    }

    protected function getQuery()
    {
        return (new DashboardImage())->newQuery();
    }
}
