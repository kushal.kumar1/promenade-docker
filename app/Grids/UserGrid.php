<?php


namespace Niyotail\Grids;

use Niyotail\Grids\Core\Grid;
use Niyotail\Models\User;
use Niyotail\Models\Order;

class UserGrid extends BaseGrid
{
    protected function setGrid(Grid $grid)
    {
        $grid->setName("grid-users");
        $grid->addColumn("id")->setSortable();
        $grid->addColumn("first_name")->setSearchFilter();
        $grid->addColumn("last_name")->setSearchFilter();
        // $grid->addColumn("email")->setSearchFilter();
        $grid->addColumn("mobile")->setSearchFilter();
        // $grid->addColumn("stores_count", "Stores")->setSortable();
        $grid->addColumn("verified")->setBooleanFilter('Yes', 'No');
        $grid->addColumn("status")->setBooleanFilter('Active', 'Disabled');
        $grid->addColumn("created_at")->setSortable()->isDate()->setDateTimeRangeFilter();
        $grid->addColumn("profile", "Action")->setLink(route('admin::users.profile', ['id' => '%s']), ['id'], 'btn btn-sm', 'Edit');
        return $grid;
    }

    protected function getQuery()
    {
        return User::with('stores');
    }
}
