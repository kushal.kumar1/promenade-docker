<?php

namespace Niyotail\Queries;

trait TaxQueries
{
    public static function getTaxes($taxClassId, $productMrp, $dateTime, $sourceStateId, $supplyCountryId, $supplyStateId=null)
    {
        $query=static::where('tax_class_id',$taxClassId)->where('source_state_id',$sourceStateId)
            ->where(function ($query) use ($productMrp) {
                $query->where('max_price', '>=', $productMrp)->orWhereNull('max_price');
            })->where('supply_country_id',$supplyCountryId)
            ->where('start_date','<=',$dateTime)->where(function ($query) use ($dateTime) {
                $query->where('end_date', '>=', $dateTime)->orWhereNull('end_date');
            });
        if(!empty($supplyStateId))
            $query->where('supply_state_id',$supplyStateId);
        else
            $query->whereNull('supply_state_id');

        return $query->get();
    }


}
