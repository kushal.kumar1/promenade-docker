<?php

namespace Niyotail\Queries;


use Illuminate\Pagination\LengthAwarePaginator;
use Niyotail\Models\Shipment;

trait ShipmentQueries
{
    public static function fetchShipmentsByStore(int $storeId): LengthAwarePaginator
    {
        return static::with('orderItems.product', 'invoice')
            ->whereHas('order', function ($query) use ($storeId) {
                $query->where('store_id', $storeId);
            })
            ->whereRaw("date(convert_tz(created_at, '+00:00','+05:30')) >= '2021-04-01'")
            ->orderBy('created_at', 'desc')
            ->paginate();
    }


    public function scopeShipmentByStore($query, $storeId)
    {
        return $query->with('orderItems.product', 'invoice')
        ->whereHas('order', function ($query) use ($storeId) {
            $query->where('store_id', $storeId);
        });
       
    }

}