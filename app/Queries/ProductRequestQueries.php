<?php


namespace Niyotail\Queries;


use Niyotail\Models\ProductRequest;

trait ProductRequestQueries
{
    public static function create(
        $id = null, $name, $desc, $barcode, $brandId, $collectionId, $mrp, $uom, $taxClassId,
        $groupId = null, $source = null, $sourceId = null, $outerUnits, $caseUnits,
        $hsnCode = null, $imagePATH = null, $autoAssignBarcode, $cl4Id
    )
    {
        $isRequestedGroup = false;

        if(!$groupId) {
            // create a group request
            $isRequestedGroup = true;
        }

        $product = new ProductRequest();

        if ($id) {
            $product = ProductRequest::find($id);
        }
        
        $product->setGroupId($groupId);
        $product->setIsRequestedGroup($isRequestedGroup);
        $product->setName($name);
        $product->setBarcode($barcode);
        $product->setDescription($desc);
        $product->setBrandId($brandId);
        $product->setCollectionId($collectionId);
        $product->setMrp($mrp);
        $product->setUom($uom);
        $product->setOuterUnits($outerUnits);
        $product->setCaseUnits($caseUnits);
        $product->setTaxClassId($taxClassId);
        $product->source = $source;
        $product->source_id = $sourceId;
        $product->setHsnCode($hsnCode);
        $product->setFile($imagePATH);
        $product->setAutoAssignBarcode($autoAssignBarcode);
        $product->setCl4Id($cl4Id);

        if ($id) {
            $product->update();
        } else {
            $product->save();
        }

        return $product;
    }

    public static function searchByBarcode($barcode)
    {
        return static::where('barcode', $barcode)->get();
    }

    public static function searchByKeywordAndBarcode($term)
    {
        return static::where(function($query) use($term) {
            $query->where("name", "like", "%$term%");
            $query->orWhere("barcode", "like", "%$term%");
        })
            ->where('status', 'product_created')
            ->get();
    }
}
