<?php

namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait PurchaseInvoiceQueries
{
    public static function searchByPurchaseInvoiceId($term, $limit = 20)
    {
        return static::where("id", "like", "%$term%")->limit($limit)->get();
    }
}
