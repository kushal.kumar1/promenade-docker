<?php

namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait VendorQueries
{
    public static function searchByVendorId($term, $limit = 20)
    {
        return static::where("id", "like", "%$term%")->limit($limit)->get();
    }
}
