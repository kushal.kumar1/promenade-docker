<?php


namespace Niyotail\Queries;


trait BeatQueries
{
    public static function searchWithKeyword($term, $limit = 10)
    {
        return static::active()->where("name", "like", "%$term%")->limit($limit)->get();
    }

    public static function getByIds($ids)
    {
        return static::active()->whereIn('id', $ids)->get();
    }
}