<?php

namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait TransactionQueries
{
    public static function searchByTransactionId($term, $limit = 20)
    {
        return static::where("transaction_id", "like", "%$term%")->limit($limit)->get();
    }
}
