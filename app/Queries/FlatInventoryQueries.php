<?php

namespace Niyotail\Queries;

use Niyotail\Models\FlatInventory;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Storage;

trait FlatInventoryQueries
{
    public static function getSaleableProductInventory($product, $warehouseId, $quantity)
    {
        $status = FlatInventory::STATUS_READY_FOR_SALE;
        $skipStorages = Storage::where('warehouse_id', $warehouseId)
            ->whereIn('zone', [Storage::ZONE_LIQUIDATION, Storage::ZONE_NEAR_EXPIRY])
            ->get()
            ->pluck('id')
            ->toArray();

        $skipStorageIds = implode(",", $skipStorages);
        $query = "select * from flat_inventories where warehouse_id = $warehouseId and product_id = $product->id and status = '$status'";
        if (!empty($skipStorages)) {
            $query .= " and storage_id NOT IN ($skipStorageIds)";
        }
        $query .= " limit $quantity for update skip locked";
        $inventories = DB::select($query);
        return self::hydrate($inventories);
    }

    public static function getInventoriesForUpdate($warehouseId, $productId, $status, $quantity)
    {
        $inventories = DB::select("select * from flat_inventories where warehouse_id = $warehouseId and product_id = $productId and status = '$status' limit $quantity for update skip locked");
        return self::hydrate($inventories);
    }

    public static function lockInventoryIdsForUpdate(array $inventoriesId): array
    {
        // $ids = implode(',', $inventoriesId);
        // DB::select("select id from flat_inventories where id in ($ids) for update skip locked");
        return $inventoriesId;
    }

    public static function getStorageInventoriesForUpdate($warehouseId, $productId, $status, $storageId, $quantity)
    {
        $inventories = DB::select("select * from flat_inventories where warehouse_id = $warehouseId and product_id = $productId and status = '$status' and storage_id = $storageId limit $quantity for update skip locked");
        return self::hydrate($inventories);
    }

    public static function getInventoriesForUpdateForDirectPutaway($warehouseId, $productId, $quantity)
    {
        $query = "select * from flat_inventories  where id not in
                  (select flat_inventory_id from putaway_item_inventories pii, putaway_items pi, putaways p
                  where pii.putaway_item_id=pi.id and pi.putaway_id=p.id and (pi.placed_quantity is NULL and p.status != 'closed')
                  and pi.product_id = $productId and p.warehouse_id = $warehouseId)
                  and warehouse_id = $warehouseId and product_id = $productId and status = 'putaway' limit $quantity";
        $inventories = DB::select($query);
        return self::hydrate($inventories);
    }

    public static function getStatusWiseCount($productId, $warehouseId)
    {
        $statusWiseSelectString = [];
        foreach (self::getConstants('STATUS') as $status) {
            $statusWiseSelectString [] = "COUNT(case when status='{$status}' then flat_inventories.id end) as {$status}";
        }

        return FlatInventory::selectRaw(implode(', ', $statusWiseSelectString))
            ->where(['product_id' => $productId, 'warehouse_id' => $warehouseId])
            ->first();
    }
}
