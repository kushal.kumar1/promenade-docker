<?php
namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait OrderItemQueries
{
    public static function getStorePendingReturnVal($storeId)
    {
        return static::whereHas('order', function ($orderQuery) use ($storeId){
            $orderQuery->where('store_id', $storeId);
        })->where('status', static::STATUS_PENDING_RETURN)
            ->sum('total');
    }
}
