<?php

namespace Niyotail\Queries;

use Niyotail\Models\Picklist;

trait PicklistItemQueries
{
    public static function getItemsByStatus(Picklist $picklist, int $productId, string $status)
    {
        return static::getAllItemsQuery($picklist, $productId)
            ->where('status', $status)
            ->get();
    }

    private static function getAllItemsQuery(Picklist $picklist, int $productId)
    {
        return static::where('picklist_id', $picklist->id)
            ->whereHas('orderItem', function ($query) use ($productId) {
                $query->where('product_id', $productId);
            });
    }

    public static function getItemsByProduct(Picklist $picklist, int $productId)
    {
        return static::getAllItemsQuery($picklist, $productId)->get();
    }


    public static function getItemsWithLocations(Picklist $picklist, $status = null)
    {
        $query = static::join('order_items', 'picklist_items.order_item_id', '=', 'order_items.id')
            ->join('order_item_flat_inventories', 'picklist_items.order_item_id', '=', 'order_item_flat_inventories.order_item_id')
            ->join('flat_inventories', 'order_item_flat_inventories.flat_inventory_id', '=', 'flat_inventories.id')
            ->join('products', 'flat_inventories.product_id', '=', 'products.id')
            ->leftjoin('storages', 'storages.id' ,'=', 'flat_inventories.storage_id')
            ->leftJoin('rack_product', function ($join) {
                $join->on('rack_product.product_id', '=', 'products.id')
                    ->where('rack_product.status', 1);
            })
            ->leftJoin('product_variants', function ($join) {
                $join->on('product_variants.product_id', '=', 'products.id')
                    ->where('product_variants.value', 'unit');
            })
            ->leftJoin('warehouse_racks', 'rack_product.rack_id', '=', 'warehouse_racks.id')
            ->leftJoin('warehouse_rack_types', 'warehouse_racks.type_id', '=', 'warehouse_rack_types.id')
            ->selectRaw('count(distinct(picklist_items.id)) as quantity, products.id as product_id, product_variants.mrp as mrp, products.barcode as barcode, products.name as product,min(warehouse_rack_types.position) as position, GROUP_CONCAT(distinct(warehouse_racks.reference) Order By position asc) as location_old, storages.label as location, storages.id as storage_id')
            ->where('picklist_items.picklist_id', $picklist->id);

        if (!empty($status)) {
            $query->where('picklist_items.status', $status);
        }

        return $query->groupBy('products.id','storages.id')
            ->orderBy('position', 'asc')
            ->get();
    }

    public static function getLocationWiseItems($pickListId)
    {
        return static::join('order_item_flat_inventories', 'picklist_items.order_item_id', '=', 'order_item_flat_inventories.order_item_id')
            ->join('flat_inventories', 'order_item_flat_inventories.flat_inventory_id', '=', 'flat_inventories.id')
            ->join('products', 'flat_inventories.product_id', '=', 'products.id')
            ->join('storages', 'flat_inventories.storage_id', '=', 'storages.id')
            ->where('picklist_items.picklist_id', $pickListId)
            ->selectRaw('count(picklist_items.id) as quantity, products.name as product, products.id as product_id, products.barcode as barcode, storages.label as location, storages.id as storage_id')
            ->groupBy('products.id','storages.id')
            ->get();
    }

    public static function getLocationWiseItemsForApi($picklistId)
    {
        return static::join('order_items', 'order_items.id', '=', 'picklist_items.order_item_id')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('storages', 'picklist_items.storage_id', '=', 'storages.id')
            ->leftJoin('product_variants', function ($join) {
                $join->on('product_variants.product_id', '=', 'products.id')
                    ->where('product_variants.value', 'unit');
            })
            ->where('picklist_items.picklist_id', $picklistId)
            ->selectRaw('count(distinct(Case when picklist_items.status = "pending" then picklist_items.id End)) as pending_quantity, count(distinct(Case when picklist_items.status = "picked" then picklist_items.id End)) as picked_quantity, count(distinct(Case when picklist_items.status = "skipped" then picklist_items.id End)) as skipped_quantity, products.id as product_id, product_variants.mrp as mrp, products.barcode as barcode, products.name as product, storages.label as location, storages.id as storage_id');
    }
}
