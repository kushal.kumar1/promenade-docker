<?php

namespace Niyotail\Queries;

use Niyotail\Models\Collection;

trait CollectionQueries
{
    public static function getAllVisible()
    {
        $table = with(new static)->getTable();
        return static::active()->visible()
            ->selectRaw("*, (SELECT id FROM $table parent_collections WHERE parent_collections.lft < $table.lft AND parent_collections.rgt > $table.rgt ORDER BY parent_collections.rgt-$table.rgt ASC limit 1) AS parent_id")
            ->orderBy('lft')->get();
    }

    // public static function getCollectionMenu()
    // {
    //     $table = with(new static)->getTable();
    //     return static::active()->visible()
    //         ->selectRaw("*, (SELECT id FROM $table parent_collections WHERE parent_collections.lft < $table.lft AND parent_collections.rgt > $table.rgt ORDER BY parent_collections.rgt-$table.rgt ASC limit 1) AS parent_id")
    //         ->whereNotIn('slug', ['color','material','pattern'])
    //         ->orderBy('lft')->get();
    // }
    //
    // public static function getCollectionFilter()
    // {
    //     $table = with(new static)->getTable();
    //     return static::fromRaw("$table AS $table, $table AS parent")
    //         ->selectRaw("$table.*,(SELECT id FROM $table parent_collections WHERE parent_collections.lft < $table.lft AND parent_collections.rgt > $table.rgt ORDER BY parent_collections.rgt-$table.rgt ASC limit 1) AS parent_id")
    //         ->whereRaw("$table.lft BETWEEN parent.lft AND parent.rgt")
    //         ->WhereIn("parent.slug", ['color','material','pattern'])
    //         ->orderBy("$table.lft")->get();
    // }

    public static function getAllWithParent()
    {
        $table = with(new static)->getTable();
        return static::selectRaw("*, (SELECT id FROM $table parent_collections WHERE parent_collections.lft < $table.lft AND parent_collections.rgt > $table.rgt ORDER BY parent_collections.rgt-$table.rgt ASC limit 1) AS parent_id")
            ->orderBy('lft')->get();
    }

    public static function getParents($childId)
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('node.id', $childId)
            ->orderBy('parent.lft')->get();
    }

    public static function getChildren($parentId)
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->select('node.*')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('parent.id', $parentId)
            ->orderBy('node.lft')->get();
    }

    public static function getAllRoots()
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->selectRaw('node.* , (COUNT(parent.name) - 1) as depth')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->groupBy('node.name')->orderBy('node.lft')->havingRaw('depth < 1')->get();
    }


    public static function getAllVisibleRoots()
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->raw("join collection_product on 'node.id' = 'collection_product.collection_id'")
            ->selectRaw('node.* , (COUNT(parent.name) - 1) as depth')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('node.status',1)->where('node.is_visible',1)
            ->groupBy('node.name')->orderBy('node.lft')->havingRaw('depth < 1')->get();
    }

    public static function getByIds($ids)
    {
        return static::active()->whereIn("id", $ids)->get();
    }

    public static function searchByKeyword($term, $limit = 10)
    {
        return static::where('name', 'like', "%$term%")->limit($limit)->get();
    }
}
