<?php
namespace Niyotail\Queries;

use Niyotail\Models\ProfileQuestionOption;

trait ProfileQuestionOptionQueries
{
    public static function createOption($arr)
    {
        $question = new ProfileQuestionOption();
        foreach ($arr as $key=>$value)
        {
            $question->{$key} = $value;
        }

        $question->save();
        return $question;
    }

    public static function updateOption($arr)
    {
        $question = ProfileQuestionOption::find($arr['id']);
        unset($arr['id']);
        foreach ($arr as $key=>$value)
        {
            $question->{$key} = $value;
        }

        $question->save();
        return $question;
    }
}
