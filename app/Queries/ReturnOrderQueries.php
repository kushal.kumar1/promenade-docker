<?php


namespace Niyotail\Queries;

use Niyotail\Models\ReturnOrder;

trait ReturnOrderQueries
{
    public static function getReturnsByStore($storeId) {
        return static::with('shipment.invoice', 'order',
            'orderItems.product', 'orderItems.returnOrderItems', 'creditNote.transaction')
            ->whereHas('order', function ($query) use ($storeId) {
                $query->where('store_id', $storeId);
            })
            ->whereRaw("date(convert_tz(created_at, '+00:00','+05:30')) >= '2021-04-01'")
            ->where('status','!=', ReturnOrder::STATUS_CANCELLED)
            ->orderBy('created_at', 'desc')
            ->paginate();
    }

    public function scopeReturnsByStore($query, $storeId)
    {
        return $query->with('shipment.invoice', 'order',
        'orderItems.product', 'orderItems.returnOrderItems', 'creditNote.transaction')
        ->whereHas('order', function ($query) use ($storeId) {
            $query->where('store_id', $storeId);
        })
        ->where('status','!=', ReturnOrder::STATUS_CANCELLED);
    }

    public static function getReturnsByStoreOrDate($storeId, $fromDate = null, $toDate = null)
    {
          $query = static::selectRaw('return_orders.reference_id as ref_id, count(distinct(return_orders.id)) as return_order_count,
                                 date(return_orders.created_at) as return_date, count(distinct(order_items.id)) as item_count')
                        ->join('return_order_items', 'return_orders.id', '=', 'return_order_items.return_order_id')
                        ->join('order_items', 'return_order_items.order_item_id', '=', 'order_items.id')
                        ->whereHas('order', function ($query) use ($storeId) {
                            $query->where('store_id', $storeId);
                        })->where('return_orders.status','!=', ReturnOrder::STATUS_CANCELLED);
          if(!empty($fromDate)) {
              $query = $query->whereRaw("date(convert_tz(return_orders.created_at, '+00:00','+05:30')) >= '$fromDate'");
              if (!empty($toDate)) {
                  $query = $query->whereRaw("date(convert_tz(return_orders.created_at, '+00:00','+05:30')) <= '$toDate'");
              }
          } else {
              $query = $query->whereRaw("date(convert_tz(return_orders.created_at, '+00:00','+05:30')) >= '2021-04-01'");
          }

          return $query->groupBy('return_orders.reference_id')
                      ->groupBy(\DB::raw('date(return_orders.created_at)'))
                      ->orderBy('return_orders.reference_id', 'desc')
                      ->paginate();
    }

    public static function getReturnsByReferenceId($refId)
    {
        return static::with('shipment.invoice', 'order', 'orderItems.product', 'orderItems.productVariant',
                            'orderItems.returnOrderItems.returnOrder', 'creditNote.transaction')
                    ->where('reference_id', $refId)
                    ->get();
    }
}
