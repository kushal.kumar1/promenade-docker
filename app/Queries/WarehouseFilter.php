<?php

namespace Niyotail\Queries;

use Niyotail\Models\WarehouseScope;

trait WarehouseFilter
{
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new WarehouseScope);

    }
}