<?php

namespace Niyotail\Queries;

use Niyotail\Models\InventoryMigration\InventoryMigration;
use Niyotail\Models\InventoryMigration\InventoryMigrationItem;
use Niyotail\Models\Putaway\Putaway;

trait InventoryMigrationQueries
{
    public static function getMigrations($forWarehouseId, $forEmployeeId = false, $forId = false)
    {
        $whereCondition = ['warehouse_id' => $forWarehouseId, 'status' => InventoryMigration::STATUS_OPEN];
        $query = static::where($whereCondition);

        if (!empty($forEmployeeId)) {
            $query = $query->whereHas('items', function($q) use($forEmployeeId) {
                          if (!empty($forEmployeeId)) {
                              $q->where('employee_id', $forEmployeeId);
                          }
                          $q->where('status', '!=', InventoryMigrationItem::STATUS_COMPLETED);
//                              ->orWhereHas('inventoryMigration.putaways', function ($subQuery) {
//                                $subQuery->where('status', '!=', Putaway::STATUS_CLOSED);
//                              });
                      });

        }

        if (!empty($forId)) {
            $query = $query->where('id', $forId);
        }
        return $query->with(['putaways' => function($q) use($forEmployeeId){
                        if (!empty($forEmployeeId)) {
                            $q->where('employee_id', $forEmployeeId);
                        }
                        $q->where('status', '!=', Putaway::STATUS_CLOSED);
                    },
                    'items' => function($q) use($forEmployeeId) {
                        if (!empty($forEmployeeId)) {
                            $q->where('employee_id', $forEmployeeId);
                        }
                        $q->with('product', 'fromStorage', 'toStorage')->where('status', '!=', InventoryMigrationItem::STATUS_COMPLETED);
                    }])->orderBy('id', 'desc');
    }

    public static function getMigrationForPutItem($inventoryMigrationId, $productId, $sourceStorageId, $employeeId)
    {
        return static::with(['putaways' => function ($putawayQuery) use($employeeId) {
                                  $putawayQuery->where('status', Putaway::STATUS_OPEN)->where('employee_id', $employeeId);
                              }, 'items' => function ($itemQuery) use ($productId, $sourceStorageId) {
                                  $itemQuery->where('product_id', $productId)->where('from_storage_id', $sourceStorageId)
                                            ->where('status', '!=', InventoryMigrationItem::STATUS_COMPLETED);
                              }])->whereHas('items', function($q) use($employeeId, $productId, $sourceStorageId) {
                                  $q->where('employee_id', $employeeId)->where('product_id', $productId)
                                    ->where('from_storage_id', $sourceStorageId)->where('status', '!=', InventoryMigrationItem::STATUS_COMPLETED);
                              })->whereDoesntHave('putaways', function($q) use($employeeId) {
                                  $q->where('status', Putaway::STATUS_STARTED)->where('employee_id', $employeeId);
                              })->find($inventoryMigrationId);
    }
}
