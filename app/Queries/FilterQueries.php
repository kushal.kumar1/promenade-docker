<?php

namespace Niyotail\Queries;


use Illuminate\Support\Str;

trait FilterQueries
{
    public function scopeFilters($query,$request)
    {
        if(!empty($request->filters)){
            if(!empty($request->filters['eq'])){
                foreach ($request->filters['eq'] as $field=>$value){
                    $fields=explode('.',$field);
                    if(count($fields) > 1){
                        $query->whereHas(Str::camel($fields[0]),function ($query)use($fields,$value){
                           $query->where($fields[1],$value);
                        });
                    }else{
                        $query->where($field,$value);
                    }
                }
            }
            if(!empty($request->filters['like'])){
                foreach ($request->filters['like'] as $field=>$value){
                    $fields=explode('.',$field);
                    if(count($fields) > 1){
                        $query->whereHas(Str::camel($fields[0]),function ($query)use($fields,$value){
                            $query->where($fields[1], 'like','%'.$value.'%');
                        });
                    }else{
                        $query->where($field, 'like','%'.$value.'%');
                    }
                }
            }
            if(!empty($request->filters['sort'])){
                foreach($request->filters['sort'] as $field=>$type){
                    $query->orderBy($field, ($type=='asc')?'asc':'desc');
                }
            }
        }
        return $query;
    }
}


