<?php
namespace Niyotail\Queries;

trait CartQueries
{
    public function scopeWithApiData($query)
    {
        return $query->with(['items.product'=>function ($query) {
            $query->with('variants', 'images', 'brand');
        }])
        ->with('store')
        ->with('user');
    }
}
