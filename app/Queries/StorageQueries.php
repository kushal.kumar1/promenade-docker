<?php
namespace Niyotail\Queries;

trait StorageQueries
{

    public static function getByStore($storeId)
    {
        return static::whereHas('stores',function ($query)use($storeId){
            $query->where('stores.id',$storeId);
        })->withCount('stores')->get();
    }

}
