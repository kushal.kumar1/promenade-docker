<?php
namespace Niyotail\Queries;

trait WarehouseQueries
{
    public static function getInventoryByVariant($variantId)
    {
        return static::with(["inventory" => function ($query) use ($variantId) {
            $query->where('product_variant_id', $variantId);
        }]);
    }
}
