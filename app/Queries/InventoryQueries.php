<?php

namespace Niyotail\Queries;

use Niyotail\Models\Product;

trait InventoryQueries
{
    public static function getAvailableInventories($orderItem)
    {
        $warehouseId = $orderItem->order->warehouse_id;
        return static::active()
            ->where('product_id', $orderItem->product_id)
            ->where('warehouse_id', $warehouseId)
            ->where('quantity', '>', 0)
            ->orderBy('created_at', 'asc')
            ->get();
    }

    public static function getProductStockByWarehouse(Product $product, int $warehouseId)
    {
        $row = static::active()
            ->selectRaw('product_id, sum(quantity) as total')
            ->where('product_id', $product->id)
            ->where('warehouse_id', $warehouseId)
            ->groupBy('product_id')
            ->first();

        return $row->total ?? 0;
    }
}
