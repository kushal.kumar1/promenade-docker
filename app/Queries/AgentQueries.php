<?php
namespace Niyotail\Queries;

use DateTime;
use DateTimeZone;
use Carbon\Carbon;

trait AgentQueries
{
    public static function getTodaySalesData($agentID)
    {
        $today = Carbon::today();
        return static::where('id', $agentID)
                      ->with(['orders' => function($query) use($today) {
                          $query->where('status', '!=', 'cancelled')
                                ->whereRaw("convert_tz(created_at, '+00:00', '+05:30') >= '$today'");
                      }])
                      ->first();
        // return static::selectRaw("date(convert_tz(invoices.created_at, '+00:00', '+05:30')) as date, sum(order_items.total) as total, count(distinct orders.id) as order_count, count(distinct(orders.store_id)) as store_count")
        //               ->join('orders', function($join) {
        //                   $join->on('orders.created_by_id', '=', 'agents.id')
        //                       ->where('orders.created_by_type', '=', 'agent');
        //               })
        //               ->join('order_items', 'orders.id', 'order_items.order_id')
        //               ->join('shipments', function($join) {
        //                   $join->on('shipments.order_id', '=', 'orders.id')
        //                       ->where('shipments.status', '!=', 'cancelled');
        //               })
        //               ->join('shipment_items', function($join) {
        //                   $join->on('shipment_items.shipment_id', '=', 'shipments.id')
        //                       ->on('shipment_items.order_item_id', '=', 'order_items.id');
        //               })
        //               ->join('invoices', function($join) {
        //                   $join->on('invoices.order_id', '=', 'orders.id')
        //                       ->on('invoices.shipment_id', '=', 'shipments.id')
        //                       ->where('invoices.status', '!=', 'cancelled');
        //               })
        //               ->where('agents.id', $agentID)
        //               ->where('orders.status', '!=', 'cancelled')
        //               ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
        //               ->whereRaw("convert_tz(invoices.created_at, '+00:00', '+05:30') >= '$today'")
        //               ->first();
    }

    public static function getMonthwiseSalesData($agentID, $request)
    {
        if ($request->has('start_date')) {
            $startDate = Carbon::parse($request->start_date)->toDateString();
        } else {
            $startDate = date('Y-m-01 00:00:00');
        }

        if ($request->has('end_date')) {
            $endDate = Carbon::parse($request->end_date)->addDay()->toDateString();
        } else {
            $endDate = (new DateTime())->setTimestamp(strtotime('+1 days'))
                    ->setTimezone(new DateTimeZone('Asia/Kolkata'))->format('Y-m-d 00:00:00');
        }
        return static::selectRaw("DATE_FORMAT(invoices.created_at, '%Y-%m') as date, sum(order_items.total) as total, count(distinct orders.id) as order_count, count(distinct(orders.store_id)) as store_count")
                      ->join('orders', function($join) {
                          $join->on('orders.created_by_id', '=', 'agents.id')
                              ->where('orders.created_by_type', '=', 'agent');
                      })
                      ->join('order_items', 'orders.id', 'order_items.order_id')
                      ->join('shipments', function($join) {
                          $join->on('shipments.order_id', '=', 'orders.id')
                              ->where('shipments.status', '!=', 'cancelled');
                      })
                      ->join('shipment_items', function($join) {
                          $join->on('shipment_items.shipment_id', '=', 'shipments.id')
                              ->on('shipment_items.order_item_id', '=', 'order_items.id');
                      })
                      ->join('invoices', function($join) {
                          $join->on('invoices.order_id', '=', 'orders.id')
                              ->on('invoices.shipment_id', '=', 'shipments.id')
                              ->where('invoices.status', '!=', 'cancelled');
                      })
                      ->where('agents.id', $agentID)
                      ->where('orders.status', '!=', 'cancelled')
                      ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
                      ->groupBy('date')
                      ->get();
    }
}
