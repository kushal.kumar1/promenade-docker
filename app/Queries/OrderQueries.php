<?php
namespace Niyotail\Queries;

trait OrderQueries
{
    public function scopeWithApiData($query)
    {
        return $query->with(['items.product'=>function ($query) {
            $query->with('images', 'brand');
        }])->with('user', 'store', 'store.beat', 'store.creditLimit', 'shippingAddress.city.state.country', 'billingAddress.city.state.country', 'rating');
    }
}
