<?php


namespace Niyotail\Queries;


trait MarketerQueries
{
    public static function getByIds($ids)
    {
        return static::whereIn("id", $ids)->get();
    }
}