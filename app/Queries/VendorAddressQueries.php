<?php

namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait VendorAddressQueries
{
    public static function searchByVendorAddressId($term, $limit = 20)
    {
        return static::where("id", "like", "%$term%")->limit($limit)->get();
    }
}
