<?php
namespace Niyotail\Queries;

use Niyotail\Models\ProfileQuestion;

trait ProfileQuestionQueries
{
    public static function getQuestions($storeID)
    {
        return static::active()
                    ->with(['answerOptions' => function($query) use($storeID) {
                        $query->active()->orderBy('priority');
                        $query->with(['storeAnswers' => function($subQuery) use ($storeID){
                          $subQuery->where('store_id', $storeID);
                        }]);
                    }])
                    ->whereDoesntHave('storeAnswers', function($subQuery) use ($storeID) {
                        $subQuery->where('store_id', $storeID)
                              ->whereRaw('updated_at <= DATE_SUB(NOW(), INTERVAL 3 HOUR)');
                    })
                    ->orderBy('priority')->paginate(10);
    }

    public static function getQuestion($questionID, $storeID, $answerIDs = null)
    {
        return static::active()
                    ->where('id', $questionID)
                    ->with(['answerOptions' => function($query) use($storeID, $answerIDs) {
                        if (!empty($answerIDs)) {
                          $query->active()->whereIn('id', $answerIDs);
                        }
                        $query->with(['storeAnswers' => function($subQuery) use ($storeID){
                          $subQuery->where('store_id', $storeID);
                        }]);
                    }])
                    ->first();
    }

    public static function getStoreAnswers($questionID, $storeID)
    {
        return static::active()
                    ->where('id', $questionID)
                    ->with(['storeAnswers' => function($subQuery) use ($storeID){
                      $subQuery->where('store_id', $storeID);
                    }])
                    ->whereHas('storeAnswers', function($subQuery) use($storeID) {
                        $subQuery->where('store_id', $storeID);
                    })
                    ->get();
    }

    public static function createQuestion($arr)
    {
        $question = new ProfileQuestion();
        foreach ($arr as $key=>$value)
        {
            $question->{$key} = $value;
        }

        $question->save();
        return $question;
    }

    public static function updateQuestion($arr)
    {
        $question = ProfileQuestion::find($arr['id']);
        unset($arr['id']);
        foreach ($arr as $key=>$value)
        {
            $question->{$key} = $value;
        }

        $question->save();
        return $question;
    }
}
