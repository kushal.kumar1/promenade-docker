<?php

namespace Niyotail\Queries;

trait BrandQueries
{
    public static function searchWithKeyword($term, $limit = 20)
    {
        return static::where("name", "like", "%$term%")->limit($limit)->get();
    }

    public static function getAllWithParent()
    {
        $table = with(new static)->getTable();
        return static::selectRaw("*, (SELECT id FROM $table parent_brands WHERE parent_brands.lft < $table.lft AND parent_brands.rgt > $table.rgt ORDER BY parent_brands.rgt-$table.rgt ASC limit 1) AS parent_id")
            ->orderBy('lft')->get();
    }

    public static function getParents($childId)
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('node.id', $childId)
            ->orderBy('parent.lft')->get();
    }

    public static function getChildren($parentId)
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->select('node.*')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('parent.id', $parentId)
            ->orderBy('node.lft')->get();
    }

    public static function getAllRoots()
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->selectRaw('node.* , (COUNT(parent.name) - 1) as depth')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->groupBy('node.name')->orderBy('node.lft')->havingRaw('depth < 1')->get();
    }


    public static function getAllVisibleRoots()
    {
        $table = with(new static)->getTable();
        return static::fromRaw("$table AS node, $table AS parent")
            ->raw("join brand_product on 'node.id' = 'brand_product.brand_id'")
            ->selectRaw('node.* , (COUNT(parent.name) - 1) as depth')
            ->whereRaw('node.lft BETWEEN parent.lft AND parent.rgt')
            ->where('node.status', 1)->where('node.is_visible', 1)
            ->groupBy('node.name')->orderBy('node.lft')->havingRaw('depth < 1')->get();
    }

    public static function getAllVisible()
    {
        $table = with(new static)->getTable();
        return static::active()->visible()
            ->selectRaw("*, (SELECT id FROM $table parent_brands WHERE parent_brands.lft < $table.lft AND parent_brands.rgt > $table.rgt ORDER BY parent_brands.rgt-$table.rgt ASC limit 1) AS parent_id")
            ->orderBy('lft')->get();
    }

    public static function getByIds($ids)
    {
        return static::active()->whereIn('id', $ids)->get();
    }
}
