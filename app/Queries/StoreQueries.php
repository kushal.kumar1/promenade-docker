<?php

namespace Niyotail\Queries;

use Carbon\Carbon;
use Niyotail\Models\OrderItem;
use Niyotail\Models\PosOrder;

trait StoreQueries
{
    public static function getWithLatestInvoices($storeID)
    {
        return static::where('id', $storeID)->with(['invoices' => function ($query) {
            $query->whereNotIn('invoices.status', ['cancelled', 'settled']);
            $query->with('invoiceTransactions')->orderBy('number', 'desc');
        }])
            ->with(['transactions' => function ($query) {
                $query->where('type', 'credit')->where('payment_details', 'like', '%cheque_number%')->orderBy('created_at', 'desc');
            }])
            ->first();
    }

    public static function getWithLatestTransactions($storeID)
    {
        return static::where('id', $storeID)
            ->with(['transactions' => function ($query) {
                $query->toBeSettled()->orWhere(function ($q) {
                    $q->where('type', 'credit')->where('payment_details', 'like', '%cheque_number%');
                })->orderBy('created_at', 'desc');
            }])
            ->first();
    }

    public static function searchWithKeyword($term, $limit = 10, $includes = [])
    {
        $query = static::where('name', 'like', "%$term%")
            ->orWhere('address', 'like', "%$term%")
            ->orWhere('id', $term)
            ->active()->verified();

        if (!empty($includes)) {
            $query->with($includes);
        }

        return $query->limit($limit)->get();
    }

    public static function getByIds($ids)
    {
        return static::whereIn('id', $ids)->get();
    }

    public static function getCommissionEarned($storeId, $fromDate, $toDate)
    {
        $startDate = Carbon::parse($fromDate)->startOfDay()->toDateTimeString();
        $endDate = Carbon::parse($toDate)->endOfDay()->toDateTimeString();

        return static::where('stores.id', $storeId)
            ->selectRaw('sum(pos_order_item_metas.commission) as commission')
            ->leftJoin('pos_orders', 'stores.id', '=', 'pos_orders.store_id')
            ->leftJoin('pos_order_items', 'pos_orders.id', '=', 'pos_order_items.pos_order_id')
            ->leftJoin('pos_order_item_shipments', 'pos_order_items.id', '=', 'pos_order_item_shipments.pos_order_item_id')
            ->leftJoin('pos_order_item_metas', 'pos_order_items.id', '=', 'pos_order_item_metas.pos_order_item_id')
            ->whereNotNull('pos_order_item_shipments.shipment_id')
            ->where('pos_orders.type', '!=', PosOrder::TYPE_WHOLESALE)
            ->groupBy('pos_order_items.id')
            ->whereBetween('pos_orders.invoiced_at', [$startDate, $endDate])
            ->get();
    }
}
