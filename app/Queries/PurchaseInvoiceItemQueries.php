<?php

namespace Niyotail\Queries;

use Illuminate\Support\Facades\DB;

trait PurchaseInvoiceItemQueries
{
    public static function searchByPurchaseInvoiceItemId($term, $limit = 20)
    {
        return static::where("transaction_id", "like", "%$term%")->limit($limit)->get();
    }
}
