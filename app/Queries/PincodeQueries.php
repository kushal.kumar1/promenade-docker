<?php

namespace Niyotail\Queries;

trait PincodeQueries
{
    public static function checkServiceability($pincode)
    {
        $pincode= static::where("pincode", $pincode)
        ->whereHas('warehouses')->first();
        return !empty($pincode);
    }
}
