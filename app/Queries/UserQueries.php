<?php
namespace Niyotail\Queries;
use Niyotail\Models\User;
use Illuminate\Support\Facades\DB;

trait UserQueries
{


    public static function getDetails($id)
    {
        return static::where('id', $id)->with('addresses', 'orders')->first();
    }

    public static function getWishCount($id)
    {
        return static::withCount('wishlist')->where('id', $id)->first()->wishlist_count;
    }

    public function hasStoreAccess($storeId)
    {
        $user= User::whereHas('stores',function ($query)use ($storeId){
            $query->where('stores.id',$storeId);
        })->find($this->id);
        return !empty($user);

    }

    public function hasClub1KStoreAccess(int $storeId)
    {
        $user = DB::select(DB::raw("select stores.* from users
        inner join stores on (stores.id=?)
        inner join store_tag on (store_tag.store_id=$storeId and store_tag.store_id=stores.id and store_tag.tag_id in (8,9))
        where users.id=? and users.status=1 and users.verified=1"), [$storeId, $this->id]);
        return !empty($user);
    }

}
