<?php

namespace Niyotail\Queries;

use Niyotail\Models\FlatInventory;
use Niyotail\Models\Order;
use Niyotail\Models\OrderItem;
use Niyotail\Models\Product;
use Niyotail\Models\Storage;

/**
 * Trait ProductQueries
 *
 * @package Niyotail\Queries
 */
trait ProductQueries
{
    public static function searchByProductId($term)
    {
        return static::where("id", "like", "$term")->get();
    }

    public static function searchWithInventoryCount($keyword, $storeId, $limit = 10)
    {
        return static::where("name", "like", "%$keyword%")
            ->orWhere("code", "like", "%$keyword%")
            ->orWhereHas('variants', function ($query) use ($keyword) {
                $query->where('sku', "like", "%$keyword%");
            })
            ->with('images')
            ->with(['variants' => function ($query) use ($storeId) {
                $query->withTotalInventory($storeId);
            }])->limit($limit)->get();
    }

    public static function getByIds($ids)
    {
        return static::active()->whereIn('id', $ids)->get();
    }

    public static function searchSimpleWithKeyword($keyword, $limit = 20, $includes = [])
    {
        $query = static::where('name', 'like', "%$keyword%")
            ->orWhere('barcode', "%{$keyword}%");
        if (!empty($includes)) $query->with($includes);
        return $query->limit($limit)->get();
    }

    public static function searchSimpleWithBarcode($barcode, $limit = 20, $includes = [])
    {
        $query = static::where('barcode', $barcode);
        if (!empty($includes)) $query->with($includes);
        return $query->limit($limit)->get();
    }


    public static function searchWithKeyword($keyword, $limit = 20)
    {
        $keyword = str_replace(' ', '%', $keyword);
        return static::selectRaw('products.*, sum(quantity) as total_inventory')
            ->active()->where("name", "like", "%$keyword%")
            ->with('images', 'brand')
            ->orWhere("code", "like", "%$keyword%")
            ->orWhereHas('variants', function ($query) use ($keyword) {
                $query->where('sku', "like", "%$keyword%");
            })
            ->join('inventories', 'inventories.product_id', '=', 'products.id')
            ->groupBy('product_id')
            ->orderBy('total_inventory', 'desc')
            ->limit($limit)->get();
    }

    public static function getCurrentMonthTopSKUData($agentID, $startDate, $endDate)
    {
        return static::
        selectRaw("products.name, products.id, sum(order_items.quantity) as quantity, order_items.sku, sum(order_items.total) as order_item_price")
            ->join('order_items', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('agents', 'orders.created_by_id', '=', 'agents.id')->where('orders.created_by_type', 'agent')
            ->where('agents.id', $agentID)
            ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$startDate' and '$endDate'")
            ->where('orders.status', '!=', 'cancelled')
            ->where('order_items.status', '!=', 'processing')
            //->groupBy('order_items.sku')
            ->groupBy('products.id')
            ->orderBy('order_item_price', 'desc')->take(10)->get();
    }

    public static function getCurrentMonthTopSKUDataForMargin($agentID, $startDate, $endDate)
    {
        return static::selectRaw("products.name, products.id, sum(order_items.total) as selling_price,
                COALESCE(sum(order_item_inventories.quantity*order_item_inventories.unit_cost_price), sum(order_items.quantity * flat_inventories.unit_cost)) as cost_price,
                (sum(order_items.total) - COALESCE(sum(order_item_inventories.quantity*order_item_inventories.unit_cost_price), sum(order_items.quantity * flat_inventories.unit_cost))) as margin")
            ->join('order_items', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'orders.id', '=', 'order_items.order_id')
            ->join('order_item_inventories', 'order_items.id', '=', 'order_item_inventories.order_item_id')
            ->leftJoin('order_item_flat_inventories', 'order_item_flat_inventories.order_item_id', '=', 'order_items.id')
            ->leftJoin('flat_inventories', 'order_item_flat_inventories.flat_inventory_id', '=', 'flat_inventories.id')
            ->join('product_images', 'product_images.product_id', '=', 'products.id')
            ->where('orders.created_by_type', 'agent')
            ->where('orders.created_by_id', $agentID)
            ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$startDate' and '$endDate'")
            ->where('orders.status', '!=', 'cancelled')
            ->where('order_items.status', '!=', 'processing')
            //->groupBy('order_items.sku')
            ->groupBy('products.id')
            ->orderBy('margin', 'desc')->take(10)->get();
    }

    /**
     * Returns all group products with stock for warehouse.
     * @param Product $product
     * @param int $warehouseId
     * @param bool $excludeSelf
     * @return null
     */
    public static function getGroupProductsWithStock(Product $product, int $warehouseId, bool $excludeSelf = true)
    {
        if (!empty($product->group_id)) {
            $query = static::where('group_id', $product->group_id)->inventoryCount([$warehouseId]);
            if ($excludeSelf) {
                $query->where('id', '!=', $product->id);
            }

            return $query->get();
        }

        return null;
    }

    public function scopeApiFilters($query, $request)
    {
        /*if ($request->has('brandIds')) {
            $query->whereIn('brand_id', $request->brandIds);
        }
        if ($request->has('collections')) {
            $query->whereHas('collections', function ($query) use ($request) {
                $query->whereIn('collections.id', $request->collections);
            });
        }
        if ($request->has('new')) {
            $query->whereHas('tags', function ($query) use ($request) {
                $query->where('tags.name', 'New');
            });
            $query->orWhereDoesntHave('tags', function ($query) use ($request) {
                $query->whereRaw('published_at >= NOW() - INTERVAL 1 WEEK');
            });
            $query->orderBy('published_at', 'desc');
        }

        if ($request->has('ufml')) {
            $query->whereHas('tags', function ($query) use ($request) {
                $query->where('tags.id', '1');
            });
        }

        if ($request->has('margin')) {
            $margin = explode('-', $request->margin);
            $query->whereHas('variants', function ($query) use ($request, $margin) {
              if (($margin[0] != '*') && ($margin[1] != '*')) {
                $query->whereRaw('((1- (price/mrp)) * 100) > '.$margin[0].' and ((1- (price/mrp)) * 100) <='.$margin[1]);
              } else if ($margin[0] == '*') {
                $query->whereRaw('((1- (price/mrp)) * 100) < '.$margin[1]);
              } else if ($margin[1] == '*') {
                $query->whereRaw('((1- (price/mrp)) * 100) > '.$margin[0]);
              }
              $query->orderByRaw('((1- (price/mrp)) * 100) desc');
            });
        }*/

        //Show only beat available products
        if (!empty($request->warehouse_id)) {
            $query->whereHas('inventories', function ($q) use ($request) {
                $q->whereIn('warehouse_id', $request->warehouse_id);
            });
        } else {
            $query->whereHas('inventories', function ($q) {
                $q->whereNotIn('warehouse_id', ['2']);
            });
        }
    }

    public function scopeWithLowStock($query, $minimumStockInDays, $saleStartDate, $saleEndDate)
    {
        return $query->selectRaw("products.*,MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) as first_sale_date,SUM(order_item_inventories.quantity) as total_sales,
              (select sum(quantity) from inventories where product_id=products.id) as total_stock,
             CASE
               WHEN order_items.id is NULL THEN
                 '0'
              WHEN MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) > '$saleStartDate' THEN
                  round((SUM(order_item_inventories.quantity))/DATEDIFF('$saleEndDate',(MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')))))
                ELSE
                  round((SUM(order_item_inventories.quantity))/DATEDIFF('$saleEndDate','$saleStartDate'))
                END as sale_per_day,
                CASE
                 WHEN order_items.id is NULL THEN
                    '0'
                 WHEN MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) > '$saleStartDate' THEN
                  DATEDIFF('$saleEndDate',(MIN(convert_tz(order_items.created_at, '+00:00', '+05:30'))))
                ELSE
                  DATEDIFF('$saleEndDate','$saleStartDate')
                END as days_for_average_sale
                ")
            ->leftJoin('order_items', 'order_items.product_id', '=', 'products.id')
            ->leftJoin('order_item_inventories', 'order_items.id', '=', 'order_item_inventories.order_item_id')
            ->whereRaw("order_items.id not in (select order_item_id from return_order_items)")
            ->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$saleStartDate' and '$saleEndDate'")
            ->whereNotIn('order_items.status', [OrderItem::STATUS_CANCELLED, OrderItem::STATUS_PROCESSING])
            ->havingRaw("total_stock < (sale_per_day * $minimumStockInDays)")
            ->groupBy('products.id');
    }

    public function scopeWithStock($query, $saleStartDate, $saleEndDate)
    {
        return $query->selectRaw("products.*,MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) as first_sale_date,SUM(order_item_inventories.quantity) as total_sales,
              (select sum(quantity) from inventories where product_id=products.id) as total_stock,
             CASE
                WHEN order_items.id is NULL THEN
                 '0'
                WHEN MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) > '$saleStartDate' THEN
                  round((SUM(order_item_inventories.quantity))/DATEDIFF('$saleEndDate',(MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')))))
                ELSE
                  round((SUM(order_item_inventories.quantity))/DATEDIFF('$saleEndDate','$saleStartDate'))
                END as sale_per_day,
                CASE
                WHEN order_items.id is NULL THEN
                    '0'
                WHEN MIN(convert_tz(order_items.created_at, '+00:00', '+05:30')) > '$saleStartDate' THEN
                  DATEDIFF('$saleEndDate',(MIN(convert_tz(order_items.created_at, '+00:00', '+05:30'))))
                ELSE
                  DATEDIFF('$saleEndDate','$saleStartDate')
                END as days_for_average_sale")
            ->leftJoin('order_items', function ($join) use ($saleStartDate, $saleEndDate) {
                $join->on('order_items.product_id', '=', 'products.id');
                $join->whereRaw("order_items.id not in (select order_item_id from return_order_items)");
                $join->whereRaw("convert_tz(order_items.created_at, '+00:00', '+05:30') between '$saleStartDate' and '$saleEndDate'");
                $join->whereNotIn('order_items.status', [OrderItem::STATUS_CANCELLED, OrderItem::STATUS_PROCESSING]);
            })->leftJoin('order_item_inventories', 'order_items.id', '=', 'order_item_inventories.order_item_id')
            ->groupBy('products.id');
    }

    // This is independent of warehouse, TODO:: make it warehouse dependent
//    public static function getTotalGroupStock(Product $product)
//    {
//        $products = self::getGroupProductsWithStock($product, false);
//        $groupStock = $products->reduce(function ($total, $product) {
//            return $total + $product->total_inventory;
//        }, 0);
//        return $groupStock;
//    }

    public function scopeInventoryCount($query, array $warehouseId = null)
    {
        $skipStorages = Storage::whereIn('warehouse_id', $warehouseId)
            ->whereIn('zone', [Storage::ZONE_LIQUIDATION, Storage::ZONE_NEAR_EXPIRY])
            ->get()
            ->pluck('id')
            ->toArray();

        if (!empty($warehouseId)) {
            return $query->withCount(['inventoriesV2 as current_inventory' => function ($inventoryQuery) use ($warehouseId, $skipStorages) {
                $inventoryQuery->whereIn('warehouse_id', $warehouseId)
                    ->where('status', FlatInventory::STATUS_READY_FOR_SALE);
                if (!empty($skipStorages)) {
                    $inventoryQuery->whereNotIn('storage_id', $skipStorages);
                }
            }]);
        }
        return $query->withCount(['inventoriesV2 as current_inventory' => function ($inventoryQuery) use($skipStorages) {
                            $inventoryQuery->where('warehouse_id', '!=', 1)
                                ->where('status', FlatInventory::STATUS_READY_FOR_SALE);
                            if (!empty($skipStorages)) {
                                $inventoryQuery->whereNotIn('storage_id', $skipStorages);
                            }
                        }]);
    }

    public function scopeBlockInventoryCount($query, $warehouseId=null)
    {
        if(!empty($warehouseId)){
            return $query->withCount(['orderItems as blocked_inventory' => function ($orderItemQuery) use ($warehouseId) {
                $orderItemQuery->whereIn('status', [Order::STATUS_MANIFESTED, OrderItem::STATUS_PROCESSING])
                    ->whereHas('order', function ($orderQuery) use ($warehouseId) {
                        $orderQuery->where('warehouse_id', $warehouseId);
                    });
            }]);
        }

        return $query->withCount(['orderItems as blocked_inventory' => function ($orderItemQuery) {
            $orderItemQuery->whereIn('status', [Order::STATUS_MANIFESTED, OrderItem::STATUS_PROCESSING]);
        }]);
    }
}
