<?php
namespace Niyotail\Contracts\Authentication;

/**
 * Interface EmployeeAuthContract
 *
 * @package Niyotail\Contracts\Authentication
 * @author  Ankit <ankitjain0269@gmail.com>
 */
interface AdminAuthContract
{
    /**
     * @param $user
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function userHasLoggedIn($user);

    /**
     * @param $message
     *
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function userLogInFailed($message);

    /**
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function userIsBlocked();


    /**
     * @return mixed
     * @author Ankit <ankitjain0269@gmail.com>
     */
    public function userHasLoggedOut();
}
