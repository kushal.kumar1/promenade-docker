<?php

namespace Niyotail\Contracts\Authentication;

interface WebAuthContract
{

    public function userCanLogIn($user);

    public function userLogInFailed($message);

    public function userIsBlocked();

    public function userHasLoggedOut();

    public function userSocialEmailMissing();

    public function userPasswordResetDone();

    public function userPasswordResetFailed($message);

}