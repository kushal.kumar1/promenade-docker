<?php

namespace Niyotail\Contracts\Authentication;

use Niyotail\Models\Employee;

interface EmployeeAuthContract
{
    public function employeeLogInFailed($message);

    public function employeeIsBlocked();

    public function employeeHasLoggedOut();

    public function employeeLoggedIn(Employee $employee, $token);

    public function otpFailed($message);

    public function otpSuccessfullySent();
}