<?php

namespace Niyotail\Contracts\Authentication;

use Niyotail\Models\Agent;

interface AgentAuthContract
{
    public function agentLogInFailed($message);

    public function agentIsBlocked();

    public function agentHasLoggedOut();

    public function agentLoggedIn(Agent $agent, $token);

    public function otpFailed($message);

    public function otpSuccessfullySent();
}