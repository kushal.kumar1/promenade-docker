<?php

namespace Niyotail\Contracts\Authentication;

use Niyotail\Models\User;

interface UserAuthContract
{
    public function userLogInFailed($message);

    public function userIsBlocked();

    public function userHasLoggedOut();

    public function userLoggedIn(User $customer, $token);

    public function otpFailed($message);

    public function otpSuccessfullySent();
}