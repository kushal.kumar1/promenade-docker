<?php

namespace Niyotail\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Niyotail\Auth\OtpUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Niyotail\Model' => 'Niyotail\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('otp', function($app, array $config) {
            return new OtpUserProvider($app['hash'],$config['model']);
        });
        $this->registerPolicies();

        //
    }
}
