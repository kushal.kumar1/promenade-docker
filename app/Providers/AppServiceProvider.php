<?php

namespace Niyotail\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Niyotail\Models\Model;
use Illuminate\Support\Facades\DB;
use Niyotail\Models\Warehouse;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // To support indexes created by migrations for mysql < 5.7.7 & mariadb < 10.2.2
        // More details: https://laravel.com/docs/master/migrations#creating-indexes
        Schema::defaultStringLength(191);
        setlocale(LC_MONETARY, "en_IN");
        Relation::morphMap(Model::$map);

        View::composer('admin.header', function ($view){
            $userWarehouses = auth()->user()->warehouses;
            $view->with('userWarehouses', $userWarehouses);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
