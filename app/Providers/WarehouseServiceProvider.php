<?php

namespace Niyotail\Providers;

use Nayjest\Grids\ServiceProvider;
use Niyotail\Helpers\MultiWarehouseHelper;

class WarehouseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(MultiWarehouseHelper::class, function (){
            return new MultiWarehouseHelper();
        });
    }
}