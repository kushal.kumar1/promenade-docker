<?php

namespace Niyotail\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use libphonenumber\PhoneNumberUtil;

class ValidationServiceProvider extends ServiceProvider {

   /**
    * Bootstrap the application services.
    *
    * @return void
    */
   public function boot()
   {
       $this->registerAlphaSpaceRule();
       $this->registerPhoneRule();
       $this->registerAlphaNumSpaceRule();
       $this->registerPresentWithoutAllRule();
       $this->registerDateRangeLimit();
   }

   /**
    * Register the application services.
    *
    * @return void
    */
   public function register()
   {
       $this->registerPhoneUtil();
   }

    private function registerAlphaSpaceRule()
    {
        $message = "The :attribute may only contain letters and spaces.";
        Validator::extend('alpha_space', function ($attribute, $value) {
            return preg_match('/^[\pL\s.\']+$/u', $value);
        }, $message);
    }

    private function registerPhoneRule()
    {
        Validator::extend('phone', 'Niyotail\Helpers\Validations\MobileValidator@validateMobile');
    }

    private function registerPhoneUtil()
    {
        $this->app->singleton('libphonenumber', function ($app) {
            return PhoneNumberUtil::getInstance();
        });
        $this->app->alias('libphonenumber', 'libphonenumber\PhoneNumberUtil');
    }

    private function registerAlphaNumSpaceRule()
    {
        $message = "The :attribute may only contain letters and spaces.";
        Validator::extend('alpha_num_space', function ($attribute, $value) {
            return preg_match('/^[a-zA-Z0-9 ]+$/', $value);
        }, $message);
    }

    private function registerPresentWithoutAllRule()
    {
        $message = "The :attribute should be present when none of dependentFields are present.";
        Validator::extendImplicit('present_without_all', function ($attribute, $value, $parameters,$validator)use(&$message) {
            foreach ($parameters as $key){
                if(!empty(Input::get($key)))
                    return true;
            }
            if($validator->validatePresent($attribute, $value))
                return true;
            return false;
        }, $message);
        Validator::replacer('present_without_all', function ($message, $attribute, $rule, $parameters) {
            return str_replace('dependentFields',implode('/',$parameters),$message);
        });
    }

    private function registerDateRangeLimit()
    {
        $message = "The maximum date range is :range days";
        Validator::extend('date_range_limit', function ($attribute, $value, $parameters)use(&$message) {
            $fromDate=Input::get($parameters[0]);
            if(empty($parameters[1]))
                throw new \InvalidArgumentException('requires at two parameters');
            $days=$parameters[1];
            $difference=Carbon::parse($value)->diffInDays(Carbon::parse($fromDate));
            return !!($difference <= $days);
        }, $message);
        Validator::replacer('date_range_limit', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':range',$parameters[1],$message);
        });
    }


}
