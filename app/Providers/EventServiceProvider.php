<?php

namespace Niyotail\Providers;

use Niyotail\Events\Cart\ItemAddedToCart;
use Niyotail\Events\Cart\ItemQuantityUpdatedInCart;
use Niyotail\Events\Cart\ItemRemovedFromCart;
use Niyotail\Events\Cart\RecalculateCart;
use Niyotail\Events\Invoice\InvoiceCancelled;
use Niyotail\Events\Invoice\InvoiceCreated;
use Niyotail\Events\Order\OrderCancelled;
use Niyotail\Events\Order\OrderPlaced;
use Niyotail\Events\Picklist\PicklistClosed;
use Niyotail\Events\Picklist\PicklistCreated;
use Niyotail\Events\Putaway\PutawayItemPlaced;
use Niyotail\Events\Putaway\PutawayItemAdded;
use Niyotail\Events\Shipment\ShipmentCancelled;
use Niyotail\Events\Shipment\ShipmentDelivered;
use Niyotail\Events\Shipment\ShipmentDispatched;
use Niyotail\Events\Shipment\ShipmentUpdated;
use Niyotail\Events\Shipment\ShipmentGenerated;
use Niyotail\Events\Shipment\ShipmentMarkedRTO;
use Niyotail\Events\ReturnOrderCompleted;
use Niyotail\Events\Cart\AddressUpdatedInCart;
use Niyotail\Events\CreditNote\CreditNoteGenerated;
use Niyotail\Events\StoreOrder\StoreOrderCreated;
use Niyotail\Events\Inventory\MallProductsInventorySync;
use Niyotail\Listeners\Cart\AfterItemAddedToCart;
use Niyotail\Listeners\Cart\AfterItemQuantityUpdate;
use Niyotail\Listeners\Cart\AfterItemRemoved;
use Niyotail\Listeners\Cart\AfterAddressUpdatedInCart;
use Niyotail\Listeners\Cart\CartToBeRecalculated;
use Niyotail\Listeners\Invoice\AfterInvoiceCancelled;
use Niyotail\Listeners\Invoice\AfterInvoiceCreated;
use Niyotail\Listeners\Order\AfterOrderCancelled;
use Niyotail\Listeners\Order\AfterOrderPlaced;
use Niyotail\Listeners\Picklist\AfterPicklistClosed;
use Niyotail\Listeners\Picklist\AfterPicklistCreated;
use Niyotail\Listeners\Putaway\AfterPutawayItemPlaced;
use Niyotail\Listeners\Putaway\AfterPutawayItemAdded;
use Niyotail\Listeners\Shipment\AfterShipmentCancelled;
use Niyotail\Listeners\Shipment\AfterShipmentDelivered;
use Niyotail\Listeners\Shipment\AfterShipmentDispatched;
use Niyotail\Listeners\Shipment\AfterShipmentUpdated;
use Niyotail\Listeners\Shipment\AfterShipmentGenerated;
use Niyotail\Listeners\Shipment\AfterShipmentMarkedRTO;
use Niyotail\Listeners\AfterCreditNoteGenerated;
use Niyotail\Listeners\AfterReturnOrderCompleted;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Niyotail\Listeners\StoreOrder\AfterStoreOrderCreated;
use Niyotail\Listeners\Inventory\SyncMallProductsInventoryToNiyoos;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ItemAddedToCart::class => [
            AfterItemAddedToCart::class
        ],
        RecalculateCart::class => [
            CartToBeRecalculated::class
        ],
        ItemQuantityUpdatedInCart::class => [
            AfterItemQuantityUpdate::class
        ],
        ItemRemovedFromCart::class => [
            AfterItemRemoved::class
        ],
        AddressUpdatedInCart::class => [
            AfterAddressUpdatedInCart::class
        ],

        //Order
        OrderCancelled::class => [
            AfterOrderCancelled::class
        ],
        OrderPlaced::class => [
          AfterOrderPlaced::class,
        ],

        //Shipment
        ShipmentGenerated::class => [
            AfterShipmentGenerated::class
        ],
        ShipmentCancelled::class => [
            AfterShipmentCancelled::class
        ],
        ShipmentUpdated::class => [
            AfterShipmentUpdated::class
        ],
        ShipmentDelivered::class => [
            AfterShipmentDelivered::class
        ],
        ShipmentMarkedRTO::class => [
            AfterShipmentMarkedRTO::class
        ],

        ShipmentDispatched::class => [
            AfterShipmentDispatched::class
        ],

        //Invoices & Credit Notes
        InvoiceCreated::class => [
            AfterInvoiceCreated::class
        ],
        InvoiceCancelled::class => [
            AfterInvoiceCancelled::class
        ],
        CreditNoteGenerated::class => [
          AfterCreditNoteGenerated::class
        ],

        //Return Order
        ReturnOrderCompleted::class => [
          AfterReturnOrderCompleted::class
        ],

        //Picklist
        PicklistCreated::class => [
            AfterPicklistCreated::class
        ],
        PicklistClosed::class => [
            AfterPicklistClosed::class
        ],

//        Store Order
        StoreOrderCreated::class => [
            AfterStoreOrderCreated::class
        ],

        // Putaway from migration
        PutawayItemPlaced::class  => [
            AfterPutawayItemPlaced::class,
        ],
        PutawayItemAdded::class  => [
            AfterPutawayItemAdded::class,
        ],

        //Mall Salability
        MallProductsInventorySync::class  => [
            SyncMallProductsInventoryToNiyoos::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
