<?php

namespace Niyotail\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Niyotail\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapAppApiRoutes();
        $this->mapAgentApiRoutes();
        $this->mapBrandApiRoutes();
        $this->mapAdminRoutes();
        $this->mapWebRoutes();
        $this->mapClientRoutes();

    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace.'\Admin')
            ->domain(config('app.admin_url'))
            ->as("admin::")
            ->group(base_path('routes/admin.php'));
    }

    protected function mapAppApiRoutes()
    {
        Route::middleware('api')
            ->namespace($this->namespace.'\Api\App')
            ->prefix('app')
            ->domain(config('app.api_url'))
            ->as("app.api::")
            ->group(base_path('routes/app.php'));
    }

    protected function mapAgentApiRoutes()
    {
        Route::middleware('api')
            ->namespace($this->namespace.'\Api\Agent')
            ->prefix('agent')
            ->domain(config('app.api_url'))
            ->as("agent.api::")
            ->group(base_path('routes/agentApi.php'));
    }

    protected function mapBrandApiRoutes()
    {
        Route::namespace($this->namespace.'\Api\Brand')
            ->prefix('brand')
            ->domain(config('app.api_url'))
            ->as("brand.api::")
            ->group(base_path('routes/brandApi.php'));
    }

    protected function mapWebRoutes()
    {
      Route::middleware('web')
          ->namespace($this->namespace.'\Web')
          ->as("web::")
          ->group(base_path('routes/web.php'));
    }

    protected function mapClientRoutes()
    {
        Route::middleware('api')
             ->namespace($this->namespace.'\Api\Client')
             ->prefix('client')
             ->domain(config('app.api_url'))
             ->as("client.api::")
             ->group(base_path('routes/clientApi.php'));
    }
}
