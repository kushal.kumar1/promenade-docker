<?php

namespace Niyotail\Providers;

use Niyotail\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AclServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Gate::before(function ($user) {
            if ($user->hasRole('Admin') || $user->hasRole('admin')) {
                return true;
            }
        });
        if (Schema::hasTable(with(new Permission())->getTable())) {
            foreach (Permission::with('roles')->get() as $permission) {
                Gate::define($permission->name, function ($user) use ($permission) {
                    return $user->hasPermission($permission);
                });
            }
        }
    }
}
