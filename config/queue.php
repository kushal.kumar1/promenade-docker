<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | Laravel's queue API supports an assortment of back-ends via a single
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
    | Supported: "sync", "database", "beanstalkd", "sqs", "redis", "null"
    |
    */

    'default' => env('QUEUE_DRIVER', 'sync'),
    'pos_to_niyotail' => env('POS_SQS_QUEUE'),
    'niyotail_to_pos' => env('NIYOTAIL_SQS_QUEUE'),

    'integration' => [
        'product-sync' => env('SQS_PRODUCT_SYNC', ''),
        'inventory-sync' => env('SQS_INVENTORY_SYNC', ''),
        'mall-inventory-sync' => env('SQS_MALL_INVENTORY_SYNC', ''),
        'price-sync' => env('SQS_PRICE_SYNC', ''),
        'mall-sync' => env('SQS_MALL_SYNC', ''),
        'niyoos-wms-bridge' => env('SQS_NIYOOS_WMS_BRIDGE', ''),
    ],

    'picklist' => env('PICKLIST_QUEUE'),
    'convertToSaleOrder' => env('CONVERT_TO_SALE_ORDER'),
    'stockTransferQueue' => env('STOCK_TRANSFER_QUEUE'),
    'wmsQueue' => env('WMS_QUEUE'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
            'retry_after' => 90,
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => 'localhost',
            'queue' => 'default',
            'retry_after' => 90,
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key'    => env('NT_SQS_KEY', ''),
            'secret' => env('NT_SQS_SECRET', ''),
            'prefix' => env('NT_SQS_PREFIX',''),
            'queue'  => env('NT_SQS_QUEUE',''),
            'region' => env('NT_SQS_REGION',''),
            'retry_after' => 60,
        ],

        'sqs-plain' => [
            'driver' => 'sqs-plain',
            'key'    => env('SQS_KEY', ''),
            'secret' => env('SQS_SECRET', ''),
            'prefix' => env('SQS_PREFIX',''),
            'queue'  => env('POS_SQS_QUEUE',''),
            'region' => env('SQS_REGION',''),
        ],

        'integration' => [
            'driver' => 'sqs-plain',
            'key'    => env('NT_SQS_KEY', ''),
            'secret' => env('NT_SQS_SECRET', ''),
            'prefix' => env('NT_SQS_PREFIX',''),
            'queue'  => env('NT_SQS_QUEUE',''),
            'region' => env('NT_SQS_REGION',''),
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
            'queue' => 'default',
            'retry_after' => 90,
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
        'driver' => 'database-uuids',
    ],

];
