<?php
return [


    /*
    |--------------------------------------------------------------------------
    | Default Payment Method
    |--------------------------------------------------------------------------
    |
    */

    'default' => env('DEFAULT_PAYMENT_METHOD', 'cod'),

    /*
    |--------------------------------------------------------------------------
    | Default Online Payment Gateway
    |--------------------------------------------------------------------------
    |
    */

    'online_payment_default' => env('DEFAULT_PAYMENT_GATEWAY', 'payu'),

    /*
    |--------------------------------------------------------------------------
    | Payment Drivers
    |--------------------------------------------------------------------------
    |
    */

    'online_methods' => [
        'cash'=>[
            'label' => 'Pay in Cash',
            'description' => ''
        ],
        // 'au_bank' => [
        //     'label' => 'Credit - AU Small Finance Bank',
        //     'description' => "Disclaimer: You agree to terms & conditions associated with AU Bank's Credit Limit. You are liable to pay the required amount to AU Bank."
        // ]
    ],

    'payu' => [
        'merchant_key' => env('PAYU_MERCHANT_KEY', 'gtKFFx'),
        'salt' => env('PAYU_SALT', 'eCwWELxi'),
        'url' => env('PAYU_URL', 'https://test.payu.in'),
        'info_url'=>env('PAYU_INFO_URL', 'https://test.payu.in')
    ],
    'paypal'=>[
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'secret' => env('PAYPAL_SECRET'),
        'settings' => [
            'mode' => env('PAYPAL_MODE', 'sandbox'),
            'http.ConnectionTimeOut' => 30
        ],
        'web_hook_id'=>env('PAYPAL_WEB_HOOK_ID'),
        'currency'=>env('PAYPAL_CURRENCY', 'INR')
    ],
    'cash'=>[],
    'cod'=>[],
    
    'bank_names' => [
        'Allahabad Bank',
        'Andhra Bank',
        'AU Small Finance Bank',
        'Axis Bank',
        'Bandhan Bank',
        'Bank of Bahrain and Kuwait',
        'Bank of Baroda',
        'Bank of India',
        'Bank of Maharashtra',
        'Canara Bank',
        'Central Bank of India',
        'City Union Bank',
        'Corporation Bank',
        'Deutsche Bank',
        'Development Credit Bank',
        'Dhanlaxmi Bank',
        'Federal Bank',
        'HDFC Bank',
        'HSBC Bank',
        'ICICI Bank',
        'IDBI Bank',
        'Indian Bank',
        'Indian Overseas Bank',
        'IndusInd Bank',
        'ING Vysya Bank',
        'Jammu and Kashmir Bank',
        'Karnataka Bank Ltd',
        'Karur Vysya Bank',
        'Kotak Bank',
        'Laxmi Vilas Bank',
        'Oriental Bank of Commerce',
        'Punjab National Bank',
        'Punjab & Sind Bank',
        'Sarva Haryana Gramina Bank',
        'Shamrao Vitthal Co-operative Bank',
        'South Indian Bank',
        'State Bank of Bikaner & Jaipur',
        'State Bank of Hyderabad',
        'State Bank of India',
        'State Bank of Mysore',
        'State Bank of Patiala',
        'State Bank of Travancore',
        'Syndicate Bank',
        'Tamilnad Mercantile Bank Ltd.',
        'UCO Bank',
        'Ujjivan Small Finance Bank',
        'Union Bank of India',
        'United Bank of India',
        'Utkarsh Small Finance Bank',
        'Vijaya Bank',
        'Yes Bank Ltd',
        'The Gurgaon Central Cooperative Bank',
        'Baroda Rajasthan Kshetriya Gramin Bank',
        'Sarv UP Gramin Bank',
	    'IDFC First Bank',
        'Equitas Small Finance Bank',
        'The Panipat Urban Co-operative Bank',
        'GPO-General Post Office'
    ],
];
