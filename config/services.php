<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('AWS_KEY'),
        'secret' => env('AWS_SECRET'),
        'region' => env('SES_REGION'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect'=>env('FACEBOOK_REDIRECT_URL')
    ],
    'google' => [
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect'=>env('GOOGLE_REDIRECT_URL')

    ],
    'bluedart'=>[
        'id' => env('BLUEDART_ID'),
        'key' => env('BLUEDART_KEY'),
    ],

    'niyoos'=>[
        'api_base' => env('NIYOOS_URL'),
        'secret' => env('NIYOOS_SECRET'),
        'apis' => [
            'increaseInventory' => '/client/v1/increase-inventory',
            'decreaseInventory' => '/client/v1/decrease-inventory'
        ]
    ]

];
