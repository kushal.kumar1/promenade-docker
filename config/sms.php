<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Sms Service
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party Sms services such
    | as Twilio, Sns, Gupshup and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
    'default' => 'karix',


    'sns' => [
        'key' => env('AWS_KEY'),
        'secret' => env('AWS_SECRET'),
        'region' => env('SNS_REGION'),
        'version' => env('SNS_VERSION', 'latest')
    ],


    'gupshup' => [
        'url' => env('GUPSHUP_URL', 'http://enterprise.smsgupshup.com/GatewayAPI/rest'),
        'user' => env('GUPSHUP_USER'),
        'password' => env('GUPSHUP_PASSWORD')
    ],


    'otp_key' => [
        'local' => 'Ek0AZpbgYay',
        'production' => 'H5Qz0BEMnHP'
    ],

    'karix' => [
        'url' => env('KARIX_URL', 'https://japi.instaalerts.zone/httpapi/QueryStringReceiver'),
        'key' => [
            'default' => env('KARIX_KEY_DEFAULT'),
            'otp' => env('KARIX_KEY_OTP'),
        ]
    ],
];
