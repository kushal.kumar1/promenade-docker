<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'upload' => [
            'driver' => 'local',
            'root' => storage_path('app/upload'),
        ],

        'invoice' => [
            'driver' => 'local',
            'root' => storage_path('app/invoices'),
        ],

        'grn' => [
            'driver' => 'local',
            'root' => storage_path('app/grn'),
        ],

        'importer' => [
            'driver' => 'local',
            'root' => storage_path('app/public/importer-output'),
        ],

        'po' => [
            'driver' => 'local',
            'root' => storage_path('app/purchase_order'),
        ],

        'manifest' => [
            'driver' => 'local',
            'root' => storage_path('app/manifest'),
        ],

        'picklist' => [
            'driver' => 'local',
            'root' => storage_path('app/picklist'),
        ],

        'credit' => [
            'driver' => 'local',
            'root' => storage_path('app/credit'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('S3_AWS_KEY'),
            'secret' => env('S3_AWS_SECRET'),
            'region' => env('S3_AWS_REGION'),
            'bucket' => env('S3_AWS_BUCKET'),
        ],

    ],

    'invoice_file_driver' => env('INVOICE_FILE_DRIVER', 'local'),
    'credit_note_file_driver' => env('CREDIT_NOTE_FILE_DRIVER','local')
];
