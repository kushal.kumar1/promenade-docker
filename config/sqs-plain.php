<?php

/**
 * List of plain SQS queues and their corresponding handling classes
 */
return [
    'handlers' => [
        config('queue.integration.mall-sync') => \Niyotail\Helpers\Integration\MallSync::class,
        config('queue.integration.niyoos-wms-bridge') => \Niyotail\Helpers\Integration\AddLedgerEntry::class,
    ],
    'default-handler' => \Niyotail\Jobs\HandlerJob::class
];
