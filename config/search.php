<?php

return [
    'SOLR_HOST' => env('SOLR_HOST', '127.0.0.1'),
    'SOLR_PORT' => env('SOLR_PORT', '8983'),
    'SOLR_PATH' => env('SOLR_PATH', ''),
    'SOLR_CORE' => env('SOLR_CORE', 'new_core'),
    'SOLR_USERNAME' => env('SOLR_USERNAME'),
    'SOLR_PASSWORD' => env('SOLR_PASSWORD'),
    'SOLR_REQUEST_TIMEOUT' => env('SOLR_REQUEST_TIMEOUT')
];
