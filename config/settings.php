<?php

return
    [
        /*
         * Cancellation reason with inventory update flag
         */
        'cancellation_reasons' => [
            'Cancelled due to high outstanding/non-payment of dues',
            'Customer changed mind',
            'Duplicate order',
            'Forgot to dispatch',
            'Inventory not available',
            'Test order',
        ],

        // key indicates the reason and value indicates whether qty needs to be restored
        'order_item_cancellation_reasons' => [
            'Product physically not available' => false,
            'Missed by mistake' => true,
            'Rate not approved' => true,
            'Expired' => false,
            'Product damage' => false,
            'MRP change' => true,
            'Pack size change' => true,
            'Product can not be dispatched in unit' => true,
            'Item not ordered' => true,
            'Product no longer required by customer' => true,
            'Wrong product punched' => true,
            'Gift/Scheme item not available' => true,
            'Need to hold' => true,
        ],

        'return_reasons' => [
            'Cash not available',
            'Credit requested',
            'Damaged product',
            'Delivery time not accepted',
            'Did not place order',
            'Duplicate order',
            'Error in invoice',
            'Expired product(s)',
            'Free item not sent',
            'Invoice not sent',
            'Item added by salesman',
            'Item(s) no longer needed',
            'Near expiry product(s)',
            'Old stock return',
            'Ordered by someone else',
            'Order placed by mistake',
            'Order sent twice/more',
            'Owner not available',
            'Product and quantity mismatch',
            'Product mismatch (Additional product sent)',
            'Product procured from elsewhere',
            'Quantity mismatch (Short)',
            'Rate is too High',
            'Shop Closed / Number not reachable',
            'Wrong product ordered',
            'Previous credit not cleared and not paying current bill too.',
            'Excess Stock.',
            'Bad Quality.'
        ],

        'store_disable_reasons' => [
            'Cheque dishonour',
            'Cheque Bounced 3rd time. No further cheques will be entertained',
            'Cheque Bounce & Overdue',
            'Deductng CD on current - Not paying old outstanding',
            'Dispute (Denying Balance)',
            'Dispute (Misbehaving)',
            'Neither giving old payment nor current billing payment',
            'OS above 30 days',
            'Overdue',
            'Overlimit',
            'Payment commitment not fulfilled',
            'Unverified Store',
        ],

        'store_inactive_reasons' => [
            'Duplicate',
            'Permanently closed',
            'Temporarily closed',
        ],

        'inventory_inactive_reasons' => [
            "Yet to be Delivered",
            "Market Rate Too Low",
            "Damaged / Defective",
            "Lost",
            "Near Expiry / Expired",
            "Sent for packaging"
        ],

        'latest_version' => env('LATEST_APP_VERSION'),
        'force_update_app_version' => env('FORCE_UPDATE_APP_VERSION'),
        'niyoos_products_sync' => [
        ],
        'wh2_enable_tags' => [8, 9],

        'non_returnable_tag_id' => env('NON_RETURNABLE_TAG_ID'),

        'inventory_migration_from_remarks' => [
            "Product quantity not available for migration",
            "Insufficient space in migration cart",
        ],

        'pos_transaction_filters' => [
            ['key' => 'rcn_promotion', 'name' => 'RCN - Promotion'],
            ['key' => 'rcn_discount', 'name'=> 'RCN - Discount'],
            ['key' => 'rcn_commission', 'name' => 'RCN - Commission'],
            ['key' => 'return', 'name' => 'Return'],
            ['key' => 'purchase', 'name' => 'Purchase'],
            ['key' => '1k_rent_reimbursement', 'name' => 'Rent Reimbursement'],
            ['key' => 'store_internet_reimbursement', 'name' => 'Store Internet Reimbursement'],
            ['key' => 'customer_loyalty_reimbursement', 'name' => 'Customer Loyalty Reimbursement'],
            ['key' => 'upi', 'name' => 'UPI'],
            ['key' => 'cash', 'name' => 'Cash'],
            ['key' => 'cheque', 'name' => 'Cheque'],
            ['key' => 'net_banking', 'name' => 'Net Banking'],
            ['key' => 'others', 'name' => 'Others']
        ],
    ];
