<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('V1')->group(function () {
    Route::post('/send-otp', 'AuthController@sendOtp');
    Route::post('/login', 'AuthController@login');

    Route::group(['middleware' => ['assign.guard:employee_api', 'auth.jwt', 'api.warehouse.check']], function () {

        Route::get('/search/storages', 'SearchController@storage')->name('search.storages');
        Route::get('search/reasons', 'SearchController@reasons')->name('search.reasons');

        //Inventory Migration
        Route::get('/migration', 'InventoryMigrationController@index');
        Route::get('/migration/{id}', 'InventoryMigrationController@detail');
        Route::get('/migration/{id}/search/products', 'InventoryMigrationController@searchProduct');
        Route::post('/putaway/carts/add-item', 'InventoryMigrationController@addItemToCart');
        Route::post('/putaway/{id}/place-item', 'InventoryMigrationController@placeItem');


        Route::group(['prefix' => 'putaway'], function () {
            Route::get('/', 'PutawayController@index');
            Route::get('/{id}', 'PutawayController@detail');
            Route::post('/init', 'PutawayController@init');
            Route::post('/{id}/start', 'PutawayController@start');
            Route::post('/{id}/close', 'PutawayController@close');
            Route::post('/item', 'PutawayController@putItem');
            Route::post('/set-storage', 'PutawayController@setItemStorage');
            Route::post('/remove-item/{id}', 'PutawayController@removeItem');
            Route::get('/search/products', 'PutawayController@searchProduct');
            Route::post('/{id}/update-cart', 'InventoryMigrationController@updateCartItem');
            Route::post('/{id}/place-item', 'InventoryMigrationController@placeItem');
        });

        Route::prefix('audits')->group( function () {
            Route::get('/search-products', 'AuditController@searchProducts')->name('audits.search-products');
            Route::get('/get-product', 'AuditController@getProduct')->name('audits.get-product');

            Route::get('/', 'AuditController@listAudits')->name('audits.list');
            Route::get('/{audit}', 'AuditController@getAudit')->name('audits.get');
            Route::post('/{audit}/extra-inventory', 'AuditController@extraInventory')->name('audits.group-items');

            Route::get('/{audit}/items', 'AuditController@listAuditItems')->name('audit-items.list');
            Route::get('/{audit}/items/{auditItem}', 'AuditController@getAuditItem')->name('audit-items.get');

            Route::post('/{audit}/items/{auditItem}/input', 'AuditController@input')->name('audit-items.input');
            Route::post('/{audit}/items/{auditItem}/submit', 'AuditController@submit')->name('audit-items.submit');
        });

        Route::prefix('picklists')->group(function () {
            Route::get('/', 'PicklistController@index');
            Route::post('/{id}/mark-picked', 'PicklistController@markPicked');
            Route::get('/{id}/items', 'PicklistController@items');
            Route::get('/{id}/search-product', 'PicklistController@search');
            Route::post('/{id}/pick-item', 'PicklistController@pickItem');
            Route::post('/{id}/unpick-item', 'PicklistController@removeItem');
            Route::get('/skip_reasons', 'PicklistController@skipReasons');
        });

    });
});
