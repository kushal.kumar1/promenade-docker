<?php

Route::get('/media/cache/{path}', 'ImageCacheController@index', function ($path) {
    return $path;
})->where('path', '.*');
