<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('V1')->group(function () {
    Route::post('/send-otp', 'AuthController@sendOtp');
    Route::post('/login', 'AuthController@login');
    Route::get('/agent-home', 'StaticController@dashboard');

    Route::group(['middleware' => ['assign.guard:agent_api', 'auth.jwt']], function () {
        Route::get('/home', 'StaticController@home');
        Route::get('/products', 'ProductController@index');
        Route::get('/products/{id}', 'ProductController@show');
        Route::get('/brands', 'StaticController@brands');
        Route::get('/categories', 'StaticController@categories');

        Route::get('/carts', 'CartController@getCart');
        Route::post('/carts/items', 'CartController@addToCart');
        Route::post('/products/carts/items', 'CartController@productVariantCartUpdate');
        Route::put('/carts/items/{id}', 'CartController@updateItem');
        Route::delete('/carts/items/{id}', 'CartController@removeItem');
        Route::post('/carts/addresses', 'CartController@updateAddress');
        Route::get('/carts/payment-methods', 'CartController@getPaymentMethods');
        Route::post('/orders', 'OrderController@store');
        Route::put('/orders/{id}', 'OrderController@update');

        Route::get('/stores', 'StoreController@index');
        Route::get('/stores/{id}', 'StoreController@show');
        Route::post('/stores', 'StoreController@store');
        Route::put('/stores/{id}', 'StoreController@update');
        Route::get('/stores/{id}/orders', 'StoreController@orders');

        Route::get('/search/products', 'SearchController@products');
        Route::get('/search/beats', 'SearchController@beats');
        Route::get('/search/stores', 'SearchController@stores');

        Route::get('/stores/{id}/questions', 'StoreProfilingController@index');
        Route::get('/stores/{id}/questions/{question_id}', 'StoreProfilingController@show');
        Route::post('/stores/{id}/store-profile', 'StoreProfilingController@store');
        Route::put('/stores/{id}/store-profile/{question_id}', 'StoreProfilingController@update');
        Route::get('/dashboard', 'AgentController@dashboard');

        Route::post('/users/settings', 'AgentController@storeDeviceParameters');

        //Collection
        Route::get('/stores/{storeId}/invoices/{invoiceId}', 'InvoiceController@getPdfUrl');
        Route::get('/stores/{id}/transactions', 'TransactionController@getPendingTransactions');
        Route::post('/transactions/store', 'TransactionController@storeTransaction');
        Route::get('/transactions/ledger/{storeId}', ['as' => 'transactions.ledger', 'uses' => 'TransactionController@getLedgerForStore']);
        Route::get('/transactions/settings', 'TransactionController@settings');
        Route::get('invoices/{id}', 'InvoiceController@getPdf');

        //Delivery
        Route::get('/shipments', 'DeliveryController@getShipments');
        Route::get('/shipments/pendingCount', 'DeliveryController@getPendingDeliveriesCount');
        Route::get('/invoices', 'DeliveryController@getInvoice');
        Route::post('/shipments/{id}/markDelivered', 'DeliveryController@deliver');
        Route::post('/shipments/{id}/fileReturn', 'DeliveryController@return');
        Route::post('/shipments/{id}/markRto', 'DeliveryController@rto');
        Route::post('/shipments/{id}/markRetry', 'DeliveryController@retry');
        Route::post('/shipments/{id}/send-otp', 'DeliveryController@sendOTP');
        Route::post('/shipments/{id}/upload-pod', 'DeliveryController@uploadPod');

        //Stats
        Route::get('/stats/store', 'StaticController@storeStats');
        Route::get('/stats/{fromDate}/{toDate?}', 'StaticController@stats');
    });

    //Route::get('/dashboard', 'AgentController@dashboard');
});
