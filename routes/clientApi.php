<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('V1')->group(function () {
    // Route::get('/dashboard/stats', 'StatsController@dashboard');

    Route::group(['middleware' => 'api.client.auth:niyoos'], function () {
        Route::get('/sync/products', 'ClientController@syncProducts');
        Route::post('/cart', 'ClientController@addToCart');
        Route::post('/order', 'ClientController@createOrder');
        Route::post('/brand', 'ClientController@createBrand');
        Route::post('/category', 'ClientController@createCategory');
        Route::post('/marketer', 'ClientController@createMarketer');
        Route::get('/shipments/{storeId}', 'ShipmentController@purchase')->name('purchase');
        Route::get('/returns/{storeId}', 'ReturnsController@returns')->name('returns');
        Route::get('/all-returns/{storeId}', 'ReturnsController@allReturns')->name('allReturns');
        Route::get('/invoice/{invoiceId}', 'ShipmentController@downloadInvoice')->name('download.invoice');

        Route::prefix('transactions')->group(function (){
            Route::get('/{storeId}/ledger', 'TransactionController@ledger');
        });

        Route::get('/orders', 'OrderController@orders');
    });
});
