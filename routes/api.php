<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('V1')->group(function () {
    Route::post('/send-otp', 'AuthController@sendOtp');
    Route::post('/login', 'AuthController@login');
    Route::get('settings', 'SettingController@index');

    Route::group(['middleware' => 'api.auth:api'], function () {
        //Route::group(['middleware' => 'user.onBoarding:api'], function () {
        Route::group(['middleware' => ['store.check:api', 'club1k.check:api']], function () {
            Route::get('/home', 'StaticController@home');
            Route::get('/about-us', 'StaticController@aboutUs');
            Route::get('/brands', 'StaticController@brands');
            Route::get('/products', 'ProductController@index')->name('get.products');
            Route::get('/products/{id}', 'ProductController@show');
            Route::get('/categories', 'StaticController@categories');
            Route::get('search', 'SearchController@search');

            Route::get('/carts', 'CartController@getCart');
            Route::post('/carts/items', 'CartController@addToCart');
            Route::put('/carts/items/{id}', 'CartController@updateItem');
            Route::delete('/carts/items/{id}', 'CartController@removeItem');
            Route::post('/carts/addresses', 'CartController@updateAddress');
            Route::get('/carts/payment-methods', 'CartController@getPaymentMethods');
            Route::post('/products/carts/items', 'CartController@productVariantCartUpdate');

            Route::post('/orders', 'OrderController@store');
            Route::put('/orders/{id}', 'OrderController@update');
            Route::post('/orders/{id}/rating', 'OrderController@saveRating');
            Route::post('/stores/{id}/credit-request', 'StoreController@requestCredit');
        });
        Route::post('/stores', 'StoreController@store');
        Route::put('/stores/{id}', 'StoreController@update');
        Route::get('/stores/{id}/orders', 'StoreController@orders');
        Route::get('/stores/{id}/transactions', 'StoreController@transactions');
        Route::get('/stores/{id}/invoices/{invoiceID}', 'StoreController@getInvoicePdf');

        Route::get('/users/{id}', 'UserController@profile');
        Route::put('/users/{id}', 'UserController@update');
        Route::post('/users/{id}/feedback', 'UserController@feedback');
        Route::get('/agents', 'AgentController@index');
        //});
        // Route::get('/pincodes', 'PincodeController@show');
        //Todo: to be removed
        Route::post('/users/{id}/on-boarding', 'UserController@onBoarding');
        Route::post('/users/settings', 'UserController@storeDeviceParameters');
        Route::get('/dashboard', 'StaticController@dashboard');
    });
});
