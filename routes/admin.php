<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'chopper.guest:admin'], function () {
    Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@index']);
    Route::post('/login', ['as' => 'authenticate', 'uses' => 'AuthController@login']);
});

Route::group(['middleware' => 'chopper.admin.auth.basic'], function () {
    Route::get('/locator', ['as' => 'locator', 'uses' => 'LocatorController@index']);
    Route::get('/locator/stores', ['as' => 'locator.stores', 'uses' => 'LocatorController@stores']);
});


Route::group(['middleware' => 'chopper.admin.auth'], function () {

    Route::get('/choose-warehouse', 'WarehouseController@chooseWarehouse')->name('choose-warehouse');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::post('/select-warehouse', 'WarehouseController@selectWarehouse')->name('select-warehouse');

    Route::group(['middleware' => 'warehouse.check'], function () {

        Route::get('/special-case/add-all-cart-items/{cartId}', ['uses' => 'SpecialCaseController@addAllCartItems']);
        Route::get('/special-case/create-order-from-cart/{cartId}', ['uses' => 'SpecialCaseController@createOrderFromCart']);
        Route::get('/special-case/create-shipment/{orderId}', ['uses' => 'SpecialCaseController@createShipment']);


        Route::get('/', ['as' => 'dashboard', 'uses' => 'Dashboard\WelcomeController@index']);
        Route::get('/dashboard/business', ['as' => 'dashboard', 'uses' => 'Dashboard\BusinessController@index']);
        Route::get('/dashboard/ops', ['as' => 'dashboard.ops', 'uses' => 'Dashboard\OperationsController@index']);
        Route::get('/dashboard/brands', ['as' => 'dashboard.brands', 'uses' => 'Dashboard\BrandsController@index']);
        Route::get('/dashboard/brands/grid', ['as' => 'dashboard.brands.grid', 'uses' => 'Dashboard\BrandsController@grid']);
        Route::get('/dashboard/brands/chart', ['as' => 'dashboard.brands.chart', 'uses' => 'Dashboard\BrandsController@chart']);
        Route::get('/dashboard/brands/heatmap', ['as' => 'dashboard.brands.heatmap', 'uses' => 'Dashboard\BrandsController@heatmap']);
        Route::get('/dashboard/brands/stats', ['as' => 'dashboard.brands.stats', 'uses' => 'Dashboard\BrandsController@getBrandsStatistics']);
        Route::get('/dashboard/brands/metrics', ['as' => 'dashboard.brands.metrics', 'uses' => 'Dashboard\BrandsController@getBrandMetrics']);
        Route::get('/dashboard/brands/chart/stats', ['as' => 'dashboard.brands.chart.stats', 'uses' => 'Dashboard\BrandsController@getBrandsChartStats']);
        Route::get('/dashboard/filters', ['as' => 'dashboard.filters', 'uses' => 'Dashboard\FiltersController@getFilters']);
        Route::get('/dashboard/chart', ['as' => 'dashboard.chart', 'uses' => 'Dashboard\ChartController@getChart']);
        Route::post('/dashboard/update', ['as' => 'dashboard.data.update', 'uses' => 'Dashboard\BusinessController@updateData']);

        Route::get('/vendors', ['as' => 'vendors.index', 'uses' => 'VendorController@index']);
        Route::get('/vendors/create', ['as' => 'vendors.add', 'uses' => 'VendorController@add']);
        Route::post('/vendors/create', ['as' => 'vendors.create', 'uses' => 'VendorController@create']);
        Route::get('/vendors/bank-accounts', ['as' => 'vendors.bank_accounts.index', 'uses' => 'VendorController@bankAccounts']);
        Route::get('/vendors/addresses', ['as' => 'vendors.addresses.index', 'uses' => 'VendorController@addresses']);

        /* Vendor marketer mapping */
        Route::get('/vendors/{id}/marketers', ['as' => 'vendors.marketers', 'uses' => 'VendorMarketerMapController@index']);
        Route::get('/vendors/{id}/marketers/add', ['as' => 'vendors.marketers.add', 'uses' => 'VendorMarketerMapController@addMarketer']);
        Route::get('/vendors/{id}/marketers/{marketer_id}/edit', ['as' => 'vendors.marketers.edit', 'uses' => 'VendorMarketerMapController@editMarketer']);
        Route::post('/vendors/{id}/marketers/save', ['as' => 'vendors.marketers.save', 'uses' => 'VendorMarketerMapController@save']);
        Route::post('/vendors/{id}/marketers/{marketer_id}/update', ['as' => 'vendors.marketers.update', 'uses' => 'VendorMarketerMapController@save']);
        Route::get('/download/tot/{id}', ['as' => 'vendors.marketers.download_tot', 'uses' => 'VendorMarketerMapController@downloadTotFile']);


        Route::get('/vendors/{id}/bank-account', ['as' => 'vendors.bank_account.edit', 'uses' => 'VendorController@editBankAccount']);
        Route::post('/vendors/{id}/bank-account/create', ['as' => 'vendors.bank_account.create', 'uses' => 'VendorController@createBankAccount']);
        Route::post('/vendors/{id}/bank-account/{accountId}/update', ['as' => 'vendors.bank_account.update', 'uses' => 'VendorController@updateBankAccount']);
        Route::post('/vendors/{id}/bank-account/{accountId}/delete', ['as' => 'vendors.bank_account.delete', 'uses' => 'VendorController@deleteBankAccount']);
        Route::get('/vendors/{id}/address', ['as' => 'vendors.address.edit', 'uses' => 'VendorController@editAddress']);
        Route::post('/vendors/{id}/address/create', ['as' => 'vendors.address.create', 'uses' => 'VendorController@createAddress']);
        Route::post('/vendors/{id}/address/{addressId}/update', ['as' => 'vendors.address.update', 'uses' => 'VendorController@updateAddress']);
        Route::post('/vendors/{id}/address/{addressId}/delete', ['as' => 'vendors.address.delete', 'uses' => 'VendorController@deleteAddress']);
        Route::get('/vendors/{id}', ['as' => 'vendors.edit', 'uses' => 'VendorController@edit']);
        Route::post('/vendors/{id}', ['as' => 'vendors.update', 'uses' => 'VendorController@update']);

        Route::get('/profile', ['as' => 'profile', 'uses' => 'EmployeeController@profile']);
        Route::post('/profile', ['as' => 'update.profile', 'uses' => 'EmployeeController@updateProfile']);
        Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);

        Route::get('/api/business/stats', ['as' => 'api.business.stats', 'uses' => 'Dashboard\BusinessController@statsAPI']);

        Route::get('/collections', ['as' => 'collections.index', 'uses' => 'CollectionController@index']);
        Route::post('/collections/store', ['as' => 'collections.store', 'uses' => 'CollectionController@store']);
        Route::post('/collections/update', ['as' => 'collections.update', 'uses' => 'CollectionController@update']);
        Route::post('/collections/sort', ['as' => 'collections.sort', 'uses' => 'CollectionController@sort']);
        Route::get('/collections/{id}', ['as' => 'collections.detail', 'uses' => 'CollectionController@detail']);
        Route::post('/collections/{id}/delete', ['as' => 'collections.delete', 'uses' => 'CollectionController@delete']);

        Route::get('/products', ['as' => 'products.index', 'uses' => 'ProductController@index']);
        Route::get('/products/create', ['as' => 'products.create', 'uses' => 'ProductController@create']);
        Route::post('/products/store', ['as' => 'products.store', 'uses' => 'ProductController@store']);
        Route::post('/products/update', ['as' => 'products.update', 'uses' => 'ProductController@update']);
        Route::get('/products/{productId}/variants/create', ['as' => 'products.variants.create', 'uses' => 'ProductVariantController@create']);
        Route::post('/products/{productId}/variants/store', ['as' => 'products.variants.store', 'uses' => 'ProductVariantController@store']);
        Route::get('/products/{productId}/variants/{variantId}', ['as' => 'products.variants.edit', 'uses' => 'ProductVariantController@edit']);
        Route::post('/products/{productId}/variants/update', ['as' => 'products.variants.update', 'uses' => 'ProductVariantController@update']);
        Route::post('/products/variants/delete', ['as' => 'products.variants.delete', 'uses' => 'ProductVariantController@delete']);
        Route::post('/products/variants/restore', ['as' => 'products.variants.restore', 'uses' => 'ProductVariantController@restore']);
        Route::get('/products/{id}', ['as' => 'products.edit', 'uses' => 'ProductController@edit']);
        Route::post('/products/{id}/images/store', ['as' => 'products.images.store', 'uses' => 'ProductController@storeImages']);
        Route::post('/products/{id}/images/update', ['as' => 'products.images.update', 'uses' => 'ProductController@updateImages']);
        Route::post('/products/{id}/images/delete', ['as' => 'products.images.delete', 'uses' => 'ProductController@deleteImage']);
        Route::post('/products/{id}/barcode/generate/', ['as' => 'products.generate.barcode', 'uses' => 'ProductController@genrateBarcode']);
        Route::post('/products/sync', 'ProductController@syncProduct')->name('products.sync');
        Route::post('/products/price/sync', 'ProductController@syncProductPrice')->name('products.sync.price');

        // - Category LC2
        Route::get('/category-l2', ['as' => 'category_l2.index', 'uses' => 'CategoryL2Controller@index']);
        Route::get('/category-l2/create', ['as' => 'category_l2.create', 'uses' => 'CategoryL2Controller@create']);
        Route::post('/category-l2/create', ['as' => 'category_l2.store', 'uses' => 'CategoryL2Controller@store']);
        Route::post('/category-l2/{id}/update', ['as' => 'category_l2.update', 'uses' => 'CategoryL2Controller@update']);
        Route::get('/category-l2/{id}', ['as' => 'category_l2.edit', 'uses' => 'CategoryL2Controller@edit']);

        //Inventory
        Route::get('/products/{id}/inventory', ['as' => 'products.inventory', 'uses' => 'InventoryController@index']);
        Route::post('/inventory/status/update', ['as' => 'inventory.status.update', 'uses' => 'InventoryController@updateStatus']);
        Route::post('/inventory/type/update', ['as' => 'inventory.type.update', 'uses' => 'InventoryController@updateType']);
        Route::post('/inventory/warehouse/update', ['as' => 'inventory.warehouse.update', 'uses' => 'InventoryController@updateWarehouse']);
        Route::get('/inventory/{inventoryId}/transactions', 'InventoryController@getInventoryTransactions')->name('inventory.transactions');
        Route::get('/inventory/status/update/command/', 'InventoryController@updateInventoryStatusUi')->name('inventory.status.update.command');
        Route::post('/inventory/status/update/command', 'InventoryController@updateInventoryStatus')->name('inventory.status.update.command');
        Route::get('/inventory/status-count', 'InventoryController@searchStatusWiseInventoryCount')->name('inventory.status.count');
        Route::get('/inventory/products', 'InventoryController@searchProducts')->name('inventory.search.products');
        Route::get('/inventory/storages', 'InventoryController@searchStorages')->name('inventory.search.storages');

        Route::get('/variants', ['as' => 'variants.index', 'uses' => 'ProductVariantController@index']);
        Route::get('/variant', ['as' => 'variant.details', 'uses' => 'ProductVariantController@getVariantDetails']);

        Route::get('/payments', ['as' => 'payments.index', 'uses' => 'PaymentController@index']);

        Route::get('/attributes', ['as' => 'attributes.index', 'uses' => 'AttributeController@index']);
        Route::post('/attributes/store', ['as' => 'attributes.store', 'uses' => 'AttributeController@store']);
        Route::post('/attributes/update', ['as' => 'attributes.update', 'uses' => 'AttributeController@update']);
        Route::post('/attributes/delete', ['as' => 'attributes.delete', 'uses' => 'AttributeController@delete']);

        Route::get('/brands', ['as' => 'brands.index', 'uses' => 'BrandController@index']);
        Route::post('/brands/store', ['as' => 'brands.store', 'uses' => 'BrandController@store']);
        Route::post('/brands/update', ['as' => 'brands.update', 'uses' => 'BrandController@update']);
        Route::post('/brands/sort', ['as' => 'brands.sort', 'uses' => 'BrandController@sort']);
        Route::get('/brands/{id}', ['as' => 'brands.detail', 'uses' => 'BrandController@detail']);
        Route::post('/brands/{id}/delete', ['as' => 'brands.delete', 'uses' => 'BrandController@delete']);

        Route::get('/marketers', ['as' => 'marketers.index', 'uses' => 'MarketerController@index']);
        Route::get('/marketers/create', ['as' => 'marketers.create', 'uses' => 'MarketerController@create']);
        Route::get('/marketers/{id}', ['as' => 'marketers.edit', 'uses' => 'MarketerController@edit']);
        Route::post('/marketers/store', ['as' => 'marketers.store', 'uses' => 'MarketerController@store']);
        Route::post('/marketers/update', ['as' => 'marketers.update', 'uses' => 'MarketerController@update']);

        Route::get('/employees', ['as' => 'employees.index', 'uses' => 'EmployeeController@index']);
        Route::get('/employees/{id}', ['as' => 'employees.edit', 'uses' => 'EmployeeController@edit']);
        Route::post('/employees/store', ['as' => 'employees.store', 'uses' => 'EmployeeController@store']);
        Route::post('/employees/update', ['as' => 'employees.update', 'uses' => 'EmployeeController@update']);

        Route::post('/permissions/store', ['as' => 'permissions.store', 'uses' => 'PermissionController@store']);
        Route::post('/permissions/update', ['as' => 'permissions.update', 'uses' => 'PermissionController@update']);
        Route::post('/permissions/delete', ['as' => 'permissions.delete', 'uses' => 'PermissionController@delete']);

        Route::get('/roles', ['as' => 'roles.index', 'uses' => 'RoleController@index']);
        Route::get('/roles/{id}', ['as' => 'roles.edit', 'uses' => 'RoleController@edit']);
        Route::post('/roles/store', ['as' => 'roles.store', 'uses' => 'RoleController@store']);
        Route::post('/roles/update', ['as' => 'roles.update', 'uses' => 'RoleController@update']);
        Route::post('/roles/delete', ['as' => 'roles.delete', 'uses' => 'RoleController@delete']);

        Route::get('/badges', ['as' => 'badges.index', 'uses' => 'BadgeController@index']);
        Route::post('/badges/store', ['as' => 'badges.store', 'uses' => 'BadgeController@store']);
        Route::post('/badges/update', ['as' => 'badges.update', 'uses' => 'BadgeController@update']);
        Route::post('/badges/delete', ['as' => 'badges.delete', 'uses' => 'BadgeController@delete']);

        Route::get('/users', ['as' => 'users.index', 'uses' => 'UserController@index']);
        Route::post('/users/store', ['as' => 'users.store', 'uses' => 'UserController@store']);
        Route::post('/users/update', ['as' => 'users.update', 'uses' => 'UserController@update']);
        Route::post('/users/status/update', ['as' => 'users.status.update', 'uses' => 'UserController@updateStatus']);
        Route::post('/users/notes/save', ['as' => 'users.notes.save', 'uses' => 'UserController@storeNotes']);
        Route::get('/users/{id}', ['as' => 'users.profile', 'uses' => 'UserController@profile']);
        Route::post('/users/{id}/badges/update', ['as' => 'users.badges.update', 'uses' => 'UserController@updateBadges']);

        //Store Order
        Route::get('/store-orders', ['as' => 'store.orders.index', 'uses' => 'StoreOrderController@index']);
        Route::get('/store-orders/{id}', ['as' => 'store.orders.edit', 'uses' => 'StoreOrderController@detail']);
        Route::post('/store-orders/store', ['as' => 'store.orders.store', 'uses' => 'StoreOrderController@store']);

        Route::get('/orders', ['as' => 'orders.index', 'uses' => 'OrderController@index']);
        Route::get('/picklist-orders', ['as' => 'orders.picklist', 'uses' => 'OrderController@createPicklistOrders']);
        Route::post('/orders/store', ['as' => 'orders.store', 'uses' => 'OrderController@store']);
        Route::get('/orders/create/products', ['as' => 'orders.create.products', 'uses' => 'OrderController@products']);
        Route::get('/orders/create/users', ['as' => 'orders.create.users', 'uses' => 'OrderController@users']);
        Route::post('/orders/cancel', ['as' => 'orders.cancel', 'uses' => 'OrderController@cancel']);
        Route::post('/orders/manifest', ['as' => 'orders.manifest.generate', 'uses' => 'OrderController@generateOrderManifest']);
        Route::get('/orders/manifest', ['as' => 'orders.manifest.show', 'uses' => 'OrderController@showOrderManifest']);

        Route::get('/orders/{id}', ['as' => 'orders.edit', 'uses' => 'OrderController@detail']);
        Route::post('/orders/{id}/items/update', ['as' => 'orders.items.update', 'uses' => 'OrderController@updateItems']);
        Route::post('/orders/{id}/agent', ['as' => 'orders.assign.agent', 'uses' => 'OrderController@assignAgent']);
        Route::post('/orders/{id}/remarks/store', ['as' => 'orders.remarks.store', 'uses' => 'OrderController@storeRemarks']);
        Route::post('/orders/{id}/item/cancel', ['as' => 'orders.items.cancel', 'uses' => 'OrderController@cancelOrderItem']);

        //TODO: Hack for Agent. To be removed.
        // Route::post('/orders/{id}/store', ['as' => 'orders.assign.store', 'uses' => 'OrderController@assignStore']);
//    Route::get('/orders/{id}/clone', ['as' => 'orders.clone', 'uses' => 'CartController@cloneOrder']);
//    Route::post('/orders/{id}/assignInventory', ['as' => 'orders.assign.inventory', 'uses' => 'OrderController@assignBackOrderedItemInventory']);

        Route::get('/invoice/{id}', ['as' => 'invoices.index', 'uses' => 'InvoiceController@index']);
        Route::post('/invoices/regenerate', ['as' => 'invoices.regenerate', 'uses' => 'InvoiceController@regenerate']);


        Route::get('/special-returns', ['uses' => 'ReturnOrderController@completeSpecialReturn']);
        Route::get('/returns', ['as' => 'returns.index', 'uses' => 'ReturnOrderController@index']);
        Route::get('/returns/{shipmentId}/create', ['as' => 'returns.create', 'uses' => 'ReturnOrderController@create']);
        Route::post('/returns/store', ['as' => 'returns.store', 'uses' => 'ReturnOrderController@store']);
        Route::post('/returns/cancel', ['as' => 'returns.cancel', 'uses' => 'ReturnOrderController@cancel']);
        Route::get('/returns/{return_id}/complete', ['as' => 'returns.confirm-complete', 'uses' => 'ReturnOrderController@confirmComplete']);
        Route::post('/returns/{return_id}/complete', ['as' => 'returns.mark-complete', 'uses' => 'ReturnOrderController@complete']);
//    Route::post('/returns/complete', ['as' => 'returns.complete', 'uses' => 'ReturnOrderController@complete']);
        Route::get('/returns/credit-note/{id}', ['as' => 'returns.credit-note', 'uses' => 'ReturnOrderController@getCreditNote']);

        //Return Cart
        Route::get('/returns/carts/suggestions', ['as' => 'return.carts.suggestions', 'uses' => 'ReturnCartController@suggestions']);
        Route::post('/returns/carts/add-item', ['as' => 'return.carts.add-item', 'uses' => 'ReturnCartController@addItem']);
        Route::post('/returns/carts/update-item', ['as' => 'return.carts.update-item', 'uses' => 'ReturnCartController@updateItem']);
        Route::post('/returns/carts/remove-item', ['as' => 'return.carts.remove-item', 'uses' => 'ReturnCartController@removeItem']);
        Route::post('/returns/orders/store', ['as' => 'return.orders.store', 'uses' => 'ReturnOrderController@createFromCart']);


        Route::get('/shipments', ['as' => 'shipments.index', 'uses' => 'ShipmentController@index']);
        Route::post('/shipments/update-boxes', ['as' => 'shipments.boxes.update', 'uses' => 'ShipmentController@updateBoxQuantity']);
        Route::post('/shipments/cancel', ['as' => 'shipments.cancel', 'uses' => 'ShipmentController@cancel']);
        Route::post('/shipments/retry', ['as' => 'shipments.retry', 'uses' => 'ShipmentController@retry']);
        Route::post('/shipments/rto', ['as' => 'shipments.rto', 'uses' => 'ShipmentController@rto']);
        Route::post('/shipments/deliver', ['as' => 'shipments.deliver', 'uses' => 'ShipmentController@deliver']);
        Route::post('/shipments/resend-otp', ['as' => 'shipments.resend-otp', 'uses' => 'ShipmentController@resendOTP']);

        Route::get('/carts/suggestions', ['as' => 'carts.suggestions', 'uses' => 'CartController@suggestions']);
        Route::post('/carts/add-item', ['as' => 'carts.add-item', 'uses' => 'CartController@addItem']);
        Route::post('/carts/update-item', ['as' => 'carts.update-item', 'uses' => 'CartController@updateItem']);
        Route::post('/carts/remove-item', ['as' => 'carts.remove-item', 'uses' => 'CartController@removeItem']);

        Route::get('/tax-classes', 'TaxClassController@index')->name('tax-classes.index');
        Route::post('/tax-classes/store', 'TaxClassController@store')->name('tax-class.store');
        Route::get('/tax-classes/{id}', 'TaxClassController@edit')->name('tax-class.edit');
        Route::post('/tax-class/update', 'TaxClassController@update')->name('tax-class.update');

        Route::get('/taxes', ['as' => 'taxes.index', 'uses' => 'TaxController@index']);
        Route::post('/taxes/store', ['as' => 'taxes.store', 'uses' => 'TaxController@store']);
        Route::post('/taxes/update', ['as' => 'taxes.update', 'uses' => 'TaxController@update']);
        Route::get('/taxes/create', ['as' => 'taxes.create', 'uses' => 'TaxController@create']);
        Route::get('/taxes/{id}', ['as' => 'taxes.edit', 'uses' => 'TaxController@edit']);

        Route::get('/importers', ['as' => 'importers.index', 'uses' => 'ImporterController@index']);
        Route::post('/importers/store', ['as' => 'importers.store', 'uses' => 'ImporterController@store']);

        Route::get('/stores', ['as' => 'stores.index', 'uses' => 'StoreController@index']);
        Route::get('/stores/{id}', ['as' => 'stores.profile', 'uses' => 'StoreController@profile']);
        Route::post('/stores/store', ['as' => 'stores.store', 'uses' => 'StoreController@store']);
        Route::post('/stores/update', ['as' => 'stores.update', 'uses' => 'StoreController@update']);
        Route::post('/stores/update-credit-limit', ['as' => 'stores.credit-limit.update', 'uses' => 'StoreController@updateCreditLimit']);
        Route::get('/stores/{id}/cart', ['as' => 'stores.cart', 'uses' => 'StoreController@cart']);
        Route::get('/stores/{id}/return-cart', ['as' => 'stores.return-cart', 'uses' => 'StoreController@returnCart']);
        Route::post('/stores/{id}/add-user', ['as' => 'stores.users.add', 'uses' => 'StoreController@addUser']);
        Route::post('/stores/{id}/remove-user', ['as' => 'stores.users.remove', 'uses' => 'StoreController@removeUser']);
        Route::post('/stores/update-status', 'StoreController@updateStatus')->name('stores.updateStatus');

        Route::get('/transactions', ['as' => 'transactions.index', 'uses' => 'TransactionController@getTransactions']);
        Route::get('/transactions/create', ['as' => 'transactions.create', 'uses' => 'TransactionController@create']);
        Route::get('/transactions/store/{id}', ['as' => 'transactions', 'uses' => 'TransactionController@index']);
        Route::post('/transactions/store', ['as' => 'transactions.store', 'uses' => 'TransactionController@store']);
        Route::get('/transactions/{id}', ['as' => 'transactions.edit', 'uses' => 'TransactionController@edit']);
        Route::post('/transactions/delete', ['as' => 'transactions.delete', 'uses' => 'TransactionController@delete']);
        Route::post('/transactions/update', ['as' => 'transactions.update', 'uses' => 'TransactionController@update']);

        Route::get('/beats', ['as' => 'beats.index', 'uses' => 'BeatController@index']);
        Route::get('/beats/create', 'BeatController@create')->name('beats.create');
        Route::get('/beats/{id}', 'BeatController@edit')->name('beats.edit');
        Route::post('/beats/store', 'BeatController@store')->name('beats.store');
        Route::post('/beats/update', 'BeatController@update')->name('beats.update');

        Route::get('/app-banners', 'AppBannerController@index')->name('app-banners.index');
        Route::get('/app-banners/create', 'AppBannerController@create')->name('app-banners.create');
        Route::get('/app-banners/{id}', 'AppBannerController@edit')->name('app-banners.edit');
        Route::post('/app-banners/store', 'AppBannerController@store')->name('app-banners.store');
        Route::post('/app-banners/update', 'AppBannerController@update')->name('app-banners.update');

        Route::get('/warehouses', ['as' => 'warehouses.index', 'uses' => 'WarehouseController@index']);
        Route::get('/warehouses/create', ['as' => 'warehouses.create', 'uses' => 'WarehouseController@create']);
        Route::get('/warehouses/{id}', ['as' => 'warehouses.edit', 'uses' => 'WarehouseController@edit']);
        Route::post('/warehouses/store', ['as' => 'warehouses.store', 'uses' => 'WarehouseController@store']);
        Route::post('/warehouses/update', ['as' => 'warehouses.update', 'uses' => 'WarehouseController@update']);

        Route::get('/reports/', ['as' => 'reports', 'uses' => 'ReportController@report']);
        Route::get('/reports/inventory', ['as' => 'reports.inventory', 'uses' => 'ReportController@inventory']);
        Route::get('/reports/grn-items', ['as' => 'reports.grn-items', 'uses' => 'ReportController@grnItems']);
        Route::get('/reports/inventory-snapshot', ['as' => 'reports.inventory-snapshot', 'uses' => 'ReportController@getInventorySnapshot']);
        Route::get('/reports/inventory-snapshot/{file}', ['as' => 'reports.inventory-snapshot-file', 'uses' => 'ReportController@getInventorySnapshotFile']);
        Route::get('/reports/purchase-invoice', ['as' => 'reports.purchase-invoice.details', 'uses' => 'DailyReportDownloadController@takeDate']);

        Route::get('/reports/product-inventory-snapshot', ['as' => 'reports.product-inventory-snapshot', 'uses' => 'ReportController@getProductInventorySnapshot']);
        Route::get('/reports/product-inventory-snapshot/{file}', ['as' => 'reports.product-inventory-snapshot-file', 'uses' => 'ReportController@getProductInventorySnapshotFile']);

        Route::get('/reports/order-item', ['as' => 'reports.order-item', 'uses' => 'ReportController@orderItem']);
        Route::get('/reports/processing-order-item', ['as' => 'reports.processing-order-item', 'uses' => 'ReportController@processingOrderItem']);
        Route::get('/reports/return-order-item', ['as' => 'reports.return-order-item', 'uses' => 'ReportController@returnOrderItem']);
        Route::get('/reports/invoice', ['as' => 'reports.invoice', 'uses' => 'ReportController@invoices']);
        Route::get('/reports/purchase-invoice/items', ['as' => 'reports.pi.items', 'uses' => 'ReportController@purchaseInvoiceItem']);

        Route::get('/search/attributes', ['as' => 'search.attributes', 'uses' => 'SearchController@attributes']);
        Route::get('/search/users', ['as' => 'search.users', 'uses' => 'SearchController@users']);
        Route::get('/search/agents', ['as' => 'search.agents', 'uses' => 'SearchController@agents']);
        Route::get('/search/vendors', ['as' => 'search.vendors', 'uses' => 'SearchController@vendors']);
        Route::get('/search/groups', ['as' => 'search.groups', 'uses' => 'SearchController@groups']);
        Route::get('/search/badges', ['as' => 'search.badges', 'uses' => 'SearchController@badges']);
        Route::get('/search/variant-type', ['as' => 'search.variant-type', 'uses' => 'SearchController@variantType']);
        Route::get('/search/states', ['as' => 'search.states', 'uses' => 'SearchController@states']);
        Route::get('/search/countries', ['as' => 'search.countries', 'uses' => 'SearchController@countries']);
        Route::get('/search/cities', ['as' => 'search.cities', 'uses' => 'SearchController@cities']);
        Route::get('/search/collections', ['as' => 'search.collections', 'uses' => 'SearchController@collections']);
        Route::get('/serach/cl4-id', ['as' => 'search.cl-id', 'uses' => 'SearchController@cl4Id']);
        Route::get('/search/products', ['as' => 'search.products', 'uses' => 'SearchController@products']);


        Route::get('/search/employees', ['as' => 'search.employees', 'uses' => 'SearchController@employees']);

        Route::get('/search/pickers', 'SearchController@pickers')->name('search.pickers');

//    Route::get('/search/products-by-barcode', ['as' => 'search.products.barcode', 'uses' => 'SearchController@searchProductsByBarcode']);

        Route::get('/search/variants', ['as' => 'search.variants', 'uses' => 'SearchController@variants']);
        Route::get('/search/users', ['as' => 'search.users', 'uses' => 'SearchController@users']);
        Route::get('/search/beats', ['as' => 'search.beats', 'uses' => 'SearchController@beats']);
        Route::get('/search/stores', ['as' => 'search.stores', 'uses' => 'SearchController@stores']);
        Route::get('/search/brands', 'SearchController@brands')->name('search.brands');
        Route::get('/search/marketers', 'SearchController@marketers')->name('search.marketers');
        Route::get('/search/vendors', 'SearchController@vendors')->name('search.vendors');
        Route::get('/search/marketers-by-beat', 'SearchController@availableMarketersByBeat')->name('search.availableMarketersByBeat');
        Route::get('/search/tags', 'SearchController@tags')->name('search.tags');
        Route::get('/search/racks', 'SearchController@racks')->name('search.racks');
        Route::get('/search/vendors', 'SearchController@vendors')->name('search.vendors');
        Route::get('/search/cl2', 'SearchController@cl2')->name('search.cl2');
        Route::get('/search/tax-classes', 'SearchController@taxClasses')->name('search.taxClasses');
        Route::get('/search/beat-locations', 'SearchController@beatLocations')->name('search.beat_locations');
        Route::get('/search/storages', 'SearchController@storages')->name('search.storages');
        Route::get('/search/reasons', 'SearchController@reasons')->name('search.reasons');


//      modification request searches
        Route::get('/search/transactions', 'SearchController@transactions')->name('search.transactions');

        Route::get('/search/purchase-invoices', 'SearchController@purchaseInvoices')->name('search.purchase_invoices');
        Route::get('/search/vendors', 'SearchController@vendors')->name('search.vendors');
        Route::get('/search/vendor-addresses', 'SearchController@vendorAddresses')->name('search.vendor_addresses');
        Route::get('/search/purchase-platforms', 'SearchController@purchasePlatforms')->name('search.purchase_platforms');
        Route::get('/search/discount-platforms', 'SearchController@discountPlatforms')->name('search.discount_platforms');

        Route::get('/search/purchase-invoice-items', 'SearchController@purchaseInvoiceItems')->name('search.purchase_invoice_items');
        Route::get('/search/products_2', 'SearchController@products_2')->name('search.products_2');

        Route::get('/search/payment-tags', 'SearchController@paymentTags')->name('search.payment_tags');

        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        //Agents
        Route::get('/agents', ['as' => 'agents.index', 'uses' => 'AgentController@index']);
        Route::post('/agents/store', ['as' => 'agents.store', 'uses' => 'AgentController@store']);
        Route::post('/agents/update', ['as' => 'agents.update', 'uses' => 'AgentController@update']);
        Route::post('/agents/map/update', ['as' => 'agents.map.update', 'uses' => 'AgentController@updateBeatMap']);
        Route::post('/agents/map/delete', ['as' => 'agents.map.delete', 'uses' => 'AgentController@deleteBeatMap']);
        Route::post('/agents/status/update', ['as' => 'agents.status.update', 'uses' => 'AgentController@updateStatus']);
        Route::get('/agents/{id}', ['as' => 'agents.profile', 'uses' => 'AgentController@profile']);

        // Rules
        Route::get('/rules', ['as' => 'rules.index', 'uses' => 'RuleController@index']);
        Route::get('/rules/create', ['as' => 'rules.create', 'uses' => 'RuleController@create']);
        Route::post('/rules/store', ['as' => 'rules.store', 'uses' => 'RuleController@store']);
        Route::get('/rules/{id}', ['as' => 'rules.detail', 'uses' => 'RuleController@detail']);
        Route::post('/rules/{id}/update', ['as' => 'rules.detail.update', 'uses' => 'RuleController@update']);

        //Product Demands
        Route::get('/product-demands', ['as' => 'product-demands.index', 'uses' => 'ProductDemandController@index']);
        Route::get('/product-demands/{id}', ['as' => 'product-demands.edit', 'uses' => 'ProductDemandController@edit']);
        Route::post('/product-demands/store', ['as' => 'product-demands.store', 'uses' => 'ProductDemandController@store']);
        Route::post('/product-demands/update-items', ['as' => 'product-demands.items.update', 'uses' => 'ProductDemandController@updateItems']);
        Route::post('/product-demands/{id}/add-item', ['as' => 'product-demands.items.add', 'uses' => 'ProductDemandController@addItem']);

        //Product Demands
        Route::get('/search/variants', ['as' => 'search.variants', 'uses' => 'SearchController@variants']);
        Route::get('/search/users', ['as' => 'search.users', 'uses' => 'SearchController@users']);
        Route::get('/search/beats', ['as' => 'search.beats', 'uses' => 'SearchController@beats']);
        Route::get('/search/stores', ['as' => 'search.stores', 'uses' => 'SearchController@stores']);
        Route::get('/search/brands', 'SearchController@brands')->name('search.brands');
        Route::get('/search/marketers', 'SearchController@marketers')->name('search.marketers');
        Route::get('/search/vendors', 'SearchController@vendors')->name('search.vendors');
        Route::get('/search/marketers-by-beat', 'SearchController@availableMarketersByBeat')->name('search.availableMarketersByBeat');
        Route::get('/search/tags', 'SearchController@tags')->name('search.tags');
        Route::get('/search/racks', 'SearchController@racks')->name('search.racks');
        Route::get('/search/vendors', 'SearchController@vendors')->name('search.vendors');
        Route::get('/search/cl2', 'SearchController@cl2')->name('search.cl2');
        Route::get('/search/tax-classes', 'SearchController@taxClasses')->name('search.taxClasses');
        Route::get('/search/beat-locations', 'SearchController@beatLocations')->name('search.beat_locations');

        Route::get('/search/warehouses', 'SearchController@warehouses')->name('search.warehouses');


        //Agents
        Route::get('/agents', ['as' => 'agents.index', 'uses' => 'AgentController@index']);
        Route::post('/agents/store', ['as' => 'agents.store', 'uses' => 'AgentController@store']);
        Route::post('/agents/update', ['as' => 'agents.update', 'uses' => 'AgentController@update']);
        Route::post('/agents/map/update', ['as' => 'agents.map.update', 'uses' => 'AgentController@updateBeatMap']);
        Route::post('/agents/map/delete', ['as' => 'agents.map.delete', 'uses' => 'AgentController@deleteBeatMap']);
        Route::post('/agents/status/update', ['as' => 'agents.status.update', 'uses' => 'AgentController@updateStatus']);
        Route::get('/agents/{id}', ['as' => 'agents.profile', 'uses' => 'AgentController@profile']);

        // Rules
        Route::get('/rules', ['as' => 'rules.index', 'uses' => 'RuleController@index']);
        Route::get('/rules/create', ['as' => 'rules.create', 'uses' => 'RuleController@create']);
        Route::post('/rules/store', ['as' => 'rules.store', 'uses' => 'RuleController@store']);
        Route::get('/rules/{id}', ['as' => 'rules.detail', 'uses' => 'RuleController@detail']);
        Route::post('/rules/{id}/update', ['as' => 'rules.detail.update', 'uses' => 'RuleController@update']);

    //Agents
    Route::get('/agents', ['as' => 'agents.index', 'uses' => 'AgentController@index']);
    Route::post('/agents/store', ['as' => 'agents.store', 'uses' => 'AgentController@store']);
    Route::post('/agents/update', ['as' => 'agents.update', 'uses' => 'AgentController@update']);
    Route::post('/agents/map/update', ['as' => 'agents.map.update', 'uses' => 'AgentController@updateBeatMap']);
    Route::post('/agents/map/delete', ['as' => 'agents.map.delete', 'uses' => 'AgentController@deleteBeatMap']);
    Route::post('/agents/status/update', ['as' => 'agents.status.update', 'uses' => 'AgentController@updateStatus']);
    Route::get('/agents/{id}', ['as' => 'agents.profile', 'uses' => 'AgentController@profile']);

    // Rules
    Route::get('/rules', ['as' => 'rules.index', 'uses' => 'RuleController@index']);
    Route::get('/rules/create', ['as' => 'rules.create', 'uses' => 'RuleController@create']);
    Route::post('/rules/store', ['as' => 'rules.store', 'uses' => 'RuleController@store']);
    Route::get('/rules/{id}', ['as' => 'rules.detail', 'uses' => 'RuleController@detail']);
    Route::post('/rules/{id}/update', ['as' => 'rules.detail.update', 'uses' => 'RuleController@update']);

    //Product Demands
    Route::get('/product-demands', ['as' => 'product-demands.index', 'uses' => 'ProductDemandController@index']);
    Route::get('/product-demands/create', ['as' => 'product-demands.create', 'uses' => 'ProductDemandController@demandForm']);
    Route::get('/product-demands/edit/{id}', ['as' => 'product-demands.edit', 'uses' => 'ProductDemandController@demandForm']);
    Route::post('/product-demands/save/{id?}', ['as' => 'product-demands.save', 'uses' => 'ProductDemandController@saveDemand']);
    Route::post('/product-demands/{id}/items/save', ['as' => 'product-demands.items.save', 'uses' => 'ProductDemandController@saveDemandItems']);
    Route::get('/product-demands/{id}/po-items-preview', ['as' => 'product-demands.preview.po-items', 'uses' => 'ProductDemandController@previewDemandOrderItems']);
    Route::post('/product-demands/{id}/items/assign-vendor', ['as' => 'product-demands.items.assign-vendor', 'uses' => 'ProductDemandController@updateVendorAssignment']);
    Route::get('/product-demands/{id}/po-preview', ['as' => 'product-demands.preview.po', 'uses' => 'ProductDemandController@previewDemandOrders']);
    Route::post('/product-demands/{id}/create-po', ['as' => 'product-demands.po.create', 'uses' => 'ProductDemandController@createPOFromDemand']);

//    Route::get('/product-demands/{id}', ['as' => 'product-demands.edit', 'uses' => 'ProductDemandController@viewItems']);


    Route::post('/product-demands/store', ['as' => 'product-demands.store', 'uses' => 'ProductDemandController@store']);
    Route::post('/product-demands/update-items', ['as' => 'product-demands.items.update', 'uses' => 'ProductDemandController@updateItems']);
    Route::post('/product-demands/{id}/add-item', ['as' => 'product-demands.items.add', 'uses' => 'ProductDemandController@addItem']);

    //Product Demands

//    Route::get('/purchase-orders', ['as' => 'purchase-orders.index', 'uses' => 'PurchaseOrderController@index']);
//    Route::post('/purchase-orders/store', ['as' => 'purchase-orders.store', 'uses' => 'PurchaseOrderController@store']);
//    Route::get('/purchase-orders/{id}', ['as' => 'purchase-orders.edit', 'uses' => 'PurchaseOrderController@edit']);
//    Route::get('/purchase-orders/{id}/pdf', ['as' => 'purchase-orders.pdf', 'uses' => 'PurchaseOrderController@pdf']);

        //Product Group
        Route::get('/product-group', ['as' => 'product-group.index', 'uses' => 'ProductGroupController@index']);
        Route::get('/product-group/store', ['as' => 'product-group.add', 'uses' => 'ProductGroupController@add']);
        Route::post('/product-group/store', ['as' => 'product-group.store', 'uses' => 'ProductGroupController@store']);
        Route::get('/product-group/edit/{id}', ['as' => 'product-group.edit', 'uses' => 'ProductGroupController@edit']);
        Route::post('/product-group/edit/{id}', ['as' => 'product-group.update', 'uses' => 'ProductGroupController@update']);
        Route::get('/product-group/delete/{id}', ['as' => 'product-group.delete', 'uses' => 'ProductGroupController@delete']);

        //Picklist
        Route::get('/picklists', 'PicklistController@index')->name('picklists.index');
        Route::get('/picklists/{id}', 'PicklistController@items')->name('picklists.items');
        Route::get('/picklists/{id}/scanner', 'PicklistController@scanner')->name('picklists.scanner');
        Route::get('/picklists/{id}/shipments', 'PicklistController@shipments')->name('picklists.shipments');
        Route::get('/picklists/{id}/pdf/download', 'PicklistController@getPdf')->name('picklists.pdf');
        Route::post('/picklists/store', 'PicklistController@store')->name('picklists.store');
        Route::post('/picklists/close', 'PicklistController@close')->name('picklists.close');
        Route::post('/picklists/mark-picked', 'PicklistController@close')->name('picklists.mark-picked');
        Route::post('/picklists/updateAgent', 'PicklistController@updateAgent')->name('picklists.agent.update');
        Route::post('/picklists/assign-picker', 'PicklistController@assignPicker')->name('picklists.assign.picker');

        //Picklist Items
        Route::get('/picklists/items/search', ['as' => 'picklists.items.search', 'uses' => 'PicklistItemController@search']);
        Route::post('/picklists/items/add', ['as' => 'picklists.items.add-item', 'uses' => 'PicklistItemController@addItem']);
        Route::post('/picklists/items/remove', ['as' => 'picklists.items.remove-item', 'uses' => 'PicklistItemController@removeItem']);

        // Manifests
        Route::get('/manifests', 'ManifestController@index')->name('manifests.index');
        Route::get('/manifests/create', 'ManifestController@create')->name('manifests.create');
        Route::get('/manifests/{id}', 'ManifestController@edit')->name('manifests.edit');
        Route::get('/manifests/{id}/pdf', 'ManifestController@getPdf')->name('manifests.pdf');
        Route::get('/manifests/{id}/invoices', 'ManifestController@getInvoices')->name('manifests.invoices.pdf');
        Route::post('/manifests/store', 'ManifestController@store')->name('manifests.store');
        Route::post('/manifests/cancel', 'ManifestController@cancel')->name('manifests.cancel');
        Route::post('/manifests/remove-shipment', 'ManifestController@removeShipment')->name('manifests.remove-shipment');
        Route::post('/manifests/dispatched', 'ManifestController@dispatched')->name('manifests.dispatch');
        Route::post('/manifests/close', 'ManifestController@close')->name('manifests.close');
        Route::post('/manifests/deliver-shipments', 'ManifestController@deliverShipments')->name('manifests.deliver-shipments');
        Route::post('/manifests/details', 'ManifestController@updateDetails')->name('manifests.update-details');

        //Agent Dashboard Image
        Route::get('/agent-dashboard-image', 'DashboardImageController@index')->name('dashboard-image.index');
        Route::get('/agent-dashboard-image/create', 'DashboardImageController@create')->name('dashboard-image.create');
        Route::get('/agent-dashboard-image/{id}', 'DashboardImageController@edit')->name('dashboard-image.edit');
        Route::post('/agent-dashboard-image/store', 'DashboardImageController@store')->name('dashboard-image.store');
        Route::post('/agent-dashboard-image/update', 'DashboardImageController@update')->name('dashboard-image.update');

        Route::get('/purchase-invoices', 'PurchaseInvoiceController@index')->name('purchase-invoices.index');
        Route::get('/purchase-invoice/carts', 'PurchaseInvoiceCartController@index')->name('purchase-invoices.cart-index');

        Route::post('/purchase-invoice/cart', 'PurchaseInvoiceCartController@createCart')->name('purchase-invoices.cart.create');
        Route::get('/purchase-invoice/cart/{vendor_id}', 'PurchaseInvoiceCartController@manageCart')->name('purchase-invoices.cart');
        Route::post('/purchase-invoice/cart/{cart_id}/upload-file', 'PurchaseInvoiceCartController@uploadCopy')->name('purchase-invoices.cart.upload-copy');
        Route::get('/purchase-invoice/cart/{cart_id}/get-file', 'PurchaseInvoiceCartController@getScannedCopy')->name('purchase-invoices.cart.get-copy');

        Route::get('/purchase-invoice/cart/{cart_id}/product/suggestion', 'PurchaseInvoiceCartController@itemSuggestions')->name('purchase-invoices.cart.suggestion');
        Route::post('/purchase-invoice/cart/{cart_id}/cart/update', 'PurchaseInvoiceCartController@updateCart')->name('purchase-invoices.cart.update');
        Route::post('/purchase-invoice/cart/{cart_id}/add-item', 'PurchaseInvoiceCartController@addItem')->name('purchase-invoices.cart.addItem');
        Route::get('/purchase-invoice/cart/{cart_id}/items', 'PurchaseInvoiceCartController@getCartItemsView')->name('purchase-invoices.cart.items');
        Route::post('/purchase-invoice/cart/remove-item', 'PurchaseInvoiceCartController@removeItem')->name('purchase-invoices.cart.remove_item');
        Route::post('/purchase-invoice/cart/update-item', 'PurchaseInvoiceCartController@updateItem')->name('purchase-invoices.cart.update_item');
        Route::post('/purchase-invoice/cart/{cart_id}/create-invoice', 'PurchaseInvoiceCartController@createInvoice')->name('purchase-invoices.cart.create_invoice');
        Route::get('/purchase-invoice/cart/{cart_id}/item/{id}', 'PurchaseInvoiceCartController@getCartItemById')->name('purchase-invoices.cart.item');
        Route::post('/purchase-invoice/cart/{cart_id}/delete', 'PurchaseInvoiceCartController@deleteCartById')->name('purchase-invoices.cart.delete');

        Route::post('/purchase-invoice/cart/{cart_id}/short-item', 'PurchaseInvoiceCartController@createShortProduct')->name('purchase-invoices.cart.store.short-item');
        Route::post('/purchase-invoice/cart/{cart_id}/remove-short-item', 'PurchaseInvoiceCartController@removeShortProduct')->name('purchase-invoices.cart.remove_short_item');

        Route::get('/purchase-invoice/get-value-preview', 'PurchaseInvoiceController@getCalculatedFields')->name('purchase-invoices.valuePreview');
        Route::post('/purchase-invoice/reset-item-import-qty', 'PurchaseInvoiceController@resetItemImportQty')->name('purchase-invoices.resetItemImport');

        Route::get('/purchase-invoice/{id}', 'PurchaseInvoiceController@editInvoice')->name('purchase-invoices.editInvoice');
        Route::get('/purchase-invoice/{id}/return-items', 'PurchaseInvoiceController@returnInvoiceItems')->name('purchase-invoices.returnItems');
        Route::post('/purchase-invoice/return-items', 'PurchaseInvoiceController@createReturnDebit')->name('purchase-invoices.returnItems.save');
        Route::post('/purchase-invoice/{id}/update-item-price', 'PurchaseInvoiceController@updateItemPrice')->name('purchase-invoices.editItemPrice');

        Route::post('/purchase-invoice/{id}/delete', 'PurchaseInvoiceController@deleteInvoice')->name('purchase-invoices.delete');

        Route::post('/purchase-invoice/{id}/short-item/{item_id}', 'PurchaseInvoiceController@updateShortProduct')->name('purchase-invoices.cart.update.short-item');
        Route::post('/purchase-invoice/generate-sale-order', 'PurchaseInvoiceController@generateSaleOrder')->name('purchase-invoices.generate-sale-order');

        Route::get('/goods-receipt-note', 'PurchaseInvoiceGrnController@index')->name('grn.index');

        // create invoice from PO
        Route::get('/purchase-invoices/create/{po_id}', 'PurchaseInvoiceController@create')->name('purchase-invoices.create');
        Route::get('/purchase-invoices/item-html', 'PurchaseInvoiceController@itemHtml')->name('purchase-invoice.item_html');
        // save
        Route::post('purchase-invoices/save', 'PurchaseInvoiceController@store')->name('purchase-invoice.save');
        Route::post('purchase-invoice/{id}/complete', 'PurchaseInvoiceController@markComplete')->name('purchase-invoices.complete');
        Route::post('purchase-invoice/{id}/updateForecastPaymentDate', 'PurchaseInvoiceController@updateForecastPaymentDate')->name('purchase-invoices.update_forecast_payment_date');
        Route::post('purchase-invoice/{id}/updatePaymentDate', 'PurchaseInvoiceController@updatePaymentDate')->name('purchase-invoices.update_payment_date');
        Route::post('purchase-invoice/{id}/updateDiscountPlatform', 'PurchaseInvoiceController@updateDiscountPlatform')->name('purchase-invoices.update_discount_platform');
        Route::post('purchase-invoice/{id}/debit-note', 'DebitNoteController@createDebitNote')->name('purchase-invoices.create-debit-note');

        Route::get('debit-notes', 'DebitNoteController@index')->name('debit-notes');
        Route::get('debit-note/{id}/pdf', 'DebitNoteController@generatePdf')->name('purchase-invoices.debit-note-pdf');

        Route::get('purchase-invoice/{id}/calculate', 'PurchaseInvoiceController@recalculateInvoice')->name('purchase-invoices.calculate');

        Route::post('purchase-invoice/{id}/upload-file', 'PurchaseInvoiceController@uploadCopy')->name('purchase-invoices.upload-copy');
        Route::get('/purchase-invoice/{id}/get-file', 'PurchaseInvoiceController@getScannedCopy')->name('purchase-invoices.get-copy');

        Route::post('purchase-invoice/{id}/generate_grn', ['as' => 'purchase-invoice.generate_grn', 'uses' => 'PurchaseInvoiceGrnController@store']);
        Route::post('purchase-invoice/{id}/approve', ['as' => 'purchase-invoice.approve', 'uses' => 'PurchaseInvoiceController@importPurchaseInvoice']);
        Route::get('purchase-invoice/{id}/get_grn', ['as' => 'purchase-invoice.grn', 'uses' => 'PurchaseInvoiceController@getGrnPdf']);
        Route::get('purchase-invoice/grn/{id}/import', ['as' => 'purchase-invoice.grn.import', 'uses' => 'PurchaseInvoiceGrnController@importInventoryByGrn']);
        Route::post('/purchase-invoice/{id}/barcode/{productId}', 'PurchaseInvoiceController@generateLabels')->name('purchase-invoices.generate-labels');

        Route::get('/product-requests', ['as' => 'product_request.list', 'uses' => 'ProductRequestController@index']);
        Route::get('/product-request', ['as' => 'product_request.detail', 'uses' => 'ProductRequestController@getById']);
        Route::get('/product-request/{id}/approve', ['as' => 'product_request.approve', 'uses' => 'ProductRequestController@createProductFromRequest']);
        Route::get('/product-request/{id}', ['as' => 'product_request.edit', 'uses' => 'ProductRequestController@edit']);
        Route::post('/product-request/store', ['as' => 'product_request.create', 'uses' => 'ProductRequestController@store']);
        Route::post('/product-request/{id}/delete', ['as' => 'product_request.delete', 'uses' => 'ProductRequestController@deleteProductRequest']);
        Route::post('/product-request/update', 'ProductRequestController@update')->name('product_request.update');

        // Profile questions
        Route::get('/profile-questions', 'ProfileQuestionsController@index')->name('profile-questions.index');
        Route::get('/profile-question/{id?}', 'ProfileQuestionsController@edit')->name('profile-questions.edit');
        Route::post('/profile-question', 'ProfileQuestionsController@manageQuestion')->name('profile-questions.post');
        // Profile questions Answers
        Route::get('/profile-question/{id}/options', 'ProfileQuestionsController@options')->name('profile-questions.options');
        Route::get('/profile-question/{q_id}/option/{id?}', 'ProfileQuestionsController@editOption')->name('profile-questions.options.edit');
        Route::post('/profile-question/{q_id}/option', 'ProfileQuestionsController@manageOption')->name('profile-questions.options.post');

        Route::get('/demand-generation', 'DemandGenerationController@generateDemand')->name('demand-generation.generateDemand');
        Route::get('/auto-replenish-demand', 'DemandGenerationController@index')->name('auto-replenish-demand.index');
        Route::post('/create-purchase-order-with-demand', 'DemandGenerationController@submit')->name('purchase-order.submit');
        Route::post('/create-purchase-order', 'PurchaseOrderController@createPurchaseOrder')->name('purchase-order.create');

        Route::get('/purchase-orders', 'PurchaseOrderController@index')->name('purchase-order.index');
        Route::get('/purchase-order/{id}', 'PurchaseOrderController@details')->name('purchase-order.single');
        Route::post('/purchase-order/{id}/update-item', 'PurchaseOrderController@updatePurchaseOrderItem')->name('purchase-order.update-item');
        Route::post('/purchase-order/{id}/add-item', 'PurchaseOrderController@addNewGroupToPurchaseOrder')->name('purchase-order.add-item');
        Route::post('/purchase-order/{id}/place-order', 'PurchaseOrderController@placePurchaseOrder')->name('purchase-order.place-order');
        Route::get('/purchase-order/{id}/generate-file', 'PurchaseOrderController@generatePurchaseOrderFile')->name('purchase-order.generate-file');
        Route::get('/purchase-order/{id}/file', 'PurchaseOrderController@getPurchaseOrderFile')->name('purchase-order.get-file');
        Route::post('/purchase-order/{id}/update-purchase-order', 'PurchaseOrderController@updatePurchaseOrder')->name('purchase-order.update-purchase-order');

        Route::post('/purchase-order/{id}/create-purchase-invoice', 'PurchaseOrderController@generatePurchaseInvoice')->name('purchase-order.create-purchase-invoice');

//    Route::get('/purchase-order-items', 'DemandGenerationController@purchaseOrders')->name('purchase-order.purchaseOrders');

        Route::get('/barcode/{productId}/{qty}', ['as' => 'barcode.labels', 'uses' => 'BarcodeController@generateLabels']);
        Route::post('/daily-report/download', ['as' => 'report.dailyPurchaseReportDownload', 'uses' => 'DailyReportDownloadController@dailyPurchaseReportDownload']);
        Route::get('/pincode', ['as' => 'pincode.index', 'uses' => 'PincodeController@index']);
        Route::get('/pincode/create', ['as' => 'pincode.create', 'uses' => 'PincodeController@create']);
        Route::post('/pincode/store', ['as' => 'pincode.store', 'uses' => 'PincodeController@store']);
        Route::get('/Pincode/{id}', ['as' => 'pincode.edit', 'uses' => 'PincodeController@edit']);
        Route::post('/pincode/{id}', ['as' => 'pincode.update', 'uses' => 'PincodeController@update']);

        Route::get('/rate-credit-note/{id}/file', 'RateCreditNoteController@downloadCreditNote')->name('rate-credit-note.get-file');
        Route::get('/rate-credit-notes', 'RateCreditNoteController@index')->name('rate-credit-note.index');

        // keeping below a get call since I'll be creating it manually
        Route::get('/rate-credit-notes/{id}/create-transaction', 'RateCreditNoteController@createTransaction')->name('rate-credit-note.create-transaction');


        Route::get('/putaway', 'PutawayController@index')->name('putaway.index');
        Route::get('/putaway/search-product', 'PutawayController@searchProduct')->name('putaway.search-product');
        Route::get('/putaway/{id}', 'PutawayController@detail')->name('putaway.detail');
        Route::post('/putaway/create', 'PutawayController@create')->name('putaway.create');
        Route::post('/putaway/{id}/put-item', 'PutawayController@putItem')->name('putaway.put-item');
        Route::post('/putaway/{id}/close', 'PutawayController@close')->name('putaway.close');

        Route::get('/tags', 'TagController@index')->name('tags.index');

        Route::get('/stock-transfers/outbound', 'StockTransferController@outBound')->name('stock-transfer.outbound');
        Route::get('/stock-transfers/inbound', 'StockTransferController@inBound')->name('stock-transfer.inbound');
        Route::post('/stock-transfer/start', 'StockTransferController@start')->name('stock-transfer.start');
        Route::post('/stock-transfer/update', 'StockTransferController@update')->name('stock-transfer.update');
        Route::get('/stock-transfers/{id}/items', 'StockTransferController@items')->name('stock-transfers.items');
        Route::post('/stock-transfers/confirm', 'StockTransferController@markConfirm')->name('stock-transfers.confirm');
        Route::post('/stock-transfers/complete', 'StockTransferController@markComplete')->name('stock-transfers.complete');
        Route::get('/stock-transfers/note', 'StockTransferController@generatePdf')->name('stock-transfers.pdf');



        // Modification Requests
        Route::get('/modification-requests/{modificationRequestType}', [
            'as' => 'modification-requests.index',
            'uses' => 'ModificationRequestController@index']);

        foreach (\Niyotail\Models\ModificationRequest::getTypes() as $modificationRequestType) {
            $slug = str_replace("_", "-", $modificationRequestType);

            Route::post("/modification-requests/{$slug}/create", [
                'as' => "modification-requests.{$slug}.create",
                'uses' => 'ModificationRequestController@create'
            ]);

            Route::post("/modification-requests/{$slug}/delete", [
                'as' => "modification-requests.{$slug}.delete",
                'uses' => 'ModificationRequestController@delete'
            ]);
        }

        Route::get("/modification-requests/purchase-invoice/download-scanned-copy", [
            'as' => "modification-requests.purchase-invoice.download-invoice",
            'uses' => 'ModificationRequestController@downloadPurchaseInvoiceImage'
        ])->where(['modificationRequestType' => \Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE]);

        Route::post("/modification-requests/purchase-invoice/upload-scanned-copy", [
            'as' => "modification-requests.purchase-invoice.upload-invoice",
            'uses' => 'ModificationRequestController@uploadPurchaseInvoiceImage'
        ])->where(['modificationRequestType' => \Niyotail\Models\ModificationRequest::TYPE_PURCHASE_INVOICE]);

        Route::prefix('/banks')->group(function (){
            Route::get('/', 'BankController@index')->name('bank.index');
            Route::post('/store','BankController@store')->name('bank.store');
            Route::post('/update','BankController@update')->name('bank.update');
        });

        // Audit Views
        Route::get("/audits", "AuditController@index")->name("audits.index");
        Route::get("/audits/{audit}", "AuditController@detail")->name("audits.detail");
        Route::get("/audit-hold-list", "AuditController@holdList")->name("audits.hold-list");
        Route::get("/audit-storage-report", "AuditController@storageReport")->name("audits.storage-report");

        // Audit Resource
        Route::post("/audits/create", "AuditController@createAudit")->name("audits.create");
        Route::post("/audits/{audit}/confirm", "AuditController@confirmAudit")->name("audits.confirm");
        Route::post("/audits/{audit}/start", "AuditController@startAudit")->name("audits.start");
        Route::post("/audits/{audit}/approve", "AuditController@approveAudit")->name("audits.approve");
        Route::post("/audits/{audit}/complete", "AuditController@completeAudit")->name("audits.complete");
        Route::post("/audits/{audit}/cancel", "AuditController@cancelAudit")->name("audits.cancel");
        Route::post("/audits/{audit}/bulk-assign", "AuditController@bulkAssign")->name("audits.bulk-assign");
        Route::post("/audits/{audit}/bulk-reject", "AuditController@bulkReject")->name("audits.bulk-reject");
        Route::post("/audits/{audit}/bulk-cancel", "AuditController@bulkCancel")->name("audits.bulk-cancel");

        // Audit Item Resource
        Route::post("/audits/{audit}/items/{auditItem}/assign", "AuditController@assign")->name("audits.items.assign");
        Route::post("/audits/{audit}/items/{auditItem}/reject", "AuditController@reject")->name("audits.items.reject");
        Route::post("/audits/{audit}/items/{auditItem}/action", "AuditController@action")->name("audits.items.action");
        Route::post("/audits/{audit}/items/{auditItem}/dismiss", "AuditController@action")->name("audits.items.dismiss");
        Route::post("/audits/{audit}/items/{auditItem}/cancel", "AuditController@cancel")->name("audits.items.cancel");

        Route::prefix('flat-inventory')->group(function (){
           Route::get('/product-wise-count', 'FlatInventoryController@productWiseInventoryCount')->name('flat-inventory.product-wise-count');
           Route::get('/storages', 'FlatInventoryController@storages')->name('flat-inventory.storages');
           Route::get('/storages/{id}/details', 'FlatInventoryController@storageDetails')->name('flat-inventory.storages.details');
           Route::get('/product/{id}/locations/', 'FlatInventoryController@productLocations')->name('flat-inventory.product-locations');
        });

        Route::post('/invoice/eway-bill-number', 'InvoiceController@addEWayBillNumber')->name('invoice.assign.eway_bill');

        Route::post('/store-order/{id}/retry-unfulfilled', 'StoreOrderController@retryUnFullFilledOrder')->name('retry-store-order');

    });
});