/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/PurchaseInvoice.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PurchaseInvoice = function () {
    function PurchaseInvoice() {
        var _this = this;

        _classCallCheck(this, PurchaseInvoice);

        this.selectStores = $('#select2-stores');
        this.initializeStoresSelect();
        $('#button-open-cart').on('click', function (e) {
            return _this.createCart(e);
        });
        $(document).on('click', '.gen-sale-order', function (e) {
            return _this.generateSaleOrder(e);
        });
    }

    _createClass(PurchaseInvoice, [{
        key: 'createCart',
        value: function createCart(e) {
            var $target = $(e.currentTarget);
            var url = $target.data('url');
            var destination = $target.data('destination');
            var vendors = this.selectStores.select2('data');
            var selectedVendor = vendors[0].id;

            $target.prop('disabled', true);
            $.ajax(url, {
                method: 'POST',
                data: {
                    vendor_id: selectedVendor
                }
            }).done(function (res) {
                window.location = destination.replace("#", res.id);
            }).fail(function (error) {
                console.log(error);
                var toast = new Toast('Something went wrong!');
                toast.show();
                $target.prop('disabled', false);
            });
        }
    }, {
        key: 'generateSaleOrder',
        value: function generateSaleOrder(e) {
            var $target = $(e.currentTarget);
            var url = $target.data('url');
            var pID = $target.data('id');
            var request = $.ajax({
                type: "POST",
                url: url,
                data: { purchase_invoice_id: pID },
                beforeSend: function beforeSend() {
                    $(".ajax-loader").show();
                    $target.attr('disabled', 'disabled');
                }
            });

            request.done(function (response) {
                location.reload();
            });
            request.fail(function (error) {
                var toast = new Toast('Something went wrong!');
                toast.show();
            });
        }
    }, {
        key: 'initializeStoresSelect',
        value: function initializeStoresSelect() {
            var searchStoresUrl = this.selectStores.data('url');
            this.selectStores.select2({
                minimumInputLength: 2,
                ajax: {
                    url: searchStoresUrl,
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: this.vendorTemplate,
                templateSelection: this.vendorSelection
            });
        }
    }, {
        key: 'vendorTemplate',
        value: function vendorTemplate(vendor) {
            if (!vendor.id) {
                return vendor.text;
            }
            var html = '<div class="padding-10">';
            html += '<h5 class="h6 bold">' + vendor.name + '</h5>';
            if (vendor.addresses.length) {
                html += '<h6 class="margin-t-5">' + vendor.addresses[0].address + '\n                <br>GSTIN: ' + vendor.addresses[0].gstin + '</h6>';
            }
            html += '</div>';

            return $(html);
        }
    }, {
        key: 'vendorSelection',
        value: function vendorSelection(vendor) {
            return vendor.name;
        }
    }]);

    return PurchaseInvoice;
}();

/* harmony default export */ __webpack_exports__["default"] = (new PurchaseInvoice());

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/PurchaseInvoice.js");


/***/ })

/******/ });