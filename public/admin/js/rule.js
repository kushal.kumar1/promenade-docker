/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/rule/Action.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Selector__ = __webpack_require__("./resources/assets/admin/js/rule/Selector.js");
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Action = function () {
    function Action(rule) {
        _classCallCheck(this, Action);

        this.rule = rule;
        this.attachListeners();
        var offer = ":first";
        if (this.rule.data.offer) {
            offer = "[value=" + this.rule.data.offer + "]";
        }
        $("#action-card input[name=offer]" + offer).trigger('click');
    }

    _createClass(Action, [{
        key: "attachListeners",
        value: function attachListeners() {
            var _this = this;

            $('#action-card input[name=offer]').on('change', function (e) {
                if (e.target.checked) {
                    _this.onOfferChanged(e.target.value);
                }
            });
        }
    }, {
        key: "onRuleTypeChanged",
        value: function onRuleTypeChanged() {
            var type = this.rule.data.type;
            $('#limits-wrapper').addClass('hidden');
            $("#action-card input[name=offer]").prop('disabled', true);
            $('#action-card .offer').addClass('hidden');

            $("#action-card .offer-" + type + " input[name=offer]").prop('disabled', false);
            $("#action-card .offer-" + type).removeClass('hidden');

            if (type != 'pricing') {
                $('#limits-wrapper').removeClass('hidden');
            }
        }
    }, {
        key: "onOfferChanged",
        value: function onOfferChanged(type) {
            var ruleData = this.rule.data;
            var data = {
                source_quantity: ruleData.source_quantity || 1,
                target_quantity: ruleData.target_quantity || 1,
                value: ruleData.value || 0
            };
            var actionName = type == "free" ? "free" : "price";
            var template = new Template($(".templates .action-" + actionName));
            template.setData(data);
            template.replaceIn($('#action'));
            var $actionNode = template.getJqueryObject();
            var selector = new __WEBPACK_IMPORTED_MODULE_0__Selector__["a" /* default */]($actionNode.find('.selector-variant'), 'variant');
            if (ruleData.targets) {
                selector.setSelected('variant', ruleData.targets);
            }
        }
    }]);

    return Action;
}();

/* harmony default export */ __webpack_exports__["a"] = (Action);

/***/ }),

/***/ "./resources/assets/admin/js/rule/Condition.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Expression__ = __webpack_require__("./resources/assets/admin/js/rule/Expression.js");
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Condition = function () {
    function Condition(rule, data, index) {
        _classCallCheck(this, Condition);

        this.rule = rule;
        this.expressions = [];
        this.data = data;
        this.index = index;
        this.lastPosition = 0;
        this.initView();
        this.attachListeners();
    }

    _createClass(Condition, [{
        key: 'initView',
        value: function initView() {
            var _this = this;

            var data = {
                id: this.data.id || '',
                conditionIndex: this.index
            };
            var template = new Template($('.templates .rule-condition'));
            template.setData(data);
            template.appendTo($('#conditions'));
            this.$node = template.getJqueryObject();

            if (this.data.groupedExpressions) {
                Object.keys(this.data.groupedExpressions).forEach(function (name) {
                    _this.addExpression(name, _this.data.groupedExpressions[name]);
                });
            } else {
                this.addExpression(null, []);
            }
            this.polishView();
        }
    }, {
        key: 'remove',
        value: function remove() {
            this.$node.remove();
            this.polishView();
        }
    }, {
        key: 'polishView',
        value: function polishView() {
            $('#conditions').find('.rule-condition .btn-remove-condition').removeClass('hidden');
            if ($('#conditions').find('.rule-condition').length < 2) {
                $('#conditions').find('.rule-condition .btn-remove-condition').addClass('hidden');
            }
            $('#conditions').find('.condition-separator').removeClass('hidden');
            $('#conditions').find('.rule-condition:first .condition-separator').addClass('hidden');
        }
    }, {
        key: 'attachListeners',
        value: function attachListeners() {
            var _this2 = this;

            this.$node.find('.btn-expression').on('click', function () {
                return _this2.addExpression(null, []);
            });
            this.$node.find('.btn-remove-condition').on('click', function () {
                return _this2.remove();
            });
        }
    }, {
        key: 'addExpression',
        value: function addExpression(name, data) {
            var expression = new __WEBPACK_IMPORTED_MODULE_0__Expression__["a" /* default */](this, name, data, this.lastPosition);
            this.expressions.push(expression);
            this.lastPosition++;
        }
    }]);

    return Condition;
}();

/* harmony default export */ __webpack_exports__["a"] = (Condition);

/***/ }),

/***/ "./resources/assets/admin/js/rule/Expression.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Selector__ = __webpack_require__("./resources/assets/admin/js/rule/Selector.js");
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



var Expression = function () {
    function Expression(condition, name, data, index) {
        _classCallCheck(this, Expression);

        this.condition = condition;
        this.name = name;
        this.data = data;
        this.index = index;
        this.initView();
        this.attachListeners();
    }

    _createClass(Expression, [{
        key: 'initView',
        value: function initView() {
            var data = {};
            data.expressionIndex = this.index;
            data.conditionIndex = this.condition.index;
            var template = new Template($('.templates .rule-expression'));
            template.setData(data);
            template.appendTo(this.condition.$node.find('.expressions'));
            this.$node = template.getJqueryObject();
            this.polishView();
        }
    }, {
        key: 'remove',
        value: function remove() {
            this.$node.remove();
            this.polishView();
        }
    }, {
        key: 'polishView',
        value: function polishView() {
            this.condition.$node.find('.rule-expression .remove-wrapper').removeClass('hidden');
            if (this.condition.$node.find('.rule-expression').length < 2) {
                this.condition.$node.find('.rule-expression .remove-wrapper').addClass('hidden');
            }
        }
    }, {
        key: 'attachListeners',
        value: function attachListeners() {
            var _this = this;

            var $select2Name = this.$node.find('.select2-expression-name');
            $select2Name.select2({ data: this.getAttributes(this.condition.rule.data.type) });
            if (this.name) {
                $select2Name.val(this.name).change();
            }
            $select2Name.on('select2:select', function (e) {
                return _this.onAttributeChanged($select2Name.select2('data')[0]);
            });
            $select2Name.trigger('select2:select');
            this.$node.find('.btn-remove-expression').on('click', function () {
                return _this.remove();
            });
        }
    }, {
        key: 'onAttributeChanged',
        value: function onAttributeChanged(attribute) {
            var data = {
                expressionIndex: this.index,
                conditionIndex: this.condition.index,
                value: 0,
                max_value: null
            };
            if (attribute.type == 'number' && this.data.length > 0) {
                data.value = this.data[0].value;
                data.max_value = this.data[0].max_value;
            }
            var template = new Template($('.templates .selector-' + attribute.type));
            template.setData(data);
            template.replaceIn(this.$node.find('.values-wrapper'));
            this.$valuesNode = template.getJqueryObject();
            if (attribute.type == "select2") {
                var selector = new __WEBPACK_IMPORTED_MODULE_0__Selector__["a" /* default */](this.$valuesNode.find('select'), attribute.id);
                selector.setSelected(attribute.id, this.data);
            }
        }
    }, {
        key: 'getAttributes',
        value: function getAttributes(type) {
            return [{
                "text": "Store",
                "children": [{
                    "id": "beat",
                    "text": "Beat",
                    "type": "select2",
                    "disabled": ['order', 'product', 'pricing'].indexOf(type) == -1
                }]
            }, {
                "text": "Product",
                "children": [{
                    "id": "variant",
                    "text": "Variant",
                    "type": "select2",
                    "disabled": ['product', 'pricing'].indexOf(type) == -1
                }, {
                    "id": "collection",
                    "text": "Collection",
                    "type": "select2",
                    "disabled": ['product'].indexOf(type) == -1
                }, {
                    "id": "price",
                    "text": "Price",
                    "type": "number",
                    "disabled": ['product'].indexOf(type) == -1
                }, {
                    "id": "quantity",
                    "text": "Quantity",
                    "type": "number",
                    "disabled": ['product'].indexOf(type) == -1
                }]
            }, {
                "text": "Brand",
                "children": [{
                    "id": "brand",
                    "text": "Brand",
                    "type": "select2",
                    "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                }, {
                    "id": "marketer",
                    "text": "Marketer",
                    "type": "select2",
                    "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                }, {
                    "id": "brand_quantity",
                    "text": "Brand Quantity",
                    "type": "number",
                    "disabled": ['order'].indexOf(type) == -1
                }, {
                    "id": "brand_amount",
                    "text": "Brand Amount",
                    "type": "number",
                    "disabled": ['order'].indexOf(type) == -1
                }]
            }, {
                "text": "Order",
                "children": [{
                    "id": "total_amount",
                    "text": "Total Amount",
                    "type": "number",
                    "disabled": ['order'].indexOf(type) == -1
                }, {
                    "id": "total_quantity",
                    "text": "Total Quantity",
                    "type": "number",
                    "disabled": ['order'].indexOf(type) == -1
                }]
            }, {
                "text": "Miscellaneous",
                "children": [{
                    "id": "medium",
                    "text": "Medium",
                    "type": "select2",
                    "disabled": ['product', 'order', 'pricing'].indexOf(type) == -1
                }]
            }];
        }
    }]);

    return Expression;
}();

/* harmony default export */ __webpack_exports__["a"] = (Expression);

/***/ }),

/***/ "./resources/assets/admin/js/rule/Rule.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Condition__ = __webpack_require__("./resources/assets/admin/js/rule/Condition.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Action__ = __webpack_require__("./resources/assets/admin/js/rule/Action.js");
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var Rule = function () {
    function Rule() {
        _classCallCheck(this, Rule);

        this.data = ruleData;
        this.conditions = [];
        this.lastPosition = 0;
        this.action = new __WEBPACK_IMPORTED_MODULE_1__Action__["a" /* default */](this);
        this.attachListeners();
        this.initView();
    }

    _createClass(Rule, [{
        key: 'initView',
        value: function initView() {
            var _this = this;

            $('#card-basic').find('input[name=type][value=' + this.data.type + ']').prop('checked', true);
            this.action.onRuleTypeChanged();
            if (this.data.conditions) {
                this.data.conditions.forEach(function (conditionData) {
                    _this.addCondition(conditionData);
                });
            } else {
                this.addCondition({});
            }
        }
    }, {
        key: 'attachListeners',
        value: function attachListeners() {
            var _this2 = this;

            $('#btn-condition').on('click', function () {
                return _this2.addCondition({});
            });
            $('#card-basic').find('input[name=type]').on('change', function (e) {
                if (e.target.checked) {
                    _this2.onTypeChanged(e.target.value);
                }
            });
        }
    }, {
        key: 'onTypeChanged',
        value: function onTypeChanged(type) {
            this.data.type = type;
            this.conditions.forEach(function (condition) {
                condition.remove();
            });
            this.conditions = [];
            this.addCondition({});
            this.action.onRuleTypeChanged();
        }
    }, {
        key: 'addCondition',
        value: function addCondition(data) {
            var condition = new __WEBPACK_IMPORTED_MODULE_0__Condition__["a" /* default */](this, data, this.lastPosition);
            this.conditions.push(condition);
            this.lastPosition++;
        }
    }]);

    return Rule;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Rule());

/***/ }),

/***/ "./resources/assets/admin/js/rule/Selector.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Selector = function () {
    function Selector($node, type) {
        _classCallCheck(this, Selector);

        this.$node = $node;
        switch (type) {
            case 'variant':
                this.attachVariantSelect2();
                break;

            case 'beat':
                this.attachBeatSelect2();
                break;

            case 'collection':
                this.attachCollectionSelect2();
                break;

            case 'brand':
                this.attachBrandSelect2();
                break;

            case 'marketer':
                this.attachMarketerSelect2();
                break;

            case 'medium':
                this.attachMediumSelect2();
                break;
        }
    }

    _createClass(Selector, [{
        key: 'getMediumData',
        value: function getMediumData() {
            return [{
                id: 'retailer',
                text: 'Retailer App'
            }, {
                id: 'agent',
                text: 'Agent App'
            }, {
                id: 'admin',
                text: 'Backend'
            }];
        }
    }, {
        key: 'attachVariantSelect2',
        value: function attachVariantSelect2() {
            var _this = this;

            this.$node.select2({
                minimumInputLength: 2,
                ajax: {
                    url: '/search/variants',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: function templateResult(obj) {
                    return _this.defaultTemplate(obj.id, obj.text || obj.product.name + ' [' + obj.value + ']');
                },
                templateSelection: function templateSelection(obj) {
                    return obj.text || obj.product.name + ' [' + obj.value + ']';
                }
            });
        }
    }, {
        key: 'attachBeatSelect2',
        value: function attachBeatSelect2() {
            var _this2 = this;

            this.$node.select2({
                minimumInputLength: 2,
                ajax: {
                    url: '/search/beats',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: function templateResult(obj) {
                    return _this2.defaultTemplate(obj.id, obj.text || obj.name);
                },
                templateSelection: function templateSelection(obj) {
                    return obj.text || obj.name;
                }
            });
        }
    }, {
        key: 'attachCollectionSelect2',
        value: function attachCollectionSelect2() {
            var _this3 = this;

            this.$node.select2({
                minimumInputLength: 2,
                ajax: {
                    url: '/search/collections',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: function templateResult(obj) {
                    return _this3.defaultTemplate(obj.id, obj.text || obj.name);
                },
                templateSelection: function templateSelection(obj) {
                    return obj.text || obj.name;
                }
            });
        }
    }, {
        key: 'attachBrandSelect2',
        value: function attachBrandSelect2() {
            var _this4 = this;

            this.$node.select2({
                minimumInputLength: 2,
                ajax: {
                    url: '/search/brands',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: function templateResult(obj) {
                    return _this4.defaultTemplate(obj.id, obj.text || obj.name);
                },
                templateSelection: function templateSelection(obj) {
                    return obj.text || obj.name;
                }
            });
        }
    }, {
        key: 'attachMarketerSelect2',
        value: function attachMarketerSelect2() {
            var _this5 = this;

            this.$node.select2({
                minimumInputLength: 2,
                ajax: {
                    url: '/search/marketers',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return { results: data };
                    }
                },
                templateResult: function templateResult(obj) {
                    return _this5.defaultTemplate(obj.id, obj.text || obj.name);
                },
                templateSelection: function templateSelection(obj) {
                    return obj.text || obj.name;
                }
            });
        }
    }, {
        key: 'attachMediumSelect2',
        value: function attachMediumSelect2() {
            var mediums = this.getMediumData();
            this.$node.select2({
                data: mediums
            });
        }
    }, {
        key: 'defaultTemplate',
        value: function defaultTemplate(id, text) {
            if (!id) {
                return text;
            }
            var html = '<div class="padding-10">';
            html += '<h5>' + text + '</h5>';
            html += '</div>';

            return $(html);
        }
    }, {
        key: 'setSelected',
        value: function setSelected(type, values) {
            var _this6 = this;

            if (values.length > 0) {
                var data = [];
                values.forEach(function (value) {
                    var optionData = {};
                    if (value.attribute) {
                        optionData = { id: value.attribute.id, text: value.attribute.name, name: value.attribute.name };
                    }
                    switch (type) {
                        case 'variant':
                            optionData.text = value.attribute.product.name + ' [' + value.attribute.value + ']';
                            optionData.name = value.attribute.product.name;
                            optionData.product = value.attribute.product;
                            optionData.value = value.attribute.value;
                            break;

                        case 'medium':
                            optionData = _this6.getMediumData().find(function (medium) {
                                return medium.id === value.value;
                            });
                            break;
                    }
                    data.push(optionData);
                    if (type != 'medium') {
                        _this6.appendOption(optionData);
                    }
                });
                if (type == 'medium') {
                    this.$node.val(data.map(function (medium) {
                        return medium.id;
                    })).trigger('change');
                }
                this.$node.trigger({ type: 'select2:select', params: { data: data } });
            }
        }
    }, {
        key: 'appendOption',
        value: function appendOption(value) {
            var option = new Option(value.text, value.id, true, true);
            this.$node.append(option).trigger('change');
        }
    }]);

    return Selector;
}();

/* harmony default export */ __webpack_exports__["a"] = (Selector);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/rule/Rule.js");


/***/ })

/******/ });