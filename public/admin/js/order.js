$('.return-cancel').on('click', function () {
    var $form = $(this).closest('form');
    dialog("Cancel Return", "Are you sure you want to cancel the return? This action cannot be undone.", function () {
        $form.submit();
    });
});
$('.return-modal').on('modalOpened', function () {
    var returnOrderId = $(this).find('input[name=id]').val();
    $(this).find(' .spinner').removeClass('hidden');
    $(this).find(' .refund-methods').addClass('hidden');
    $(this).find(' .refunds').addClass('hidden');
    $(this).find(' .submit-button').addClass('hidden');
    var $modal = $(this);
    var request = $.ajax({
        url: "/orders/return/" + returnOrderId + "/refund_amount",
        type: 'GET'
    });
    request.done(function (data) {
        $modal.find(' .spinner').addClass('hidden');
        $modal.find(' .credit-refund-value').text(data.storeCreditRefund);
        $modal.find(' .other-refund-value').text(data.balanceRefund);
        $modal.find(' .refunds').removeClass('hidden');
        $modal.find(' .submit-button').removeClass('hidden');
        if (data.balanceRefund > 0) {
            $modal.find(' .refund-methods').removeClass('hidden');
        }
    });
});

$('.items-select2').select2({});

$('.toggle-items').click(function () {
    $(this).find('i').toggleClass('fa-plus-circle').toggleClass('fa-minus-circle').closest('.items-by-status').find('.items-by-sku').slideToggle();
});

$('#change-address .mobile-country-picker').intlTelInput({
    autoPlaceholder: false,
    initialCountry: 'IN'
});

$('#agents-search').select2({
    placeholder: "Search Agent by name or code",
    minimumInputLength: 2,
    ajax: {
        url: function url(params) {
            return '/search/agents?type=sales';
        },
        dataType: 'json',
        processResults: function processResults(data) {
            data = $.map(data, function (obj) {
                obj.id = obj.id;
                obj.text = obj.name;
                return obj;
            });
            return {
                results: data
            };
        }
    }
});
$('#otp-modal').on('modalOpened', function (e, relatedTarget) {
    var shipmentId = $(relatedTarget).data('id');
    $(this).find('#shipment_id').val(shipmentId);
});

var Shipment = {
    init: function init() {
        $('.shipment .shipment-cancel').on('click', this.cancel);
        $('.shipment .shipment-deliver').on('click', this.deliver);
    },
    cancel: function cancel() {
        var id = $(this).data('shipment-id');
        var url = $(this).data('url');
        dialog("Cancel Shipment", "Are you sure you want to cancel this shipment? This action cannot be reversed.", function () {
            var request = $.ajax({
                url: url,
                data: { id: id },
                type: 'POST'
            });
            request.done(function (data) {
                location.reload();
            });
            request.fail(function () {});
        });
    }

    // deliver: function cancel() {
    //     var id = $(this).data('shipment-id');
    //     var url = $(this).data('url');
    //     dialog("Mark Shipment as Delivered", "Are you sure you want to deliver this shipment? This action cannot be reversed.", function () {
    //         var request = $.ajax({
    //             url: url,
    //             data: { id: id },
    //             type: 'POST'
    //         });
    //         request.done(function (data) {
    //             location.reload();
    //         });
    //         request.fail(function () {});
    //     });
    // },
};

Shipment.init();