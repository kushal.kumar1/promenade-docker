/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/Store.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Store = function () {
    function Store() {
        var _this = this;

        _classCallCheck(this, Store);

        $('.users-select').on('select2:select', function () {
            return _this.addUser();
        });
        $('.remove-user').on('click', function (e) {
            return _this.removeUser(e);
        });
        this.attachUserSearch();
        $('select[name="status"]').on('change', function (e) {
            return _this.toggleDisableReasons(e);
        });
    }

    _createClass(Store, [{
        key: 'addUser',
        value: function addUser() {
            $('#form-users').submit();
        }
    }, {
        key: 'removeUser',
        value: function removeUser(e) {
            $(e.currentTarget).closest('.form-remove-user').submit();
        }
    }, {
        key: 'attachUserSearch',
        value: function attachUserSearch() {
            $('.users-select').select2({
                placeholder: "Search & Add Users",
                minimumInputLength: 2,
                ajax: {
                    url: '/search/users',
                    dataType: 'json',
                    processResults: function processResults(data) {
                        data = $.map(data, function (obj) {
                            obj.id = obj.id;
                            obj.text = obj.first_name + " " + obj.last_name;
                            return obj;
                        });
                        return {
                            results: data
                        };
                    }
                }
            });
        }
    }, {
        key: 'toggleDisableReasons',
        value: function toggleDisableReasons(e) {
            var value = $(e.currentTarget).val();
            if (value == 0) {
                $('.inactive_reason :input').prop('disabled', false);
                $('.disable_reason :input').prop('disabled', true);
                $('.disable_reason').addClass('hidden');
                $('.inactive_reason').removeClass('hidden');
            } else if (value == 2) {
                $('.inactive_reason :input').prop('disabled', true);
                $('.disable_reason :input').prop('disabled', false);
                $('.inactive_reason').addClass('hidden');
                $('.disable_reason').removeClass('hidden');
            } else {
                $('.inactive_reason').addClass('hidden');
                $('.disable_reason').addClass('hidden');
            }
        }
    }]);

    return Store;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Store());

/***/ }),

/***/ "./resources/assets/admin/sass/app.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./resources/assets/admin/js/Store.js");
module.exports = __webpack_require__("./resources/assets/admin/sass/app.scss");


/***/ })

/******/ });