/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/Agent.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Agent = function () {
    function Agent() {
        var _this = this;

        _classCallCheck(this, Agent);

        $('.agent-disable, .agent-enable').on('click', function (e) {
            return _this.toggleStatus(e);
        });
        $('.beat-search').on('change', function () {
            return _this.enableMarketerSearch();
        });
        $('.btn-remove-map').on('click', function (e) {
            return _this.removeBeatMap(e);
        });
    }

    _createClass(Agent, [{
        key: 'enableMarketerSearch',
        value: function enableMarketerSearch() {
            $('.search-beat-marketers').select2({
                minimumInputLength: 2,
                disabled: false,
                multiple: true,
                ajax: {
                    url: '/search/marketers-by-beat',
                    dataType: 'json',
                    data: function data(params) {
                        return {
                            term: params.term,
                            beat_id: $('.beat-search').val()
                        };
                    },
                    processResults: function processResults(data) {
                        data = $.map(data, function (obj) {
                            obj.id = obj.id;
                            obj.text = obj.name;
                            return obj;
                        });
                        return {
                            results: data
                        };
                    }
                }
            });
        }
    }, {
        key: 'removeBeatMap',
        value: function removeBeatMap(e) {
            var $form = $(e.currentTarget).closest('form');
            console.log($form);
            dialog("Remove Entry", "Are you sure you want to delete this entry?", function () {
                $form.submit();
            });
        }
    }, {
        key: 'toggleStatus',
        value: function toggleStatus(e) {
            var target = e.currentTarget;
            var $form = $(target).closest('form');
            var title = "Disable Account";
            var description = "Are you sure you want to disable this account?";
            if ($(this).hasClass('user-enable')) {
                title = "Enable Account";
                description = "Are you sure you want to enable this account?";
            }
            dialog(title, description, function () {
                $form.submit();
            });
        }
    }]);

    return Agent;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Agent());

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/Agent.js");


/***/ })

/******/ });