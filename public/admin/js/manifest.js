/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/Manifest.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Manifest = function () {
    function Manifest() {
        var _this = this;

        _classCallCheck(this, Manifest);

        $('.shipment').on('change', 'input[name=boxes]', function (e) {
            return _this.updateBoxQuantity(e);
        });
        $('input[name=select_all]').on('click', function (e) {
            return _this.toggleSelection(e);
        });
        this.initAgentSearch();
    }

    _createClass(Manifest, [{
        key: 'toggleSelection',
        value: function toggleSelection(e) {
            var $target = $(e.currentTarget);
            if ($target.is(":checked")) {
                $('.item-checkbox').prop('checked', true);
            } else if ($target.is(":not(:checked)")) {
                $('.item-checkbox').prop('checked', false);
            }
            $('.item-checkbox').trigger('change');
        }
    }, {
        key: 'updateBoxQuantity',
        value: function updateBoxQuantity(e) {
            var _this2 = this;

            var $input = $(e.currentTarget);
            $input.closest('.shipment').find('.input-loader').removeClass('hidden');
            var id = $input.closest('.shipment').data('shipment_id');
            var boxes = $input.val();
            var Request = $.ajax('/shipments/update-boxes', {
                method: 'POST',
                data: {
                    id: id,
                    boxes: boxes
                }
            });

            Request.done(function (data) {
                $('.input-loader').addClass('hidden');
            });
            Request.fail(function (error) {
                return _this2.onError(error);
            });
        }
    }, {
        key: 'initAgentSearch',
        value: function initAgentSearch() {
            $('#agents-search').select2({
                placeholder: "Search Agent by name or code",
                minimumInputLength: 2,
                ajax: {
                    url: function url(params) {
                        return '/search/agents?type=delivery';
                    },
                    dataType: 'json',
                    processResults: function processResults(data) {
                        data = $.map(data, function (obj) {
                            obj.id = obj.id;
                            obj.text = obj.name;
                            return obj;
                        });
                        return {
                            results: data
                        };
                    }
                }
            });
        }
    }, {
        key: 'onError',
        value: function onError(error) {
            $('.input-loader').addClass('hidden');
            var message = error.responseJSON.message;
            if (message == undefined) {
                message = "An unexpected error has occurred";
            }
            var toast = new Toast(message);
            toast.show();
        }
    }]);

    return Manifest;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Manifest());

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/Manifest.js");


/***/ })

/******/ });