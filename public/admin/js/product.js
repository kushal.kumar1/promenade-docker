/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/product/Product.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__variant_js__ = __webpack_require__("./resources/assets/admin/js/product/variant.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__attribute_js__ = __webpack_require__("./resources/assets/admin/js/product/attribute.js");
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }




var Product = function () {
    function Product() {
        var _this = this;

        _classCallCheck(this, Product);

        __WEBPACK_IMPORTED_MODULE_0__variant_js__["a" /* Variants */].init();
        __WEBPACK_IMPORTED_MODULE_1__attribute_js__["a" /* Attributes */].init();

        $('.custom-select2').each(function (index, obj) {
            _this.initSelect2($(obj));
        });
        this.sortImages();
        $(document).on('click', '.product-image-delete', function (e) {
            return _this.deleteProductImage(e);
        });
        $(document).on('click', '.sync-product', function (e) {
            return _this.runSync(e);
        });
        $(document).on('click', '.sync-price', function (e) {
            return _this.runSync(e);
        });
        $(document).on('click', '.generate-barcode', function (e) {
            return _this.generateBarcode(e);
        });
        $(document).on('click', '.barcode-print', function (e) {
            return _this.printBarcodeLabels(e);
        });
    }

    _createClass(Product, [{
        key: 'runSync',
        value: function runSync(e) {
            var $selector = $(e.currentTarget);
            var productId = $selector.data('id');
            var url = $selector.data('url');
            $.ajax({
                url: url,
                type: 'POST',
                data: { id: productId }
            }).done(function (response) {
                var toast = new Toast(response.message);
                toast.show();
            }).fail(function (error) {
                var toast = new Toast(error.responseJSON.message);
                toast.show();
            });
        }
    }, {
        key: 'initSelect2',
        value: function initSelect2($selector) {
            var minInput = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;

            var selectorPlaceholder = $selector.data('placeholder');
            var selectorUrl = $selector.data('url');
            $selector.select2({
                minimumInputLength: minInput,
                placeholder: selectorPlaceholder,
                ajax: {
                    url: selectorUrl,
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return {
                            results: data
                        };
                    }
                },
                templateResult: this.resultsTemplate,

                templateSelection: function templateSelection(result) {
                    if (result.text !== '') {
                        return result.id + '  ' + result.text;
                    }
                    if (typeof result.name === 'undefined') {
                        return result.id + ' ' + result.reference;
                    }
                    return result.id + '  ' + result.name;
                }
            });
        }
    }, {
        key: 'resultsTemplate',
        value: function resultsTemplate(result) {
            if (!result.id) {
                return result.name;
            }
            if (typeof result.name === 'undefined') {
                return result.id + ' ' + result.reference;
            }

            var html = '<div class="padding-10">';
            html += '<h5 class="h6 bold">' + (result.id + ' ' + result.name) + '</h5>';
            html += '</div>';

            return $(html);
        }
    }, {
        key: 'generateBarcode',
        value: function generateBarcode(e) {
            var url = $(e.currentTarget).data('url');
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json'
            }).done(function () {
                location.reload();
            });
        }
    }, {
        key: 'printBarcodeLabels',
        value: function printBarcodeLabels(e) {
            var qty = $('.js-barcode-qty').val();
            if (qty % 2 === 0 && qty > 0) {
                var productId = $(e.currentTarget).data('id');
                var url = "/barcode/" + productId + "/" + qty;
                window.open(url, '_blank');
                Modal.hide($('#modal-barcode'));
            } else {
                var toast = new Toast('Invalid Quantity');
                toast.show();
            }
        }
    }, {
        key: 'sortImages',
        value: function sortImages() {
            var imagelist = document.getElementById('image-list');
            if (imagelist != null) {
                var sortable = Sortable.create(imagelist, {
                    animation: 150,
                    dataIdAttr: 'data-id',

                    onEnd: function onEnd(evt) {
                        var data = sortable.toArray();
                        var url = imagelist.dataset.destination;
                        $.ajax({
                            url: url,
                            type: 'post',
                            data: {
                                images: data
                            }
                        });
                    }
                });
            }
        }
    }, {
        key: 'deleteProductImage',
        value: function deleteProductImage(e) {
            var $button = $(e.currentTarget);
            dialog("Delete Image", "Are you sure you want to delete this Image? This action cannot be reversed.", function () {
                $.ajax({
                    url: "/products/" + $button.data('product-id') + "/images/delete",
                    type: 'post',
                    data: {
                        id: $button.data('id')
                    },
                    success: function success() {
                        $button.closest('div').remove();
                    }
                });
            });
        }
    }]);

    return Product;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Product());

/***/ }),

/***/ "./resources/assets/admin/js/product/attribute.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Attributes; });
var Attributes = {
    init: function init() {
        $('#attribute-add').on('click', function () {
            var selections = $('.attribute-select').select2('data');
            if (selections.length > 0) {
                var attribute = selections[0];
                Attributes.add(attribute);
            }
        });
        $('#attributes').on('click', '.attribute .remove', this.delete);
        this.attachSelect2();
    },
    add: function add(attribute) {
        $('.attribute-select').val(null).trigger('change');
        var attributeTemplate = new Template($('.templates .attribute'));
        attributeTemplate.setData(attribute);
        attributeTemplate.appendTo($('#attributes'));
    },
    attachSelect2: function attachSelect2() {
        $('.attribute-select').select2({
            minimumInputLength: 2,
            ajax: {
                url: '/search/attributes',
                dataType: 'json',
                processResults: function processResults(data) {
                    data = $.map(data, function (obj) {
                        obj.id = obj.id;
                        obj.text = obj.name;
                        return obj;
                    });
                    return {
                        results: data
                    };
                }
            }
        });
    },
    delete: function _delete() {
        var $attribute = $(this);
        dialog("Delete Attribute", "Are you sure you want to delete this attribute? This action cannot be reversed.", function () {
            $attribute.parent().remove();
        });
    }
};



/***/ }),

/***/ "./resources/assets/admin/js/product/variant.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Variants; });
var Variants = {
    init: function init() {
        $('.variants').on('click', '.variant .remove', this.delete);
        $('.variants').on('click', '.variant .restore', this.restore);
    },
    add: function add() {
        var $variant = $('#variants');
        var $clone = $('.templates .variant').clone();
        var index = $variant.data('length');
        $clone.find("input").each(function () {
            this.name = this.name.replace(/variant\[variantIndex\]/, "variant[" + index + "]");
        });
        index++;
        $variant.append($clone);
        $variant.data('length', index);
    },
    delete: function _delete() {
        var $variant = $(this).closest('.variant');
        dialog("Delete Variant", "Are you sure you want to delete this variant? This action cannot be reversed.", function () {
            var request = $.ajax({
                url: "/products/variants/delete",
                data: {
                    id: $variant.data('id')
                },
                type: 'POST'
            });
            request.done(function () {
                window.location.reload();
            });
            request.fail(function () {});
        });
    },
    restore: function restore() {
        var $variant = $(this).closest('.variant');
        var request = $.ajax({
            url: "/products/variants/restore",
            data: {
                id: $variant.data('id')
            },
            type: 'POST'
        });
        request.done(function () {
            window.location.reload();
        });
    }
};



/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/product/Product.js");


/***/ })

/******/ });