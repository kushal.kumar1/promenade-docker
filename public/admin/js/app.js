var isSidebarVisible = localStorage.getItem("isSidebarVisible");
var sidebar = $(".sidebar");

var toggleSidebar = function toggleSidebar() {
  if (isSidebarVisible == "true") {
    sidebar.addClass("active");
    $(".header").addClass("active");
    $(".container-wrapper").addClass("active");
    localStorage.setItem("isSidebarVisible", "true");
  } else {
    sidebar.removeClass("active");
    $(".header").removeClass("active");
    $(".container-wrapper").removeClass("active");
    localStorage.setItem("isSidebarVisible", "false");
  }
};

if (isSidebarVisible) {
  toggleSidebar();
}

$(".sidebar-close").on("click", function (e) {
  e.preventDefault();
  isSidebarVisible = "false";
  toggleSidebar();
});

$(".sidebar-open").on("click", function (e) {
  e.preventDefault();
  if (isSidebarVisible == "true") {
    isSidebarVisible = "false";
  } else {
    isSidebarVisible = "true";
  }
  toggleSidebar();
});

$(".has-submenu").on("click", function () {
  var $submenu = $(this).next(".submenu");
  var isActive = $submenu.hasClass("active");
  $(this).closest(".menu").find(".submenu").removeClass("active");
  if (!isActive) {
    $submenu.addClass("active");
  }
});

//Image Upload
$(".upload").on("click", function (e) {
  e.preventDefault();
  var target = $(this).data("target");
  $(target).trigger("click");
});

$("#upload-input").change(function (e) {
  $("#form-upload").submit();
});

$(".test-dialog").on("click", function () {
  dialog("Reset Settings", "This will restore all system settings to defaults. Are you sure that you want to proceed?", function () {
    alert("confirmed");
  });
});

$(".form").each(function () {
  var $form = $(this);
  if ($form.data("actions") != undefined) {
    var $actions = $($form.data("actions"));
    var initialData = $(this).serialize();
    $form.on("keyup change", ":input", function () {
      $actions.addClass("hidden");
      if ($form.serialize() != initialData) {
        $actions.removeClass("hidden");
      }
    });
  }
});

$(".btn-discard").on("click", function () {
  dialog("Discard all changes", "Any unsaved changes will be lost. Are you sure that you want to proceed?", function () {
    location.reload();
  });
});

$(".mobile-country-picker").intlTelInput({
  autoPlaceholder: false,
  initialCountry: "IN"
});

$(".grid").on("click", ".grid-reset", function () {
  var form = $(this).closest("form");
  var gridName = form.find("table").prop("id");
  clearCookiesForGrid(gridName);
  form.find("input").val("");
  form.find("select").prop("selectedIndex", 0);
  form.submit();
  return false;
});

$(".grid").on("change", "select", function (e) {
  $(this).closest("form").submit();
  return false;
});

$(".grid").on("click", ".pagination li a", function () {
  var grid = $(this).closest(".grid");
  var url = $(this).prop("href");
  submitGridForm(grid, url, null);
  return false;
});

$(".grid").on("click", 'thead th small a[title~="Sort"]', function () {
  var grid = $(this).closest(".grid");
  var url = $(this).prop("href");

  if (url === "") return false;

  submitGridForm(grid, url, null);
  return false;
});

$(".grid").on("submit", "form", function () {
  var data = $(this).serialize();
  var grid = $(this).closest(".grid");
  submitGridForm(grid, "", data);
  return false;
});

function submitGridForm(grid, url, data) {
  $(".cache_form .changed_through_ajax").val("1");
  $(".ajax-loader").show();
  $.ajax(url, {
    type: "GET",
    data: data,
    cache: false,
    success: function success(data) {
      grid.html(data);
      $(".ajax-loader").hide();
    },
    error: function error(data) {}
  });
}

function clearCookiesForGrid(gridName) {
  var cookies = document.cookie.split(";");
  for (var i = 0; i < cookies.length; i++) {
    var c = cookies[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1, c.length);
    }if (c.indexOf(gridName) == 0) {
      cookieName = c.split("=")[0];
      document.cookie = cookieName + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    }
  }
}

$(".grid").on("change", ".export_checkbox", function () {
  var grid = $(this).closest(".grid");
  var exportButton = grid.find(".excel_export a");
  exportButton.prop("href", exportButton.prop("href").replace("&" + $(this).prop("name") + "=" + $(this).val(), ""));
  exportButton.prop("href", exportButton.prop("href").replace($(this).prop("name") + "=" + $(this).val(), ""));
  if (this.checked) exportButton.prop("href", addParameter(exportButton.prop("href"), $(this).prop("name"), $(this).val(), false));
});

$(".grid").on("change", ".select_all_checkbox", function () {
  var checkboxes = $(this).closest(".grid").find(".select-checkbox");
  checkboxes.filter(":checked").trigger("click");
  if (this.checked) {
    checkboxes.trigger("click");
  }
});

$(".grid").on("click", ".bulk-actions .action > a, .btn-custom > a", function (e) {
  e.preventDefault();
  if ($(this).attr("href").charAt(0) === "#") {
    return false;
  }

  var href = $(this).prop("href");
  var $form = $(this).closest("form");
  var destination = $(this).parent().attr("data-destination");
  var itemsCount = getSelectedRecords($(this).closest(".grid"));

  if (itemsCount == 0) {
    itemsCount = getTotalRecords($(this).closest(".grid"));
  }
  var title = "Are you Sure?";
  var message = "This will affect " + itemsCount + " items based on the filters selected.";

  dialog(title, message, function () {
    // toggleSubmitDisable($form);
    var request = $.ajax(href, {
      type: "POST",
      data: $form.serialize(),
      cache: false
    });

    request.done(function (response) {
      return onBulkActionCompleted(destination, response);
    });
    request.fail(function (error) {
      var errorFunction = $form.data("error");
      if (errorFunction != undefined) {
        window[errorFunction](error);
      } else if (error.status == 422) {
        showFormErrors($form, error);
        showMessage(error);
      } else {
        showMessage(error);
      }
    });
  });
});

function getTotalRecords($grid) {
  var recordsRegex = /Showing records [0-9]+—[0-9]+ of ([0-9]+)/;
  var match = recordsRegex.exec($grid.find(".showing_records").text());
  return match ? match[1] : 0;
}

function getSelectedRecords($grid) {
  var checkboxes = $grid.find(".select-checkbox");
  return checkboxes.filter(":checked").length;
}

function onBulkActionCompleted(destination, response) {
  if (destination != undefined) {
    if (destination != false) {
      var regex = /#(\S+)#/;
      while (match = regex.exec(destination)) {
        path = match[1].split(".");
        var value = response;
        for (var i = 0; i < path.length; i++) {
          value = value[path[i]];
        }destination = destination.replace(match[0], value == undefined ? "" : value);
        match = regex.exec(destination);
      }
      window.location = destination;
    }
  } else {
    window.location.reload();
  }
}

function addParameter(url, parameterName, parameterValue, replaceDuplicates) {
  var cl;
  if (url.indexOf("#") > 0) {
    cl = url.indexOf("#");
    urlhash = url.substring(url.indexOf("#"), url.length);
  } else {
    urlhash = "";
    cl = url.length;
  }
  sourceUrl = url.substring(0, cl);

  var urlParts = sourceUrl.split("?");
  var newQueryString = "";

  if (urlParts.length > 1) {
    var parameters = urlParts[1].split("&");
    for (var i = 0; i < parameters.length; i++) {
      var parameterParts = parameters[i].split("=");
      if (!(replaceDuplicates && parameterParts[0] == parameterName)) {
        if (newQueryString === "") newQueryString = "?";else newQueryString += "&";
        newQueryString += parameterParts[0] + "=" + (parameterParts[1] ? parameterParts[1] : "");
      }
    }
  }
  if (newQueryString === "") newQueryString = "?";

  if (newQueryString !== "" && newQueryString != "?") newQueryString += "&";
  newQueryString += parameterName + "=" + (parameterValue ? parameterValue : "");

  return urlParts[0] + newQueryString + urlhash;
}

$('.form-button').on('click', function () {
  var $form = $($(this).data('form'));
  if ($form == undefined) {
    $form = $(this).closest('form');
  }
  var url = $(this).data('action');
  if (url == undefined) {
    url = $form.prop('action');
  }
  toggleSubmitDisable($form);
  onFormSubmitted($form, url);
});

$(document).on('submit', '.form', function () {
  var $form = $(this);
  var url = $form.prop('action');
  toggleSubmitDisable($form);
  onFormSubmitted($form, url);
  return false;
});

function toggleSubmitDisable($form) {
  $btn = $form.find('.btn');
  var isdisabled = function () {
    return $btn.attr('disabled') != undefined;
  }();
  isdisabled ? $btn.removeAttr('disabled') : $btn.attr('disabled', 'disabled');
  $btn.toggleClass('loading');
}

function onFormSubmitted($form, url) {
  $form.find('.mobile-country-picker').each(function () {
    $(this).val($(this).intlTelInput("getNumber"));
  });
  var data = $form.serialize();
  var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
  if ($form.hasClass('form-multipart')) {
    data = new FormData($form[0]);
    contentType = false;
  }

  var method = $form.prop('method');
  var request = ajax(url, method, data, contentType);

  request.done(function (response) {
    var successFunction = $form.data('success');
    var destination = $form.data('destination');
    if (successFunction) {
      window[successFunction](response);
    } else if (destination != undefined) {
      if (destination != false) {
        var regex = /#(\S+)#/;
        while (match = regex.exec(destination)) {
          path = match[1].split('.');
          var value = response;
          for (var i = 0; i < path.length; i++) {
            value = value[path[i]];
          }destination = destination.replace(match[0], value == undefined ? "" : value);
          match = regex.exec(destination);
        }
        window.location = destination;
      }
    } else {
      location.reload();
    }
  });

  request.fail(function (error) {
    toggleSubmitDisable($form);
    var errorFunction = $form.data('error');
    if (errorFunction != undefined) {
      window[errorFunction](error);
    } else if (error.status == 422) {
      showFormErrors($form, error);
      showMessage(error);
    } else {
      showMessage(error);
    }
  });
}

function ajax(url, method, data, contentType) {
  return $.ajax({
    url: url,
    data: data,
    type: method,
    processData: false,
    contentType: contentType
  });
}

function showFormErrors($form, error) {
  var errors = error.responseJSON.errors;
  $form.find('.error').removeClass('active');
  $form.find('.form-control').removeClass('has-error');
  for (key in errors) {
    var $field = $form.find('[name="' + key + '"]');
    $field.addClass('has-error');
    var toast = new Toast(errors[key]);
    toast.show();
    var $error = $form.find('.error.error-' + key);
    $error.siblings('input').addClass('has-error');
    $error.text(errors[key]);
    $error.addClass('active');
  }
}

function showMessage(error) {
  var message = error.responseJSON.message;
  if (message == undefined) {
    message = "An unexpected error has occurred";
  }
  var toast = new Toast(message);
  toast.show();
}

function showAllErrorsInToast(error) {
  var errors = error.responseJSON.errors;
  var message = '';
  for (key in errors) {
    message = message + "<br\>" + errors[key];
  }
  if (error.responseJSON.message == undefined) {
    message = "An unexpected error has occurred";
  } else {
    message = message + "<br\>" + error.responseJSON.message;
  }
  var toast = new Toast(message, 'error');
  toast.show();
}

var Modal = function ($) {
  return {
    $body: $('body'),
    $modalBackdrop: $('.modal-backdrop'),
    init: function init() {
      $(document).on('click', '[data-dismiss="modal"]', function () {
        Modal.hide($(this).closest('.modal'));
      });
      $(document).on('click', '[data-modal]', function () {
        var selector = $(this).data('modal');
        Modal.show($(selector), this);
      });
    },
    hide: function hide($selector) {
      $selector.removeClass('active');
      this.$modalBackdrop.removeClass('active');
      this.$body.css('overflow', 'auto');
      $selector.trigger('modalClosed');
    },
    show: function show($selector, triggeredBy) {
      this.$body.css('overflow', 'hidden');
      this.$modalBackdrop.addClass('active');
      $selector.addClass('active');
      $selector.trigger('modalOpened', [triggeredBy]);
    }
  };
}(jQuery);

Modal.init();
function dialog(title, message, success, reject) {
  $dialog = $('#dialog');
  $dialog.find('.title').text(title);
  $dialog.find('.message').text(message);
  $buttonSuccess = $dialog.find('.button-success');
  $buttonReject = $dialog.find('.button-reject');
  $buttonClose = $dialog.find('.close');
  $buttonSuccess.on('click', function () {
    $buttonSuccess.off('click');
    $buttonReject.off('click');
    Modal.hide($dialog);
    success();
  });
  $buttonReject.on('click', function () {
    exitDialog($buttonSuccess, $buttonReject, reject);
  });
  $buttonClose.on('click', function () {
    exitDialog($buttonSuccess, $buttonReject, reject);
  });
  Modal.show($dialog);
}

function exitDialog($buttonSuccess, $buttonReject, reject) {
  $buttonSuccess.off('click');
  $buttonReject.off('click');
  Modal.hide($dialog);
  if (reject !== undefined) {
    reject();
  }
}

var Template = function Template($template) {
  var $clone = null;
  this.setData = function (data) {
    var match;
    var regex = /{#\s*(\S+)\s*#}/;
    $clone = $template.clone();
    $clone.html(function (i, html) {
      while (match = regex.exec(html)) {
        var value = data[match[1]];
        html = html.replace(match[0], value == undefined ? "" : value);
        match = regex.exec(html);
      }
      return html;
    });
    $clone.find('select').each(function (index) {
      var value = $(this).data('value');
      if (value != undefined) {
        $(this).val(value);
      }
    });
    $clone.find('.mobile-country-picker').intlTelInput({
      autoPlaceholder: false,
      initialCountry: 'IN'
    });
  };
  this.appendTo = function ($container) {
    $container.append($clone);
  };
  this.replaceIn = function ($container) {
    $container.html('');
    this.appendTo($container);
  };
  this.getJqueryObject = function () {
    return $clone;
  };
};

$(document).on('click', '.toast-dismiss', function () {
  $(this).closest('.toast').remove();
});

var Toast = function Toast(message) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'info';


  var $toastHtml = $('.templates>.toast').clone();
  $toastHtml.find('.toast-message').text(message);
  var defaultTimeout = -1;

  switch (type) {
    case 'success':
      {
        $toastHtml.find('.toast-heading').text('SUCCESS');
        $toastHtml.addClass('success');
        $toastHtml.find('.toast-icon').addClass('fa').addClass(' fa-check-circle');
        defaultTimeout = 6000;
        break;
      }
    case 'error':
      {
        $toastHtml.find('.toast-heading').text('ERROR');
        $toastHtml.addClass('error');
        $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-times-circle');
        break;
      }
    case 'warn':
      {
        $toastHtml.find('.toast-heading').text('WARNING');
        $toastHtml.addClass('warn');
        $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-exclamation-circle');
        defaultTimeout = 10000;
        break;
      }
    case 'info':
    default:
      {
        $toastHtml.find('.toast-heading').text('INFO');
        $toastHtml.addClass('info');
        $toastHtml.find('.toast-icon').addClass('fa').addClass('fa-info-circle');
        defaultTimeout = 6000;
        break;
      }
  }

  this.show = function () {
    var timeout = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    var self = this;
    $('#notification-container').append($toastHtml);
    $toastHtml.show();

    if (timeout == null) {
      timeout = defaultTimeout;
    }

    if (timeout > 0) {
      setTimeout(function () {
        self.hide();
      }, timeout);
    }
  };
  this.hide = function () {
    $toastHtml.remove();
  };
};