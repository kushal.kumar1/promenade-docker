/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/Search.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Search = function () {
    function Search() {
        var _this = this;

        _classCallCheck(this, Search);

        var searchElems = this.getSearchElems();
        searchElems.forEach(function (searchElem) {
            _this.attachSelect2(searchElem);
        });
    }

    _createClass(Search, [{
        key: 'attachSelect2',
        value: function attachSelect2(searchElem) {
            var _this2 = this;

            $(searchElem.selector).select2({
                minimumInputLength: searchElem.hasOwnProperty('min_input') ? searchElem.min_input : 2,
                ajax: {
                    url: searchElem.url,
                    dataType: 'json',
                    processResults: function processResults(data) {
                        return _this2.processResults(data);
                    }
                }
            });
        }
    }, {
        key: 'processResults',
        value: function processResults(data) {
            data = $.map(data, function (obj) {
                obj.id = obj.id;
                obj.text = obj.name || obj.reference || obj.label;
                return obj;
            });

            return {
                results: data
            };
        }

        //add more common searches here

    }, {
        key: 'getSearchElems',
        value: function getSearchElems() {
            return [{
                'selector': '.beat-search',
                'url': '/search/beats'
            }, {
                'selector': '.brand-search',
                'url': '/search/brands'
            }, {
                'selector': '.marketer-search',
                'url': '/search/marketers'
            }, {
                'selector': '.agent-search',
                'url': '/search/agents'
            }, {
                'selector': '.agent-search-picker',
                'url': '/search/agents?type=picker'
            }, {
                'selector': '.select-rack',
                'url': '/search/racks'
            }, {
                'selector': '.beat-locations-search',
                'url': '/search/beat-locations',
                'min_input': '0'
            }, {
                'selector': '.warehouse-search',
                'url': '/search/warehouses',
                'min_input': '0'
            }, {
                'selector': '.storages-search',
                'url': '/search/storages',
                'min_input': '1'
            }];
        }
    }]);

    return Search;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Search());

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/Search.js");


/***/ })

/******/ });