/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/admin/js/Picklist.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Picklist = function () {
    function Picklist() {
        var _this = this;

        _classCallCheck(this, Picklist);

        this.picklistId = $('#item-search').data('id');
        $('#item-search').on('input', function (e) {
            return _this.onQueryChanged(e);
        });
        $('#item-suggestions').on('click', '.js-btn-submit', function (e) {
            return _this.processItem(e);
        });
        $('#picked-items').on('click', '.btn-remove', function (e) {
            return _this.removeItem(e);
        });
        $('#item-suggestions').on('click', '.js-add-skip-row', function (e) {
            return _this.addSkipRow(e);
        });
        $('#item-suggestions').on('click', '.js-delete-skip-row', function (e) {
            return _this.deleteSkipRow(e);
        });
    }

    _createClass(Picklist, [{
        key: 'onQueryChanged',
        value: function onQueryChanged(e) {
            var _this2 = this;

            $('.suggestions-overlay').removeClass('hidden');
            clearTimeout(this.searchTimer);
            this.searchTimer = setTimeout(function () {
                var barcode = e.target.value;
                if (barcode.length >= 8) {
                    if (_this2.searchRequest) {
                        _this2.searchRequest.abort();
                    }
                    _this2.searchRequest = $.ajax('/picklists/items/search', {
                        method: 'GET',
                        data: { barcode: barcode, picklist_id: _this2.picklistId }
                    });
                    _this2.searchRequest.done(function (items) {
                        return $('#item-suggestions').html(items);
                    });
                    _this2.searchRequest.fail(function (error) {
                        return _this2.onError(error);
                    });
                }
            }, 100);
        }
    }, {
        key: 'processItem',
        value: function processItem(e) {
            var _this3 = this;

            $('.picked-items-overlay').removeClass('hidden');
            var form = $(e.currentTarget).closest('form');
            this.pickRequest = $.ajax('/picklists/items/add', {
                method: 'POST',
                data: form.serialize()
            });
            this.pickRequest.done(function (items) {
                $('#picked-items').html(items);
                $('#item-search').val("");
                form.closest('.item').remove();
                location.reload();
            });
            this.pickRequest.fail(function (error) {
                return _this3.onError(error);
            });
        }
    }, {
        key: 'removeItem',
        value: function removeItem(e) {
            var _this4 = this;

            $('.picked-items-overlay').removeClass('hidden');
            var $target = $(e.currentTarget).closest('.item');
            this.removeRequest = $.ajax('/picklists/items/remove', {
                method: 'POST',
                data: { 'product_id': $target.data('product_id'), picklist_id: this.picklistId }
            });
            this.removeRequest.done(function (items) {
                $('#picked-items').html(items);
                location.reload();
            });
            this.removeRequest.fail(function (error) {
                return _this4.onError(error);
            });
        }
    }, {
        key: 'addSkipRow',
        value: function addSkipRow(e) {
            var $target = $(e.currentTarget).closest('.item');
            var template = new Template($('.templates .skip-row'));
            var data = {
                index: $target.find('.skip-row').length
            };
            template.setData(data);
            template.appendTo($target.find('#js-skip-rows'));
        }
    }, {
        key: 'deleteSkipRow',
        value: function deleteSkipRow(e) {
            var $target = $(e.currentTarget).closest('.skip-row');
            $target.remove();
        }
    }, {
        key: 'onError',
        value: function onError(error) {
            $('.picked-items-overlay').addClass('hidden');
            var message = error.responseJSON.message;
            if (message == undefined) {
                message = "An unexpected error has occurred";
            }
            var toast = new Toast(message);
            toast.show();
        }
    }]);

    return Picklist;
}();

/* harmony default export */ __webpack_exports__["default"] = (new Picklist());

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/admin/js/Picklist.js");


/***/ })

/******/ });